.class public Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
.super Ljava/lang/Object;
.source "BrightnessControlGridUI.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final LOW_BATTERY_THRESHOLD:I = 0x5


# instance fields
.field private final GAP_BRIGHTNESS:I

.field private final MAX_AUTO_BRIGHTNESS:I

.field private final MAX_AUTO_BRIGHTNESS_LEVEL:I

.field private final MAX_BRIGHTNESS:I

.field private final MIN_AUTO_BRIGHTNESS:I

.field private final MIN_AUTO_BRIGHTNESS_LEVEL:I

.field private final MSG_BRIGHTNESS_UPDATE:I

.field private mAccelRateVal:F

.field private mAutoBg:Landroid/widget/ImageView;

.field private mAutoBrightnessControlListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

.field private mAutoSeekBar:Landroid/widget/SeekBar;

.field private mBrightnessAutoEnable:Z

.field private mBrightnessControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

.field private mBrightnessControlGridUIViewMain:Landroid/widget/RelativeLayout;

.field private mBrightnessControlListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mBtnBrightnessDown:Landroid/widget/ImageButton;

.field private mBtnBrightnessUp:Landroid/widget/ImageButton;

.field private mCBAutoBrightness:Landroid/widget/CheckBox;

.field private mCBAutoBrightnessButton:Landroid/widget/TextView;

.field private mCurBrightnessVal:I

.field private mDualFolderType:Z

.field private final mHandler:Landroid/os/Handler;

.field private mIBtnBrightnessRemoveView:Landroid/widget/LinearLayout;

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsFolderClose:Z

.field private mIsLongkeyProcessing:Z

.field private mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

.field private mLevel:Landroid/widget/TextView;

.field private mLevelLayout:Landroid/widget/RelativeLayout;

.field private mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mMaxBrightnessVal:I

.field private mNoSupportLightSensor:Landroid/widget/TextView;

.field private mParentSvcHandler:Landroid/os/Handler;

.field private mResolver:Landroid/content/ContentResolver;

.field private mScreenBrightnessMinimum:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekBarTopMarginLayout:Landroid/widget/LinearLayout;

.field private mService:Landroid/app/Service;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 4
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    .line 57
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    .line 63
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIViewMain:Landroid/widget/RelativeLayout;

    .line 65
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 69
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    .line 71
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessUp:Landroid/widget/ImageButton;

    .line 73
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessDown:Landroid/widget/ImageButton;

    .line 75
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIBtnBrightnessRemoveView:Landroid/widget/LinearLayout;

    .line 77
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    .line 79
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    .line 81
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoBg:Landroid/widget/ImageView;

    .line 83
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevelLayout:Landroid/widget/RelativeLayout;

    .line 87
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBarTopMarginLayout:Landroid/widget/LinearLayout;

    .line 91
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mNoSupportLightSensor:Landroid/widget/TextView;

    .line 93
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevel:Landroid/widget/TextView;

    .line 94
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mTitle:Landroid/widget/TextView;

    .line 96
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    .line 98
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightnessButton:Landroid/widget/TextView;

    .line 100
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 102
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    .line 104
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F

    .line 106
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    .line 108
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    .line 110
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mDualFolderType:Z

    .line 112
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsFolderClose:Z

    .line 116
    const/16 v1, 0xff

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->MAX_BRIGHTNESS:I

    .line 118
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->MIN_AUTO_BRIGHTNESS:I

    .line 120
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->MIN_AUTO_BRIGHTNESS_LEVEL:I

    .line 122
    const/16 v1, 0xc8

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->MAX_AUTO_BRIGHTNESS:I

    .line 124
    const/16 v1, 0xa

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->MAX_AUTO_BRIGHTNESS_LEVEL:I

    .line 126
    const/16 v1, 0x19

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->GAP_BRIGHTNESS:I

    .line 128
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->MSG_BRIGHTNESS_UPDATE:I

    .line 132
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 136
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mHandler:Landroid/os/Handler;

    .line 302
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 738
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 849
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 893
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoBrightnessControlListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    .line 162
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    move-object v0, p1

    .line 164
    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 165
    .local v0, "svc":Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    .line 166
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 167
    return-void
.end method

.method private ClickOutSideLayout()V
    .locals 2

    .prologue
    .line 914
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 915
    return-void
.end method

.method private GetMaxBrightnessVal()I
    .locals 6

    .prologue
    const/16 v5, 0x82

    const/16 v4, 0x69

    const/16 v3, 0x50

    const/16 v2, 0x37

    const/16 v1, 0x1e

    .line 659
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-gt v0, v1, :cond_0

    .line 660
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    .line 692
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    return v0

    .line 662
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-gt v0, v2, :cond_1

    .line 663
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0

    .line 665
    :cond_1
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-gt v0, v3, :cond_2

    .line 666
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0

    .line 668
    :cond_2
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-gt v0, v4, :cond_3

    .line 669
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0

    .line 671
    :cond_3
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-gt v0, v5, :cond_4

    .line 672
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0

    .line 674
    :cond_4
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    const/16 v1, 0x9b

    if-gt v0, v1, :cond_5

    .line 675
    const/16 v0, 0x9b

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0

    .line 677
    :cond_5
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    const/16 v1, 0xb4

    if-gt v0, v1, :cond_6

    .line 678
    const/16 v0, 0xb4

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0

    .line 680
    :cond_6
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    const/16 v1, 0xcd

    if-gt v0, v1, :cond_7

    .line 681
    const/16 v0, 0xcd

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0

    .line 683
    :cond_7
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    const/16 v1, 0xe6

    if-gt v0, v1, :cond_8

    .line 684
    const/16 v0, 0xe6

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0

    .line 689
    :cond_8
    const/16 v0, 0xff

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mMaxBrightnessVal:I

    goto :goto_0
.end method

.method private Init()V
    .locals 17

    .prologue
    .line 323
    new-instance v10, Landroid/content/IntentFilter;

    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    .line 324
    .local v10, "filter":Landroid/content/IntentFilter;
    const-string v13, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v10, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v13, v14, v10}, Landroid/app/Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 327
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    .line 328
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v13}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->isAutoBrightness(Landroid/content/ContentResolver;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    .line 330
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    const v14, 0x7f030004

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    .line 333
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d000a

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIViewMain:Landroid/widget/RelativeLayout;

    .line 339
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v13, v13, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_2

    const/4 v13, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsFolderClose:Z

    .line 343
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    check-cast v13, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v13}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v13

    if-eqz v13, :cond_3

    const v13, 0x7f09005a

    :goto_1
    invoke-virtual {v14, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 346
    .local v9, "displayWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    check-cast v13, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v13}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v13

    if-eqz v13, :cond_4

    const v13, 0x7f090059

    :goto_2
    invoke-virtual {v14, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 350
    .local v8, "displayHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    sget-object v14, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/app/Service;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 352
    .local v1, "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    sget-object v13, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v14}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090015

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    sub-int v14, v9, v14

    invoke-interface {v1, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 356
    .local v6, "cornertabX":I
    sget-object v13, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-interface {v1, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 357
    .local v7, "cornertabY":I
    sget-object v13, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    invoke-interface {v1, v13, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 364
    .local v5, "cornertabDisplayHeight":I
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090010

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v14}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f09000d

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    invoke-direct {v11, v13, v14}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 368
    .local v11, "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    check-cast v13, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v13}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v13

    if-eqz v13, :cond_5

    const v13, 0x7f09005a

    :goto_3
    invoke-virtual {v14, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    div-int/lit8 v13, v13, 0x2

    if-le v6, v13, :cond_6

    .line 372
    const/16 v13, 0x15

    invoke-virtual {v11, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 373
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f09000f

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    invoke-virtual {v11, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 383
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090008

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    iput v13, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 386
    iget v13, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v14}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f09000d

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    add-int/2addr v13, v14

    if-le v7, v13, :cond_0

    .line 388
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f09000d

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    div-int/lit8 v13, v13, 0x2

    sub-int v13, v7, v13

    iput v13, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 392
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090008

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    sub-int v13, v5, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v14}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f09000d

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    sub-int v12, v13, v14

    .line 396
    .local v12, "maxCornertabBottomMargin":I
    iget v13, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    if-le v13, v12, :cond_7

    .end local v12    # "maxCornertabBottomMargin":I
    :goto_5
    iput v12, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 399
    const/16 v13, 0xc

    invoke-virtual {v11, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 403
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIViewMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v13, v11}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 412
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d001b

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessUp:Landroid/widget/ImageButton;

    .line 414
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d001a

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessDown:Landroid/widget/ImageButton;

    .line 416
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d000d

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIBtnBrightnessRemoveView:Landroid/widget/LinearLayout;

    .line 418
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d000f

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    .line 419
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0010

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightnessButton:Landroid/widget/TextView;

    .line 420
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0013

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevel:Landroid/widget/TextView;

    .line 422
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0012

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevelLayout:Landroid/widget/RelativeLayout;

    .line 426
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0014

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBarTopMarginLayout:Landroid/widget/LinearLayout;

    .line 430
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0016

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    .line 431
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 434
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0011

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mNoSupportLightSensor:Landroid/widget/TextView;

    .line 437
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessUp:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 438
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessDown:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 439
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIBtnBrightnessRemoveView:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIBtnBrightnessRemoveView:Landroid/widget/LinearLayout;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setHoverPopupType(I)V

    .line 441
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 442
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightnessButton:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessUp:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v13, v14}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 445
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessDown:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v13, v14}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 447
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessUp:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 448
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessDown:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 450
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0009

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 452
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIViewMain:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 456
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const/4 v14, 0x2

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/widget/RelativeLayout;->setLayerType(ILandroid/graphics/Paint;)V

    .line 458
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0018

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    .line 459
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 460
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    const v14, 0x7f0d0017

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoBg:Landroid/widget/ImageView;

    .line 484
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v13, :cond_9

    sget-boolean v13, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-eqz v13, :cond_9

    .line 485
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mDualFolderType:Z

    if-eqz v13, :cond_8

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsFolderClose:Z

    if-eqz v13, :cond_8

    .line 486
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v13}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->getScreenBrightness(Landroid/content/ContentResolver;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 487
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    rsub-int v14, v14, 0xff

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setMax(I)V

    .line 488
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    sub-int/2addr v14, v15

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 489
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    .line 490
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 505
    :goto_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    const/4 v14, 0x0

    new-instance v15, Landroid/content/IntentFilter;

    const-string v16, "android.intent.action.BATTERY_CHANGED"

    invoke-direct/range {v15 .. v16}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v14, v15}, Landroid/app/Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    .line 506
    .local v2, "bat":Landroid/content/Intent;
    const-string v13, "level"

    const/4 v14, -0x1

    invoke-virtual {v2, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 507
    .local v3, "batNow":I
    const/4 v13, 0x5

    if-gt v3, v13, :cond_1

    .line 508
    const-string v13, "status"

    const/4 v14, 0x1

    invoke-virtual {v2, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 509
    .local v4, "battStatus":I
    const-string v13, "level"

    const/4 v14, 0x0

    invoke-virtual {v2, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const/4 v14, 0x5

    if-gt v13, v14, :cond_1

    const/4 v13, 0x2

    if-eq v4, v13, :cond_1

    .line 510
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->setButtonsDisable()V

    .line 513
    .end local v4    # "battStatus":I
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->UpdateUI()V

    .line 514
    return-void

    .line 339
    .end local v1    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .end local v2    # "bat":Landroid/content/Intent;
    .end local v3    # "batNow":I
    .end local v5    # "cornertabDisplayHeight":I
    .end local v6    # "cornertabX":I
    .end local v7    # "cornertabY":I
    .end local v8    # "displayHeight":I
    .end local v9    # "displayWidth":I
    .end local v11    # "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 343
    :cond_3
    const v13, 0x7f090059

    goto/16 :goto_1

    .line 346
    .restart local v9    # "displayWidth":I
    :cond_4
    const v13, 0x7f09005a

    goto/16 :goto_2

    .line 368
    .restart local v1    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .restart local v5    # "cornertabDisplayHeight":I
    .restart local v6    # "cornertabX":I
    .restart local v7    # "cornertabY":I
    .restart local v8    # "displayHeight":I
    .restart local v11    # "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    const v13, 0x7f090059

    goto/16 :goto_3

    .line 378
    :cond_6
    const/16 v13, 0x9

    invoke-virtual {v11, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 379
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v13}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f09000e

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    invoke-virtual {v11, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    goto/16 :goto_4

    .line 396
    .restart local v12    # "maxCornertabBottomMargin":I
    :cond_7
    iget v12, v11, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_5

    .line 492
    .end local v12    # "maxCornertabBottomMargin":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v13}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->getScreenAutoBrightness(Landroid/content/ContentResolver;)I

    move-result v13

    add-int/lit8 v13, v13, 0x0

    div-int/lit8 v13, v13, 0x14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 494
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    const/16 v14, 0xa

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setMax(I)V

    .line 495
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 496
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setMode(I)V

    goto/16 :goto_6

    .line 499
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v13}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->getScreenBrightness(Landroid/content/ContentResolver;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 500
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    rsub-int v14, v14, 0xff

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setMax(I)V

    .line 501
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    sub-int/2addr v14, v15

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_6
.end method

.method private SetAutoBrightnessLevel()V
    .locals 2

    .prologue
    .line 649
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevel:Landroid/widget/TextView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    return-void
.end method

.method private SetAutoBrightnessMode()V
    .locals 1

    .prologue
    .line 726
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v0, :cond_0

    .line 728
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->startAutoBrightness(Landroid/content/ContentResolver;)V

    .line 733
    :goto_0
    return-void

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->stopAutoBrightness(Landroid/content/ContentResolver;)V

    goto :goto_0
.end method

.method private SetBrightness(Z)V
    .locals 4
    .param p1, "needSave"    # Z

    .prologue
    .line 705
    if-eqz p1, :cond_0

    .line 706
    iget-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-eqz v1, :cond_1

    .line 708
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    mul-int/lit8 v1, v1, 0x14

    add-int/lit8 v0, v1, 0x0

    .line 711
    .local v0, "brightness":I
    const-string v1, "BrightnessControlUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[c] SetBrightness auto:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->saveAutoBrightness(Landroid/content/ContentResolver;I)V

    .line 718
    .end local v0    # "brightness":I
    :cond_0
    :goto_0
    return-void

    .line 714
    :cond_1
    const-string v1, "BrightnessControlUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[c] SetBrightness:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->saveBrightness(Landroid/content/ContentResolver;I)V

    goto :goto_0
.end method

.method private SetBrightnessControlGridUIParams()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 546
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_0

    .line 547
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const v4, 0x1000028

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 554
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "brightnessControlGridUI"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 555
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x800053

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 557
    :cond_0
    return-void
.end method

.method private UpdateUI()V
    .locals 6

    .prologue
    const v5, 0x7f0d0019

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 560
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    if-nez v0, :cond_0

    .line 561
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-eqz v0, :cond_1

    .line 562
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 569
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->isSupportLightSensor(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 570
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-eqz v0, :cond_2

    .line 572
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 573
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 574
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 575
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 576
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mTitle:Landroid/widget/TextView;

    .line 577
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 579
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 581
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBarTopMarginLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 585
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightnessButton:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 586
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessLevel()V

    .line 606
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 608
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v0, :cond_3

    .line 609
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 626
    :goto_2
    return-void

    .line 564
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 590
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 591
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 592
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 593
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 594
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mTitle:Landroid/widget/TextView;

    .line 595
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 597
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLevel:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 599
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBarTopMarginLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 603
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightnessButton:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 610
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mDualFolderType:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsFolderClose:Z

    if-eqz v0, :cond_4

    .line 611
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 612
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightnessButton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 613
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mNoSupportLightSensor:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_2

    .line 615
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    .line 618
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 620
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 621
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 622
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 623
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mNoSupportLightSensor:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessMode()V

    return-void
.end method

.method static synthetic access$116(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;F)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    int-to-float v0, v0

    add-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    return v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F

    return v0
.end method

.method static synthetic access$1208(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)F
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v1, v0

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F

    return v0
.end method

.method static synthetic access$124(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;F)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessLevel()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->UpdateUI()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetBrightness(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->setButtonsDisable()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->setButtonsEnable()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    return p1
.end method

.method public static isSupportLightSensor(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 983
    const/4 v1, 0x0

    .line 985
    .local v1, "isSupport":Z
    const-string v5, "sensor"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    .line 987
    .local v3, "sensorMgr":Landroid/hardware/SensorManager;
    const/4 v5, -0x1

    invoke-virtual {v3, v5}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v2

    .line 988
    .local v2, "sensorList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 989
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/Sensor;

    invoke-virtual {v5}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    .line 991
    .local v4, "sensorType":I
    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    .line 992
    const/4 v1, 0x1

    .line 988
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 996
    .end local v4    # "sensorType":I
    :cond_1
    return v1
.end method

.method private setButtonsDisable()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 637
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 638
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 639
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 640
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 641
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightnessButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 642
    return-void
.end method

.method private setButtonsEnable()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 629
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 630
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 631
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 632
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 633
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightnessButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 634
    return-void
.end method


# virtual methods
.method public RemoveView()V
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 537
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StopAutoExit(Landroid/os/Handler;)V

    .line 538
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 540
    return-void
.end method

.method public ScreenRotateStatusChanged()V
    .locals 0

    .prologue
    .line 971
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->RemoveView()V

    .line 972
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->ShowView()V

    .line 973
    return-void
.end method

.method public ShowView()V
    .locals 4

    .prologue
    .line 520
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->Init()V

    .line 521
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 522
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetBrightnessControlGridUIParams()V

    .line 523
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIView:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 525
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    const v3, 0x7f040003

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 526
    .local v0, "alphaAni":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessControlGridUIViewMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 528
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 529
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 530
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v0, 0xa

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 298
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 299
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 300
    :goto_1
    return-void

    .line 176
    :sswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessDown:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->isPressed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 177
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    if-eqz v3, :cond_1

    .line 178
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F

    .line 179
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    goto :goto_0

    .line 184
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v3, :cond_2

    .line 185
    sget-boolean v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-nez v3, :cond_2

    .line 186
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    .line 187
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 188
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessMode()V

    .line 192
    :cond_2
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v3, :cond_4

    .line 193
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 203
    :goto_2
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v3, :cond_6

    .line 205
    .local v0, "maxBrightness":I
    :goto_3
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-le v3, v0, :cond_3

    .line 206
    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 210
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->UpdateUI()V

    .line 212
    invoke-direct {p0, v6}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetBrightness(Z)V

    goto :goto_0

    .line 195
    .end local v0    # "maxBrightness":I
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->GetMaxBrightnessVal()I

    move-result v1

    .line 196
    .local v1, "maxBrightnessVal":I
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-eq v1, v3, :cond_5

    .line 197
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    sub-int v4, v1, v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    goto :goto_2

    .line 199
    :cond_5
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    add-int/lit8 v3, v3, 0x19

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    goto :goto_2

    .line 203
    .end local v1    # "maxBrightnessVal":I
    :cond_6
    const/16 v0, 0xff

    goto :goto_3

    .line 219
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBtnBrightnessUp:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->isPressed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 220
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    if-eqz v3, :cond_7

    .line 221
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F

    .line 222
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    goto :goto_0

    .line 226
    :cond_7
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v3, :cond_8

    .line 227
    sget-boolean v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-nez v3, :cond_8

    .line 228
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    .line 229
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 230
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessMode()V

    .line 234
    :cond_8
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v3, :cond_a

    .line 235
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 249
    :goto_4
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v3, :cond_d

    .line 251
    .local v2, "minBrightness":I
    :goto_5
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-ge v3, v2, :cond_9

    .line 252
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 256
    :cond_9
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->UpdateUI()V

    .line 258
    invoke-direct {p0, v6}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetBrightness(Z)V

    goto/16 :goto_0

    .line 237
    .end local v2    # "minBrightness":I
    :cond_a
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    const/16 v4, 0x1e

    if-gt v3, v4, :cond_b

    .line 238
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    goto :goto_4

    .line 240
    :cond_b
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->GetMaxBrightnessVal()I

    move-result v1

    .line 241
    .restart local v1    # "maxBrightnessVal":I
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    if-eq v1, v3, :cond_c

    .line 242
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    add-int/lit8 v5, v1, -0x19

    sub-int/2addr v4, v5

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    goto :goto_4

    .line 244
    :cond_c
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    add-int/lit8 v3, v3, -0x19

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    goto :goto_4

    .line 249
    .end local v1    # "maxBrightnessVal":I
    :cond_d
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    goto :goto_5

    .line 264
    :sswitch_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->ClickOutSideLayout()V

    goto/16 :goto_1

    .line 268
    :sswitch_3
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    .line 270
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z

    if-eqz v3, :cond_e

    sget-boolean v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-eqz v3, :cond_e

    .line 271
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->getScreenAutoBrightness(Landroid/content/ContentResolver;)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    div-int/lit8 v3, v3, 0x14

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 273
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v0}, Landroid/widget/SeekBar;->setMax(I)V

    .line 274
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 282
    :goto_6
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->UpdateUI()V

    .line 283
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessMode()V

    goto/16 :goto_0

    .line 277
    :cond_e
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;->getScreenBrightness(Landroid/content/ContentResolver;)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    .line 278
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    rsub-int v4, v4, 0xff

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 279
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_6

    .line 288
    :sswitch_4
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 292
    :sswitch_5
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mService:Landroid/app/Service;

    const v4, 0x7f0a000a

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 173
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d0009 -> :sswitch_2
        0x7f0d000d -> :sswitch_4
        0x7f0d000f -> :sswitch_3
        0x7f0d0010 -> :sswitch_5
        0x7f0d001a -> :sswitch_1
        0x7f0d001b -> :sswitch_0
    .end sparse-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 920
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 961
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 962
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 963
    return v1

    .line 922
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 939
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    .line 952
    :pswitch_3
    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z

    .line 953
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mDualFolderType:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsFolderClose:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 920
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 922
    :pswitch_data_1
    .packed-switch 0x7f0d001a
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 939
    :pswitch_data_2
    .packed-switch 0x7f0d001a
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
