.class public final Lcom/samsung/android/app/assistantmenu/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Rotate:I = 0x7f0a0000

.field public static final accessibility_fmpad_category:I = 0x7f0a0001

.field public static final accessibility_fmpointer_category:I = 0x7f0a0002

.field public static final accessibility_magnifier_category:I = 0x7f0a0003

.field public static final app_name:I = 0x7f0a0004

.field public static final assistant_menu_title:I = 0x7f0a0005

.field public static final assistantmenu_main_startservice:I = 0x7f0a0006

.field public static final assistantmenu_main_stopservice:I = 0x7f0a0007

.field public static final auto_brightness:I = 0x7f0a0008

.field public static final auto_detail_title:I = 0x7f0a0009

.field public static final automatic_brightness_message:I = 0x7f0a000a

.field public static final back:I = 0x7f0a000b

.field public static final brightness:I = 0x7f0a000c

.field public static final brightnessLevel:I = 0x7f0a000d

.field public static final brightness_minus_description:I = 0x7f0a000e

.field public static final brightness_plus_description:I = 0x7f0a000f

.field public static final can_not_assistantmenu:I = 0x7f0a0010

.field public static final can_not_assistantmenu_and_AccessControl:I = 0x7f0a0011

.field public static final can_not_assistantmenu_and_Talkback:I = 0x7f0a0012

.field public static final can_not_magnify_hoverZoom:I = 0x7f0a0013

.field public static final cancel:I = 0x7f0a0014

.field public static final cancel_description:I = 0x7f0a0015

.field public static final center_rotation_description:I = 0x7f0a0016

.field public static final check_rotate_or_not:I = 0x7f0a0017

.field public static final close_scroll_description:I = 0x7f0a0018

.field public static final close_zoom_description:I = 0x7f0a0019

.field public static final deviceOption:I = 0x7f0a001a

.field public static final disable_hoverzoom:I = 0x7f0a001b

.field public static final disable_interactioncontrol:I = 0x7f0a001c

.field public static final disable_talkBack:I = 0x7f0a001d

.field public static final disable_talkBack_and_interactioncontrol:I = 0x7f0a001e

.field public static final dominant_hand:I = 0x7f0a001f

.field public static final dont_show_dialog:I = 0x7f0a0020

.field public static final eamenableinfo:I = 0x7f0a0021

.field public static final enhanced_assistant_summary:I = 0x7f0a0022

.field public static final enhanced_assistant_title:I = 0x7f0a0023

.field public static final enhanced_assistant_unable_use:I = 0x7f0a0024

.field public static final fetch_progress:I = 0x7f0a0025

.field public static final fingermouse:I = 0x7f0a0026

.field public static final fingermouse_view:I = 0x7f0a0027

.field public static final floating_icon_description:I = 0x7f0a0028

.field public static final fmpad_size:I = 0x7f0a0029

.field public static final fmpad_size_big:I = 0x7f0a002a

.field public static final fmpad_size_med:I = 0x7f0a002b

.field public static final fmpad_size_small:I = 0x7f0a002c

.field public static final fmpointer_size:I = 0x7f0a002d

.field public static final fmpointer_size_big:I = 0x7f0a002e

.field public static final fmpointer_size_small:I = 0x7f0a002f

.field public static final fmpointer_speed:I = 0x7f0a0030

.field public static final fmpointer_speed_1x:I = 0x7f0a0031

.field public static final fmpointer_speed_2x:I = 0x7f0a0032

.field public static final fmpointer_speed_3x:I = 0x7f0a0033

.field public static final fmpointer_speed_fast:I = 0x7f0a0034

.field public static final fmpointer_speed_slow:I = 0x7f0a0035

.field public static final forward_slash:I = 0x7f0a0036

.field public static final home:I = 0x7f0a0037

.field public static final hoverZoom:I = 0x7f0a0038

.field public static final left_handed:I = 0x7f0a0039

.field public static final left_rotation_description:I = 0x7f0a003a

.field public static final lock:I = 0x7f0a003b

.field public static final magnifier_seekbar_minus_tts:I = 0x7f0a003c

.field public static final magnifier_seekbar_plus_tts:I = 0x7f0a003d

.field public static final magnifier_size:I = 0x7f0a003e

.field public static final menu:I = 0x7f0a003f

.field public static final menu_edit:I = 0x7f0a0040

.field public static final menu_edit_summary:I = 0x7f0a0041

.field public static final more_options:I = 0x7f0a0042

.field public static final mute:I = 0x7f0a0043

.field public static final next_page_description:I = 0x7f0a0044

.field public static final no:I = 0x7f0a0045

.field public static final none:I = 0x7f0a0046

.field public static final notificationPanel:I = 0x7f0a0047

.field public static final ok:I = 0x7f0a0048

.field public static final ok_description:I = 0x7f0a0049

.field public static final powerOff:I = 0x7f0a004a

.field public static final powerOffMsg:I = 0x7f0a004b

.field public static final preference_dominant_hand_default:I = 0x7f0a004c

.field public static final preference_menu_type_default:I = 0x7f0a004d

.field public static final previous_page_description:I = 0x7f0a004e

.field public static final quickPanel:I = 0x7f0a004f

.field public static final quickSettings:I = 0x7f0a0050

.field public static final recentappList:I = 0x7f0a0051

.field public static final reset:I = 0x7f0a0052

.field public static final reset_menu_message:I = 0x7f0a0053

.field public static final restart:I = 0x7f0a0054

.field public static final restartMsg:I = 0x7f0a0055

.field public static final right_handed:I = 0x7f0a0056

.field public static final right_rotation_description:I = 0x7f0a0057

.field public static final rotate_screen:I = 0x7f0a0058

.field public static final save:I = 0x7f0a0059

.field public static final screenCapture:I = 0x7f0a005a

.field public static final screenRotate:I = 0x7f0a005b

.field public static final select_all:I = 0x7f0a005c

.field public static final setting:I = 0x7f0a005d

.field public static final single_tap_mode_description:I = 0x7f0a005e

.field public static final single_tap_mode_title:I = 0x7f0a005f

.field public static final sound:I = 0x7f0a0060

.field public static final sound_off_toast:I = 0x7f0a0061

.field public static final stms_version:I = 0x7f0a0062

.field public static final title_activity_assistantmenu_main:I = 0x7f0a0063

.field public static final unable_assistantmenu_while_sidesync:I = 0x7f0a0064

.field public static final vibrate:I = 0x7f0a0065

.field public static final volume:I = 0x7f0a0066

.field public static final volume_down_description:I = 0x7f0a0067

.field public static final volume_mode_interruption_message:I = 0x7f0a0068

.field public static final volume_mute_description:I = 0x7f0a0069

.field public static final volume_sound_description:I = 0x7f0a006a

.field public static final volume_up_description:I = 0x7f0a006b

.field public static final volume_vibrate_description:I = 0x7f0a006c

.field public static final yes:I = 0x7f0a006d

.field public static final zoom:I = 0x7f0a006e

.field public static final zoom_decrease_description:I = 0x7f0a006f

.field public static final zoom_increase_description:I = 0x7f0a0070

.field public static final zoom_percentage:I = 0x7f0a0071

.field public static final zoom_point:I = 0x7f0a0072

.field public static final zoom_seekbar:I = 0x7f0a0073


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
