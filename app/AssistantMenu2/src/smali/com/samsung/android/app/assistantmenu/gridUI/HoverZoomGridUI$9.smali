.class Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;
.super Ljava/lang/Object;
.source "HoverZoomGridUI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectAccessibilityMotionEvent(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

.field final synthetic val$eventAction:I

.field final synthetic val$isDouble:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;IZ)V
    .locals 0

    .prologue
    .line 805
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iput p2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$eventAction:I

    iput-boolean p3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$isDouble:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 44

    .prologue
    .line 809
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 810
    .local v2, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 811
    .local v4, "eventTime":J
    sget v14, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->touchDeviceID:I

    .line 812
    .local v14, "deviceId":I
    const/4 v15, 0x0

    .line 813
    .local v15, "edgeFlag":I
    const/4 v10, 0x0

    .line 814
    .local v10, "metaState":I
    const/high16 v17, -0x80000000

    .line 815
    .local v17, "AMOTION_EVENT_FLAG_WINDOW_IS_ACCESSIBILITY":I
    const/high16 v12, 0x3f800000    # 1.0f

    .line 816
    .local v12, "xPrecision":F
    const/high16 v13, 0x3f800000    # 1.0f

    .line 817
    .local v13, "yPrecision":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v42

    .line 818
    .local v42, "x":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v43

    .line 819
    .local v43, "y":F
    new-instance v38, Landroid/app/Instrumentation;

    invoke-direct/range {v38 .. v38}, Landroid/app/Instrumentation;-><init>()V

    .line 821
    .local v38, "inst":Landroid/app/Instrumentation;
    const/4 v6, 0x2

    new-array v8, v6, [Landroid/view/MotionEvent$PointerProperties;

    .line 822
    .local v8, "properties":[Landroid/view/MotionEvent$PointerProperties;
    new-instance v41, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct/range {v41 .. v41}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    .line 823
    .local v41, "pp1":Landroid/view/MotionEvent$PointerProperties;
    const/4 v6, 0x0

    move-object/from16 v0, v41

    iput v6, v0, Landroid/view/MotionEvent$PointerProperties;->id:I

    .line 824
    const/4 v6, 0x1

    move-object/from16 v0, v41

    iput v6, v0, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 826
    const/4 v6, 0x0

    aput-object v41, v8, v6

    .line 828
    const/4 v6, 0x1

    new-array v9, v6, [Landroid/view/MotionEvent$PointerCoords;

    .line 829
    .local v9, "pointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    new-instance v23, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v23 .. v23}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 830
    .local v23, "pc1":Landroid/view/MotionEvent$PointerCoords;
    const/16 v40, 0x0

    .line 831
    .local v40, "pc2":Landroid/view/MotionEvent$PointerCoords;
    move/from16 v0, v42

    move-object/from16 v1, v23

    iput v0, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 832
    move/from16 v0, v43

    move-object/from16 v1, v23

    iput v0, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 833
    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, v23

    iput v6, v0, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 834
    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, v23

    iput v6, v0, Landroid/view/MotionEvent$PointerCoords;->size:F

    .line 835
    const/4 v6, 0x0

    aput-object v23, v9, v6

    .line 837
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$eventAction:I

    if-lez v6, :cond_0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$eventAction:I

    const/4 v7, 0x4

    if-gt v6, v7, :cond_0

    .line 839
    const-string v6, "HoverZoomGridUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "injectAccessibilityMotionEvent SWIPERIGHT1 :: x = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", y = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    new-instance v40, Landroid/view/MotionEvent$PointerCoords;

    .end local v40    # "pc2":Landroid/view/MotionEvent$PointerCoords;
    invoke-direct/range {v40 .. v40}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 844
    .restart local v40    # "pc2":Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$eventAction:I

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getPointerCoordinates(Landroid/view/MotionEvent$PointerCoords;I)Landroid/view/MotionEvent$PointerCoords;

    move-result-object v40

    .line 845
    const/4 v6, 0x0

    aput-object v40, v9, v6

    .line 848
    :cond_0
    const-string v6, "HoverZoomGridUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "injectAccessibilityMotionEvent ACTION_DOWN :: x = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", y = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v11, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v39

    .line 858
    .local v39, "mMotionEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {v38 .. v39}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 860
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$eventAction:I

    if-lez v6, :cond_1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$eventAction:I

    const/4 v7, 0x4

    if-gt v6, v7, :cond_1

    .line 862
    const-string v6, "HoverZoomGridUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "injectAccessibilityMotionEvent SWIPERIGHT2 :: x = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", y = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$eventAction:I

    move/from16 v30, v0

    move-wide/from16 v20, v2

    move-object/from16 v22, v8

    move/from16 v24, v10

    move/from16 v25, v12

    move/from16 v26, v13

    move/from16 v27, v14

    move/from16 v28, v15

    move/from16 v29, v17

    invoke-virtual/range {v19 .. v30}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectMotionEvent(J[Landroid/view/MotionEvent$PointerProperties;Landroid/view/MotionEvent$PointerCoords;IFFIIII)V

    .line 872
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 874
    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v11, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v39

    .line 879
    invoke-virtual/range {v38 .. v39}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 881
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$isDouble:Z

    if-eqz v6, :cond_2

    .line 882
    const-string v6, "HoverZoomGridUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "injectAccessibilityMotionEvent isDouble : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->val$isDouble:Z

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-object/from16 v25, v0

    move-wide/from16 v26, v2

    move-wide/from16 v28, v4

    move-object/from16 v30, v8

    move-object/from16 v31, v9

    move/from16 v32, v10

    move/from16 v33, v12

    move/from16 v34, v13

    move/from16 v35, v14

    move/from16 v36, v15

    move/from16 v37, v17

    invoke-virtual/range {v25 .. v37}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->InjectDoubleTapEvent(JJ[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IFFIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 893
    .end local v2    # "downTime":J
    .end local v4    # "eventTime":J
    .end local v8    # "properties":[Landroid/view/MotionEvent$PointerProperties;
    .end local v9    # "pointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    .end local v10    # "metaState":I
    .end local v12    # "xPrecision":F
    .end local v13    # "yPrecision":F
    .end local v14    # "deviceId":I
    .end local v15    # "edgeFlag":I
    .end local v17    # "AMOTION_EVENT_FLAG_WINDOW_IS_ACCESSIBILITY":I
    .end local v23    # "pc1":Landroid/view/MotionEvent$PointerCoords;
    .end local v38    # "inst":Landroid/app/Instrumentation;
    .end local v39    # "mMotionEvent":Landroid/view/MotionEvent;
    .end local v40    # "pc2":Landroid/view/MotionEvent$PointerCoords;
    .end local v41    # "pp1":Landroid/view/MotionEvent$PointerProperties;
    .end local v42    # "x":F
    .end local v43    # "y":F
    :cond_2
    :goto_0
    return-void

    .line 890
    :catch_0
    move-exception v18

    .line 891
    .local v18, "e":Ljava/lang/Exception;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
