.class Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;
.super Landroid/view/View;
.source "CustomSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mBarWidth:F

.field private mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private mIsTouchEvent:Z

.field private mMax:I

.field private mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

.field private mPaint:Landroid/graphics/Paint;

.field private mPos:I

.field private mPrePosX:F

.field private mProgressBitmap:Landroid/graphics/Bitmap;

.field private mProgressEnd_X:F

.field private mProgressEnd_Y:F

.field private mProgressHeight:I

.field private mProgressNinePatchDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field private mProgressStart_X:F

.field private mProgressStart_Y:F

.field private mScrubberBitmap:Landroid/graphics/Bitmap;

.field private mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

.field private mScrubberPressedBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 18
    const-string v0, "CustomSeekBar"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->TAG:Ljava/lang/String;

    .line 22
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPaint:Landroid/graphics/Paint;

    .line 26
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    .line 28
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    .line 30
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    .line 32
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    .line 34
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressBitmap:Landroid/graphics/Bitmap;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressNinePatchDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mEnabled:Z

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mIsTouchEvent:Z

    .line 66
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mContext:Landroid/content/Context;

    .line 67
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->init()V

    .line 68
    return-void
.end method


# virtual methods
.method public Clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 265
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 267
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 272
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 276
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 277
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 281
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 282
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressBitmap:Landroid/graphics/Bitmap;

    .line 285
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressNinePatchDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v0, :cond_4

    .line 286
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressNinePatchDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 288
    :cond_4
    return-void
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    return v0
.end method

.method init()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 71
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 73
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    .line 74
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    .line 75
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPrePosX:F

    .line 76
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mIsTouchEvent:Z

    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 79
    .local v1, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0200c3

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    .line 81
    const v0, 0x7f0200c5

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    .line 83
    const v0, 0x7f0200c4

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    .line 85
    const v0, 0x7f0200c2

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressBitmap:Landroid/graphics/Bitmap;

    .line 87
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressNinePatchDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    .line 89
    const v0, 0x7f09001e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressHeight:I

    .line 90
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 105
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->Clear()V

    .line 107
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 111
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 112
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    div-int/lit8 v1, v1, 0x2

    if-lt v0, v1, :cond_0

    .line 113
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_X:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mBarWidth:F

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_X:F

    .line 118
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_X:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_X:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressNinePatchDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_X:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_Y:F

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_X:F

    float-to-int v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_Y:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressHeight:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 126
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressNinePatchDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 128
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mIsTouchEvent:Z

    if-nez v0, :cond_3

    .line 129
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mEnabled:Z

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_X:F

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v6

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_Y:F

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressHeight:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 145
    :goto_2
    return-void

    .line 115
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_X:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mBarWidth:F

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_X:F

    goto :goto_0

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressNinePatchDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_X:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_Y:F

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_X:F

    float-to-int v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_Y:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressHeight:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    goto :goto_1

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_X:F

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v6

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_Y:F

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberDisabledBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressHeight:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 140
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_X:F

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v6

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_Y:F

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mScrubberPressedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressHeight:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 95
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mBarWidth:F

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mBarWidth:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_X:F

    .line 99
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressHeight:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_Y:F

    .line 100
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressStart_Y:F

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mProgressEnd_Y:F

    .line 101
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 157
    iget-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mEnabled:Z

    if-nez v6, :cond_0

    .line 241
    :goto_0
    return v4

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getPaddingLeft()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getPaddingRight()I

    move-result v7

    sub-int v3, v6, v7

    .line 163
    .local v3, "progWidth":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 165
    :pswitch_0
    const-string v4, "CustomSeekBar"

    const-string v6, "onTouchEvent(): ACTION_DOWN"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getPaddingLeft()I

    move-result v4

    int-to-float v4, v4

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    int-to-float v6, v6

    int-to-float v7, v3

    iget v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    mul-float/2addr v6, v7

    add-float v2, v4, v6

    .line 167
    .local v2, "posX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float v1, v4, v2

    .line 169
    .local v1, "gapX":F
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    int-to-float v4, v4

    int-to-float v6, v3

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    div-float v6, v1, v6

    add-float/2addr v4, v6

    float-to-int v4, v4

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    .line 170
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getPaddingLeft()I

    move-result v4

    int-to-float v4, v4

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    int-to-float v6, v6

    int-to-float v7, v3

    iget v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPrePosX:F

    .line 171
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    if-eqz v4, :cond_1

    .line 172
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    invoke-interface {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;->CustomSeekBarProgressChanged()V

    .line 174
    :cond_1
    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mIsTouchEvent:Z

    .line 175
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->invalidate()V

    .end local v1    # "gapX":F
    .end local v2    # "posX":F
    :goto_1
    move v4, v5

    .line 241
    goto :goto_0

    .line 180
    :pswitch_1
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPrePosX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    sub-float v1, v6, v7

    .line 181
    .restart local v1    # "gapX":F
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPrePosX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 183
    .local v0, "absGapX":F
    int-to-float v6, v3

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    cmpl-float v6, v0, v6

    if-lez v6, :cond_8

    .line 184
    cmpl-float v6, v1, v8

    if-ltz v6, :cond_5

    .line 185
    int-to-float v6, v3

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    mul-float/2addr v6, v9

    cmpl-float v6, v0, v6

    if-lez v6, :cond_4

    .line 186
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    int-to-float v6, v6

    int-to-float v7, v3

    iget v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    div-float v7, v0, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    .line 200
    :goto_2
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    if-gtz v6, :cond_7

    .line 201
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    .line 205
    :cond_2
    :goto_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPrePosX:F

    .line 206
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    if-eqz v4, :cond_3

    .line 207
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    invoke-interface {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;->CustomSeekBarProgressChanged()V

    .line 209
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->invalidate()V

    goto :goto_1

    .line 188
    :cond_4
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    goto :goto_2

    .line 192
    :cond_5
    int-to-float v6, v3

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    mul-float/2addr v6, v9

    cmpl-float v6, v0, v6

    if-lez v6, :cond_6

    .line 193
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    int-to-float v6, v6

    int-to-float v7, v3

    iget v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    div-float v7, v0, v7

    add-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    goto :goto_2

    .line 195
    :cond_6
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    goto :goto_2

    .line 202
    :cond_7
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    if-lt v4, v6, :cond_2

    .line 203
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    goto :goto_3

    .line 211
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->getPaddingLeft()I

    move-result v4

    int-to-float v4, v4

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    int-to-float v6, v6

    int-to-float v7, v3

    iget v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPrePosX:F

    goto/16 :goto_1

    .line 217
    .end local v0    # "absGapX":F
    .end local v1    # "gapX":F
    :pswitch_2
    const-string v6, "CustomSeekBar"

    const-string v7, "onTouchEvent(): ACTION_UP"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iput v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPrePosX:F

    .line 219
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    if-eqz v6, :cond_9

    .line 220
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    invoke-interface {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;->CustomeSeekBarStopTrackingTouch()V

    .line 222
    :cond_9
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mIsTouchEvent:Z

    .line 223
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->invalidate()V

    goto/16 :goto_1

    .line 228
    :pswitch_3
    const-string v6, "CustomSeekBar"

    const-string v7, "onTouchEvent(): ACTION_CANCEL"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iput v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPrePosX:F

    .line 230
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    if-eqz v6, :cond_a

    .line 231
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    invoke-interface {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;->CustomeSeekBarStopTrackingTouch()V

    .line 233
    :cond_a
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mIsTouchEvent:Z

    .line 234
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->invalidate()V

    goto/16 :goto_1

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 150
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 151
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mEnabled:Z

    .line 152
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->invalidate()V

    .line 153
    return-void
.end method

.method public setMax(I)V
    .locals 0
    .param p1, "max"    # I

    .prologue
    .line 245
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    .line 246
    return-void
.end method

.method public setOnSeekBarChangeListener(Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;)V
    .locals 0
    .param p1, "onSeekBarChangeListener"    # Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mOnSeekBarChangeListener:Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;

    .line 262
    return-void
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 249
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mMax:I

    if-gt p1, v0, :cond_0

    .line 250
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->mPos:I

    .line 251
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar;->invalidate()V

    .line 254
    :cond_0
    return-void
.end method
