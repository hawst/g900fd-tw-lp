.class public Lcom/samsung/android/app/assistantmenu/AssistantMenuReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AssistantMenuReceiver.java"


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 31
    const-string v0, "AssistantMenuReceiver"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method private getServiceTaskName(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 187
    .local v1, "checked":Z
    const-string v5, "activity"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 190
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v5, 0x100

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    .line 191
    .local v2, "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v2, :cond_1

    .line 192
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 193
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 195
    .local v4, "runningTaskInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    const-string v5, "AssistantMenuReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Service name :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 199
    const/4 v1, 0x1

    .line 200
    const-string v5, "AssistantMenuReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] Service is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    .end local v4    # "runningTaskInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_1
    return v1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 44
    .local v3, "action":Ljava/lang/String;
    const-string v19, "AssistantMenuReceiver"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[c] AssistantMenuReceiver:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v20, "assistant_menu"

    const/16 v21, 0x0

    invoke-static/range {v19 .. v21}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v18

    .line 49
    .local v18, "state":I
    sget-object v19, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_BOOT_COMPLETED:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 51
    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 53
    new-instance v19, Landroid/content/Intent;

    const-class v20, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v7

    .line 56
    .local v7, "cn":Landroid/content/ComponentName;
    const-string v19, "AssistantMenuReceiver"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[c] ACTION_BOOT_COMPLETED:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    .end local v7    # "cn":Landroid/content/ComponentName;
    :cond_0
    new-instance v11, Landroid/content/Intent;

    const-class v19, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v11, "intentparser":Landroid/content/Intent;
    const-string v19, "trigger"

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 61
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 177
    .end local v11    # "intentparser":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 63
    :cond_2
    sget-object v19, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SHARING_COMPLETE:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 65
    const/4 v9, 0x1

    .line 66
    .local v9, "dominant_hand_type":I
    const/4 v6, 0x2

    .line 67
    .local v6, "assistant_menu_pointer_speed":I
    const/4 v5, 0x0

    .line 68
    .local v5, "assistant_menu_pointer_size":I
    const/4 v4, 0x1

    .line 70
    .local v4, "assistant_menu_pad_size":I
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v20, "assistant_menu_dominant_hand_type"

    const/16 v21, 0x1

    invoke-static/range {v19 .. v21}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    .line 74
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v20, "assistant_menu_pointer_speed"

    const/16 v21, 0x2

    invoke-static/range {v19 .. v21}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 78
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v20, "assistant_menu_pointer_size"

    const/16 v21, 0x0

    invoke-static/range {v19 .. v21}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v20, "assistant_menu_pad_size"

    const/16 v21, 0x1

    invoke-static/range {v19 .. v21}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 86
    const-string v19, "AssistantMenuReceiver"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[c] ReadSharingPref dominant_hand_type: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " pointer_speed : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " pointer_size : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " pad_size"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const-string v19, "dominant_hand_type"

    const/16 v20, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v10

    .line 95
    .local v10, "handMode":I
    const-string v19, "pointer_speed"

    const/16 v20, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v15

    .line 99
    .local v15, "pointer_speed":I
    const-string v19, "pointer_size"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v14

    .line 103
    .local v14, "pointer_size":I
    const-string v19, "pad_size"

    const/16 v20, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v12

    .line 107
    .local v12, "pad_size":I
    if-eq v9, v10, :cond_3

    .line 109
    sget-object v19, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v17

    .line 111
    .local v17, "servicePreference":Landroid/content/SharedPreferences;
    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    .line 112
    .local v16, "prefsEdit":Landroid/content/SharedPreferences$Editor;
    sget-object v19, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    const/16 v20, 0x1

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 113
    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 115
    const-string v19, "dominant_hand_type"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 119
    .end local v16    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    .end local v17    # "servicePreference":Landroid/content/SharedPreferences;
    :cond_3
    if-eq v6, v15, :cond_4

    .line 120
    const-string v19, "pointer_speed"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1, v6}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 124
    :cond_4
    if-eq v5, v14, :cond_5

    .line 125
    const-string v19, "pointer_size"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 129
    :cond_5
    if-eq v4, v12, :cond_6

    .line 130
    const-string v19, "pad_size"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 134
    :cond_6
    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 135
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuReceiver;->getServiceTaskName(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 136
    new-instance v19, Landroid/content/Intent;

    const-class v20, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 139
    :cond_7
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuReceiver;->getServiceTaskName(Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 140
    new-instance v19, Landroid/content/Intent;

    const-class v20, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 143
    .end local v4    # "assistant_menu_pad_size":I
    .end local v5    # "assistant_menu_pointer_size":I
    .end local v6    # "assistant_menu_pointer_speed":I
    .end local v9    # "dominant_hand_type":I
    .end local v10    # "handMode":I
    .end local v12    # "pad_size":I
    .end local v14    # "pointer_size":I
    .end local v15    # "pointer_speed":I
    :cond_8
    sget-object v19, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_ADDED:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    sget-object v19, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_INSTALLED:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 145
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    .line 146
    .local v8, "data":Landroid/net/Uri;
    invoke-virtual {v8}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v13

    .line 148
    .local v13, "pkgName":Ljava/lang/String;
    const-string v19, "Logging Service"

    move-object/from16 v0, v19

    invoke-static {v0, v13}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    new-instance v11, Landroid/content/Intent;

    const-class v19, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 151
    .restart local v11    # "intentparser":Landroid/content/Intent;
    const-string v19, "trigger"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 152
    const-string v19, "package"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 155
    .end local v8    # "data":Landroid/net/Uri;
    .end local v11    # "intentparser":Landroid/content/Intent;
    .end local v13    # "pkgName":Ljava/lang/String;
    :cond_a
    sget-object v19, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_UNINSTALLED:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 157
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    .line 158
    .restart local v8    # "data":Landroid/net/Uri;
    invoke-virtual {v8}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v13

    .line 160
    .restart local v13    # "pkgName":Ljava/lang/String;
    const-string v19, "Logging Service"

    move-object/from16 v0, v19

    invoke-static {v0, v13}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    new-instance v11, Landroid/content/Intent;

    const-class v19, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    .restart local v11    # "intentparser":Landroid/content/Intent;
    const-string v19, "trigger"

    const/16 v20, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 164
    const-string v19, "package"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 167
    .end local v8    # "data":Landroid/net/Uri;
    .end local v11    # "intentparser":Landroid/content/Intent;
    .end local v13    # "pkgName":Ljava/lang/String;
    :cond_b
    sget-object v19, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_REPLACED:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 169
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    .line 170
    .restart local v8    # "data":Landroid/net/Uri;
    invoke-virtual {v8}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v13

    .line 171
    .restart local v13    # "pkgName":Ljava/lang/String;
    new-instance v11, Landroid/content/Intent;

    const-class v19, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .restart local v11    # "intentparser":Landroid/content/Intent;
    const-string v19, "trigger"

    const/16 v20, 0x3

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 173
    const-string v19, "package"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
