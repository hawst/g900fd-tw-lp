.class Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;
.super Ljava/lang/Object;
.source "AssistantMenuEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->ShowDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 249
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$000(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 250
    const-string v1, "AssistantMenuEnabler"

    const-string v2, "switch is on"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackState:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$300(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 253
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->accesscontrolState:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$400(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 255
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 257
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "access_control_enabled"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 259
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "access_control_use"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 260
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 261
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.app.accesscontrol"

    const-string v2, "com.samsung.android.app.accesscontrol.AccessControlMainService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 268
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackOffAction:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$500(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 270
    .restart local v0    # "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 284
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->isSinglePopupNeeded()I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$600(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 285
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->showSingleTapModeDialog()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$700(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)V

    .line 301
    :goto_2
    return-void

    .line 266
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->turnOffTalkBack(Landroid/content/Context;)Z

    goto :goto_0

    .line 272
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "access_control_enabled"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 274
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "access_control_use"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 275
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 276
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.app.accesscontrol"

    const-string v2, "com.samsung.android.app.accesscontrol.AccessControlMainService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 279
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->turnOffTalkBack(Landroid/content/Context;)Z

    goto :goto_1

    .line 287
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "assistant_menu"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 289
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapMode:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$100(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)I

    move-result v1

    if-nez v1, :cond_4

    .line 290
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_interaction"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 293
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_2
.end method
