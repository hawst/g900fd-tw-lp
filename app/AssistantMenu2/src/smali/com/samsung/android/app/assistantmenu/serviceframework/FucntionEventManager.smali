.class public Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;
.super Ljava/lang/Object;
.source "FucntionEventManager.java"


# static fields
.field private static sScreenshotConnection:Landroid/content/ServiceConnection;

.field private static final sScreenshotHandler:Landroid/os/Handler;

.field private static final sScreenshotLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotHandler:Landroid/os/Handler;

    .line 117
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotLock:Ljava/lang/Object;

    .line 119
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static PowerOff(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 2
    .param p0, "context_"    # Landroid/content/Context;
    .param p1, "isConfirm"    # Ljava/lang/Boolean;

    .prologue
    .line 227
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 228
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.KEY_CONFIRM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 229
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 230
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 231
    return-void
.end method

.method public static Restart(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "context_"    # Landroid/content/Context;
    .param p1, "isConfirm"    # Ljava/lang/Boolean;

    .prologue
    .line 242
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REBOOT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 243
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.KEY_CONFIRM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 244
    const-string v1, "android.intent.extra.REBOOT_REASON"

    const-string v2, "Assistant Menu"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 246
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 247
    return-void
.end method

.method public static ScreenCapture(Landroid/content/Context;)V
    .locals 0
    .param p0, "context_"    # Landroid/content/Context;

    .prologue
    .line 215
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->takeScreenshot(Landroid/content/Context;)V

    .line 216
    return-void
.end method

.method public static ShowDeviceOptions(Landroid/content/Context;)V
    .locals 0
    .param p0, "context_"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->lockButtonLongTapEvent()V

    .line 64
    return-void
.end method

.method public static ShowQuickPanel(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 73
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "kids_home_mode"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 74
    .local v0, "KidsModestate":I
    if-ne v0, v6, :cond_1

    .line 75
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.kidsplat.quickpanel.PANEL_OPEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 76
    .local v2, "show_statusbar":Landroid/content/Intent;
    const-string v3, "open_from_menu"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 78
    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 89
    .end local v2    # "show_statusbar":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    const-string v3, "statusbar"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 82
    .local v1, "mStatusBar":Landroid/app/StatusBarManager;
    if-eqz v1, :cond_0

    .line 83
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.systemui.statusbar.TOGGLED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    .restart local v2    # "show_statusbar":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 86
    invoke-virtual {v1}, Landroid/app/StatusBarManager;->expandNotificationsPanel()V

    goto :goto_0
.end method

.method public static ShowRecentAppList()V
    .locals 0

    .prologue
    .line 50
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->recentButtonTapEvent()V

    .line 51
    return-void
.end method

.method public static ShowSettingsPanel(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 100
    .local v0, "mStatusBar":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->expandSettingsPanel()V

    .line 102
    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100()Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$102(Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;
    .locals 0
    .param p0, "x0"    # Landroid/content/ServiceConnection;

    .prologue
    .line 29
    sput-object p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;

    return-object p0
.end method

.method static synthetic access$200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static takeScreenshot(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    new-instance v3, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$1;-><init>(Landroid/content/Context;)V

    .line 141
    .local v3, "sScreenshotTimeout":Ljava/lang/Runnable;
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotLock:Ljava/lang/Object;

    monitor-enter v5

    .line 142
    :try_start_0
    sget-object v4, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;

    if-eqz v4, :cond_0

    .line 143
    monitor-exit v5

    .line 202
    :goto_0
    return-void

    .line 145
    :cond_0
    new-instance v0, Landroid/content/ComponentName;

    const-string v4, "com.android.systemui"

    const-string v6, "com.android.systemui.screenshot.TakeScreenshotService"

    invoke-direct {v0, v4, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 148
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 149
    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;

    invoke-direct {v1, p0, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 196
    .local v1, "conn":Landroid/content/ServiceConnection;
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v1, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 198
    sput-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;

    .line 199
    sget-object v4, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x2710

    invoke-virtual {v4, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 201
    :cond_1
    monitor-exit v5

    goto :goto_0

    .end local v0    # "cn":Landroid/content/ComponentName;
    .end local v1    # "conn":Landroid/content/ServiceConnection;
    .end local v2    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
