.class Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;
.super Landroid/os/AsyncTask;
.source "ZoomControlGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComeBackZoomPointerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final DIRECTION_EAST:I

.field private final DIRECTION_WEST:I

.field private mDirection:I

.field private mInclination:F

.field private mInterceptY:F

.field private mLastX:F

.field private mLastY:F

.field private mMaxAccelRate:I

.field private mMovedPointValue:I

.field private mSlowDistance:I

.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;FF)V
    .locals 6
    .param p2, "lastPositionX"    # F
    .param p3, "lastPositionY"    # F

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 871
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 851
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->DIRECTION_EAST:I

    .line 853
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->DIRECTION_WEST:I

    .line 872
    const-string v0, "ZoomControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ComeBackZoomPointerTask - ComeBackZoomPointerTask() :: lastPositionX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", lastPositionY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    sub-float v0, p2, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    .line 876
    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    sub-float v0, p3, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    .line 877
    const-string v0, "ZoomControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mLastX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mLastY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F
    invoke-static {p1, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 880
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F
    invoke-static {p1, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 882
    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsTouchable:Z
    invoke-static {p1, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$602(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z

    .line 883
    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 884
    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 885
    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 887
    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0200be

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 889
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 890
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mDirection:I

    .line 895
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v2

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mInclination:F

    .line 896
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v1

    mul-float/2addr v0, v1

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v2

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mInterceptY:F

    .line 899
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    .line 900
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMovedPointValue:I

    .line 901
    return-void

    .line 892
    :cond_0
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mDirection:I

    goto :goto_0
.end method

.method private DetectSlowSection()V
    .locals 4

    .prologue
    .line 973
    const-string v0, "ZoomControlGridUI"

    const-string v1, "ComeBackZoomPointerTask - DetectSlowSection()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    new-instance v1, Landroid/graphics/Point;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->GetDistance(Landroid/graphics/Point;Landroid/graphics/Point;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    .line 977
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0xc8

    if-le v0, v1, :cond_0

    .line 1002
    :goto_0
    return-void

    .line 980
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0xb4

    if-le v0, v1, :cond_1

    .line 981
    const/16 v0, 0x12

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 982
    :cond_1
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0xa0

    if-le v0, v1, :cond_2

    .line 983
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 984
    :cond_2
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0x8c

    if-le v0, v1, :cond_3

    .line 985
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 986
    :cond_3
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0x78

    if-le v0, v1, :cond_4

    .line 987
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 988
    :cond_4
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0x64

    if-le v0, v1, :cond_5

    .line 989
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 990
    :cond_5
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0x50

    if-le v0, v1, :cond_6

    .line 991
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 992
    :cond_6
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0x3c

    if-le v0, v1, :cond_7

    .line 993
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 994
    :cond_7
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0x28

    if-le v0, v1, :cond_8

    .line 995
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 996
    :cond_8
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mSlowDistance:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_9

    .line 997
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    goto :goto_0

    .line 999
    :cond_9
    const-string v0, "ZoomControlGridUI"

    const-string v1, "DetectSlowSection else"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 10
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 905
    const-string v3, "ZoomControlGridUI"

    const-string v4, "ComeBackZoomPointerTask - doInBackground()"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    const/4 v2, 0x0

    .line 908
    .local v2, "isWorkComplete":Z
    :cond_0
    :goto_0
    if-nez v2, :cond_9

    .line 909
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->DetectSlowSection()V

    .line 911
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mInclination:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_4

    .line 912
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mInclination:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v8

    if-gtz v3, :cond_3

    .line 913
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mDirection:I

    if-nez v3, :cond_2

    .line 914
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    sub-float/2addr v3, v8

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    .line 918
    :goto_1
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mInclination:F

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mInterceptY:F

    add-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    .line 927
    :goto_2
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMovedPointValue:I

    new-instance v4, Landroid/graphics/Point;

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    float-to-int v6, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/Point;-><init>(II)V

    new-instance v5, Landroid/graphics/Point;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1900(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F
    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v7

    float-to-int v7, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    invoke-static {v4, v5}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->GetDistance(Landroid/graphics/Point;Landroid/graphics/Point;)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMovedPointValue:I

    .line 930
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 931
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 933
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    float-to-int v4, v4

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 934
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    float-to-int v4, v4

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 936
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMovedPointValue:I

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMaxAccelRate:I

    if-le v3, v4, :cond_1

    .line 937
    iput v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mMovedPointValue:I

    .line 939
    new-array v3, v9, [Ljava/lang/Void;

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->publishProgress([Ljava/lang/Object;)V

    .line 942
    const-wide/16 v4, 0xa

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 949
    :cond_1
    :goto_3
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    if-ge v3, v4, :cond_5

    .line 950
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 951
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 916
    :cond_2
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    add-float/2addr v3, v8

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    goto/16 :goto_1

    .line 920
    :cond_3
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    sub-float/2addr v3, v8

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    .line 921
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mInterceptY:F

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mInclination:F

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    goto/16 :goto_2

    .line 924
    :cond_4
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    add-float/2addr v3, v8

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    goto/16 :goto_2

    .line 943
    :catch_0
    move-exception v0

    .line 945
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 954
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    const v1, 0x800053

    .line 955
    .local v1, "gravity":I
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    if-ne v3, v1, :cond_7

    .line 956
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    cmpg-float v3, v3, v4

    if-lez v3, :cond_6

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 958
    :cond_6
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 961
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-gez v3, :cond_8

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 963
    :cond_8
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 969
    .end local v1    # "gravity":I
    :cond_9
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    return-object v3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 850
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1014
    const-string v0, "ZoomControlGridUI"

    const-string v1, "ComeBackZoomPointerTask - onPostExecute()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1016
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F
    invoke-static {v0, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 1017
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F
    invoke-static {v0, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 1019
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastX:F

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2602(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 1020
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->mLastY:F

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2702(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 1022
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2600(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1023
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1024
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1025
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1027
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsTouchable:Z
    invoke-static {v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$602(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z

    .line 1028
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1029
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1030
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1032
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 850
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 850
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 1006
    const-string v0, "ZoomControlGridUI"

    const-string v1, "ComeBackZoomPointerTask - onProgressUpdate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1010
    :cond_0
    return-void
.end method
