.class Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;
.super Ljava/lang/Object;
.source "HoverZoomGridUI.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->SetEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 362
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 363
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 431
    :cond_0
    :goto_0
    return v8

    .line 367
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePad:Z
    invoke-static {v3, v8}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    .line 368
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePtr:Z
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    .line 370
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v4, v4

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitX:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2102(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 371
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitY:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2202(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 373
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/LinearLayout;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 374
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    .line 375
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v5

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getBackGroundImage(I)I
    invoke-static {v4, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 382
    :pswitch_1
    const/4 v1, 0x0

    .line 383
    .local v1, "newX":I
    const/4 v2, 0x0

    .line 385
    .local v2, "newY":I
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePad:Z
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 386
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v1, v3

    .line 387
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v2, v3

    .line 389
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitX:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v5

    sub-int v5, v1, v5

    # += operator for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I
    invoke-static {v3, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2712(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 390
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitY:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v5

    sub-int v5, v2, v5

    # += operator for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I
    invoke-static {v3, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2812(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 392
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitX:I
    invoke-static {v3, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2102(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 393
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitY:I
    invoke-static {v3, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2202(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 395
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 396
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v3

    if-gez v3, :cond_2

    move v3, v4

    :goto_1
    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I
    invoke-static {v5, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2702(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 399
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v5

    if-gez v5, :cond_4

    :goto_2
    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2802(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 403
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v4

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 404
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v4

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 405
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3200(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/RelativeLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 406
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isSecureKeyguardLocked()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 407
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    goto/16 :goto_0

    .line 396
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v3

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3000(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v6, v7

    if-le v3, v6, :cond_3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3000(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v3

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v3, v6

    goto/16 :goto_1

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v3

    goto/16 :goto_1

    .line 399
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_5

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v4, v5

    goto/16 :goto_2

    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v4

    goto/16 :goto_2

    .line 415
    .end local v1    # "newX":I
    .end local v2    # "newY":I
    :pswitch_2
    const-string v3, "HoverZoomGridUI"

    const-string v5, "ACTION_UP "

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitX:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2102(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 417
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitY:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2202(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 419
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePad:Z
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    .line 420
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePtr:Z
    invoke-static {v3, v8}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    .line 421
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    .line 422
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v5, v5, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 423
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 424
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 363
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
