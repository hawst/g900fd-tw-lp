.class public Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;
.super Ljava/lang/Object;
.source "DisplayUtil.java"


# static fields
.field private static TAG:Ljava/lang/String; = null

.field private static final THEME_TW_DARK:I = 0x0

.field private static final THEME_TW_LIGHT:I = 0x1

.field private static actionMenuTextColor:I

.field private static deviceThemeStyle:I

.field public static mDeviceType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 20
    const-string v0, "AssistantMenuDisplayUtil"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->TAG:Ljava/lang/String;

    .line 28
    sput v1, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->deviceThemeStyle:I

    .line 30
    sput v1, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->actionMenuTextColor:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetDistance(IIII)I
    .locals 4
    .param p0, "spx"    # I
    .param p1, "spy"    # I
    .param p2, "epx"    # I
    .param p3, "epy"    # I

    .prologue
    .line 90
    sub-int v1, p0, p2

    sub-int v2, p0, p2

    mul-int/2addr v1, v2

    sub-int v2, p1, p3

    sub-int v3, p1, p3

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    int-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 91
    .local v0, "moveDistance":I
    return v0
.end method

.method public static GetDistance(Landroid/graphics/Point;Landroid/graphics/Point;)I
    .locals 5
    .param p0, "sp"    # Landroid/graphics/Point;
    .param p1, "ep"    # Landroid/graphics/Point;

    .prologue
    .line 84
    iget v1, p0, Landroid/graphics/Point;->x:I

    iget v2, p1, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    iget v2, p0, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    mul-int/2addr v1, v2

    iget v2, p0, Landroid/graphics/Point;->y:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    sub-int/2addr v2, v3

    iget v3, p0, Landroid/graphics/Point;->y:I

    iget v4, p1, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    int-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 86
    .local v0, "moveDistance":I
    return v0
.end method

.method public static GetDistanceDegree(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;)F
    .locals 7
    .param p0, "basePoint"    # Landroid/graphics/Point;
    .param p1, "p1"    # Landroid/graphics/Point;
    .param p2, "p2"    # Landroid/graphics/Point;

    .prologue
    .line 68
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p0, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Point;->x:I

    .line 69
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p0, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Point;->y:I

    .line 70
    iget v0, p2, Landroid/graphics/Point;->x:I

    iget v1, p0, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->x:I

    .line 71
    iget v0, p2, Landroid/graphics/Point;->y:I

    iget v1, p0, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->y:I

    .line 72
    iget v0, p1, Landroid/graphics/Point;->y:I

    int-to-double v0, v0

    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->x:I

    mul-int/2addr v2, v3

    iget v3, p1, Landroid/graphics/Point;->y:I

    iget v4, p1, Landroid/graphics/Point;->y:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    iget v2, p2, Landroid/graphics/Point;->y:I

    int-to-double v2, v2

    iget v4, p2, Landroid/graphics/Point;->x:I

    iget v5, p2, Landroid/graphics/Point;->x:I

    mul-int/2addr v4, v5

    iget v5, p2, Landroid/graphics/Point;->y:I

    iget v6, p2, Landroid/graphics/Point;->y:I

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->asin(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static dpToPixel(Landroid/content/Context;F)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "DP"    # F

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static isActionbarLightTheme(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    sget v0, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->actionMenuTextColor:I

    if-gez v0, :cond_0

    .line 160
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->parseDeviceTheme(Landroid/content/Context;)V

    .line 163
    :cond_0
    sget v0, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->actionMenuTextColor:I

    const v1, 0x888888

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLightTheme(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 145
    sget v1, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->deviceThemeStyle:I

    if-gez v1, :cond_0

    .line 146
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->parseDeviceTheme(Landroid/content/Context;)V

    .line 149
    :cond_0
    sget v1, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->deviceThemeStyle:I

    if-ne v1, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidArea(Landroid/content/Context;IILandroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "minRadius"    # I
    .param p2, "maxRadius"    # I
    .param p3, "curPoint"    # Landroid/graphics/Point;
    .param p4, "basePoint"    # Landroid/graphics/Point;

    .prologue
    .line 106
    invoke-static {p3, p4}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->GetDistance(Landroid/graphics/Point;Landroid/graphics/Point;)I

    move-result v0

    int-to-float v1, p1

    invoke-static {p0, v1}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-static {p3, p4}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->GetDistance(Landroid/graphics/Point;Landroid/graphics/Point;)I

    move-result v0

    int-to-float v1, p2

    invoke-static {p0, v1}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v1

    if-le v0, v1, :cond_1

    .line 108
    :cond_0
    const/4 v0, 0x0

    .line 111
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static parseDeviceTheme(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const v13, 0xffffff

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 172
    const-string v1, "com.android.settings"

    .line 175
    .local v1, "appName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.android.settings"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 176
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    const-string v6, "com.android.settings"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v4

    .line 177
    .local v4, "settingsApp":Landroid/content/Context;
    new-instance v6, Landroid/view/ContextThemeWrapper;

    iget v7, v0, Landroid/content/pm/ApplicationInfo;->theme:I

    invoke-direct {v6, v4, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6}, Landroid/view/ContextThemeWrapper;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    .line 180
    .local v5, "settingsTheme":Landroid/content/res/Resources$Theme;
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 181
    .local v3, "outValue":Landroid/util/TypedValue;
    const v6, 0x10102ce

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 182
    iget v6, v3, Landroid/util/TypedValue;->resourceId:I

    sparse-switch v6, :sswitch_data_0

    .line 190
    const/4 v6, 0x0

    sput v6, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->deviceThemeStyle:I

    .line 192
    :goto_0
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->TAG:Ljava/lang/String;

    const-string v7, "AssistantMenuDisplayUtil : actionBarStyle = 0x%x, deviceThemeStyle = %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget v10, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->deviceThemeStyle:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const v6, 0x1010361

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 198
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget v7, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    and-int/2addr v6, v13

    sput v6, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->actionMenuTextColor:I

    .line 199
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->TAG:Ljava/lang/String;

    const-string v7, "AssistantMenuDisplayUtil : actionMenuTextColor = 0x%08x, %6x"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget v10, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->actionMenuTextColor:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "outValue":Landroid/util/TypedValue;
    .end local v4    # "settingsApp":Landroid/content/Context;
    .end local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :goto_1
    return-void

    .line 185
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v3    # "outValue":Landroid/util/TypedValue;
    .restart local v4    # "settingsApp":Landroid/content/Context;
    .restart local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :sswitch_0
    const/4 v6, 0x1

    sput v6, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->deviceThemeStyle:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 202
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "outValue":Landroid/util/TypedValue;
    .end local v4    # "settingsApp":Landroid/content/Context;
    .end local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :catch_0
    move-exception v2

    .line 203
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->TAG:Ljava/lang/String;

    const-string v7, "AssistantMenuDisplayUtil : %s not found"

    new-array v8, v12, [Ljava/lang/Object;

    const-string v9, "com.android.settings"

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    sput v11, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->deviceThemeStyle:I

    .line 206
    sput v13, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->actionMenuTextColor:I

    goto :goto_1

    .line 182
    :sswitch_data_0
    .sparse-switch
        0x10300e1 -> :sswitch_0
        0x10301a3 -> :sswitch_0
    .end sparse-switch
.end method

.method public static pixelToDp(Landroid/content/Context;F)F
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "px"    # F

    .prologue
    .line 56
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    div-float v0, p1, v0

    return v0
.end method

.method public static spToPixel(Landroid/content/Context;F)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sp"    # F

    .prologue
    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static toViewRawXY(Landroid/view/View;)Landroid/graphics/Point;
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 121
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    .line 122
    .local v1, "parentView":Landroid/view/View;
    const/4 v2, 0x0

    .line 123
    .local v2, "sumX":I
    const/4 v3, 0x0

    .line 125
    .local v3, "sumY":I
    const/4 v0, 0x0

    .line 126
    .local v0, "chk":Z
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 127
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v4

    add-int/2addr v2, v4

    .line 128
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v4

    add-int/2addr v3, v4

    .line 130
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    .end local p0    # "view":Landroid/view/View;
    check-cast p0, Landroid/view/View;

    .line 131
    .restart local p0    # "view":Landroid/view/View;
    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 132
    const/4 v0, 0x1

    goto :goto_0

    .line 135
    :cond_1
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    return-object v4
.end method
