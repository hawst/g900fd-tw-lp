.class Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;
.super Ljava/lang/Object;
.source "BrightnessControlGridUI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;)V
    .locals 0

    .prologue
    .line 760
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 763
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 765
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # operator++ for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1208(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)F

    .line 766
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)F

    move-result v3

    # += operator for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$116(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;F)I

    .line 768
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0xa

    .line 770
    .local v1, "maxBrightness":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I

    move-result v2

    if-le v2, v1, :cond_2

    .line 771
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v2, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$102(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;I)I

    .line 772
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 785
    .end local v1    # "maxBrightness":I
    :cond_0
    return-void

    .line 768
    :cond_1
    const/16 v1, 0xff

    goto :goto_1

    .line 776
    .restart local v1    # "maxBrightness":I
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 779
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 780
    :catch_0
    move-exception v0

    .line 782
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
