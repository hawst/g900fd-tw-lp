.class Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;
.super Landroid/database/ContentObserver;
.source "FingerMouseGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v4, 0x0

    .line 183
    const-string v1, "FingerMouseGridUIHoverZoom"

    const-string v2, "1,mHoverPadSizeObserver "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    const-string v1, "FingerMouseGridUIHoverZoom"

    const-string v2, "1-if,mHoverPadSizeObserver "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hover_zoom_magnifier_size"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$202(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 187
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hover_zoom_value"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$302(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 189
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v3

    aget v2, v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    aget v3, v3, v4

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/accessibility/AccessibilityManager;->enableMagnifier(IIF)V

    .line 190
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
