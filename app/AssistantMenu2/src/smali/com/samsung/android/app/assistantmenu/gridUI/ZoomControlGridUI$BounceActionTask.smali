.class Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;
.super Landroid/os/AsyncTask;
.source "ZoomControlGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BounceActionTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 765
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 766
    const-string v0, "ZoomControlGridUI"

    const-string v1, "BounceActionTask - BounceActionTask()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarEventHandler:Landroid/os/Handler;
    invoke-static {p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 768
    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteBounce:Z
    invoke-static {p1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z

    .line 769
    return-void
.end method

.method private BounceAction(I)V
    .locals 10
    .param p1, "distanceFromMiddle"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 812
    const-string v7, "ZoomControlGridUI"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BounceActionTask - BounceAction() :: distanceFromMiddle = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I
    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v7

    const/16 v8, 0x64

    if-le v7, v8, :cond_2

    move v4, v5

    .line 815
    .local v4, "isPositionRight":Z
    :goto_0
    int-to-float v7, p1

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBounceDegree:F
    invoke-static {v8}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v2, v7

    .line 816
    .local v2, "bouceDistance":I
    add-int v1, p1, v2

    .line 817
    .local v1, "actualDistance":I
    const/4 v0, 0x1

    .line 819
    .local v0, "accelRate":I
    :goto_1
    if-eqz v1, :cond_4

    .line 820
    if-le v1, v2, :cond_0

    .line 821
    add-int/lit8 v0, v0, 0x5

    .line 823
    :cond_0
    if-gt v1, v0, :cond_1

    .line 824
    move v0, v1

    .line 826
    :cond_1
    if-eqz v4, :cond_3

    .line 827
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # -= operator for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I
    invoke-static {v7, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1220(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;I)I

    .line 831
    :goto_2
    new-array v7, v5, [Ljava/lang/Integer;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I
    invoke-static {v8}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v6

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->publishProgress([Ljava/lang/Object;)V

    .line 834
    const-wide/16 v8, 0x1e

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 839
    :goto_3
    sub-int/2addr v1, v0

    goto :goto_1

    .end local v0    # "accelRate":I
    .end local v1    # "actualDistance":I
    .end local v2    # "bouceDistance":I
    .end local v4    # "isPositionRight":Z
    :cond_2
    move v4, v6

    .line 814
    goto :goto_0

    .line 829
    .restart local v0    # "accelRate":I
    .restart local v1    # "actualDistance":I
    .restart local v2    # "bouceDistance":I
    .restart local v4    # "isPositionRight":Z
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # += operator for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I
    invoke-static {v7, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1212(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;I)I

    goto :goto_2

    .line 835
    :catch_0
    move-exception v3

    .line 836
    .local v3, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 842
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_4
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 763
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/16 v5, 0x64

    const/high16 v4, 0x40400000    # 3.0f

    .line 773
    const-string v2, "ZoomControlGridUI"

    const-string v3, "BounceActionTask - doInBackground()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v2

    if-eq v2, v5, :cond_4

    .line 775
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v3

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->GetDistanceFromMiddle(II)I
    invoke-static {v2, v5, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1700(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;II)I

    move-result v0

    .line 777
    .local v0, "distanceFromMiddle":I
    rem-int/lit8 v1, v0, 0x64

    .line 779
    .local v1, "positionPerDistance":I
    const/16 v2, 0x4b

    if-gt v1, v2, :cond_0

    if-nez v1, :cond_1

    .line 780
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBounceDegree:F
    invoke-static {v2, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1802(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    .line 789
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->BounceAction(I)V

    goto :goto_0

    .line 781
    :cond_1
    const/16 v2, 0x32

    if-le v1, v2, :cond_2

    .line 782
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBounceDegree:F
    invoke-static {v2, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1802(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    goto :goto_1

    .line 783
    :cond_2
    const/16 v2, 0x19

    if-le v1, v2, :cond_3

    .line 784
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    const/high16 v3, 0x40000000    # 2.0f

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBounceDegree:F
    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1802(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    goto :goto_1

    .line 786
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    const v3, 0x3fe66666    # 1.8f

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBounceDegree:F
    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1802(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F

    goto :goto_1

    .line 791
    .end local v0    # "distanceFromMiddle":I
    .end local v1    # "positionPerDistance":I
    :cond_4
    const/4 v2, 0x0

    return-object v2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 763
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 803
    const-string v0, "ZoomControlGridUI"

    const-string v1, "BounceActionTask - onPostExecute()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteBounce:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z

    .line 807
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteZoom:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteBounce:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 808
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 809
    :cond_0
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 796
    const-string v0, "ZoomControlGridUI"

    const-string v1, "BounceActionTask - onProgressUpdate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 799
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 763
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
