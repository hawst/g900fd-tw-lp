.class Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;
.super Ljava/lang/Object;
.source "FingerMouseGridUI.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->SetEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v5, 0x8

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 378
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 379
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 447
    :cond_0
    :goto_0
    return v8

    .line 383
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePad:Z
    invoke-static {v3, v8}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1802(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    .line 384
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePtr:Z
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    .line 386
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v4, v4

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitX:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 387
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitY:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 389
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 390
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMagnifier:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 391
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    .line 392
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v5

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getBackGroundImage(I)I
    invoke-static {v4, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2600(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 399
    :pswitch_1
    const/4 v1, 0x0

    .line 400
    .local v1, "newX":I
    const/4 v2, 0x0

    .line 402
    .local v2, "newY":I
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePad:Z
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 403
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v1, v3

    .line 404
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v2, v3

    .line 406
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitX:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v5

    sub-int v5, v1, v5

    # += operator for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I
    invoke-static {v3, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2712(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 407
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitY:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v5

    sub-int v5, v2, v5

    # += operator for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I
    invoke-static {v3, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2812(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 409
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitX:I
    invoke-static {v3, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 410
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitY:I
    invoke-static {v3, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 412
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 413
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v3

    if-gez v3, :cond_2

    move v3, v4

    :goto_1
    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I
    invoke-static {v5, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2702(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 415
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v5

    if-gez v5, :cond_4

    :goto_2
    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2802(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 418
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 419
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 420
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/RelativeLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 421
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isSecureKeyguardLocked()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 422
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    goto/16 :goto_0

    .line 413
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v3

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v6, v7

    if-le v3, v6, :cond_3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v3

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v3, v6

    goto/16 :goto_1

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v3

    goto/16 :goto_1

    .line 415
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_5

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v4, v5

    goto/16 :goto_2

    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    goto/16 :goto_2

    .line 430
    .end local v1    # "newX":I
    .end local v2    # "newY":I
    :pswitch_2
    const-string v3, "FingerMouseGridUI"

    const-string v5, "ACTION_UP "

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitX:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 432
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitY:I
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 434
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePad:Z
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1802(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    .line 435
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePtr:Z
    invoke-static {v3, v8}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    .line 436
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    .line 437
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v5, v5, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 438
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 439
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSupportHoverZoom:Z
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMagnifier:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 440
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 379
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
