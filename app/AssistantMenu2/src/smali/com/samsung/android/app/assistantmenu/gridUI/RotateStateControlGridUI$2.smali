.class Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$2;
.super Ljava/lang/Object;
.source "RotateStateControlGridUI.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 100
    const-string v0, "RotateStateControlGridUI"

    const-string v1, "[c] onDismiss()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->removeInstance()V

    .line 104
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->ClickOutSideLayout()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)V

    .line 108
    :cond_1
    return-void
.end method
