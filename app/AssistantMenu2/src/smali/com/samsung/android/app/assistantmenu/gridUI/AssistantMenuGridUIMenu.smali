.class public Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;
.super Ljava/lang/Object;
.source "AssistantMenuGridUIMenu.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnHoverListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static mLatestPageIndex:I

.field public static mLocalItemSize:I


# instance fields
.field private final EVENT_MENU_LONGPRESS:I

.field private final EVENT_MENU_PAGE_LOOPING:I

.field private final MENU_LONGPRESS_DELAYED_TIME:I

.field private final TAG:Ljava/lang/String;

.field amdataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;",
            ">;"
        }
    .end annotation
.end field

.field private currIntentAction:Ljava/lang/String;

.field currPairs:Ljava/util/Map$Entry;

.field private dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

.field private hoverTimer:J

.field private hoverTimerEnabler:Z

.field private indexAppIcon:I

.field private isTouchedNextBtn:Z

.field private mAppIcons:I

.field private mAppTempIcons:I

.field private mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCurrentPage:Landroid/widget/TextView;

.field private mGridUIMenu:Landroid/widget/RelativeLayout;

.field private mGridUIMenuBackground:Landroid/widget/RelativeLayout;

.field private mGridUIMenuLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mGridUIMenuMain:Landroid/widget/RelativeLayout;

.field private mGridUIMenuNextButton:Landroid/widget/ImageView;

.field private mGridUIMenuNextHover:Landroid/widget/ImageView;

.field private mGridUIMenuPrevButton:Landroid/widget/ImageView;

.field private mGridUIMenuPrevHover:Landroid/widget/ImageView;

.field private final mHandler:Landroid/os/Handler;

.field private mIsViewPageLooping:Z

.field private mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

.field private mPageLayout:Landroid/widget/RelativeLayout;

.field private final mParentSvcHandler:Landroid/os/Handler;

.field private mPrePageTouchY:I

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private mResource:Landroid/content/res/Resources;

.field private mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

.field private mTotalItemSize:I

.field private mTotalPage:Landroid/widget/TextView;

.field private mTotalPageSize:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field private mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

.field private mWindowManager:Landroid/view/WindowManager;

.field private packMngr:Landroid/content/pm/PackageManager;

.field runAppIterator:Ljava/util/Iterator;

.field runningApp:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private stateScreen:Z

.field private topClassName:Ljava/lang/String;

.field private topPackName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLatestPageIndex:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Service;)V
    .locals 5
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const-string v0, "AssistantMenuGridUIMenu"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->TAG:Ljava/lang/String;

    .line 77
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 79
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    .line 85
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mWindowManager:Landroid/view/WindowManager;

    .line 87
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAudioManager:Landroid/media/AudioManager;

    .line 89
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    .line 91
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuMain:Landroid/widget/RelativeLayout;

    .line 93
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuBackground:Landroid/widget/RelativeLayout;

    .line 95
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPageLayout:Landroid/widget/RelativeLayout;

    .line 97
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mCurrentPage:Landroid/widget/TextView;

    .line 99
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPage:Landroid/widget/TextView;

    .line 101
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 103
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 105
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevButton:Landroid/widget/ImageView;

    .line 107
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextButton:Landroid/widget/ImageView;

    .line 110
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevHover:Landroid/widget/ImageView;

    .line 112
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextHover:Landroid/widget/ImageView;

    .line 116
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 118
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    .line 120
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 122
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->stateScreen:Z

    .line 124
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPrePageTouchY:I

    .line 129
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    .line 130
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppTempIcons:I

    .line 133
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->isTouchedNextBtn:Z

    .line 135
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mIsViewPageLooping:Z

    .line 141
    const/16 v0, 0x384

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->MENU_LONGPRESS_DELAYED_TIME:I

    .line 143
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->EVENT_MENU_LONGPRESS:I

    .line 145
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->EVENT_MENU_PAGE_LOOPING:I

    .line 148
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimer:J

    .line 149
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    .line 159
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currPairs:Ljava/util/Map$Entry;

    .line 160
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->indexAppIcon:I

    .line 165
    new-instance v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;

    .line 392
    new-instance v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mReceiver:Landroid/content/BroadcastReceiver;

    move-object v0, p1

    .line 229
    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 231
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    .line 233
    check-cast p1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .end local p1    # "service":Landroid/app/Service;
    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;

    .line 234
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 236
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAudioManager:Landroid/media/AudioManager;

    .line 238
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mWindowManager:Landroid/view/WindowManager;

    .line 240
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->packMngr:Landroid/content/pm/PackageManager;

    .line 241
    return-void
.end method

.method private AddPage(I)V
    .locals 14
    .param p1, "pageIndex"    # I

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v8, -0x2

    const/4 v10, 0x0

    .line 774
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v0, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 776
    .local v0, "child":Landroid/widget/LinearLayout;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 779
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 782
    .local v3, "lp2":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 785
    .local v4, "lp3":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 788
    .local v5, "lp4":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 791
    .local v6, "lp5":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f09002b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {v3, v7, v10, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 795
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f09002c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {v4, v10, v7, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 799
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f09002b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f09002c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {v5, v7, v8, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    .line 804
    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 805
    const/16 v7, 0x11

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 807
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v1, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 808
    .local v1, "child2":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 810
    invoke-direct {p0, p1, v11}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v8

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    invoke-direct {p0, p1, v11}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v8, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->MakeButton(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;I)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v1, v7, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 815
    invoke-direct {p0, p1, v13}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v8

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    invoke-direct {p0, p1, v13}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v8, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->MakeButton(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;I)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v1, v7, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 820
    invoke-virtual {v0, v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 822
    new-instance v1, Landroid/widget/LinearLayout;

    .end local v1    # "child2":Landroid/widget/LinearLayout;
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v1, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 823
    .restart local v1    # "child2":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 825
    invoke-direct {p0, p1, v10}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v8

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    invoke-direct {p0, p1, v10}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v8, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->MakeButton(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;I)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v1, v7, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 829
    invoke-direct {p0, p1, v12}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v8

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    invoke-direct {p0, p1, v12}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v8, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->MakeButton(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;I)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v1, v7, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 834
    invoke-virtual {v0, v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 836
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v7, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;->add(Landroid/view/View;)V

    .line 838
    return-void
.end method

.method private ClickOutSideLayout(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 563
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 564
    return-void
.end method

.method private ConnectUIObject()V
    .locals 11

    .prologue
    .line 296
    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v8

    if-eqz v8, :cond_1

    const v8, 0x7f09005a

    :goto_0
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 298
    .local v5, "displayWidth":I
    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v8

    if-eqz v8, :cond_2

    const v8, 0x7f090059

    :goto_1
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 301
    .local v4, "displayHeight":I
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v9, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 303
    .local v0, "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    sget-object v8, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v10, 0x7f090015

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sub-int v9, v5, v9

    invoke-interface {v0, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 307
    .local v2, "cornertabX":I
    sget-object v8, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-interface {v0, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 308
    .local v3, "cornertabY":I
    sget-object v8, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    invoke-interface {v0, v8, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 311
    .local v1, "cornertabDisplayHeight":I
    const-string v8, "AssistantMenuGridUIMenu"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[c] cornertabY:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", displayHeight:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090038

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090021

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-direct {v6, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 317
    .local v6, "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v8

    if-eqz v8, :cond_3

    const v8, 0x7f09005a

    :goto_2
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    if-le v2, v8, :cond_4

    .line 320
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v9, 0x7f03000d

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    .line 322
    const/16 v8, 0x15

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 323
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f090032

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 325
    const-string v8, "AssistantMenuGridUIMenu"

    const-string v9, "Load R.layout.grid_menu_right"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f09001f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 339
    iget v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v10, 0x7f090021

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    if-le v3, v8, :cond_0

    .line 341
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f090021

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int v8, v3, v8

    iput v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 345
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f09001f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sub-int v8, v1, v8

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v10, 0x7f090021

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sub-int v7, v8, v9

    .line 348
    .local v7, "maxCornertabBottomMargin":I
    iget v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    if-le v8, v7, :cond_5

    .end local v7    # "maxCornertabBottomMargin":I
    :goto_4
    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 351
    const/16 v8, 0xc

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 353
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d003f

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuMain:Landroid/widget/RelativeLayout;

    .line 355
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v6}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 359
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0040

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/support/v4/view/ViewPager;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 361
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d003e

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuBackground:Landroid/widget/RelativeLayout;

    .line 364
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0041

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPageLayout:Landroid/widget/RelativeLayout;

    .line 365
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0042

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mCurrentPage:Landroid/widget/TextView;

    .line 366
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mCurrentPage:Landroid/widget/TextView;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0044

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPage:Landroid/widget/TextView;

    .line 368
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPage:Landroid/widget/TextView;

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0045

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevButton:Landroid/widget/ImageView;

    .line 371
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0046

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextButton:Landroid/widget/ImageView;

    .line 373
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0047

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevHover:Landroid/widget/ImageView;

    .line 374
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0048

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextHover:Landroid/widget/ImageView;

    .line 375
    return-void

    .line 296
    .end local v0    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .end local v1    # "cornertabDisplayHeight":I
    .end local v2    # "cornertabX":I
    .end local v3    # "cornertabY":I
    .end local v4    # "displayHeight":I
    .end local v5    # "displayWidth":I
    .end local v6    # "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const v8, 0x7f090059

    goto/16 :goto_0

    .line 298
    .restart local v5    # "displayWidth":I
    :cond_2
    const v8, 0x7f09005a

    goto/16 :goto_1

    .line 317
    .restart local v0    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .restart local v1    # "cornertabDisplayHeight":I
    .restart local v2    # "cornertabX":I
    .restart local v3    # "cornertabY":I
    .restart local v4    # "displayHeight":I
    .restart local v6    # "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    const v8, 0x7f090059

    goto/16 :goto_2

    .line 328
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v9, 0x7f03000c

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    .line 330
    const/16 v8, 0x14

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 331
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f090025

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 333
    const-string v8, "AssistantMenuGridUIMenu"

    const-string v9, "Load R.layout.grid_menu_left"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 348
    .restart local v7    # "maxCornertabBottomMargin":I
    :cond_5
    iget v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_4
.end method

.method private GetGridUIMenuItem(II)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .locals 5
    .param p1, "itemPage"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 677
    sget v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->pageTotalItem:I

    mul-int/2addr v2, p1

    add-int v1, v2, p2

    .line 679
    .local v1, "curTotalItemIndex":I
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    add-int/lit8 v2, v2, -0x1

    if-le p1, v2, :cond_0

    .line 680
    const-string v2, "AssistantMenuGridUIMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] GetGridUIMenuItem:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 707
    :goto_0
    return-object v2

    .line 684
    :cond_0
    if-nez p1, :cond_3

    .line 685
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 686
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v2, v2, -0x2

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->pageTotalItem:I

    mul-int/2addr v2, v3

    add-int v1, v2, p2

    .line 695
    :cond_1
    :goto_1
    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v0

    .line 696
    .local v0, "curActString":Ljava/lang/String;
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    if-ge v1, v2, :cond_2

    .line 697
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    if-ge v1, v2, :cond_5

    .line 698
    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ShortCut:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v0

    .line 699
    const-string v2, "AssistantMenuGridUIMenu"

    const-string v3, "[c] GetGridUIMenuItem Shortcuts handling"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    :cond_2
    :goto_2
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->indexAppIcon:I

    .line 706
    const-string v2, "AssistantMenuGridUIMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] GetGridUIMenuItem return:mTotalItemSize ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "curTotalItemIndex ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mAppIcons ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v2

    goto :goto_0

    .line 689
    .end local v0    # "curActString":Ljava/lang/String;
    :cond_3
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_4

    .line 690
    move v1, p2

    goto :goto_1

    .line 692
    :cond_4
    sget v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->pageTotalItem:I

    sub-int/2addr v1, v2

    goto :goto_1

    .line 701
    .restart local v0    # "curActString":Ljava/lang/String;
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    sub-int v3, v1, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private Init()V
    .locals 3

    .prologue
    .line 249
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->SetGridUIParams()V

    .line 252
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ConnectUIObject()V

    .line 254
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->SetEventListener()V

    .line 255
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getdbHandler()Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    .line 257
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 259
    new-instance v0, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-direct {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    .line 261
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 262
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getFragOptionCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    .line 264
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->InitGridUIMenuItem()V

    .line 265
    return-void
.end method

.method private InitGridUIMenuItem()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 633
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyMenuItemTotalCount:Ljava/lang/String;

    invoke-interface {v2, v5, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    .line 635
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    if-lez v2, :cond_0

    .line 637
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getFragOptions()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->runningApp:Ljava/util/HashMap;

    .line 639
    :cond_0
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    sput v2, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLocalItemSize:I

    .line 640
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->indexAppIcon:I

    .line 641
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppTempIcons:I

    .line 642
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    if-nez v2, :cond_2

    .line 643
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->AddOptionalMenuIfNeed(Landroid/content/Context;)Z

    .line 644
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 645
    .local v1, "prefsEdit":Landroid/content/SharedPreferences$Editor;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 646
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    sget-object v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v5, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 645
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 649
    :cond_1
    sget-object v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyMenuItemTotalCount:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 651
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 653
    sget-object v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    .line 656
    .end local v0    # "i":I
    .end local v1    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    :cond_2
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    add-int/2addr v2, v5

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    .line 658
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    sget v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->pageTotalItem:I

    div-int v5, v2, v5

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalItemSize:I

    sget v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->pageTotalItem:I

    rem-int/2addr v2, v6

    if-lez v2, :cond_3

    move v2, v3

    :goto_1
    add-int/2addr v2, v5

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    .line 661
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    if-le v2, v3, :cond_4

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    :goto_2
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    .line 663
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->SetGridUIMenuPage()V

    .line 664
    return-void

    :cond_3
    move v2, v4

    .line 658
    goto :goto_1

    .line 661
    :cond_4
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    goto :goto_2
.end method

.method private InitGridUIMenuPage()V
    .locals 2

    .prologue
    .line 715
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 716
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;->clear()V

    .line 717
    return-void
.end method

.method private MakeButton(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;I)Landroid/widget/RelativeLayout;
    .locals 26
    .param p1, "act"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .param p2, "imageId"    # I

    .prologue
    .line 848
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    .line 849
    .local v18, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v22, v0

    const v23, 0x7f030001

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    .line 851
    .local v11, "mGridUIMenuButton":Landroid/widget/RelativeLayout;
    const v22, 0x7f0d0003

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageButton;

    .line 852
    .local v12, "mMenuButton":Landroid/widget/ImageButton;
    const v22, 0x7f0d0002

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageButton;

    .line 853
    .local v13, "mMenuButtonBg":Landroid/widget/ImageButton;
    invoke-virtual {v13}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    const v23, -0xff4321

    sget-object v24, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual/range {v22 .. v24}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 854
    const v22, 0x7f0d0004

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    .line 859
    .local v14, "mMenuButtonIcon":Landroid/widget/ImageView;
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 867
    const v22, 0x7f0d0005

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 871
    .local v15, "mMenuButtonName":Landroid/widget/TextView;
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ShortCut:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_7

    .line 874
    const-string v4, ""

    .local v4, "actKey":Ljava/lang/String;
    const-string v16, ""

    .line 875
    .local v16, "optVal":Ljava/lang/String;
    const/4 v10, 0x0

    .line 876
    .local v10, "length":I
    const/4 v6, 0x0

    .line 877
    .local v6, "count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->runningApp:Ljava/util/HashMap;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/HashMap;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_1

    .line 878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->runningApp:Ljava/util/HashMap;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->runAppIterator:Ljava/util/Iterator;

    .line 879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->runAppIterator:Ljava/util/Iterator;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->indexAppIcon:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_1

    .line 880
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->runAppIterator:Ljava/util/Iterator;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_1

    .line 881
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->runAppIterator:Ljava/util/Iterator;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/util/Map$Entry;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currPairs:Ljava/util/Map$Entry;

    .line 882
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currPairs:Ljava/util/Map$Entry;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    const-string v23, ";"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 883
    .local v8, "keyValues":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currPairs:Ljava/util/Map$Entry;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    const-string v23, ";"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 884
    .local v17, "optValues":[Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v10, v0

    .line 885
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->indexAppIcon:I

    move/from16 v22, v0

    add-int v23, v6, v10

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_5

    .line 886
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->indexAppIcon:I

    move/from16 v22, v0

    sub-int v20, v22, v6

    .line 887
    .local v20, "val":I
    const/16 v22, 0x0

    aget-object v4, v8, v22

    .line 888
    aget-object v16, v17, v20

    .line 889
    const-string v22, "AssistantMenuGridUIMenu"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    .end local v8    # "keyValues":[Ljava/lang/String;
    .end local v17    # "optValues":[Ljava/lang/String;
    .end local v20    # "val":I
    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_2

    .line 898
    const-string v22, "AssistantMenuGridUIMenu"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getSingleAMData(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    move-result-object v19

    .line 900
    .local v19, "showData":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    if-eqz v19, :cond_2

    .line 901
    const-string v22, "AssistantMenuGridUIMenu"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Addedicon"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getDrawable()[B

    move-result-object v22

    const/16 v23, 0x0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getDrawable()[B

    move-result-object v24

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    invoke-static/range {v22 .. v24}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 905
    .local v5, "bmp":Landroid/graphics/Bitmap;
    invoke-virtual {v14, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 906
    const/4 v9, 0x0

    .line 907
    .local v9, "label":Ljava/lang/String;
    const-string v22, "AssistantMenuGridUIMenu"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "String lable id: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAmLblName()J

    move-result-wide v24

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    const-string v22, "AssistantMenuGridUIMenu"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "String tilte name: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getTitleName()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAmLblName()J

    move-result-wide v22

    const-wide/16 v24, 0x0

    cmp-long v22, v22, v24

    if-eqz v22, :cond_6

    .line 911
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAppName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAmLblName()J

    move-result-wide v24

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 914
    const-string v22, "AssistantMenuGridUIMenu"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "lable id is not zero String: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    :goto_1
    invoke-virtual {v15, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 920
    invoke-virtual {v15, v9}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 928
    :goto_2
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAmAction()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 929
    const-string v23, "AssistantMenuGridUIMenu"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v24, " "

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v12}, Landroid/widget/ImageButton;->getTag()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 980
    .end local v4    # "actKey":Ljava/lang/String;
    .end local v5    # "bmp":Landroid/graphics/Bitmap;
    .end local v6    # "count":I
    .end local v9    # "label":Ljava/lang/String;
    .end local v10    # "length":I
    .end local v16    # "optVal":Ljava/lang/String;
    .end local v19    # "showData":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    :cond_2
    :goto_3
    invoke-virtual {v15}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v22

    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v21

    .line 989
    .local v21, "width":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    move-object/from16 v22, v0

    const v23, 0x7f090036

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    cmpl-float v22, v21, v22

    if-lez v22, :cond_3

    .line 990
    const/16 v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    move-object/from16 v24, v0

    const v25, 0x7f090037

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->pixelToDp(Landroid/content/Context;F)F

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v15, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 995
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->stateScreen:Z

    move/from16 v22, v0

    if-eqz v22, :cond_4

    sget-object v22, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->invalidList:Ljava/util/ArrayList;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 996
    const/high16 v22, 0x3f000000    # 0.5f

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 997
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1002
    :cond_4
    new-instance v22, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1109
    return-object v11

    .line 891
    .end local v21    # "width":F
    .restart local v4    # "actKey":Ljava/lang/String;
    .restart local v6    # "count":I
    .restart local v8    # "keyValues":[Ljava/lang/String;
    .restart local v10    # "length":I
    .restart local v16    # "optVal":Ljava/lang/String;
    .restart local v17    # "optValues":[Ljava/lang/String;
    :cond_5
    add-int v22, v6, v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->indexAppIcon:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-gt v0, v1, :cond_0

    .line 892
    add-int/2addr v6, v10

    goto/16 :goto_0

    .line 916
    .end local v8    # "keyValues":[Ljava/lang/String;
    .end local v17    # "optValues":[Ljava/lang/String;
    .restart local v5    # "bmp":Landroid/graphics/Bitmap;
    .restart local v9    # "label":Ljava/lang/String;
    .restart local v19    # "showData":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    :cond_6
    :try_start_1
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getTitleName()Ljava/lang/String;

    move-result-object v9

    .line 917
    const-string v22, "AssistantMenuGridUIMenu"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "lable id is zero String: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 921
    :catch_0
    move-exception v7

    .line 923
    .local v7, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v7}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto/16 :goto_2

    .line 924
    .end local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_1
    move-exception v7

    .line 926
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_2

    .line 933
    .end local v4    # "actKey":Ljava/lang/String;
    .end local v5    # "bmp":Landroid/graphics/Bitmap;
    .end local v6    # "count":I
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v9    # "label":Ljava/lang/String;
    .end local v10    # "length":I
    .end local v16    # "optVal":Ljava/lang/String;
    .end local v19    # "showData":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    :cond_7
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_8

    .line 935
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 936
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 937
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_3

    .line 938
    :cond_8
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 939
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickNoti()Z

    move-result v22

    if-eqz v22, :cond_9

    .line 940
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 942
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(I)V

    .line 944
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 946
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 949
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickSettings()Z

    move-result v22

    if-eqz v22, :cond_a

    .line 950
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 952
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(I)V

    .line 954
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 957
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 960
    :cond_a
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 962
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(I)V

    .line 964
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 968
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v23, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 974
    :cond_b
    move/from16 v0, p2

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 975
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setText(I)V

    .line 976
    sget-object v22, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method private SetEventListener()V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 383
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuBackground:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevHover:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 386
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextHover:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 387
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 388
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 390
    return-void
.end method

.method private SetGridUIMenuPage()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 724
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    .line 725
    .local v2, "menuItemListCount":I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->InitGridUIMenuPage()V

    .line 727
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 728
    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->AddPage(I)V

    .line 727
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 731
    :cond_0
    if-le v2, v5, :cond_1

    .line 732
    add-int/lit8 v2, v2, -0x2

    .line 735
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPage:Landroid/widget/TextView;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 737
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    if-ne v3, v5, :cond_4

    .line 738
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 739
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 741
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPageLayout:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 750
    :goto_1
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    if-le v3, v5, :cond_5

    .line 751
    sget v3, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLatestPageIndex:I

    if-eqz v3, :cond_2

    sget v3, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLatestPageIndex:I

    if-le v3, v2, :cond_3

    .line 752
    :cond_2
    sput v5, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLatestPageIndex:I

    .line 760
    :cond_3
    :goto_2
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    sget v4, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLatestPageIndex:I

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 761
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 766
    :goto_3
    return-void

    .line 743
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 745
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 746
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 755
    :cond_5
    sput v5, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLatestPageIndex:I

    .line 756
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mCurrentPage:Landroid/widget/TextView;

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 762
    :catch_0
    move-exception v0

    .line 763
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_3
.end method

.method private SetGridUIParams()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 282
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const/16 v4, 0x28

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 286
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "gridUI"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 287
    return-void
.end method

.method private ShowNextPage()V
    .locals 4

    .prologue
    .line 571
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mIsViewPageLooping:Z

    if-eqz v2, :cond_0

    .line 572
    const-string v2, "AssistantMenuGridUIMenu"

    const-string v3, "[c] ShowNextPage() return!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :goto_0
    return-void

    .line 577
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 578
    .local v0, "currentPageIndex":I
    add-int/lit8 v0, v0, 0x1

    .line 587
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 588
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 591
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 592
    .end local v0    # "currentPageIndex":I
    :catch_0
    move-exception v1

    .line 593
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method private ShowPrevPage()V
    .locals 4

    .prologue
    .line 602
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mIsViewPageLooping:Z

    if-eqz v2, :cond_0

    .line 603
    const-string v2, "AssistantMenuGridUIMenu"

    const-string v3, "[c] ShowPrevPage() return!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    :goto_0
    return-void

    .line 608
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 609
    .local v0, "currentPageIndex":I
    add-int/lit8 v0, v0, -0x1

    .line 618
    if-gez v0, :cond_1

    .line 619
    const/4 v0, 0x0

    .line 622
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 623
    .end local v0    # "currentPageIndex":I
    :catch_0
    move-exception v1

    .line 624
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;
    .param p1, "x1"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->launchBroadcastToApps(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mIsViewPageLooping:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->stateScreen:Z

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;

    return-object p1
.end method

.method private findButtonInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .locals 13
    .param p1, "actName"    # Ljava/lang/String;
    .param p2, "optName"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 1114
    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    const/4 v11, 0x1

    move-object v5, p1

    move-object v6, p2

    move-wide v7, v2

    move-object v9, v4

    move-object v10, v4

    invoke-direct/range {v1 .. v11}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;[BI)V

    .line 1116
    .local v1, "amLocal":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->amdataList:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->amdataList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1117
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->amdataList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v12

    .line 1118
    .local v12, "index":I
    if-ltz v12, :cond_0

    .line 1119
    const-string v3, "AssistantMenuGridUIMenu"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] findButtonInfo"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->amdataList:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getTitleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->amdataList:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    .line 1124
    .end local v12    # "index":I
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v4

    goto :goto_0
.end method

.method private launchBroadcastToApps(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V
    .locals 2
    .param p1, "act"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .prologue
    .line 1366
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1367
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1369
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1370
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;

    .line 1371
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v1, p1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->invokeAct(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    .line 1372
    return-void
.end method


# virtual methods
.method public ReloadView()V
    .locals 2

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    if-eqz v0, :cond_0

    .line 1345
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;->clear()V

    .line 1346
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;->notifyDataSetChanged()V

    .line 1348
    :cond_0
    new-instance v0, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-direct {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    .line 1350
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1352
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getFragOptionCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAppIcons:I

    .line 1354
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->InitGridUIMenuItem()V

    .line 1355
    return-void
.end method

.method public RemoveView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 436
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 438
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 441
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    .line 444
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 446
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;->clear()V

    .line 448
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;->notifyDataSetChanged()V

    .line 449
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StopAutoExit(Landroid/os/Handler;)V

    .line 453
    return-void
.end method

.method public ScreenRotateStatusChanged()V
    .locals 0

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->RemoveView()V

    .line 274
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowView()V

    .line 275
    return-void
.end method

.method public ShowView()V
    .locals 5

    .prologue
    .line 411
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isUnsecureKeyguardLocked()Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->stateScreen:Z

    .line 413
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_0

    .line 414
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->Init()V

    .line 417
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenu:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 419
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v3, 0x7f040003

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 420
    .local v0, "alphaAni":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 422
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 423
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 425
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 427
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 428
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 429
    return-void
.end method

.method public getIntentAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 464
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 465
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 467
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 487
    :goto_0
    return-void

    .line 470
    :sswitch_0
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ClickOutSideLayout(Landroid/view/View;)V

    goto :goto_0

    .line 475
    :sswitch_1
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowPrevPage()V

    goto :goto_0

    .line 480
    :sswitch_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowNextPage()V

    goto :goto_0

    .line 467
    :sswitch_data_0
    .sparse-switch
        0x7f0d003e -> :sswitch_0
        0x7f0d0045 -> :sswitch_1
        0x7f0d0046 -> :sswitch_2
    .end sparse-switch
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0x9

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 492
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 554
    :cond_0
    :goto_0
    return v7

    .line 495
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 497
    const/16 v1, 0xb

    const/4 v2, -0x1

    :try_start_0
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 511
    :cond_1
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    if-eqz v1, :cond_3

    .line 512
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimer:J

    .line 513
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    goto :goto_0

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 502
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 504
    const/4 v1, 0x1

    const/4 v2, -0x1

    :try_start_1
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 509
    :goto_2
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    goto :goto_1

    .line 506
    :catch_1
    move-exception v0

    .line 507
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 515
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimer:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 516
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowPrevPage()V

    .line 517
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    goto :goto_0

    .line 524
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v2, :cond_5

    .line 526
    const/16 v1, 0xf

    const/4 v2, -0x1

    :try_start_2
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 540
    :cond_4
    :goto_3
    iget-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    if-eqz v1, :cond_6

    .line 541
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimer:J

    .line 542
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    goto :goto_0

    .line 528
    :catch_2
    move-exception v0

    .line 529
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 531
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_4

    .line 533
    const/4 v1, 0x1

    const/4 v2, -0x1

    :try_start_3
    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setIcon(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    .line 538
    :goto_4
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    goto :goto_3

    .line 535
    :catch_3
    move-exception v0

    .line 536
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 544
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimer:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 545
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowNextPage()V

    .line 546
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->hoverTimerEnabler:Z

    goto/16 :goto_0

    .line 492
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0047
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 3
    .param p1, "arg0"    # I

    .prologue
    .line 1310
    if-nez p1, :cond_0

    .line 1311
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mIsViewPageLooping:Z

    .line 1312
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1313
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1318
    :cond_0
    :goto_0
    return-void

    .line 1315
    :catch_0
    move-exception v0

    .line 1316
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    .line 1337
    .line 1338
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1, "pos"    # I

    .prologue
    const/4 v5, 0x1

    .line 1134
    const-string v2, "AssistantMenuGridUIMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] onPageSelected:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 1139
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPagerAdapter:Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ViewPagerAdapter;->getCount()I

    move-result v1

    .line 1140
    .local v1, "pageCount":I
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    if-le v2, v5, :cond_0

    .line 1141
    add-int/lit8 v1, v1, -0x2

    .line 1142
    if-gtz p1, :cond_1

    .line 1143
    add-int/lit8 p1, v1, -0x1

    .line 1150
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mCurrentPage:Landroid/widget/TextView;

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1152
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mTotalPageSize:I

    if-le v2, v5, :cond_3

    .line 1153
    add-int/lit8 v2, p1, 0x1

    sput v2, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLatestPageIndex:I

    .line 1160
    .end local v1    # "pageCount":I
    :goto_1
    return-void

    .line 1144
    .restart local v1    # "pageCount":I
    :cond_1
    if-le p1, v1, :cond_2

    .line 1145
    const/4 p1, 0x0

    goto :goto_0

    .line 1147
    :cond_2
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 1155
    :cond_3
    sput p1, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLatestPageIndex:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1157
    .end local v1    # "pageCount":I
    :catch_0
    move-exception v0

    .line 1158
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v8, 0x7f090027

    const v7, 0x7f090026

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1175
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 1294
    const-string v1, "AssistantMenuGridUIMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] default: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    move v1, v2

    .line 1298
    :cond_1
    :goto_1
    return v1

    .line 1177
    :pswitch_0
    const-string v3, "AssistantMenuGridUIMenu"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[c] ACTION_DOWN:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1179
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 1180
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 1182
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f0d0041

    if-ne v3, v4, :cond_3

    .line 1183
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPrePageTouchY:I

    .line 1185
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09002e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v2, v6

    if-lez v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09002d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09002f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 1193
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v2, v6

    if-lez v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f090028

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 1200
    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->isTouchedNextBtn:Z

    goto/16 :goto_1

    .line 1205
    :cond_3
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPrePageTouchY:I

    goto/16 :goto_0

    .line 1212
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09002e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gez v1, :cond_4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v1, v1, v6

    if-lez v1, :cond_4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09002d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gez v1, :cond_4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09002f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-gtz v1, :cond_5

    .line 1219
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevButton:Landroid/widget/ImageView;

    const v3, 0x7f020057

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1222
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gez v1, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v1, v1, v6

    if-lez v1, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gez v1, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090028

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-gtz v1, :cond_0

    .line 1229
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextButton:Landroid/widget/ImageView;

    const v3, 0x7f020056

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1238
    :pswitch_2
    const-string v3, "AssistantMenuGridUIMenu"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[c] ACTION_UP:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPrePageTouchY:I

    if-lez v3, :cond_0

    .line 1241
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPrePageTouchY:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    sub-int v0, v3, v4

    .line 1242
    .local v0, "distance":I
    const-string v3, "AssistantMenuGridUIMenu"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[c] distance:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1244
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f090029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-gt v0, v3, :cond_7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_a

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_a

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_a

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f090028

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_a

    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->isTouchedNextBtn:Z

    if-eqz v3, :cond_a

    .line 1256
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f090031

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-le v0, v3, :cond_8

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-ge v0, v3, :cond_8

    .line 1260
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAudioManager:Landroid/media/AudioManager;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 1263
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowNextPage()V

    .line 1285
    :cond_9
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuNextButton:Landroid/widget/ImageView;

    const v4, 0x7f020056

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1286
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mGridUIMenuPrevButton:Landroid/widget/ImageView;

    const v4, 0x7f020057

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1288
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->isTouchedNextBtn:Z

    .line 1289
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mPrePageTouchY:I

    goto/16 :goto_1

    .line 1264
    :cond_a
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f090030

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-lt v0, v3, :cond_b

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f09002e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_9

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_9

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f09002d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_9

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f09002f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_9

    .line 1275
    :cond_b
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f090031

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-le v0, v3, :cond_c

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-ge v0, v3, :cond_c

    .line 1279
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAudioManager:Landroid/media/AudioManager;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 1282
    :cond_c
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowPrevPage()V

    goto :goto_2

    .line 1175
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setIntentAction(Ljava/lang/String;)V
    .locals 0
    .param p1, "intentact"    # Ljava/lang/String;

    .prologue
    .line 1362
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;

    .line 1363
    return-void
.end method
