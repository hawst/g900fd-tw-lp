.class public Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;
.super Ljava/lang/Object;
.source "RotateStateControlGridUI.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final SCREEN_ROTATION_CENTER:I

.field private final SCREEN_ROTATION_LEFT:I

.field private final SCREEN_ROTATION_RIGHT:I

.field private final TAG:Ljava/lang/String;

.field private isAutoRotateScreen:Z

.field private mBRotateCenter:Landroid/widget/ImageButton;

.field private mBRotateLeft:Landroid/widget/ImageButton;

.field private mBRotateRight:Landroid/widget/ImageButton;

.field private mCornertabBottomMargin:I

.field private mCurRotation:I

.field private mDefaultRotation:I

.field private final mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mIBRotateRemoveView:Landroid/widget/LinearLayout;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

.field private final mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mParentSvcHandler:Landroid/os/Handler;

.field private mResolver:Landroid/content/ContentResolver;

.field private final mRotateHandler:Landroid/os/Handler;

.field private mRotateStateControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

.field private mRotateWorking:Z

.field private final mRotateWorkingHandler:Landroid/os/Handler;

.field private mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

.field private mWm:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 4
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-string v1, "RotateStateControlGridUI"

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->TAG:Ljava/lang/String;

    .line 43
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 45
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    .line 47
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mLayout:Landroid/widget/RelativeLayout;

    .line 49
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 53
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    .line 55
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    .line 57
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    .line 60
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mIBRotateRemoveView:Landroid/widget/LinearLayout;

    .line 62
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mResolver:Landroid/content/ContentResolver;

    .line 64
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->SCREEN_ROTATION_CENTER:I

    .line 66
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->SCREEN_ROTATION_LEFT:I

    .line 68
    const/4 v1, 0x3

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->SCREEN_ROTATION_RIGHT:I

    .line 70
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 72
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->isAutoRotateScreen:Z

    .line 77
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mWm:Landroid/view/WindowManager;

    .line 81
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mCurRotation:I

    .line 83
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mCornertabBottomMargin:I

    .line 85
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateWorking:Z

    .line 87
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateWorkingHandler:Landroid/os/Handler;

    .line 89
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 97
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 124
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$3;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    move-object v1, p1

    .line 117
    check-cast v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object v0, p1

    .line 119
    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 120
    .local v0, "svc":Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    .line 121
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 122
    return-void
.end method

.method private ClickOutSideLayout()V
    .locals 2

    .prologue
    .line 498
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 499
    return-void
.end method

.method private Init()V
    .locals 11

    .prologue
    .line 272
    const-string v8, "RotateStateControlGridUI"

    const-string v9, "[c] Init()+"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v9, "window"

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mWm:Landroid/view/WindowManager;

    .line 276
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mResolver:Landroid/content/ContentResolver;

    .line 278
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v9, 0x7f03000e

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    .line 281
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d004b

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mLayout:Landroid/widget/RelativeLayout;

    .line 286
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v8

    if-eqz v8, :cond_1

    const v8, 0x7f09005a

    :goto_0
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 289
    .local v5, "displayWidth":I
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v8

    if-eqz v8, :cond_2

    const v8, 0x7f090059

    :goto_1
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 293
    .local v4, "displayHeight":I
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v9, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 296
    .local v0, "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    sget-object v8, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090015

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sub-int v9, v5, v9

    invoke-interface {v0, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 300
    .local v2, "cornertabX":I
    sget-object v8, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-interface {v0, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 301
    .local v3, "cornertabY":I
    sget-object v8, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    invoke-interface {v0, v8, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 304
    .local v1, "cornertabDisplayHeight":I
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090058

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090055

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-direct {v6, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 308
    .local v6, "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v8

    if-eqz v8, :cond_3

    const v8, 0x7f09005a

    :goto_2
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    if-le v2, v8, :cond_4

    .line 312
    const/16 v8, 0xb

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 313
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090057

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 323
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090054

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 326
    iget v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090055

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    if-le v3, v8, :cond_0

    .line 328
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090055

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int v8, v3, v8

    iput v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 332
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090054

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sub-int v8, v1, v8

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090055

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sub-int v7, v8, v9

    .line 335
    .local v7, "maxCornertabBottomMargin":I
    iget v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    if-le v8, v7, :cond_5

    .end local v7    # "maxCornertabBottomMargin":I
    :goto_4
    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 338
    const/16 v8, 0xc

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 340
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v6}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 342
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/RotateScreenManager;->getScreenRotationMode(Landroid/content/ContentResolver;)I

    move-result v8

    iput v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mDefaultRotation:I

    .line 346
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0051

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    .line 348
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0053

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    .line 350
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d0055

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    .line 352
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d004f

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mIBRotateRemoveView:Landroid/widget/LinearLayout;

    .line 355
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    invoke-virtual {v8, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 356
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    invoke-virtual {v8, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    invoke-virtual {v8, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mIBRotateRemoveView:Landroid/widget/LinearLayout;

    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 359
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mIBRotateRemoveView:Landroid/widget/LinearLayout;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setHoverPopupType(I)V

    .line 361
    iget v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mDefaultRotation:I

    invoke-direct {p0, v8}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->SetCurrentRotationButtonChange(I)V

    .line 363
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d004a

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/RotateScreenManager;->isAutoScreenRotation(Landroid/content/ContentResolver;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->isAutoRotateScreen:Z

    .line 384
    return-void

    .line 286
    .end local v0    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .end local v1    # "cornertabDisplayHeight":I
    .end local v2    # "cornertabX":I
    .end local v3    # "cornertabY":I
    .end local v4    # "displayHeight":I
    .end local v5    # "displayWidth":I
    .end local v6    # "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const v8, 0x7f090059

    goto/16 :goto_0

    .line 289
    .restart local v5    # "displayWidth":I
    :cond_2
    const v8, 0x7f09005a

    goto/16 :goto_1

    .line 308
    .restart local v0    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .restart local v1    # "cornertabDisplayHeight":I
    .restart local v2    # "cornertabX":I
    .restart local v3    # "cornertabY":I
    .restart local v4    # "displayHeight":I
    .restart local v6    # "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    const v8, 0x7f090059

    goto/16 :goto_2

    .line 318
    :cond_4
    const/16 v8, 0x9

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 319
    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090056

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto/16 :goto_3

    .line 335
    .restart local v7    # "maxCornertabBottomMargin":I
    :cond_5
    iget v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_4
.end method

.method private SetCurrentRotationButtonChange(I)V
    .locals 4
    .param p1, "curRotation"    # I

    .prologue
    const v3, -0xb451b0

    const v2, -0x252526

    .line 393
    packed-switch p1, :pswitch_data_0

    .line 412
    :goto_0
    :pswitch_0
    return-void

    .line 395
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v3, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 397
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 400
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v3, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 401
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 402
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 405
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 406
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 407
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v3, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 393
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private SetRotateStateControlGridUIParams()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 480
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_0

    .line 481
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const/16 v4, 0x28

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 487
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "rotateStateControlGridUI"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 488
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 490
    :cond_0
    return-void
.end method

.method private SetRotateWorking(I)Z
    .locals 8
    .param p1, "viewId"    # I

    .prologue
    const/4 v2, 0x1

    const v6, -0xb451b0

    const v5, -0x252526

    .line 218
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateWorking:Z

    if-eqz v3, :cond_0

    .line 219
    const-string v2, "RotateStateControlGridUI"

    const-string v3, "[c] SetRotateWorking mRotateWorking... return!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v2, 0x0

    .line 262
    :goto_0
    return v2

    .line 223
    :cond_0
    const/16 v1, 0x64

    .line 224
    .local v1, "workingDelay":I
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mWm:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 225
    .local v0, "curRotation":I
    packed-switch p1, :pswitch_data_0

    .line 254
    :cond_1
    :goto_1
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateWorkingHandler:Landroid/os/Handler;

    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$4;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI$4;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)V

    int-to-long v6, v1

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateWorking:Z

    goto :goto_0

    .line 227
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v6, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 228
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v5, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 229
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v5, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 231
    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    .line 232
    const/16 v1, 0xc8

    goto :goto_1

    .line 236
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v5, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 237
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v6, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 238
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v5, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_1

    .line 243
    :pswitch_3
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateLeft:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v5, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 244
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateCenter:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v5, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 245
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mBRotateRight:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v6, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 246
    if-ne v0, v2, :cond_1

    .line 247
    const/16 v1, 0xc8

    goto :goto_1

    .line 225
    :pswitch_data_0
    .packed-switch 0x7f0d0051
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->ClickOutSideLayout()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateWorking:Z

    return p1
.end method


# virtual methods
.method public ClickBtnOK()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 502
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/RotateScreenManager;->setAutoScreenRotation(Landroid/content/ContentResolver;Z)V

    .line 503
    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->isAutoRotateScreen:Z

    .line 506
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mResolver:Landroid/content/ContentResolver;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mCurRotation:I

    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/RotateScreenManager;->setScreenRotationMode(Landroid/content/ContentResolver;I)V

    .line 507
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/RotateScreenManager;->getScreenRotationMode(Landroid/content/ContentResolver;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mDefaultRotation:I

    .line 508
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mDefaultRotation:I

    invoke-direct {p0, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->SetCurrentRotationButtonChange(I)V

    .line 510
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->ShowView()V

    .line 514
    return-void
.end method

.method public RemoveView()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 457
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StopAutoExit(Landroid/os/Handler;)V

    .line 458
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 460
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    .line 462
    :cond_0
    return-void
.end method

.method public RemoveViewWithoutPokeWakelock()V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StopAutoExit(Landroid/os/Handler;)V

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 471
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    .line 472
    return-void
.end method

.method public ScreenRotateStatusChanged()V
    .locals 1

    .prologue
    .line 546
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->ScreenRotateStatusChanged(I)V

    .line 547
    return-void
.end method

.method public ScreenRotateStatusChanged(I)V
    .locals 3
    .param p1, "curRotation"    # I

    .prologue
    .line 550
    const-string v0, "RotateStateControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] ScreenRotateStatusChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mWm:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->CornertabUiUpdate()V

    .line 553
    return-void
.end method

.method public ShowView()V
    .locals 7

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 421
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->Init()V

    .line 423
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->SetRotateStateControlGridUIParams()V

    .line 424
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mWm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v1, 0x7f040003

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v6

    .line 428
    .local v6, "alphaAni":Landroid/view/animation/Animation;
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 430
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->isAutoRotateScreen:Z

    if-eqz v0, :cond_1

    .line 432
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->removeInstance()V

    .line 433
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0058

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->ShowDialog(Landroid/content/Context;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 436
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 437
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->RemoveView()V

    .line 444
    :goto_0
    return-void

    .line 439
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateStateControlGridUIView:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 441
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 442
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v6, 0xc8

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 155
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->SetRotateWorking(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    :goto_0
    return-void

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mWm:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mCurRotation:I

    .line 160
    const-string v0, "RotateStateControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] onClick:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mCurRotation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 163
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 166
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 205
    :goto_1
    :sswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->ClickOutSideLayout()V

    goto :goto_0

    .line 168
    :sswitch_1
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mCurRotation:I

    if-ne v0, v5, :cond_1

    .line 169
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 170
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 177
    :sswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 181
    :sswitch_3
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mCurRotation:I

    if-ne v0, v4, :cond_2

    .line 182
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 183
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 185
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->mRotateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 166
    :sswitch_data_0
    .sparse-switch
        0x7f0d004a -> :sswitch_0
        0x7f0d004f -> :sswitch_0
        0x7f0d0051 -> :sswitch_1
        0x7f0d0053 -> :sswitch_2
        0x7f0d0055 -> :sswitch_3
    .end sparse-switch
.end method
