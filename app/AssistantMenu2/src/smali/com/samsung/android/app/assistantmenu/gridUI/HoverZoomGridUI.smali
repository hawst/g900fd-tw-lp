.class public Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
.super Ljava/lang/Object;
.source "HoverZoomGridUI.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomAction;,
        Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;
    }
.end annotation


# static fields
.field public static final HOVER_ZOOM_MAGNIFIER_SIZE:Ljava/lang/String; = "hover_zoom_magnifier_size"

.field public static final HOVER_ZOOM_VALUE:Ljava/lang/String; = "hover_zoom_value"

.field private static final SCROLL_ACTION_MOVE_STEPS:I = 0x4


# instance fields
.field private final TAG:Ljava/lang/String;

.field private an:Landroid/view/animation/Animation;

.field private dblCursX:F

.field private dblCursY:F

.field private isCueShown:Z

.field private isCursorChange:Z

.field private isPokeLockEnable:Z

.field private final mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mCount_hold:I

.field private mCursorCue:Landroid/widget/ImageView;

.field private mCursorCueLayout:Landroid/widget/RelativeLayout;

.field private mCursorLayoutHeight:I

.field private mCursorLayoutWidth:I

.field private mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

.field private mCursorX:F

.field private mCursorY:F

.field private mDeviceWidth:I

.field private mDeviceheight:I

.field private final mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mDouble:Z

.field private mDoubleTap:Z

.field private mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mFmGridUIMain:Landroid/widget/RelativeLayout;

.field private mFmGridUITab:Landroid/widget/RelativeLayout;

.field mHoldCursorFlag:Z

.field private mHoverPadSizeObserver:Landroid/database/ContentObserver;

.field private mHoverScaleObserver:Landroid/database/ContentObserver;

.field private mHoverZoomEnabled:I

.field private mHoverZoomPadDisplayed:Z

.field private final mHoverZoomShowHandler:Landroid/os/Handler;

.field private mIBtnFmMoveView:Landroid/widget/ImageView;

.field private mIBtnFmRemoveView:Landroid/widget/LinearLayout;

.field private mIBtnFmTouchView:Landroid/widget/LinearLayout;

.field private mInitX:I

.field private mInitY:I

.field private final mInvalidateMargin:I

.field private mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

.field private mLongPress:Z

.field mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

.field private mMagnifierSize:I

.field mManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mOffsetCursorPosition:I

.field private final mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mPadSize:I

.field private final mParentSvcHandler:Landroid/os/Handler;

.field private mPointerSize:I

.field private mPointerSpeed:I

.field private mResource:Landroid/content/res/Resources;

.field mResourceBackGround:Landroid/graphics/drawable/Drawable;

.field private mScaleSize:I

.field private mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

.field private mTabX:I

.field private mTabY:I

.field private final mThreshold:I

.field private mTouchGestureDetector:Landroid/view/GestureDetector;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mfloatingUILayoutFm:Landroid/widget/ImageView;

.field private movePad:Z

.field private movePtr:Z

.field private pCurX:F

.field private pCurY:F

.field private params_cursorCue:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 10
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v4, "HoverZoomGridUI"

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->TAG:Ljava/lang/String;

    .line 51
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 53
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    .line 55
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 57
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 61
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mWindowManager:Landroid/view/WindowManager;

    .line 63
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    .line 65
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUIMain:Landroid/widget/RelativeLayout;

    .line 67
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;

    .line 69
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmTouchView:Landroid/widget/LinearLayout;

    .line 71
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmMoveView:Landroid/widget/ImageView;

    .line 73
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    .line 79
    iput v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSpeed:I

    .line 81
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSize:I

    .line 83
    iput v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    .line 85
    iput v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I

    .line 87
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mScaleSize:I

    .line 91
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePad:Z

    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePtr:Z

    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDouble:Z

    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDoubleTap:Z

    .line 93
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitX:I

    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitY:I

    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I

    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I

    .line 95
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    .line 97
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    .line 99
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F

    .line 101
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurX:F

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurY:F

    .line 103
    const/16 v4, 0x8

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mThreshold:I

    .line 105
    const/16 v4, 0x64

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInvalidateMargin:I

    .line 109
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorChange:Z

    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isPokeLockEnable:Z

    .line 111
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    .line 114
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLongPress:Z

    .line 116
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    .line 119
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    .line 121
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCue:Landroid/widget/ImageView;

    .line 123
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    .line 125
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCueShown:Z

    .line 127
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->an:Landroid/view/animation/Animation;

    .line 129
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomEnabled:I

    .line 131
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCount_hold:I

    .line 133
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoldCursorFlag:Z

    .line 135
    const/16 v4, 0x1e

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mOffsetCursorPosition:I

    .line 137
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z

    .line 139
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomShowHandler:Landroid/os/Handler;

    .line 167
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$2;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverScaleObserver:Landroid/database/ContentObserver;

    .line 184
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverPadSizeObserver:Landroid/database/ContentObserver;

    .line 722
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$6;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$6;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 735
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$7;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$7;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 746
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$8;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$8;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    move-object v4, p1

    .line 205
    check-cast v4, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 208
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v5, "accessibility"

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    .line 215
    check-cast p1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .end local p1    # "service":Landroid/app/Service;
    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mParentSvcHandler:Landroid/os/Handler;

    .line 217
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v5, "window"

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mWindowManager:Landroid/view/WindowManager;

    .line 219
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 221
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "assistant_menu_pointer_speed"

    invoke-static {v4, v5, v9}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .local v3, "pointer_speed":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSpeed:I

    if-eq v3, v4, :cond_0

    move v4, v3

    :goto_1
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSpeed:I

    .line 225
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "assistant_menu_pointer_size"

    invoke-static {v4, v5, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .local v2, "pointer_size":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSize:I

    if-eq v2, v4, :cond_1

    move v4, v2

    :goto_2
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSize:I

    .line 229
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "assistant_menu_pad_size"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .local v1, "pad_size":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    if-eq v1, v4, :cond_2

    move v4, v1

    :goto_3
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    .line 233
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v4, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    .line 234
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "FmMagnifier"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 235
    const-string v4, "HoverZoomGridUI"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[c] pointer_speed="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pointer_size= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pad_size="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mMagnifierSize="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iput-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z

    .line 238
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomShowHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 239
    return-void

    .line 209
    .end local v1    # "pad_size":I
    .end local v2    # "pointer_size":I
    .end local v3    # "pointer_speed":I
    .restart local p1    # "service":Landroid/app/Service;
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 221
    .end local v0    # "e":Ljava/lang/Exception;
    .end local p1    # "service":Landroid/app/Service;
    .restart local v3    # "pointer_speed":I
    :cond_0
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSpeed:I

    goto/16 :goto_1

    .line 225
    .restart local v2    # "pointer_size":I
    :cond_1
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSize:I

    goto :goto_2

    .line 229
    .restart local v1    # "pad_size":I
    :cond_2
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    goto :goto_3
.end method

.method private ConnectUIObject()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 281
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    if-le v1, v3, :cond_0

    .line 282
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    .line 284
    :cond_0
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->setLayoutDimensions(I)V

    .line 285
    new-array v0, v3, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    new-array v1, v6, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v2

    .line 293
    .local v0, "fmGridLayout":[[I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getHandmode()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 294
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    aget-object v2, v0, v5

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    aget v2, v2, v3

    invoke-static {v1, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    .line 298
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0024

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUIMain:Landroid/widget/RelativeLayout;

    .line 300
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0029

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmTouchView:Landroid/widget/LinearLayout;

    .line 302
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0026

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;

    .line 304
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0030

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmMoveView:Landroid/widget/ImageView;

    .line 306
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0028

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;

    .line 309
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v2, 0x7f030003

    invoke-static {v1, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    .line 311
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0008

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCue:Landroid/widget/ImageView;

    .line 313
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCue:Landroid/widget/ImageView;

    const v2, 0x7f02010b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 315
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v2, 0x7f040002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->an:Landroid/view/animation/Animation;

    .line 316
    return-void

    .line 296
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    aget-object v2, v0, v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    aget v2, v2, v3

    invoke-static {v1, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    goto :goto_0

    .line 285
    :array_0
    .array-data 4
        0x7f030015
        0x7f030013
        0x7f030014
    .end array-data

    :array_1
    .array-data 4
        0x7f030012
        0x7f030010
        0x7f030011
    .end array-data
.end method

.method private Init()V
    .locals 5

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 242
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 243
    .local v0, "screen_size":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 244
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I

    .line 245
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    .line 249
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    .line 250
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    .line 252
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->SetGridUIParams()V

    .line 254
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    new-instance v3, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$1;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTouchGestureDetector:Landroid/view/GestureDetector;

    .line 256
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->ConnectUIObject()V

    .line 257
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->SetEventListener()V

    .line 258
    return-void
.end method

.method private SetEventListener()V
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmTouchView:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 356
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmMoveView:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$5;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 434
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 435
    return-void
.end method

.method private SetGridUIParams()V
    .locals 6

    .prologue
    const/4 v1, -0x2

    .line 261
    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    .line 262
    .local v3, "windowParamType":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x28

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 270
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    const/high16 v2, -0x80000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 271
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit16 v1, v1, 0x8ad

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 276
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a0026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 277
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomEnabled:I

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomEnabled:I

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverPadSizeObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDoubleTap:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDoubleTap:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLongPress:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLongPress:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDouble:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDouble:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectAccessibilityMotionEvent(IZ)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTouchGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePad:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePad:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePtr:Z

    return p1
.end method

.method static synthetic access$202(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitX:I

    return v0
.end method

.method static synthetic access$2102(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitX:I

    return p1
.end method

.method static synthetic access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitY:I

    return v0
.end method

.method static synthetic access$2202(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mInitY:I

    return p1
.end method

.method static synthetic access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getBackGroundImage(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I

    return v0
.end method

.method static synthetic access$2702(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I

    return p1
.end method

.method static synthetic access$2712(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I

    return v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I

    return v0
.end method

.method static synthetic access$2802(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I

    return p1
.end method

.method static synthetic access$2812(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I

    return v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I

    return v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I

    return p1
.end method

.method static synthetic access$3100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I

    return v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomShowHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mParentSvcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorOnTop()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorBottom()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->closeStatusBarPanel()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mScaleSize:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mScaleSize:I

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurX:F

    return p1
.end method

.method static synthetic access$802(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurY:F

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverScaleObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method private changeCursorStatus(Z)V
    .locals 2
    .param p1, "isChange"    # Z

    .prologue
    .line 618
    if-eqz p1, :cond_1

    .line 619
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorChange:Z

    .line 621
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorOnTop()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->addCue()V

    .line 628
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->update(FF)V

    .line 629
    return-void

    .line 624
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorChange:Z

    .line 626
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->removeCue()V

    goto :goto_0
.end method

.method private closeStatusBarPanel()V
    .locals 3

    .prologue
    .line 632
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "statusbar"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 634
    .local v0, "mStatusBar":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 635
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    .line 636
    :cond_0
    return-void
.end method

.method private getBackGroundImage(I)I
    .locals 3
    .param p1, "padSize"    # I

    .prologue
    const/4 v2, 0x1

    .line 1185
    const/4 v0, 0x0

    .line 1186
    .local v0, "backgroundResid":I
    packed-switch p1, :pswitch_data_0

    .line 1211
    :goto_0
    return v0

    .line 1188
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getHandmode()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1189
    const v0, 0x7f02011d

    goto :goto_0

    .line 1191
    :cond_0
    const v0, 0x7f0200fc

    .line 1193
    goto :goto_0

    .line 1195
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getHandmode()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 1196
    const v0, 0x7f020117

    goto :goto_0

    .line 1198
    :cond_1
    const v0, 0x7f0200f6

    .line 1200
    goto :goto_0

    .line 1202
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getHandmode()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 1203
    const v0, 0x7f020111

    goto :goto_0

    .line 1205
    :cond_2
    const v0, 0x7f0200f0

    .line 1207
    goto :goto_0

    .line 1186
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getHandmode()I
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 678
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f09005a

    :goto_0
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 680
    .local v2, "displayWidth":I
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 684
    .local v0, "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f090015

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v5, v2, v5

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 690
    .local v1, "cornertabX":I
    div-int/lit8 v3, v2, 0x2

    if-le v1, v3, :cond_1

    .line 691
    const/4 v3, 0x1

    .line 693
    :goto_1
    return v3

    .line 678
    .end local v0    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .end local v1    # "cornertabX":I
    .end local v2    # "displayWidth":I
    :cond_0
    const v3, 0x7f090059

    goto :goto_0

    .restart local v0    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .restart local v1    # "cornertabX":I
    .restart local v2    # "displayWidth":I
    :cond_1
    move v3, v4

    .line 693
    goto :goto_1
.end method

.method private injectAccessibilityMotionEvent(IZ)V
    .locals 2
    .param p1, "eventAction"    # I
    .param p2, "isDouble"    # Z

    .prologue
    .line 804
    const-string v0, "HoverZoomGridUI"

    const-string v1, "injectAccessibilityMotionEvent()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$9;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;IZ)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 895
    return-void
.end method

.method private isCursorBottom()Z
    .locals 4

    .prologue
    .line 610
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f09001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 611
    const/4 v0, 0x1

    .line 613
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCursorOnTop()Z
    .locals 3

    .prologue
    .line 602
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f09001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 603
    const/4 v0, 0x1

    .line 605
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setLayoutDimensions(I)V
    .locals 2
    .param p1, "padSize"    # I

    .prologue
    .line 698
    packed-switch p1, :pswitch_data_0

    .line 720
    :goto_0
    return-void

    .line 700
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutWidth:I

    .line 702
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutHeight:I

    goto :goto_0

    .line 706
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutWidth:I

    .line 708
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutHeight:I

    goto :goto_0

    .line 712
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutWidth:I

    .line 714
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutHeight:I

    goto :goto_0

    .line 698
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public ClearMagnifierStatus()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 528
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "FmMagnifier"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 529
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomEnabled:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 531
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->disableMagnifier()V

    .line 532
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 536
    :goto_0
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomEnabled:I

    .line 538
    :cond_0
    return-void

    .line 533
    :catch_0
    move-exception v0

    .line 534
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public InjectDoubleTapEvent(JJ[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IFFIII)V
    .locals 19
    .param p1, "downTime"    # J
    .param p3, "eventTime"    # J
    .param p5, "properties"    # [Landroid/view/MotionEvent$PointerProperties;
    .param p6, "pointerCoords"    # [Landroid/view/MotionEvent$PointerCoords;
    .param p7, "metaState"    # I
    .param p8, "xPrecision"    # F
    .param p9, "yPrecision"    # F
    .param p10, "deviceId"    # I
    .param p11, "edgeFlag"    # I
    .param p12, "eventFlag"    # I

    .prologue
    .line 980
    const/16 v17, 0x0

    .line 981
    .local v17, "mMotionEvent":Landroid/view/MotionEvent;
    new-instance v16, Landroid/app/Instrumentation;

    invoke-direct/range {v16 .. v16}, Landroid/app/Instrumentation;-><init>()V

    .line 983
    .local v16, "insObj":Landroid/app/Instrumentation;
    const-wide/16 v0, 0x32

    add-long p3, p3, v0

    .line 985
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v14, 0x0

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v15, p12

    :try_start_0
    invoke-static/range {v0 .. v15}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 990
    invoke-virtual/range {v16 .. v17}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 992
    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v14, 0x0

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v15, p12

    invoke-static/range {v0 .. v15}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 996
    invoke-virtual/range {v16 .. v17}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 998
    if-eqz v17, :cond_0

    .line 999
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 1000
    const/16 v17, 0x0

    .line 1003
    :cond_0
    return-void

    .line 998
    :catchall_0
    move-exception v0

    if-eqz v17, :cond_1

    .line 999
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 1000
    const/16 v17, 0x0

    :cond_1
    throw v0
.end method

.method public RemoveView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 495
    const-string v1, "HoverZoomGridUI"

    const-string v2, "RemoveView()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 497
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 499
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 501
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->removeCue()V

    .line 502
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    .line 504
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 505
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 508
    :try_start_0
    const-string v1, "HoverZoomGridUI"

    const-string v2, "mManager.hideMagnifier()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->hideMagnifier()V

    .line 510
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 514
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverScaleObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 515
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverPadSizeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 518
    :try_start_1
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->CloseDialog()V

    .line 519
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->removeInstance()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 523
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomShowHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 524
    return-void

    .line 511
    :catch_0
    move-exception v0

    .line 512
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 520
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public ScreenRotateStatusChanged()V
    .locals 3

    .prologue
    .line 545
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCount_hold:I

    .line 546
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->RemoveView()V

    .line 547
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->ShowView()V

    .line 548
    const-string v0, "HoverZoomGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init ScreenRotateStatusChanged mMagnifierSize= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    return-void
.end method

.method public ShowView()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v1, -0x1

    const/4 v6, 0x1

    .line 441
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-nez v2, :cond_1

    .line 442
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->Init()V

    .line 444
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->initCursorPreference()V

    .line 446
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabX:I

    .line 447
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTabY:I

    .line 449
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v4, v5}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 451
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v4, 0x7f040003

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v10

    .line 452
    .local v10, "alphaAni":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUIMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v10}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 456
    new-instance v2, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v2, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    .line 457
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setMagnifierCursorImage(Landroid/content/Context;)V

    .line 459
    const/16 v3, 0x7d6

    .line 460
    .local v3, "windowParamType":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v3, 0x8ad

    .line 462
    :cond_2
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const v4, 0x1000118

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 471
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 472
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a0027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 473
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 474
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isPokeLockEnable:Z

    .line 475
    invoke-direct {p0, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->changeCursorStatus(Z)V

    .line 476
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    if-nez v1, :cond_3

    .line 477
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 480
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accessibility_magnifier"

    invoke-static {v1, v2, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v6, :cond_4

    .line 481
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->RemoveView()V

    .line 482
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->removeInstance()V

    .line 483
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual/range {v4 .. v9}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->ShowDialog(Landroid/content/Context;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 489
    :goto_0
    return-void

    .line 486
    :cond_4
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomEnabled:I

    .line 487
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomShowHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public addCue()V
    .locals 6

    .prologue
    const/4 v1, -0x2

    .line 1240
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCueShown:Z

    if-nez v0, :cond_2

    .line 1241
    const/16 v3, 0x7d6

    .line 1242
    .local v3, "windowParamType":I
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v3, 0x8ad

    .line 1244
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_1

    .line 1245
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x118

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    .line 1253
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1256
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1257
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCue:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->an:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCueShown:Z

    .line 1260
    .end local v3    # "windowParamType":I
    :cond_2
    return-void
.end method

.method public changePadSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 1181
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    .line 1182
    return-void
.end method

.method public changePointerSize(I)V
    .locals 3
    .param p1, "size"    # I

    .prologue
    .line 1175
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSize:I

    .line 1176
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSize:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setCursorImage(Landroid/content/Context;I)V

    .line 1177
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->update(FF)V

    .line 1178
    return-void
.end method

.method public changePointerSpeed(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 1171
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSpeed:I

    .line 1172
    return-void
.end method

.method public editCursorPreference(II)V
    .locals 6
    .param p1, "xPos"    # I
    .param p2, "yPos"    # I

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getHandmode()I

    move-result v1

    .line 640
    .local v1, "dominantMode":I
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->hoverZoomPref:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 642
    .local v0, "cursorPreference":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 643
    .local v2, "e":Landroid/content/SharedPreferences$Editor;
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosX:Ljava/lang/String;

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 644
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosY:Ljava/lang/String;

    invoke-interface {v2, v3, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 645
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPortrait:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 646
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 647
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorPadSize:Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 648
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 649
    return-void
.end method

.method public getPadxPos()I
    .locals 3

    .prologue
    .line 1215
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v1, :cond_1

    .line 1216
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v0, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1223
    :cond_0
    :goto_0
    return v0

    .line 1218
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getHandmode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1219
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutWidth:I

    sub-int v0, v1, v2

    .line 1220
    .local v0, "valuePos":I
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f090011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 1223
    .end local v0    # "valuePos":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPadyPos()I
    .locals 3

    .prologue
    .line 1229
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_0

    .line 1230
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1232
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutHeight:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f090016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method getPointerCoordinates(Landroid/view/MotionEvent$PointerCoords;I)Landroid/view/MotionEvent$PointerCoords;
    .locals 5
    .param p1, "initial"    # Landroid/view/MotionEvent$PointerCoords;
    .param p2, "eventAction"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x40800000    # 4.0f

    .line 898
    const/4 v2, 0x1

    new-array v0, v2, [Landroid/view/MotionEvent$PointerCoords;

    .line 899
    .local v0, "motion_EventpointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    new-instance v1, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v1}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 900
    .local v1, "pc1":Landroid/view/MotionEvent$PointerCoords;
    move-object v1, p1

    .line 901
    packed-switch p2, :pswitch_data_0

    .line 921
    :goto_0
    aput-object v1, v0, v4

    .line 922
    aget-object v2, v0, v4

    return-object v2

    .line 903
    :pswitch_0
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 904
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_0

    .line 907
    :pswitch_1
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 908
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_0

    .line 911
    :pswitch_2
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 912
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_0

    .line 915
    :pswitch_3
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    mul-int/lit8 v2, v2, 0x3

    int-to-float v2, v2

    div-float/2addr v2, v3

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 916
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_0

    .line 901
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public initCursorPreference()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 652
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getHandmode()I

    move-result v3

    .line 654
    .local v3, "dominantHand":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->hoverZoomPref:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 656
    .local v0, "cursorPreference":Landroid/content/SharedPreferences;
    const/4 v2, 0x0

    .line 657
    .local v2, "defaultX":I
    if-ne v3, v7, :cond_0

    .line 658
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutWidth:I

    sub-int v2, v4, v5

    .line 659
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f090011

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v2, v4

    .line 661
    :cond_0
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutHeight:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f090016

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v1, v4, v5

    .line 664
    .local v1, "deafaultY":I
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPortrait:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v5

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v5

    if-ne v4, v5, :cond_1

    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorPadSize:Ljava/lang/String;

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPadSize:I

    if-ne v4, v5, :cond_1

    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v3, :cond_2

    .line 667
    :cond_1
    invoke-virtual {p0, v2, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->editCursorPreference(II)V

    .line 669
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosX:Ljava/lang/String;

    invoke-interface {v0, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 671
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosY:Ljava/lang/String;

    invoke-interface {v0, v5, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 673
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutHeight:I

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 674
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorLayoutWidth:I

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 675
    return-void
.end method

.method public injectMotionEvent(J[Landroid/view/MotionEvent$PointerProperties;Landroid/view/MotionEvent$PointerCoords;IFFIIII)V
    .locals 25
    .param p1, "downTime"    # J
    .param p3, "properties"    # [Landroid/view/MotionEvent$PointerProperties;
    .param p4, "pointerCoords"    # Landroid/view/MotionEvent$PointerCoords;
    .param p5, "metaState"    # I
    .param p6, "xPrecision"    # F
    .param p7, "yPrecision"    # F
    .param p8, "deviceId"    # I
    .param p9, "edgeFlag"    # I
    .param p10, "eventFlag"    # I
    .param p11, "eventAction"    # I

    .prologue
    .line 929
    const/16 v20, 0x0

    .line 930
    .local v20, "mMotionEvent":Landroid/view/MotionEvent;
    new-instance v19, Landroid/app/Instrumentation;

    invoke-direct/range {v19 .. v19}, Landroid/app/Instrumentation;-><init>()V

    .line 931
    .local v19, "insObj":Landroid/app/Instrumentation;
    const/4 v2, 0x1

    new-array v9, v2, [Landroid/view/MotionEvent$PointerCoords;

    .line 932
    .local v9, "motion_EventpointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    new-instance v21, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v21 .. v21}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 933
    .local v21, "pc1":Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v21, p4

    .line 935
    const/16 v22, 0x0

    .line 936
    .local v22, "stepX":F
    const/16 v23, 0x0

    .line 937
    .local v23, "stepY":F
    packed-switch p11, :pswitch_data_0

    .line 954
    :goto_0
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    const/4 v2, 0x4

    move/from16 v0, v18

    if-ge v0, v2, :cond_0

    .line 955
    :try_start_0
    move-object/from16 v0, v21

    iget v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    add-float v2, v2, v22

    move-object/from16 v0, v21

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 956
    move-object/from16 v0, v21

    iget v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    add-float v2, v2, v23

    move-object/from16 v0, v21

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 957
    const/4 v2, 0x0

    aput-object v21, v9, v2

    .line 958
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 959
    .local v4, "eventTime":J
    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v11, 0x0

    const/16 v16, 0x0

    move-wide/from16 v2, p1

    move-object/from16 v8, p3

    move/from16 v10, p5

    move/from16 v12, p6

    move/from16 v13, p7

    move/from16 v14, p8

    move/from16 v15, p9

    move/from16 v17, p10

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v20

    .line 963
    invoke-virtual/range {v19 .. v20}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 954
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 939
    .end local v4    # "eventTime":J
    .end local v18    # "i":I
    :pswitch_0
    const/high16 v23, -0x3d380000    # -100.0f

    .line 940
    goto :goto_0

    .line 942
    :pswitch_1
    const/high16 v23, 0x42c80000    # 100.0f

    .line 943
    goto :goto_0

    .line 945
    :pswitch_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x41000000    # 8.0f

    div-float v22, v2, v3

    .line 946
    goto :goto_0

    .line 948
    :pswitch_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    neg-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x41000000    # 8.0f

    div-float v22, v2, v3

    .line 949
    goto :goto_0

    .line 966
    .restart local v18    # "i":I
    :cond_0
    if-eqz v20, :cond_1

    .line 967
    invoke-virtual/range {v20 .. v20}, Landroid/view/MotionEvent;->recycle()V

    .line 968
    const/16 v20, 0x0

    .line 969
    const/16 v21, 0x0

    .line 970
    const/16 v19, 0x0

    .line 974
    :cond_1
    return-void

    .line 966
    :catchall_0
    move-exception v2

    if-eqz v20, :cond_2

    .line 967
    invoke-virtual/range {v20 .. v20}, Landroid/view/MotionEvent;->recycle()V

    .line 968
    const/16 v20, 0x0

    .line 969
    const/16 v21, 0x0

    .line 970
    const/16 v19, 0x0

    :cond_2
    throw v2

    .line 937
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 760
    const-string v1, "HoverZoomGridUI"

    const-string v2, " onClick  "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 800
    :goto_0
    :pswitch_0
    return-void

    .line 763
    :pswitch_1
    const-string v1, "HoverZoomGridUI"

    const-string v2, "onClick() :: R.id.grid_FmClosed"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v1, :cond_0

    .line 765
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->editCursorPreference(II)V

    .line 768
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->hideMagnifier()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 773
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 774
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->setFMClosedStatus(Z)V

    goto :goto_0

    .line 769
    :catch_0
    move-exception v0

    .line 770
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 778
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_2
    const-string v1, "HoverZoomGridUI"

    const-string v2, "onClick() :: R.id.fm_scrollDown"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    const/4 v1, 0x1

    invoke-direct {p0, v1, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectAccessibilityMotionEvent(IZ)V

    goto :goto_0

    .line 783
    :pswitch_3
    const-string v1, "HoverZoomGridUI"

    const-string v2, "onClick() :: R.id.fm_scrollUp"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    invoke-direct {p0, v4, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectAccessibilityMotionEvent(IZ)V

    goto :goto_0

    .line 788
    :pswitch_4
    const-string v1, "HoverZoomGridUI"

    const-string v2, "onClick() :: R.id.fm_swipeLeft"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    const/4 v1, 0x3

    invoke-direct {p0, v1, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectAccessibilityMotionEvent(IZ)V

    goto :goto_0

    .line 793
    :pswitch_5
    const-string v1, "HoverZoomGridUI"

    const-string v2, "onClick() :: R.id.fm_swipeRight"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    const/4 v1, 0x4

    invoke-direct {p0, v1, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectAccessibilityMotionEvent(IZ)V

    goto :goto_0

    .line 761
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0026
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public removeCue()V
    .locals 2

    .prologue
    .line 1263
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCueShown:Z

    if-eqz v0, :cond_0

    .line 1264
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    .line 1267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCueShown:Z

    .line 1269
    :cond_0
    return-void
.end method

.method public update(FF)V
    .locals 11
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v7, 0x41f00000    # 30.0f

    const/high16 v6, 0x41000000    # 8.0f

    .line 1109
    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoldCursorFlag:Z

    .line 1111
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurX:F

    cmpg-float v5, v5, p1

    if-gtz v5, :cond_5

    .line 1112
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurX:F

    float-to-int v5, v5

    add-int/lit8 v2, v5, -0x64

    .line 1113
    .local v2, "left":I
    float-to-int v5, p1

    add-int/lit8 v3, v5, 0x64

    .line 1119
    .local v3, "right":I
    :goto_0
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurY:F

    cmpg-float v5, v5, p2

    if-gtz v5, :cond_6

    .line 1120
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurY:F

    float-to-int v5, v5

    add-int/lit8 v4, v5, -0x64

    .line 1121
    .local v4, "top":I
    float-to-int v5, p2

    add-int/lit8 v0, v5, 0x64

    .line 1129
    .local v0, "bottom":I
    :goto_1
    :try_start_0
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurX:F

    add-float/2addr v5, v6

    cmpl-float v5, v5, p1

    if-lez v5, :cond_8

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurX:F

    sub-float/2addr v5, v6

    cmpg-float v5, v5, p1

    if-gez v5, :cond_8

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurY:F

    add-float/2addr v5, v6

    cmpl-float v5, v5, p2

    if-lez v5, :cond_8

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurY:F

    sub-float/2addr v5, v6

    cmpg-float v5, v5, p2

    if-gez v5, :cond_8

    .line 1130
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCount_hold:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_7

    .line 1131
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoldCursorFlag:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1145
    :goto_2
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isSecureKeyguardLocked()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1146
    :cond_0
    iget-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isPokeLockEnable:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    if-eqz v5, :cond_1

    .line 1147
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 1148
    :cond_1
    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isPokeLockEnable:Z

    .line 1150
    :cond_2
    cmpl-float v5, p1, v10

    if-nez v5, :cond_3

    cmpl-float v5, p2, v10

    if-eqz v5, :cond_9

    :cond_3
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->isCursorShown()Z

    move-result v5

    if-nez v5, :cond_9

    .line 1151
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    sub-float v6, p1, v7

    sub-float v7, p2, v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->Update(FF)V

    .line 1152
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v5, v9}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->showCursor(Z)V

    .line 1166
    :cond_4
    :goto_3
    const-string v5, "HoverZoomGridUI"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "update mMagnifierSize= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1167
    return-void

    .line 1115
    .end local v0    # "bottom":I
    .end local v2    # "left":I
    .end local v3    # "right":I
    .end local v4    # "top":I
    :cond_5
    float-to-int v5, p1

    add-int/lit8 v2, v5, -0x64

    .line 1116
    .restart local v2    # "left":I
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurX:F

    float-to-int v5, v5

    add-int/lit8 v3, v5, 0x64

    .restart local v3    # "right":I
    goto/16 :goto_0

    .line 1123
    :cond_6
    float-to-int v5, p2

    add-int/lit8 v4, v5, -0x64

    .line 1124
    .restart local v4    # "top":I
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurY:F

    float-to-int v5, v5

    add-int/lit8 v0, v5, 0x64

    .restart local v0    # "bottom":I
    goto/16 :goto_1

    .line 1133
    :cond_7
    :try_start_1
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCount_hold:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCount_hold:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 1141
    :catch_0
    move-exception v1

    .line 1142
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2

    .line 1136
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_8
    :try_start_2
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurX:F

    .line 1137
    iput p2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->pCurY:F

    .line 1138
    const/4 v5, 0x0

    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCount_hold:I

    .line 1139
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoldCursorFlag:Z
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 1154
    :cond_9
    iget-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z

    if-ne v5, v9, :cond_4

    .line 1156
    :try_start_3
    iget-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoldCursorFlag:Z

    if-nez v5, :cond_4

    .line 1157
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    sub-float v6, p1, v7

    sub-float v7, p2, v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->Update(FF)V

    .line 1158
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v5, v2, v4, v3, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->invalidate(IIII)V

    .line 1159
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v5, p1, p2}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 1161
    :catch_1
    move-exception v1

    .line 1162
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public updateCursorFromGesture(FF)V
    .locals 8
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 553
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mPointerSpeed:I

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    const/high16 v6, 0x40400000    # 3.0f

    div-float v3, v4, v6

    .line 555
    .local v3, "mul":F
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->movePtr:Z

    if-eqz v4, :cond_4

    .line 556
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    mul-float v6, v3, p1

    add-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    .line 557
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    mul-float v6, v3, p2

    add-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    .line 559
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceWidth:I

    .line 560
    .local v0, "cursorXposMax":I
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDeviceheight:I

    .line 562
    .local v1, "cursorYposMax":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    int-to-float v6, v0

    cmpl-float v4, v4, v6

    if-lez v4, :cond_5

    int-to-float v4, v0

    :goto_0
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    .line 564
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    int-to-float v6, v1

    cmpl-float v4, v4, v6

    if-lez v4, :cond_7

    int-to-float v5, v1

    :cond_0
    :goto_1
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    .line 567
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z

    if-ne v4, v7, :cond_1

    .line 569
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    invoke-virtual {v4, v5, v6}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorOnTop()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorBottom()Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 577
    :cond_3
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorChange:Z

    if-nez v4, :cond_8

    .line 578
    invoke-direct {p0, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->changeCursorStatus(Z)V

    .line 590
    .end local v0    # "cursorXposMax":I
    .end local v1    # "cursorYposMax":I
    :cond_4
    :goto_3
    return-void

    .line 562
    .restart local v0    # "cursorXposMax":I
    .restart local v1    # "cursorYposMax":I
    :cond_5
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_6

    move v4, v5

    goto :goto_0

    :cond_6
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    goto :goto_0

    .line 564
    :cond_7
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_0

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    goto :goto_1

    .line 570
    :catch_0
    move-exception v2

    .line 571
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 580
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_8
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->update(FF)V

    goto :goto_3

    .line 583
    :cond_9
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorChange:Z

    if-eqz v4, :cond_a

    .line 584
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->changeCursorStatus(Z)V

    goto :goto_3

    .line 586
    :cond_a
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->update(FF)V

    goto :goto_3
.end method

.method public updateStatusBarStatus()V
    .locals 2

    .prologue
    .line 593
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v0

    .line 594
    .local v0, "isQuickPanelOpen":Z
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorBottom()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorOnTop()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    .line 595
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->changeCursorStatus(Z)V

    .line 599
    :goto_0
    return-void

    .line 597
    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->changeCursorStatus(Z)V

    goto :goto_0
.end method
