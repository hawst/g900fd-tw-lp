.class public Lcom/samsung/android/app/assistantmenu/serviceframework/BrightnessManager;
.super Ljava/lang/Object;
.source "BrightnessManager.java"


# static fields
.field private static final MAX_BRIGHTNESS:I = 0xff

.field private static final MIN_BRIGHTNESS:I = 0x14


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getScreenAutoBrightness(Landroid/content/ContentResolver;)I
    .locals 3
    .param p0, "aContentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 57
    .local v0, "CurrentBrightnessValue":I
    :try_start_0
    const-string v2, "auto_brightness_detail"

    invoke-static {p0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 62
    :goto_0
    return v0

    .line 59
    :catch_0
    move-exception v1

    .line 60
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getScreenBrightness(Landroid/content/ContentResolver;)I
    .locals 3
    .param p0, "aContentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 45
    .local v0, "CurrentBrightnessValue":I
    :try_start_0
    const-string v2, "screen_brightness"

    invoke-static {p0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 50
    :goto_0
    return v0

    .line 47
    :catch_0
    move-exception v1

    .line 48
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isAutoBrightness(Landroid/content/ContentResolver;)Z
    .locals 4
    .param p0, "aContentResolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v2, 0x1

    .line 25
    const/4 v0, 0x0

    .line 27
    .local v0, "automicBrightness":Z
    :try_start_0
    const-string v3, "screen_brightness_mode"

    invoke-static {p0, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v3, v2, :cond_0

    move v0, v2

    .line 32
    :goto_0
    return v0

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 29
    :catch_0
    move-exception v1

    .line 30
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static saveAutoBrightness(Landroid/content/ContentResolver;I)V
    .locals 1
    .param p0, "aContentResolver"    # Landroid/content/ContentResolver;
    .param p1, "brightness"    # I

    .prologue
    .line 132
    const-string v0, "auto_brightness_detail"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 134
    return-void
.end method

.method public static saveBrightness(Landroid/content/ContentResolver;I)V
    .locals 1
    .param p0, "aContentResolver"    # Landroid/content/ContentResolver;
    .param p1, "brightness"    # I

    .prologue
    .line 128
    const-string v0, "screen_brightness"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 129
    return-void
.end method

.method public static setBrightness(IZ)V
    .locals 0
    .param p0, "brightness"    # I
    .param p1, "autoBrightness"    # Z

    .prologue
    .line 97
    return-void
.end method

.method public static startAutoBrightness(Landroid/content/ContentResolver;)V
    .locals 2
    .param p0, "aContentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 117
    const-string v0, "screen_brightness_mode"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 119
    return-void
.end method

.method public static stopAutoBrightness(Landroid/content/ContentResolver;)V
    .locals 2
    .param p0, "aContentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 106
    const-string v0, "screen_brightness_mode"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 108
    return-void
.end method
