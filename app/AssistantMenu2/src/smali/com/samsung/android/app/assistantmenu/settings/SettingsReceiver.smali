.class public Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SettingsReceiver.java"


# static fields
.field private static final ASSISTANTMENU_SETTINGS_RESET:Ljava/lang/String; = "com.samsung.action.ASSISTANTMENU_SETTINGS_RESET"

.field private static final TAG:Ljava/lang/String; = "SettingsReceiver"


# instance fields
.field private final PREFIX_LOWER_KEYSTR:Ljava/lang/String;

.field private mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

.field private mContext:Landroid/content/Context;

.field private mGridViewItemListLower:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;",
            ">;"
        }
    .end annotation
.end field

.field private mGridViewItemListUpper:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;",
            ">;"
        }
    .end annotation
.end field

.field private mIconImgMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIconStringMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNotPrefsDefaltValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPrefsDefaltValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 21
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListUpper:Ljava/util/ArrayList;

    .line 22
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListLower:Ljava/util/ArrayList;

    .line 23
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mPrefsDefaltValue:Ljava/util/HashMap;

    .line 24
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    .line 25
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 26
    const-string v0, "00"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->PREFIX_LOWER_KEYSTR:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mIconImgMap:Ljava/util/HashMap;

    .line 28
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mIconStringMap:Ljava/util/HashMap;

    .line 29
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mResources:Landroid/content/res/Resources;

    return-void
.end method

.method private MakeSettingItem(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;
    .locals 5
    .param p1, "act"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .prologue
    .line 99
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mIconImgMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 101
    .local v0, "img":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mIconStringMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 103
    .local v1, "name":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 105
    .local v3, "strname":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-direct {v2, v0, p1, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;-><init>(ILcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;Ljava/lang/String;)V

    .line 107
    .local v2, "settingItem":Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;
    return-object v2
.end method

.method private SaveSettings()V
    .locals 6

    .prologue
    .line 80
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 81
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 82
    .local v3, "upperItemSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 83
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    :cond_0
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyMenuItemTotalCount:Ljava/lang/String;

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 88
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 89
    .local v2, "lowerItemSize":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 90
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 93
    :cond_1
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyLowerItemTotalCount:Ljava/lang/String;

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 94
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 95
    return-void
.end method


# virtual methods
.method public ResetSettings()V
    .locals 4

    .prologue
    .line 60
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 61
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 64
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 65
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 66
    .local v0, "act":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->MakeSettingItem(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    .end local v0    # "act":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 71
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 72
    .restart local v0    # "act":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->MakeSettingItem(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 74
    .end local v0    # "act":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 76
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->SaveSettings()V

    .line 77
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 34
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "action":Ljava/lang/String;
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mContext:Landroid/content/Context;

    .line 36
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 41
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mIconImgMap:Ljava/util/HashMap;

    .line 42
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mIconStringMap:Ljava/util/HashMap;

    .line 43
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mResources:Landroid/content/res/Resources;

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListUpper:Ljava/util/ArrayList;

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mGridViewItemListLower:Ljava/util/ArrayList;

    .line 50
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mPrefsDefaltValue:Ljava/util/HashMap;

    .line 51
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    .line 53
    if-eqz v0, :cond_0

    const-string v1, "com.samsung.action.ASSISTANTMENU_SETTINGS_RESET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsReceiver;->ResetSettings()V

    .line 56
    :cond_0
    return-void
.end method
