.class public Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
.super Landroid/app/Activity;
.source "SettingsMenuEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$11;,
        Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;,
        Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;
    }
.end annotation


# instance fields
.field private final EVENT_AFTER_ANIMATION_END:I

.field private final EVENT_GRIDVIEW_SCROLL:I

.field private final GRIDVIEW_DRAG:I

.field private final GRIDVIEW_LANDSCAPE_COLUMN_ITEM_COUNT:I

.field private final GRIDVIEW_PORTRAIT_COLUMN_ITEM_COUNT:I

.field private final GRIDVIEW_TOUCH:I

.field private final NONE:I

.field private final PREFIX_LOWER_KEYSTR:Ljava/lang/String;

.field private final START_SCROLL_TIME:I

.field private final TAG:Ljava/lang/String;

.field private mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

.field private mControlBar:Landroid/widget/ImageView;

.field private mDialog:Landroid/app/AlertDialog;

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

.field private mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

.field private mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

.field private mGridViewItemListLower:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;",
            ">;"
        }
    .end annotation
.end field

.field private mGridViewItemListUpper:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;",
            ">;"
        }
    .end annotation
.end field

.field public mGridViewLower:Landroid/widget/GridView;

.field private mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

.field private final mGridViewTouchListener:Landroid/view/View$OnTouchListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field public mGridViewUpper:Landroid/widget/GridView;

.field private mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

.field private mIconImgMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIconStringMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsTranslateAnimationEnd:Z

.field private mIsWidgetVisible:Z

.field private mIsdetectedUpper:Z

.field private mLowerDetectedItemId:I

.field private mNewLowerDetectedItemId:I

.field private mNewUpperDetectedItemId:I

.field private mNotPrefsDefaltValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPreDetectedItemId:I

.field private mPrePositionX:F

.field private mPrePositionY:F

.field private mPrefsDefaltValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResources:Landroid/content/res/Resources;

.field private mScrollDirection:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

.field public mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

.field private final mSettingsMenuEditorHandler:Landroid/os/Handler;

.field private mTotalLowerItemCount:I

.field private mTotalUpperItemCount:I

.field mTouchSyncObj:Ljava/lang/Object;

.field private mTranslateAnimation:Landroid/view/animation/Animation;

.field mTranslateLowerGridViwAnimationListner:Landroid/view/animation/Animation$AnimationListener;

.field mTranslateUpperGridViwAnimationListner:Landroid/view/animation/Animation$AnimationListener;

.field private mUpperDetectedItemId:I

.field private mWdigetItemBmpX:F

.field private mWdigetItemBmpY:F

.field private mWidgetImageView:Landroid/widget/ImageView;

.field private mWidgetItemBmp:Landroid/graphics/Bitmap;

.field private mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    const-string v0, "SettingsMenuEditorActivity"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->TAG:Ljava/lang/String;

    .line 49
    const-string v0, "00"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->PREFIX_LOWER_KEYSTR:Ljava/lang/String;

    .line 51
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GRIDVIEW_PORTRAIT_COLUMN_ITEM_COUNT:I

    .line 53
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GRIDVIEW_LANDSCAPE_COLUMN_ITEM_COUNT:I

    .line 55
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GRIDVIEW_TOUCH:I

    .line 57
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GRIDVIEW_DRAG:I

    .line 59
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->NONE:I

    .line 61
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDisplayWidth:I

    .line 63
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDisplayHeight:I

    .line 65
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTotalUpperItemCount:I

    .line 67
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTotalLowerItemCount:I

    .line 69
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    .line 71
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    .line 73
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    .line 75
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    .line 81
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    .line 83
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    .line 85
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    .line 89
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    .line 91
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    .line 93
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    .line 95
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    .line 97
    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsdetectedUpper:Z

    .line 99
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPreDetectedItemId:I

    .line 102
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    .line 105
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    .line 107
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIconImgMap:Ljava/util/HashMap;

    .line 109
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIconStringMap:Ljava/util/HashMap;

    .line 111
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrefsDefaltValue:Ljava/util/HashMap;

    .line 113
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    .line 115
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 117
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    .line 119
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    .line 121
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    .line 123
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    .line 125
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mResources:Landroid/content/res/Resources;

    .line 127
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWindowManager:Landroid/view/WindowManager;

    .line 129
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 132
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    .line 134
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    .line 136
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDialog:Landroid/app/AlertDialog;

    .line 138
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F

    .line 140
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F

    .line 146
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    .line 148
    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z

    .line 154
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->EVENT_GRIDVIEW_SCROLL:I

    .line 156
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->EVENT_AFTER_ANIMATION_END:I

    .line 158
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->START_SCROLL_TIME:I

    .line 160
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;->SCROLL_UP:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mScrollDirection:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    .line 162
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$1;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;

    .line 358
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewTouchListener:Landroid/view/View$OnTouchListener;

    .line 495
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$9;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateUpperGridViwAnimationListner:Landroid/view/animation/Animation$AnimationListener;

    .line 518
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateLowerGridViwAnimationListner:Landroid/view/animation/Animation$AnimationListener;

    .line 1422
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTouchSyncObj:Ljava/lang/Object;

    return-void
.end method

.method private AfterTranslateAnimationEnd()V
    .locals 3

    .prologue
    .line 484
    const-string v0, "SettingsMenuEditorActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] AfterTranslateAnimationEnd()+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z

    if-nez v0, :cond_0

    .line 487
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z

    .line 489
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 490
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 493
    :cond_0
    return-void
.end method

.method private AssignDragItem()V
    .locals 5

    .prologue
    const v4, 0x7f0d0081

    const v3, 0x7f020150

    const/4 v2, -0x1

    .line 935
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    if-eq v0, v2, :cond_0

    .line 936
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 942
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    if-eq v0, v2, :cond_1

    .line 943
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 944
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 949
    :cond_1
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPreDetectedItemId:I

    .line 951
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 952
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 954
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V

    .line 956
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->SaveSettings()V

    .line 957
    return-void
.end method

.method private ChangelayoutByLine(ZI)V
    .locals 8
    .param p1, "isInitialize"    # Z
    .param p2, "type"    # I

    .prologue
    const v7, 0x7f090042

    const/4 v6, 0x3

    const/4 v5, 0x2

    const v4, 0x7f090044

    const/4 v3, 0x1

    .line 785
    const/4 v1, 0x0

    .line 787
    .local v1, "x":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->getCount()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetGridViewLine(I)I

    move-result v0

    .line 789
    .local v0, "line":I
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v2

    if-nez v2, :cond_2

    .line 791
    if-ne v0, v3, :cond_1

    .line 793
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 842
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 843
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 846
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDisplayHeight:I

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f09005c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetActionBarHeight()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v3, v1

    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetTopMargin()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetControlBarHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 863
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 864
    :goto_1
    return-void

    .line 796
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 800
    :cond_2
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 801
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 802
    if-ne v0, v3, :cond_3

    .line 803
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090043

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 805
    :cond_3
    if-ne v0, v5, :cond_4

    .line 806
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090045

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 808
    :cond_4
    if-ne v0, v6, :cond_5

    .line 809
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090047

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 811
    :cond_5
    const/4 v2, 0x4

    if-ne v0, v2, :cond_6

    .line 812
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 814
    :cond_6
    const/4 v2, 0x5

    if-ne v0, v2, :cond_7

    .line 815
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090049

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto/16 :goto_0

    .line 818
    :cond_7
    const-string v2, "SettingsMenuEditorActivity"

    const-string v3, "ChangelayoutByLine else"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 822
    :cond_8
    if-ne v0, v3, :cond_9

    .line 823
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto/16 :goto_0

    .line 825
    :cond_9
    if-ne v0, v5, :cond_a

    .line 826
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto/16 :goto_0

    .line 828
    :cond_a
    if-ne v0, v6, :cond_b

    .line 829
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 831
    if-ne p2, v3, :cond_0

    .line 832
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v3}, Landroid/widget/GridView;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->smoothScrollToPosition(I)V

    goto/16 :goto_0

    .line 835
    :cond_b
    const-string v2, "SettingsMenuEditorActivity"

    const-string v3, "ChangelayoutByLine else"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private CheckIntersects(Landroid/widget/GridView;Landroid/widget/LinearLayout$LayoutParams;)I
    .locals 22
    .param p1, "gridview"    # Landroid/widget/GridView;
    .param p2, "layoutParams"    # Landroid/widget/LinearLayout$LayoutParams;

    .prologue
    .line 1089
    const/4 v7, -0x1

    .line 1091
    .local v7, "itemIndex":I
    invoke-virtual/range {p1 .. p1}, Landroid/widget/GridView;->getWidth()I

    move-result v5

    .line 1092
    .local v5, "gapMin":I
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 1094
    .local v14, "rectWidget":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/ImageView;->getWidth()I

    move-result v20

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/ImageView;->getHeight()I

    move-result v21

    add-int v20, v20, v21

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1099
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 1102
    .local v9, "rectItem":Landroid/graphics/Rect;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/widget/GridView;->getCount()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_0

    .line 1103
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1104
    .local v8, "itemView":Landroid/view/View;
    if-nez v8, :cond_1

    .line 1156
    .end local v8    # "itemView":Landroid/view/View;
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v17

    add-int v17, v17, v7

    return v17

    .line 1107
    .restart local v8    # "itemView":Landroid/view/View;
    :cond_1
    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v17

    move/from16 v0, v17

    float-to-int v12, v0

    .line 1110
    .local v12, "rectItemX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_3

    .line 1111
    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/app/ActionBar;->getHeight()I

    move-result v18

    add-int v17, v17, v18

    move-object/from16 v0, p2

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    move/from16 v18, v0

    add-int v13, v17, v18

    .line 1120
    .local v13, "rectItemY":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getWidth()I

    move-result v17

    add-int v17, v17, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getHeight()I

    move-result v18

    add-int v18, v18, v13

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v12, v13, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 1123
    invoke-static {v14, v9}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1127
    const-string v17, "SettingsMenuEditorActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "FindNearItem()_intersects_ItemIndex: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    move-result v15

    .line 1130
    .local v15, "rectWidgetCX":I
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v16

    .line 1132
    .local v16, "rectWidgetCY":I
    invoke-virtual {v9}, Landroid/graphics/Rect;->centerX()I

    move-result v10

    .line 1133
    .local v10, "rectItemCX":I
    invoke-virtual {v9}, Landroid/graphics/Rect;->centerY()I

    move-result v11

    .line 1137
    .local v11, "rectItemCY":I
    sub-int v17, v15, v10

    sub-int v18, v15, v10

    mul-int v17, v17, v18

    sub-int v18, v16, v11

    sub-int v19, v16, v11

    mul-int v18, v18, v19

    add-int v17, v17, v18

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v4, v0

    .line 1143
    .local v4, "gap":I
    if-le v5, v4, :cond_2

    .line 1144
    move v5, v4

    .line 1145
    move v7, v6

    .line 1102
    .end local v4    # "gap":I
    .end local v10    # "rectItemCX":I
    .end local v11    # "rectItemCY":I
    .end local v15    # "rectWidgetCX":I
    .end local v16    # "rectWidgetCY":I
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 1115
    .end local v13    # "rectItemY":I
    :cond_3
    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/app/ActionBar;->getHeight()I

    move-result v18

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/GridView;->getHeight()I

    move-result v18

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    move/from16 v18, v0

    add-int v13, v17, v18

    .restart local v13    # "rectItemY":I
    goto/16 :goto_1
.end method

.method private FindNearItem()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1181
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    .line 1182
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    .line 1184
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->IsLowerGridViewArea()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1186
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->CheckIntersects(Landroid/widget/GridView;Landroid/widget/LinearLayout$LayoutParams;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    .line 1187
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    if-eq v0, v1, :cond_3

    .line 1189
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1190
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1191
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1193
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    if-ne v0, v3, :cond_2

    .line 1195
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    .line 1204
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    .line 1234
    :cond_0
    :goto_1
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    .line 1289
    :cond_1
    :goto_2
    return-void

    .line 1198
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    .line 1200
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    goto :goto_0

    .line 1206
    :cond_3
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewUpperDetectedItemId:I

    if-ne v0, v3, :cond_0

    .line 1208
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1209
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1210
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1211
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    if-eq v0, v3, :cond_4

    .line 1213
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    .line 1215
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    goto :goto_1

    .line 1218
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    .line 1220
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    .line 1222
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 1224
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 1225
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    goto/16 :goto_1

    .line 1227
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    goto/16 :goto_1

    .line 1237
    :cond_6
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPreDetectedItemId:I

    if-ne v0, v3, :cond_1

    .line 1239
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->CheckIntersects(Landroid/widget/GridView;Landroid/widget/LinearLayout$LayoutParams;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    .line 1240
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    if-eq v0, v3, :cond_9

    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    if-eq v0, v1, :cond_9

    .line 1243
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1244
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1245
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1247
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    if-ne v0, v3, :cond_8

    .line 1249
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    .line 1258
    :goto_3
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    .line 1286
    :cond_7
    :goto_4
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    goto/16 :goto_2

    .line 1253
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    .line 1255
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    goto :goto_3

    .line 1260
    :cond_9
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNewLowerDetectedItemId:I

    if-ne v0, v3, :cond_7

    .line 1262
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1263
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1264
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1265
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    if-eq v0, v3, :cond_a

    .line 1267
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    .line 1269
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    goto :goto_4

    .line 1272
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    .line 1274
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    .line 1275
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 1277
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_b

    .line 1278
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    goto/16 :goto_4

    .line 1280
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ItemAnimationRun(Landroid/widget/GridView;II)V

    goto/16 :goto_4
.end method

.method private GetControlBarHeight()I
    .locals 2

    .prologue
    .line 1714
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private GetGridViewLine(I)I
    .locals 3
    .param p1, "gridViewItemCount"    # I

    .prologue
    const/16 v2, 0xc

    .line 1387
    const/4 v0, -0x1

    .line 1389
    .local v0, "rowLine":I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1390
    if-ltz p1, :cond_0

    .line 1391
    const/4 v1, 0x4

    if-gt p1, v1, :cond_1

    .line 1392
    const/4 v0, 0x1

    .line 1418
    :cond_0
    :goto_0
    return v0

    .line 1393
    :cond_1
    const/16 v1, 0x8

    if-gt p1, v1, :cond_2

    .line 1394
    const/4 v0, 0x2

    goto :goto_0

    .line 1395
    :cond_2
    if-gt p1, v2, :cond_3

    .line 1396
    const/4 v0, 0x3

    goto :goto_0

    .line 1397
    :cond_3
    const/16 v1, 0x10

    if-gt p1, v1, :cond_4

    .line 1398
    const/4 v0, 0x4

    goto :goto_0

    .line 1399
    :cond_4
    const/16 v1, 0x14

    if-gt p1, v1, :cond_5

    .line 1400
    const/4 v0, 0x5

    goto :goto_0

    .line 1402
    :cond_5
    const-string v1, "SettingsMenuEditorActivity"

    const-string v2, "GetGridViewLine [GetOrientationState is true] else"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1406
    :cond_6
    if-ltz p1, :cond_0

    .line 1407
    const/4 v1, 0x6

    if-gt p1, v1, :cond_7

    .line 1408
    const/4 v0, 0x1

    goto :goto_0

    .line 1409
    :cond_7
    if-gt p1, v2, :cond_8

    .line 1410
    const/4 v0, 0x2

    goto :goto_0

    .line 1411
    :cond_8
    const/16 v1, 0x12

    if-gt p1, v1, :cond_9

    .line 1412
    const/4 v0, 0x3

    goto :goto_0

    .line 1414
    :cond_9
    const-string v1, "SettingsMenuEditorActivity"

    const-string v2, "GetGridViewLine [GetOrientationState is false] else"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private GetGridViewLowerHeight(I)I
    .locals 3
    .param p1, "gridViewUpperHeight"    # I

    .prologue
    .line 1297
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDisplayHeight:I

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f09005c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetActionBarHeight()I

    move-result v1

    sub-int/2addr v0, v1

    sub-int/2addr v0, p1

    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetTopMargin()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private GetOrientationPortraite()Z
    .locals 2

    .prologue
    .line 1314
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 1315
    .local v0, "orientation":I
    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1316
    :cond_0
    const/4 v1, 0x1

    .line 1319
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private GetTopMargin()I
    .locals 3

    .prologue
    .line 1699
    const/4 v0, 0x0

    .line 1701
    .local v0, "top_margin":I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1702
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1709
    :goto_0
    return v0

    .line 1705
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method private GetWindowSize()V
    .locals 2

    .prologue
    .line 1323
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1324
    .local v0, "displaySize":Landroid/graphics/Point;
    const-string v1, "window"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1326
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDisplayWidth:I

    .line 1327
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDisplayHeight:I

    .line 1328
    return-void
.end method

.method private GridViewScroll(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;)V
    .locals 2
    .param p1, "direction"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    .prologue
    .line 1331
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$11;->$SwitchMap$com$samsung$android$app$assistantmenu$settings$SettingsMenuEditorActivity$SCROLL_MOTION:[I

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1341
    :goto_0
    return-void

    .line 1333
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->smoothScrollByOffset(I)V

    goto :goto_0

    .line 1336
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->smoothScrollByOffset(I)V

    goto :goto_0

    .line 1331
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private Init()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x2

    const/4 v4, 0x1

    .line 558
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mResources:Landroid/content/res/Resources;

    .line 561
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWindowManager:Landroid/view/WindowManager;

    .line 564
    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 567
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    .line 568
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    const v2, 0x7f0a0040

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 569
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 570
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 573
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    .line 575
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    .line 577
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->AddOptionalMenuIfNeed(Landroid/content/Context;)Z

    .line 579
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIconImgMap:Ljava/util/HashMap;

    .line 580
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIconStringMap:Ljava/util/HashMap;

    .line 581
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrefsDefaltValue:Ljava/util/HashMap;

    .line 582
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    .line 584
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyMenuItemTotalCount:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTotalUpperItemCount:I

    .line 586
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyLowerItemTotalCount:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTotalLowerItemCount:I

    .line 589
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    .line 592
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTotalUpperItemCount:I

    if-ge v7, v0, :cond_1

    .line 593
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 594
    .local v8, "position":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v8, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v6

    .line 597
    .local v6, "act":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-eq v6, v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-direct {p0, v6}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->MakeSettingItem(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 604
    .end local v6    # "act":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .end local v8    # "position":Ljava/lang/String;
    :cond_1
    const/4 v7, 0x0

    :goto_1
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTotalLowerItemCount:I

    if-ge v7, v0, :cond_2

    .line 605
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 607
    .restart local v8    # "position":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "00"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v6

    .line 610
    .restart local v6    # "act":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-direct {p0, v6}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->MakeSettingItem(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 604
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 614
    .end local v6    # "act":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .end local v8    # "position":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 616
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->InitUI()V

    .line 618
    invoke-direct {p0, v4, v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V

    .line 620
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d3

    const/16 v4, 0x28

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 626
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 627
    return-void
.end method

.method private InitUI()V
    .locals 4

    .prologue
    const v3, 0x7f03001c

    .line 233
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetWindowSize()V

    .line 235
    const v0, 0x7f0d007d

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    .line 236
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    .line 238
    const v0, 0x7f0d007e

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    .line 239
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    .line 241
    const v0, 0x7f0d007f

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    .line 242
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    const v1, 0x7f020062

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 244
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;->MINUS_IMAGE_MODE:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    invoke-direct {v0, p0, v3, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    .line 247
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;->PLUS_IMAGE_MODE:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    invoke-direct {v0, p0, v3, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    .line 251
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 252
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 253
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$2;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 265
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 266
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 267
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$3;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 279
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$4;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 294
    return-void
.end method

.method private IsLowerGridViewArea()Z
    .locals 4

    .prologue
    .line 1164
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    add-int v1, v2, v3

    .line 1165
    .local v1, "widgetBottomY":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v3}, Landroid/widget/GridView;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v3}, Landroid/widget/GridView;->getY()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v0, v2

    .line 1168
    .local v0, "gridViewLowerY":I
    if-le v1, v0, :cond_0

    .line 1169
    const/4 v2, 0x1

    .line 1171
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private IsScrollArea()Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1349
    const/4 v3, 0x0

    .line 1351
    .local v3, "result":Z
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1352
    .local v2, "rectWidget":Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1353
    .local v1, "rectControlbar":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1355
    .local v0, "rectActionbar":Landroid/graphics/Rect;
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    float-to-int v6, v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    float-to-int v7, v7

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 1360
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getX()F

    move-result v4

    float-to-int v4, v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getY()F

    move-result v5

    float-to-int v5, v5

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6}, Landroid/app/ActionBar;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getX()F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getY()F

    move-result v7

    float-to-int v7, v7

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 1365
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDisplayWidth:I

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->getHeight()I

    move-result v5

    invoke-virtual {v0, v9, v9, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1367
    invoke-virtual {v2, v1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1368
    sget-object v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;->SCROLL_UP:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mScrollDirection:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    .line 1369
    const/4 v3, 0x1

    .line 1377
    :goto_0
    const-string v4, "SettingsMenuEditorActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IsScrollArea:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1378
    return v3

    .line 1370
    :cond_0
    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1371
    sget-object v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;->SCROLL_DOWN:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mScrollDirection:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    .line 1372
    const/4 v3, 0x1

    goto :goto_0

    .line 1374
    :cond_1
    const-string v4, "SettingsMenuEditorActivity"

    const-string v5, "IsScrollArea : else"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ItemAnimationRun(Landroid/widget/GridView;II)V
    .locals 11
    .param p1, "gridView"    # Landroid/widget/GridView;
    .param p2, "startPosition"    # I
    .param p3, "endPosition"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const v8, 0x7f09004c

    const v7, 0x7f09004b

    const/4 v6, 0x0

    .line 968
    invoke-virtual {p1}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v0

    .line 969
    .local v0, "firstVisiblePostion":I
    if-eqz v0, :cond_0

    .line 970
    sub-int/2addr p2, v0

    .line 971
    sub-int/2addr p3, v0

    .line 977
    :cond_0
    if-le p3, p2, :cond_7

    .line 978
    add-int/lit8 v1, p2, 0x1

    .local v1, "i":I
    :goto_0
    if-gt v1, p3, :cond_e

    .line 980
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 981
    rem-int/lit8 v2, v1, 0x4

    if-eqz v2, :cond_3

    .line 982
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-direct {v2, v6, v3, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    .line 1008
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1011
    if-ne v1, p3, :cond_2

    .line 1012
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1013
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1016
    :cond_1
    iput-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z

    .line 1017
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateUpperGridViwAnimationListner:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1019
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v10, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1022
    :cond_2
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1023
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 978
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 986
    :cond_3
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09004d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    mul-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    invoke-direct {v2, v6, v3, v6, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    goto :goto_1

    .line 994
    :cond_4
    rem-int/lit8 v2, v1, 0x6

    if-eqz v2, :cond_5

    .line 995
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-direct {v2, v6, v3, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    goto :goto_1

    .line 999
    :cond_5
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x5

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    mul-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    invoke-direct {v2, v6, v3, v6, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    goto/16 :goto_1

    .line 1025
    :cond_6
    const-string v2, "SettingsMenuEditorActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] gridview.getChildAt(i) null!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1029
    .end local v1    # "i":I
    :cond_7
    move v1, p3

    .restart local v1    # "i":I
    :goto_3
    add-int/lit8 v2, p2, -0x1

    if-gt v1, v2, :cond_e

    .line 1031
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1032
    add-int/lit8 v2, v1, 0x1

    rem-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_a

    .line 1033
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v2, v6, v3, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    .line 1057
    :goto_4
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1060
    add-int/lit8 v2, p2, -0x1

    if-ne v1, v2, :cond_9

    .line 1061
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 1062
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1065
    :cond_8
    iput-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z

    .line 1066
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateUpperGridViwAnimationListner:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1068
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v10, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1071
    :cond_9
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 1072
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1029
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1037
    :cond_a
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09004d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v6, v3, v6, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    goto :goto_4

    .line 1044
    :cond_b
    add-int/lit8 v2, v1, 0x1

    rem-int/lit8 v2, v2, 0x6

    if-eqz v2, :cond_c

    .line 1045
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v2, v6, v3, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    goto :goto_4

    .line 1049
    :cond_c
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, -0x5

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v6, v3, v6, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTranslateAnimation:Landroid/view/animation/Animation;

    goto/16 :goto_4

    .line 1074
    :cond_d
    const-string v2, "SettingsMenuEditorActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] gridview.getChildAt(i) null!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 1079
    :cond_e
    return-void
.end method

.method private MakeSettingItem(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;
    .locals 5
    .param p1, "act"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .prologue
    .line 631
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIconImgMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 633
    .local v0, "img":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIconStringMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 635
    .local v1, "name":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 637
    .local v3, "strname":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-direct {v2, v0, p1, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;-><init>(ILcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;Ljava/lang/String;)V

    .line 638
    .local v2, "settingItem":Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;
    return-object v2
.end method

.method private RemoveWidget()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 922
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    if-eqz v0, :cond_0

    .line 923
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 924
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    .line 926
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 927
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setEnabled(Z)V

    .line 930
    :cond_0
    return-void
.end method

.method private ResetSettings()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 667
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 668
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 671
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 672
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 673
    .local v0, "act":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->MakeSettingItem(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 677
    .end local v0    # "act":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 678
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 679
    .restart local v0    # "act":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->MakeSettingItem(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 677
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 682
    .end local v0    # "act":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 684
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 685
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 687
    invoke-direct {p0, v4, v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V

    .line 689
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    iget v3, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-direct {p0, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetGridViewLowerHeight(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 691
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 692
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 694
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->SaveSettings()V

    .line 695
    return-void
.end method

.method private SaveSettings()V
    .locals 6

    .prologue
    .line 646
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 647
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 648
    .local v3, "upperItemSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 649
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 648
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 651
    :cond_0
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyMenuItemTotalCount:Ljava/lang/String;

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 653
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 654
    .local v2, "lowerItemSize":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 655
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "00"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 654
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 658
    :cond_1
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyLowerItemTotalCount:Ljava/lang/String;

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 659
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 660
    return-void
.end method

.method private ShowWidget()V
    .locals 3

    .prologue
    .line 896
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    if-nez v0, :cond_0

    .line 897
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 898
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 900
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 902
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 903
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    .line 904
    const-string v0, "SettingsMenuEditorActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShowWidget() mWidgetPointLayoutParams.x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mWidgetPointLayoutParams.y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    :cond_0
    return-void
.end method

.method private UpdateWidget()V
    .locals 3

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    if-eqz v0, :cond_0

    .line 912
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 913
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 914
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 916
    const-string v0, "SettingsMenuEditorActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpdateWidget() mWidgetLayoutParams.x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mWidgetLayoutParams.y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
    .param p1, "x1"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GridViewScroll(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->AfterTranslateAnimationEnd()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->UpdateWidget()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->IsScrollArea()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mScrollDirection:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->FindNearItem()V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->RemoveWidget()V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->AssignDragItem()V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    return v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->changeLayoutByLowerLine()V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ResetSettings()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    return v0
.end method

.method static synthetic access$616(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;F)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
    .param p1, "x1"    # F

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
    .param p1, "x1"    # F

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F

    return p1
.end method

.method static synthetic access$816(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;F)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
    .param p1, "x1"    # F

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
    .param p1, "x1"    # F

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F

    return p1
.end method

.method private changeLayoutByLowerLine()V
    .locals 5

    .prologue
    .line 867
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v1

    if-nez v1, :cond_0

    .line 868
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->getCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetGridViewLine(I)I

    move-result v0

    .line 870
    .local v0, "line":I
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090042

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/2addr v2, v0

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 872
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 874
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDisplayHeight:I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f09005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetActionBarHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    iget v3, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    sub-int/2addr v2, v3

    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetTopMargin()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetControlBarHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 891
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 893
    .end local v0    # "line":I
    :cond_0
    return-void
.end method


# virtual methods
.method public GetActionBarHeight()I
    .locals 3

    .prologue
    .line 1680
    const/4 v0, 0x0

    .line 1682
    .local v0, "action_bar":I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1683
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1690
    :goto_0
    return v0

    .line 1686
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public GetIsDetectedUpper()Z
    .locals 1

    .prologue
    .line 1676
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsdetectedUpper:Z

    return v0
.end method

.method public GetIsWidgetVisible()Z
    .locals 1

    .prologue
    .line 1672
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    return v0
.end method

.method public GetLowerDetectedItemId()I
    .locals 1

    .prologue
    .line 1668
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    return v0
.end method

.method public GetTranslateAnimationEnd()Z
    .locals 1

    .prologue
    .line 1660
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z

    return v0
.end method

.method public GetUpperDetectedItemId()I
    .locals 1

    .prologue
    .line 1664
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    return v0
.end method

.method public ItemClick(Landroid/view/View;ILcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "imageMode"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    .prologue
    .line 1426
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v5

    .line 1427
    :try_start_0
    sget-object v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$11;->$SwitchMap$com$samsung$android$app$assistantmenu$settings$SettingsMenuEditorActivity$IMAGE_MODE:[I

    invoke-virtual {p3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 1507
    :cond_0
    :goto_0
    monitor-exit v5

    .line 1508
    :goto_1
    return-void

    .line 1429
    :pswitch_0
    const-string v4, "SettingsMenuEditorActivity"

    const-string v6, "OnItemClickListener - mGridViewUpper"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1431
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-le p2, v4, :cond_1

    .line 1432
    const-string v4, "SettingsMenuEditorActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] ItemClick: wrong position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1433
    monitor-exit v5

    goto :goto_1

    .line 1507
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 1435
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v3

    .line 1436
    .local v3, "itemAct":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    sget-object v4, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1441
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    .line 1443
    .local v2, "item":Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1445
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 1447
    const-string v4, "SettingsMenuEditorActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mGridViewUpper.getFirstVisiblePosition() = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mGridViewUpper.getLastVisiblePosition() = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->getLastVisiblePosition()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1454
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z

    move-result v1

    .line 1455
    .local v1, "isPortrait":Z
    if-eqz v1, :cond_4

    const/4 v0, 0x4

    .line 1458
    .local v0, "columnItemCount":I
    :goto_2
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v4}, Landroid/widget/GridView;->getCount()I

    move-result v4

    rem-int/2addr v4, v0

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v4}, Landroid/widget/GridView;->getCount()I

    move-result v4

    if-ge v4, v0, :cond_3

    .line 1461
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v4}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v4}, Landroid/widget/GridView;->getCount()I

    move-result v4

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v6}, Landroid/widget/GridView;->getLastVisiblePosition()I

    move-result v6

    if-ne v4, v6, :cond_3

    .line 1465
    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, v4, v6}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V

    .line 1471
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 1472
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->SaveSettings()V

    goto/16 :goto_0

    .line 1455
    .end local v0    # "columnItemCount":I
    :cond_4
    const/4 v0, 0x6

    goto :goto_2

    .line 1477
    .end local v1    # "isPortrait":Z
    .end local v2    # "item":Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;
    .end local v3    # "itemAct":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    :pswitch_1
    const-string v4, "SettingsMenuEditorActivity"

    const-string v6, "OnItemClickListener - mGridViewLower"

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1481
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-le p2, v4, :cond_5

    .line 1482
    const-string v4, "SettingsMenuEditorActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] ItemClick: wrong position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1483
    monitor-exit v5

    goto/16 :goto_1

    .line 1486
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    .line 1487
    .restart local v2    # "item":Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1489
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 1493
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 1497
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v4}, Landroid/widget/GridView;->getCount()I

    move-result v4

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v6}, Landroid/widget/GridView;->getLastVisiblePosition()I

    move-result v6

    if-eq v4, v6, :cond_6

    .line 1498
    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, v4, v6}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V

    .line 1501
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->SaveSettings()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1427
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public ItemLongClick(Landroid/view/View;ILcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "imageMode"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    .prologue
    const/4 v7, 0x1

    .line 1519
    const-string v3, "SettingsMenuEditorActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[c] ItemLongClick()+"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1521
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v4

    .line 1522
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1523
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 1525
    :cond_0
    const v3, 0x7f0d0081

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1526
    .local v0, "ItemView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->buildDrawingCache()V

    .line 1527
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1529
    .local v1, "copy":Landroid/graphics/Bitmap;
    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    .line 1531
    const/4 v2, 0x0

    .line 1533
    .local v2, "itemAct":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    sget-object v3, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$11;->$SwitchMap$com$samsung$android$app$assistantmenu$settings$SettingsMenuEditorActivity$IMAGE_MODE:[I

    invoke-virtual {p3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 1574
    :goto_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    if-eqz v2, :cond_3

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1576
    :cond_2
    const-string v3, "SettingsMenuEditorActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[c] mGridViewUpperItemLongClickListener() - home/setting:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1578
    iput p2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPreDetectedItemId:I

    .line 1581
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ShowWidget()V

    .line 1583
    monitor-exit v4

    .line 1584
    return v7

    .line 1535
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v2

    .line 1539
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 1540
    iput p2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    .line 1541
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    .line 1542
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsdetectedUpper:Z

    .line 1543
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    .line 1545
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    .line 1546
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v3

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v3, v5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    iget v5, v5, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    int-to-float v5, v5

    add-float/2addr v3, v5

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    .line 1548
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/GridView;->setEnabled(Z)V

    goto :goto_0

    .line 1583
    .end local v0    # "ItemView":Landroid/view/View;
    .end local v1    # "copy":Landroid/graphics/Bitmap;
    .end local v2    # "itemAct":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1552
    .restart local v0    # "ItemView":Landroid/view/View;
    .restart local v1    # "copy":Landroid/graphics/Bitmap;
    .restart local v2    # "itemAct":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    :pswitch_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v2

    .line 1556
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 1557
    iput p2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    .line 1558
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    .line 1559
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsdetectedUpper:Z

    .line 1560
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDragItem:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    .line 1562
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    .line 1563
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v3

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    invoke-virtual {v5}, Landroid/app/ActionBar;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v3, v5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    iget v5, v5, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    int-to-float v5, v5

    add-float/2addr v3, v5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v5}, Landroid/widget/GridView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v3, v5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v3, v5

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    .line 1566
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/GridView;->setEnabled(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1533
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public SetOnTouchActionDown(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1588
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F

    .line 1589
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F

    .line 1590
    return-void
.end method

.method public SetOnTouchActionMove(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    .line 1594
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v2

    .line 1595
    :try_start_0
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    if-eqz v3, :cond_4

    .line 1597
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F

    sub-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F

    .line 1598
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F

    sub-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F

    .line 1600
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F

    .line 1601
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F

    .line 1603
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->UpdateWidget()V

    .line 1607
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->IsScrollArea()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1608
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    if-eq v3, v6, :cond_0

    .line 1609
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1610
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1611
    const v3, 0x7f0d0081

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f020150

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1614
    .end local v0    # "childView":Landroid/view/View;
    :cond_0
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    if-eq v3, v6, :cond_1

    .line 1615
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mLowerDetectedItemId:I

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1616
    .restart local v0    # "childView":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1617
    const v3, 0x7f0d0081

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f020150

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1621
    .end local v0    # "childView":Landroid/view/View;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1622
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mScrollDirection:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    const-wide/16 v6, 0x1f4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1634
    :cond_2
    :goto_0
    monitor-exit v2

    .line 1643
    :goto_1
    return v1

    .line 1627
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1632
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->FindNearItem()V

    goto :goto_0

    .line 1642
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1639
    :cond_4
    :try_start_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F

    .line 1640
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F

    .line 1642
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1643
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public SetOnTouchActionUp(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1647
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1648
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    if-eqz v0, :cond_0

    .line 1649
    const-string v0, "SettingsMenuEditorActivity"

    const-string v2, "mGridViewUpperTouchListener_ACTION_UP"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1651
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->RemoveWidget()V

    .line 1652
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->AssignDragItem()V

    .line 1653
    const/4 v0, 0x1

    monitor-exit v1

    .line 1656
    :goto_0
    return v0

    .line 1655
    :cond_0
    monitor-exit v1

    .line 1656
    const/4 v0, 0x0

    goto :goto_0

    .line 1655
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v1, 0x1

    .line 216
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 218
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z

    if-eqz v0, :cond_0

    .line 219
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->RemoveWidget()V

    .line 220
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->AssignDragItem()V

    .line 222
    :cond_0
    const v0, 0x7f03001b

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->setContentView(I)V

    .line 223
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->InitUI()V

    .line 224
    invoke-direct {p0, v1, v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V

    .line 226
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 182
    const v0, 0x7f03001b

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->setContentView(I)V

    .line 184
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->Init()V

    .line 185
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 298
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 300
    const v1, 0x7f0a0052

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 301
    .local v0, "item":Landroid/view/MenuItem;
    const v1, 0x7f02015b

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 302
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 189
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetImageView:Landroid/widget/ImageView;

    .line 190
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    .line 191
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLower:Landroid/widget/GridView;

    .line 192
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpperParam:Landroid/widget/LinearLayout$LayoutParams;

    .line 193
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewLowerParam:Landroid/widget/LinearLayout$LayoutParams;

    .line 194
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mControlBar:Landroid/widget/ImageView;

    .line 195
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorActionBar:Landroid/app/ActionBar;

    .line 196
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 199
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWidgetItemBmp:Landroid/graphics/Bitmap;

    .line 201
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 203
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListUpper:Ljava/util/ArrayList;

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 208
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewItemListLower:Ljava/util/ArrayList;

    .line 211
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 212
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v5, 0x7f0a0052

    const/4 v3, 0x1

    .line 312
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 355
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 314
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v4, 0x7f070000

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 315
    .local v1, "isLightTheme":Z
    new-instance v0, Landroid/app/AlertDialog$Builder;

    if-eqz v1, :cond_0

    const/4 v2, 0x5

    :goto_1
    invoke-direct {v0, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 319
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 320
    const v2, 0x7f0a0053

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 321
    new-instance v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$5;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 328
    const v2, 0x7f0a0014

    new-instance v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$6;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$6;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    invoke-virtual {v0, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 335
    new-instance v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$7;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$7;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 344
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDialog:Landroid/app/AlertDialog;

    .line 345
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    move v2, v3

    .line 346
    goto :goto_0

    .line 315
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_0
    const/4 v2, 0x4

    goto :goto_1

    .line 349
    .end local v1    # "isLightTheme":Z
    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->finish()V

    move v2, v3

    .line 350
    goto :goto_0

    .line 312
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method
