.class final Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$9;
.super Ljava/lang/Object;
.source "HWkeyControlManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->lockButtonLongTapEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 21

    .prologue
    .line 186
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 187
    .local v4, "now":J
    new-instance v20, Landroid/app/Instrumentation;

    invoke-direct/range {v20 .. v20}, Landroid/app/Instrumentation;-><init>()V

    .line 188
    .local v20, "instrumentation":Landroid/app/Instrumentation;
    new-instance v3, Landroid/view/KeyEvent;

    const/4 v8, 0x0

    const/16 v9, 0x1a

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, -0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x101

    move-wide v6, v4

    invoke-direct/range {v3 .. v15}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    .line 191
    .local v3, "event":Landroid/view/KeyEvent;
    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    .line 193
    const-wide/16 v8, 0x3e8

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 199
    new-instance v7, Landroid/view/KeyEvent;

    const/4 v12, 0x1

    const/16 v13, 0x1a

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x101

    move-wide v8, v4

    move-wide v10, v4

    invoke-direct/range {v7 .. v19}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    .line 202
    .local v7, "upEvent":Landroid/view/KeyEvent;
    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    .line 215
    return-void

    .line 194
    .end local v7    # "upEvent":Landroid/view/KeyEvent;
    :catch_0
    move-exception v2

    .line 195
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
