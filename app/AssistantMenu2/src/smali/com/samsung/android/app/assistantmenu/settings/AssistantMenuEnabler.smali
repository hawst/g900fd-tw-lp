.class public Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;
.super Ljava/lang/Object;
.source "AssistantMenuEnabler.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final PREF_ASSISTANT_NOTI:Ljava/lang/String; = "pref_assistant_noti"

.field public static final PREF_SINGLE_TAP:Ljava/lang/String; = "pref_single_tap"

.field public static final SINGLE_TAP_KEY:Ljava/lang/String; = "single_tap_key"


# instance fields
.field private final ACCESS_CONTROL_ON:I

.field private final TAG:Ljava/lang/String;

.field private final TALKBACK_ON:Z

.field private accesscontrolState:I

.field private final mContext:Landroid/content/Context;

.field private mDialogMessage:Ljava/lang/String;

.field private mDialogTitle:Ljava/lang/String;

.field private mOtherAccessiblityOnDialog:Landroid/app/AlertDialog;

.field private mSingleTapCheckbox:Landroid/preference/CheckBoxPreference;

.field private mSingleTapMode:I

.field private mSingleTapModeDialog:Landroid/app/AlertDialog;

.field private mSwitch:Landroid/widget/Switch;

.field private sideSyncState:I

.field private talkbackOffAction:Ljava/lang/String;

.field private talkbackState:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "switch_"    # Landroid/widget/Switch;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "AssistantMenuEnabler"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->TAG:Ljava/lang/String;

    .line 40
    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->TALKBACK_ON:Z

    .line 42
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->ACCESS_CONTROL_ON:I

    .line 44
    const-string v0, "com.android.settings.action.talkback_off"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackOffAction:Ljava/lang/String;

    .line 60
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapModeDialog:Landroid/app/AlertDialog;

    .line 61
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mOtherAccessiblityOnDialog:Landroid/app/AlertDialog;

    .line 69
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    .line 71
    return-void
.end method

.method private ShowDialog()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 228
    iget-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackState:Z

    if-ne v1, v2, :cond_1

    .line 229
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->accesscontrolState:I

    if-ne v1, v2, :cond_0

    .line 230
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogTitle:Ljava/lang/String;

    .line 231
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogMessage:Ljava/lang/String;

    .line 241
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 242
    .local v0, "isLightTheme":Z
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    const/4 v1, 0x5

    :goto_1
    invoke-direct {v2, v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogTitle:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0048

    new-instance v3, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$6;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0014

    new-instance v3, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$5;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$4;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mOtherAccessiblityOnDialog:Landroid/app/AlertDialog;

    .line 320
    return-void

    .line 233
    .end local v0    # "isLightTheme":Z
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogTitle:Ljava/lang/String;

    .line 234
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogMessage:Ljava/lang/String;

    goto :goto_0

    .line 237
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogTitle:Ljava/lang/String;

    .line 238
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 242
    .restart local v0    # "isLightTheme":Z
    :cond_2
    const/4 v1, 0x4

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapMode:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapMode:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackState:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->accesscontrolState:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackOffAction:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->isSinglePopupNeeded()I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->showSingleTapModeDialog()V

    return-void
.end method

.method public static getEnabledServicesFromSettings(Landroid/content/Context;)Ljava/util/Set;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421
    const/16 v0, 0x3a

    .line 423
    .local v0, "ENABLED_ACCESSIBILITY_SERVICES_SEPARATOR":C
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "enabled_accessibility_services"

    invoke-static {v6, v7}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 425
    .local v5, "enabledServicesSetting":Ljava/lang/String;
    if-nez v5, :cond_1

    .line 426
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    .line 442
    :cond_0
    return-object v4

    .line 429
    :cond_1
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 430
    .local v4, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    new-instance v1, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v6, 0x3a

    invoke-direct {v1, v6}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 431
    .local v1, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v1, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 433
    :cond_2
    :goto_0
    invoke-virtual {v1}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 434
    invoke-virtual {v1}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v2

    .line 435
    .local v2, "componentNameString":Ljava/lang/String;
    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v3

    .line 437
    .local v3, "enabledService":Landroid/content/ComponentName;
    if-eqz v3, :cond_2

    .line 438
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private isSinglePopupNeeded()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "easy_interaction"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapMode:I

    .line 109
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 110
    .local v0, "mAutoAssistPreferences":Landroid/content/SharedPreferences;
    const-string v3, "pref_assistant_noti"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 112
    .local v1, "never_show_assistant_menu_noti":Ljava/lang/Boolean;
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "assistant_menu"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapMode:I

    if-nez v3, :cond_0

    .line 114
    const/4 v2, 0x1

    .line 117
    :cond_0
    return v2
.end method

.method public static isTalkBackEnabled(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 323
    const/16 v1, 0x3a

    .line 324
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string v0, "com.google.android.marvin.talkback"

    .line 325
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    new-instance v6, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v7, 0x3a

    invoke-direct {v6, v7}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 327
    .local v6, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "enabled_accessibility_services"

    invoke-static {v7, v8}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 329
    .local v5, "enabledServicesSetting":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 330
    const-string v5, ""

    .line 333
    :cond_0
    move-object v2, v6

    .line 335
    .local v2, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 337
    :cond_1
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 338
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v3

    .line 339
    .local v3, "componentNameString":Ljava/lang/String;
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    .line 341
    .local v4, "enabledService":Landroid/content/ComponentName;
    if-eqz v4, :cond_1

    .line 342
    const-string v7, "com.google.android.marvin.talkback"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 344
    const/4 v7, 0x1

    .line 347
    .end local v3    # "componentNameString":Ljava/lang/String;
    .end local v4    # "enabledService":Landroid/content/ComponentName;
    :goto_0
    return v7

    :cond_2
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private showSingleTapModeDialog()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 176
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 177
    .local v4, "mSharedPreferences":Landroid/content/SharedPreferences;
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 178
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030019

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 179
    .local v0, "customView":Landroid/view/View;
    const v6, 0x7f0d0079

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 180
    .local v5, "mcheck_assistantmenu":Landroid/widget/CheckBox;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 182
    .local v1, "edit":Landroid/content/SharedPreferences$Editor;
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f070000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 183
    .local v3, "isLightTheme":Z
    new-instance v7, Landroid/app/AlertDialog$Builder;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    const/4 v6, 0x5

    :goto_0
    invoke-direct {v7, v8, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mDialogTitle:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a005f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x104000a

    new-instance v8, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;

    invoke-direct {v8, p0, v1, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;Landroid/content/SharedPreferences$Editor;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0a0014

    new-instance v8, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$2;

    invoke-direct {v8, p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$2;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$1;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$1;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapModeDialog:Landroid/app/AlertDialog;

    .line 225
    return-void

    .line 183
    :cond_0
    const/4 v6, 0x4

    goto :goto_0
.end method

.method public static turnOffTalkBack(Landroid/content/Context;)Z
    .locals 16
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 351
    const/16 v0, 0x3a

    .line 352
    .local v0, "ENABLED_ACCESSIBILITY_SERVICES_SEPARATOR":C
    new-instance v11, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v13, 0x3a

    invoke-direct {v11, v13}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 355
    .local v11, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->getEnabledServicesFromSettings(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v5

    .line 358
    .local v5, "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v13

    if-ne v5, v13, :cond_0

    .line 359
    new-instance v5, Ljava/util/HashSet;

    .end local v5    # "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 362
    .restart local v5    # "enabledServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    :cond_0
    const-string v13, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-static {v13}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v12

    .line 363
    .local v12, "toggledService":Landroid/content/ComponentName;
    const/4 v1, 0x0

    .line 365
    .local v1, "accessibilityEnabled":Z
    invoke-interface {v5, v12}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 367
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 368
    .local v10, "installedServices":Ljava/util/Set;, "Ljava/util/Set<Landroid/content/ComponentName;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    .line 369
    .local v4, "enabledService":Landroid/content/ComponentName;
    invoke-interface {v10, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 371
    const/4 v1, 0x1

    .line 377
    .end local v4    # "enabledService":Landroid/content/ComponentName;
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 382
    .local v6, "enabledServicesBuilder":Ljava/lang/StringBuilder;
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    .line 383
    .restart local v4    # "enabledService":Landroid/content/ComponentName;
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    const/16 v13, 0x3a

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 386
    .end local v4    # "enabledService":Landroid/content/ComponentName;
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    .line 387
    .local v7, "enabledServicesBuilderLength":I
    if-lez v7, :cond_4

    .line 388
    add-int/lit8 v13, v7, -0x1

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 391
    :cond_4
    const/4 v8, 0x0

    .line 392
    .local v8, "enabledServicesSetting":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 393
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "enabled_accessibility_services"

    invoke-static {v13, v14, v8}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 396
    if-eqz v8, :cond_6

    .line 397
    move-object v2, v11

    .line 398
    .local v2, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v8}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 400
    :cond_5
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 401
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v3

    .line 402
    .local v3, "componentNameString":Ljava/lang/String;
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    .line 404
    .restart local v4    # "enabledService":Landroid/content/ComponentName;
    if-eqz v4, :cond_5

    .line 405
    const/4 v1, 0x1

    .line 413
    .end local v2    # "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    .end local v3    # "componentNameString":Ljava/lang/String;
    .end local v4    # "enabledService":Landroid/content/ComponentName;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "accessibility_enabled"

    if-eqz v1, :cond_7

    const/4 v13, 0x1

    :goto_1
    invoke-static {v14, v15, v13}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 416
    const/4 v13, 0x1

    return v13

    .line 413
    :cond_7
    const/4 v13, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 123
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 124
    .local v1, "state":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackState:Z

    .line 125
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "access_control_use"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->accesscontrolState:I

    .line 126
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sidesync_source_connect"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->sideSyncState:I

    .line 128
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->sideSyncState:I

    if-ne v2, v5, :cond_0

    .line 129
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0064

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 130
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 131
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 173
    :goto_0
    return-void

    .line 134
    :cond_0
    if-eqz p2, :cond_8

    if-nez v1, :cond_8

    .line 135
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 137
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v5, :cond_7

    .line 138
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 139
    const-string v2, "AssistantMenuEnabler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Talkback state "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackState:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and Access Control state "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->accesscontrolState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackState:Z

    if-ne v2, v5, :cond_1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->accesscontrolState:I

    if-eq v2, v5, :cond_3

    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackState:Z

    if-ne v2, v5, :cond_2

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->accesscontrolState:I

    if-ne v2, v5, :cond_3

    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->talkbackState:Z

    if-eq v2, v5, :cond_4

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->accesscontrolState:I

    if-ne v2, v5, :cond_4

    .line 143
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->ShowDialog()V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 161
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 145
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_4
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 146
    const-string v2, "AssistantMenuEnabler"

    const-string v3, "switch is on"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->isSinglePopupNeeded()I

    move-result v2

    if-ne v2, v5, :cond_5

    .line 148
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->showSingleTapModeDialog()V

    goto/16 :goto_0

    .line 150
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 151
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapMode:I

    if-nez v2, :cond_6

    .line 152
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "easy_interaction"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 154
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    const-class v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 158
    :cond_7
    const-string v2, "AssistantMenuEnabler"

    const-string v3, "Provider save failed!!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 165
    :cond_8
    if-nez p2, :cond_9

    if-ne v1, v5, :cond_9

    .line 166
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 167
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 168
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    const-class v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 169
    const-string v2, "AssistantMenuEnabler"

    const-string v3, "switch is off"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 171
    :cond_9
    const-string v2, "AssistantMenuEnabler"

    const-string v3, "switch error"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 85
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 75
    return-void
.end method

.method public setSwitch(Landroid/widget/Switch;)V
    .locals 5
    .param p1, "switch_"    # Landroid/widget/Switch;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 88
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 92
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    .line 93
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 95
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "assistant_menu"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 98
    .local v0, "assistantMenu_state":I
    if-ne v0, v4, :cond_1

    .line 99
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 100
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 103
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setSwitchChecked(Z)V
    .locals 1
    .param p1, "mAMEnable"    # Z

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mOtherAccessiblityOnDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mOtherAccessiblityOnDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mOtherAccessiblityOnDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 81
    :cond_1
    return-void
.end method
