.class public final Lcom/samsung/android/app/assistantmenu/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_bar_icon_back:I = 0x7f020000

.field public static final action_bar_icon_more:I = 0x7f020001

.field public static final assistant_brightness_progress_bg:I = 0x7f020002

.field public static final assistant_btn_grid:I = 0x7f020003

.field public static final assistant_btn_minus:I = 0x7f020004

.field public static final assistant_btn_plus:I = 0x7f020005

.field public static final assistant_button:I = 0x7f020006

.field public static final assistant_button_icon_minus:I = 0x7f020007

.field public static final assistant_button_icon_minus_disabled:I = 0x7f020008

.field public static final assistant_button_icon_plus:I = 0x7f020009

.field public static final assistant_button_icon_plus_disabled:I = 0x7f02000a

.field public static final assistant_button_opacity:I = 0x7f02000b

.field public static final assistant_cursor_close_btn_large_off_default:I = 0x7f02000c

.field public static final assistant_cursor_close_btn_large_off_focused:I = 0x7f02000d

.field public static final assistant_cursor_close_btn_large_off_pressed:I = 0x7f02000e

.field public static final assistant_cursor_close_btn_large_on_default:I = 0x7f02000f

.field public static final assistant_cursor_close_btn_large_on_focused:I = 0x7f020010

.field public static final assistant_cursor_close_btn_large_on_pressed:I = 0x7f020011

.field public static final assistant_cursor_close_btn_middle_off_default:I = 0x7f020012

.field public static final assistant_cursor_close_btn_middle_off_focused:I = 0x7f020013

.field public static final assistant_cursor_close_btn_middle_off_pressed:I = 0x7f020014

.field public static final assistant_cursor_close_btn_middle_on_default:I = 0x7f020015

.field public static final assistant_cursor_close_btn_middle_on_focused:I = 0x7f020016

.field public static final assistant_cursor_close_btn_middle_on_pressed:I = 0x7f020017

.field public static final assistant_cursor_close_btn_small_off_default:I = 0x7f020018

.field public static final assistant_cursor_close_btn_small_off_focused:I = 0x7f020019

.field public static final assistant_cursor_close_btn_small_off_pressed:I = 0x7f02001a

.field public static final assistant_cursor_close_btn_small_on_default:I = 0x7f02001b

.field public static final assistant_cursor_close_btn_small_on_focused:I = 0x7f02001c

.field public static final assistant_cursor_close_btn_small_on_pressed:I = 0x7f02001d

.field public static final assistant_cursor_hover_zoom_pointer:I = 0x7f02001e

.field public static final assistant_cursor_large_ic_close:I = 0x7f02001f

.field public static final assistant_cursor_large_ic_down:I = 0x7f020020

.field public static final assistant_cursor_large_ic_left:I = 0x7f020021

.field public static final assistant_cursor_large_ic_move:I = 0x7f020022

.field public static final assistant_cursor_large_ic_right:I = 0x7f020023

.field public static final assistant_cursor_large_ic_toggle:I = 0x7f020024

.field public static final assistant_cursor_large_ic_up:I = 0x7f020025

.field public static final assistant_cursor_left_large_bg:I = 0x7f020026

.field public static final assistant_cursor_left_large_bg_02:I = 0x7f020027

.field public static final assistant_cursor_left_large_bg_stroke:I = 0x7f020028

.field public static final assistant_cursor_left_large_bg_stroke_02:I = 0x7f020029

.field public static final assistant_cursor_left_middle_bg:I = 0x7f02002a

.field public static final assistant_cursor_left_middle_bg_02:I = 0x7f02002b

.field public static final assistant_cursor_left_middle_bg_stroke:I = 0x7f02002c

.field public static final assistant_cursor_left_middle_bg_stroke_02:I = 0x7f02002d

.field public static final assistant_cursor_left_small_bg:I = 0x7f02002e

.field public static final assistant_cursor_left_small_bg_02:I = 0x7f02002f

.field public static final assistant_cursor_left_small_bg_stroke:I = 0x7f020030

.field public static final assistant_cursor_left_small_bg_stroke_02:I = 0x7f020031

.field public static final assistant_cursor_middle_ic_close:I = 0x7f020032

.field public static final assistant_cursor_middle_ic_down:I = 0x7f020033

.field public static final assistant_cursor_middle_ic_left:I = 0x7f020034

.field public static final assistant_cursor_middle_ic_move:I = 0x7f020035

.field public static final assistant_cursor_middle_ic_right:I = 0x7f020036

.field public static final assistant_cursor_middle_ic_toggle:I = 0x7f020037

.field public static final assistant_cursor_middle_ic_up:I = 0x7f020038

.field public static final assistant_cursor_mouse_pointer_l:I = 0x7f020039

.field public static final assistant_cursor_mouse_pointer_quickpanel_l:I = 0x7f02003a

.field public static final assistant_cursor_mouse_pointer_quickpanel_s:I = 0x7f02003b

.field public static final assistant_cursor_mouse_pointer_s:I = 0x7f02003c

.field public static final assistant_cursor_pad_btn_focused:I = 0x7f02003d

.field public static final assistant_cursor_pad_btn_pressed:I = 0x7f02003e

.field public static final assistant_cursor_pointer_cue:I = 0x7f02003f

.field public static final assistant_cursor_right_large_bg:I = 0x7f020040

.field public static final assistant_cursor_right_large_bg_02:I = 0x7f020041

.field public static final assistant_cursor_right_large_bg_stroke:I = 0x7f020042

.field public static final assistant_cursor_right_large_bg_stroke_02:I = 0x7f020043

.field public static final assistant_cursor_right_middle_bg:I = 0x7f020044

.field public static final assistant_cursor_right_middle_bg_02:I = 0x7f020045

.field public static final assistant_cursor_right_middle_bg_stroke:I = 0x7f020046

.field public static final assistant_cursor_right_middle_bg_stroke_02:I = 0x7f020047

.field public static final assistant_cursor_right_small_bg:I = 0x7f020048

.field public static final assistant_cursor_right_small_bg_02:I = 0x7f020049

.field public static final assistant_cursor_right_small_bg_stroke:I = 0x7f02004a

.field public static final assistant_cursor_right_small_bg_stroke_02:I = 0x7f02004b

.field public static final assistant_cursor_small_ic_close:I = 0x7f02004c

.field public static final assistant_cursor_small_ic_down:I = 0x7f02004d

.field public static final assistant_cursor_small_ic_left:I = 0x7f02004e

.field public static final assistant_cursor_small_ic_move:I = 0x7f02004f

.field public static final assistant_cursor_small_ic_right:I = 0x7f020050

.field public static final assistant_cursor_small_ic_toggle:I = 0x7f020051

.field public static final assistant_cursor_small_ic_up:I = 0x7f020052

.field public static final assistant_device_01:I = 0x7f020053

.field public static final assistant_device_02:I = 0x7f020054

.field public static final assistant_device_03:I = 0x7f020055

.field public static final assistant_home_down:I = 0x7f020056

.field public static final assistant_home_up:I = 0x7f020057

.field public static final assistant_icon_mute:I = 0x7f020058

.field public static final assistant_icon_mute_off:I = 0x7f020059

.field public static final assistant_icon_mute_on:I = 0x7f02005a

.field public static final assistant_icon_sound:I = 0x7f02005b

.field public static final assistant_icon_sound_off:I = 0x7f02005c

.field public static final assistant_icon_sound_on:I = 0x7f02005d

.field public static final assistant_icon_vibrate:I = 0x7f02005e

.field public static final assistant_icon_vibrate_off:I = 0x7f02005f

.field public static final assistant_icon_vibrate_on:I = 0x7f020060

.field public static final assistant_magnifier:I = 0x7f020061

.field public static final assistant_menu_edit_bar_normal:I = 0x7f020062

.field public static final assistant_menu_edit_bar_pressed:I = 0x7f020063

.field public static final assistant_menu_edit_btn_create:I = 0x7f020064

.field public static final assistant_menu_edit_btn_delete:I = 0x7f020065

.field public static final assistant_menu_edit_ic_back:I = 0x7f020066

.field public static final assistant_menu_edit_ic_brightness:I = 0x7f020067

.field public static final assistant_menu_edit_ic_cursor:I = 0x7f020068

.field public static final assistant_menu_edit_ic_device_options:I = 0x7f020069

.field public static final assistant_menu_edit_ic_easy_pinch_zoom:I = 0x7f02006a

.field public static final assistant_menu_edit_ic_home:I = 0x7f02006b

.field public static final assistant_menu_edit_ic_lock:I = 0x7f02006c

.field public static final assistant_menu_edit_ic_menu:I = 0x7f02006d

.field public static final assistant_menu_edit_ic_more:I = 0x7f02006e

.field public static final assistant_menu_edit_ic_open_quick_pannel:I = 0x7f02006f

.field public static final assistant_menu_edit_ic_power_off:I = 0x7f020070

.field public static final assistant_menu_edit_ic_quick_function:I = 0x7f020071

.field public static final assistant_menu_edit_ic_recent_app_list:I = 0x7f020072

.field public static final assistant_menu_edit_ic_restart:I = 0x7f020073

.field public static final assistant_menu_edit_ic_rotate_screen:I = 0x7f020074

.field public static final assistant_menu_edit_ic_screen_capture:I = 0x7f020075

.field public static final assistant_menu_edit_ic_setting:I = 0x7f020076

.field public static final assistant_menu_edit_ic_volume:I = 0x7f020077

.field public static final assistant_menu_edit_ic_zoom:I = 0x7f020078

.field public static final assistant_menu_edit_icon_back:I = 0x7f020079

.field public static final assistant_menu_edit_icon_brightness:I = 0x7f02007a

.field public static final assistant_menu_edit_icon_cursor:I = 0x7f02007b

.field public static final assistant_menu_edit_icon_device_options:I = 0x7f02007c

.field public static final assistant_menu_edit_icon_easy_pinch_zoom:I = 0x7f02007d

.field public static final assistant_menu_edit_icon_home:I = 0x7f02007e

.field public static final assistant_menu_edit_icon_lock:I = 0x7f02007f

.field public static final assistant_menu_edit_icon_menu:I = 0x7f020080

.field public static final assistant_menu_edit_icon_more:I = 0x7f020081

.field public static final assistant_menu_edit_icon_open_quick_pannel:I = 0x7f020082

.field public static final assistant_menu_edit_icon_pinch_gesture:I = 0x7f020083

.field public static final assistant_menu_edit_icon_power_off:I = 0x7f020084

.field public static final assistant_menu_edit_icon_quick_function:I = 0x7f020085

.field public static final assistant_menu_edit_icon_recent_app_list:I = 0x7f020086

.field public static final assistant_menu_edit_icon_restart:I = 0x7f020087

.field public static final assistant_menu_edit_icon_rotate_screen:I = 0x7f020088

.field public static final assistant_menu_edit_icon_screen_capture:I = 0x7f020089

.field public static final assistant_menu_edit_icon_screen_recording:I = 0x7f02008a

.field public static final assistant_menu_edit_icon_scroll:I = 0x7f02008b

.field public static final assistant_menu_edit_icon_setting:I = 0x7f02008c

.field public static final assistant_menu_edit_icon_volume:I = 0x7f02008d

.field public static final assistant_menu_edit_icon_zoom:I = 0x7f02008e

.field public static final assistant_menu_ic_auto_brightness:I = 0x7f02008f

.field public static final assistant_menu_ic_back:I = 0x7f020090

.field public static final assistant_menu_ic_brightness:I = 0x7f020091

.field public static final assistant_menu_ic_cursor:I = 0x7f020092

.field public static final assistant_menu_ic_device_options:I = 0x7f020093

.field public static final assistant_menu_ic_easy_pinch_zoom:I = 0x7f020094

.field public static final assistant_menu_ic_home:I = 0x7f020095

.field public static final assistant_menu_ic_lock:I = 0x7f020096

.field public static final assistant_menu_ic_menu:I = 0x7f020097

.field public static final assistant_menu_ic_more:I = 0x7f020098

.field public static final assistant_menu_ic_notification_panel_grid:I = 0x7f020099

.field public static final assistant_menu_ic_notification_panel_list:I = 0x7f02009a

.field public static final assistant_menu_ic_open_quick_pannel:I = 0x7f02009b

.field public static final assistant_menu_ic_power_off:I = 0x7f02009c

.field public static final assistant_menu_ic_quick_function:I = 0x7f02009d

.field public static final assistant_menu_ic_recent_app_list:I = 0x7f02009e

.field public static final assistant_menu_ic_restart:I = 0x7f02009f

.field public static final assistant_menu_ic_rotate_screen:I = 0x7f0200a0

.field public static final assistant_menu_ic_screen_capture:I = 0x7f0200a1

.field public static final assistant_menu_ic_setting:I = 0x7f0200a2

.field public static final assistant_menu_ic_volume:I = 0x7f0200a3

.field public static final assistant_menu_ic_zoom:I = 0x7f0200a4

.field public static final assistant_menu_icon_back:I = 0x7f0200a5

.field public static final assistant_menu_icon_brightness:I = 0x7f0200a6

.field public static final assistant_menu_icon_cursor:I = 0x7f0200a7

.field public static final assistant_menu_icon_device_options:I = 0x7f0200a8

.field public static final assistant_menu_icon_easy_pinch_zoom:I = 0x7f0200a9

.field public static final assistant_menu_icon_home:I = 0x7f0200aa

.field public static final assistant_menu_icon_lock:I = 0x7f0200ab

.field public static final assistant_menu_icon_menu:I = 0x7f0200ac

.field public static final assistant_menu_icon_more:I = 0x7f0200ad

.field public static final assistant_menu_icon_notification_panel_grid:I = 0x7f0200ae

.field public static final assistant_menu_icon_notification_panel_list:I = 0x7f0200af

.field public static final assistant_menu_icon_open_quick_pannel:I = 0x7f0200b0

.field public static final assistant_menu_icon_power_off:I = 0x7f0200b1

.field public static final assistant_menu_icon_recent_app_list:I = 0x7f0200b2

.field public static final assistant_menu_icon_restart:I = 0x7f0200b3

.field public static final assistant_menu_icon_rotate_screen:I = 0x7f0200b4

.field public static final assistant_menu_icon_screen_capture:I = 0x7f0200b5

.field public static final assistant_menu_icon_setting:I = 0x7f0200b6

.field public static final assistant_menu_icon_volume:I = 0x7f0200b7

.field public static final assistant_menu_icon_zoom:I = 0x7f0200b8

.field public static final assistant_pinch:I = 0x7f0200b9

.field public static final assistant_pinch_normal:I = 0x7f0200ba

.field public static final assistant_pinch_press:I = 0x7f0200bb

.field public static final assistant_pinch_pressed:I = 0x7f0200bc

.field public static final assistant_point:I = 0x7f0200bd

.field public static final assistant_point_normal:I = 0x7f0200be

.field public static final assistant_point_press:I = 0x7f0200bf

.field public static final assistant_point_pressed:I = 0x7f0200c0

.field public static final assistant_progress_bg:I = 0x7f0200c1

.field public static final assistant_progress_primary:I = 0x7f0200c2

.field public static final assistant_scrubber_control:I = 0x7f0200c3

.field public static final assistant_scrubber_control_disabled:I = 0x7f0200c4

.field public static final assistant_scrubber_control_pressed:I = 0x7f0200c5

.field public static final assistant_title_close_button:I = 0x7f0200c6

.field public static final assistant_title_close_button_press:I = 0x7f0200c7

.field public static final assistant_title_close_icon:I = 0x7f0200c8

.field public static final assistant_title_ic_close:I = 0x7f0200c9

.field public static final assistant_title_icon_brightness:I = 0x7f0200ca

.field public static final assistant_title_icon_media:I = 0x7f0200cb

.field public static final assistant_title_icon_notification:I = 0x7f0200cc

.field public static final assistant_title_icon_notification_vibrate:I = 0x7f0200cd

.field public static final assistant_title_icon_ringtone:I = 0x7f0200ce

.field public static final assistant_title_icon_rotate:I = 0x7f0200cf

.field public static final assistant_title_icon_system:I = 0x7f0200d0

.field public static final assistant_title_icon_zoom:I = 0x7f0200d1

.field public static final assistant_zoom_progress_bg:I = 0x7f0200d2

.field public static final brightness_auto_checkbox:I = 0x7f0200d3

.field public static final btn_oval_ripple:I = 0x7f0200d4

.field public static final btn_rectangle_ripple:I = 0x7f0200d5

.field public static final control_screen_bg:I = 0x7f0200d6

.field public static final cursor:I = 0x7f0200d7

.field public static final cursor_close_btn_large_default:I = 0x7f0200d8

.field public static final cursor_close_btn_large_focused:I = 0x7f0200d9

.field public static final cursor_close_btn_large_pressed:I = 0x7f0200da

.field public static final cursor_close_btn_middle_default:I = 0x7f0200db

.field public static final cursor_close_btn_middle_focused:I = 0x7f0200dc

.field public static final cursor_close_btn_middle_pressed:I = 0x7f0200dd

.field public static final cursor_close_btn_small_default:I = 0x7f0200de

.field public static final cursor_close_btn_small_focused:I = 0x7f0200df

.field public static final cursor_close_btn_small_pressed:I = 0x7f0200e0

.field public static final cursor_close_btn_state_large:I = 0x7f0200e1

.field public static final cursor_close_btn_state_middle:I = 0x7f0200e2

.field public static final cursor_close_btn_state_small:I = 0x7f0200e3

.field public static final cursor_large_icon_close:I = 0x7f0200e4

.field public static final cursor_large_icon_down:I = 0x7f0200e5

.field public static final cursor_large_icon_left:I = 0x7f0200e6

.field public static final cursor_large_icon_magnifier:I = 0x7f0200e7

.field public static final cursor_large_icon_move:I = 0x7f0200e8

.field public static final cursor_large_icon_right:I = 0x7f0200e9

.field public static final cursor_large_icon_up:I = 0x7f0200ea

.field public static final cursor_left_large_bg:I = 0x7f0200eb

.field public static final cursor_left_large_bg_01:I = 0x7f0200ec

.field public static final cursor_left_large_bg_01_stroke:I = 0x7f0200ed

.field public static final cursor_left_large_bg_02:I = 0x7f0200ee

.field public static final cursor_left_large_bg_stroke:I = 0x7f0200ef

.field public static final cursor_left_large_bg_stroke_02:I = 0x7f0200f0

.field public static final cursor_left_middle_bg:I = 0x7f0200f1

.field public static final cursor_left_middle_bg_01:I = 0x7f0200f2

.field public static final cursor_left_middle_bg_01_stroke:I = 0x7f0200f3

.field public static final cursor_left_middle_bg_02:I = 0x7f0200f4

.field public static final cursor_left_middle_bg_stroke:I = 0x7f0200f5

.field public static final cursor_left_middle_bg_stroke_02:I = 0x7f0200f6

.field public static final cursor_left_small_bg:I = 0x7f0200f7

.field public static final cursor_left_small_bg_01:I = 0x7f0200f8

.field public static final cursor_left_small_bg_01_stroke:I = 0x7f0200f9

.field public static final cursor_left_small_bg_02:I = 0x7f0200fa

.field public static final cursor_left_small_bg_stroke:I = 0x7f0200fb

.field public static final cursor_left_small_bg_stroke_02:I = 0x7f0200fc

.field public static final cursor_middle_icon_close:I = 0x7f0200fd

.field public static final cursor_middle_icon_down:I = 0x7f0200fe

.field public static final cursor_middle_icon_left:I = 0x7f0200ff

.field public static final cursor_middle_icon_magnifier:I = 0x7f020100

.field public static final cursor_middle_icon_move:I = 0x7f020101

.field public static final cursor_middle_icon_right:I = 0x7f020102

.field public static final cursor_middle_icon_up:I = 0x7f020103

.field public static final cursor_mouse_pointer_l:I = 0x7f020104

.field public static final cursor_mouse_pointer_quickpanel_l:I = 0x7f020105

.field public static final cursor_mouse_pointer_quickpanel_s:I = 0x7f020106

.field public static final cursor_mouse_pointer_s:I = 0x7f020107

.field public static final cursor_pad_btn_focus:I = 0x7f020108

.field public static final cursor_pad_btn_press:I = 0x7f020109

.field public static final cursor_pad_btn_state:I = 0x7f02010a

.field public static final cursor_pointer_cue:I = 0x7f02010b

.field public static final cursor_right_large_bg:I = 0x7f02010c

.field public static final cursor_right_large_bg_01:I = 0x7f02010d

.field public static final cursor_right_large_bg_01_stroke:I = 0x7f02010e

.field public static final cursor_right_large_bg_02:I = 0x7f02010f

.field public static final cursor_right_large_bg_stroke:I = 0x7f020110

.field public static final cursor_right_large_bg_stroke_02:I = 0x7f020111

.field public static final cursor_right_middle_bg:I = 0x7f020112

.field public static final cursor_right_middle_bg_01:I = 0x7f020113

.field public static final cursor_right_middle_bg_01_stroke:I = 0x7f020114

.field public static final cursor_right_middle_bg_02:I = 0x7f020115

.field public static final cursor_right_middle_bg_stroke:I = 0x7f020116

.field public static final cursor_right_middle_bg_stroke_02:I = 0x7f020117

.field public static final cursor_right_small_bg:I = 0x7f020118

.field public static final cursor_right_small_bg_01:I = 0x7f020119

.field public static final cursor_right_small_bg_01_stroke:I = 0x7f02011a

.field public static final cursor_right_small_bg_02:I = 0x7f02011b

.field public static final cursor_right_small_bg_stroke:I = 0x7f02011c

.field public static final cursor_right_small_bg_stroke_02:I = 0x7f02011d

.field public static final cursor_small_icon_close:I = 0x7f02011e

.field public static final cursor_small_icon_down:I = 0x7f02011f

.field public static final cursor_small_icon_left:I = 0x7f020120

.field public static final cursor_small_icon_magnifier:I = 0x7f020121

.field public static final cursor_small_icon_move:I = 0x7f020122

.field public static final cursor_small_icon_right:I = 0x7f020123

.field public static final cursor_small_icon_up:I = 0x7f020124

.field public static final device_01:I = 0x7f020125

.field public static final device_02:I = 0x7f020126

.field public static final device_03:I = 0x7f020127

.field public static final grid_ui_menubutton_icon:I = 0x7f020128

.field public static final home_bg:I = 0x7f020129

.field public static final home_bg_btn:I = 0x7f02012a

.field public static final home_bg_btn_focused:I = 0x7f02012b

.field public static final home_bg_btn_pressed:I = 0x7f02012c

.field public static final home_down:I = 0x7f02012d

.field public static final home_down_press:I = 0x7f02012e

.field public static final home_up:I = 0x7f02012f

.field public static final home_up_press:I = 0x7f020130

.field public static final hover_zoom_progress_minus:I = 0x7f020131

.field public static final hover_zoom_progress_minus_focused:I = 0x7f020132

.field public static final hover_zoom_progress_minus_pressed:I = 0x7f020133

.field public static final hover_zoom_progress_plus:I = 0x7f020134

.field public static final hover_zoom_progress_plus_focused:I = 0x7f020135

.field public static final hover_zoom_progress_plus_pressed:I = 0x7f020136

.field public static final ic_launcher:I = 0x7f020137

.field public static final ic_launcher_settings:I = 0x7f020138

.field public static final list_divider_vertical_holo_light:I = 0x7f020139

.field public static final magnifier_0:I = 0x7f02013a

.field public static final magnifier_1:I = 0x7f02013b

.field public static final magnifier_2:I = 0x7f02013c

.field public static final magnifier_3:I = 0x7f02013d

.field public static final magnifier_4:I = 0x7f02013e

.field public static final magnifier_5:I = 0x7f02013f

.field public static final magnifier_6:I = 0x7f020140

.field public static final magnifier_7:I = 0x7f020141

.field public static final magnifier_zoom_minus_button:I = 0x7f020142

.field public static final magnifier_zoom_plus_button:I = 0x7f020143

.field public static final menu_edit_bar:I = 0x7f020144

.field public static final menu_edit_bar_press:I = 0x7f020145

.field public static final menu_edit_bg:I = 0x7f020146

.field public static final menu_edit_btn_add:I = 0x7f020147

.field public static final menu_edit_btn_deiete:I = 0x7f020148

.field public static final menu_edit_focus_bg:I = 0x7f020149

.field public static final menu_edit_focus_h_bg:I = 0x7f02014a

.field public static final menu_edit_h_bg:I = 0x7f02014b

.field public static final menu_edit_press_bg:I = 0x7f02014c

.field public static final menu_edit_press_h_bg:I = 0x7f02014d

.field public static final menu_edit_select_bg:I = 0x7f02014e

.field public static final menu_edit_select_h_bg:I = 0x7f02014f

.field public static final menu_icon_button:I = 0x7f020150

.field public static final oval:I = 0x7f020151

.field public static final pointer:I = 0x7f020152

.field public static final rectangle_button:I = 0x7f020153

.field public static final rectangle_popup_bg:I = 0x7f020154

.field public static final rotate_screen_bg:I = 0x7f020155

.field public static final seek_thumb:I = 0x7f020156

.field public static final seekbar_layout:I = 0x7f020157

.field public static final settings_display_slider_picker:I = 0x7f020158

.field public static final switch_selector_thumb:I = 0x7f020159

.field public static final switch_selector_track:I = 0x7f02015a

.field public static final tw_action_bar_icon_reset_holo_light:I = 0x7f02015b

.field public static final tw_btn_check_off_disabled_holo_light:I = 0x7f02015c

.field public static final tw_btn_check_off_holo_light:I = 0x7f02015d

.field public static final tw_btn_check_off_pressed_holo_light:I = 0x7f02015e

.field public static final tw_btn_check_on_disabled_holo_light:I = 0x7f02015f

.field public static final tw_btn_check_on_holo_light:I = 0x7f020160

.field public static final tw_btn_check_on_pressed_holo_light:I = 0x7f020161

.field public static final tw_btn_default_disabled_holo_light:I = 0x7f020162

.field public static final tw_btn_default_focused_holo_light:I = 0x7f020163

.field public static final tw_btn_default_normal_holo_light:I = 0x7f020164

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f020165

.field public static final tw_btn_next_depth:I = 0x7f020166

.field public static final tw_btn_next_depth_disabled:I = 0x7f020167

.field public static final tw_btn_next_depth_pressed:I = 0x7f020168

.field public static final tw_btn_switch_mtrl:I = 0x7f020169

.field public static final tw_list_divider_holo_light:I = 0x7f02016a

.field public static final tw_pointer_spot_hovering_more:I = 0x7f02016b

.field public static final tw_sound_balance_progress_bg_holo_light:I = 0x7f02016c

.field public static final tw_sub_action_bar_bg_holo_light:I = 0x7f02016d

.field public static final tw_switch_thumb_material_disabled_off:I = 0x7f02016e

.field public static final tw_switch_thumb_material_disabled_on:I = 0x7f02016f

.field public static final tw_switch_thumb_material_off:I = 0x7f020170

.field public static final tw_switch_thumb_material_on:I = 0x7f020171

.field public static final tw_switch_track_mtrl_alpha:I = 0x7f020172

.field public static final volume_bg:I = 0x7f020173

.field public static final volume_icon_minus_button:I = 0x7f020174

.field public static final volume_icon_progress_off:I = 0x7f020175

.field public static final volume_icon_progress_on:I = 0x7f020176

.field public static final zoom_icon_minus_button:I = 0x7f020177


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
