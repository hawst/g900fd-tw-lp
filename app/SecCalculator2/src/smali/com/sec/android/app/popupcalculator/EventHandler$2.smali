.class Lcom/sec/android/app/popupcalculator/EventHandler$2;
.super Landroid/os/Handler;
.source "EventHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/EventHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/EventHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/EventHandler;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/EventHandler$2;->this$0:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 446
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler$2;->this$0:Lcom/sec/android/app/popupcalculator/EventHandler;

    # getter for: Lcom/sec/android/app/popupcalculator/EventHandler;->mIsBackSpaceTouch:Z
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->access$000(Lcom/sec/android/app/popupcalculator/EventHandler;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler$2;->this$0:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->onBackspace()V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler$2;->this$0:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mBackSpaceHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
