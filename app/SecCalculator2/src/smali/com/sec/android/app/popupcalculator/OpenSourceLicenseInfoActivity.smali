.class public Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;
.super Landroid/app/Activity;
.source "OpenSourceLicenseInfoActivity.java"


# instance fields
.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 32
    const v0, 0x7f040008

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;->setContentView(I)V

    .line 33
    const v0, 0x7f0e0040

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity$1;-><init>(Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 45
    const v0, 0x7f0b0061

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;->setTitle(I)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/text/NOTICE"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 48
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 49
    return-void
.end method
