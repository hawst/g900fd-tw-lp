.class public Lcom/sec/android/app/popupcalculator/History;
.super Ljava/lang/Object;
.source "History.java"


# static fields
.field private static final MAX_ENTRIES:I = 0xa

.field private static final VERSION_1:I = 0x1


# instance fields
.field mEntries:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/popupcalculator/HistoryEntry;",
            ">;"
        }
    .end annotation
.end field

.field mObserver:Landroid/widget/BaseAdapter;

.field mPos:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/History;->clear()V

    .line 41
    return-void
.end method

.method constructor <init>(ILjava/io/DataInput;)V
    .locals 5
    .param p1, "version"    # I
    .param p2, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    .line 44
    const/4 v2, 0x1

    if-lt p1, v2, :cond_1

    .line 45
    invoke-interface {p2}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 46
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 47
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    new-instance v3, Lcom/sec/android/app/popupcalculator/HistoryEntry;

    invoke-direct {v3, p1, p2}, Lcom/sec/android/app/popupcalculator/HistoryEntry;-><init>(ILjava/io/DataInput;)V

    invoke-virtual {v2, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    invoke-interface {p2}, Ljava/io/DataInput;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    .line 52
    return-void

    .line 51
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_1
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private notifyChanged()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/History;->mObserver:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/History;->mObserver:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method clear()V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    new-instance v1, Lcom/sec/android/app/popupcalculator/HistoryEntry;

    const-string v2, ""

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/popupcalculator/HistoryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/History;->notifyChanged()V

    .line 69
    return-void
.end method

.method current(I)Lcom/sec/android/app/popupcalculator/HistoryEntry;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/popupcalculator/HistoryEntry;

    return-object v0
.end method

.method enter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "result"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/popupcalculator/HistoryEntry;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getBase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_1

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    .line 97
    .local v0, "hisSize":I
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 98
    const/4 v0, 0x1

    .line 100
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    new-instance v2, Lcom/sec/android/app/popupcalculator/HistoryEntry;

    invoke-direct {v2, p1, p2}, Lcom/sec/android/app/popupcalculator/HistoryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/History;->notifyChanged()V

    .line 104
    .end local v0    # "hisSize":I
    :cond_3
    return-void
.end method

.method public isHistory()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/History;->current(I)Lcom/sec/android/app/popupcalculator/HistoryEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method lastHistoryDelete()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    .line 109
    .local v0, "length":I
    if-le v0, v2, :cond_1

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    if-lez v0, :cond_0

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 115
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    goto :goto_0
.end method

.method setObserver(Landroid/widget/BaseAdapter;)V
    .locals 0
    .param p1, "observer"    # Landroid/widget/BaseAdapter;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/History;->mObserver:Landroid/widget/BaseAdapter;

    .line 56
    return-void
.end method

.method write(Ljava/io/DataOutput;)V
    .locals 3
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 84
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/popupcalculator/HistoryEntry;

    .line 85
    .local v0, "entry":Lcom/sec/android/app/popupcalculator/HistoryEntry;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->write(Ljava/io/DataOutput;)V

    goto :goto_0

    .line 87
    .end local v0    # "entry":Lcom/sec/android/app/popupcalculator/HistoryEntry;
    :cond_0
    iget v2, p0, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 88
    return-void
.end method
