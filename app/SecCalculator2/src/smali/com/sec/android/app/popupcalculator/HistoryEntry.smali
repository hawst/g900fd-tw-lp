.class Lcom/sec/android/app/popupcalculator/HistoryEntry;
.super Ljava/lang/Object;
.source "HistoryEntry.java"


# static fields
.field private static final VERSION_1:I = 0x1


# instance fields
.field private mBase:Ljava/lang/String;

.field private mEdited:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/io/DataInput;)V
    .locals 3
    .param p1, "version"    # I
    .param p2, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    .line 37
    invoke-interface {p2}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mBase:Ljava/lang/String;

    .line 38
    invoke-interface {p2}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mEdited:Ljava/lang/String;

    .line 41
    return-void

    .line 40
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "rst"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mBase:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mEdited:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method clearEdited()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mBase:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mEdited:Ljava/lang/String;

    .line 54
    return-void
.end method

.method getBase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mBase:Ljava/lang/String;

    return-object v0
.end method

.method getEdited()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mEdited:Ljava/lang/String;

    return-object v0
.end method

.method public setBase(Ljava/lang/String;)V
    .locals 0
    .param p1, "base"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mBase:Ljava/lang/String;

    .line 67
    return-void
.end method

.method setEdited(Ljava/lang/String;)V
    .locals 0
    .param p1, "edited"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mEdited:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mBase:Ljava/lang/String;

    return-object v0
.end method

.method write(Ljava/io/DataOutput;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mBase:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryEntry;->mEdited:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    .line 46
    return-void
.end method
