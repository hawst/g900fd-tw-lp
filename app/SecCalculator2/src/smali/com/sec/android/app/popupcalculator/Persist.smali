.class public Lcom/sec/android/app/popupcalculator/Persist;
.super Ljava/lang/Object;
.source "Persist.java"


# static fields
.field private static final FILE_NAME:Ljava/lang/String; = "calculator.data"

.field private static final LAST_VERSION:I = 0x1


# instance fields
.field public history:Lcom/sec/android/app/popupcalculator/History;

.field private mContext:Landroid/content/Context;

.field public mInstance:Lcom/sec/android/app/popupcalculator/Persist;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Persist;->history:Lcom/sec/android/app/popupcalculator/History;

    .line 43
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Persist;->mInstance:Lcom/sec/android/app/popupcalculator/Persist;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Persist;->mContext:Landroid/content/Context;

    .line 48
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->fileList()[Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "fileList":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 51
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_1

    .line 52
    aget-object v3, v1, v2

    const-string v4, "calculator.data"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Persist;->load()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 57
    .end local v1    # "fileList":[Ljava/lang/String;
    .end local v2    # "i":I
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Persist"

    const-string v4, "Persist"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    return-void
.end method

.method private load()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 78
    const/4 v4, 0x0

    .line 79
    .local v4, "is":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 80
    .local v2, "in":Ljava/io/DataInputStream;
    const/4 v1, 0x0

    .line 83
    .local v1, "fin":Ljava/io/FileInputStream;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/Persist;->mContext:Landroid/content/Context;

    const-string v8, "calculator.data"

    invoke-virtual {v7, v8}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    .line 84
    new-instance v5, Ljava/io/BufferedInputStream;

    const/16 v7, 0x2000

    invoke-direct {v5, v1, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    .end local v4    # "is":Ljava/io/InputStream;
    .local v5, "is":Ljava/io/InputStream;
    :try_start_1
    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 86
    .end local v2    # "in":Ljava/io/DataInputStream;
    .local v3, "in":Ljava/io/DataInputStream;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    .line 87
    .local v6, "version":I
    if-le v6, v9, :cond_3

    .line 88
    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "data version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; expected "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 91
    .end local v6    # "version":I
    :catch_0
    move-exception v0

    move-object v2, v3

    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    move-object v4, v5

    .line 92
    .end local v5    # "is":Ljava/io/InputStream;
    .local v0, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "is":Ljava/io/InputStream;
    :goto_0
    :try_start_3
    const-string v7, "Persist"

    const-string v8, "load-FNFE"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 96
    if-eqz v2, :cond_0

    .line 98
    :try_start_4
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6

    .line 102
    :cond_0
    :goto_1
    if-eqz v4, :cond_1

    .line 104
    :try_start_5
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_7

    .line 109
    :cond_1
    :goto_2
    if-eqz v1, :cond_2

    .line 111
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_8

    .line 116
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_2
    :goto_3
    return-void

    .line 90
    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "version":I
    :cond_3
    :try_start_7
    new-instance v7, Lcom/sec/android/app/popupcalculator/History;

    invoke-direct {v7, v6, v3}, Lcom/sec/android/app/popupcalculator/History;-><init>(ILjava/io/DataInput;)V

    iput-object v7, p0, Lcom/sec/android/app/popupcalculator/Persist;->history:Lcom/sec/android/app/popupcalculator/History;
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_f
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 96
    if-eqz v3, :cond_4

    .line 98
    :try_start_8
    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_4

    .line 102
    :cond_4
    :goto_4
    if-eqz v5, :cond_5

    .line 104
    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_5

    .line 109
    :cond_5
    :goto_5
    if-eqz v1, :cond_b

    .line 111
    :try_start_a
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    move-object v4, v5

    .line 113
    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_3

    .line 112
    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    move-object v4, v5

    .line 113
    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_3

    .line 93
    .end local v6    # "version":I
    :catch_2
    move-exception v0

    .line 94
    .local v0, "e":Ljava/io/IOException;
    :goto_6
    :try_start_b
    const-string v7, "Persist"

    const-string v8, "load-IOE"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 96
    if-eqz v2, :cond_6

    .line 98
    :try_start_c
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_9

    .line 102
    :cond_6
    :goto_7
    if-eqz v4, :cond_7

    .line 104
    :try_start_d
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_a

    .line 109
    :cond_7
    :goto_8
    if-eqz v1, :cond_2

    .line 111
    :try_start_e
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_3

    goto :goto_3

    .line 112
    :catch_3
    move-exception v7

    goto :goto_3

    .line 96
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_9
    if-eqz v2, :cond_8

    .line 98
    :try_start_f
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_b

    .line 102
    :cond_8
    :goto_a
    if-eqz v4, :cond_9

    .line 104
    :try_start_10
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_c

    .line 109
    :cond_9
    :goto_b
    if-eqz v1, :cond_a

    .line 111
    :try_start_11
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_d

    .line 113
    :cond_a
    :goto_c
    throw v7

    .line 99
    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "version":I
    :catch_4
    move-exception v7

    goto :goto_4

    .line 105
    :catch_5
    move-exception v7

    goto :goto_5

    .line 99
    .end local v3    # "in":Ljava/io/DataInputStream;
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v6    # "version":I
    .local v0, "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    :catch_6
    move-exception v7

    goto :goto_1

    .line 105
    :catch_7
    move-exception v7

    goto :goto_2

    .line 112
    :catch_8
    move-exception v7

    goto :goto_3

    .line 99
    .local v0, "e":Ljava/io/IOException;
    :catch_9
    move-exception v7

    goto :goto_7

    .line 105
    :catch_a
    move-exception v7

    goto :goto_8

    .line 99
    .end local v0    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v8

    goto :goto_a

    .line 105
    :catch_c
    move-exception v8

    goto :goto_b

    .line 112
    :catch_d
    move-exception v8

    goto :goto_c

    .line 96
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_9

    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    :catchall_2
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_9

    .line 93
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    :catch_e
    move-exception v0

    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_6

    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    :catch_f
    move-exception v0

    move-object v2, v3

    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_6

    .line 91
    :catch_10
    move-exception v0

    goto/16 :goto_0

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    :catch_11
    move-exception v0

    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto/16 :goto_0

    .end local v2    # "in":Ljava/io/DataInputStream;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/DataInputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "version":I
    :cond_b
    move-object v2, v3

    .end local v3    # "in":Ljava/io/DataInputStream;
    .restart local v2    # "in":Ljava/io/DataInputStream;
    move-object v4, v5

    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    goto :goto_3
.end method


# virtual methods
.method public getHistory()Lcom/sec/android/app/popupcalculator/History;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Persist;->history:Lcom/sec/android/app/popupcalculator/History;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/sec/android/app/popupcalculator/History;

    invoke-direct {v0}, Lcom/sec/android/app/popupcalculator/History;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Persist;->history:Lcom/sec/android/app/popupcalculator/History;

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Persist;->history:Lcom/sec/android/app/popupcalculator/History;

    return-object v0
.end method

.method public getInstance(Landroid/content/Context;)Lcom/sec/android/app/popupcalculator/Persist;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Persist;->mInstance:Lcom/sec/android/app/popupcalculator/Persist;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/sec/android/app/popupcalculator/Persist;

    invoke-direct {v0, p1}, Lcom/sec/android/app/popupcalculator/Persist;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Persist;->mInstance:Lcom/sec/android/app/popupcalculator/Persist;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Persist;->mInstance:Lcom/sec/android/app/popupcalculator/Persist;

    return-object v0
.end method

.method public save()V
    .locals 9

    .prologue
    .line 120
    const/4 v2, 0x0

    .line 121
    .local v2, "os":Ljava/io/OutputStream;
    const/4 v4, 0x0

    .line 122
    .local v4, "out":Ljava/io/DataOutputStream;
    const/4 v1, 0x0

    .line 125
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Persist;->mContext:Landroid/content/Context;

    const-string v7, "calculator.data"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 126
    new-instance v3, Ljava/io/BufferedOutputStream;

    const/16 v6, 0x2000

    invoke-direct {v3, v1, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    .end local v2    # "os":Ljava/io/OutputStream;
    .local v3, "os":Ljava/io/OutputStream;
    :try_start_1
    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 128
    .end local v4    # "out":Ljava/io/DataOutputStream;
    .local v5, "out":Ljava/io/DataOutputStream;
    const/4 v6, 0x1

    :try_start_2
    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 129
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Persist;->history:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/popupcalculator/History;->write(Ljava/io/DataOutput;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 135
    if-eqz v5, :cond_0

    .line 137
    :try_start_3
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_5

    .line 141
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 143
    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6

    .line 147
    :cond_1
    :goto_1
    if-eqz v1, :cond_a

    .line 149
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    move-object v4, v5

    .end local v5    # "out":Ljava/io/DataOutputStream;
    .restart local v4    # "out":Ljava/io/DataOutputStream;
    move-object v2, v3

    .line 154
    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    :cond_2
    :goto_2
    return-void

    .line 150
    .end local v2    # "os":Ljava/io/OutputStream;
    .end local v4    # "out":Ljava/io/DataOutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/DataOutputStream;
    :catch_0
    move-exception v6

    move-object v4, v5

    .end local v5    # "out":Ljava/io/DataOutputStream;
    .restart local v4    # "out":Ljava/io/DataOutputStream;
    move-object v2, v3

    .line 151
    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_2

    .line 130
    :catch_1
    move-exception v0

    .line 131
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v6, "Persist"

    const-string v7, "save-IOE"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 135
    if-eqz v4, :cond_3

    .line 137
    :try_start_7
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_7

    .line 141
    :cond_3
    :goto_4
    if-eqz v2, :cond_4

    .line 143
    :try_start_8
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_8

    .line 147
    :cond_4
    :goto_5
    if-eqz v1, :cond_2

    .line 149
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_2

    .line 150
    :catch_2
    move-exception v6

    goto :goto_2

    .line 132
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/NullPointerException;
    :goto_6
    :try_start_a
    const-string v6, "Persist"

    const-string v7, "save-NPE"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 135
    if-eqz v4, :cond_5

    .line 137
    :try_start_b
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_9

    .line 141
    :cond_5
    :goto_7
    if-eqz v2, :cond_6

    .line 143
    :try_start_c
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_a

    .line 147
    :cond_6
    :goto_8
    if-eqz v1, :cond_2

    .line 149
    :try_start_d
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_4

    goto :goto_2

    .line 150
    :catch_4
    move-exception v6

    goto :goto_2

    .line 135
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v6

    :goto_9
    if-eqz v4, :cond_7

    .line 137
    :try_start_e
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_b

    .line 141
    :cond_7
    :goto_a
    if-eqz v2, :cond_8

    .line 143
    :try_start_f
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_c

    .line 147
    :cond_8
    :goto_b
    if-eqz v1, :cond_9

    .line 149
    :try_start_10
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_d

    .line 151
    :cond_9
    :goto_c
    throw v6

    .line 138
    .end local v2    # "os":Ljava/io/OutputStream;
    .end local v4    # "out":Ljava/io/DataOutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/DataOutputStream;
    :catch_5
    move-exception v6

    goto :goto_0

    .line 144
    :catch_6
    move-exception v6

    goto :goto_1

    .line 138
    .end local v3    # "os":Ljava/io/OutputStream;
    .end local v5    # "out":Ljava/io/DataOutputStream;
    .local v0, "e":Ljava/io/IOException;
    .restart local v2    # "os":Ljava/io/OutputStream;
    .restart local v4    # "out":Ljava/io/DataOutputStream;
    :catch_7
    move-exception v6

    goto :goto_4

    .line 144
    :catch_8
    move-exception v6

    goto :goto_5

    .line 138
    .local v0, "e":Ljava/lang/NullPointerException;
    :catch_9
    move-exception v6

    goto :goto_7

    .line 144
    :catch_a
    move-exception v6

    goto :goto_8

    .line 138
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_b
    move-exception v7

    goto :goto_a

    .line 144
    :catch_c
    move-exception v7

    goto :goto_b

    .line 150
    :catch_d
    move-exception v7

    goto :goto_c

    .line 135
    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_9

    .end local v2    # "os":Ljava/io/OutputStream;
    .end local v4    # "out":Ljava/io/DataOutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/DataOutputStream;
    :catchall_2
    move-exception v6

    move-object v4, v5

    .end local v5    # "out":Ljava/io/DataOutputStream;
    .restart local v4    # "out":Ljava/io/DataOutputStream;
    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_9

    .line 132
    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    :catch_e
    move-exception v0

    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_6

    .end local v2    # "os":Ljava/io/OutputStream;
    .end local v4    # "out":Ljava/io/DataOutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/DataOutputStream;
    :catch_f
    move-exception v0

    move-object v4, v5

    .end local v5    # "out":Ljava/io/DataOutputStream;
    .restart local v4    # "out":Ljava/io/DataOutputStream;
    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_6

    .line 130
    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    :catch_10
    move-exception v0

    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_3

    .end local v2    # "os":Ljava/io/OutputStream;
    .end local v4    # "out":Ljava/io/DataOutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/DataOutputStream;
    :catch_11
    move-exception v0

    move-object v4, v5

    .end local v5    # "out":Ljava/io/DataOutputStream;
    .restart local v4    # "out":Ljava/io/DataOutputStream;
    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_3

    .end local v2    # "os":Ljava/io/OutputStream;
    .end local v4    # "out":Ljava/io/DataOutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/DataOutputStream;
    :cond_a
    move-object v4, v5

    .end local v5    # "out":Ljava/io/DataOutputStream;
    .restart local v4    # "out":Ljava/io/DataOutputStream;
    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_2
.end method
