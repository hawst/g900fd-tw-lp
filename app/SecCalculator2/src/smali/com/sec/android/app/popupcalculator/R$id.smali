.class public final Lcom/sec/android/app/popupcalculator/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CloseBtn:I = 0x7f0e004e

.field public static final advancedPanel:I = 0x7f0e0043

.field public static final basicPanel:I = 0x7f0e0041

.field public static final bg_layout:I = 0x7f0e0050

.field public static final bottom:I = 0x7f0e0000

.field public static final bottomPanel:I = 0x7f0e0008

.field public static final btMinimodeFullScreen:I = 0x7f0e0051

.field public static final bt_00:I = 0x7f0e002a

.field public static final bt_01:I = 0x7f0e0026

.field public static final bt_02:I = 0x7f0e0027

.field public static final bt_03:I = 0x7f0e0028

.field public static final bt_04:I = 0x7f0e0022

.field public static final bt_05:I = 0x7f0e0023

.field public static final bt_06:I = 0x7f0e0024

.field public static final bt_07:I = 0x7f0e001e

.field public static final bt_08:I = 0x7f0e001f

.field public static final bt_09:I = 0x7f0e0020

.field public static final bt_1divx:I = 0x7f0e0036

.field public static final bt_abs:I = 0x7f0e003a

.field public static final bt_add:I = 0x7f0e0025

.field public static final bt_arrow_left:I = 0x7f0e0042

.field public static final bt_arrow_right:I = 0x7f0e0044

.field public static final bt_backspace:I = 0x7f0e001d

.field public static final bt_clear:I = 0x7f0e001a

.field public static final bt_cos:I = 0x7f0e0032

.field public static final bt_div:I = 0x7f0e001b

.field public static final bt_dot:I = 0x7f0e002b

.field public static final bt_e:I = 0x7f0e003c

.field public static final bt_e_y:I = 0x7f0e0037

.field public static final bt_equal:I = 0x7f0e002d

.field public static final bt_fact:I = 0x7f0e002e

.field public static final bt_ln:I = 0x7f0e0034

.field public static final bt_log:I = 0x7f0e0035

.field public static final bt_mul:I = 0x7f0e001c

.field public static final bt_parenthesis:I = 0x7f0e0029

.field public static final bt_persentage:I = 0x7f0e0030

.field public static final bt_pie:I = 0x7f0e003b

.field public static final bt_plusminus:I = 0x7f0e002c

.field public static final bt_root:I = 0x7f0e002f

.field public static final bt_sin:I = 0x7f0e0031

.field public static final bt_sub:I = 0x7f0e0021

.field public static final bt_tan:I = 0x7f0e0033

.field public static final bt_x_2:I = 0x7f0e0038

.field public static final bt_x_y:I = 0x7f0e0039

.field public static final canvas_view:I = 0x7f0e000b

.field public static final center_button:I = 0x7f0e0004

.field public static final clear_btn:I = 0x7f0e0016

.field public static final handwrite_clear:I = 0x7f0e0010

.field public static final handwrite_eraser:I = 0x7f0e000d

.field public static final handwrite_pen:I = 0x7f0e000c

.field public static final handwrite_redo:I = 0x7f0e000f

.field public static final handwrite_undo:I = 0x7f0e000e

.field public static final history:I = 0x7f0e0015

.field public static final historyExpression:I = 0x7f0e0013

.field public static final historyFrame:I = 0x7f0e0005

.field public static final historyList:I = 0x7f0e0012

.field public static final historyResult:I = 0x7f0e0014

.field public static final history_scroll:I = 0x7f0e004d

.field public static final iv_bt_background:I = 0x7f0e003f

.field public static final iv_onehand_left:I = 0x7f0e0047

.field public static final iv_onehand_right:I = 0x7f0e0049

.field public static final left:I = 0x7f0e0001

.field public static final ll_onehand_left:I = 0x7f0e0046

.field public static final ll_onehand_right:I = 0x7f0e0048

.field public static final main_layout:I = 0x7f0e003e

.field public static final main_view_group:I = 0x7f0e0011

.field public static final menu_clear:I = 0x7f0e0059

.field public static final menu_handwriting_keypad:I = 0x7f0e005b

.field public static final menu_keypad:I = 0x7f0e005c

.field public static final menu_multiwindow:I = 0x7f0e005a

.field public static final no_history:I = 0x7f0e003d

.field public static final onehand_keypad_left:I = 0x7f0e004a

.field public static final onehand_keypad_right:I = 0x7f0e004c

.field public static final onehand_numpad:I = 0x7f0e004b

.field public static final panelContent:I = 0x7f0e0009

.field public static final panelHandle:I = 0x7f0e000a

.field public static final panel_bg:I = 0x7f0e0045

.field public static final right:I = 0x7f0e0002

.field public static final textLinear:I = 0x7f0e0017

.field public static final text_item_text_large:I = 0x7f0e0057

.field public static final text_item_text_medium:I = 0x7f0e0056

.field public static final text_item_text_small:I = 0x7f0e0055

.field public static final text_size_item:I = 0x7f0e0054

.field public static final text_size_item_radio:I = 0x7f0e0058

.field public static final top:I = 0x7f0e0003

.field public static final top_txtCalc:I = 0x7f0e0019

.field public static final touchLinear:I = 0x7f0e0053

.field public static final trans_window:I = 0x7f0e004f

.field public static final txtCalc:I = 0x7f0e0018

.field public static final txtCalcLinear:I = 0x7f0e0006

.field public static final webView:I = 0x7f0e0007

.field public static final webview:I = 0x7f0e0040

.field public static final window_title:I = 0x7f0e0052


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
