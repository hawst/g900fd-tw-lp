.class Lcom/sec/android/app/popupcalculator/HistoryListView$1;
.super Ljava/lang/Object;
.source "HistoryListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupcalculator/HistoryListView;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/HistoryListView;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$1;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v2, 0x7f0e0013

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 81
    .local v1, "txtView":Landroid/widget/TextView;
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$1;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$1;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    iget-object v2, v2, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-boolean v2, v2, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$1;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    iget-object v2, v2, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 84
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$1;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/app/popupcalculator/HistoryListView;->saveToSharedPref(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/popupcalculator/HistoryListView;->access$000(Lcom/sec/android/app/popupcalculator/HistoryListView;Ljava/lang/String;)V

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$1;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->startActivity(Landroid/content/Intent;)V

    .line 90
    :goto_0
    return-void

    .line 87
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$1;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/popupcalculator/HistoryListView;->createDiscardDialog(Ljava/lang/String;)V

    goto :goto_0
.end method
