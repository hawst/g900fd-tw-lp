.class Lcom/sec/android/app/popupcalculator/CToken;
.super Ljava/lang/Object;
.source "Logic.java"


# instance fields
.field private tokenId:Lcom/sec/android/app/popupcalculator/token_type;

.field private tokenValue:D


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    sget-object v0, Lcom/sec/android/app/popupcalculator/token_type;->EOS:Lcom/sec/android/app/popupcalculator/token_type;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/CToken;->tokenId:Lcom/sec/android/app/popupcalculator/token_type;

    .line 272
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/popupcalculator/CToken;->tokenValue:D

    .line 273
    return-void
.end method

.method constructor <init>(Lcom/sec/android/app/popupcalculator/token_type;D)V
    .locals 0
    .param p1, "tt"    # Lcom/sec/android/app/popupcalculator/token_type;
    .param p2, "tv"    # D

    .prologue
    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/CToken;->tokenId:Lcom/sec/android/app/popupcalculator/token_type;

    .line 277
    iput-wide p2, p0, Lcom/sec/android/app/popupcalculator/CToken;->tokenValue:D

    .line 278
    return-void
.end method


# virtual methods
.method getTokenType()Lcom/sec/android/app/popupcalculator/token_type;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CToken;->tokenId:Lcom/sec/android/app/popupcalculator/token_type;

    return-object v0
.end method

.method getTokenValue()D
    .locals 2

    .prologue
    .line 293
    iget-wide v0, p0, Lcom/sec/android/app/popupcalculator/CToken;->tokenValue:D

    return-wide v0
.end method

.method setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V
    .locals 0
    .param p1, "t"    # Lcom/sec/android/app/popupcalculator/token_type;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/CToken;->tokenId:Lcom/sec/android/app/popupcalculator/token_type;

    .line 282
    return-void
.end method

.method setTokenValue(D)V
    .locals 1
    .param p1, "d"    # D

    .prologue
    .line 285
    iput-wide p1, p0, Lcom/sec/android/app/popupcalculator/CToken;->tokenValue:D

    .line 286
    return-void
.end method
