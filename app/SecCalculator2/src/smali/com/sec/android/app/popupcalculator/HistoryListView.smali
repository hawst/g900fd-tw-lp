.class public Lcom/sec/android/app/popupcalculator/HistoryListView;
.super Landroid/app/Fragment;
.source "HistoryListView.java"


# static fields
.field private static final CURRENT_DISPLAY_FILE:Ljava/lang/String; = "backup_dsp"


# instance fields
.field OptionDialog:Landroid/app/AlertDialog;

.field adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

.field private isAlertShown:Z

.field listView:Landroid/widget/ListView;

.field mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

.field mHistory:Lcom/sec/android/app/popupcalculator/History;

.field private mHistoryInput:Ljava/lang/String;

.field values:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/popupcalculator/HistoryEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->isAlertShown:Z

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->OptionDialog:Landroid/app/AlertDialog;

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/popupcalculator/History;Lcom/sec/android/app/popupcalculator/EventHandler;)V
    .locals 1
    .param p1, "entries"    # Lcom/sec/android/app/popupcalculator/History;
    .param p2, "handler"    # Lcom/sec/android/app/popupcalculator/EventHandler;

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->isAlertShown:Z

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->OptionDialog:Landroid/app/AlertDialog;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/popupcalculator/HistoryListView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/HistoryListView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/popupcalculator/HistoryListView;->saveToSharedPref(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/popupcalculator/HistoryListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/HistoryListView;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->isAlertShown:Z

    return p1
.end method

.method private notifyChanged()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->notifyDataSetChanged()V

    .line 59
    :cond_0
    return-void
.end method

.method private saveToSharedPref(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "backup_dsp"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 109
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 110
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "Epression"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 111
    const-string v2, "HistoryListOpenState"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 112
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 114
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method


# virtual methods
.method protected createDiscardDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHistoryInput:Ljava/lang/String;

    .line 118
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 120
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 121
    .local v0, "DiscardDialog":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b0056

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 122
    const v2, 0x7f0b0055

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 124
    const v2, 0x7f0b0054

    new-instance v3, Lcom/sec/android/app/popupcalculator/HistoryListView$2;

    invoke-direct {v3, p0, p1, v1}, Lcom/sec/android/app/popupcalculator/HistoryListView$2;-><init>(Lcom/sec/android/app/popupcalculator/HistoryListView;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 136
    const/high16 v2, 0x1040000

    new-instance v3, Lcom/sec/android/app/popupcalculator/HistoryListView$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/popupcalculator/HistoryListView$3;-><init>(Lcom/sec/android/app/popupcalculator/HistoryListView;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 144
    new-instance v2, Lcom/sec/android/app/popupcalculator/HistoryListView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/popupcalculator/HistoryListView$4;-><init>(Lcom/sec/android/app/popupcalculator/HistoryListView;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 153
    iget-boolean v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->isAlertShown:Z

    if-nez v2, :cond_0

    .line 155
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->OptionDialog:Landroid/app/AlertDialog;

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->OptionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 157
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->isAlertShown:Z

    .line 160
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 63
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->listView:Landroid/widget/ListView;

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0e0012

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->listView:Landroid/widget/ListView;

    .line 65
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->listView:Landroid/widget/ListView;

    const-string v4, "\u00a0"

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 66
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->values:Ljava/util/ArrayList;

    .line 67
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 68
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->values:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 69
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->values:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/History;->mEntries:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 71
    :cond_0
    new-instance v3, Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->values:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-direct {v3, v4, v7, v5, v6}, Lcom/sec/android/app/popupcalculator/HistoryAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/android/app/popupcalculator/EventHandler;)V

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->notifyDataSetChanged()V

    .line 73
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->listView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 74
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->listView:Landroid/widget/ListView;

    new-instance v4, Lcom/sec/android/app/popupcalculator/HistoryListView$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/popupcalculator/HistoryListView$1;-><init>(Lcom/sec/android/app/popupcalculator/HistoryListView;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "backup_dsp"

    invoke-virtual {v3, v4, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 94
    .local v2, "sp":Landroid/content/SharedPreferences;
    const-string v3, "DiscardEpression"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 95
    const-string v3, "DiscardEpression"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "displayExp":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->createDiscardDialog(Ljava/lang/String;)V

    .line 97
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 98
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "DiscardEpression"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 99
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 101
    .end local v0    # "displayExp":Ljava/lang/String;
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 102
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 47
    const v0, 0x7f040003

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->OptionDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->OptionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 176
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->isAlertShown:Z

    if-eqz v2, :cond_1

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "backup_dsp"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 179
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 180
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "DiscardEpression"

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->mHistoryInput:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 181
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 182
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->isAlertShown:Z

    .line 184
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "sp":Landroid/content/SharedPreferences;
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 185
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->notifyDataSetChanged()V

    .line 166
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->notifyChanged()V

    .line 167
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 168
    return-void
.end method

.method setObserver(Landroid/widget/BaseAdapter;)V
    .locals 0
    .param p1, "observer"    # Landroid/widget/BaseAdapter;

    .prologue
    .line 52
    check-cast p1, Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    .end local p1    # "observer":Landroid/widget/BaseAdapter;
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryListView;->adapter:Lcom/sec/android/app/popupcalculator/HistoryAdapter;

    .line 53
    return-void
.end method
