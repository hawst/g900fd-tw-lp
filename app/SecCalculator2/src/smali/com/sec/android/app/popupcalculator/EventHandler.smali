.class public Lcom/sec/android/app/popupcalculator/EventHandler;
.super Ljava/lang/Object;
.source "EventHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/popupcalculator/EventHandler$OnSecretActionListener;,
        Lcom/sec/android/app/popupcalculator/EventHandler$CheckHiddenMenu;,
        Lcom/sec/android/app/popupcalculator/EventHandler$DelKeyEvent;
    }
.end annotation


# static fields
.field public static final ABSOLUTE_VALUE:Ljava/lang/String; = "abs("

.field private static final COMMA_ADD_ERROR:I = 0x7f0b0058

.field private static final CTNKeystring:Ljava/lang/String; = "(+286+("

.field private static final CTNKeystring2:Ljava/lang/String; = "(+68277+("

.field private static final CTNKeystring2_len:I

.field private static final CTNKeystring_len:I

.field private static final CTN_FILE:Ljava/lang/String; = "com.android.hiddenmenu.CTN"

.field private static final CTN_PACKAGE:Ljava/lang/String; = "com.android.hiddenmenu"

.field public static final DISPLAY_TEXT:Ljava/lang/String; = "DISPLAY_TEXT"

.field private static final DIVISION:C = '\u00f7'

.field private static final DIVISION_ZERO:Ljava/lang/String; = "Division by zero"

.field private static final DIVISION_ZERO_ERROR:I = 0x7f0b0006

.field public static final END_CURSOR:I = 0x1

.field private static final ERROR:I = 0x7f0b0057

.field private static final ERROR_STATE:I = -0x1

.field private static final FACTORY_INPUT_ERROR:I = 0x7f0b000a

.field private static final FACTORY_MODE_FILE:Ljava/lang/String; = "com.sec.android.app.factorymode.FactoryCTRL"

.field private static final FACTORY_MODE_PACKAGE:Ljava/lang/String; = "com.sec.android.app.factorymode"

.field private static final FactoryModeKeystring:Ljava/lang/String; = "(+30012012732+"

.field private static final FactoryModeKeystring_len:I

.field public static final HISTORY_CLEAR:Ljava/lang/String; = "history_clear"

.field private static final INFINITY:Ljava/lang/String; = "Infinity"

.field private static final INSERT_PLUS_MINUS_DELETE:I = 0x6

.field private static final INSERT_PLUS_MINUS_FRIST:I = 0x1

.field private static final INSERT_PLUS_MINUS_L_PAREN:I = 0x2

.field private static final INSERT_PLUS_MINUS_R_PAREN:I = 0x5

.field private static final INVALID_INPUT_ERROR:I = 0x7f0b0004

.field private static final K_COMMA_ADD_ERROR:I = 0x7f0b0017

.field private static final K_DIVISION_ZERO_ERROR:I = 0x7f0b0011

.field private static final K_ERROR:I = 0x7f0b0016

.field private static final K_FACTORY_INPUT_ERROR:I = 0x7f0b0015

.field private static final K_INVALID_INPUT_ERROR:I = 0x7f0b000f

.field private static final K_LOG_ERROR:I = 0x7f0b0015

.field private static final K_MAX_COMMA_ERROR:I = 0x7f0b000c

.field private static final K_MAX_DOT_ERROR:I = 0x7f0b000d

.field private static final K_MAX_INPUT_ERROR:I = 0x7f0b0010

.field private static final K_MAX_POINT_ERROR:I = 0x7f0b000e

.field private static final K_OVERFLOW_ERROR:I = 0x7f0b0013

.field private static final K_ROOT_ERROR:I = 0x7f0b0014

.field private static final LOG_ERROR:I = 0x7f0b000a

.field private static final MAX_COMMA_ERROR:I = 0x7f0b0001

.field private static final MAX_DOT_ERROR:I = 0x7f0b0002

.field private static final MAX_INPUT_ERROR:I = 0x7f0b0005

.field private static final MAX_POINT_ERROR:I = 0x7f0b0003

.field private static final MULTIPLICATION:C = '\u00d7'

.field private static MULTIWINDOW_TEXT_SIZE_LARGE:I = 0x0

.field private static MULTIWINDOW_TEXT_SIZE_LARGE_SCIENTIFIC:I = 0x0

.field private static MULTIWINDOW_TEXT_SIZE_MEDIUM:I = 0x0

.field private static MULTIWINDOW_TEXT_SIZE_MEDIUM_SCIENTIFIC:I = 0x0

.field private static MULTIWINDOW_TEXT_SIZE_SMALL:I = 0x0

.field private static MULTIWINDOW_TEXT_SIZE_SMALL_SCIENTIFIC:I = 0x0

.field private static final NAN:Ljava/lang/String; = "NaN"

.field private static final NEGATIVE_SIGN:C = '-'

.field public static final ONE_DIVISTION_X:Ljava/lang/String; = "1\u00f7"

.field private static final OTASP_FILE:Ljava/lang/String; = "com.android.hiddenmenu.OTASP"

.field private static final OVERFLOW_ERROR:I = 0x7f0b0008

.field private static final PARSER_MODE_FILE:Ljava/lang/String; = "com.sec.android.app.parser.SecretCodeIME"

.field private static final PARSER_MODE_PACKAGE:Ljava/lang/String; = "com.sec.android.app.parser"

.field public static final PKG_NAME:Ljava/lang/String; = "com.sec.android.app.popupcalculator"

.field public static final PLUS_MINUS:Ljava/lang/String; = "(\u2212"

.field private static final ROOT_ERROR:I = 0x7f0b0009

.field public static final START_CURSOR:I

.field private static final TAG:Ljava/lang/String;

.field private static TEXT_SIZE_LARGE_LAND:I

.field private static TEXT_SIZE_LARGE_PORT:I

.field private static TEXT_SIZE_MEDIUM_LAND:I

.field private static TEXT_SIZE_MEDIUM_PORT:I

.field private static TEXT_SIZE_SMALL_LAND:I

.field private static TEXT_SIZE_SMALL_PORT:I

.field public static deleteDotCount:I

.field public static mFormulaBackup:Ljava/lang/StringBuilder;

.field private static mIsOpenedSignParenthesis:Z


# instance fields
.field private FULL_ENTER_SIZE:I

.field private MINI_ENTER_SIZE:I

.field public SelectedTextSize:F

.field public error_check:I

.field private exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

.field public isErrorClearedOnInsert:Z

.field public mBackSpaceHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

.field public mEnterEnd:Z

.field private mErrorCursor:I

.field private mFormula:Ljava/lang/StringBuilder;

.field private mFormulaLengthBackup:I

.field private mHistory:Lcom/sec/android/app/popupcalculator/History;

.field private mHistoryScreen:Landroid/widget/EditText;

.field private mInsertError:Z

.field private mIsBackSpaceTouch:Z

.field private mIsError:Z

.field private mIsInputError:Z

.field private mIsResultError:Z

.field private mIsUpdatingHistory:Z

.field private mIswvga:I

.field private mLastResult:Ljava/lang/StringBuilder;

.field private mMainView:Landroid/view/View;

.field private mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mMultplicationCheck:Z

.field private mNewLineChek:Z

.field private mNumberColor:I

.field private mParanthesisCheck:Z

.field private mParanthesisDelete:Z

.field private mResult:Ljava/lang/StringBuilder;

.field private mResultColor:I

.field public mSmallFontCheck:Z

.field private mSymbolsColor:I

.field public mToast:Landroid/widget/Toast;

.field public mToastHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteDotCount:I

    .line 181
    const-class v0, Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    .line 187
    const-string v0, "(+30012012732+"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/sec/android/app/popupcalculator/EventHandler;->FactoryModeKeystring_len:I

    .line 192
    const-string v0, "(+286+("

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/sec/android/app/popupcalculator/EventHandler;->CTNKeystring_len:I

    .line 194
    const-string v0, "(+68277+("

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/sec/android/app/popupcalculator/EventHandler;->CTNKeystring2_len:I

    .line 205
    sput-boolean v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 231
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_SMALL_PORT:I

    .line 233
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_MEDIUM_PORT:I

    .line 235
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_LARGE_PORT:I

    .line 237
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_SMALL_LAND:I

    .line 239
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_MEDIUM_LAND:I

    .line 241
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_LARGE_LAND:I

    .line 243
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->MULTIWINDOW_TEXT_SIZE_SMALL:I

    .line 245
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->MULTIWINDOW_TEXT_SIZE_MEDIUM:I

    .line 247
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->MULTIWINDOW_TEXT_SIZE_LARGE:I

    .line 249
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->MULTIWINDOW_TEXT_SIZE_SMALL_SCIENTIFIC:I

    .line 251
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->MULTIWINDOW_TEXT_SIZE_MEDIUM_SCIENTIFIC:I

    .line 253
    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->MULTIWINDOW_TEXT_SIZE_LARGE_SCIENTIFIC:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;ILcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "display"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .param p3, "hisscreen"    # Landroid/widget/EditText;
    .param p4, "history"    # Lcom/sec/android/app/popupcalculator/History;
    .param p5, "_mIswvga"    # I
    .param p6, "mwActivity"    # Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .prologue
    const/4 v3, 0x0

    .line 324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v1, Lcom/sec/android/app/popupcalculator/SyntaxException;

    invoke-direct {v1}, Lcom/sec/android/app/popupcalculator/SyntaxException;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    .line 80
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->MINI_ENTER_SIZE:I

    .line 82
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->FULL_ENTER_SIZE:I

    .line 196
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 198
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 200
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsInputError:Z

    .line 202
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsResultError:Z

    .line 208
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 210
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsBackSpaceTouch:Z

    .line 212
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    .line 214
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    .line 216
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSymbolsColor:I

    .line 218
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    .line 220
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultplicationCheck:Z

    .line 223
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNewLineChek:Z

    .line 224
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisCheck:Z

    .line 225
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisDelete:Z

    .line 227
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsUpdatingHistory:Z

    .line 261
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->SelectedTextSize:F

    .line 263
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    .line 269
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    .line 272
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIswvga:I

    .line 274
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->isErrorClearedOnInsert:Z

    .line 444
    new-instance v1, Lcom/sec/android/app/popupcalculator/EventHandler$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/popupcalculator/EventHandler$2;-><init>(Lcom/sec/android/app/popupcalculator/EventHandler;)V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mBackSpaceHandler:Landroid/os/Handler;

    .line 325
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    .line 326
    iput-object p2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 327
    iput-object p3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    .line 328
    iput-object p4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    .line 329
    iput-object p6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResult:Ljava/lang/StringBuilder;

    .line 332
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormula:Ljava/lang/StringBuilder;

    .line 333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    .line 334
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    .line 335
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaLengthBackup:I

    .line 336
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    .line 337
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSymbolsColor:I

    .line 338
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    .line 339
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->FULL_ENTER_SIZE:I

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 345
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0a0038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_SMALL_PORT:I

    .line 346
    const v1, 0x7f0a0034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_MEDIUM_PORT:I

    .line 347
    const v1, 0x7f0a0030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_LARGE_PORT:I

    .line 348
    const v1, 0x7f0a0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_SMALL_LAND:I

    .line 349
    const v1, 0x7f0a0031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_MEDIUM_LAND:I

    .line 350
    const v1, 0x7f0a002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_LARGE_LAND:I

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    .line 368
    iput p5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIswvga:I

    .line 369
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/content/Context;Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;Lcom/sec/android/app/popupcalculator/EventHandler$OnSecretActionListener;)V
    .locals 4
    .param p1, "mainView"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "display"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .param p4, "hisscreen"    # Landroid/widget/EditText;
    .param p5, "history"    # Lcom/sec/android/app/popupcalculator/History;
    .param p6, "listener"    # Lcom/sec/android/app/popupcalculator/EventHandler$OnSecretActionListener;

    .prologue
    const/4 v3, 0x0

    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v1, Lcom/sec/android/app/popupcalculator/SyntaxException;

    invoke-direct {v1}, Lcom/sec/android/app/popupcalculator/SyntaxException;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    .line 80
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->MINI_ENTER_SIZE:I

    .line 82
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->FULL_ENTER_SIZE:I

    .line 196
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 198
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 200
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsInputError:Z

    .line 202
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsResultError:Z

    .line 208
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 210
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsBackSpaceTouch:Z

    .line 212
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    .line 214
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    .line 216
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSymbolsColor:I

    .line 218
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    .line 220
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultplicationCheck:Z

    .line 223
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNewLineChek:Z

    .line 224
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisCheck:Z

    .line 225
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisDelete:Z

    .line 227
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsUpdatingHistory:Z

    .line 261
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->SelectedTextSize:F

    .line 263
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    .line 269
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    .line 272
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIswvga:I

    .line 274
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->isErrorClearedOnInsert:Z

    .line 444
    new-instance v1, Lcom/sec/android/app/popupcalculator/EventHandler$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/popupcalculator/EventHandler$2;-><init>(Lcom/sec/android/app/popupcalculator/EventHandler;)V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mBackSpaceHandler:Landroid/os/Handler;

    .line 280
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    .line 281
    iput-object p2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    .line 282
    iput-object p3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 283
    iput-object p4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    .line 284
    iput-object p5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    .line 285
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResult:Ljava/lang/StringBuilder;

    .line 286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormula:Ljava/lang/StringBuilder;

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    .line 288
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    .line 289
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaLengthBackup:I

    .line 290
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    .line 291
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSymbolsColor:I

    .line 292
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    .line 293
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0028

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->MINI_ENTER_SIZE:I

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 299
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0a0038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_SMALL_PORT:I

    .line 300
    const v1, 0x7f0a0034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_MEDIUM_PORT:I

    .line 301
    const v1, 0x7f0a0030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_LARGE_PORT:I

    .line 302
    const v1, 0x7f0a0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_SMALL_LAND:I

    .line 303
    const v1, 0x7f0a0031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_MEDIUM_LAND:I

    .line 304
    const v1, 0x7f0a002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_LARGE_LAND:I

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    .line 320
    return-void
.end method

.method private IsFactoryModeKeyString(Ljava/lang/String;)Z
    .locals 7
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 3172
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v4, 0x0

    .line 3173
    .local v4, "j":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, "in_len":I
    move v5, v4

    .end local v4    # "j":I
    .local v5, "j":I
    move v2, v1

    .line 3175
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_4

    .line 3176
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 3177
    .local v0, "ch":C
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_0

    move v2, v1

    .line 3178
    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 3179
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_0
    sget v6, Lcom/sec/android/app/popupcalculator/EventHandler;->FactoryModeKeystring_len:I

    if-ge v5, v6, :cond_3

    const-string v6, "(+30012012732+"

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    invoke-virtual {v6, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eq v0, v6, :cond_1

    .line 3184
    .end local v0    # "ch":C
    :goto_1
    const/4 v6, 0x0

    :goto_2
    return v6

    .line 3181
    .restart local v0    # "ch":C
    :cond_1
    if-ne v1, v3, :cond_2

    sget v6, Lcom/sec/android/app/popupcalculator/EventHandler;->FactoryModeKeystring_len:I

    if-ne v4, v6, :cond_2

    .line 3182
    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_3
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto :goto_1

    .end local v0    # "ch":C
    .end local v1    # "i":I
    .end local v4    # "j":I
    .restart local v2    # "i":I
    .restart local v5    # "j":I
    :cond_4
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_1
.end method

.method private IsFactoryModeKeyStringCTN(Ljava/lang/String;)Z
    .locals 7
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 3132
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v4, 0x0

    .line 3133
    .local v4, "j":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, "in_len":I
    move v5, v4

    .end local v4    # "j":I
    .local v5, "j":I
    move v2, v1

    .line 3135
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_4

    .line 3136
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 3137
    .local v0, "ch":C
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_0

    move v2, v1

    .line 3138
    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 3140
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_0
    sget v6, Lcom/sec/android/app/popupcalculator/EventHandler;->CTNKeystring_len:I

    if-ge v5, v6, :cond_3

    const-string v6, "(+286+("

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    invoke-virtual {v6, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eq v0, v6, :cond_1

    .line 3147
    .end local v0    # "ch":C
    :goto_1
    const/4 v6, 0x0

    :goto_2
    return v6

    .line 3143
    .restart local v0    # "ch":C
    :cond_1
    if-ne v1, v3, :cond_2

    sget v6, Lcom/sec/android/app/popupcalculator/EventHandler;->CTNKeystring_len:I

    if-ne v4, v6, :cond_2

    .line 3144
    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_3
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto :goto_1

    .end local v0    # "ch":C
    .end local v1    # "i":I
    .end local v4    # "j":I
    .restart local v2    # "i":I
    .restart local v5    # "j":I
    :cond_4
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_1
.end method

.method private IsFactoryModeKeyStringCTN2(Ljava/lang/String;)Z
    .locals 7
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 3150
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v4, 0x0

    .line 3151
    .local v4, "j":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, "in_len":I
    move v5, v4

    .end local v4    # "j":I
    .local v5, "j":I
    move v2, v1

    .line 3153
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_4

    .line 3154
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 3155
    .local v0, "ch":C
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_0

    move v2, v1

    .line 3156
    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 3158
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_0
    sget v6, Lcom/sec/android/app/popupcalculator/EventHandler;->CTNKeystring2_len:I

    if-ge v5, v6, :cond_3

    const-string v6, "(+68277+("

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    invoke-virtual {v6, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eq v0, v6, :cond_1

    .line 3165
    .end local v0    # "ch":C
    :goto_1
    const/4 v6, 0x0

    :goto_2
    return v6

    .line 3161
    .restart local v0    # "ch":C
    :cond_1
    if-ne v1, v3, :cond_2

    sget v6, Lcom/sec/android/app/popupcalculator/EventHandler;->CTNKeystring2_len:I

    if-ne v4, v6, :cond_2

    .line 3162
    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_3
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto :goto_1

    .end local v0    # "ch":C
    .end local v1    # "i":I
    .end local v4    # "j":I
    .restart local v2    # "i":I
    .restart local v5    # "j":I
    :cond_4
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_1
.end method

.method private abs(CI)V
    .locals 3
    .param p1, "frontChar"    # C
    .param p2, "startCursor"    # I

    .prologue
    .line 918
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 919
    .local v0, "insertString":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 920
    .local v1, "text":Ljava/lang/StringBuilder;
    if-lez p2, :cond_0

    .line 921
    invoke-direct {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->nextAutoMultiple(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 923
    :cond_0
    const-string v2, "abs("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 924
    invoke-virtual {v1, p2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 925
    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 926
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/popupcalculator/EventHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/EventHandler;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsBackSpaceTouch:Z

    return v0
.end method

.method private cDecimal(IC)V
    .locals 1
    .param p1, "startCursor"    # I
    .param p2, "frontChar"    # C

    .prologue
    .line 1210
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    if-eqz v0, :cond_0

    .line 1211
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->clear()V

    .line 1214
    :cond_0
    if-lez p1, :cond_1

    invoke-static {p2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 1218
    :cond_1
    return-void
.end method

.method private cMaxInput(Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v3, 0x1

    .line 1151
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    if-nez v0, :cond_0

    .line 1153
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsInputError:Z

    .line 1154
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 1156
    :cond_0
    return-void
.end method

.method private cNegativeSign(CCC)V
    .locals 3
    .param p1, "frontChar"    # C
    .param p2, "checkChar"    # C
    .param p3, "nextChar"    # C

    .prologue
    const/16 v2, 0x2d

    const/4 v1, 0x1

    .line 1199
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    if-nez v0, :cond_1

    .line 1200
    if-ne p1, v2, :cond_2

    const/16 v0, 0x2b

    if-eq p2, v0, :cond_0

    const/16 v0, 0x2212

    if-eq p2, v0, :cond_0

    const/16 v0, 0xd7

    if-eq p2, v0, :cond_0

    const/16 v0, 0xf7

    if-ne p2, v0, :cond_2

    .line 1202
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 1207
    :cond_1
    :goto_0
    return-void

    .line 1203
    :cond_2
    if-eqz p3, :cond_1

    if-ne p3, v2, :cond_1

    const/16 v0, 0x28

    if-ne p1, v0, :cond_1

    .line 1204
    iput-boolean v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    goto :goto_0
.end method

.method private cNotStartArithmetic(IC)V
    .locals 1
    .param p1, "startCursor"    # I
    .param p2, "checkChar"    # C

    .prologue
    .line 1163
    invoke-static {p2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    if-nez v0, :cond_0

    .line 1164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 1166
    :cond_0
    return-void
.end method

.method private cOperation(CCCII)V
    .locals 4
    .param p1, "front"    # C
    .param p2, "next"    # C
    .param p3, "check"    # C
    .param p4, "startCursor"    # I
    .param p5, "endCurosr"    # I

    .prologue
    const/4 v3, 0x1

    .line 1130
    iget-boolean v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    if-nez v1, :cond_1

    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    .line 1131
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-static {p1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p3}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1133
    add-int/lit8 v1, p4, -0x2

    if-ltz v1, :cond_2

    add-int/lit8 v1, p4, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x28

    if-ne v1, v2, :cond_2

    const/16 v1, 0x2b

    if-eq p3, v1, :cond_2

    const/16 v1, 0x2212

    if-eq p3, v1, :cond_2

    .line 1135
    add-int/lit8 v1, p4, -0x1

    invoke-virtual {v0, v1, p4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1139
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 1140
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 1147
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    :goto_1
    return-void

    .line 1137
    .restart local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    add-int/lit8 v1, p4, -0x1

    invoke-virtual {v0, v1, p3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_0

    .line 1141
    :cond_3
    invoke-static {p2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p3}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1142
    invoke-virtual {v0, p5, p3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 1143
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 1144
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    goto :goto_1
.end method

.method private cParenthesis(CC)V
    .locals 1
    .param p1, "frontChar"    # C
    .param p2, "checkChar"    # C

    .prologue
    .line 1174
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    if-nez v0, :cond_1

    .line 1175
    const/16 v0, 0x28

    if-ne p1, v0, :cond_1

    const/16 v0, 0xd7

    if-eq p2, v0, :cond_0

    const/16 v0, 0xf7

    if-ne p2, v0, :cond_1

    .line 1185
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 1188
    :cond_1
    return-void
.end method

.method private changeColor(Ljava/lang/StringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 38
    .param p1, "text"    # Ljava/lang/StringBuilder;

    .prologue
    .line 1622
    new-instance v27, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1624
    .local v27, "output":Landroid/text/SpannableStringBuilder;
    const v13, 0x3b9ac9ff

    .line 1625
    .local v13, "count":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v34

    if-nez v34, :cond_0

    .line 1626
    const-string v34, ""

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v34

    .line 1853
    :goto_0
    return-object v34

    .line 1628
    :cond_0
    const/16 v18, 0x0

    .line 1630
    .local v18, "equal":Z
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_1
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v34

    move/from16 v0, v20

    move/from16 v1, v34

    if-ge v0, v1, :cond_28

    .line 1631
    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v10

    .line 1632
    .local v10, "ch":C
    const/16 v34, 0x2d

    move/from16 v0, v34

    if-ne v10, v0, :cond_1

    .line 1633
    add-int/lit8 v34, v20, 0x1

    const-string v35, "\u2212"

    move-object/from16 v0, v27

    move/from16 v1, v20

    move/from16 v2, v34

    move-object/from16 v3, v35

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1635
    :cond_1
    sparse-switch v10, :sswitch_data_0

    .line 1748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isFocused()Z

    move-result v34

    if-nez v34, :cond_2

    sget-boolean v34, Lcom/sec/android/app/popupcalculator/Calculator;->isLoadingSavedText:Z

    if-eqz v34, :cond_18

    :cond_2
    move/from16 v0, v20

    if-le v0, v13, :cond_18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    move/from16 v34, v0

    if-eqz v34, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    move-object/from16 v34, v0

    if-eqz v34, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/EditText;->getVisibility()I

    move-result v34

    if-nez v34, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsUpdatingHistory:Z

    move/from16 v34, v0

    if-nez v34, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_18

    .line 1753
    :cond_4
    new-instance v34, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v35, v0

    invoke-direct/range {v34 .. v35}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v35

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_5

    .line 1756
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v35

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_5
    move-object/from16 v34, v27

    .line 1759
    goto/16 :goto_0

    .line 1638
    :sswitch_0
    if-nez v20, :cond_8

    .line 1639
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    const-string v35, "="

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v20

    move/from16 v1, v34

    if-eq v0, v1, :cond_6

    move-object/from16 v34, v27

    .line 1641
    goto/16 :goto_0

    .line 1644
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1646
    .local v8, "beforeResult":Ljava/lang/String;
    const-string v34, "Error"

    move-object/from16 v0, v34

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v34

    if-eqz v34, :cond_7

    .line 1647
    new-instance v34, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v35, v0

    invoke-direct/range {v34 .. v35}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v35

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v36

    const/16 v37, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_7

    .line 1652
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v35

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v36

    const/16 v37, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1658
    :cond_7
    new-instance v34, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    move/from16 v35, v0

    invoke-direct/range {v34 .. v35}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v35

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object/from16 v34, v27

    .line 1661
    goto/16 :goto_0

    .line 1664
    .end local v8    # "beforeResult":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    move-object/from16 v34, v0

    if-eqz v34, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/EditText;->getVisibility()I

    move-result v34

    if-nez v34, :cond_a

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsUpdatingHistory:Z

    move/from16 v34, v0

    if-nez v34, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_c

    :cond_a
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    const-string v35, "="

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v34

    move/from16 v0, v20

    move/from16 v1, v34

    if-eq v0, v1, :cond_c

    .line 1630
    :cond_b
    :goto_2
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_1

    .line 1668
    :cond_c
    move/from16 v13, v20

    .line 1669
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    const-string v35, "\n"

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v17

    .line 1670
    .local v17, "enter":I
    const/16 v34, -0x1

    move/from16 v0, v17

    move/from16 v1, v34

    if-eq v0, v1, :cond_10

    move/from16 v15, v17

    .line 1671
    .local v15, "en":I
    :goto_3
    sget-object v34, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "en = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1672
    sget-object v34, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "text(length) = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1673
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v34

    move/from16 v0, v34

    if-le v15, v0, :cond_d

    .line 1674
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v15

    .line 1676
    :cond_d
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v34

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v35

    move/from16 v0, v34

    move/from16 v1, v35

    if-eq v0, v1, :cond_e

    .line 1677
    const/16 v34, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v35

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v35

    move-object/from16 v3, v36

    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1680
    :cond_e
    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1, v15}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v34

    const-string v35, "="

    const-string v36, ""

    invoke-virtual/range {v34 .. v36}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v32

    .line 1681
    .local v32, "sub":Ljava/lang/String;
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v34

    if-lez v34, :cond_f

    const-string v34, "Error"

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v34

    if-eqz v34, :cond_f

    const-string v34, "Error:"

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_f

    .line 1682
    const-string v34, "r"

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v34

    add-int/lit8 v34, v34, 0x1

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    move-object/from16 v0, v32

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 1684
    .local v19, "errorCode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 1685
    add-int/lit8 v34, v20, 0x1

    move-object/from16 v0, v27

    move/from16 v1, v34

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v15, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1688
    .end local v19    # "errorCode":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    move-object/from16 v34, v0

    if-eqz v34, :cond_11

    .line 1689
    new-instance v34, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v35, v0

    invoke-direct/range {v34 .. v35}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1691
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_b

    .line 1692
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_2

    .line 1705
    .end local v32    # "sub":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 1706
    .local v14, "e":Ljava/lang/NumberFormatException;
    sget-object v34, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "text = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1707
    invoke-virtual {v14}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_2

    .line 1670
    .end local v14    # "e":Ljava/lang/NumberFormatException;
    .end local v15    # "en":I
    :cond_10
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v15

    goto/16 :goto_3

    .line 1696
    .restart local v15    # "en":I
    .restart local v32    # "sub":Ljava/lang/String;
    :cond_11
    const/16 v18, 0x1

    .line 1697
    :try_start_1
    new-instance v35, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, v20

    if-le v13, v0, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    move/from16 v34, v0

    :goto_4
    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v34, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move/from16 v2, v20

    move/from16 v3, v34

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1700
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_b

    move/from16 v0, v20

    if-gt v13, v0, :cond_b

    .line 1701
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 1708
    .end local v32    # "sub":Ljava/lang/String;
    :catch_1
    move-exception v14

    .line 1709
    .local v14, "e":Ljava/lang/Exception;
    sget-object v34, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "text = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1710
    sget-object v34, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "output = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1711
    sget-object v34, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "output(length) = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1712
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 1697
    .end local v14    # "e":Ljava/lang/Exception;
    .restart local v32    # "sub":Ljava/lang/String;
    :cond_12
    :try_start_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v34, v0
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_4

    .line 1717
    .end local v15    # "en":I
    .end local v17    # "enter":I
    .end local v32    # "sub":Ljava/lang/String;
    :sswitch_1
    const v13, 0x3b9ac9ff

    .line 1718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    move-object/from16 v34, v0

    if-eqz v34, :cond_15

    .line 1719
    new-instance v35, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, v20

    if-le v13, v0, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    move/from16 v34, v0

    :goto_5
    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v34, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move/from16 v2, v20

    move/from16 v3, v34

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1722
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_13

    move/from16 v0, v20

    if-gt v13, v0, :cond_13

    .line 1723
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1726
    :cond_13
    new-instance v34, Landroid/text/style/AbsoluteSizeSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->MINI_ENTER_SIZE:I

    move/from16 v35, v0

    const/16 v36, 0x0

    invoke-direct/range {v34 .. v36}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1719
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v34, v0

    goto :goto_5

    .line 1730
    :cond_15
    new-instance v35, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, v20

    if-le v13, v0, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    move/from16 v34, v0

    :goto_6
    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v34, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move/from16 v2, v20

    move/from16 v3, v34

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1733
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_16

    move/from16 v0, v20

    if-gt v13, v0, :cond_16

    .line 1734
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1737
    :cond_16
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v34

    add-int/lit8 v35, v20, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_b

    .line 1738
    add-int/lit8 v34, v20, 0x1

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v34

    const/16 v35, 0xa

    move/from16 v0, v34

    move/from16 v1, v35

    if-eq v0, v1, :cond_b

    .line 1739
    new-instance v34, Landroid/text/style/AbsoluteSizeSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->FULL_ENTER_SIZE:I

    move/from16 v35, v0

    const/16 v36, 0x0

    invoke-direct/range {v34 .. v36}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1730
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v34, v0

    goto/16 :goto_6

    .line 1761
    :cond_18
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/popupcalculator/EventHandler;->isArabicCharacter(C)Z

    move-result v34

    if-eqz v34, :cond_1b

    .line 1762
    add-int/lit8 v21, v20, 0x1

    .end local v20    # "i":I
    .local v21, "i":I
    move/from16 v6, v20

    .line 1763
    .local v6, "arabicStart":I
    const/16 v23, 0x0

    .local v23, "lastArabic":I
    move/from16 v20, v21

    .line 1764
    .end local v21    # "i":I
    .restart local v20    # "i":I
    :goto_7
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v34

    move/from16 v0, v20

    move/from16 v1, v34

    if-ge v0, v1, :cond_1a

    .line 1765
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/popupcalculator/EventHandler;->isArabicCharacter(C)Z

    move-result v34

    if-eqz v34, :cond_19

    .line 1766
    add-int/lit8 v20, v20, 0x1

    goto :goto_7

    .line 1768
    :cond_19
    move/from16 v23, v20

    .line 1772
    :cond_1a
    move/from16 v23, v20

    .line 1773
    new-instance v34, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v35, v0

    invoke-direct/range {v34 .. v35}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v35, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v23

    move/from16 v3, v35

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_b

    .line 1777
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v35, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v23

    move/from16 v3, v35

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1783
    .end local v6    # "arabicStart":I
    .end local v23    # "lastArabic":I
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v34

    if-nez v34, :cond_1c

    invoke-static {v10}, Lcom/sec/android/app/popupcalculator/EventHandler;->isIndianChar(C)Z

    move-result v34

    if-nez v34, :cond_1c

    .line 1785
    move/from16 v0, v20

    if-le v0, v13, :cond_1c

    invoke-static {v10}, Ljava/lang/Character;->isLetter(C)Z

    move-result v34

    if-eqz v34, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    move-object/from16 v34, v0

    if-nez v34, :cond_1c

    .line 1787
    new-instance v34, Landroid/text/style/ScaleXSpan;

    const v35, 0x3f59999a    # 0.85f

    invoke-direct/range {v34 .. v35}, Landroid/text/style/ScaleXSpan;-><init>(F)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1791
    :cond_1c
    invoke-static {v10}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOpByTwo(C)Z

    move-result v34

    if-eqz v34, :cond_20

    const/16 v34, 0x5e

    move/from16 v0, v34

    if-eq v10, v0, :cond_20

    .line 1792
    const/16 v34, 0x2d

    move/from16 v0, v34

    if-ne v10, v0, :cond_1e

    .line 1793
    new-instance v35, Landroid/text/style/ForegroundColorSpan;

    if-nez v18, :cond_1d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    move/from16 v34, v0

    :goto_8
    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v34, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move/from16 v2, v20

    move/from16 v3, v34

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1796
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_b

    if-eqz v18, :cond_b

    .line 1797
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1793
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v34, v0

    goto :goto_8

    .line 1802
    :cond_1e
    new-instance v35, Landroid/text/style/ForegroundColorSpan;

    const/16 v34, 0x2d

    move/from16 v0, v34

    if-ne v10, v0, :cond_1f

    move/from16 v0, v20

    if-le v0, v13, :cond_1f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSymbolsColor:I

    move/from16 v34, v0

    :goto_9
    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v34, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move/from16 v2, v20

    move/from16 v3, v34

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    :cond_1f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSymbolsColor:I

    move/from16 v34, v0

    goto :goto_9

    .line 1809
    :cond_20
    new-instance v35, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, v20

    if-le v13, v0, :cond_23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    move/from16 v34, v0

    :goto_a
    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v34, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move/from16 v2, v20

    move/from16 v3, v34

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_21

    move/from16 v0, v20

    if-gt v13, v0, :cond_21

    .line 1813
    new-instance v34, Landroid/text/style/StyleSpan;

    const/16 v35, 0x1

    invoke-direct/range {v34 .. v35}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v35, v20, 0x1

    const/16 v36, 0x21

    move-object/from16 v0, v27

    move-object/from16 v1, v34

    move/from16 v2, v20

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1816
    :cond_21
    invoke-static {v10}, Lcom/sec/android/app/popupcalculator/EventHandler;->isIndianChar(C)Z

    move-result v34

    if-eqz v34, :cond_b

    .line 1817
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    .line 1818
    .local v33, "temp_output":Ljava/lang/String;
    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    .line 1819
    .local v11, "char1":[C
    array-length v0, v11

    move/from16 v28, v0

    .line 1820
    .local v28, "size":I
    move/from16 v0, v28

    new-array v12, v0, [C

    .line 1821
    .local v12, "charNew":[C
    const/16 v26, 0x0

    .line 1823
    .local v26, "newSize":I
    move-object v7, v11

    .local v7, "arr$":[C
    array-length v0, v7

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v22, 0x0

    .local v22, "i$":I
    :goto_b
    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_24

    aget-char v9, v7, v22

    .line 1824
    .local v9, "c":C
    move v5, v9

    .line 1825
    .local v5, "a":I
    const/16 v34, 0x200c

    move/from16 v0, v34

    if-eq v5, v0, :cond_22

    .line 1826
    aput-char v9, v12, v26

    .line 1827
    add-int/lit8 v26, v26, 0x1

    .line 1823
    :cond_22
    add-int/lit8 v22, v22, 0x1

    goto :goto_b

    .line 1809
    .end local v5    # "a":I
    .end local v7    # "arr$":[C
    .end local v9    # "c":C
    .end local v11    # "char1":[C
    .end local v12    # "charNew":[C
    .end local v22    # "i$":I
    .end local v24    # "len$":I
    .end local v26    # "newSize":I
    .end local v28    # "size":I
    .end local v33    # "temp_output":Ljava/lang/String;
    :cond_23
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v34, v0

    goto :goto_a

    .line 1830
    .restart local v7    # "arr$":[C
    .restart local v11    # "char1":[C
    .restart local v12    # "charNew":[C
    .restart local v22    # "i$":I
    .restart local v24    # "len$":I
    .restart local v26    # "newSize":I
    .restart local v28    # "size":I
    .restart local v33    # "temp_output":Ljava/lang/String;
    :cond_24
    new-instance v25, Ljava/lang/String;

    const/16 v34, 0x0

    move/from16 v0, v34

    move/from16 v1, v26

    invoke-static {v12, v0, v1}, Ljava/util/Arrays;->copyOfRange([CII)[C

    move-result-object v34

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    .line 1832
    .local v25, "newOutPut":Ljava/lang/String;
    invoke-virtual/range {v27 .. v27}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->toCharArray()[C

    move-result-object v29

    .line 1833
    .local v29, "spanChar":[C
    if-eqz v29, :cond_b

    .line 1834
    new-instance v30, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1835
    .local v30, "spanString":Landroid/text/SpannableStringBuilder;
    const/16 v34, 0x3d

    move-object/from16 v0, v25

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v31

    .line 1836
    .local v31, "startpoint":I
    const/16 v34, -0x1

    move/from16 v0, v31

    move/from16 v1, v34

    if-ne v0, v1, :cond_25

    .line 1837
    const/16 v31, 0x0

    .line 1838
    :cond_25
    const/16 v16, 0x0

    .line 1839
    .local v16, "endpoint":I
    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v34, v0

    if-eqz v34, :cond_26

    .line 1840
    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v34, v0

    add-int v16, v16, v34

    .line 1841
    new-instance v35, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, v20

    if-le v13, v0, :cond_27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNumberColor:I

    move/from16 v34, v0

    :goto_c
    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v34, 0x21

    move-object/from16 v0, v30

    move-object/from16 v1, v35

    move/from16 v2, v31

    move/from16 v3, v16

    move/from16 v4, v34

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1845
    :cond_26
    move-object/from16 v27, v30

    goto/16 :goto_2

    .line 1841
    :cond_27
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResultColor:I

    move/from16 v34, v0

    goto :goto_c

    .end local v7    # "arr$":[C
    .end local v10    # "ch":C
    .end local v11    # "char1":[C
    .end local v12    # "charNew":[C
    .end local v16    # "endpoint":I
    .end local v22    # "i$":I
    .end local v24    # "len$":I
    .end local v25    # "newOutPut":Ljava/lang/String;
    .end local v26    # "newSize":I
    .end local v28    # "size":I
    .end local v29    # "spanChar":[C
    .end local v30    # "spanString":Landroid/text/SpannableStringBuilder;
    .end local v31    # "startpoint":I
    .end local v33    # "temp_output":Ljava/lang/String;
    :cond_28
    move-object/from16 v34, v27

    .line 1853
    goto/16 :goto_0

    .line 1635
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0x3d -> :sswitch_0
    .end sparse-switch
.end method

.method private changeHistoryColor(Ljava/lang/StringBuilder;)Landroid/text/SpannableStringBuilder;
    .locals 23
    .param p1, "text"    # Ljava/lang/StringBuilder;

    .prologue
    .line 1463
    new-instance v17, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1465
    .local v17, "output":Landroid/text/SpannableStringBuilder;
    const v8, 0x3b9ac9ff

    .line 1466
    .local v8, "count":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v19

    if-nez v19, :cond_1

    .line 1467
    const-string v19, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v17

    .line 1613
    .end local v17    # "output":Landroid/text/SpannableStringBuilder;
    :cond_0
    :goto_0
    return-object v17

    .line 1469
    .restart local v17    # "output":Landroid/text/SpannableStringBuilder;
    :cond_1
    const/4 v12, 0x0

    .line 1471
    .local v12, "equal":Z
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v14, v0, :cond_0

    .line 1472
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v7

    .line 1473
    .local v7, "ch":C
    sparse-switch v7, :sswitch_data_0

    .line 1559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isFocused()Z

    move-result v19

    if-nez v19, :cond_2

    sget-boolean v19, Lcom/sec/android/app/popupcalculator/Calculator;->isLoadingSavedText:Z

    if-eqz v19, :cond_12

    :cond_2
    if-le v14, v8, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    move/from16 v19, v0

    if-eqz v19, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/EditText;->getVisibility()I

    move-result v19

    if-nez v19, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsUpdatingHistory:Z

    move/from16 v19, v0

    if-nez v19, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_12

    .line 1564
    :cond_4
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v20

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 1475
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    move-object/from16 v19, v0

    if-eqz v19, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/EditText;->getVisibility()I

    move-result v19

    if-nez v19, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsUpdatingHistory:Z

    move/from16 v19, v0

    if-nez v19, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_8

    :cond_6
    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    if-eq v14, v0, :cond_8

    .line 1471
    :cond_7
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_1

    .line 1479
    :cond_8
    if-nez v14, :cond_9

    .line 1480
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1482
    .local v6, "beforeResult":Ljava/lang/String;
    const-string v19, "Error"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 1483
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v21

    const/16 v22, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 1491
    .end local v6    # "beforeResult":Ljava/lang/String;
    :cond_9
    move v8, v14

    .line 1492
    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "\n"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v11

    .line 1493
    .local v11, "enter":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v11, v0, :cond_d

    move v10, v11

    .line 1494
    .local v10, "en":I
    :goto_3
    sget-object v19, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "en = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1495
    sget-object v19, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "text(length) = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v19

    move/from16 v0, v19

    if-le v10, v0, :cond_a

    .line 1497
    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    .line 1499
    :cond_a
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v19

    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_b

    .line 1500
    const/16 v19, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1503
    :cond_b
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v10}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v19

    const-string v20, "="

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    .line 1504
    .local v18, "sub":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v19

    if-lez v19, :cond_c

    const-string v19, "Error"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 1505
    const-string v19, "r"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v19

    add-int/lit8 v19, v19, 0x1

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v20

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 1507
    .local v13, "errorCode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 1508
    add-int/lit8 v19, v14, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v10, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1511
    .end local v13    # "errorCode":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    move-object/from16 v19, v0

    if-eqz v19, :cond_e

    .line 1512
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_2

    .line 1522
    .end local v18    # "sub":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 1523
    .local v9, "e":Ljava/lang/NumberFormatException;
    sget-object v19, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "text = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1524
    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_2

    .line 1493
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    .end local v10    # "en":I
    :cond_d
    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    goto/16 :goto_3

    .line 1516
    .restart local v10    # "en":I
    .restart local v18    # "sub":Ljava/lang/String;
    :cond_e
    const/4 v12, 0x1

    .line 1517
    if-gt v8, v14, :cond_7

    .line 1518
    :try_start_1
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 1525
    .end local v18    # "sub":Ljava/lang/String;
    :catch_1
    move-exception v9

    .line 1526
    .local v9, "e":Ljava/lang/Exception;
    sget-object v19, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "text = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1527
    sget-object v19, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "output = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1528
    sget-object v19, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "output(length) = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 1534
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v10    # "en":I
    .end local v11    # "enter":I
    :sswitch_1
    const v8, 0x3b9ac9ff

    .line 1535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    move-object/from16 v19, v0

    if-eqz v19, :cond_10

    .line 1536
    if-gt v8, v14, :cond_f

    .line 1537
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1540
    :cond_f
    new-instance v19, Landroid/text/style/AbsoluteSizeSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->MINI_ENTER_SIZE:I

    move/from16 v20, v0

    const/16 v21, 0x0

    invoke-direct/range {v19 .. v21}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1544
    :cond_10
    if-gt v8, v14, :cond_11

    .line 1545
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1548
    :cond_11
    invoke-virtual/range {v17 .. v17}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v19

    add-int/lit8 v20, v14, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_7

    .line 1549
    add-int/lit8 v19, v14, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v19

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_7

    .line 1550
    new-instance v19, Landroid/text/style/AbsoluteSizeSpan;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->FULL_ENTER_SIZE:I

    move/from16 v20, v0

    const/16 v21, 0x0

    invoke-direct/range {v19 .. v21}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1569
    :cond_12
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->isArabicCharacter(C)Z

    move-result v19

    if-eqz v19, :cond_15

    .line 1570
    add-int/lit8 v15, v14, 0x1

    .end local v14    # "i":I
    .local v15, "i":I
    move v5, v14

    .line 1571
    .local v5, "arabicStart":I
    const/16 v16, 0x0

    .local v16, "lastArabic":I
    move v14, v15

    .line 1572
    .end local v15    # "i":I
    .restart local v14    # "i":I
    :goto_4
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v14, v0, :cond_14

    .line 1573
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->isArabicCharacter(C)Z

    move-result v19

    if-eqz v19, :cond_13

    .line 1574
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 1576
    :cond_13
    move/from16 v16, v14

    .line 1580
    :cond_14
    move/from16 v16, v14

    .line 1581
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v20, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v16

    move/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1588
    .end local v5    # "arabicStart":I
    .end local v16    # "lastArabic":I
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_16

    invoke-static {v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->isIndianChar(C)Z

    move-result v19

    if-nez v19, :cond_16

    .line 1590
    if-le v14, v8, :cond_16

    invoke-static {v7}, Ljava/lang/Character;->isLetter(C)Z

    move-result v19

    if-eqz v19, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    move-object/from16 v19, v0

    if-nez v19, :cond_16

    .line 1592
    new-instance v19, Landroid/text/style/ScaleXSpan;

    const v20, 0x3f59999a    # 0.85f

    invoke-direct/range {v19 .. v20}, Landroid/text/style/ScaleXSpan;-><init>(F)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1596
    :cond_16
    invoke-static {v7}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOpByTwo(C)Z

    move-result v19

    if-eqz v19, :cond_17

    const/16 v19, 0x5e

    move/from16 v0, v19

    if-eq v7, v0, :cond_17

    .line 1597
    const/16 v19, 0x2d

    move/from16 v0, v19

    if-ne v7, v0, :cond_7

    .line 1598
    if-eqz v12, :cond_7

    .line 1599
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1604
    :cond_17
    if-gt v8, v14, :cond_7

    .line 1605
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v20, 0x1

    invoke-direct/range {v19 .. v20}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v20, v14, 0x1

    const/16 v21, 0x21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 1473
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0x3d -> :sswitch_0
    .end sparse-switch
.end method

.method private checkError(Ljava/lang/StringBuilder;)V
    .locals 8
    .param p1, "text"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1094
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v4

    .line 1095
    .local v4, "startCursor":I
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v5

    .line 1096
    .local v5, "endCursor":I
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    .line 1097
    .local v3, "checkChar":C
    const/4 v1, 0x0

    .line 1098
    .local v1, "frontChar":C
    const/4 v2, 0x0

    .line 1100
    .local v2, "nextChar":C
    if-lez v4, :cond_0

    .line 1101
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    add-int/lit8 v6, v4, -0x1

    invoke-interface {v0, v6}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    .line 1104
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v5, v0, :cond_1

    .line 1105
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    .line 1108
    :cond_1
    if-nez v3, :cond_3

    .line 1109
    iput-boolean v7, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 1126
    :cond_2
    :goto_0
    return-void

    :cond_3
    move-object v0, p0

    .line 1113
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->cOperation(CCCII)V

    .line 1115
    invoke-direct {p0, v1, v3, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->cNegativeSign(CCC)V

    .line 1117
    invoke-direct {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->cMaxInput(Ljava/lang/StringBuilder;)V

    .line 1119
    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->cNotStartArithmetic(IC)V

    .line 1121
    invoke-direct {p0, v1, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->cParenthesis(CC)V

    .line 1123
    const/16 v0, 0x2e

    if-ne v3, v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    if-nez v0, :cond_2

    .line 1124
    invoke-direct {p0, v4, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->cDecimal(IC)V

    goto :goto_0
.end method

.method private closeSignParenthesis(Ljava/lang/StringBuilder;I)V
    .locals 3
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "startCursor"    # I

    .prologue
    const/4 v2, 0x0

    .line 3034
    if-eqz p2, :cond_0

    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x28

    if-eq v0, v1, :cond_0

    .line 3035
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    if-eqz v0, :cond_0

    .line 3036
    const-string v0, ")"

    invoke-virtual {p1, p2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 3037
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 3038
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 3042
    :cond_0
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 3043
    return-void
.end method

.method private copy()V
    .locals 8

    .prologue
    const/16 v7, 0x2d

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1863
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1864
    .local v1, "result":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    .line 1865
    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isLargeChar(C)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1866
    :cond_0
    const-string v2, "Error"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Error:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1867
    const-string v2, "r"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1870
    .local v0, "errorCode":I
    const v2, 0x7f0b0009

    if-eq v2, v0, :cond_1

    const v2, 0x7f0b000a

    if-eq v2, v0, :cond_1

    const v2, 0x7f0b0014

    if-eq v2, v0, :cond_1

    const v2, 0x7f0b0015

    if-ne v2, v0, :cond_2

    .line 1871
    :cond_1
    sput-boolean v5, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1874
    .end local v0    # "errorCode":I
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsResultError:Z

    .line 1875
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 1876
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1877
    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 1921
    :cond_3
    :goto_0
    return-void

    .line 1881
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    .line 1882
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->copyFormula(Ljava/lang/StringBuilder;)V

    .line 1883
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 1884
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v7, :cond_5

    .line 1885
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "(\u2212"

    invoke-virtual {v2, v5, v6, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1886
    sput-boolean v6, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1887
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->closeSignParenthesis(Ljava/lang/StringBuilder;I)V

    .line 1890
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1891
    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 1894
    :cond_6
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ltz v2, :cond_3

    .line 1896
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1897
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    .line 1901
    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isLargeChar(C)Z

    move-result v2

    if-nez v2, :cond_7

    iget v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    if-eqz v2, :cond_9

    .line 1903
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 1904
    iput-boolean v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsResultError:Z

    .line 1906
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1907
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 1909
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1910
    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_0

    .line 1913
    :cond_9
    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 1914
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_a

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v7, :cond_a

    .line 1915
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "(\u2212"

    invoke-virtual {v2, v5, v6, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916
    sput-boolean v6, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1918
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1919
    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_0
.end method

.method private copyFormula(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 1924
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormulaClear()V

    .line 1925
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1926
    return-void
.end method

.method private deleteToken(II)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 2230
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-gt p2, v0, :cond_0

    .line 2231
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 2233
    :cond_0
    return-void
.end method

.method private evaluate(Ljava/lang/StringBuilder;Z)Ljava/lang/String;
    .locals 11
    .param p1, "input"    # Ljava/lang/StringBuilder;
    .param p2, "isEnter"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/popupcalculator/SyntaxException;
        }
    .end annotation

    .prologue
    .line 2518
    sget-boolean v8, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v8, :cond_0

    .line 2520
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 2521
    .local v7, "txt":Ljava/lang/String;
    new-instance p1, Ljava/lang/StringBuilder;

    .end local p1    # "input":Ljava/lang/StringBuilder;
    invoke-direct {p1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2524
    .end local v7    # "txt":Ljava/lang/String;
    .restart local p1    # "input":Ljava/lang/StringBuilder;
    :cond_0
    const-string v5, ""

    .line 2527
    .local v5, "result":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/popupcalculator/CExpression;

    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    mul-int/lit8 v9, v9, 0x2

    invoke-direct {v3, v8, v9}, Lcom/sec/android/app/popupcalculator/CExpression;-><init>(Landroid/content/Context;I)V

    .line 2528
    .local v3, "exp":Lcom/sec/android/app/popupcalculator/CExpression;
    invoke-virtual {v3, p1, p2}, Lcom/sec/android/app/popupcalculator/CExpression;->checkInputExp(Ljava/lang/StringBuilder;Z)Z

    move-result v6

    .line 2530
    .local v6, "state":Z
    const-string v8, "-"

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_3

    .line 2531
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-ge v4, v8, :cond_3

    .line 2532
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    const/16 v9, 0x2d

    if-ne v8, v9, :cond_2

    .line 2531
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2534
    :cond_2
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    const/16 v9, 0x2d

    if-ne v8, v9, :cond_1

    add-int/lit8 v8, v4, -0x1

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    const/16 v9, 0x28

    if-eq v8, v9, :cond_1

    .line 2535
    add-int/lit8 v8, v4, 0x1

    const-string v9, "\u2212"

    invoke-virtual {p1, v4, v8, v9}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2539
    .end local v4    # "i":I
    :cond_3
    if-nez v6, :cond_5

    .line 2540
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v8

    const-string v9, "capuccino"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2541
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0016

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2543
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0057

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2546
    :cond_5
    :try_start_0
    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CExpression;->evaluateExp()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/app/popupcalculator/SyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 2608
    return-object v5

    .line 2547
    :catch_0
    move-exception v0

    .line 2548
    .local v0, "e":Lcom/sec/android/app/popupcalculator/SyntaxException;
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 2549
    const/16 v8, 0xf7

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_7

    const/16 v8, 0xf7

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    :goto_2
    iput v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    .line 2552
    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/SyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 2553
    .local v2, "errorMsg":Ljava/lang/String;
    iget v1, v0, Lcom/sec/android/app/popupcalculator/SyntaxException;->message:I

    .line 2555
    .local v1, "error":I
    if-nez v2, :cond_6

    .line 2556
    const-string v2, "null"

    .line 2558
    :cond_6
    const-string v8, "Division by zero"

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2559
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v8

    const-string v9, "capuccino"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2560
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0011

    iget v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2549
    .end local v1    # "error":I
    .end local v2    # "errorMsg":Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    .line 2562
    .restart local v1    # "error":I
    .restart local v2    # "errorMsg":Ljava/lang/String;
    :cond_8
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0006

    iget v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2563
    :cond_9
    const v8, 0x7f0b000a

    if-eq v1, v8, :cond_a

    const v8, 0x7f0b0009

    if-ne v1, v8, :cond_b

    .line 2564
    :cond_a
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0004

    iget v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2565
    :cond_b
    const v8, 0x7f0b000a

    if-ne v1, v8, :cond_c

    .line 2566
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    iget v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v8, v1, v9}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2567
    :cond_c
    const v8, 0x7f0b0015

    if-ne v1, v8, :cond_d

    .line 2568
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    iget v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v8, v1, v9}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2569
    :cond_d
    const v8, 0x7f0b0015

    if-eq v1, v8, :cond_e

    const v8, 0x7f0b0014

    if-ne v1, v8, :cond_f

    .line 2570
    :cond_e
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b000f

    iget v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2572
    :cond_f
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v8

    const-string v9, "capuccino"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 2573
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0016

    iget v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2575
    :cond_10
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0057

    iget v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2577
    .end local v0    # "e":Lcom/sec/android/app/popupcalculator/SyntaxException;
    .end local v1    # "error":I
    .end local v2    # "errorMsg":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 2578
    .local v0, "e":Ljava/lang/Exception;
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 2579
    const/16 v8, 0xf7

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_13

    const/16 v8, 0xf7

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    :goto_3
    iput v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    .line 2582
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 2584
    .restart local v2    # "errorMsg":Ljava/lang/String;
    if-nez v2, :cond_11

    .line 2585
    const-string v2, "null"

    .line 2586
    :cond_11
    const-string v8, "Infinity"

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_17

    .line 2587
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "ln(0)"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_12

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "log(0)"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_15

    .line 2588
    :cond_12
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v8

    const-string v9, "capuccino"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 2589
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0016

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2579
    .end local v2    # "errorMsg":Ljava/lang/String;
    :cond_13
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    goto :goto_3

    .line 2591
    .restart local v2    # "errorMsg":Ljava/lang/String;
    :cond_14
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0057

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2593
    :cond_15
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v8

    const-string v9, "capuccino"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 2594
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0013

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2596
    :cond_16
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v9, 0x7f0b0008

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    .line 2599
    :cond_17
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v8

    const-string v9, "capuccino"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_19

    .line 2600
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const-string v8, "Division by zero"

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_18

    const v8, 0x7f0b0011

    :goto_4
    iget v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v9, v8, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    :cond_18
    const v8, 0x7f0b0016

    goto :goto_4

    .line 2603
    :cond_19
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const-string v8, "Division by zero"

    invoke-virtual {v2, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1a

    const v8, 0x7f0b0006

    :goto_5
    iget v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    invoke-virtual {v9, v8, v10}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v8

    throw v8

    :cond_1a
    const v8, 0x7f0b0057

    goto :goto_5
.end method

.method private fraction(ILjava/lang/String;C)V
    .locals 3
    .param p1, "startCursor"    # I
    .param p2, "input"    # Ljava/lang/String;
    .param p3, "frontChar"    # C

    .prologue
    .line 929
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 930
    .local v1, "text":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 932
    .local v0, "insertString":Ljava/lang/StringBuilder;
    if-nez p1, :cond_0

    .line 933
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 934
    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 943
    :goto_0
    return-void

    .line 937
    :cond_0
    const/16 v2, 0x25

    if-eq p3, v2, :cond_1

    .line 938
    invoke-direct {p0, p3}, Lcom/sec/android/app/popupcalculator/EventHandler;->nextAutoMultiple(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 940
    :cond_1
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 941
    invoke-virtual {v1, p1, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 942
    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method

.method private getCursorPostion(I)I
    .locals 2
    .param p1, "endCursor"    # I

    .prologue
    .line 490
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 491
    .local v0, "len":I
    sub-int p1, v0, p1

    .line 492
    return p1
.end method

.method private getCursorToken(Ljava/lang/String;I)I
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "cursor"    # I

    .prologue
    const/4 v2, -0x1

    const/16 v5, 0x7a

    const/16 v4, 0x61

    .line 2198
    const/4 v1, 0x0

    .line 2200
    .local v1, "tokenPos":I
    add-int/lit8 v3, p2, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2225
    :cond_0
    :goto_0
    return v2

    .line 2202
    :cond_1
    add-int/lit8 v3, p2, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-lt v3, v4, :cond_5

    add-int/lit8 v3, p2, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v3, v5, :cond_5

    .line 2203
    move v0, p2

    .local v0, "i":I
    :goto_1
    if-lez v0, :cond_0

    .line 2204
    add-int/lit8 v1, v0, -0x1

    .line 2205
    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-lt v3, v4, :cond_2

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-le v3, v5, :cond_3

    .line 2206
    :cond_2
    add-int/lit8 v2, v1, 0x1

    goto :goto_0

    .line 2207
    :cond_3
    add-int/lit8 v3, v0, -0x1

    if-nez v3, :cond_4

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-lt v3, v4, :cond_4

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v3, v5, :cond_4

    move v2, v1

    .line 2209
    goto :goto_0

    .line 2203
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 2212
    .end local v0    # "i":I
    :cond_5
    add-int/lit8 v2, p2, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_7

    .line 2213
    const/4 v2, 0x1

    if-le p2, v2, :cond_6

    add-int/lit8 v2, p2, -0x2

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x28

    if-ne v2, v3, :cond_6

    .line 2218
    add-int/lit8 v2, p2, -0x2

    goto :goto_0

    .line 2220
    :cond_6
    add-int/lit8 v2, p2, -0x1

    goto :goto_0

    .line 2223
    :cond_7
    add-int/lit8 v2, p2, -0x1

    goto :goto_0
.end method

.method private getDotCount(Ljava/lang/String;I)I
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "cursor"    # I

    .prologue
    .line 564
    const/4 v1, 0x0

    .line 565
    .local v1, "dotCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 566
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 567
    .local v0, "ch":C
    if-ne p2, v2, :cond_1

    .line 575
    .end local v0    # "ch":C
    :cond_0
    return v1

    .line 571
    .restart local v0    # "ch":C
    :cond_1
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->thousandSepChar()C

    move-result v3

    if-ne v0, v3, :cond_2

    .line 572
    add-int/lit8 v1, v1, 0x1

    .line 565
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static getVersionOfContextProviders(Landroid/content/Context;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 3482
    const/4 v2, -0x1

    .line 3484
    .local v2, "version":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 3486
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3490
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 3487
    :catch_0
    move-exception v0

    .line 3488
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    const-string v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertInput(CCLjava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p1, "frontChar"    # C
    .param p2, "nextChar"    # C
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "displayText"    # Ljava/lang/String;
    .param p5, "startCursor"    # I

    .prologue
    const/16 v5, 0xd7

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 826
    invoke-virtual {p3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 827
    .local v1, "checkChar":C
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 829
    .local v2, "string":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 831
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, v3, p5}, Lcom/sec/android/app/popupcalculator/EventHandler;->decimal(CLjava/lang/String;I)V

    .line 907
    :goto_0
    return-void

    .line 833
    :cond_0
    const-string v3, "("

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 834
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->parenthesis()V

    goto :goto_0

    .line 836
    :cond_1
    const-string v3, "(\u2212"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 837
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->plusMinus()V

    goto :goto_0

    .line 839
    :cond_2
    const-string v3, "1\u00f7"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 840
    invoke-direct {p0, p5, p3, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->fraction(ILjava/lang/String;C)V

    goto :goto_0

    .line 842
    :cond_3
    const-string v3, "abs("

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 843
    invoke-direct {p0, p1, p5}, Lcom/sec/android/app/popupcalculator/EventHandler;->abs(CI)V

    goto :goto_0

    .line 846
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    if-eqz v3, :cond_8

    .line 847
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p5

    .line 852
    :goto_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isNextAutoMultiple(C)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_5
    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isScientific(C)Z

    move-result v3

    if-eqz v3, :cond_a

    if-eqz p1, :cond_a

    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isNextAutoMultiple(C)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {p1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 855
    :cond_6
    iget-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    if-eqz v3, :cond_9

    .line 856
    new-instance v2, Ljava/lang/StringBuilder;

    .end local v2    # "string":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 871
    .restart local v2    # "string":Ljava/lang/StringBuilder;
    :cond_7
    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    goto/16 :goto_0

    .line 849
    :cond_8
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 850
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {p0, v3, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result p5

    goto :goto_1

    .line 859
    :cond_9
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p5, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 861
    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\u00d7"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 862
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 865
    .local v0, "_tempString":Ljava/lang/StringBuilder;
    const-string v3, "\u00d7"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 867
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    goto/16 :goto_0

    .line 872
    .end local v0    # "_tempString":Ljava/lang/StringBuilder;
    :cond_a
    if-ltz p5, :cond_c

    if-eqz p2, :cond_c

    invoke-virtual {p0, p2}, Lcom/sec/android/app/popupcalculator/EventHandler;->isScientific(C)Z

    move-result v3

    if-nez v3, :cond_b

    const/16 v3, 0x28

    if-ne p2, v3, :cond_c

    :cond_b
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 875
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xd7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p5, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 876
    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 877
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultplicationCheck:Z
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 878
    :catch_0
    move-exception v3

    goto/16 :goto_0

    .line 882
    :cond_c
    iget-boolean v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    if-eqz v3, :cond_e

    .line 883
    new-instance v2, Ljava/lang/StringBuilder;

    .end local v2    # "string":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 901
    .restart local v2    # "string":Ljava/lang/StringBuilder;
    :goto_2
    if-ne p5, v7, :cond_d

    const/16 v3, 0x30

    if-ne p1, v3, :cond_d

    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 902
    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 905
    :cond_d
    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    goto/16 :goto_0

    .line 885
    :cond_e
    if-eqz v2, :cond_f

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-gt p5, v3, :cond_f

    .line 886
    invoke-virtual {v2, p5, p3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 889
    :cond_f
    :try_start_1
    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_11

    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v3

    if-nez v3, :cond_10

    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isToken(C)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 892
    :cond_10
    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x2

    const-string v5, "0"

    invoke-virtual {v2, v3, v4, v5}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 897
    :cond_11
    :goto_3
    invoke-direct {p0, v2, p1, p3, p5}, Lcom/sec/android/app/popupcalculator/EventHandler;->insertSignParenthesis(Ljava/lang/StringBuilder;CLjava/lang/String;I)V

    goto :goto_2

    .line 894
    :catch_1
    move-exception v3

    goto :goto_3
.end method

.method public static insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 3461
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getVersionOfContextProviders(Landroid/content/Context;)I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 3479
    :goto_0
    return-void

    .line 3464
    :cond_0
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3469
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3470
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 3471
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3472
    const-string v4, "feature"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3473
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 3474
    sget-object v4, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    const-string v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3475
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 3476
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 3477
    sget-object v4, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private insertSignParenthesis(Ljava/lang/StringBuilder;CLjava/lang/String;I)V
    .locals 2
    .param p1, "string"    # Ljava/lang/StringBuilder;
    .param p2, "frontChar"    # C
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "startCursor"    # I

    .prologue
    .line 947
    sget-boolean v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x2d

    if-eq p2, v1, :cond_3

    .line 948
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 949
    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v1

    if-nez v1, :cond_1

    .line 950
    const/4 v1, 0x1

    invoke-static {p3, v1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->whereLastToken(Ljava/lang/String;Z)I

    move-result v1

    if-gtz v1, :cond_0

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 952
    :cond_0
    invoke-static {p2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 953
    invoke-direct {p0, p1, p4}, Lcom/sec/android/app/popupcalculator/EventHandler;->closeSignParenthesis(Ljava/lang/StringBuilder;I)V

    .line 948
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 956
    :cond_2
    invoke-direct {p0, p1, p4}, Lcom/sec/android/app/popupcalculator/EventHandler;->closeSignParenthesis(Ljava/lang/StringBuilder;I)V

    goto :goto_1

    .line 961
    .end local v0    # "i":I
    :cond_3
    return-void
.end method

.method public static isIndianChar(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1858
    const/16 v0, 0x900

    if-lt p0, v0, :cond_0

    const/16 v0, 0xdff

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isParenthesisAutoMultiple(C)Z
    .locals 1
    .param p1, "frontChar"    # C

    .prologue
    .line 1085
    invoke-static {p1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x65

    if-eq p1, v0, :cond_0

    const/16 v0, 0x21

    if-eq p1, v0, :cond_0

    const/16 v0, 0x25

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3c0

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2c

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2e

    if-eq p1, v0, :cond_0

    const/16 v0, 0x29

    if-ne p1, v0, :cond_1

    .line 1088
    :cond_0
    const/4 v0, 0x1

    .line 1090
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private nextAutoMultiple(C)Ljava/lang/StringBuilder;
    .locals 2
    .param p1, "frontChar"    # C

    .prologue
    .line 910
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 911
    .local v0, "insertString":Ljava/lang/StringBuilder;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isNextAutoMultiple(C)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 912
    :cond_0
    const/16 v1, 0xd7

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 914
    :cond_1
    return-object v0
.end method

.method private plusMinusAutoPar(ILjava/lang/StringBuilder;)V
    .locals 5
    .param p1, "insertPos"    # I
    .param p2, "text"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v4, 0x0

    .line 1068
    const-string v2, "(\u2212"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int v1, p1, v2

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1069
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 1070
    .local v0, "ch":C
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x2e

    if-eq v0, v2, :cond_0

    .line 1072
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x45

    if-ne v2, v3, :cond_1

    .line 1068
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1074
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisCheck:Z

    .line 1075
    const-string v2, ")"

    invoke-virtual {p2, v1, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1076
    sput-boolean v4, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1080
    .end local v0    # "ch":C
    :cond_2
    const-string v2, "("

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 1081
    sput-boolean v4, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1082
    :cond_3
    return-void
.end method

.method private reCalculation()V
    .locals 22

    .prologue
    .line 2634
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 2635
    .local v16, "text":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 2636
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 2638
    .local v17, "tmpResult":Ljava/lang/String;
    const-string v13, ""

    .local v13, "result":Ljava/lang/String;
    const-string v15, ""

    .line 2640
    .local v15, "sub":Ljava/lang/String;
    const/16 v18, 0x1

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->evaluate(Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v13

    .line 2642
    const-string v18, "-"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_2

    .line 2643
    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v18

    const/16 v20, 0x10

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDoubleToString(DI)Ljava/lang/String;

    move-result-object v13

    .line 2647
    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResultFormating(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 2648
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->charAt(I)C

    move-result v18

    const/16 v19, 0x2e

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 2649
    const/16 v18, 0x0

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/app/popupcalculator/SyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 2656
    :cond_0
    :goto_1
    const/4 v5, 0x0

    .line 2657
    .local v5, "digitCount":I
    const-string v18, "."

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_4

    const-string v18, "E"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 2658
    const-string v18, "."

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    add-int/lit8 v9, v18, 0x1

    .local v9, "i":I
    :goto_2
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    if-ge v9, v0, :cond_3

    .line 2659
    invoke-virtual {v13, v9}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 2660
    .local v4, "ch":C
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 2661
    add-int/lit8 v5, v5, 0x1

    .line 2658
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 2645
    .end local v4    # "ch":C
    .end local v5    # "digitCount":I
    .end local v9    # "i":I
    :cond_2
    :try_start_1
    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v18

    const/16 v20, 0xf

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDoubleToString(DI)Ljava/lang/String;
    :try_end_1
    .catch Lcom/sec/android/app/popupcalculator/SyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v13

    goto :goto_0

    .line 2650
    :catch_0
    move-exception v6

    .line 2651
    .local v6, "e":Lcom/sec/android/app/popupcalculator/SyntaxException;
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 2652
    iget v0, v6, Lcom/sec/android/app/popupcalculator/SyntaxException;->position:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    goto :goto_1

    .line 2664
    .end local v6    # "e":Lcom/sec/android/app/popupcalculator/SyntaxException;
    .restart local v5    # "digitCount":I
    .restart local v9    # "i":I
    :cond_3
    const/16 v18, 0xa

    move/from16 v0, v18

    if-le v5, v0, :cond_4

    .line 2666
    new-instance v11, Ljava/text/DecimalFormatSymbols;

    sget-object v18, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 2667
    .local v11, "locale":Ljava/text/DecimalFormatSymbols;
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v18, "0.00000000E+0"

    move-object/from16 v0, v18

    invoke-direct {v8, v0, v11}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 2668
    .local v8, "formatter":Ljava/text/DecimalFormat;
    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v8, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v13

    .line 2672
    .end local v8    # "formatter":Ljava/text/DecimalFormat;
    .end local v9    # "i":I
    .end local v11    # "locale":Ljava/text/DecimalFormatSymbols;
    :cond_4
    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    move/from16 v18, v0

    if-nez v18, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "Error"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_7

    const-string v18, "Error:"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_7

    .line 2674
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 2729
    :cond_6
    :goto_3
    return-void

    .line 2678
    :cond_7
    const/4 v4, 0x0

    .restart local v4    # "ch":C
    const/4 v12, 0x0

    .line 2679
    .local v12, "rear":C
    const/4 v14, 0x0

    .local v14, "start":I
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    .line 2680
    .local v7, "end":I
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_4
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v18

    move/from16 v0, v18

    if-ge v9, v0, :cond_8

    .line 2681
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    .line 2683
    const/16 v18, 0x3d

    move/from16 v0, v18

    if-ne v4, v0, :cond_a

    .line 2684
    move v7, v9

    .line 2697
    :cond_8
    if-eqz v14, :cond_9

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v18

    move/from16 v0, v18

    if-lt v14, v0, :cond_11

    .line 2698
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    goto :goto_3

    .line 2688
    :cond_a
    if-eqz v9, :cond_b

    const/16 v18, 0x28

    move/from16 v0, v18

    if-ne v12, v0, :cond_d

    :cond_b
    const/16 v18, 0x2d

    move/from16 v0, v18

    if-eq v4, v0, :cond_c

    const/16 v18, 0x2b

    move/from16 v0, v18

    if-eq v4, v0, :cond_c

    const/16 v18, 0xa

    move/from16 v0, v18

    if-ne v4, v0, :cond_d

    .line 2689
    :cond_c
    move v12, v4

    .line 2680
    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 2692
    :cond_d
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v18

    if-nez v18, :cond_e

    const/16 v18, 0x5e

    move/from16 v0, v18

    if-ne v4, v0, :cond_10

    :cond_e
    const/16 v18, 0x45

    move/from16 v0, v18

    if-eq v12, v0, :cond_10

    const/16 v18, 0x2b

    move/from16 v0, v18

    if-ne v4, v0, :cond_f

    const/16 v18, 0x2d

    move/from16 v0, v18

    if-eq v4, v0, :cond_10

    .line 2693
    :cond_f
    move v14, v9

    .line 2695
    :cond_10
    move v12, v4

    goto :goto_5

    .line 2701
    :cond_11
    const/4 v4, 0x0

    .line 2702
    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 2704
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v10, v18, -0x1

    .local v10, "j":I
    :goto_6
    if-ltz v10, :cond_12

    .line 2705
    invoke-virtual {v15, v10}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 2706
    const/16 v18, 0x29

    move/from16 v0, v18

    if-eq v4, v0, :cond_13

    .line 2711
    :cond_12
    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 2712
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v18

    if-nez v18, :cond_14

    .line 2713
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    goto/16 :goto_3

    .line 2709
    :cond_13
    add-int/lit8 v7, v7, -0x1

    .line 2704
    add-int/lit8 v10, v10, -0x1

    goto :goto_6

    .line 2717
    :cond_14
    sget-boolean v18, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v18, :cond_15

    .line 2718
    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v20

    sub-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->abs(D)D

    move-result-wide v18

    const-wide v20, 0x3e112e0be826d695L    # 1.0E-9

    cmpg-double v18, v18, v20

    if-gez v18, :cond_6

    .line 2720
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->copy()V

    .line 2721
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2724
    :cond_15
    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v20

    sub-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->abs(D)D

    move-result-wide v18

    const-wide v20, 0x3e112e0be826d695L    # 1.0E-9

    cmpg-double v18, v18, v20

    if-gez v18, :cond_6

    .line 2725
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->copy()V

    .line 2726
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3
.end method

.method private repairDisplay()V
    .locals 5

    .prologue
    .line 2612
    const/4 v1, 0x0

    .line 2613
    .local v1, "par_num":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2615
    .local v2, "text":Ljava/lang/StringBuilder;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_2

    .line 2616
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    const/16 v4, 0x29

    if-ne v3, v4, :cond_0

    .line 2617
    add-int/lit8 v1, v1, -0x1

    .line 2619
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    const/16 v4, 0x28

    if-ne v3, v4, :cond_1

    .line 2620
    add-int/lit8 v1, v1, 0x1

    .line 2615
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2624
    :cond_2
    if-nez v1, :cond_4

    .line 2631
    :cond_3
    return-void

    .line 2628
    :cond_4
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    .line 2629
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private resultFormating(Ljava/lang/String;I)Ljava/lang/String;
    .locals 26
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "round"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/popupcalculator/SyntaxException;
        }
    .end annotation

    .prologue
    .line 2399
    const-wide/16 v6, 0x0

    .line 2400
    .local v6, "dAbsValue":D
    const-string v18, ""

    .line 2401
    .local v18, "strAbsValue":Ljava/lang/String;
    const/4 v14, -0x1

    .line 2402
    .local v14, "nOriEPos":I
    const/16 v16, -0x1

    .line 2403
    .local v16, "nTmpDotPos":I
    const/16 v17, 0x0

    .line 2404
    .local v17, "nTmpIntNumLen":I
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 2405
    .local v19, "strRtn":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    .line 2407
    .local v9, "errorCursor":I
    const/16 v21, 0x1

    move/from16 v0, p2

    move/from16 v1, v21

    if-ge v0, v1, :cond_0

    .line 2513
    .end local p1    # "text":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 2411
    .restart local p1    # "text":Ljava/lang/String;
    :cond_0
    :try_start_0
    const-string v21, "E"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v14

    .line 2412
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v14, v0, :cond_4

    .line 2413
    invoke-static/range {p1 .. p1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x10

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_3

    .line 2414
    invoke-static/range {p1 .. p1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2416
    const-string v21, "."

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    .line 2417
    const/16 v21, -0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-eq v0, v1, :cond_2

    .line 2418
    :goto_1
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v21

    add-int/lit8 v21, v21, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v21

    const/16 v22, 0x30

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    .line 2419
    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 2420
    .local v20, "strTmp":Ljava/lang/String;
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2421
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2434
    .end local v20    # "strTmp":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 2435
    .local v8, "e":Ljava/lang/Exception;
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 2436
    const/16 v21, 0xf7

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_5

    const/16 v21, 0xf7

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    add-int/lit8 v9, v21, 0x1

    .line 2439
    :goto_2
    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    .line 2441
    .local v10, "errorMsg":Ljava/lang/String;
    if-nez v10, :cond_1

    .line 2442
    const-string v10, "null"

    .line 2443
    :cond_1
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v21

    const-string v22, "capuccino"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 2444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v22, v0

    const-string v21, "Division by zero"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_6

    const v21, 0x7f0b0011

    :goto_3
    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v21

    throw v21

    .line 2424
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v10    # "errorMsg":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 2427
    :cond_3
    const/16 v21, 0x0

    add-int/lit8 v22, v14, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v6

    .line 2429
    invoke-static {v6, v7}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v18

    .line 2452
    :goto_4
    const-string v21, "."

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    .line 2453
    const/16 v21, -0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-eq v0, v1, :cond_9

    .line 2454
    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v17

    .line 2458
    :goto_5
    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0x30

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 2459
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_6
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    if-ge v11, v0, :cond_b

    .line 2460
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 2461
    .local v5, "cVal":C
    const/16 v21, 0x2e

    move/from16 v0, v21

    if-ne v5, v0, :cond_a

    .line 2459
    :goto_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 2431
    .end local v5    # "cVal":C
    .end local v11    # "i":I
    :cond_4
    :try_start_2
    invoke-static/range {p1 .. p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v6

    .line 2432
    invoke-static {v6, v7}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v18

    goto :goto_4

    .line 2436
    .restart local v8    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v21

    add-int/lit8 v9, v21, -0x1

    goto/16 :goto_2

    .line 2444
    .restart local v10    # "errorMsg":Ljava/lang/String;
    :cond_6
    const v21, 0x7f0b0016

    goto/16 :goto_3

    .line 2448
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v22, v0

    const-string v21, "Division by zero"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_8

    const v21, 0x7f0b0006

    :goto_8
    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v21

    throw v21

    :cond_8
    const v21, 0x7f0b0057

    goto :goto_8

    .line 2456
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v10    # "errorMsg":Ljava/lang/String;
    :cond_9
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v17

    goto :goto_5

    .line 2463
    .restart local v5    # "cVal":C
    .restart local v11    # "i":I
    :cond_a
    const/16 v21, 0x30

    move/from16 v0, v21

    if-ne v5, v0, :cond_b

    .line 2464
    add-int/lit8 v17, v17, -0x1

    goto :goto_7

    .line 2470
    .end local v5    # "cVal":C
    .end local v11    # "i":I
    :cond_b
    add-int v15, p2, v17

    .line 2471
    .local v15, "nParam":I
    add-int v21, p2, v17

    if-gtz v21, :cond_c

    .line 2472
    move/from16 v15, p2

    .line 2473
    :cond_c
    new-instance v12, Ljava/math/MathContext;

    sget-object v21, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    move-object/from16 v0, v21

    invoke-direct {v12, v15, v0}, Ljava/math/MathContext;-><init>(ILjava/math/RoundingMode;)V

    .line 2474
    .local v12, "mc":Ljava/math/MathContext;
    sget-object v4, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    .line 2475
    .local v4, "bigTemp":Ljava/math/BigDecimal;
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v14, v0, :cond_e

    .line 2476
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/math/BigDecimal;->round(Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 2480
    :goto_9
    invoke-virtual {v4}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2481
    invoke-virtual {v4}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v22

    invoke-virtual {v4}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v24, v0

    cmpl-double v21, v22, v24

    if-nez v21, :cond_d

    const-string v21, "."

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v21

    if-lez v21, :cond_d

    .line 2483
    const/16 v21, 0x0

    const-string v22, "."

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 2484
    .restart local v20    # "strTmp":Ljava/lang/String;
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2485
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2488
    .end local v20    # "strTmp":Ljava/lang/String;
    :cond_d
    const-string v21, "."

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    .line 2489
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v14, v0, :cond_12

    .line 2491
    const/16 v21, -0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-eq v0, v1, :cond_f

    .line 2492
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v21

    sub-int v21, v21, v16

    add-int/lit8 v13, v21, -0x1

    .line 2493
    .local v13, "nDotNum":I
    sub-int v11, p2, v13

    .restart local v11    # "i":I
    :goto_a
    if-lez v11, :cond_10

    .line 2494
    const-string v21, "0"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2493
    add-int/lit8 v11, v11, -0x1

    goto :goto_a

    .line 2478
    .end local v11    # "i":I
    .end local v13    # "nDotNum":I
    :cond_e
    invoke-static/range {p1 .. p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/math/BigDecimal;->round(Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v4

    goto/16 :goto_9

    .line 2496
    :cond_f
    const/4 v13, 0x0

    .line 2497
    .restart local v13    # "nDotNum":I
    const/16 v21, 0x2e

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2498
    sub-int v11, p2, v13

    .restart local v11    # "i":I
    :goto_b
    if-lez v11, :cond_10

    .line 2499
    const-string v21, "0"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2498
    add-int/lit8 v11, v11, -0x1

    goto :goto_b

    .line 2502
    :cond_10
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2513
    .end local v11    # "i":I
    .end local v13    # "nDotNum":I
    :cond_11
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 2504
    :cond_12
    const/16 v21, -0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-eq v0, v1, :cond_11

    .line 2505
    :goto_c
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v21

    add-int/lit8 v21, v21, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v21

    const/16 v22, 0x30

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_11

    .line 2506
    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 2507
    .restart local v20    # "strTmp":Ljava/lang/String;
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2508
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_c
.end method

.method private setFormulaClear()V
    .locals 3

    .prologue
    .line 530
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 533
    :cond_0
    return-void
.end method

.method private setResult(Ljava/lang/StringBuilder;Ljava/lang/String;IZ)V
    .locals 11
    .param p1, "text"    # Ljava/lang/StringBuilder;
    .param p2, "result"    # Ljava/lang/String;
    .param p3, "error"    # I
    .param p4, "enter"    # Z

    .prologue
    const/16 v10, 0x64

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1292
    sget-boolean v5, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v5, :cond_0

    .line 1295
    invoke-static {p2, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p2

    .line 1298
    :cond_0
    iput p3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    .line 1299
    if-eqz p3, :cond_9

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1300
    .local v3, "msg":Ljava/lang/String;
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {p0, v5, v8}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursorPostion(I)I

    move-result v0

    .line 1301
    .local v0, "cursorPos":I
    sparse-switch p3, :sswitch_data_0

    .line 1332
    :goto_1
    if-nez p3, :cond_1

    const-string v5, "Infinity"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "NaN"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1333
    :cond_1
    const-string v5, "Infinity"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "NaN"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1334
    :cond_2
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v5

    const-string v6, "capuccino"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1335
    const p3, 0x7f0b0016

    .line 1338
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1339
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    .line 1342
    :cond_3
    if-eqz p3, :cond_4

    .line 1343
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 1345
    :cond_4
    if-eqz p3, :cond_b

    .line 1347
    :goto_3
    const-string v5, "%d"

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_7

    .line 1348
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0005

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eq v3, v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0010

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-ne v3, v5, :cond_c

    .line 1350
    :cond_5
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\n"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v10, :cond_6

    .line 1356
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    const/16 v6, 0x64

    invoke-virtual {p1, v5, v6}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .end local p1    # "text":Ljava/lang/StringBuilder;
    .local v4, "text":Ljava/lang/StringBuilder;
    move-object p1, v4

    .line 1360
    .end local v4    # "text":Ljava/lang/StringBuilder;
    .restart local p1    # "text":Ljava/lang/StringBuilder;
    :goto_4
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 1363
    :cond_6
    const-string v5, "%d"

    const-string v6, "100"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 1368
    :cond_7
    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1369
    .local v2, "fma":Ljava/lang/StringBuilder;
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1370
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v9, v5, v6}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1371
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\n="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1373
    if-eqz p3, :cond_e

    .line 1375
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-direct {p0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/StringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v8

    invoke-interface {v5, v6, v7, v8}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1376
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 1377
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1384
    :goto_6
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 1385
    .end local v2    # "fma":Ljava/lang/StringBuilder;
    :cond_8
    :goto_7
    return-void

    .end local v0    # "cursorPos":I
    .end local v3    # "msg":Ljava/lang/String;
    :cond_9
    move-object v3, p2

    .line 1299
    goto/16 :goto_0

    .line 1304
    .restart local v0    # "cursorPos":I
    .restart local v3    # "msg":Ljava/lang/String;
    :sswitch_0
    invoke-virtual {p0, p1, v8}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1305
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto :goto_7

    .line 1309
    :sswitch_1
    const-string v5, "Debug"

    const-string v6, "MAX_COMMA_ERROR"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 1310
    invoke-virtual {p0, p1, v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1312
    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 1315
    if-eqz p4, :cond_8

    goto/16 :goto_1

    .line 1320
    :sswitch_2
    invoke-virtual {p0, p1, v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1321
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto :goto_7

    .line 1326
    :sswitch_3
    invoke-virtual {p0, p1, v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1327
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_1

    .line 1337
    :cond_a
    const p3, 0x7f0b0057

    goto/16 :goto_2

    .line 1345
    :cond_b
    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeNumFomat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 1357
    :catch_0
    move-exception v1

    .line 1358
    .local v1, "e":Ljava/lang/StringIndexOutOfBoundsException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {p1, v9, v5}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .end local p1    # "text":Ljava/lang/StringBuilder;
    .restart local v4    # "text":Ljava/lang/StringBuilder;
    move-object p1, v4

    .end local v4    # "text":Ljava/lang/StringBuilder;
    .restart local p1    # "text":Ljava/lang/StringBuilder;
    goto/16 :goto_4

    .line 1364
    .end local v1    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0007

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eq v3, v5, :cond_d

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0012

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-ne v3, v5, :cond_7

    .line 1365
    :cond_d
    const-string v5, "%d"

    const-string v6, "20"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 1378
    .restart local v2    # "fma":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v1

    .line 1379
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    invoke-interface {v5, v9, v6, v7}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto/16 :goto_6

    .line 1382
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_e
    invoke-virtual {p0, v2, v8}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    goto/16 :goto_6

    .line 1301
    :sswitch_data_0
    .sparse-switch
        0x7f0b0001 -> :sswitch_1
        0x7f0b0002 -> :sswitch_3
        0x7f0b0003 -> :sswitch_0
        0x7f0b000c -> :sswitch_1
        0x7f0b000d -> :sswitch_3
        0x7f0b000e -> :sswitch_0
        0x7f0b0017 -> :sswitch_2
        0x7f0b0058 -> :sswitch_2
    .end sparse-switch
.end method

.method private setResultClear()V
    .locals 3

    .prologue
    .line 536
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 539
    :cond_0
    return-void
.end method


# virtual methods
.method public autoTextSize(Ljava/lang/String;)V
    .locals 12
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3195
    move-object v7, p1

    .line 3196
    .local v7, "str":Ljava/lang/String;
    move-object v5, p1

    .line 3199
    .local v5, "pre_str":Ljava/lang/String;
    move-object v3, p1

    .line 3201
    .local v3, "full_str":Ljava/lang/String;
    const/16 v9, 0x3d

    invoke-virtual {v7, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_0

    .line 3202
    const/16 v9, 0x3d

    invoke-virtual {v7, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v7, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 3203
    const/4 v9, 0x0

    const/16 v10, 0x3d

    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 3206
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v4, v9, Landroid/content/res/Configuration;->orientation:I

    .line 3232
    .local v4, "ori":I
    const/4 v9, 0x1

    if-ne v4, v9, :cond_4

    .line 3233
    sget v9, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_LARGE_PORT:I

    int-to-float v0, v9

    .line 3234
    .local v0, "Textsize_Large":F
    sget v9, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_MEDIUM_PORT:I

    int-to-float v1, v9

    .line 3235
    .local v1, "Textsize_Medium":F
    sget v9, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_SMALL_PORT:I

    int-to-float v2, v9

    .line 3243
    .local v2, "Textsize_Small":F
    :goto_0
    iget v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->SelectedTextSize:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_1

    .line 3244
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    iget v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->SelectedTextSize:F

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    .line 3246
    :cond_1
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    .line 3248
    iget v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->SelectedTextSize:F

    cmpl-float v9, v9, v2

    if-nez v9, :cond_7

    .line 3249
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "0"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 3250
    .local v8, "str_temp":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "0"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 3251
    .local v6, "pre_str_temp":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v9

    if-nez v9, :cond_5

    .line 3252
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_2

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 3258
    :cond_2
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    .line 3348
    .end local v6    # "pre_str_temp":Ljava/lang/String;
    .end local v8    # "str_temp":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void

    .line 3237
    .end local v0    # "Textsize_Large":F
    .end local v1    # "Textsize_Medium":F
    .end local v2    # "Textsize_Small":F
    :cond_4
    sget v9, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_LARGE_LAND:I

    int-to-float v0, v9

    .line 3238
    .restart local v0    # "Textsize_Large":F
    sget v9, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_MEDIUM_LAND:I

    int-to-float v1, v9

    .line 3239
    .restart local v1    # "Textsize_Medium":F
    sget v9, Lcom/sec/android/app/popupcalculator/EventHandler;->TEXT_SIZE_SMALL_LAND:I

    int-to-float v2, v9

    .restart local v2    # "Textsize_Small":F
    goto/16 :goto_0

    .line 3261
    .restart local v6    # "pre_str_temp":Ljava/lang/String;
    .restart local v8    # "str_temp":Ljava/lang/String;
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_6

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 3267
    :cond_6
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    goto :goto_1

    .line 3273
    .end local v6    # "pre_str_temp":Ljava/lang/String;
    .end local v8    # "str_temp":Ljava/lang/String;
    :cond_7
    iget v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->SelectedTextSize:F

    cmpl-float v9, v9, v0

    if-nez v9, :cond_f

    .line 3274
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v9

    if-nez v9, :cond_b

    .line 3275
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_8

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_a

    .line 3281
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    .line 3282
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_9

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 3288
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    .line 3289
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    goto/16 :goto_1

    .line 3295
    :cond_a
    const/16 v9, 0x3d

    invoke-virtual {v3, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_3

    const/16 v9, 0xa

    invoke-virtual {v3, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_3

    iget v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIswvga:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 3297
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    goto/16 :goto_1

    .line 3301
    :cond_b
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_c

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_e

    .line 3305
    :cond_c
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    .line 3306
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_d

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 3312
    :cond_d
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    .line 3313
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    goto/16 :goto_1

    .line 3319
    :cond_e
    const/16 v9, 0x3d

    invoke-virtual {v3, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_3

    const/16 v9, 0xa

    invoke-virtual {v3, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_3

    iget v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIswvga:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 3321
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    goto/16 :goto_1

    .line 3325
    :cond_f
    iget v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->SelectedTextSize:F

    cmpl-float v9, v9, v1

    if-nez v9, :cond_3

    .line 3326
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v9

    if-nez v9, :cond_11

    .line 3327
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_10

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 3333
    :cond_10
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    .line 3334
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    goto/16 :goto_1

    .line 3338
    :cond_11
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_12

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getMeasuredWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingRight()I

    move-result v11

    sub-int/2addr v10, v11

    iget-object v11, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 3342
    :cond_12
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTextSize(IF)V

    .line 3343
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    goto/16 :goto_1
.end method

.method public cancelToast()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    goto :goto_0
.end method

.method public changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1617
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1618
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/StringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    return-object v1
.end method

.method public changeHistoryColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1458
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1459
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeHistoryColor(Ljava/lang/StringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    return-object v1
.end method

.method public changeNumFomat(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1388
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_1

    .line 1389
    const-string p1, ""

    .line 1447
    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 1392
    .restart local p1    # "text":Ljava/lang/String;
    :cond_1
    const-string v10, "NaN"

    invoke-virtual {p1, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1393
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v10

    const-string v11, "capuccino"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1394
    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0016

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1396
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0057

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1399
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1400
    .local v6, "sBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 1402
    .local v2, "end":I
    const-string v10, "E"

    invoke-virtual {p1, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 1406
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    if-gt v4, v10, :cond_5

    .line 1407
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    if-eq v4, v10, :cond_4

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0x2d

    if-eq v10, v11, :cond_8

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v10

    if-nez v10, :cond_8

    .line 1409
    :cond_4
    move v2, v4

    .line 1414
    :cond_5
    const/4 v10, 0x0

    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0x2d

    if-ne v10, v11, :cond_9

    const/4 v7, 0x1

    .line 1415
    .local v7, "start":I
    :goto_2
    const/4 v10, 0x1

    if-ne v7, v10, :cond_6

    .line 1416
    const-string v10, "-"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1419
    :cond_6
    invoke-virtual {p1, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 1420
    .local v8, "subtext":Ljava/lang/String;
    const-string v10, "Infinity"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "NaN"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1422
    :cond_7
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v10

    const-string v11, "capuccino"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1423
    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0016

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 1406
    .end local v7    # "start":I
    .end local v8    # "subtext":Ljava/lang/String;
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1414
    :cond_9
    const/4 v7, 0x0

    goto :goto_2

    .line 1425
    .restart local v7    # "start":I
    .restart local v8    # "subtext":Ljava/lang/String;
    :cond_a
    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0057

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 1428
    :cond_b
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v10}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v5

    .line 1430
    .local v5, "nf":Ljava/text/NumberFormat;
    instance-of v10, v5, Ljava/text/DecimalFormat;

    if-eqz v10, :cond_c

    move-object v0, v5

    .line 1432
    check-cast v0, Ljava/text/DecimalFormat;

    .line 1433
    .local v0, "df":Ljava/text/DecimalFormat;
    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v9

    .line 1434
    .local v9, "symbols":Ljava/text/DecimalFormatSymbols;
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v10

    invoke-virtual {v9, v10}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 1435
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->thousandSepChar()C

    move-result v10

    invoke-virtual {v9, v10}, Ljava/text/DecimalFormatSymbols;->setGroupingSeparator(C)V

    .line 1436
    invoke-virtual {v0, v9}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    .line 1440
    .end local v0    # "df":Ljava/text/DecimalFormat;
    .end local v9    # "symbols":Ljava/text/DecimalFormatSymbols;
    :cond_c
    :try_start_0
    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1444
    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1445
    .local v3, "fx":Ljava/lang/String;
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1446
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1447
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 1441
    .end local v3    # "fx":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1442
    .local v1, "e":Ljava/lang/Exception;
    goto/16 :goto_0
.end method

.method public checkInput(Ljava/lang/StringBuilder;I)V
    .locals 10
    .param p1, "fma"    # Ljava/lang/StringBuilder;
    .param p2, "cursorPos"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 1221
    const/4 v2, 0x0

    .line 1223
    .local v2, "error":I
    new-instance v3, Lcom/sec/android/app/popupcalculator/CExpression;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/sec/android/app/popupcalculator/CExpression;-><init>(Landroid/content/Context;I)V

    .line 1224
    .local v3, "exp":Lcom/sec/android/app/popupcalculator/CExpression;
    const/4 v0, 0x0

    .line 1226
    .local v0, "ch":C
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 1227
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 1230
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v5

    if-nez v5, :cond_1

    const/16 v5, 0x2d

    if-ne v0, v5, :cond_2

    :cond_1
    if-nez v0, :cond_3

    .line 1231
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 1234
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1235
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1237
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_5

    .line 1287
    :cond_4
    :goto_0
    return-void

    .line 1239
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-ne v5, v7, :cond_a

    .line 1240
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1257
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->handleTestmodeSecretCode(Landroid/content/Context;Ljava/lang/String;)V

    .line 1260
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/popupcalculator/CExpression;->checkInputExp(Ljava/lang/StringBuilder;Z)Z
    :try_end_0
    .catch Lcom/sec/android/app/popupcalculator/SyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1268
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CExpression;->getMaxDigitToast()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1269
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b005c

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v8, 0xf

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->showToast(Ljava/lang/String;)V

    .line 1274
    :cond_7
    :goto_2
    iget v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    if-gez v5, :cond_8

    .line 1275
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    .line 1278
    :cond_8
    const v5, 0x7f0b008a

    if-ne v5, v2, :cond_9

    .line 1279
    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 1282
    :cond_9
    iget-boolean v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    if-eqz v5, :cond_4

    .line 1283
    invoke-direct {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->copyFormula(Ljava/lang/StringBuilder;)V

    .line 1284
    const-string v5, ""

    invoke-direct {p0, p1, v5, v2, v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResult(Ljava/lang/StringBuilder;Ljava/lang/String;IZ)V

    goto :goto_0

    .line 1243
    :cond_a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_b

    .line 1244
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\u221a("

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    goto :goto_0

    .line 1247
    :cond_b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_c

    .line 1248
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "e^("

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ln("

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    goto/16 :goto_0

    .line 1251
    :cond_c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_6

    .line 1252
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "sin("

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "cos("

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "tan("

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    goto/16 :goto_0

    .line 1261
    :catch_0
    move-exception v1

    .line 1262
    .local v1, "e":Lcom/sec/android/app/popupcalculator/SyntaxException;
    iput-boolean v7, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 1263
    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 1264
    iget v2, v1, Lcom/sec/android/app/popupcalculator/SyntaxException;->message:I

    .line 1265
    iget v5, v1, Lcom/sec/android/app/popupcalculator/SyntaxException;->position:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    goto/16 :goto_1

    .line 1271
    .end local v1    # "e":Lcom/sec/android/app/popupcalculator/SyntaxException;
    :cond_d
    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CExpression;->getMaxDotToast()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1272
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b005b

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v8, 0xa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->showToast(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 514
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormulaClear()V

    .line 515
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 516
    iput-boolean v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 517
    iput-boolean v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 518
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->clearmOrigin()V

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 520
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 521
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->clearBackup()V

    .line 522
    return-void
.end method

.method public clearBackup()V
    .locals 3

    .prologue
    .line 3357
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaLengthBackup:I

    .line 3359
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3360
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 3362
    :cond_0
    return-void
.end method

.method public decimal(CLjava/lang/String;I)V
    .locals 8
    .param p1, "frontChar"    # C
    .param p2, "displayText"    # Ljava/lang/String;
    .param p3, "startCursor"    # I

    .prologue
    .line 2854
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2855
    .local v4, "text":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 2856
    .local v3, "mMultipleDecimal":Z
    move v5, p3

    .local v5, "y":I
    :goto_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 2857
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 2859
    .local v0, "c":C
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v6

    if-ne v0, v6, :cond_4

    .line 2861
    const/4 v3, 0x1

    .line 2866
    .end local v0    # "c":C
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v6

    if-nez v6, :cond_1

    if-nez p3, :cond_6

    .line 2867
    :cond_1
    if-nez v3, :cond_2

    .line 2868
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_5

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v6

    const/16 v7, 0x28

    if-ne v6, v7, :cond_5

    .line 2882
    :cond_2
    :goto_1
    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 2920
    :cond_3
    :goto_2
    return-void

    .line 2863
    .restart local v0    # "c":C
    :cond_4
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2856
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2877
    .end local v0    # "c":C
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, p3, v6}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 2879
    add-int/lit8 v6, p3, 0x1

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteDotCount(ILjava/lang/String;)V

    goto :goto_1

    .line 2886
    :cond_6
    const/4 v1, 0x0

    .line 2888
    .local v1, "flag":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    if-ge v2, p3, :cond_a

    .line 2890
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v6

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v7

    if-ne v6, v7, :cond_7

    if-nez v1, :cond_3

    .line 2895
    :cond_7
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isToken(C)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2896
    const/4 v1, 0x0

    .line 2888
    :cond_8
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2898
    :cond_9
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v6

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v7

    if-ne v6, v7, :cond_8

    .line 2900
    const/4 v1, 0x1

    goto :goto_4

    .line 2904
    :cond_a
    if-nez v1, :cond_d

    .line 2905
    const/16 v6, 0x29

    if-eq p1, v6, :cond_b

    const/16 v6, 0x21

    if-eq p1, v6, :cond_b

    const/16 v6, 0x65

    if-eq p1, v6, :cond_b

    const/16 v6, 0x3c0

    if-ne p1, v6, :cond_c

    .line 2907
    :cond_b
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\u00d70"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, p3, v6}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 2909
    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    goto/16 :goto_2

    .line 2912
    :cond_c
    if-nez v3, :cond_d

    .line 2914
    invoke-static {p1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v6

    if-eqz v6, :cond_e

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_5
    invoke-virtual {v4, p3, v6}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 2916
    add-int/lit8 v6, p3, 0x1

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteDotCount(ILjava/lang/String;)V

    .line 2919
    :cond_d
    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    goto/16 :goto_2

    .line 2914
    :cond_e
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_5
.end method

.method public deleteDotCount(ILjava/lang/String;)V
    .locals 4
    .param p1, "startCursor"    # I
    .param p2, "displayText"    # Ljava/lang/String;

    .prologue
    .line 2923
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteDotCount:I

    .line 2924
    const/4 v0, 0x0

    .line 2925
    .local v0, "count":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2926
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigitDot(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2927
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2c

    if-ne v2, v3, :cond_0

    .line 2928
    add-int/lit8 v0, v0, 0x1

    .line 2925
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2934
    :cond_1
    sput v0, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteDotCount:I

    .line 2935
    return-void
.end method

.method public getCursor(Landroid/widget/EditText;I)I
    .locals 4
    .param p1, "et"    # Landroid/widget/EditText;
    .param p2, "position"    # I

    .prologue
    .line 458
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v2

    .line 459
    .local v2, "start":I
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 461
    .local v1, "end":I
    const/4 v0, 0x0

    .line 462
    .local v0, "cursor":I
    if-nez p2, :cond_1

    .line 463
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 467
    :cond_0
    :goto_0
    return v0

    .line 464
    :cond_1
    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    .line 465
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public getDeleteDot(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x3d

    .line 579
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 580
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v0, ""

    .line 582
    .local v0, "mTempText":Ljava/lang/String;
    const-string v2, "="

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 583
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 584
    const/4 v2, 0x0

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 587
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->thousandSepChar()C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 589
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 590
    invoke-virtual {p1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 592
    :cond_1
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getDeleteEquals(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 600
    const-string v2, "="

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 601
    .local v0, "idx":I
    if-lez v0, :cond_0

    .line 602
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    .line 604
    .end local p1    # "text":Ljava/lang/String;
    .local v1, "text":Ljava/lang/String;
    :goto_0
    return-object v1

    .end local v1    # "text":Ljava/lang/String;
    .restart local p1    # "text":Ljava/lang/String;
    :cond_0
    move-object v1, p1

    .end local p1    # "text":Ljava/lang/String;
    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method public getDeleteNewLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 608
    const-string v2, "\n"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 609
    .local v0, "idx":I
    const-string v2, "\n"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 610
    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 611
    :cond_0
    if-lez v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_1

    .line 612
    const/4 v2, 0x0

    const-string v3, "\n"

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    .line 614
    .end local p1    # "text":Ljava/lang/String;
    .local v1, "text":Ljava/lang/String;
    :goto_0
    return-object v1

    .end local v1    # "text":Ljava/lang/String;
    .restart local p1    # "text":Ljava/lang/String;
    :cond_1
    move-object v1, p1

    .end local p1    # "text":Ljava/lang/String;
    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method public getDoubleToString(D)Ljava/lang/String;
    .locals 17
    .param p1, "v"    # D

    .prologue
    .line 2274
    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 2275
    .local v0, "absv":D
    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v11

    .line 2277
    .local v11, "str":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2278
    .local v2, "buf":Ljava/lang/StringBuffer;
    const/16 v10, 0xa

    .line 2280
    .local v10, "roundingStart":I
    const/16 v13, 0x45

    invoke-virtual {v11, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 2281
    .local v5, "ePos":I
    const/4 v13, -0x1

    if-eq v5, v13, :cond_1

    add-int/lit8 v13, v5, 0x1

    invoke-virtual {v11, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 2283
    .local v6, "exp":I
    :goto_0
    const/4 v13, -0x1

    if-eq v5, v13, :cond_0

    .line 2284
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 2286
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    .line 2288
    .local v8, "len":I
    const/4 v3, 0x0

    .local v3, "dotPos":I
    :goto_1
    if-ge v3, v8, :cond_2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x2e

    if-eq v13, v14, :cond_2

    .line 2289
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2281
    .end local v3    # "dotPos":I
    .end local v6    # "exp":I
    .end local v8    # "len":I
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 2291
    .restart local v3    # "dotPos":I
    .restart local v6    # "exp":I
    .restart local v8    # "len":I
    :cond_2
    add-int/2addr v6, v3

    .line 2293
    if-ge v3, v8, :cond_3

    .line 2294
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 2295
    add-int/lit8 v8, v8, -0x1

    .line 2297
    :cond_3
    const/4 v13, -0x1

    if-eq v5, v13, :cond_6

    const/16 v13, 0x10

    if-ge v6, v13, :cond_4

    const/16 v13, 0x11

    if-ge v8, v13, :cond_5

    :cond_4
    const/16 v13, -0x9

    if-ge v6, v13, :cond_6

    :cond_5
    add-int/lit8 v10, v8, -0x1

    .line 2300
    :goto_2
    if-ge v10, v8, :cond_9

    .line 2301
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x35

    if-lt v13, v14, :cond_8

    .line 2303
    add-int/lit8 v9, v10, -0x1

    .local v9, "p":I
    :goto_3
    if-ltz v9, :cond_7

    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x39

    if-ne v13, v14, :cond_7

    .line 2304
    const/16 v13, 0x30

    invoke-virtual {v2, v9, v13}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2303
    add-int/lit8 v9, v9, -0x1

    goto :goto_3

    .line 2297
    .end local v9    # "p":I
    :cond_6
    add-int/2addr v10, v6

    goto :goto_2

    .line 2306
    .restart local v9    # "p":I
    :cond_7
    if-ltz v9, :cond_c

    .line 2307
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    add-int/lit8 v13, v13, 0x1

    int-to-char v13, v13

    invoke-virtual {v2, v9, v13}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2314
    .end local v9    # "p":I
    :cond_8
    :goto_4
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 2317
    :cond_9
    const/4 v13, -0x5

    if-lt v6, v13, :cond_a

    const/16 v13, 0xf

    if-le v6, v13, :cond_d

    .line 2318
    :cond_a
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    if-lez v13, :cond_b

    .line 2319
    const/4 v13, 0x1

    const/16 v14, 0x2e

    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 2320
    add-int/lit8 v6, v6, -0x1

    .line 2330
    :cond_b
    :goto_5
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    .line 2332
    const/4 v3, 0x0

    :goto_6
    if-ge v3, v8, :cond_11

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x2e

    if-eq v13, v14, :cond_11

    .line 2333
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 2309
    .restart local v9    # "p":I
    :cond_c
    const/4 v13, 0x0

    const/16 v14, 0x31

    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 2310
    add-int/lit8 v10, v10, 0x1

    .line 2311
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 2323
    .end local v9    # "p":I
    :cond_d
    move v7, v8

    .local v7, "i":I
    :goto_7
    if-ge v7, v6, :cond_e

    .line 2324
    const/16 v13, 0x30

    invoke-virtual {v2, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2323
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 2325
    :cond_e
    move v7, v6

    :goto_8
    if-gtz v7, :cond_f

    .line 2326
    const/4 v13, 0x0

    const/16 v14, 0x30

    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 2325
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 2327
    :cond_f
    if-gtz v6, :cond_10

    const/4 v6, 0x1

    .end local v6    # "exp":I
    :cond_10
    const/16 v13, 0x2e

    invoke-virtual {v2, v6, v13}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 2328
    const/4 v6, 0x0

    .restart local v6    # "exp":I
    goto :goto_5

    .line 2334
    .end local v7    # "i":I
    :cond_11
    sub-int v13, v8, v3

    add-int/lit8 v4, v13, -0x1

    .line 2335
    .local v4, "dotlen":I
    const/16 v13, 0xa

    if-le v4, v13, :cond_12

    .line 2336
    add-int/lit8 v4, v4, -0xa

    .line 2337
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_9
    if-ge v7, v4, :cond_12

    .line 2338
    add-int/lit8 v13, v3, 0xa

    add-int/2addr v13, v7

    add-int/lit8 v13, v13, 0x1

    const/16 v14, 0x30

    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2337
    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    .line 2343
    .end local v7    # "i":I
    :cond_12
    add-int/lit8 v12, v8, -0x1

    .local v12, "tail":I
    :goto_a
    if-ltz v12, :cond_13

    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x30

    if-ne v13, v14, :cond_13

    .line 2344
    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 2343
    add-int/lit8 v12, v12, -0x1

    goto :goto_a

    .line 2346
    :cond_13
    if-ltz v12, :cond_14

    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x2e

    if-ne v13, v14, :cond_14

    .line 2347
    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 2348
    :cond_14
    if-eqz v6, :cond_15

    .line 2349
    const/16 v13, 0x45

    invoke-virtual {v2, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 2350
    :cond_15
    const-wide/16 v14, 0x0

    cmpg-double v13, p1, v14

    if-gez v13, :cond_16

    .line 2351
    const/4 v13, 0x0

    const/16 v14, 0x2d

    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 2352
    :cond_16
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    return-object v13
.end method

.method public getDoubleToString(DI)Ljava/lang/String;
    .locals 1
    .param p1, "v"    # D
    .param p3, "maxLen"    # I

    .prologue
    .line 2356
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDoubleToString(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/sec/android/app/popupcalculator/EventHandler;->sizeTruncate(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEnterEnd()Z
    .locals 1

    .prologue
    .line 3353
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    return v0
.end method

.method public getFormula()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 2236
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormula:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getFormulaBackup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3438
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormulaLengthBackup()I
    .locals 1

    .prologue
    .line 3430
    iget v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaLengthBackup:I

    return v0
.end method

.method public getLastResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3419
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 2240
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mResult:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public getResultFormating(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "strValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/popupcalculator/SyntaxException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2360
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v4, :cond_1

    .line 2361
    :cond_0
    const-string v2, ""

    .line 2395
    :goto_0
    return-object v2

    .line 2364
    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 2391
    const-string v2, "E"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_7

    .line 2392
    const/16 v2, 0xa

    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->resultFormating(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2365
    :catch_0
    move-exception v0

    .line 2366
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2367
    .local v1, "errorStr":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 2368
    const-string v2, ""

    goto :goto_0

    .line 2371
    :cond_2
    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Infinity"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2376
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0008

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 2379
    :cond_3
    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NaN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2380
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v2

    const-string v3, "capuccino"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2381
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b000f

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 2383
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0004

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 2385
    :cond_5
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v2

    const-string v3, "capuccino"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2386
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0016

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 2388
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0057

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 2395
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v1    # "errorStr":[Ljava/lang/String;
    :cond_7
    const/16 v2, 0x8

    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->resultFormating(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 621
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public handleTestmodeSecretCode(Landroid/content/Context;Ljava/lang/String;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "input"    # Ljava/lang/String;

    .prologue
    const v10, 0x7f0e004e

    const/high16 v9, 0x10000000

    .line 3046
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    .line 3048
    .local v4, "len":I
    const/4 v6, 0x7

    if-le v4, v6, :cond_2

    invoke-direct {p0, p2}, Lcom/sec/android/app/popupcalculator/EventHandler;->IsFactoryModeKeyString(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3049
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3051
    .local v0, "callMom":Landroid/content/Intent;
    const/4 v5, 0x0

    .line 3053
    .local v5, "parser":Z
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.parser"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3054
    const/4 v5, 0x1

    .line 3059
    :goto_0
    if-eqz v5, :cond_5

    .line 3060
    sget-object v6, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "com.sec.android.app.parser = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3061
    const-string v6, "com.sec.android.app.parser"

    const-string v7, "com.sec.android.app.parser.SecretCodeIME"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3078
    :cond_0
    :goto_1
    invoke-virtual {v0, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3080
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    if-eqz v6, :cond_1

    .line 3081
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->performClick()Z

    .line 3085
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 3091
    .end local v0    # "callMom":Landroid/content/Intent;
    .end local v5    # "parser":Z
    :cond_2
    :goto_2
    const/4 v6, 0x6

    if-le v4, v6, :cond_4

    .line 3093
    invoke-direct {p0, p2}, Lcom/sec/android/app/popupcalculator/EventHandler;->IsFactoryModeKeyStringCTN(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 3094
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3095
    .local v3, "i":Landroid/content/Intent;
    const-string v6, "7267864872"

    const-string v7, "72678647376477466"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3096
    const-string v6, "com.android.hiddenmenu"

    const-string v7, "com.android.hiddenmenu.CTN"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "keyString"

    const-string v8, "CTNFROMMSL"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3097
    invoke-virtual {v3, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3098
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    if-eqz v6, :cond_3

    .line 3099
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->performClick()Z

    .line 3102
    :cond_3
    :try_start_2
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 3122
    .end local v3    # "i":Landroid/content/Intent;
    :cond_4
    :goto_3
    return-void

    .line 3055
    .restart local v0    # "callMom":Landroid/content/Intent;
    .restart local v5    # "parser":Z
    :catch_0
    move-exception v1

    .line 3056
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v5, 0x0

    goto :goto_0

    .line 3064
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_5
    const/4 v2, 0x0

    .line 3066
    .local v2, "factorymode":Z
    :try_start_3
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.factorymode"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    .line 3068
    const/4 v2, 0x1

    .line 3073
    :goto_4
    if-eqz v2, :cond_0

    .line 3074
    sget-object v6, Lcom/sec/android/app/popupcalculator/EventHandler;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "com.sec.android.app.factorymode = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3075
    const-string v6, "com.sec.android.app.factorymode"

    const-string v7, "com.sec.android.app.factorymode.FactoryCTRL"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 3069
    :catch_1
    move-exception v1

    .line 3070
    .restart local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_4

    .line 3087
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v2    # "factorymode":Z
    :catch_2
    move-exception v1

    .line 3088
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 3103
    .end local v0    # "callMom":Landroid/content/Intent;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v5    # "parser":Z
    .restart local v3    # "i":Landroid/content/Intent;
    :catch_3
    move-exception v1

    .line 3104
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 3107
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "i":Landroid/content/Intent;
    :cond_6
    invoke-direct {p0, p2}, Lcom/sec/android/app/popupcalculator/EventHandler;->IsFactoryModeKeyStringCTN2(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 3108
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3109
    .restart local v3    # "i":Landroid/content/Intent;
    const-string v6, "7267864872"

    const-string v7, "72678647376477466"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3110
    const-string v6, "com.android.hiddenmenu"

    const-string v7, "com.android.hiddenmenu.OTASP"

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "keyString"

    const-string v8, "OTASP"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3111
    invoke-virtual {v3, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3112
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    if-eqz v6, :cond_7

    .line 3113
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->performClick()Z

    .line 3116
    :cond_7
    :try_start_4
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_3

    .line 3117
    :catch_4
    move-exception v1

    .line 3118
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public insert(Ljava/lang/StringBuilder;)V
    .locals 25
    .param p1, "text"    # Ljava/lang/StringBuilder;

    .prologue
    .line 645
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v2

    const-string v5, "capuccino"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 646
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0010

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v5, "%d"

    const/16 v6, 0x64

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 651
    .local v19, "errorText":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 652
    .local v12, "displayString":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 822
    :goto_1
    return-void

    .line 649
    .end local v12    # "displayString":Ljava/lang/String;
    .end local v19    # "errorText":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0005

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v5, "%d"

    const/16 v6, 0x64

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .restart local v19    # "errorText":Ljava/lang/String;
    goto :goto_0

    .line 654
    .restart local v12    # "displayString":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 655
    .local v8, "beforeResult":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v2

    const-string v5, "capuccino"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 656
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0016

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 657
    .local v15, "error":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b000b

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 658
    .local v16, "error1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0011

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 665
    .local v17, "error2":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "0."

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 668
    :cond_2
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 669
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->isErrorClearedOnInsert:Z

    .line 672
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->copy()V

    .line 674
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v7

    .line 675
    .local v7, "startCursor":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v14

    .line 676
    .local v14, "endCursor":I
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursorPostion(I)I

    move-result v11

    .line 677
    .local v11, "cursorPos":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v13, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 678
    .local v13, "displayText":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 679
    .local v3, "frontChar":C
    const/4 v4, 0x0

    .line 682
    .local v4, "nextChar":C
    const/16 v18, 0x0

    .line 684
    .local v18, "errorCode":I
    if-lez v7, :cond_4

    .line 685
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    add-int/lit8 v5, v7, -0x1

    invoke-interface {v2, v5}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    .line 688
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v14, v2, :cond_5

    .line 689
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2, v14}, Landroid/text/Editable;->charAt(I)C

    move-result v4

    .line 692
    :cond_5
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_7

    .line 695
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigitDot(C)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    if-nez v2, :cond_6

    const-string v2, "Error"

    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 698
    :cond_6
    const-string v2, "Error"

    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "Error:"

    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 699
    const-string v2, "r"

    invoke-virtual {v8, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v8, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    .line 701
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0005

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0007

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0010

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0012

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 709
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->clear()V

    .line 710
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v7

    .line 719
    :cond_7
    :goto_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 720
    .local v10, "copyFma":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 722
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->checkError(Ljava/lang/StringBuilder;)V

    .line 724
    if-eqz v11, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v11, v2, :cond_a

    .line 725
    const/16 v20, 0x0

    .line 726
    .local v20, "mDecimalPresent":Z
    add-int/lit8 v22, v7, -0x1

    .local v22, "x":I
    :goto_4
    if-lez v22, :cond_8

    .line 727
    if-lez v7, :cond_d

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 734
    :cond_8
    :goto_5
    if-eqz v20, :cond_a

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "(\u2212"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigitDot(C)Z

    move-result v2

    if-nez v2, :cond_a

    .line 735
    const/16 v21, 0x0

    .line 736
    .local v21, "mNumberOfDigits":I
    move/from16 v23, v7

    .local v23, "y":I
    :goto_6
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    move/from16 v0, v23

    if-ge v0, v2, :cond_9

    .line 737
    move/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v9

    .line 738
    .local v9, "c":C
    invoke-static {v9}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 742
    .end local v9    # "c":C
    :cond_9
    const/16 v2, 0xc

    move/from16 v0, v21

    if-le v0, v2, :cond_10

    .line 743
    add-int/lit8 v11, v11, 0x4

    .line 753
    .end local v20    # "mDecimalPresent":Z
    .end local v21    # "mNumberOfDigits":I
    .end local v22    # "x":I
    .end local v23    # "y":I
    :cond_a
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    if-eqz v2, :cond_15

    .line 754
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 755
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsInputError:Z

    if-eqz v2, :cond_14

    .line 756
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v2

    const-string v5, "capuccino"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 757
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ""

    const v6, 0x7f0b0010

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v2, v5, v6, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResult(Ljava/lang/StringBuilder;Ljava/lang/String;IZ)V

    .line 760
    :goto_8
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    .line 761
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsInputError:Z

    goto/16 :goto_1

    .line 660
    .end local v3    # "frontChar":C
    .end local v4    # "nextChar":C
    .end local v7    # "startCursor":I
    .end local v10    # "copyFma":Ljava/lang/StringBuilder;
    .end local v11    # "cursorPos":I
    .end local v13    # "displayText":Ljava/lang/StringBuilder;
    .end local v14    # "endCursor":I
    .end local v15    # "error":Ljava/lang/String;
    .end local v16    # "error1":Ljava/lang/String;
    .end local v17    # "error2":Ljava/lang/String;
    .end local v18    # "errorCode":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0057

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 661
    .restart local v15    # "error":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v5, 0x7f0b0000

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 662
    .restart local v16    # "error1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b0006

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .restart local v17    # "error2":Ljava/lang/String;
    goto/16 :goto_2

    .line 713
    .restart local v3    # "frontChar":C
    .restart local v4    # "nextChar":C
    .restart local v7    # "startCursor":I
    .restart local v11    # "cursorPos":I
    .restart local v13    # "displayText":Ljava/lang/StringBuilder;
    .restart local v14    # "endCursor":I
    .restart local v18    # "errorCode":I
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->clear()V

    .line 714
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v7

    goto/16 :goto_3

    .line 729
    .restart local v10    # "copyFma":Ljava/lang/StringBuilder;
    .restart local v20    # "mDecimalPresent":Z
    .restart local v22    # "x":I
    :cond_d
    if-ltz v7, :cond_e

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v5, 0x2e

    if-ne v2, v5, :cond_e

    .line 730
    const/16 v20, 0x1

    .line 731
    goto/16 :goto_5

    .line 726
    :cond_e
    add-int/lit8 v22, v22, -0x1

    goto/16 :goto_4

    .line 740
    .restart local v9    # "c":C
    .restart local v21    # "mNumberOfDigits":I
    .restart local v23    # "y":I
    :cond_f
    add-int/lit8 v21, v21, 0x1

    .line 736
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_6

    .line 744
    .end local v9    # "c":C
    :cond_10
    const/16 v2, 0x9

    move/from16 v0, v21

    if-le v0, v2, :cond_11

    .line 745
    add-int/lit8 v11, v11, 0x3

    goto/16 :goto_7

    .line 746
    :cond_11
    const/4 v2, 0x6

    move/from16 v0, v21

    if-le v0, v2, :cond_12

    .line 747
    add-int/lit8 v11, v11, 0x2

    goto/16 :goto_7

    .line 748
    :cond_12
    const/4 v2, 0x3

    move/from16 v0, v21

    if-le v0, v2, :cond_a

    .line 749
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_7

    .line 759
    .end local v20    # "mDecimalPresent":Z
    .end local v21    # "mNumberOfDigits":I
    .end local v22    # "x":I
    .end local v23    # "y":I
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ""

    const v6, 0x7f0b0005

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v2, v5, v6, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResult(Ljava/lang/StringBuilder;Ljava/lang/String;IZ)V

    goto/16 :goto_8

    .line 764
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 765
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mInsertError:Z

    goto/16 :goto_1

    .line 769
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v2

    if-nez v2, :cond_19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v7, :cond_19

    .line 770
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->insertInput(CCLjava/lang/String;Ljava/lang/String;I)V

    .line 777
    :cond_16
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 779
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultplicationCheck:Z

    if-nez v2, :cond_17

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNewLineChek:Z

    if-eqz v2, :cond_20

    .line 781
    :cond_17
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultplicationCheck:Z

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNewLineChek:Z

    if-eqz v2, :cond_1a

    .line 782
    add-int/lit8 v2, v11, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 803
    :cond_18
    :goto_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultplicationCheck:Z

    .line 804
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNewLineChek:Z

    .line 818
    :goto_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/sec/android/app/popupcalculator/EventHandler;->checkInput(Ljava/lang/StringBuilder;I)V

    .line 819
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 820
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 821
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsResultError:Z

    goto/16 :goto_1

    .line 771
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 772
    invoke-virtual {v13, v7, v14}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 773
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 774
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->insertInput(CCLjava/lang/String;Ljava/lang/String;I)V

    goto :goto_9

    .line 783
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultplicationCheck:Z

    if-eqz v2, :cond_1b

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNewLineChek:Z

    if-nez v2, :cond_1b

    .line 784
    add-int/lit8 v2, v11, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto :goto_a

    .line 785
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultplicationCheck:Z

    if-nez v2, :cond_18

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNewLineChek:Z

    if-eqz v2, :cond_18

    .line 786
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v11, :cond_1f

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v5, v11

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 789
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisCheck:Z

    if-eqz v2, :cond_1c

    .line 790
    add-int/lit8 v11, v11, 0x1

    .line 791
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisCheck:Z

    .line 793
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisDelete:Z

    if-eqz v2, :cond_1e

    .line 794
    if-lez v11, :cond_1d

    .line 795
    add-int/lit8 v11, v11, -0x1

    .line 796
    :cond_1d
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisDelete:Z

    .line 798
    :cond_1e
    add-int/lit8 v2, v11, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_a

    .line 800
    :cond_1f
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_a

    .line 806
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisCheck:Z

    if-eqz v2, :cond_21

    .line 807
    add-int/lit8 v11, v11, 0x1

    .line 808
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisCheck:Z

    .line 810
    :cond_21
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisDelete:Z

    if-eqz v2, :cond_23

    .line 811
    if-lez v11, :cond_22

    .line 812
    add-int/lit8 v11, v11, -0x1

    .line 813
    :cond_22
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisDelete:Z

    .line 815
    :cond_23
    sget v2, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteDotCount:I

    sub-int v2, v11, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 816
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteDotCount:I

    goto/16 :goto_b
.end method

.method public isArabicCharacter(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 1451
    const/16 v0, 0x600

    if-le p1, v0, :cond_0

    const/16 v0, 0x6ff

    if-ge p1, v0, :cond_0

    .line 1452
    const/4 v0, 0x1

    .line 1454
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCheckResult()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 411
    :cond_0
    :goto_0
    return v1

    .line 410
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 411
    .local v0, "ch":C
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x2d

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isDisplayScreen()Z
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHistoryScreen()Z
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/History;->isHistory()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiSelection(Landroid/widget/EditText;)Z
    .locals 7
    .param p1, "et"    # Landroid/widget/EditText;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 497
    invoke-virtual {p1}, Landroid/widget/EditText;->getId()I

    move-result v5

    const v6, 0x7f0e0015

    if-ne v5, v6, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    .line 498
    .local v0, "editText":Landroid/widget/EditText;
    :goto_0
    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v2

    .line 499
    .local v2, "start":I
    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v1

    .line 500
    .local v1, "end":I
    if-eq v2, v1, :cond_1

    :goto_1
    return v3

    .line 497
    .end local v0    # "editText":Landroid/widget/EditText;
    .end local v1    # "end":I
    .end local v2    # "start":I
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    goto :goto_0

    .restart local v0    # "editText":Landroid/widget/EditText;
    .restart local v1    # "end":I
    .restart local v2    # "start":I
    :cond_1
    move v3, v4

    .line 500
    goto :goto_1
.end method

.method public isNextAutoMultiple(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 430
    const/16 v0, 0x29

    if-eq p1, v0, :cond_0

    const/16 v0, 0x65

    if-eq p1, v0, :cond_0

    const/16 v0, 0x21

    if-eq p1, v0, :cond_0

    const/16 v0, 0x25

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3c0

    if-ne p1, v0, :cond_1

    .line 431
    :cond_0
    const/4 v0, 0x1

    .line 433
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotStartOperator(Ljava/lang/String;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 423
    const-string v0, "%"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "!"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "^(2)"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "^("

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 424
    :cond_0
    const/4 v0, 0x1

    .line 426
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScientific(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 437
    const/16 v0, 0x73

    if-eq p1, v0, :cond_0

    const/16 v0, 0x63

    if-eq p1, v0, :cond_0

    const/16 v0, 0x74

    if-eq p1, v0, :cond_0

    const/16 v0, 0x6c

    if-eq p1, v0, :cond_0

    const/16 v0, 0x221a

    if-eq p1, v0, :cond_0

    const/16 v0, 0x65

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3c0

    if-ne p1, v0, :cond_1

    .line 439
    :cond_0
    const/4 v0, 0x1

    .line 441
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackspace()V
    .locals 28

    .prologue
    .line 1929
    const/4 v12, 0x0

    .line 1930
    .local v12, "isResultCleared":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v26

    const-string v27, "="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 1931
    const/4 v12, 0x1

    .line 1933
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v26

    const-string v27, "="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_1

    .line 1934
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1936
    :cond_1
    const/4 v4, 0x0

    .line 1937
    .local v4, "calc_temp":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_2

    .line 1938
    const/4 v4, 0x1

    .line 1939
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    move/from16 v26, v0

    if-eqz v26, :cond_3

    .line 1940
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 1943
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v7

    .line 1944
    .local v7, "displayText":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v13

    .line 1945
    .local v13, "len":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v20

    .line 1946
    .local v20, "startCursor":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v9

    .line 1947
    .local v9, "endCursor":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursorPostion(I)I

    move-result v5

    .line 1948
    .local v5, "cursorPos":I
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v7, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDotCount(Ljava/lang/String;I)I

    move-result v8

    .line 1949
    .local v8, "dotCount":I
    const/4 v11, 0x0

    .line 1950
    .local v11, "isDelete":Z
    const/16 v26, 0x3d

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v26

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_5

    const/16 v26, 0x3d

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v26

    move/from16 v0, v26

    if-ge v9, v0, :cond_5

    .line 1951
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 1952
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 1953
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v26

    sub-int v26, v26, v9

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 2195
    :cond_4
    :goto_0
    return-void

    .line 1957
    :cond_5
    if-nez v20, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v26

    if-nez v26, :cond_6

    if-eqz v13, :cond_6

    .line 1958
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 1959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v20

    .line 1960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v9

    .line 1961
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursorPostion(I)I

    move-result v5

    .line 1962
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v7, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDotCount(Ljava/lang/String;I)I

    move-result v8

    .line 1964
    :cond_6
    if-nez v20, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 1967
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v26

    if-eqz v26, :cond_8

    sub-int v26, v9, v20

    move/from16 v0, v26

    if-eq v0, v13, :cond_9

    :cond_8
    const-string v26, "="

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 1969
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->clear()V

    goto/16 :goto_0

    .line 1973
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v26

    if-eqz v26, :cond_f

    move/from16 v0, v20

    if-gt v0, v13, :cond_f

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v26

    if-gt v9, v0, :cond_f

    .line 1974
    new-instance v19, Ljava/lang/StringBuilder;

    move-object/from16 v0, v19

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1975
    .local v19, "sb":Ljava/lang/StringBuilder;
    const/4 v6, -0x1

    .line 1978
    .local v6, "deleteEndPos":I
    const-string v26, "="

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v26

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_b

    .line 1979
    new-instance v21, Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1980
    .local v21, "tmpsb":Ljava/lang/StringBuilder;
    const-string v26, "="

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v26

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1, v9}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v26

    const-string v27, "="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_b

    .line 1982
    add-int/lit8 v20, v20, 0x1

    .line 1985
    .end local v21    # "tmpsb":Ljava/lang/StringBuilder;
    :cond_b
    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v9}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1987
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    if-lez v26, :cond_c

    if-lez v5, :cond_c

    const/16 v26, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v26

    const/16 v27, 0x2c

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_c

    .line 1997
    add-int/lit8 v5, v5, -0x1

    .line 1999
    :cond_c
    move/from16 v10, v20

    .local v10, "i":I
    :goto_1
    if-lez v10, :cond_d

    .line 2000
    add-int/lit8 v26, v10, -0x1

    if-ltz v26, :cond_d

    add-int/lit8 v26, v10, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v26

    const/16 v27, 0x61

    move/from16 v0, v26

    move/from16 v1, v27

    if-lt v0, v1, :cond_d

    add-int/lit8 v26, v10, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v26

    const/16 v27, 0x7a

    move/from16 v0, v26

    move/from16 v1, v27

    if-gt v0, v1, :cond_d

    .line 2001
    add-int/lit8 v26, v10, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v26

    invoke-virtual {v0, v1, v10}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 2002
    add-int/lit8 v6, v10, -0x1

    .line 1999
    add-int/lit8 v10, v10, -0x1

    goto :goto_1

    .line 2007
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 2008
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 2009
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 2010
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v6, v0, :cond_e

    .line 2011
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursorPostion(I)I

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_0

    .line 2013
    :cond_e
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_0

    .line 2018
    .end local v6    # "deleteEndPos":I
    .end local v10    # "i":I
    .end local v19    # "sb":Ljava/lang/StringBuilder;
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->copy()V

    .line 2019
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->onClearOrigin()V

    .line 2021
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsResultError:Z

    move/from16 v26, v0

    if-eqz v26, :cond_10

    .line 2022
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 2024
    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 2025
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsResultError:Z

    .line 2026
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    .line 2027
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->isErrorClearedOnInsert:Z

    move/from16 v26, v0

    if-nez v26, :cond_10

    .line 2028
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->isErrorClearedOnInsert:Z

    goto/16 :goto_0

    .line 2033
    :cond_10
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v7, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursorToken(Ljava/lang/String;I)I

    move-result v23

    .line 2037
    .local v23, "tokenPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v26

    if-nez v26, :cond_17

    const/16 v26, -0x1

    move/from16 v0, v23

    move/from16 v1, v26

    if-eq v0, v1, :cond_17

    move/from16 v0, v23

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0x29

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_17

    move/from16 v0, v20

    if-gt v0, v13, :cond_17

    .line 2039
    const/16 v26, 0x28

    move/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v18

    .line 2040
    .local v18, "nOpenPos":I
    if-ltz v18, :cond_11

    add-int/lit8 v26, v18, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ge v0, v1, :cond_11

    add-int/lit8 v26, v18, 0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0x2d

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_11

    .line 2042
    const/16 v26, 0x1

    sput-boolean v26, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 2044
    :cond_11
    sub-int v26, v23, v8

    add-int/lit8 v27, v23, 0x1

    sub-int v27, v27, v8

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    .line 2045
    if-lez v23, :cond_16

    add-int/lit8 v26, v23, -0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0x2e

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_16

    .line 2047
    const/16 v16, 0x0

    .line 2048
    .local v16, "mNumberOfCommas":I
    move/from16 v25, v20

    .local v25, "y":I
    :goto_2
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_13

    .line 2049
    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2050
    .local v3, "c":C
    const/16 v26, 0x2c

    move/from16 v0, v26

    if-ne v3, v0, :cond_12

    .line 2051
    add-int/lit8 v16, v16, 0x1

    .line 2052
    :cond_12
    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v26

    if-eqz v26, :cond_15

    .line 2055
    .end local v3    # "c":C
    :cond_13
    sub-int v5, v5, v16

    .line 2059
    .end local v16    # "mNumberOfCommas":I
    .end local v25    # "y":I
    :goto_3
    const/4 v11, 0x1

    .line 2191
    .end local v18    # "nOpenPos":I
    :cond_14
    :goto_4
    if-eqz v11, :cond_4

    .line 2192
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 2193
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_0

    .line 2048
    .restart local v3    # "c":C
    .restart local v16    # "mNumberOfCommas":I
    .restart local v18    # "nOpenPos":I
    .restart local v25    # "y":I
    :cond_15
    add-int/lit8 v25, v25, 0x1

    goto :goto_2

    .line 2057
    .end local v3    # "c":C
    .end local v16    # "mNumberOfCommas":I
    .end local v25    # "y":I
    :cond_16
    add-int/lit8 v5, v5, -0x1

    goto :goto_3

    .line 2066
    .end local v18    # "nOpenPos":I
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v26

    if-nez v26, :cond_19

    const/16 v26, -0x1

    move/from16 v0, v23

    move/from16 v1, v26

    if-eq v0, v1, :cond_19

    move/from16 v0, v20

    if-gt v0, v13, :cond_19

    add-int/lit8 v26, v20, -0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0x2d

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_18

    add-int/lit8 v26, v20, -0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0x28

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_19

    .line 2071
    :cond_18
    sub-int v26, v23, v8

    sub-int v27, v20, v8

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    .line 2072
    const/4 v11, 0x1

    .line 2073
    sget-boolean v26, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    if-eqz v26, :cond_14

    .line 2074
    const/16 v26, 0x0

    sput-boolean v26, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    goto :goto_4

    .line 2077
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v26

    if-nez v26, :cond_2e

    const/16 v26, -0x1

    move/from16 v0, v23

    move/from16 v1, v26

    if-eq v0, v1, :cond_2e

    move/from16 v0, v20

    if-gt v0, v13, :cond_2e

    .line 2078
    move/from16 v0, v23

    invoke-static {v7, v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->getLastToken(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v22

    .line 2079
    .local v22, "token":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v26

    if-eqz v26, :cond_24

    .line 2081
    const/4 v15, 0x0

    .line 2083
    .local v15, "mNewLinePos":I
    const/16 v26, 0x1

    move/from16 v0, v20

    move/from16 v1, v26

    if-le v0, v1, :cond_1a

    add-int/lit8 v26, v20, -0x2

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0xa

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1a

    .line 2084
    add-int/lit8 v15, v15, 0x1

    .line 2085
    :cond_1a
    if-nez v4, :cond_1b

    .line 2086
    sub-int v26, v23, v8

    sub-int v26, v26, v15

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v27

    add-int v27, v27, v23

    sub-int v27, v27, v8

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    .line 2090
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 2092
    if-ltz v23, :cond_1c

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v26

    move/from16 v0, v23

    move/from16 v1, v26

    if-gt v0, v1, :cond_1c

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_1c

    .line 2093
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto/16 :goto_0

    .line 2120
    :catch_0
    move-exception v26

    goto/16 :goto_0

    .line 2095
    :cond_1c
    const/4 v14, 0x0

    .line 2096
    .local v14, "mDecimalPresent":Z
    add-int/lit8 v24, v20, -0x2

    .local v24, "x":I
    :goto_5
    if-lez v24, :cond_1d

    .line 2097
    if-lez v20, :cond_21

    move/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v26

    if-eqz v26, :cond_21

    .line 2104
    :cond_1d
    :goto_6
    const/16 v16, 0x0

    .line 2105
    .restart local v16    # "mNumberOfCommas":I
    if-eqz v14, :cond_1f

    .line 2107
    move/from16 v25, v20

    .restart local v25    # "y":I
    :goto_7
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_1f

    .line 2108
    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2109
    .restart local v3    # "c":C
    const/16 v26, 0x2c

    move/from16 v0, v26

    if-ne v3, v0, :cond_1e

    .line 2110
    add-int/lit8 v16, v16, 0x1

    .line 2111
    :cond_1e
    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v26

    if-eqz v26, :cond_23

    .line 2115
    .end local v3    # "c":C
    .end local v25    # "y":I
    :cond_1f
    const-string v26, "(\u2212"

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_20

    .line 2117
    sub-int v5, v5, v16

    .line 2118
    :cond_20
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    goto/16 :goto_0

    .line 2099
    .end local v16    # "mNumberOfCommas":I
    :cond_21
    if-ltz v20, :cond_22

    move/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v26

    const/16 v27, 0x2e

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_22

    .line 2100
    const/4 v14, 0x1

    .line 2101
    goto :goto_6

    .line 2096
    :cond_22
    add-int/lit8 v24, v24, -0x1

    goto :goto_5

    .line 2107
    .restart local v3    # "c":C
    .restart local v16    # "mNumberOfCommas":I
    .restart local v25    # "y":I
    :cond_23
    add-int/lit8 v25, v25, 0x1

    goto :goto_7

    .line 2125
    .end local v3    # "c":C
    .end local v14    # "mDecimalPresent":Z
    .end local v15    # "mNewLinePos":I
    .end local v16    # "mNumberOfCommas":I
    .end local v24    # "x":I
    .end local v25    # "y":I
    :cond_24
    add-int/lit8 v26, v20, -0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_2b

    .line 2127
    add-int/lit8 v26, v20, -0x1

    sub-int v26, v26, v8

    sub-int v27, v20, v8

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    .line 2128
    const/16 v17, 0x0

    .line 2129
    .local v17, "mNumberOfDigits":I
    move/from16 v25, v20

    .restart local v25    # "y":I
    :goto_8
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_25

    .line 2130
    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2131
    .restart local v3    # "c":C
    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v26

    if-eqz v26, :cond_27

    .line 2135
    .end local v3    # "c":C
    :cond_25
    const/16 v26, 0xc

    move/from16 v0, v17

    move/from16 v1, v26

    if-le v0, v1, :cond_28

    .line 2136
    add-int/lit8 v5, v5, 0x4

    .line 2147
    :cond_26
    :goto_9
    const/4 v11, 0x1

    .line 2148
    goto/16 :goto_4

    .line 2133
    .restart local v3    # "c":C
    :cond_27
    add-int/lit8 v17, v17, 0x1

    .line 2129
    add-int/lit8 v25, v25, 0x1

    goto :goto_8

    .line 2138
    .end local v3    # "c":C
    :cond_28
    const/16 v26, 0x9

    move/from16 v0, v17

    move/from16 v1, v26

    if-le v0, v1, :cond_29

    .line 2139
    add-int/lit8 v5, v5, 0x3

    goto :goto_9

    .line 2141
    :cond_29
    const/16 v26, 0x6

    move/from16 v0, v17

    move/from16 v1, v26

    if-le v0, v1, :cond_2a

    .line 2142
    add-int/lit8 v5, v5, 0x2

    goto :goto_9

    .line 2144
    :cond_2a
    const/16 v26, 0x3

    move/from16 v0, v17

    move/from16 v1, v26

    if-le v0, v1, :cond_26

    .line 2145
    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    .line 2148
    .end local v17    # "mNumberOfDigits":I
    .end local v25    # "y":I
    :cond_2b
    add-int/lit8 v26, v20, -0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0x3d

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_2c

    .line 2149
    const/4 v11, 0x1

    goto/16 :goto_4

    .line 2150
    :cond_2c
    add-int/lit8 v26, v13, -0x1

    move/from16 v0, v20

    move/from16 v1, v26

    if-ge v0, v1, :cond_2d

    add-int/lit8 v26, v20, -0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0x3d

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_2d

    add-int/lit8 v26, v20, 0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0x2e

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_2d

    .line 2152
    add-int/lit8 v26, v20, -0x1

    add-int/lit8 v27, v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    .line 2153
    const/4 v11, 0x1

    goto/16 :goto_4

    .line 2154
    :cond_2d
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v26

    if-nez v26, :cond_14

    .line 2155
    add-int/lit8 v26, v20, -0x1

    sub-int v26, v26, v8

    sub-int v27, v20, v8

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    .line 2156
    const/4 v11, 0x1

    .line 2157
    if-eqz v12, :cond_14

    .line 2158
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 2163
    .end local v22    # "token":Ljava/lang/String;
    :cond_2e
    const/16 v26, -0x1

    move/from16 v0, v23

    move/from16 v1, v26

    if-ne v0, v1, :cond_14

    add-int/lit8 v26, v20, -0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v26

    if-eqz v26, :cond_14

    .line 2165
    add-int/lit8 v26, v20, -0x1

    if-nez v26, :cond_2f

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v20

    move/from16 v1, v26

    if-ge v0, v1, :cond_2f

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    const/16 v27, 0xa

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_2f

    .line 2167
    add-int/lit8 v26, v20, -0x1

    add-int/lit8 v27, v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    .line 2169
    :cond_2f
    add-int/lit8 v26, v20, -0x1

    if-nez v26, :cond_31

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v20

    move/from16 v1, v26

    if-ge v0, v1, :cond_31

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v26

    if-eqz v26, :cond_31

    .line 2172
    if-nez v4, :cond_30

    .line 2173
    add-int/lit8 v26, v20, -0x1

    sub-int v26, v26, v8

    add-int/lit8 v27, v20, 0x1

    sub-int v27, v27, v8

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    .line 2184
    :cond_30
    :goto_a
    const/4 v11, 0x1

    .line 2185
    if-eqz v12, :cond_14

    .line 2186
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 2176
    :cond_31
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v20

    move/from16 v1, v26

    if-ne v0, v1, :cond_32

    add-int/lit8 v26, v20, -0x1

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->charAt(I)C

    move-result v26

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->thousandSepChar()C

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_32

    .line 2178
    add-int/lit8 v8, v8, -0x1

    .line 2181
    :cond_32
    if-nez v4, :cond_30

    .line 2182
    add-int/lit8 v26, v20, -0x1

    sub-int v26, v26, v8

    sub-int v27, v20, v8

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->deleteToken(II)V

    goto :goto_a
.end method

.method public onClearOrigin()V
    .locals 6

    .prologue
    .line 2244
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->getmOrigin()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 2245
    .local v2, "len":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2247
    .local v3, "result":Ljava/lang/StringBuilder;
    if-nez v2, :cond_1

    .line 2260
    :cond_0
    :goto_0
    return-void

    .line 2250
    :cond_1
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->getmOrigin()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2251
    .local v1, "index":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 2254
    add-int v4, v2, v1

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 2255
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    add-int v5, v2, v1

    invoke-interface {v4, v5}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    .line 2256
    .local v0, "ch":C
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOpByTwo(C)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x21

    if-eq v0, v4, :cond_0

    .line 2259
    .end local v0    # "ch":C
    :cond_2
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->clearmOrigin()V

    goto :goto_0
.end method

.method public onEnter()V
    .locals 23

    .prologue
    .line 2731
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->onClearOrigin()V

    .line 2734
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v19

    const-string v20, "capuccino"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 2735
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0016

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2736
    .local v11, "errorString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0014

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 2742
    .local v12, "errorString2":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    if-eqz v19, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 2744
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2745
    .local v8, "displayText":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v19

    if-eqz v19, :cond_2

    invoke-virtual {v8, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_2

    invoke-virtual {v8, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_2

    .line 2747
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 2752
    .end local v8    # "displayText":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    move/from16 v19, v0

    if-eqz v19, :cond_4

    .line 2753
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->reCalculation()V

    .line 2758
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->repairDisplay()V

    .line 2759
    const/16 v19, 0x0

    sput-boolean v19, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 2761
    const-string v17, ""

    .local v17, "result":Ljava/lang/String;
    const-string v18, ""

    .line 2762
    .local v18, "tmpResult":Ljava/lang/String;
    const/4 v10, 0x0

    .line 2763
    .local v10, "error":I
    const/4 v7, 0x0

    .line 2764
    .local v7, "digitCount":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 2766
    .local v14, "formula":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "\n"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    const/16 v20, 0x64

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_6

    .line 2767
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v19

    const-string v20, "capuccino"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 2768
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ""

    const v21, 0x7f0b0010

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResult(Ljava/lang/StringBuilder;Ljava/lang/String;IZ)V

    .line 2851
    .end local v7    # "digitCount":I
    .end local v10    # "error":I
    .end local v14    # "formula":Ljava/lang/StringBuilder;
    .end local v17    # "result":Ljava/lang/String;
    .end local v18    # "tmpResult":Ljava/lang/String;
    :cond_2
    :goto_2
    return-void

    .line 2738
    .end local v11    # "errorString":Ljava/lang/String;
    .end local v12    # "errorString2":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0057

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2739
    .restart local v11    # "errorString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0b0009

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "errorString2":Ljava/lang/String;
    goto/16 :goto_0

    .line 2755
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    goto :goto_1

    .line 2770
    .restart local v7    # "digitCount":I
    .restart local v10    # "error":I
    .restart local v14    # "formula":Ljava/lang/StringBuilder;
    .restart local v17    # "result":Ljava/lang/String;
    .restart local v18    # "tmpResult":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ""

    const v21, 0x7f0b0005

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResult(Ljava/lang/StringBuilder;Ljava/lang/String;IZ)V

    goto :goto_2

    .line 2773
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaLengthBackup:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1, v14}, Lcom/sec/android/app/popupcalculator/EventHandler;->restoreFomula(ILjava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 2776
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 2777
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v14, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->evaluate(Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v18

    .line 2779
    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_9

    .line 2780
    invoke-static/range {v18 .. v18}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v20

    const/16 v19, 0x10

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDoubleToString(DI)Ljava/lang/String;

    move-result-object v17

    .line 2784
    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResultFormating(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2785
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v19

    const/16 v20, 0x2e

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 2786
    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 2788
    :cond_7
    const-string v19, "."

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_b

    const-string v19, "E"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_b

    .line 2789
    const-string v19, "."

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    add-int/lit8 v15, v19, 0x1

    .local v15, "i":I
    :goto_4
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v15, v0, :cond_a

    .line 2790
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 2791
    .local v6, "ch":C
    invoke-static {v6}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 2792
    add-int/lit8 v7, v7, 0x1

    .line 2789
    :cond_8
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 2782
    .end local v6    # "ch":C
    .end local v15    # "i":I
    :cond_9
    invoke-static/range {v18 .. v18}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v20

    const/16 v19, 0xf

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDoubleToString(DI)Ljava/lang/String;

    move-result-object v17

    goto :goto_3

    .line 2796
    .restart local v15    # "i":I
    :cond_a
    const/16 v19, 0xa

    move/from16 v0, v19

    if-le v7, v0, :cond_b

    .line 2798
    new-instance v16, Ljava/text/DecimalFormatSymbols;

    sget-object v19, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 2799
    .local v16, "locale":Ljava/text/DecimalFormatSymbols;
    new-instance v13, Ljava/text/DecimalFormat;

    const-string v19, "0.00000000E+0"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-direct {v13, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 2800
    .local v13, "fomatter":Ljava/text/DecimalFormat;
    invoke-static/range {v17 .. v17}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v13, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v17

    .line 2803
    .end local v13    # "fomatter":Ljava/text/DecimalFormat;
    .end local v15    # "i":I
    .end local v16    # "locale":Ljava/text/DecimalFormatSymbols;
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 2804
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v14}, Lcom/sec/android/app/popupcalculator/EventHandler;->updateBackup(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    :try_end_0
    .catch Lcom/sec/android/app/popupcalculator/SyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2821
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v10, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResult(Ljava/lang/StringBuilder;Ljava/lang/String;IZ)V

    .line 2823
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    move/from16 v19, v0

    if-nez v19, :cond_d

    const-string v19, "Infinity"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_d

    const-string v19, "NaN"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "\n"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    const/16 v20, 0x64

    move/from16 v0, v19

    move/from16 v1, v20

    if-gt v0, v1, :cond_d

    .line 2828
    sget-boolean v19, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v19, :cond_c

    .line 2829
    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v17

    .line 2831
    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeNumFomat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/History;->enter(Ljava/lang/String;Ljava/lang/String;)V

    .line 2833
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->setmTrans(Ljava/lang/String;)V

    .line 2834
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->setmOrigin(Ljava/lang/String;)V

    .line 2840
    :cond_d
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 2842
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 2845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V

    .line 2847
    sget-boolean v19, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandEnabled:Z

    if-eqz v19, :cond_f

    .line 2848
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v20, "com.sec.android.app.popupcalculator"

    const-string v21, "2001"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/popupcalculator/EventHandler;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2806
    :catch_0
    move-exception v9

    .line 2807
    .local v9, "e":Lcom/sec/android/app/popupcalculator/SyntaxException;
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 2808
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 2809
    iget v10, v9, Lcom/sec/android/app/popupcalculator/SyntaxException;->message:I

    .line 2810
    iget v0, v9, Lcom/sec/android/app/popupcalculator/SyntaxException;->position:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mErrorCursor:I

    goto/16 :goto_5

    .line 2811
    .end local v9    # "e":Lcom/sec/android/app/popupcalculator/SyntaxException;
    :catch_1
    move-exception v9

    .line 2812
    .local v9, "e":Ljava/lang/Exception;
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsError:Z

    .line 2813
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 2814
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v19

    const-string v20, "capuccino"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 2815
    const v10, 0x7f0b0016

    .line 2818
    :goto_6
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 2817
    :cond_e
    const v10, 0x7f0b0057

    goto :goto_6

    .line 2849
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 2850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v20, "com.sec.android.app.popupcalculator"

    const-string v21, "2002"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/popupcalculator/EventHandler;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public onHisClear()V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/History;->clear()V

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->clear()V

    .line 510
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/popupcalculator/EventListener;->flagClearHis:Z

    .line 511
    return-void
.end method

.method public parenthesis()V
    .locals 12

    .prologue
    const/16 v11, 0x29

    const/16 v10, 0x28

    const/4 v9, 0x0

    .line 2938
    const/4 v3, 0x0

    .line 2939
    .local v3, "leftParenthesis":I
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {p0, v7, v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v5

    .line 2940
    .local v5, "startCursor":I
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v0

    .line 2941
    .local v0, "endCursor":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2943
    .local v6, "text":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 2944
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v10, :cond_0

    .line 2945
    add-int/lit8 v3, v3, 0x1

    .line 2947
    :cond_0
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v11, :cond_1

    .line 2948
    add-int/lit8 v3, v3, -0x1

    .line 2943
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2952
    :cond_2
    const/4 v1, 0x0

    .line 2953
    .local v1, "frontChar":C
    const/4 v4, 0x0

    .line 2954
    .local v4, "nextChar":C
    if-lez v5, :cond_3

    .line 2955
    add-int/lit8 v7, v5, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    .line 2957
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v0, v7, :cond_4

    .line 2958
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    .line 2961
    :cond_4
    if-lez v3, :cond_c

    .line 2962
    if-nez v4, :cond_8

    .line 2963
    invoke-direct {p0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isParenthesisAutoMultiple(C)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2964
    const-string v7, ")"

    invoke-virtual {v6, v5, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 2980
    :goto_1
    sget-boolean v7, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    if-eqz v7, :cond_5

    .line 2981
    sput-boolean v9, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 2992
    :cond_5
    :goto_2
    invoke-virtual {p0, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 2993
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0, v7, v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 2994
    return-void

    .line 2966
    :cond_6
    if-ne v1, v11, :cond_7

    if-lez v5, :cond_7

    const-string v7, "\u00d7("

    :goto_3
    invoke-virtual {v6, v5, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_7
    const-string v7, "("

    goto :goto_3

    .line 2970
    :cond_8
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v7

    if-eqz v7, :cond_9

    if-eq v1, v10, :cond_9

    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2972
    const-string v7, ")\u00d7"

    invoke-virtual {v6, v5, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2973
    :cond_9
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v7

    if-nez v7, :cond_a

    const/16 v7, 0xa

    if-ne v4, v7, :cond_b

    add-int/lit8 v7, v0, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 2975
    :cond_a
    const-string v7, ")"

    invoke-virtual {v6, v5, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2977
    :cond_b
    const-string v7, "("

    invoke-virtual {v6, v5, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2984
    :cond_c
    invoke-direct {p0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->isParenthesisAutoMultiple(C)Z

    move-result v7

    if-eqz v7, :cond_d

    if-ltz v5, :cond_d

    const-string v7, "\u00d7("

    :goto_4
    invoke-virtual {v6, v5, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_d
    const-string v7, "("

    goto :goto_4
.end method

.method public plusMinus()V
    .locals 14

    .prologue
    const/16 v13, 0x2d

    const/16 v12, 0x29

    const/4 v11, 0x0

    const/16 v9, 0x28

    const/4 v10, 0x1

    .line 964
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 965
    .local v5, "text":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "c":I
    const/4 v6, 0x0

    .line 966
    .local v6, "x":I
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {p0, v7, v11}, Lcom/sec/android/app/popupcalculator/EventHandler;->getCursor(Landroid/widget/EditText;I)I

    move-result v4

    .line 967
    .local v4, "startCorsor":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v3

    .line 969
    .local v3, "insertPos":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 970
    const-string v7, "(\u2212"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 971
    sput-boolean v10, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1065
    :cond_0
    :goto_0
    return-void

    .line 975
    :cond_1
    move v2, v4

    .local v2, "i":I
    :goto_1
    if-lez v2, :cond_5

    .line 976
    add-int/lit8 v7, v2, -0x1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isToken(C)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 977
    add-int/lit8 v7, v2, -0x1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    const/16 v8, 0x2b

    if-eq v7, v8, :cond_2

    add-int/lit8 v7, v2, -0x1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v13, :cond_4

    :cond_2
    add-int/lit8 v7, v2, -0x2

    if-lez v7, :cond_4

    add-int/lit8 v7, v2, -0x2

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    const/16 v8, 0x45

    if-ne v7, v8, :cond_4

    .line 975
    :cond_3
    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 981
    :cond_4
    add-int/lit8 v0, v2, -0x1

    .line 987
    :cond_5
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-eq v7, v13, :cond_6

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-eq v7, v13, :cond_6

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    const/16 v8, 0x2212

    if-ne v7, v8, :cond_f

    .line 988
    :cond_6
    move v3, v0

    .line 990
    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v10, :cond_c

    .line 991
    const/4 v6, 0x3

    .line 1015
    :cond_7
    :goto_3
    packed-switch v6, :pswitch_data_0

    .line 1054
    :cond_8
    :goto_4
    const/4 v7, 0x3

    if-eq v6, v7, :cond_9

    const/4 v7, 0x6

    if-ne v6, v7, :cond_15

    .line 1055
    :cond_9
    sput-boolean v11, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    .line 1060
    :goto_5
    sget-boolean v7, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    if-eqz v7, :cond_a

    .line 1061
    invoke-direct {p0, v3, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->plusMinusAutoPar(ILjava/lang/StringBuilder;)V

    .line 1064
    :cond_a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    goto :goto_0

    .line 983
    :cond_b
    if-ne v2, v10, :cond_3

    add-int/lit8 v7, v2, -0x1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isToken(C)Z

    move-result v7

    if-nez v7, :cond_3

    .line 984
    const/4 v0, 0x0

    goto :goto_2

    .line 992
    :cond_c
    if-lez v0, :cond_d

    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v9, :cond_d

    .line 993
    const/4 v6, 0x6

    goto :goto_3

    .line 994
    :cond_d
    if-nez v0, :cond_e

    .line 995
    const/4 v6, 0x3

    goto :goto_3

    .line 996
    :cond_e
    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-eq v7, v9, :cond_7

    .line 997
    const/4 v6, 0x4

    goto :goto_3

    .line 999
    :cond_f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    const/16 v8, 0x25

    if-eq v7, v8, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    const/16 v8, 0x21

    if-eq v7, v8, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-eq v7, v12, :cond_0

    .line 1003
    move v3, v0

    .line 1004
    if-nez v0, :cond_10

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-eq v7, v9, :cond_10

    .line 1005
    const/4 v6, 0x1

    goto :goto_3

    .line 1006
    :cond_10
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v9, :cond_11

    .line 1007
    const/4 v6, 0x2

    goto :goto_3

    .line 1008
    :cond_11
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v7

    if-ne v7, v12, :cond_12

    .line 1009
    const/4 v6, 0x5

    goto :goto_3

    .line 1011
    :cond_12
    const/4 v6, 0x4

    goto :goto_3

    .line 1017
    :pswitch_0
    const/4 v3, 0x0

    .line 1018
    const-string v7, "(\u2212"

    invoke-virtual {v5, v3, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1021
    :pswitch_1
    add-int/lit8 v3, v3, 0x1

    .line 1022
    const-string v7, "(\u2212"

    invoke-virtual {v5, v3, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1025
    :pswitch_2
    add-int/lit8 v7, v3, 0x1

    const-string v8, ""

    invoke-virtual {v5, v3, v7, v8}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1028
    :pswitch_3
    add-int/lit8 v3, v3, 0x1

    .line 1029
    const-string v7, "(\u2212"

    invoke-virtual {v5, v3, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1032
    :pswitch_4
    iget-boolean v7, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    if-eqz v7, :cond_13

    iget-boolean v7, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsResultError:Z

    if-nez v7, :cond_13

    .line 1033
    add-int/lit8 v7, v3, 0x1

    const-string v8, ""

    invoke-virtual {v5, v3, v7, v8}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1034
    const/4 v7, 0x2

    const-string v8, ""

    invoke-virtual {v5, v11, v7, v8}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1036
    :cond_13
    add-int/lit8 v3, v3, 0x1

    .line 1037
    const-string v7, "\u00d7(\u2212"

    invoke-virtual {v5, v3, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1041
    :pswitch_5
    add-int/lit8 v7, v3, -0x1

    add-int/lit8 v8, v3, 0x1

    const-string v9, ""

    invoke-virtual {v5, v7, v8, v9}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1042
    move v2, v3

    :goto_6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-ge v2, v7, :cond_8

    .line 1043
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    .line 1044
    .local v1, "ch":C
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v7

    if-nez v7, :cond_14

    if-ne v1, v12, :cond_14

    .line 1045
    add-int/lit8 v7, v2, 0x1

    const-string v8, ""

    invoke-virtual {v5, v2, v7, v8}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1046
    iput-boolean v10, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mParanthesisDelete:Z

    goto/16 :goto_4

    .line 1042
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1057
    .end local v1    # "ch":C
    :cond_15
    sput-boolean v10, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsOpenedSignParenthesis:Z

    goto/16 :goto_5

    .line 1015
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public refreshText(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 2997
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2998
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDeleteDot(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2999
    const/4 v4, 0x0

    .line 3000
    .local v4, "start":I
    const/4 v1, 0x0

    .line 3001
    .local v1, "end":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 3002
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 3003
    .local v0, "ch":C
    add-int/lit8 v1, v2, 0x1

    .line 3005
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v6

    if-nez v6, :cond_2

    if-eq v4, v1, :cond_2

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v6

    if-eq v0, v6, :cond_2

    add-int/lit8 v6, v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-gt v6, v7, :cond_2

    .line 3007
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {p1, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 3008
    .local v5, "subString":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3009
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeNumFomat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3010
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3015
    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3016
    move v4, v1

    .line 3001
    .end local v5    # "subString":Ljava/lang/String;
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 3013
    .restart local v5    # "subString":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeNumFomat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 3018
    .end local v5    # "subString":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v1, v6, :cond_0

    if-eq v4, v1, :cond_0

    .line 3019
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeNumFomat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 3023
    .end local v0    # "ch":C
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-nez v6, :cond_4

    .line 3024
    const-string v6, ""

    .line 3030
    :goto_3
    return-object v6

    .line 3026
    :cond_4
    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/StringBuilder;)V

    .line 3027
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Wrong format"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 3028
    const-string v6, ""

    invoke-virtual {p0, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 3030
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3
.end method

.method public resetInstance()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3494
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3495
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMainView:Landroid/view/View;

    .line 3497
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 3498
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mContext:Landroid/content/Context;

    .line 3500
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_2

    .line 3501
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 3504
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    if-eqz v0, :cond_3

    .line 3505
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 3506
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 3507
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->resetInstance()V

    .line 3508
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 3511
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    if-eqz v0, :cond_4

    .line 3512
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    .line 3514
    :cond_4
    return-void
.end method

.method public restoreFomula(ILjava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 3
    .param p1, "backupLength"    # I
    .param p2, "formula"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v2, 0x0

    .line 3393
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 3394
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigitDot(C)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "!"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3414
    :goto_0
    return-object p2

    .line 3404
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 3405
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->clearBackup()V

    goto :goto_0

    .line 3408
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 3409
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 3410
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 3412
    :cond_2
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public setBackSpaceTouch(Z)V
    .locals 0
    .param p1, "isBackSpaceTouch"    # Z

    .prologue
    .line 403
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsBackSpaceTouch:Z

    .line 404
    return-void
.end method

.method public setCursor(I)V
    .locals 4
    .param p1, "cursorPos"    # I

    .prologue
    .line 471
    const/4 v0, 0x0

    .line 472
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    .line 473
    .local v2, "len":I
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le p1, v3, :cond_1

    .line 487
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 477
    if-ne v2, p1, :cond_4

    .line 484
    :cond_2
    if-gez v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v0, v3, :cond_0

    .line 485
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto :goto_0

    .line 480
    :cond_4
    add-int/lit8 v2, v2, -0x1

    .line 481
    add-int/lit8 v0, v0, 0x1

    .line 476
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public setFormula(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 542
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormulaClear()V

    .line 543
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDeleteDot(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 544
    .local v0, "input":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDeleteEquals(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 545
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDeleteNewLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 546
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 547
    return-void
.end method

.method public setFormula(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/StringBuilder;

    .prologue
    .line 550
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 551
    return-void
.end method

.method public setFormulaBackup(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 3442
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3443
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 3445
    :cond_0
    sget-object v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3446
    return-void
.end method

.method public setFormulaLengthBackup(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 3434
    iput p1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaLengthBackup:I

    .line 3435
    return-void
.end method

.method public setLastResult(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 3423
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3424
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 3426
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3427
    return-void
.end method

.method public setNewLineCheck()V
    .locals 1

    .prologue
    .line 526
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mNewLineChek:Z

    .line 527
    return-void
.end method

.method public setText(Ljava/lang/StringBuilder;Z)V
    .locals 7
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "isComma"    # Z

    .prologue
    const/4 v6, 0x0

    .line 625
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 626
    if-eqz p2, :cond_0

    .line 627
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-interface {v2, v6, v3, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 637
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-boolean v6, v2, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 638
    return-void

    .line 630
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 632
    :catch_0
    move-exception v0

    .line 633
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 634
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setUpdatingHistoryCheck(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "check"    # Ljava/lang/Boolean;

    .prologue
    .line 3451
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIsUpdatingHistory:Z

    .line 3452
    return-void
.end method

.method public setView(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;ILcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V
    .locals 0
    .param p1, "display"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .param p2, "hisscreen"    # Landroid/widget/EditText;
    .param p3, "history"    # Lcom/sec/android/app/popupcalculator/History;
    .param p4, "_mIswvga"    # I
    .param p5, "mwActivity"    # Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 374
    iput-object p2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistoryScreen:Landroid/widget/EditText;

    .line 375
    iput-object p3, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    .line 377
    iput p4, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mIswvga:I

    .line 378
    iput-object p5, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 380
    return-void
.end method

.method public setmResult(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 554
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setResultClear()V

    .line 556
    sget-boolean v1, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v1, :cond_0

    .line 557
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    .line 559
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/EventHandler;->getDeleteDot(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 560
    .local v0, "input":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToastHandler:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 392
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToastHandler:Landroid/os/Handler;

    .line 393
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mToastHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/popupcalculator/EventHandler$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/popupcalculator/EventHandler$1;-><init>(Lcom/sec/android/app/popupcalculator/EventHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public sizeTruncate(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "maxLen"    # I

    .prologue
    .line 2263
    const/16 v4, 0x64

    if-ne p2, v4, :cond_0

    .line 2270
    .end local p1    # "str":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 2266
    .restart local p1    # "str":Ljava/lang/String;
    :cond_0
    const/16 v4, 0x45

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2267
    .local v0, "ePos":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 2268
    .local v2, "tail":Ljava/lang/String;
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    .line 2269
    .local v3, "tailLen":I
    sub-int v1, p2, v3

    .line 2270
    .local v1, "maxHeadLen":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v3

    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 2267
    .end local v1    # "maxHeadLen":I
    .end local v2    # "tail":Ljava/lang/String;
    .end local v3    # "tailLen":I
    :cond_1
    const-string v2, ""

    goto :goto_1
.end method

.method public updateBackup(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 7
    .param p1, "result"    # Ljava/lang/String;
    .param p2, "formula"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v6, 0x0

    .line 3365
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 3366
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v6, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 3368
    :cond_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_2

    .line 3369
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3373
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 3374
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 3375
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x2d

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x28

    if-ne v1, v2, :cond_1

    .line 3376
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    add-int/lit8 v2, v0, 0x1

    const-string v3, "-"

    invoke-virtual {v1, v0, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 3374
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3371
    .end local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3381
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mLastResult:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaLengthBackup:I

    .line 3383
    sget-object v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_4

    .line 3384
    sget-object v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v6, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 3386
    :cond_4
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3387
    sget-object v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 3390
    :goto_2
    return-void

    .line 3389
    :cond_5
    sget-object v1, Lcom/sec/android/app/popupcalculator/EventHandler;->mFormulaBackup:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method
