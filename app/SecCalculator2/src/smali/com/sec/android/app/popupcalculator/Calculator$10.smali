.class Lcom/sec/android/app/popupcalculator/Calculator$10;
.super Landroid/database/ContentObserver;
.source "Calculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/Calculator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/Calculator;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 3573
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3577
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->isOnehandAnyScreenMode()Z
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1500(Lcom/sec/android/app/popupcalculator/Calculator;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandRetain:Z
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1600(Lcom/sec/android/app/popupcalculator/Calculator;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->isGetReducedSizeMode()Z
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1700(Lcom/sec/android/app/popupcalculator/Calculator;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3579
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->savePanelState()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1800(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3580
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->changeOnehandView(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1900(Lcom/sec/android/app/popupcalculator/Calculator;I)V

    .line 3581
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->loadPanelState()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$2000(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3582
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandRetain:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1602(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 3584
    :cond_2
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandEnabled:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->isOnehandAnyScreenMode()Z
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1500(Lcom/sec/android/app/popupcalculator/Calculator;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3585
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandRetain:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1602(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 3586
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1400(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3587
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$500(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/EventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventListener;->onPanelOpened(Lcom/sec/android/widgetapp/calculator/Panel;)V

    .line 3588
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->changeOnehandView(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1900(Lcom/sec/android/app/popupcalculator/Calculator;I)V

    .line 3591
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$10;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->invalidateOptionsMenu()V

    .line 3592
    return-void
.end method
