.class public Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;
.super Landroid/database/ContentObserver;
.source "Calculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CustomContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/Calculator;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 1

    .prologue
    .line 678
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    .line 679
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 680
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 8
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 687
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/Calculator;->invalidateOptionsMenu()V

    .line 688
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "accelerometer_rotation"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 690
    .local v1, "setting":I
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->isVolumeKeyPressed:Z
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Calculator;->access$100(Lcom/sec/android/app/popupcalculator/Calculator;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 691
    if-ne v1, v7, :cond_0

    .line 692
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->isVolumeKeyPressed:Z
    invoke-static {v4, v6}, Lcom/sec/android/app/popupcalculator/Calculator;->access$102(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 693
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/Calculator;->checkOrientation()V

    .line 715
    :cond_0
    :goto_0
    return-void

    .line 697
    :cond_1
    if-nez p1, :cond_2

    .line 698
    if-ne v1, v7, :cond_3

    .line 699
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/Calculator;->checkOrientation()V

    .line 713
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/Calculator;->checkHapticFeedbackEnabled()Z

    move-result v5

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mHapticFeedbackEnabled:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->access$302(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 714
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    goto :goto_0

    .line 700
    :cond_3
    if-nez v1, :cond_2

    .line 701
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    const-string v5, "backup_dsp"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 703
    .local v2, "sp":Landroid/content/SharedPreferences;
    const-string v4, "orientationByUser"

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 704
    .local v0, "configByUser":I
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "user_rotation"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 706
    .local v3, "userSetting":I
    if-eq v0, v3, :cond_4

    .line 707
    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mAutoRotateSetting:I
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$202(I)I

    .line 708
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    .line 710
    :cond_4
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "orientationByUser"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method
