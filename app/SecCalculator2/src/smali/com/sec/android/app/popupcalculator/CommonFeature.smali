.class public Lcom/sec/android/app/popupcalculator/CommonFeature;
.super Ljava/lang/Object;
.source "CommonFeature.java"


# static fields
.field public static final CALCULATOR_SUPPORTS_OPENSOURCE_LICENSE:Z

.field public static final CALCULATOR_TW_LANDSCAPE_ONLY:Z

.field public static final CALCULATOR_TW_REMOVE_ACTION_BAR:Z

.field public static final CALCULATOR_TW_SMALL_LCD:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-string v0, "KOREA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CHINA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/popupcalculator/CommonFeature;->CALCULATOR_SUPPORTS_OPENSOURCE_LICENSE:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
