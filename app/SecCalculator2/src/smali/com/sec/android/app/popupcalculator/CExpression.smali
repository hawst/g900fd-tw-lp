.class Lcom/sec/android/app/popupcalculator/CExpression;
.super Ljava/lang/Object;
.source "Logic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/popupcalculator/CExpression$1;
    }
.end annotation


# static fields
.field static final ABS:Ljava/lang/String; = "abs"

.field static final COS:Ljava/lang/String; = "cos"

.field static final DIV:Ljava/lang/String; = "\u00f7"

.field static final FACT:[D

.field static final FACTO:Ljava/lang/String; = "!"

.field static final GAMMA:[D

.field static final LN:Ljava/lang/String; = "ln"

.field static final LOG:Ljava/lang/String; = "log"

.field static final L_PAREN:Ljava/lang/String; = "("

.field static final MINUS:Ljava/lang/String; = "-"

.field static final MUL:Ljava/lang/String; = "\u00d7"

.field static final PERCENTAGE:Ljava/lang/String; = "%"

.field static final PLUS:Ljava/lang/String; = "+"

.field static final ROOT:Ljava/lang/String; = "\u221a"

.field static final R_PAREN:Ljava/lang/String; = ")"

.field static final SIGN:Ljava/lang/String; = "\u2212"

.field static final SIN:Ljava/lang/String; = "sin"

.field static final SQUARE:Ljava/lang/String; = "^"

.field static final TAN:Ljava/lang/String; = "tan"


# instance fields
.field Cursor:I

.field private context:Landroid/content/Context;

.field private exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

.field private expression:Ljava/lang/StringBuilder;

.field private infixStringExp:Ljava/lang/StringBuilder;

.field private infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

.field private mMaxDigitToast:Z

.field private mMaxDotToast:Z

.field private postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

.field private value:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 359
    const/16 v0, 0xe

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/popupcalculator/CExpression;->GAMMA:[D

    .line 379
    const/16 v0, 0x16

    new-array v0, v0, [D

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/popupcalculator/CExpression;->FACT:[D

    return-void

    .line 359
    nop

    :array_0
    .array-data 8
        0x404c93ff87c1acceL    # 57.15623566586292
        -0x3fb2337608fa76d0L    # -59.59796035547549
        0x402c45aea23d22a1L    # 14.136097974741746
        -0x4020847be9da401cL    # -0.4919138160976202
        0x3f01d2af4786183aL    # 3.399464998481189E-5
        0x3f08644bb7c5e3bdL    # 4.652362892704858E-5
        -0x40e63633621a8b49L    # -9.837447530487956E-5
        0x3f24b8939ed4e66dL    # 1.580887032249125E-4
        -0x40d470b232d541caL    # -2.1026444172410488E-4
        0x3f2c801018e9e826L    # 2.1743961811521265E-4
        -0x40da7666366ad9c0L    # -1.643181065367639E-4
        0x3f1621360b773d55L    # 8.441822398385275E-5
        -0x410489734a2e1dfaL    # -2.6190838401581408E-5
        0x3ecef40a04fc9810L    # 3.6899182659531625E-6
    .end array-data

    .line 379
    :array_1
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x40e3b00000000000L    # 40320.0
        0x42b3077775800000L    # 2.0922789888E13
        0x44e06c52687a7b9aL    # 6.204484017332394E23
        0x474956ad0aae33a4L    # 2.631308369336935E35
        0x49e1dd5d037098feL    # 8.159152832478977E47
        0x4c9ee69a78d72cb6L    # 1.2413915592536073E61
        0x4f792693359a4003L    # 7.109985878048635E74
        0x526fe478ee34844aL    # 1.2688693218588417E89
        0x557b5705796695b6L    # 6.1234458376886085E103
        0x589c619094edabffL    # 7.156945704626381E118
        0x5bd0550c4b30743eL    # 1.8548264225739844E134
        0x5f13638dd7bd6347L    # 9.916779348709496E149
        0x62665b0eb1760a70L    # 1.0299016745145628E166
        0x65c7cac197cfe503L    # 1.974506857221074E182
        0x69365f6380a9d916L    # 6.689502913449127E198
        0x6cb1e5dfc140e1e5L    # 3.856204823625804E215
        0x70379185413b0855L    # 3.659042881952549E232
        0x73c8ce85fadb707eL    # 5.5502938327393044E249
        0x776455903aefd5a3L    # 1.3113358856834524E267
        0x7b095d5f3d928edeL    # 4.7147236359920616E284
        0x7eb7932fa79d3a43L    # 2.5260757449731984E302
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1444
    const/16 v0, 0x400

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/popupcalculator/CExpression;-><init>(Landroid/content/Context;I)V

    .line 1446
    return-void
.end method

.method constructor <init>(Landroid/content/Context;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "size"    # I

    .prologue
    const/4 v2, 0x0

    .line 1432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344
    iput-boolean v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->mMaxDigitToast:Z

    .line 346
    iput-boolean v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->mMaxDotToast:Z

    .line 348
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    .line 357
    new-instance v1, Lcom/sec/android/app/popupcalculator/SyntaxException;

    invoke-direct {v1}, Lcom/sec/android/app/popupcalculator/SyntaxException;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    .line 367
    iput v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    .line 1433
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/CExpression;->context:Landroid/content/Context;

    .line 1434
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    .line 1435
    new-array v1, p2, [Lcom/sec/android/app/popupcalculator/CToken;

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    .line 1436
    new-array v1, p2, [Lcom/sec/android/app/popupcalculator/CToken;

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    .line 1437
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 1438
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    new-instance v2, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v2}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    aput-object v2, v1, v0

    .line 1439
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    new-instance v2, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v2}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    aput-object v2, v1, v0

    .line 1437
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1441
    :cond_0
    return-void
.end method

.method public static final abs(D)D
    .locals 2
    .param p0, "x"    # D

    .prologue
    .line 428
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private checkingExpression(Z)V
    .locals 38
    .param p1, "checkEnter"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/popupcalculator/SyntaxException;
        }
    .end annotation

    .prologue
    .line 850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 851
    .local v32, "s":Ljava/lang/String;
    new-instance v35, Ljava/lang/StringBuilder;

    move-object/from16 v0, v35

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->expression:Ljava/lang/StringBuilder;

    .line 853
    const/16 v31, 0x0

    .line 854
    .local v31, "rear":C
    const/16 v30, 0x0

    .line 855
    .local v30, "param_num":I
    const/16 v26, 0x0

    .line 856
    .local v26, "numMax":I
    const/4 v12, 0x0

    .line 857
    .local v12, "comMax":I
    const/4 v15, 0x0

    .line 858
    .local v15, "dot_cnt":I
    const/16 v16, 0x0

    .line 859
    .local v16, "dot_num":I
    const/16 v29, 0x0

    .line 860
    .local v29, "opMax":I
    const/16 v28, 0x0

    .line 861
    .local v28, "opCount":I
    const/16 v18, 0x0

    .line 862
    .local v18, "factError7":Z
    const/16 v17, 0x0

    .line 863
    .local v17, "factError14":Z
    const/4 v3, 0x0

    .line 864
    .local v3, "Error":Z
    const/4 v4, 0x0

    .line 865
    .local v4, "Error1":Z
    const/4 v7, 0x0

    .line 866
    .local v7, "Error2":Z
    const/4 v8, 0x0

    .line 867
    .local v8, "Error4":Z
    const/4 v9, 0x0

    .line 868
    .local v9, "Error5":Z
    const/4 v5, 0x0

    .line 869
    .local v5, "Error14":Z
    const/4 v6, 0x0

    .line 870
    .local v6, "Error15":Z
    const/4 v10, 0x0

    .line 871
    .local v10, "Etc":Z
    const/4 v13, 0x0

    .line 872
    .local v13, "comflag":Z
    const/16 v27, 0x0

    .line 873
    .local v27, "numflag":Z
    const/4 v14, 0x0

    .line 874
    .local v14, "dotInput":Z
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    .line 875
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    move/from16 v0, v20

    move/from16 v1, v35

    if-ge v0, v1, :cond_0

    .line 877
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isCharSet(C)Z

    move-result v35

    if-nez v35, :cond_2

    .line 878
    const/4 v3, 0x1

    .line 1238
    :cond_0
    :goto_1
    if-nez v3, :cond_1

    if-gez v30, :cond_52

    .line 1239
    :cond_1
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_51

    .line 1240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 881
    :cond_2
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x2e

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_3

    .line 882
    const/4 v14, 0x1

    .line 884
    :cond_3
    if-eqz v14, :cond_4

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOpByTwo(C)Z

    move-result v35

    if-eqz v35, :cond_4

    .line 885
    const/4 v14, 0x0

    .line 904
    :cond_4
    const/16 v35, 0x2

    move/from16 v0, v20

    move/from16 v1, v35

    if-le v0, v1, :cond_5

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x2d

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_5

    .line 905
    add-int/lit8 v21, v20, -0x1

    .local v21, "j":I
    :goto_2
    if-ltz v21, :cond_5

    .line 906
    move-object/from16 v0, v32

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x28

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_7

    move-object/from16 v0, v32

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x30

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_7

    move-object/from16 v0, v32

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x221a

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_7

    move-object/from16 v0, v32

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x67

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_7

    .line 920
    .end local v21    # "j":I
    :cond_5
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-nez v35, :cond_6

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x2e

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_1d

    .line 923
    :cond_6
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    add-int/lit8 v35, v35, -0x1

    move/from16 v0, v20

    move/from16 v1, v35

    if-ne v0, v1, :cond_9

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x45

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_9

    .line 924
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_8

    .line 925
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 905
    .restart local v21    # "j":I
    :cond_7
    add-int/lit8 v21, v21, -0x1

    goto/16 :goto_2

    .line 927
    .end local v21    # "j":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 930
    :cond_9
    add-int/lit8 v26, v26, 0x1

    .line 931
    const/16 v27, 0x1

    .line 932
    const/16 v35, 0xf

    move/from16 v0, v26

    move/from16 v1, v35

    if-le v0, v1, :cond_a

    if-nez p1, :cond_a

    .line 938
    const/4 v4, 0x1

    .line 939
    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->mMaxDigitToast:Z

    goto/16 :goto_1

    .line 942
    :cond_a
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->mMaxDigitToast:Z

    .line 944
    if-lez v15, :cond_10

    .line 945
    add-int/lit8 v16, v16, 0x1

    .line 946
    const/16 v35, 0xa

    move/from16 v0, v16

    move/from16 v1, v35

    if-le v0, v1, :cond_10

    if-nez p1, :cond_10

    .line 951
    const/16 v35, 0x45

    move-object/from16 v0, v32

    move/from16 v1, v35

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v25

    .line 952
    .local v25, "nEPos":I
    const/16 v35, -0x1

    move/from16 v0, v25

    move/from16 v1, v35

    if-ne v0, v1, :cond_c

    const/16 v19, 0x0

    .line 954
    .local v19, "hasE":Z
    :goto_3
    add-int/lit8 v21, v20, 0x1

    .restart local v21    # "j":I
    :goto_4
    move/from16 v0, v21

    move/from16 v1, v25

    if-ge v0, v1, :cond_d

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    move/from16 v0, v21

    move/from16 v1, v35

    if-ge v0, v1, :cond_d

    .line 955
    move-object/from16 v0, v32

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v35

    if-nez v35, :cond_b

    .line 956
    const/16 v19, 0x0

    .line 954
    :cond_b
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 952
    .end local v19    # "hasE":Z
    .end local v21    # "j":I
    :cond_c
    const/16 v19, 0x1

    goto :goto_3

    .line 957
    .restart local v19    # "hasE":Z
    .restart local v21    # "j":I
    :cond_d
    if-eqz v19, :cond_e

    if-eqz v19, :cond_f

    const/16 v35, 0x9

    move/from16 v0, v16

    move/from16 v1, v35

    if-le v0, v1, :cond_f

    .line 958
    :cond_e
    const/4 v4, 0x1

    .line 963
    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->mMaxDotToast:Z

    goto/16 :goto_1

    .line 966
    :cond_f
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->mMaxDotToast:Z

    .line 971
    .end local v19    # "hasE":Z
    .end local v21    # "j":I
    .end local v25    # "nEPos":I
    :cond_10
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x2e

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_12

    .line 973
    add-int/lit8 v15, v15, 0x1

    .line 974
    const/16 v35, 0x1

    move/from16 v0, v35

    if-gt v15, v0, :cond_11

    const-string v35, "."

    move-object/from16 v0, v35

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_12

    .line 975
    :cond_11
    const/4 v3, 0x1

    .line 976
    goto/16 :goto_1

    .line 979
    :cond_12
    const/16 v35, 0x2e

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_14

    .line 980
    const/4 v13, 0x1

    .line 981
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x65

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_13

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x3c0

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_14

    .line 982
    :cond_13
    const/4 v3, 0x1

    .line 983
    goto/16 :goto_1

    .line 987
    :cond_14
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x65

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_16

    .line 988
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-eqz v35, :cond_15

    .line 989
    const/4 v3, 0x1

    .line 990
    goto/16 :goto_1

    .line 992
    :cond_15
    const/4 v13, 0x0

    .line 993
    const/4 v12, 0x0

    .line 996
    :cond_16
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x45

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_19

    .line 997
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v35

    if-nez v35, :cond_18

    .line 998
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_17

    .line 999
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1001
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1003
    :cond_18
    const/4 v13, 0x0

    .line 1004
    const/4 v12, 0x0

    .line 1008
    :cond_19
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-eqz v35, :cond_1b

    .line 1011
    const/16 v35, 0x65

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_1a

    const/16 v35, 0x21

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_1a

    const/16 v35, 0x25

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_1a

    const/16 v35, 0x3c0

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_1b

    .line 1012
    :cond_1a
    const/4 v8, 0x1

    .line 1013
    goto/16 :goto_1

    .line 1016
    :cond_1b
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v31

    .line 1018
    if-eqz v13, :cond_1c

    .line 1019
    add-int/lit8 v12, v12, 0x1

    .line 1020
    :cond_1c
    const/16 v35, 0xa

    move/from16 v0, v35

    if-le v12, v0, :cond_22

    if-nez p1, :cond_22

    .line 1021
    const/4 v7, 0x1

    .line 1022
    goto/16 :goto_1

    .line 1026
    :cond_1d
    const/4 v15, 0x0

    .line 1027
    const/16 v16, 0x0

    .line 1028
    const/4 v13, 0x0

    .line 1029
    const/16 v26, 0x0

    .line 1030
    const/4 v12, 0x0

    .line 1031
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x28

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_21

    .line 1032
    add-int/lit8 v30, v30, 0x1

    .line 1034
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-nez v35, :cond_1e

    const/16 v35, 0x29

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_1e

    const/16 v35, 0x21

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_1f

    .line 1035
    :cond_1e
    const/4 v8, 0x1

    .line 1036
    goto/16 :goto_1

    .line 1039
    :cond_1f
    const/16 v35, 0x2e

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_50

    .line 1040
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_20

    .line 1041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0017

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1043
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0058

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1045
    :cond_21
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x29

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_27

    .line 1047
    const/4 v13, 0x0

    .line 1048
    add-int/lit8 v30, v30, -0x1

    .line 1049
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-nez v35, :cond_22

    const/16 v35, 0x21

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_22

    const/16 v35, 0x25

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_22

    const/16 v35, 0x29

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_22

    const/16 v35, 0x2e

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_23

    .line 875
    :cond_22
    :goto_5
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .line 1052
    :cond_23
    const/16 v35, 0x28

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_24

    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v35

    if-eqz v35, :cond_25

    .line 1054
    :cond_24
    const/4 v9, 0x1

    goto/16 :goto_1

    .line 1055
    :cond_25
    const/16 v35, 0xf7

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_26

    .line 1056
    const/16 v17, 0x1

    goto/16 :goto_1

    .line 1058
    :cond_26
    const/4 v3, 0x1

    .line 1059
    goto/16 :goto_1

    .line 1060
    :cond_27
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x2d

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_31

    .line 1061
    const/4 v13, 0x0

    .line 1062
    const/4 v12, 0x0

    .line 1063
    add-int/lit8 v29, v29, 0x1

    .line 1064
    move/from16 v28, v20

    .line 1065
    const/16 v35, 0x1

    move/from16 v0, v20

    move/from16 v1, v35

    if-le v0, v1, :cond_28

    const/16 v35, 0x2d

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_28

    .line 1066
    const/4 v9, 0x1

    .line 1069
    :cond_28
    if-lez v20, :cond_29

    const/16 v35, 0x221a

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_2a

    :cond_29
    const/16 v35, 0x1

    move/from16 v0, v20

    move/from16 v1, v35

    if-le v0, v1, :cond_2c

    const/16 v35, 0x28

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_2c

    add-int/lit8 v35, v20, -0x2

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x221a

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_2c

    .line 1071
    :cond_2a
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_2b

    .line 1072
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0014

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1074
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0009

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1078
    :cond_2c
    if-nez p1, :cond_50

    const-string v35, "log"

    move-object/from16 v0, v32

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_50

    .line 1079
    const-string v36, "log"

    add-int/lit8 v35, v20, -0x3

    if-lez v35, :cond_2f

    add-int/lit8 v35, v20, -0x4

    :goto_6
    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v24

    .line 1080
    .local v24, "log":I
    const/16 v35, 0x28

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_2d

    add-int/lit8 v35, v20, -0x4

    move/from16 v0, v24

    move/from16 v1, v35

    if-eq v0, v1, :cond_2e

    :cond_2d
    const/16 v35, 0x67

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_50

    add-int/lit8 v35, v20, -0x3

    move/from16 v0, v24

    move/from16 v1, v35

    if-ne v0, v1, :cond_50

    .line 1081
    :cond_2e
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_30

    .line 1082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0015

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1079
    .end local v24    # "log":I
    :cond_2f
    const/16 v35, 0x0

    goto :goto_6

    .line 1084
    .restart local v24    # "log":I
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b000a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1088
    .end local v24    # "log":I
    :cond_31
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v35

    if-eqz v35, :cond_38

    .line 1089
    move/from16 v34, v20

    .line 1091
    .local v34, "tmp":I
    :goto_7
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_32

    move-object/from16 v0, v32

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v35

    if-eqz v35, :cond_32

    .line 1092
    add-int/lit8 v34, v34, 0x1

    goto :goto_7

    .line 1094
    :cond_32
    move-object/from16 v0, v32

    move/from16 v1, v20

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v33

    .line 1095
    .local v33, "sub":Ljava/lang/String;
    const-string v35, "sin"

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_34

    const-string v35, "cos"

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_34

    const-string v35, "tan"

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_34

    const-string v35, "log"

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_34

    const-string v35, "ln"

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_34

    const-string v35, "abs"

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-nez v35, :cond_34

    .line 1099
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_33

    .line 1100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1102
    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1104
    :cond_34
    const/16 v35, 0x2e

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_36

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOpByTwo(C)Z

    move-result v35

    if-nez v35, :cond_36

    .line 1105
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_35

    .line 1106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1108
    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1110
    :cond_36
    const/4 v13, 0x0

    .line 1111
    add-int/lit8 v29, v29, 0x1

    .line 1112
    const/4 v12, 0x0

    .line 1113
    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    move-result v35

    add-int/lit8 v35, v35, -0x1

    add-int v20, v20, v35

    .line 1114
    move/from16 v28, v20

    .line 1115
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    move/from16 v0, v34

    move/from16 v1, v35

    if-lt v0, v1, :cond_50

    if-eqz p1, :cond_50

    .line 1116
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_37

    .line 1117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1119
    :cond_37
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1123
    .end local v33    # "sub":Ljava/lang/String;
    .end local v34    # "tmp":I
    :cond_38
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isToken(C)Z

    move-result v35

    if-eqz v35, :cond_50

    .line 1124
    const/4 v13, 0x0

    .line 1125
    const/16 v35, 0x45

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_3a

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x2b

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_3a

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x2d

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_3a

    .line 1127
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_39

    .line 1128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1130
    :cond_39
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1133
    :cond_3a
    const/16 v35, 0x2e

    move/from16 v0, v31

    move/from16 v1, v35

    if-ne v0, v1, :cond_3c

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOpByTwo(C)Z

    move-result v35

    if-nez v35, :cond_3c

    .line 1135
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_3b

    .line 1136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1138
    :cond_3b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1141
    :cond_3c
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x5e

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_43

    .line 1142
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-eqz v35, :cond_3d

    .line 1143
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v31

    .line 1145
    add-int/lit8 v29, v29, 0x1

    .line 1146
    goto/16 :goto_5

    .line 1148
    :cond_3d
    if-nez v31, :cond_3f

    add-int/lit8 v35, v20, 0x1

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v36

    move/from16 v0, v35

    move/from16 v1, v36

    if-ge v0, v1, :cond_3f

    add-int/lit8 v35, v20, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x32

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_3f

    .line 1149
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_3e

    .line 1150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0015

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1152
    :cond_3e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b000a

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1154
    :cond_3f
    if-nez v31, :cond_41

    add-int/lit8 v35, v20, 0x1

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v36

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_41

    .line 1155
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_40

    .line 1156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0015

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1158
    :cond_40
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b000a

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1161
    :cond_41
    const/16 v35, 0x65

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_43

    .line 1163
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-nez v35, :cond_43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->length()I

    move-result v35

    add-int/lit8 v36, v20, 0x1

    move/from16 v0, v35

    move/from16 v1, v36

    if-le v0, v1, :cond_42

    add-int/lit8 v35, v20, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x28

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_43

    .line 1165
    :cond_42
    add-int/lit8 v23, v20, 0x2

    .line 1166
    .local v23, "length":I
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    move/from16 v0, v23

    move/from16 v1, v35

    if-lt v0, v1, :cond_45

    .line 1167
    const/4 v6, 0x1

    .line 1174
    .end local v23    # "length":I
    :cond_43
    :goto_8
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x21

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_46

    .line 1175
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v35

    if-nez v35, :cond_44

    const/16 v35, 0x28

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_44

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    const/16 v36, 0x1

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_46

    .line 1176
    :cond_44
    add-int/lit8 v35, v20, -0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    .line 1177
    const/16 v18, 0x1

    .line 1178
    goto/16 :goto_1

    .line 1168
    .restart local v23    # "length":I
    :cond_45
    move-object/from16 v0, v32

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x32

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_43

    .line 1169
    const/4 v5, 0x1

    goto :goto_8

    .line 1181
    .end local v23    # "length":I
    :cond_46
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x25

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_4a

    .line 1182
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-nez v35, :cond_48

    const/16 v35, 0x29

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_48

    const/16 v35, 0x3c0

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_48

    const/16 v35, 0x2e

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_48

    const/16 v35, 0x21

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_48

    .line 1184
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_47

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1187
    :cond_47
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    add-int/lit8 v37, v20, -0x1

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1190
    :cond_48
    add-int/lit8 v35, v20, 0x1

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v36

    move/from16 v0, v35

    move/from16 v1, v36

    if-ge v0, v1, :cond_4a

    add-int/lit8 v35, v20, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v35

    if-nez v35, :cond_4a

    add-int/lit8 v35, v20, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x29

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_4a

    .line 1192
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_49

    .line 1193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1195
    :cond_49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1201
    :cond_4a
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x221a

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_4c

    .line 1203
    const/16 v35, 0x29

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_4b

    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v35

    if-eqz v35, :cond_4c

    .line 1204
    :cond_4b
    const/4 v8, 0x1

    .line 1205
    add-int/lit8 v35, v20, -0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    goto/16 :goto_1

    .line 1210
    :cond_4c
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x21

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_4d

    .line 1211
    const/16 v26, 0x0

    .line 1212
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    .line 1214
    :cond_4d
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v35

    if-eqz v35, :cond_4f

    .line 1215
    sparse-switch v31, :sswitch_data_0

    .line 1223
    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v35

    if-nez v35, :cond_4e

    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v35

    if-eqz v35, :cond_4f

    const/16 v35, 0x65

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_4f

    .line 1224
    :cond_4e
    add-int/lit8 v35, v20, -0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    .line 1225
    const/4 v9, 0x1

    .line 1230
    :cond_4f
    :goto_9
    :sswitch_0
    const/4 v13, 0x0

    .line 1231
    add-int/lit8 v29, v29, 0x1

    .line 1232
    move/from16 v28, v20

    .line 1233
    const/4 v12, 0x0

    .line 1235
    :cond_50
    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v31

    goto/16 :goto_5

    .line 1219
    :sswitch_1
    add-int/lit8 v35, v20, -0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    .line 1220
    const/4 v9, 0x1

    .line 1221
    goto :goto_9

    .line 1242
    :cond_51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1246
    :cond_52
    if-eqz v4, :cond_54

    .line 1247
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_53

    .line 1248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b000c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1250
    :cond_53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0001

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1254
    :cond_54
    if-eqz v7, :cond_56

    .line 1256
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_55

    .line 1257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b000d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1259
    :cond_55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0002

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1263
    :cond_56
    const/16 v35, 0x14

    move/from16 v0, v29

    move/from16 v1, v35

    if-le v0, v1, :cond_58

    if-nez p1, :cond_58

    .line 1264
    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    .line 1265
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_57

    .line 1266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0012

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1268
    :cond_57
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0007

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1273
    :cond_58
    if-eqz v18, :cond_5a

    .line 1274
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_59

    .line 1275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0014

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1277
    :cond_59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0009

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1281
    :cond_5a
    if-eqz v5, :cond_5c

    .line 1282
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_5b

    .line 1283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0014

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1285
    :cond_5b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0009

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1289
    :cond_5c
    if-eqz v6, :cond_5e

    .line 1290
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_5d

    .line 1291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0014

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1293
    :cond_5d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0009

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1297
    :cond_5e
    if-nez v27, :cond_60

    if-nez v28, :cond_60

    const/16 v35, 0x28

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_60

    const/16 v35, 0x221a

    move/from16 v0, v31

    move/from16 v1, v35

    if-eq v0, v1, :cond_60

    .line 1298
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_5f

    .line 1299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1301
    :cond_5f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1305
    :cond_60
    if-eqz v9, :cond_63

    .line 1306
    if-eqz v10, :cond_61

    .line 1307
    const/4 v10, 0x0

    .line 1308
    :cond_61
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_62

    .line 1309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1311
    :cond_62
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1316
    :cond_63
    move/from16 v11, v30

    .local v11, "clearparam":I
    :goto_a
    if-lez v11, :cond_64

    .line 1317
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    move-object/from16 v35, v0

    const-string v36, ")"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1316
    add-int/lit8 v11, v11, -0x1

    goto :goto_a

    .line 1320
    :cond_64
    if-eqz v8, :cond_66

    .line 1322
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_65

    .line 1323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1325
    :cond_65
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1330
    :cond_66
    if-eqz p1, :cond_6d

    .line 1331
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    add-int/lit8 v35, v35, -0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v35

    if-eqz v35, :cond_68

    .line 1332
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_67

    .line 1333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0015

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1335
    :cond_67
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b000a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1339
    :cond_68
    const/16 v35, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v35

    if-eqz v35, :cond_6a

    .line 1341
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_69

    .line 1342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b000b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1344
    :cond_69
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const/high16 v36, 0x7f0b0000

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1347
    :cond_6a
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    add-int/lit8 v35, v35, -0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v22

    .line 1348
    .local v22, "last":C
    const/16 v35, 0x221a

    move/from16 v0, v22

    move/from16 v1, v35

    if-eq v0, v1, :cond_6b

    const/16 v35, 0x5e

    move/from16 v0, v22

    move/from16 v1, v35

    if-ne v0, v1, :cond_6f

    .line 1350
    :cond_6b
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_6c

    .line 1351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1353
    :cond_6c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1357
    .end local v22    # "last":C
    :cond_6d
    if-nez v10, :cond_6e

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    add-int/lit8 v35, v35, -0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    invoke-static/range {v35 .. v35}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v35

    if-nez v35, :cond_6e

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v35

    add-int/lit8 v35, v35, -0x1

    move-object/from16 v0, v32

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v35

    const/16 v36, 0x2d

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_6f

    .line 1360
    :cond_6e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b008a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1364
    :cond_6f
    if-eqz v17, :cond_71

    .line 1366
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v35

    const-string v36, "capuccino"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_70

    .line 1367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0016

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1369
    :cond_70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-object/from16 v35, v0

    const v36, 0x7f0b0057

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v35

    throw v35

    .line 1371
    :cond_71
    return-void

    .line 1215
    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_0
        0x5e -> :sswitch_1
        0x221a -> :sswitch_1
    .end sparse-switch
.end method

.method public static final cos(D)D
    .locals 4
    .param p0, "x"    # D

    .prologue
    .line 436
    invoke-static {p0, p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/CExpression;->isPiMultiple(D)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method private evaluateByToken(Lcom/sec/android/app/popupcalculator/CToken;Lcom/sec/android/app/popupcalculator/CToken;Lcom/sec/android/app/popupcalculator/CToken;)D
    .locals 8
    .param p1, "t"    # Lcom/sec/android/app/popupcalculator/CToken;
    .param p2, "t_1"    # Lcom/sec/android/app/popupcalculator/CToken;
    .param p3, "t_2"    # Lcom/sec/android/app/popupcalculator/CToken;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/popupcalculator/SyntaxException;
        }
    .end annotation

    .prologue
    .line 450
    const-wide/16 v0, 0x0

    .local v0, "t1":D
    const-wide/16 v2, 0x0

    .line 451
    .local v2, "t2":D
    invoke-virtual {p2}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v0

    .line 452
    invoke-virtual {p3}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v2

    .line 453
    sget-object v4, Lcom/sec/android/app/popupcalculator/CExpression$1;->$SwitchMap$com$sec$android$app$popupcalculator$token_type:[I

    invoke-virtual {p1}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/token_type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 494
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    :goto_0
    return-wide v4

    .line 455
    :pswitch_0
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    goto :goto_0

    .line 481
    :pswitch_1
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v4

    goto :goto_0

    .line 484
    :pswitch_2
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v5

    const/16 v6, 0x64

    const/4 v7, 0x4

    invoke-virtual {v4, v5, v6, v7}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;II)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v4

    goto :goto_0

    .line 488
    :pswitch_3
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v4

    goto :goto_0

    .line 490
    :pswitch_4
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v4

    goto :goto_0

    .line 453
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private evaluateByTokenOne(Lcom/sec/android/app/popupcalculator/CToken;Lcom/sec/android/app/popupcalculator/CToken;)D
    .locals 6
    .param p1, "t"    # Lcom/sec/android/app/popupcalculator/CToken;
    .param p2, "t_1"    # Lcom/sec/android/app/popupcalculator/CToken;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/popupcalculator/SyntaxException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 498
    invoke-virtual {p2}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/popupcalculator/CExpression;->setRounds(D)D

    move-result-wide v0

    .line 499
    .local v0, "t1":D
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    .line 500
    sget-object v2, Lcom/sec/android/app/popupcalculator/CExpression$1;->$SwitchMap$com$sec$android$app$popupcalculator$token_type:[I

    invoke-virtual {p1}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/token_type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 556
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    :goto_0
    return-wide v2

    .line 502
    :pswitch_0
    invoke-virtual {p2}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/popupcalculator/CExpression;->abs(D)D

    move-result-wide v2

    goto :goto_0

    .line 504
    :pswitch_1
    invoke-virtual {p2}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/popupcalculator/CExpression;->persentage(D)D

    move-result-wide v2

    goto :goto_0

    .line 506
    :pswitch_2
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/CExpression;->sin(D)D

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/popupcalculator/CExpression;->setRounds(D)D

    move-result-wide v2

    goto :goto_0

    .line 508
    :pswitch_3
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/CExpression;->cos(D)D

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/popupcalculator/CExpression;->setRounds(D)D

    move-result-wide v2

    goto :goto_0

    .line 510
    :pswitch_4
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/CExpression;->tan(D)D

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/popupcalculator/CExpression;->setRounds(D)D

    move-result-wide v2

    goto :goto_0

    .line 512
    :pswitch_5
    cmpg-double v2, v0, v4

    if-gez v2, :cond_1

    .line 513
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v2

    const-string v3, "capuccino"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 514
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0016

    iget v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 516
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0057

    iget v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 518
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    goto :goto_0

    .line 521
    :pswitch_6
    cmpg-double v2, v0, v4

    if-gez v2, :cond_3

    .line 522
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v2

    const-string v3, "capuccino"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0015

    iget v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 525
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b000a

    iget v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 527
    :cond_3
    invoke-static {v0, v1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    goto/16 :goto_0

    .line 530
    :pswitch_7
    cmpg-double v2, v0, v4

    if-gez v2, :cond_5

    .line 531
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v2

    const-string v3, "capuccino"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 532
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0014

    iget v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 534
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CExpression;->exception:Lcom/sec/android/app/popupcalculator/SyntaxException;

    const v3, 0x7f0b0009

    iget v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->Cursor:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/popupcalculator/SyntaxException;->set(II)Lcom/sec/android/app/popupcalculator/SyntaxException;

    move-result-object v2

    throw v2

    .line 542
    :cond_5
    invoke-virtual {p2}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    goto/16 :goto_0

    .line 550
    :pswitch_8
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/CExpression;->factorial(D)D

    move-result-wide v2

    goto/16 :goto_0

    .line 552
    :pswitch_9
    invoke-virtual {p1}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v2

    mul-double/2addr v0, v2

    move-wide v2, v0

    goto/16 :goto_0

    .line 500
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static final factorial(D)D
    .locals 8
    .param p0, "x"    # D

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 390
    const-wide/16 v4, 0x0

    cmpg-double v3, p0, v4

    if-gez v3, :cond_0

    .line 391
    const-wide/high16 v4, 0x7ff8000000000000L    # NaN

    .line 419
    :goto_0
    return-wide v4

    .line 392
    :cond_0
    const-wide v4, 0x4065400000000000L    # 170.0

    cmpl-double v3, p0, v4

    if-lez v3, :cond_1

    .line 393
    invoke-static {p0, p1}, Lcom/sec/android/app/popupcalculator/CExpression;->lgamma(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    goto :goto_0

    .line 394
    :cond_1
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    cmpl-double v3, v4, p0

    if-eqz v3, :cond_2

    .line 395
    invoke-static {p0, p1}, Lcom/sec/android/app/popupcalculator/CExpression;->lgamma(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    goto :goto_0

    .line 397
    :cond_2
    double-to-int v2, p0

    .line 398
    .local v2, "n":I
    move-wide v0, p0

    .line 399
    .local v0, "extra":D
    and-int/lit8 v3, v2, 0x7

    packed-switch v3, :pswitch_data_0

    .line 419
    invoke-static {p0, p1}, Lcom/sec/android/app/popupcalculator/CExpression;->lgamma(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    goto :goto_0

    .line 401
    :pswitch_0
    sub-double/2addr p0, v6

    mul-double/2addr v0, p0

    .line 403
    :pswitch_1
    sub-double/2addr p0, v6

    mul-double/2addr v0, p0

    .line 405
    :pswitch_2
    sub-double/2addr p0, v6

    mul-double/2addr v0, p0

    .line 407
    :pswitch_3
    sub-double/2addr p0, v6

    mul-double/2addr v0, p0

    .line 409
    :pswitch_4
    sub-double/2addr p0, v6

    mul-double/2addr v0, p0

    .line 411
    :pswitch_5
    sub-double/2addr p0, v6

    mul-double/2addr v0, p0

    .line 413
    :pswitch_6
    sget-object v3, Lcom/sec/android/app/popupcalculator/CExpression;->FACT:[D

    shr-int/lit8 v4, v2, 0x3

    aget-wide v4, v3, v4

    mul-double/2addr v4, v0

    goto :goto_0

    .line 415
    :pswitch_7
    sget-object v3, Lcom/sec/android/app/popupcalculator/CExpression;->FACT:[D

    shr-int/lit8 v4, v2, 0x3

    aget-wide v4, v3, v4

    goto :goto_0

    .line 399
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getToken(Lcom/sec/android/app/popupcalculator/Int;I)Lcom/sec/android/app/popupcalculator/CToken;
    .locals 8
    .param p1, "n"    # Lcom/sec/android/app/popupcalculator/Int;
    .param p2, "abs"    # I

    .prologue
    .line 682
    const/4 v0, 0x0

    .line 683
    .local v0, "c":C
    new-instance v2, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v2}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 685
    .local v2, "t":Lcom/sec/android/app/popupcalculator/CToken;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v5, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isSpace(C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 686
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto :goto_0

    .line 688
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v5, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 689
    const/16 v4, 0x65

    if-ne v0, v4, :cond_1

    .line 690
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 691
    const-wide v4, 0x4005bf0a8b145774L    # 2.71828182845905

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 692
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    .line 845
    :goto_1
    return-object v2

    .line 693
    :cond_1
    const/16 v4, 0x3c0

    if-ne v0, v4, :cond_2

    .line 694
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 695
    const-wide v4, 0x400921fb54442d11L    # 3.14159265358979

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 696
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto :goto_1

    .line 697
    :cond_2
    const/16 v4, 0x2d

    if-ne v0, v4, :cond_9

    .line 698
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/popupcalculator/CExpression;->isSign(Lcom/sec/android/app/popupcalculator/Int;I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 699
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->MINUS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 700
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto :goto_1

    .line 702
    :cond_3
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v3, v4, 0x1

    .line 704
    .local v3, "tmp":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-ge v3, v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x2e

    if-ne v4, v5, :cond_5

    .line 706
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 709
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v5, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 710
    .local v1, "sub":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 711
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->SIGN:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 712
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 713
    iput v3, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto :goto_1

    .line 715
    :cond_6
    const-string v4, "e"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 716
    const-wide v4, -0x3ffa40f574eba88cL    # -2.71828182845905

    iput-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    .line 723
    :goto_3
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 724
    iget-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 725
    iput v3, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 717
    :cond_7
    const-string v4, "\u03c0"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 718
    const-wide v4, -0x3ff6de04abbbd2efL    # -3.14159265358979

    iput-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    goto :goto_3

    .line 720
    :cond_8
    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    .line 721
    iget-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    mul-double/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    goto :goto_3

    .line 728
    .end local v1    # "sub":Ljava/lang/String;
    .end local v3    # "tmp":I
    :cond_9
    const/16 v4, 0x2b

    if-ne v0, v4, :cond_11

    .line 729
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/popupcalculator/CExpression;->isSign(Lcom/sec/android/app/popupcalculator/Int;I)Z

    move-result v4

    if-nez v4, :cond_a

    .line 730
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->PLUS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 731
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 733
    :cond_a
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v3, v4, 0x1

    .line 735
    .restart local v3    # "tmp":I
    :goto_4
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-ge v3, v4, :cond_c

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x2e

    if-ne v4, v5, :cond_c

    .line 737
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 739
    :cond_c
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x1

    if-ne v3, v4, :cond_d

    .line 740
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->ENDMARKER:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 741
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 743
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v5, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 744
    .restart local v1    # "sub":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 745
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->SIGN:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 746
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 747
    iput v3, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 749
    :cond_e
    const-string v4, "e"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 750
    const-wide v4, 0x4005bf0a8b145774L    # 2.71828182845905

    iput-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    .line 755
    :goto_5
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 756
    iget-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 757
    iput v3, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 751
    :cond_f
    const-string v4, "\u03c0"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 752
    const-wide v4, 0x400921fb54442d11L    # 3.14159265358979

    iput-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    goto :goto_5

    .line 754
    :cond_10
    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    goto :goto_5

    .line 761
    .end local v1    # "sub":Ljava/lang/String;
    .end local v3    # "tmp":I
    :cond_11
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 762
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v3, v4, 0x1

    .line 764
    .restart local v3    # "tmp":I
    :goto_6
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-ge v3, v4, :cond_12

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 765
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 767
    :cond_12
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v5, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    invoke-virtual {v4, v5, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 769
    .restart local v1    # "sub":Ljava/lang/String;
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x3

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    .line 770
    const-string v4, "sin"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 771
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->SIN:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto/16 :goto_1

    .line 772
    :cond_13
    const-string v4, "cos"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 773
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->COS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto/16 :goto_1

    .line 774
    :cond_14
    const-string v4, "tan"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 775
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->TAN:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto/16 :goto_1

    .line 776
    :cond_15
    const-string v4, "log"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 777
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->LOG:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto/16 :goto_1

    .line 778
    :cond_16
    const-string v4, "abs"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 779
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->ABS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto/16 :goto_1

    .line 780
    :cond_17
    const-string v4, "ln"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 781
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->LN:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 782
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 784
    :cond_18
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, -0x3

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 787
    .end local v1    # "sub":Ljava/lang/String;
    .end local v3    # "tmp":I
    :cond_19
    const/16 v4, 0x29

    if-ne v0, v4, :cond_1a

    .line 788
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->RPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 843
    :goto_7
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 789
    :cond_1a
    const/16 v4, 0x28

    if-ne v0, v4, :cond_1b

    .line 790
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->LPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 791
    :cond_1b
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isToken(C)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 793
    sparse-switch v0, :sswitch_data_0

    goto :goto_7

    .line 819
    :sswitch_0
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->EOS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 795
    :sswitch_1
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->MULTI:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 798
    :sswitch_2
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->DIVIDE:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 801
    :sswitch_3
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->PLUS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 804
    :sswitch_4
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->MINUS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 807
    :sswitch_5
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->SQUARE:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 810
    :sswitch_6
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->ROOT:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 813
    :sswitch_7
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->PERSENTAGE:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 816
    :sswitch_8
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->FACT:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    goto :goto_7

    .line 825
    :cond_1c
    iget v4, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v3, v4, 0x1

    .line 827
    .restart local v3    # "tmp":I
    :goto_8
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-ge v3, v4, :cond_1f

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_1e

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x2e

    if-eq v4, v5, :cond_1e

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x45

    if-eq v4, v5, :cond_1e

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x2c

    if-eq v4, v5, :cond_1e

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x2d

    if-ne v4, v5, :cond_1d

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x45

    if-eq v4, v5, :cond_1e

    :cond_1d
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x2b

    if-ne v4, v5, :cond_1f

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    const/16 v5, 0x45

    if-ne v4, v5, :cond_1f

    .line 834
    :cond_1e
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 836
    :cond_1f
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v5, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    invoke-virtual {v4, v5, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 837
    .restart local v1    # "sub":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    .line 838
    sget-object v4, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 839
    iget-wide v4, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 840
    iput v3, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    goto/16 :goto_1

    .line 793
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21 -> :sswitch_8
        0x25 -> :sswitch_7
        0x2b -> :sswitch_3
        0x2d -> :sswitch_4
        0x5e -> :sswitch_5
        0xd7 -> :sswitch_1
        0xf7 -> :sswitch_2
        0x221a -> :sswitch_6
    .end sparse-switch
.end method

.method private infix2postfix()V
    .locals 11

    .prologue
    const/16 v9, 0x14

    .line 1379
    const/4 v2, 0x0

    .line 1380
    .local v2, "infix_n":I
    const/4 v6, 0x0

    .line 1381
    .local v6, "postfix_n":I
    new-instance v8, Ljava/util/Stack;

    invoke-direct {v8}, Ljava/util/Stack;-><init>()V

    .line 1382
    .local v8, "tokenStack":Ljava/util/Stack;, "Ljava/util/Stack<Lcom/sec/android/app/popupcalculator/CToken;>;"
    new-instance v5, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v5}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 1383
    .local v5, "ist":Lcom/sec/android/app/popupcalculator/CToken;
    new-instance v1, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v1}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 1384
    .local v1, "ict":Lcom/sec/android/app/popupcalculator/CToken;
    new-array v4, v9, [I

    fill-array-data v4, :array_0

    .line 1387
    .local v4, "isp":[I
    new-array v0, v9, [I

    fill-array-data v0, :array_1

    .line 1391
    .local v0, "icp":[I
    sget-object v9, Lcom/sec/android/app/popupcalculator/token_type;->EOS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 1392
    invoke-virtual {v8, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1393
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "infix_n":I
    .local v3, "infix_n":I
    aget-object v1, v9, v2

    move v2, v3

    .line 1395
    .end local v3    # "infix_n":I
    .restart local v2    # "infix_n":I
    :goto_0
    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/popupcalculator/token_type;->EOS:Lcom/sec/android/app/popupcalculator/token_type;

    if-eq v9, v10, :cond_3

    .line 1396
    sget-object v9, Lcom/sec/android/app/popupcalculator/CExpression$1;->$SwitchMap$com$sec$android$app$popupcalculator$token_type:[I

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/token_type;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 1408
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    check-cast v5, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1409
    .restart local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    :goto_1
    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/token_type;->ordinal()I

    move-result v9

    aget v9, v4, v9

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/token_type;->ordinal()I

    move-result v10

    aget v10, v0, v10

    if-lt v9, v10, :cond_1

    .line 1414
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "postfix_n":I
    .local v7, "postfix_n":I
    aput-object v5, v9, v6

    .line 1415
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    check-cast v5, Lcom/sec/android/app/popupcalculator/CToken;

    .restart local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    move v6, v7

    .end local v7    # "postfix_n":I
    .restart local v6    # "postfix_n":I
    goto :goto_1

    .line 1398
    :pswitch_0
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "postfix_n":I
    .restart local v7    # "postfix_n":I
    aput-object v1, v9, v6

    move v6, v7

    .line 1421
    .end local v7    # "postfix_n":I
    .restart local v6    # "postfix_n":I
    :cond_0
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "infix_n":I
    .restart local v3    # "infix_n":I
    aget-object v1, v9, v2

    move v2, v3

    .end local v3    # "infix_n":I
    .restart local v2    # "infix_n":I
    goto :goto_0

    .line 1401
    :pswitch_1
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    check-cast v5, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1402
    .restart local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    :goto_3
    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/popupcalculator/token_type;->LPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    if-eq v9, v10, :cond_0

    .line 1403
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "postfix_n":I
    .restart local v7    # "postfix_n":I
    aput-object v5, v9, v6

    .line 1404
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    check-cast v5, Lcom/sec/android/app/popupcalculator/CToken;

    .restart local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    move v6, v7

    .end local v7    # "postfix_n":I
    .restart local v6    # "postfix_n":I
    goto :goto_3

    .line 1417
    :cond_1
    invoke-virtual {v8, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1418
    invoke-virtual {v8, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1428
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "postfix_n":I
    .restart local v7    # "postfix_n":I
    aput-object v5, v9, v6

    move v6, v7

    .line 1424
    .end local v7    # "postfix_n":I
    .restart local v6    # "postfix_n":I
    :cond_3
    invoke-virtual {v8}, Ljava/util/Stack;->empty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 1425
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    check-cast v5, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1426
    .restart local v5    # "ist":Lcom/sec/android/app/popupcalculator/CToken;
    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/popupcalculator/token_type;->EOS:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v9, v10, :cond_2

    .line 1430
    :cond_4
    return-void

    .line 1384
    nop

    :array_0
    .array-data 4
        0x0
        0x14
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x11
        0x10
        0x10
        0xf
        0xf
        0xe
        0xd
        0x0
    .end array-data

    .line 1387
    :array_1
    .array-data 4
        0x15
        0x14
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x12
        0x11
        0x10
        0x10
        0xf
        0xf
        0xe
        0xd
        0x0
    .end array-data

    .line 1396
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final isPiMultiple(D)Z
    .locals 4
    .param p0, "x"    # D

    .prologue
    .line 423
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    div-double v0, p0, v2

    .line 424
    .local v0, "d":D
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isSign(Lcom/sec/android/app/popupcalculator/Int;I)Z
    .locals 2
    .param p1, "n"    # Lcom/sec/android/app/popupcalculator/Int;
    .param p2, "abs"    # I

    .prologue
    .line 664
    iget v0, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x29

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x21

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x25

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/sec/android/app/popupcalculator/Int;->data:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_1

    .line 670
    :cond_0
    const/4 v0, 0x0

    .line 672
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final lgamma(D)D
    .locals 12
    .param p0, "x"    # D

    .prologue
    .line 371
    const-wide v6, 0x4014f80000000000L    # 5.2421875

    add-double v4, p0, v6

    .line 372
    .local v4, "tmp":D
    const-wide v2, 0x3fefffffffffffe6L    # 0.9999999999999971

    .line 373
    .local v2, "sum":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/popupcalculator/CExpression;->GAMMA:[D

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 374
    sget-object v1, Lcom/sec/android/app/popupcalculator/CExpression;->GAMMA:[D

    aget-wide v6, v1, v0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    add-double/2addr p0, v8

    div-double/2addr v6, p0

    add-double/2addr v2, v6

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 376
    :cond_0
    const-wide v6, 0x3fed67f1c864beb5L    # 0.9189385332046728

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    add-double/2addr v6, v8

    const-wide v8, 0x4012f80000000000L    # 4.7421875

    sub-double v8, v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    sub-double/2addr v6, v4

    return-wide v6
.end method

.method public static final persentage(D)D
    .locals 2
    .param p0, "x"    # D

    .prologue
    .line 446
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    div-double v0, p0, v0

    return-wide v0
.end method

.method private setRounds(D)D
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 562
    const/16 v0, 0xa

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/popupcalculator/CExpression;->setRounds(DI)D

    move-result-wide v0

    return-wide v0
.end method

.method private setRounds(DI)D
    .locals 19
    .param p1, "v"    # D
    .param p3, "roundingStart"    # I

    .prologue
    .line 566
    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 567
    .local v2, "absv":D
    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v13

    .line 569
    .local v13, "str":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, v13}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 571
    .local v4, "buf":Ljava/lang/StringBuffer;
    const/16 v15, 0x45

    invoke-virtual {v13, v15}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    .line 572
    .local v7, "ePos":I
    const/4 v15, -0x1

    if-eq v7, v15, :cond_1

    add-int/lit8 v15, v7, 0x1

    invoke-virtual {v13, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 574
    .local v12, "rounds":Ljava/lang/String;
    :goto_0
    const/4 v15, -0x1

    if-eq v7, v15, :cond_0

    .line 575
    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 578
    :cond_0
    const/4 v8, 0x0

    .line 580
    .local v8, "exp":I
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    .line 582
    .local v10, "len":I
    const/4 v5, 0x0

    .local v5, "dotPos":I
    :goto_1
    if-ge v5, v10, :cond_2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-eq v15, v0, :cond_2

    .line 583
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 572
    .end local v5    # "dotPos":I
    .end local v8    # "exp":I
    .end local v10    # "len":I
    .end local v12    # "rounds":Ljava/lang/String;
    :cond_1
    const-string v12, ""

    goto :goto_0

    .line 585
    .restart local v5    # "dotPos":I
    .restart local v8    # "exp":I
    .restart local v10    # "len":I
    .restart local v12    # "rounds":Ljava/lang/String;
    :cond_2
    add-int/2addr v8, v5

    .line 587
    if-ge v5, v10, :cond_3

    .line 588
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 589
    add-int/lit8 v10, v10, -0x1

    .line 591
    :cond_3
    add-int p3, p3, v8

    .line 592
    move/from16 v0, p3

    if-ge v0, v10, :cond_6

    .line 595
    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x35

    move/from16 v0, v16

    if-lt v15, v0, :cond_5

    .line 597
    add-int/lit8 v11, p3, -0x1

    .local v11, "p":I
    :goto_2
    if-ltz v11, :cond_4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x39

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 598
    const/16 v15, 0x30

    invoke-virtual {v4, v11, v15}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 597
    add-int/lit8 v11, v11, -0x1

    goto :goto_2

    .line 600
    :cond_4
    if-ltz v11, :cond_8

    .line 601
    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    add-int/lit8 v15, v15, 0x1

    int-to-char v15, v15

    invoke-virtual {v4, v11, v15}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 608
    .end local v11    # "p":I
    :cond_5
    :goto_3
    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 611
    :cond_6
    const/4 v15, -0x5

    if-lt v8, v15, :cond_7

    const/16 v15, 0xf

    if-le v8, v15, :cond_9

    .line 612
    :cond_7
    const/4 v15, 0x1

    const/16 v16, 0x2e

    move/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 613
    add-int/lit8 v8, v8, -0x1

    .line 622
    :goto_4
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    .line 625
    sub-int v15, v10, v5

    add-int/lit8 v6, v15, -0x1

    .line 626
    .local v6, "dotlen":I
    const/16 v15, 0xa

    if-le v6, v15, :cond_d

    .line 627
    add-int/lit8 v6, v6, -0xa

    .line 628
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_5
    if-ge v9, v6, :cond_d

    .line 629
    add-int/lit8 v15, v5, 0xa

    add-int/2addr v15, v9

    add-int/lit8 v15, v15, 0x1

    const/16 v16, 0x30

    move/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 628
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 603
    .end local v6    # "dotlen":I
    .end local v9    # "i":I
    .restart local v11    # "p":I
    :cond_8
    const/4 v15, 0x0

    const/16 v16, 0x31

    move/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 604
    add-int/lit8 p3, p3, 0x1

    .line 605
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 615
    .end local v11    # "p":I
    :cond_9
    move v9, v10

    .restart local v9    # "i":I
    :goto_6
    if-ge v9, v8, :cond_a

    .line 616
    const/16 v15, 0x30

    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 615
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 617
    :cond_a
    move v9, v8

    :goto_7
    if-gtz v9, :cond_b

    .line 618
    const/4 v15, 0x0

    const/16 v16, 0x30

    move/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 617
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 619
    :cond_b
    if-gtz v8, :cond_c

    const/4 v8, 0x1

    .end local v8    # "exp":I
    :cond_c
    const/16 v15, 0x2e

    invoke-virtual {v4, v8, v15}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 620
    const/4 v8, 0x0

    .restart local v8    # "exp":I
    goto :goto_4

    .line 635
    .end local v9    # "i":I
    .restart local v6    # "dotlen":I
    :cond_d
    add-int/lit8 v14, v10, -0x1

    .local v14, "tail":I
    :goto_8
    if-ltz v14, :cond_e

    invoke-virtual {v4, v14}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x30

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 636
    invoke-virtual {v4, v14}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 635
    add-int/lit8 v14, v14, -0x1

    goto :goto_8

    .line 638
    :cond_e
    if-ltz v14, :cond_f

    invoke-virtual {v4, v14}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    const/16 v16, 0x2e

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 639
    invoke-virtual {v4, v14}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 641
    :cond_f
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_10

    .line 642
    const/16 v15, 0x45

    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 644
    :cond_10
    const-wide/16 v16, 0x0

    cmpg-double v15, p1, v16

    if-gez v15, :cond_11

    .line 645
    const/4 v15, 0x0

    const/16 v16, 0x2d

    move/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 646
    :cond_11
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v16

    return-wide v16
.end method

.method public static final sin(D)D
    .locals 2
    .param p0, "x"    # D

    .prologue
    .line 432
    invoke-static {p0, p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/CExpression;->isPiMultiple(D)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static final tan(D)D
    .locals 4
    .param p0, "x"    # D

    .prologue
    .line 440
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4066800000000000L    # 180.0

    rem-double/2addr v0, v2

    const-wide v2, 0x4056800000000000L    # 90.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 441
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .line 442
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/CExpression;->isPiMultiple(D)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p0, p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public checkInputExp(Ljava/lang/StringBuilder;Z)Z
    .locals 6
    .param p1, "input"    # Ljava/lang/StringBuilder;
    .param p2, "checkEnter"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/popupcalculator/SyntaxException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x2d

    const/4 v2, 0x1

    .line 1523
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 1524
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_0

    .line 1525
    add-int/lit8 v3, v1, 0x1

    const-string v4, "-"

    invoke-virtual {p1, v1, v3, v4}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1527
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    const/16 v4, 0x2212

    if-ne v3, v4, :cond_1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-le v3, v2, :cond_1

    .line 1528
    add-int/lit8 v3, v1, 0x1

    const-string v4, "-"

    invoke-virtual {p1, v1, v3, v4}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1530
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    .line 1531
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1523
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1534
    :cond_4
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    const/16 v4, 0x2c

    if-eq v3, v4, :cond_3

    .line 1537
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_6

    .line 1538
    if-eqz v1, :cond_5

    if-eqz v1, :cond_6

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    const/16 v4, 0x28

    if-ne v3, v4, :cond_6

    .line 1539
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    const/16 v4, 0x30

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1542
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1544
    :cond_7
    invoke-direct {p0, p2}, Lcom/sec/android/app/popupcalculator/CExpression;->checkingExpression(Z)V

    .line 1545
    if-eqz p2, :cond_8

    .line 1547
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CExpression;->transInfixStringExp2TokenExp()V

    .line 1548
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/CExpression;->infix2postfix()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1553
    :cond_8
    :goto_2
    return v2

    .line 1549
    :catch_0
    move-exception v0

    .line 1550
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public evaluateExp()Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/app/popupcalculator/SyntaxException;
        }
    .end annotation

    .prologue
    .line 1558
    const-wide/16 v8, 0x0

    .line 1559
    .local v8, "val":D
    const/4 v1, 0x0

    .line 1561
    .local v1, "n":I
    new-instance v5, Ljava/util/Stack;

    invoke-direct {v5}, Ljava/util/Stack;-><init>()V

    .line 1562
    .local v5, "tokenStack":Ljava/util/Stack;, "Ljava/util/Stack<Lcom/sec/android/app/popupcalculator/CToken;>;"
    new-instance v3, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v3}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 1564
    .local v3, "t":Lcom/sec/android/app/popupcalculator/CToken;
    sget-object v10, Lcom/sec/android/app/popupcalculator/token_type;->EOS:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v3, v10}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 1565
    invoke-virtual {v5, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1566
    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .local v2, "n":I
    aget-object v3, v10, v1

    move v1, v2

    .line 1568
    .end local v2    # "n":I
    .restart local v1    # "n":I
    :goto_0
    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->EOS:Lcom/sec/android/app/popupcalculator/token_type;

    if-eq v10, v11, :cond_4

    .line 1569
    sget-object v10, Lcom/sec/android/app/popupcalculator/CExpression$1;->$SwitchMap$com$sec$android$app$popupcalculator$token_type:[I

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/popupcalculator/token_type;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 1609
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1610
    .local v6, "token_1":Lcom/sec/android/app/popupcalculator/CToken;
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1611
    .local v7, "token_2":Lcom/sec/android/app/popupcalculator/CToken;
    invoke-direct {p0, v3, v7, v6}, Lcom/sec/android/app/popupcalculator/CExpression;->evaluateByToken(Lcom/sec/android/app/popupcalculator/CToken;Lcom/sec/android/app/popupcalculator/CToken;Lcom/sec/android/app/popupcalculator/CToken;)D

    move-result-wide v8

    .line 1612
    new-instance v10, Lcom/sec/android/app/popupcalculator/CToken;

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-direct {v10, v11, v8, v9}, Lcom/sec/android/app/popupcalculator/CToken;-><init>(Lcom/sec/android/app/popupcalculator/token_type;D)V

    invoke-virtual {v5, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1615
    .end local v6    # "token_1":Lcom/sec/android/app/popupcalculator/CToken;
    .end local v7    # "token_2":Lcom/sec/android/app/popupcalculator/CToken;
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "n":I
    .restart local v2    # "n":I
    aget-object v3, v10, v1

    move v1, v2

    .end local v2    # "n":I
    .restart local v1    # "n":I
    goto :goto_0

    .line 1571
    :pswitch_0
    invoke-virtual {v5, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1582
    :pswitch_1
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1583
    .restart local v6    # "token_1":Lcom/sec/android/app/popupcalculator/CToken;
    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/popupcalculator/CExpression;->evaluateByTokenOne(Lcom/sec/android/app/popupcalculator/CToken;Lcom/sec/android/app/popupcalculator/CToken;)D

    move-result-wide v8

    .line 1584
    new-instance v10, Lcom/sec/android/app/popupcalculator/CToken;

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-direct {v10, v11, v8, v9}, Lcom/sec/android/app/popupcalculator/CToken;-><init>(Lcom/sec/android/app/popupcalculator/token_type;D)V

    invoke-virtual {v5, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1587
    .end local v6    # "token_1":Lcom/sec/android/app/popupcalculator/CToken;
    :pswitch_2
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1588
    .restart local v6    # "token_1":Lcom/sec/android/app/popupcalculator/CToken;
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1589
    .restart local v7    # "token_2":Lcom/sec/android/app/popupcalculator/CToken;
    invoke-virtual {v5, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1590
    const/4 v4, -0x1

    .line 1591
    .local v4, "tmp":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    array-length v10, v10

    if-ge v0, v10, :cond_0

    .line 1592
    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aget-object v10, v10, v0

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->PERSENTAGE:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v10, v11, :cond_2

    .line 1593
    move v4, v0

    .line 1597
    :cond_0
    invoke-virtual {v7}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v10, v11, :cond_3

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aget-object v10, v10, v1

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->PLUS:Lcom/sec/android/app/popupcalculator/token_type;

    if-eq v10, v11, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->postfixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aget-object v10, v10, v1

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->MINUS:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v10, v11, :cond_3

    :cond_1
    const/4 v10, 0x2

    if-le v4, v10, :cond_3

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v11, v4, -0x2

    aget-object v10, v10, v11

    invoke-virtual {v10}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->LPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    if-eq v10, v11, :cond_3

    .line 1601
    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/popupcalculator/CExpression;->evaluateByTokenOne(Lcom/sec/android/app/popupcalculator/CToken;Lcom/sec/android/app/popupcalculator/CToken;)D

    move-result-wide v8

    .line 1602
    invoke-virtual {v6}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v10

    mul-double/2addr v8, v10

    .line 1606
    :goto_3
    new-instance v10, Lcom/sec/android/app/popupcalculator/CToken;

    sget-object v11, Lcom/sec/android/app/popupcalculator/token_type;->OPERAND:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-direct {v10, v11, v8, v9}, Lcom/sec/android/app/popupcalculator/CToken;-><init>(Lcom/sec/android/app/popupcalculator/token_type;D)V

    invoke-virtual {v5, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1591
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1604
    :cond_3
    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/popupcalculator/CExpression;->evaluateByTokenOne(Lcom/sec/android/app/popupcalculator/CToken;Lcom/sec/android/app/popupcalculator/CToken;)D

    move-result-wide v8

    goto :goto_3

    .line 1617
    .end local v0    # "i":I
    .end local v4    # "tmp":I
    .end local v6    # "token_1":Lcom/sec/android/app/popupcalculator/CToken;
    .end local v7    # "token_2":Lcom/sec/android/app/popupcalculator/CToken;
    :cond_4
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "t":Lcom/sec/android/app/popupcalculator/CToken;
    check-cast v3, Lcom/sec/android/app/popupcalculator/CToken;

    .line 1618
    .restart local v3    # "t":Lcom/sec/android/app/popupcalculator/CToken;
    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    .line 1621
    iget-wide v10, p0, Lcom/sec/android/app/popupcalculator/CExpression;->value:D

    invoke-static {v10, v11}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v10

    return-object v10

    .line 1569
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getMaxDigitToast()Z
    .locals 1

    .prologue
    .line 650
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/CExpression;->mMaxDigitToast:Z

    return v0
.end method

.method public getMaxDotToast()Z
    .locals 1

    .prologue
    .line 654
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/CExpression;->mMaxDotToast:Z

    return v0
.end method

.method public transInfixStringExp2TokenExp()V
    .locals 10

    .prologue
    .line 1450
    new-instance v3, Lcom/sec/android/app/popupcalculator/Int;

    invoke-direct {v3}, Lcom/sec/android/app/popupcalculator/Int;-><init>()V

    .line 1451
    .local v3, "n":Lcom/sec/android/app/popupcalculator/Int;
    const/4 v1, 0x0

    .line 1452
    .local v1, "index":I
    new-instance v4, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v4}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 1453
    .local v4, "t":Lcom/sec/android/app/popupcalculator/CToken;
    const/4 v7, 0x0

    iput v7, v3, Lcom/sec/android/app/popupcalculator/Int;->data:I

    .line 1454
    const/4 v0, 0x0

    .line 1456
    .local v0, "abs_num":I
    :cond_0
    :goto_0
    iget v7, v3, Lcom/sec/android/app/popupcalculator/Int;->data:I

    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixStringExp:Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-ge v7, v8, :cond_9

    .line 1457
    invoke-direct {p0, v3, v0}, Lcom/sec/android/app/popupcalculator/CExpression;->getToken(Lcom/sec/android/app/popupcalculator/Int;I)Lcom/sec/android/app/popupcalculator/CToken;

    move-result-object v4

    .line 1458
    iget v7, v3, Lcom/sec/android/app/popupcalculator/Int;->data:I

    if-nez v7, :cond_1

    .line 1459
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->getmOrigin()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 1460
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->getmTrans()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 1462
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/popupcalculator/token_type;->PERSENTAGE:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v7, v8, :cond_8

    .line 1463
    const/4 v7, 0x3

    if-lt v1, v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v1, -0x2

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/popupcalculator/token_type;->PLUS:Lcom/sec/android/app/popupcalculator/token_type;

    if-eq v7, v8, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v1, -0x2

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/popupcalculator/token_type;->MINUS:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v7, v8, :cond_5

    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v1, -0x3

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/popupcalculator/token_type;->RPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    if-eq v7, v8, :cond_5

    .line 1467
    add-int/lit8 v7, v1, -0x4

    if-ltz v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v1, -0x4

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/popupcalculator/token_type;->LPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v7, v8, :cond_3

    .line 1469
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aput-object v4, v7, v1

    .line 1470
    add-int/lit8 v1, v1, 0x1

    .line 1471
    goto :goto_0

    .line 1474
    :cond_3
    move v2, v1

    .local v2, "j":I
    :goto_1
    add-int/lit8 v7, v1, -0x3

    if-lt v2, v7, :cond_4

    .line 1475
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v2, 0x1

    new-instance v9, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v9}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    aput-object v9, v7, v8

    .line 1476
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v2, 0x1

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 1477
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v2, 0x1

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 1474
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 1479
    :cond_4
    new-instance v5, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v5}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 1480
    .local v5, "t1":Lcom/sec/android/app/popupcalculator/CToken;
    sget-object v7, Lcom/sec/android/app/popupcalculator/token_type;->LPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 1481
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v1, -0x3

    aput-object v5, v7, v8

    .line 1482
    add-int/lit8 v1, v1, 0x1

    .line 1483
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aput-object v4, v7, v1

    .line 1484
    add-int/lit8 v1, v1, 0x1

    .line 1485
    new-instance v6, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v6}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 1486
    .local v6, "t2":Lcom/sec/android/app/popupcalculator/CToken;
    sget-object v7, Lcom/sec/android/app/popupcalculator/token_type;->RPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 1487
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aput-object v6, v7, v1

    .line 1488
    add-int/lit8 v1, v1, 0x1

    .line 1489
    goto/16 :goto_0

    .line 1490
    .end local v2    # "j":I
    .end local v5    # "t1":Lcom/sec/android/app/popupcalculator/CToken;
    .end local v6    # "t2":Lcom/sec/android/app/popupcalculator/CToken;
    :cond_5
    const/4 v7, 0x7

    if-lt v1, v7, :cond_7

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v1, -0x3

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/popupcalculator/token_type;->RPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v7, v8, :cond_7

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v1, -0x7

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/popupcalculator/token_type;->LPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    if-ne v7, v8, :cond_7

    .line 1491
    move v2, v1

    .restart local v2    # "j":I
    :goto_2
    add-int/lit8 v7, v1, -0x7

    if-lt v2, v7, :cond_6

    .line 1492
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v2, 0x1

    new-instance v9, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v9}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    aput-object v9, v7, v8

    .line 1493
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v2, 0x1

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 1494
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v2, 0x1

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenValue()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenValue(D)V

    .line 1491
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 1496
    :cond_6
    new-instance v5, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v5}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 1497
    .restart local v5    # "t1":Lcom/sec/android/app/popupcalculator/CToken;
    sget-object v7, Lcom/sec/android/app/popupcalculator/token_type;->LPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 1498
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    add-int/lit8 v8, v1, -0x7

    aput-object v5, v7, v8

    .line 1499
    add-int/lit8 v1, v1, 0x1

    .line 1500
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aput-object v4, v7, v1

    .line 1501
    add-int/lit8 v1, v1, 0x1

    .line 1502
    new-instance v6, Lcom/sec/android/app/popupcalculator/CToken;

    invoke-direct {v6}, Lcom/sec/android/app/popupcalculator/CToken;-><init>()V

    .line 1503
    .restart local v6    # "t2":Lcom/sec/android/app/popupcalculator/CToken;
    sget-object v7, Lcom/sec/android/app/popupcalculator/token_type;->RPARAM:Lcom/sec/android/app/popupcalculator/token_type;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/popupcalculator/CToken;->setTokenType(Lcom/sec/android/app/popupcalculator/token_type;)V

    .line 1504
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aput-object v6, v7, v1

    .line 1505
    add-int/lit8 v1, v1, 0x1

    .line 1506
    goto/16 :goto_0

    .line 1508
    .end local v2    # "j":I
    .end local v5    # "t1":Lcom/sec/android/app/popupcalculator/CToken;
    .end local v6    # "t2":Lcom/sec/android/app/popupcalculator/CToken;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aput-object v4, v7, v1

    .line 1509
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 1513
    :cond_8
    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CToken;->getTokenType()Lcom/sec/android/app/popupcalculator/token_type;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/popupcalculator/token_type;->ENDMARKER:Lcom/sec/android/app/popupcalculator/token_type;

    if-eq v7, v8, :cond_0

    .line 1515
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/CExpression;->infixTokenExp:[Lcom/sec/android/app/popupcalculator/CToken;

    aput-object v4, v7, v1

    .line 1516
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 1519
    :cond_9
    return-void
.end method
