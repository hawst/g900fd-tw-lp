.class public Lcom/sec/android/app/popupcalculator/HistoryAdapter;
.super Landroid/widget/ArrayAdapter;
.source "HistoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/popupcalculator/HistoryEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

.field mHistory:Lcom/sec/android/app/popupcalculator/History;

.field mPersist:Lcom/sec/android/app/popupcalculator/Persist;

.field private values:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/popupcalculator/HistoryEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/android/app/popupcalculator/EventHandler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .param p4, "handler"    # Lcom/sec/android/app/popupcalculator/EventHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/popupcalculator/HistoryEntry;",
            ">;",
            "Lcom/sec/android/app/popupcalculator/EventHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    .local p3, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/popupcalculator/HistoryEntry;>;"
    const v0, 0x7f040004

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->context:Landroid/content/Context;

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->values:Ljava/util/ArrayList;

    .line 29
    iput-object p4, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 30
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 34
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->context:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 35
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040004

    const/4 v6, 0x0

    invoke-virtual {v0, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 37
    .local v1, "rowView":Landroid/view/View;
    const v5, 0x7f0e0013

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 39
    .local v2, "textViewHistoryExpression":Landroid/widget/TextView;
    const v5, 0x7f0e0014

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 42
    .local v3, "textViewHistoryResult":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxhdp(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 43
    const-string v5, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v5}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v4

    .line 44
    .local v4, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 45
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 47
    .end local v4    # "tf":Landroid/graphics/Typeface;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->values:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->values:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, p1, :cond_1

    .line 48
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/popupcalculator/HistoryEntry;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getBase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/HistoryAdapter;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/popupcalculator/HistoryEntry;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getEdited()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_1
    return-object v1
.end method
