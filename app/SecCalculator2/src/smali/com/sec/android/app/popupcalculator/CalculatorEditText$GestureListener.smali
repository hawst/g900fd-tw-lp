.class Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "CalculatorEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/CalculatorEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)V
    .locals 0

    .prologue
    .line 788
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .param p2, "x1"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    .prologue
    .line 788
    invoke-direct {p0, p1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;-><init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)V

    return-void
.end method


# virtual methods
.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x2

    const/16 v7, 0x2a

    const/16 v6, 0xf

    const/4 v5, 0x5

    const/4 v4, 0x1

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-eq v1, v4, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-ne v1, v4, :cond_1

    .line 845
    :cond_0
    :goto_0
    return v4

    .line 818
    :cond_1
    const/4 v0, 0x0

    .line 819
    .local v0, "setting":I
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 820
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 823
    :cond_2
    if-ne v0, v4, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v8, :cond_3

    sget-boolean v1, Lcom/sec/android/app/popupcalculator/Feature;->CALCULATOR_SUPPORTS_OPENSOURCE_LICENSE:Z

    if-nez v1, :cond_3

    .line 826
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    goto :goto_0

    .line 828
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v8, :cond_0

    .line 829
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 830
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 831
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-ne v1, v4, :cond_4

    .line 832
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/16 v2, 0x13

    invoke-virtual {v1, v6, v2, v7, v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setPadding(IIII)V

    .line 835
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    goto/16 :goto_0

    .line 834
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/16 v2, 0x27

    invoke-virtual {v1, v6, v2, v7, v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setPadding(IIII)V

    goto :goto_1

    .line 838
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-ne v1, v4, :cond_6

    .line 839
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/16 v2, 0x5a

    invoke-virtual {v1, v6, v2, v7, v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setPadding(IIII)V

    .line 842
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->show()V

    goto/16 :goto_0

    .line 841
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/16 v2, 0x9f

    invoke-virtual {v1, v6, v2, v7, v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setPadding(IIII)V

    goto :goto_2
.end method
