.class Lcom/sec/android/app/popupcalculator/EventListener$1;
.super Ljava/lang/Object;
.source "EventListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupcalculator/EventListener;->updateHistory()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/EventListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/EventListener;)V
    .locals 0

    .prologue
    .line 597
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 600
    const/4 v0, 0x0

    .line 601
    .local v0, "focus_check":Z
    const/4 v3, 0x0

    .line 602
    .local v3, "vw1":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$000(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$000(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 603
    const/4 v0, 0x1

    .line 616
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreenParentScrollView:Landroid/widget/ScrollView;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$300(Lcom/sec/android/app/popupcalculator/EventListener;)Landroid/widget/ScrollView;

    move-result-object v4

    const/16 v5, 0x82

    invoke-virtual {v4, v5}, Landroid/widget/ScrollView;->fullScroll(I)Z

    .line 617
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$400(Lcom/sec/android/app/popupcalculator/EventListener;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 618
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$400(Lcom/sec/android/app/popupcalculator/EventListener;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 619
    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$000(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 620
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$000(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->requestFocus()Z

    .line 624
    :cond_1
    :goto_0
    return-void

    .line 606
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$100(Lcom/sec/android/app/popupcalculator/EventListener;)[I

    move-result-object v4

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 607
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$200(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 608
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$200(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I
    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/EventListener;->access$100(Lcom/sec/android/app/popupcalculator/EventListener;)[I

    move-result-object v5

    aget v5, v5, v1

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 609
    .local v2, "vw":Landroid/view/View;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 611
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/EventListener;->access$200(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener$1;->this$0:Lcom/sec/android/app/popupcalculator/EventListener;

    # getter for: Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I
    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/EventListener;->access$100(Lcom/sec/android/app/popupcalculator/EventListener;)[I

    move-result-object v5

    aget v5, v5, v1

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 606
    .end local v2    # "vw":Landroid/view/View;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 621
    .end local v1    # "i":I
    :cond_4
    if-eqz v3, :cond_1

    .line 622
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method
