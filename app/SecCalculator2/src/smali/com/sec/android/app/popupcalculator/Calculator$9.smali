.class Lcom/sec/android/app/popupcalculator/Calculator$9;
.super Landroid/database/ContentObserver;
.source "Calculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/Calculator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/Calculator;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 3512
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 3515
    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hand grip onChange "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 3516
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandSwitchStateValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3538
    :cond_0
    :goto_0
    return-void

    .line 3518
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$700(Lcom/sec/android/app/popupcalculator/Calculator;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3519
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$702(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 3520
    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1300()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->isStateReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3521
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1400(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3522
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$500(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/EventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventListener;->onPanelOpened(Lcom/sec/android/widgetapp/calculator/Panel;)V

    goto :goto_0

    .line 3527
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$700(Lcom/sec/android/app/popupcalculator/Calculator;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3528
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$702(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 3529
    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1300()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->isStateReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3530
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1400(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3531
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$500(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/EventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$9;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventListener;->onPanelOpened(Lcom/sec/android/widgetapp/calculator/Panel;)V

    goto/16 :goto_0

    .line 3516
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
