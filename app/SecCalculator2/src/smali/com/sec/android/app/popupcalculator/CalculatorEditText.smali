.class public Lcom/sec/android/app/popupcalculator/CalculatorEditText;
.super Landroid/widget/EditText;
.source "CalculatorEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DUBLENEWLINE:Ljava/lang/String; = "\n\n"

.field private static final ERROR:I = 0x7f0b0057

.field private static final K_ERROR:I = 0x7f0b0016

.field private static final TAG:Ljava/lang/String; = "CalculatorEditText"

.field public static mBlankCheckResult:Z

.field private static mLineCount:I


# instance fields
.field Cursor_pos:I

.field private gestureDetector:Landroid/view/GestureDetector;

.field private isError:Z

.field public isInitKeyboard:Z

.field private isSpaceBefore:Z

.field private mActionModeCallback:Landroid/view/ActionMode$Callback;

.field private mContext:Landroid/content/Context;

.field public mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

.field public mListener:Lcom/sec/android/app/popupcalculator/EventListener;

.field textWhatcher:Landroid/text/TextWatcher;

.field private tf:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mLineCount:I

    .line 63
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mBlankCheckResult:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 83
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 65
    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z

    .line 69
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->tf:Landroid/graphics/Typeface;

    .line 77
    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isSpaceBefore:Z

    .line 106
    new-instance v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;-><init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->textWhatcher:Landroid/text/TextWatcher;

    .line 750
    new-instance v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText$2;-><init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    .line 85
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;

    .line 87
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v0

    const-string v1, "capuccino"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxxhdp(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    :cond_0
    const-string v0, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->tf:Landroid/graphics/Typeface;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->tf:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 92
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 94
    :cond_2
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText$GestureListener;-><init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->gestureDetector:Landroid/view/GestureDetector;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->textWhatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 103
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isSpaceBefore:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isSpaceBefore:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z

    return p1
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 45
    sget v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mLineCount:I

    return v0
.end method

.method static synthetic access$402(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 45
    sput p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mLineCount:I

    return p0
.end method


# virtual methods
.method public getTextForAccessibility()Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 663
    invoke-super {p0}, Landroid/widget/EditText;->getTextForAccessibility()Ljava/lang/CharSequence;

    move-result-object v0

    .line 664
    .local v0, "msg":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackOn:Z

    if-nez v2, :cond_1

    :cond_0
    move-object v1, v0

    .line 682
    .end local v0    # "msg":Ljava/lang/CharSequence;
    .local v1, "msg":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 668
    .end local v1    # "msg":Ljava/lang/Object;
    .restart local v0    # "msg":Ljava/lang/CharSequence;
    :cond_1
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "^"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b004a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 669
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "sin"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 670
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "tan"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 671
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "cos"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 672
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "!"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b003d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 673
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "log"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0041

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 674
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ln"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0040

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 675
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "abs"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b002e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 676
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u03c0"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0048

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 677
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u221a"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b004d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 678
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u00f7"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0036

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 679
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 680
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 682
    .restart local v1    # "msg":Ljava/lang/Object;
    goto/16 :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 686
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 688
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->forceHideSoftInput(Z)V

    .line 689
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 4
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 709
    invoke-super {p0, p1}, Landroid/widget/EditText;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 710
    const/4 v1, 0x0

    .line 711
    .local v1, "msg":Ljava/lang/CharSequence;
    const/4 v0, 0x0

    .line 712
    .local v0, "isEditing":Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getTextForAccessibility()Ljava/lang/CharSequence;

    move-result-object v1

    .line 714
    if-eqz v1, :cond_0

    .line 715
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 716
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0066

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 721
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 722
    move-object v1, v0

    .line 727
    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 729
    :cond_0
    return-void

    .line 718
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0065

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 724
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 742
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onMeasure(II)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 748
    :goto_0
    return-void

    .line 743
    :catch_0
    move-exception v0

    .line 744
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 745
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V

    .line 746
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onMeasure(II)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 646
    invoke-super {p0, p1}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 649
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->Cursor_pos:I

    .line 650
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 652
    iget v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->Cursor_pos:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->Cursor_pos:I

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 653
    iget v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->Cursor_pos:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 658
    :goto_0
    return-void

    .line 655
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 734
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onSizeChanged(IIII)V

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V

    .line 737
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 693
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 694
    .local v1, "isSuperTouch":Z
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 696
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 697
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 700
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->gestureDetector:Landroid/view/GestureDetector;

    if-eqz v2, :cond_1

    .line 701
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 703
    :cond_1
    return v1
.end method

.method public resetInstance()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 850
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->clearFocus()V

    .line 851
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clearSpans()V

    .line 852
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 854
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->textWhatcher:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->textWhatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 858
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->gestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_1

    .line 859
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->gestureDetector:Landroid/view/GestureDetector;

    .line 862
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 863
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;

    .line 866
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    if-eqz v0, :cond_3

    .line 867
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    .line 870
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-eqz v0, :cond_4

    .line 871
    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 875
    :cond_4
    return-void
.end method
