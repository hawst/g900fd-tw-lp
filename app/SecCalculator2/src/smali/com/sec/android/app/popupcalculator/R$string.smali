.class public final Lcom/sec/android/app/popupcalculator/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Error1:I = 0x7f0b0000

.field public static final Error13:I = 0x7f0b0001

.field public static final Error14:I = 0x7f0b0002

.field public static final Error15:I = 0x7f0b0003

.field public static final Error19:I = 0x7f0b0004

.field public static final Error2:I = 0x7f0b0005

.field public static final Error21:I = 0x7f0b0006

.field public static final Error3:I = 0x7f0b0007

.field public static final Error4:I = 0x7f0b0008

.field public static final Error6:I = 0x7f0b0009

.field public static final Error7:I = 0x7f0b000a

.field public static final Etc:I = 0x7f0b008a

.field public static final K_Error1:I = 0x7f0b000b

.field public static final K_Error13:I = 0x7f0b000c

.field public static final K_Error14:I = 0x7f0b000d

.field public static final K_Error15:I = 0x7f0b000e

.field public static final K_Error19:I = 0x7f0b000f

.field public static final K_Error2:I = 0x7f0b0010

.field public static final K_Error21:I = 0x7f0b0011

.field public static final K_Error3:I = 0x7f0b0012

.field public static final K_Error4:I = 0x7f0b0013

.field public static final K_Error6:I = 0x7f0b0014

.field public static final K_Error7:I = 0x7f0b0015

.field public static final K_error:I = 0x7f0b0016

.field public static final K_errorparen:I = 0x7f0b0017

.field public static final Text_size:I = 0x7f0b0018

.field public static final Text_size_large:I = 0x7f0b0019

.field public static final Text_size_medium:I = 0x7f0b001a

.field public static final Text_size_small:I = 0x7f0b001b

.field public static final about_this_service:I = 0x7f0b001c

.field public static final advanced:I = 0x7f0b001d

.field public static final app_name:I = 0x7f0b001e

.field public static final basic:I = 0x7f0b001f

.field public static final cancel:I = 0x7f0b0020

.field public static final clear:I = 0x7f0b0021

.field public static final clear_history:I = 0x7f0b0022

.field public static final description_0:I = 0x7f0b0023

.field public static final description_1:I = 0x7f0b0024

.field public static final description_1divx:I = 0x7f0b0025

.field public static final description_2:I = 0x7f0b0026

.field public static final description_3:I = 0x7f0b0027

.field public static final description_4:I = 0x7f0b0028

.field public static final description_5:I = 0x7f0b0029

.field public static final description_6:I = 0x7f0b002a

.field public static final description_7:I = 0x7f0b002b

.field public static final description_8:I = 0x7f0b002c

.field public static final description_9:I = 0x7f0b002d

.field public static final description_abs:I = 0x7f0b002e

.field public static final description_add:I = 0x7f0b002f

.field public static final description_arrow_down:I = 0x7f0b0030

.field public static final description_arrow_up:I = 0x7f0b0031

.field public static final description_backspace:I = 0x7f0b0032

.field public static final description_clear:I = 0x7f0b0033

.field public static final description_close:I = 0x7f0b0034

.field public static final description_cos:I = 0x7f0b0035

.field public static final description_devided_by:I = 0x7f0b0036

.field public static final description_div:I = 0x7f0b0037

.field public static final description_dot:I = 0x7f0b0038

.field public static final description_e:I = 0x7f0b0039

.field public static final description_enter_calculation:I = 0x7f0b003a

.field public static final description_equal:I = 0x7f0b003b

.field public static final description_ey:I = 0x7f0b003c

.field public static final description_fact:I = 0x7f0b003d

.field public static final description_fullscr:I = 0x7f0b003e

.field public static final description_leftparenthesis:I = 0x7f0b003f

.field public static final description_ln:I = 0x7f0b0040

.field public static final description_log:I = 0x7f0b0041

.field public static final description_mul:I = 0x7f0b0042

.field public static final description_onehandLeft:I = 0x7f0b0043

.field public static final description_onehandRight:I = 0x7f0b0044

.field public static final description_parenthesis:I = 0x7f0b0045

.field public static final description_pen:I = 0x7f0b0046

.field public static final description_persentage:I = 0x7f0b0047

.field public static final description_pie:I = 0x7f0b0048

.field public static final description_plusminus:I = 0x7f0b0049

.field public static final description_power_of:I = 0x7f0b004a

.field public static final description_redo:I = 0x7f0b004b

.field public static final description_rightparenthesis:I = 0x7f0b004c

.field public static final description_root:I = 0x7f0b004d

.field public static final description_sin:I = 0x7f0b004e

.field public static final description_sub:I = 0x7f0b004f

.field public static final description_tan:I = 0x7f0b0050

.field public static final description_undo:I = 0x7f0b0051

.field public static final description_x2:I = 0x7f0b0052

.field public static final description_xy:I = 0x7f0b0053

.field public static final discard:I = 0x7f0b0054

.field public static final discard_context:I = 0x7f0b0055

.field public static final discard_title:I = 0x7f0b0056

.field public static final error:I = 0x7f0b0057

.field public static final errorparen:I = 0x7f0b0058

.field public static final handwriting:I = 0x7f0b0059

.field public static final keypad:I = 0x7f0b005a

.field public static final max_point:I = 0x7f0b005b

.field public static final maximum_digits_exceeded:I = 0x7f0b005c

.field public static final mini_mode:I = 0x7f0b005d

.field public static final no_history:I = 0x7f0b005e

.field public static final onehand_mode_off:I = 0x7f0b005f

.field public static final onehand_mode_on:I = 0x7f0b0060

.field public static final open_source_license:I = 0x7f0b0061

.field public static final service_name:I = 0x7f0b0062

.field public static final stms_appgroup:I = 0x7f0b0063

.field public static final stms_version:I = 0x7f0b0064

.field public static final tts_double_tap_description:I = 0x7f0b0065

.field public static final tts_editing_description:I = 0x7f0b0066

.field public static final unicode_0:I = 0x7f0b0067

.field public static final unicode_1:I = 0x7f0b0068

.field public static final unicode_1divx:I = 0x7f0b0069

.field public static final unicode_2:I = 0x7f0b006a

.field public static final unicode_3:I = 0x7f0b006b

.field public static final unicode_4:I = 0x7f0b006c

.field public static final unicode_5:I = 0x7f0b006d

.field public static final unicode_6:I = 0x7f0b006e

.field public static final unicode_7:I = 0x7f0b006f

.field public static final unicode_8:I = 0x7f0b0070

.field public static final unicode_9:I = 0x7f0b0071

.field public static final unicode_abs:I = 0x7f0b0072

.field public static final unicode_add:I = 0x7f0b0073

.field public static final unicode_clear:I = 0x7f0b0074

.field public static final unicode_comma:I = 0x7f0b0075

.field public static final unicode_cos:I = 0x7f0b0076

.field public static final unicode_div:I = 0x7f0b0077

.field public static final unicode_dot:I = 0x7f0b0078

.field public static final unicode_e:I = 0x7f0b0079

.field public static final unicode_equal:I = 0x7f0b007a

.field public static final unicode_ey:I = 0x7f0b007b

.field public static final unicode_fact:I = 0x7f0b007c

.field public static final unicode_ln:I = 0x7f0b007d

.field public static final unicode_log:I = 0x7f0b007e

.field public static final unicode_mul:I = 0x7f0b007f

.field public static final unicode_parenthesis:I = 0x7f0b0080

.field public static final unicode_persentage:I = 0x7f0b0081

.field public static final unicode_pie:I = 0x7f0b0082

.field public static final unicode_plusminus:I = 0x7f0b0083

.field public static final unicode_root:I = 0x7f0b0084

.field public static final unicode_sin:I = 0x7f0b0085

.field public static final unicode_sub:I = 0x7f0b0086

.field public static final unicode_tan:I = 0x7f0b0087

.field public static final unicode_x2:I = 0x7f0b0088

.field public static final unicode_xy:I = 0x7f0b0089


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
