.class Lcom/sec/android/app/popupcalculator/Cmyfunc;
.super Ljava/lang/Object;
.source "Logic.java"


# static fields
.field private static mOrigin:Ljava/lang/String;

.field static final mStrTokens:[Ljava/lang/String;

.field private static mTrans:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 58
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "("

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ")"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "abs"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "^"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "sin"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "cos"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "tan"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ln"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\u221a"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "%"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "!"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "log"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\u00d7"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\u00f7"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "-"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "\u2212"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    .line 232
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mTrans:Ljava/lang/String;

    .line 246
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mOrigin:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearmOrigin()V
    .locals 1

    .prologue
    .line 257
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mOrigin:Ljava/lang/String;

    .line 258
    return-void
.end method

.method public static clearmTrans()V
    .locals 1

    .prologue
    .line 243
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mTrans:Ljava/lang/String;

    .line 244
    return-void
.end method

.method static getLastToken(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "strExp"    # Ljava/lang/String;
    .param p1, "startOper"    # I

    .prologue
    const/4 v4, 0x0

    .line 115
    const-string v1, ""

    .line 116
    .local v1, "string":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 117
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v2, v3, :cond_0

    .line 119
    sget-object v2, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, p1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_2

    sget-object v2, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, p1

    invoke-virtual {p0, p1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 122
    sget-object v2, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, p1

    invoke-virtual {p0, p1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 124
    const-string v2, "l"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 116
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_1
    const-string v2, ""

    .line 144
    :goto_1
    return-object v2

    .line 130
    :cond_2
    const-string v2, ""

    goto :goto_1

    .line 132
    :cond_3
    const-string v2, "l"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 133
    add-int/lit8 v2, p1, 0x2

    invoke-virtual {p0, p1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 134
    const-string v2, "ln"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 135
    const-string v2, "ln"

    goto :goto_1

    .line 137
    :cond_4
    const-string v2, "log"

    goto :goto_1

    .line 140
    :cond_5
    sget-object v2, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v2, v2, v0

    goto :goto_1

    .line 144
    :cond_6
    const-string v2, ""

    goto :goto_1
.end method

.method public static getmOrigin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    sget-object v0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mOrigin:Ljava/lang/String;

    return-object v0
.end method

.method public static getmTrans()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mTrans:Ljava/lang/String;

    return-object v0
.end method

.method static isChar(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 195
    const/16 v0, 0x61

    if-gt v0, p0, :cond_0

    const/16 v0, 0x7a

    if-gt p0, v0, :cond_0

    const/16 v0, 0x65

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isCharSet(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 214
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isToken(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isSpace(C)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x2e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x45

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3d

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2c

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isDigit(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 203
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x65

    if-eq p0, v0, :cond_1

    const/16 v0, 0x3c0

    if-eq p0, v0, :cond_1

    const/16 v0, 0x45

    if-eq p0, v0, :cond_1

    const/16 v0, 0x2c

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isDigitDot(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 220
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x2c

    if-eq p0, v0, :cond_1

    const/16 v0, 0x2e

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInteger(Ljava/lang/String;)Z
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 225
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static isLargeChar(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 199
    const/16 v0, 0x41

    if-gt v0, p0, :cond_0

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isOnlyDigit(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 209
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->thousandSepChar()C

    move-result v0

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isOpByOne(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 186
    const/16 v0, 0x21

    if-eq p0, v0, :cond_0

    const/16 v0, 0x221a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x73

    if-eq p0, v0, :cond_0

    const/16 v0, 0x63

    if-eq p0, v0, :cond_0

    const/16 v0, 0x74

    if-eq p0, v0, :cond_0

    const/16 v0, 0x6c

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isOpByTwo(C)Z
    .locals 2
    .param p0, "ch"    # C

    .prologue
    const/16 v1, 0x2d

    .line 181
    const/16 v0, 0x2b

    if-eq p0, v0, :cond_0

    if-eq p0, v1, :cond_0

    const/16 v0, 0xd7

    if-eq p0, v0, :cond_0

    const/16 v0, 0xf7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x25

    if-eq p0, v0, :cond_0

    if-eq p0, v1, :cond_0

    const/16 v0, 0x2212

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isOprator(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 176
    const/16 v0, 0x2b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2212

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd7

    if-eq p0, v0, :cond_0

    const/16 v0, 0xf7

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isSpace(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 191
    const/16 v0, 0x20

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isToken(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 148
    sparse-switch p0, :sswitch_data_0

    .line 172
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 168
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 148
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21 -> :sswitch_0
        0x25 -> :sswitch_0
        0x28 -> :sswitch_0
        0x29 -> :sswitch_0
        0x2b -> :sswitch_0
        0x2d -> :sswitch_0
        0x5e -> :sswitch_0
        0x61 -> :sswitch_0
        0x63 -> :sswitch_0
        0x67 -> :sswitch_0
        0x6c -> :sswitch_0
        0x73 -> :sswitch_0
        0x74 -> :sswitch_0
        0xd7 -> :sswitch_0
        0xf7 -> :sswitch_0
        0x2212 -> :sswitch_0
        0x221a -> :sswitch_0
    .end sparse-switch
.end method

.method public static setmOrigin(Ljava/lang/String;)V
    .locals 0
    .param p0, "mOrigin"    # Ljava/lang/String;

    .prologue
    .line 253
    sput-object p0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mOrigin:Ljava/lang/String;

    .line 254
    return-void
.end method

.method public static setmTrans(Ljava/lang/String;)V
    .locals 0
    .param p0, "mTrans"    # Ljava/lang/String;

    .prologue
    .line 239
    sput-object p0, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mTrans:Ljava/lang/String;

    .line 240
    return-void
.end method

.method static whereLastToken(Ljava/lang/String;Z)I
    .locals 4
    .param p0, "strExp"    # Ljava/lang/String;
    .param p1, "bContainToken"    # Z

    .prologue
    .line 71
    const/4 v1, -0x1

    .line 74
    .local v1, "nFind":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 75
    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 76
    .local v2, "nTmp":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 77
    if-eqz p1, :cond_0

    .line 78
    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v2, v3

    .line 79
    :cond_0
    if-ge v1, v2, :cond_1

    .line 80
    move v1, v2

    .line 74
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    .end local v2    # "nTmp":I
    :cond_2
    return v1
.end method

.method static whereLastTokenArithmetic(Ljava/lang/String;Z)I
    .locals 5
    .param p0, "strExp"    # Ljava/lang/String;
    .param p1, "bContainToken"    # Z

    .prologue
    .line 89
    const/4 v1, -0x1

    .line 92
    .local v1, "nFind":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 94
    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    const-string v4, "\u2212"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    const-string v4, "\u00d7"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    const-string v4, "\u00f7"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 92
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_1
    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 101
    .local v2, "nTmp":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 102
    if-eqz p1, :cond_2

    .line 103
    sget-object v3, Lcom/sec/android/app/popupcalculator/Cmyfunc;->mStrTokens:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v2, v3

    .line 104
    :cond_2
    if-ge v1, v2, :cond_0

    .line 105
    move v1, v2

    goto :goto_1

    .line 109
    .end local v2    # "nTmp":I
    :cond_3
    return v1
.end method
