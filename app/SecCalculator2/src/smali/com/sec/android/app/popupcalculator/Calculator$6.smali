.class Lcom/sec/android/app/popupcalculator/Calculator$6;
.super Ljava/lang/Object;
.source "Calculator.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/Calculator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 0

    .prologue
    .line 1200
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0e004c

    const v5, 0x7f0e004a

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1203
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1237
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1206
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->access$702(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 1207
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$800(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$900(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1208
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$800(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1209
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$900(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1211
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "onehand_direction"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1212
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$000(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->requestFocus()Z

    .line 1213
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1216
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1222
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->access$702(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 1223
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$800(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$900(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1224
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$800(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1225
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$900(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1227
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "onehand_direction"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1228
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$000(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->requestFocus()Z

    .line 1229
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1232
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1233
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$6;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1203
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0047
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
