.class public Lcom/sec/android/app/popupcalculator/Calculator;
.super Landroid/app/Activity;
.source "Calculator.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnHoverListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;
    }
.end annotation


# static fields
.field private static final ACCEPTED_CHARS:[C

.field private static final CAL_MODE_ADVANCED:I = 0x1

.field private static final CAL_MODE_BASIC:I = 0x0

.field private static final CMD_ADVANCED_PANEL:I = 0x3

.field private static final CMD_BASIC_PANEL:I = 0x2

.field private static final CMD_CLEAR_HISTORY:I = 0x1

.field private static final CMD_DISPLAY_LICENSE:I = 0x7

.field private static final CMD_ONEHAND_OFF:I = 0x6

.field private static final CMD_ONEHAND_ON:I = 0x5

.field private static final CURRENT_DISPLAY_FILE:Ljava/lang/String; = "backup_dsp"

.field private static final DECIMAL_BUTTON_TYPE:Ljava/lang/String; = "euro_mode"

.field private static final DECIMAL_EURO:C = ','

.field private static final DECIMAL_US:C = '.'

.field private static final DISPLAY_TEXT:Ljava/lang/String; = "full_display"

.field private static final EXTRA_CURRENT_INDEX:Ljava/lang/String; = "EXTRA_CURRENT_INDEX"

.field private static final EXTRA_ENTER_END:Ljava/lang/String; = "EXTRA_ENTER_END"

.field private static final EXTRA_FORMULA:Ljava/lang/String; = "EXTRA_FURMULA"

.field private static final EXTRA_FORMULA_BACKUP:Ljava/lang/String; = "EXTRA_FORMULA_BACKUP"

.field private static final EXTRA_FORMULA_LENGTH_BACKUP:Ljava/lang/String; = "EXTRA_FORMULA_LENGTH_BACKUP"

.field private static final EXTRA_IS_TOUCHMODE:Ljava/lang/String; = "EXTRA_IS_TOUCHMODE"

.field private static final EXTRA_LAST_RESULT:Ljava/lang/String; = "EXTRA_LAST_RESULT"

.field private static final EXTRA_ORIENTATION:Ljava/lang/String; = "EXTRA_ORIENTATION"

.field private static final EXTRA_PANEL_STATE:Ljava/lang/String; = "EXTRA_PANEL_STATE"

.field private static final EXTRA_RESULT:Ljava/lang/String; = "EXTRA_RESULT"

.field private static final EXTRA_REVIEW:Ljava/lang/String; = "EXTRA_REVIEW"

.field private static final EXTRA_STRING:Ljava/lang/String; = "EXTRA_STRING"

.field private static final IS_ONE_HAND_RETAIN:Ljava/lang/String; = "IS_ONE_HAND_RETAIN"

.field private static MULTIWINDOW_TEXT_SIZE_LARGE:I = 0x0

.field private static MULTIWINDOW_TEXT_SIZE_LARGE_SCIENTIFIC:I = 0x0

.field private static MULTIWINDOW_TEXT_SIZE_MEDIUM:I = 0x0

.field private static MULTIWINDOW_TEXT_SIZE_MEDIUM_SCIENTIFIC:I = 0x0

.field private static NB_BUTTON_COLUMNS:I = 0x0

.field private static final ONE_HAND_DISABLE:I = 0x0

.field private static final ONE_HAND_ENABLE:I = 0x1

.field private static final PANEL_STATE:Ljava/lang/String; = "full_panel"

.field private static STARTTALKBACK_LONGPRESS:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final TEMP_SWAP_SEPARATOR:C = '_'

.field private static TEXT_SIZE_LARGE_LAND:I = 0x0

.field private static TEXT_SIZE_LARGE_PORT:I = 0x0

.field private static TEXT_SIZE_MEDIUM_LAND:I = 0x0

.field private static TEXT_SIZE_MEDIUM_PORT:I = 0x0

.field private static final THOUSAND_SEPARATOR_EURO:C = '.'

.field private static final THOUSAND_SEPARATOR_US:C = ','

.field private static cTalkbackHandler:Landroid/os/Handler;

.field public static euroOn:Z

.field public static isEqualPressed:Z

.field public static isEuroModeOn:Z

.field private static isFlatMode:Z

.field public static isLoadingSavedText:Z

.field public static isOneHandEnabled:Z

.field public static isTouchsounded:Z

.field public static mActionBar:Landroid/app/ActionBar;

.field private static mAutoRotateSetting:I

.field public static mConfigChange:Z

.field private static mCurrentBackkeySoundID:I

.field private static mCurrentSoundID:I

.field private static mCurrentTextSize:I

.field public static mIsGeneral:Z

.field public static mIsKProject:Ljava/lang/String;

.field static mIsRapidKeyInputValue:I

.field public static mIsTalkBackOn:Z

.field public static mIsTalkBackSettingOn:Z

.field private static mLongPressed:Z

.field private static mOnehandAdaptableOperation:Z


# instance fields
.field private HISTORY_TEXT_SIZE_LAND:I

.field private final MULTIWINDOW_PANEL_CONTENT_MIN_HEIGHT:I

.field private bt_id:[I

.field private bt_large_src_id:[I

.field private bt_small_src_id:[I

.field public clearBtn:Landroid/widget/Button;

.field private clear_scr:Z

.field private context:Landroid/content/Context;

.field cursor_position:I

.field filter:Landroid/content/IntentFilter;

.field isIntentContainData:Z

.field private isOneHandRetain:Z

.field private isOptionMenuShow:Z

.field private isVolumeKeyPressed:Z

.field private ivLeft:Landroid/widget/ImageView;

.field private ivRight:Landroid/widget/ImageView;

.field private final mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBackKey:Z

.field private mBundle:Landroid/os/Bundle;

.field private mButtonFontTypeface:Landroid/graphics/Typeface;

.field private mButtonSoundPool:Landroid/media/SoundPool;

.field private mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

.field private mCurrentIndex:I

.field private mCustomObserver:Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;

.field private mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

.field private mHandGripChangeObserver:Landroid/database/ContentObserver;

.field private mHapticFeedbackEnabled:Z

.field private mHasKeyboard:Z

.field private mHistory:Lcom/sec/android/app/popupcalculator/History;

.field private mHistoryScreen:Landroid/widget/EditText;

.field private mIsCreated:Z

.field private mIsFull:Z

.field private mIsScaleWindow:Z

.field private mIsTouchMode:Z

.field private mIsWin:Z

.field private mLeft:Z

.field private mListener:Lcom/sec/android/app/popupcalculator/EventListener;

.field private mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

.field private mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field public mNoHistory:Landroid/widget/EditText;

.field private mOnConfigurationEnable:Z

.field private mOneHandAdaptableOperationState:Z

.field private mOneHandSettingValue:I

.field private mOnehandAnyScreenObserver:Landroid/database/ContentObserver;

.field private mOnehandClickLitener:Landroid/view/View$OnClickListener;

.field private mOnehandLeft:Landroid/widget/LinearLayout;

.field private mOnehandRight:Landroid/widget/LinearLayout;

.field private mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

.field private mPanelHandle:Landroid/view/View;

.field private mPersist:Lcom/sec/android/app/popupcalculator/Persist;

.field private mReview:Landroid/view/View;

.field private mSContextListener:Landroid/hardware/scontext/SContextListener;

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;

.field mVolumeSeekbarFragmentTimer:Landroid/os/CountDownTimer;

.field private model:Ljava/lang/String;

.field private prev_rect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 122
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    .line 123
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->isEqualPressed:Z

    .line 157
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->isTouchsounded:Z

    .line 177
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->euroOn:Z

    .line 233
    const-string v0, "0123456789.+-*/\u2212\u00d7\u00f7()!%^=&@#$"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->ACCEPTED_CHARS:[C

    .line 238
    sput v1, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentSoundID:I

    .line 240
    sput v1, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentBackkeySoundID:I

    .line 242
    sput v1, Lcom/sec/android/app/popupcalculator/Calculator;->mAutoRotateSetting:I

    .line 272
    const-class v0, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    .line 276
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->mConfigChange:Z

    .line 280
    const-string v0, "ro.product.device"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsKProject:Ljava/lang/String;

    .line 302
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentTextSize:I

    .line 304
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_MEDIUM_PORT:I

    .line 306
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_LARGE_PORT:I

    .line 308
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_MEDIUM_LAND:I

    .line 310
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_LARGE_LAND:I

    .line 312
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->MULTIWINDOW_TEXT_SIZE_MEDIUM:I

    .line 314
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->MULTIWINDOW_TEXT_SIZE_LARGE:I

    .line 316
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->MULTIWINDOW_TEXT_SIZE_MEDIUM_SCIENTIFIC:I

    .line 318
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->MULTIWINDOW_TEXT_SIZE_LARGE_SCIENTIFIC:I

    .line 326
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/popupcalculator/Calculator;->NB_BUTTON_COLUMNS:I

    .line 336
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsGeneral:Z

    .line 340
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->isLoadingSavedText:Z

    .line 342
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandEnabled:Z

    .line 364
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackOn:Z

    .line 366
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackSettingOn:Z

    .line 372
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->mLongPressed:Z

    .line 376
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    .line 378
    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->STARTTALKBACK_LONGPRESS:I

    .line 380
    sput-boolean v2, Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z

    .line 1353
    new-instance v0, Lcom/sec/android/app/popupcalculator/Calculator$7;

    invoke-direct {v0}, Lcom/sec/android/app/popupcalculator/Calculator$7;-><init>()V

    sput-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->cTalkbackHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v2, 0x1388

    const/16 v1, 0x23

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 104
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 159
    iput-boolean v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isIntentContainData:Z

    .line 163
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clear_scr:Z

    .line 169
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isVolumeKeyPressed:Z

    .line 171
    iput v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOneHandSettingValue:I

    .line 173
    iput-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 175
    iput-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->context:Landroid/content/Context;

    .line 180
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    .line 189
    iput-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonFontTypeface:Landroid/graphics/Typeface;

    .line 191
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_large_src_id:[I

    .line 212
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_small_src_id:[I

    .line 274
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTouchMode:Z

    .line 282
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOptionMenuShow:Z

    .line 284
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandRetain:Z

    .line 320
    iput v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->MULTIWINDOW_PANEL_CONTENT_MIN_HEIGHT:I

    .line 322
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    .line 328
    iput v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentIndex:I

    .line 330
    iput-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 332
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsWin:Z

    .line 334
    iput-boolean v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsFull:Z

    .line 338
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->prev_rect:Landroid/graphics/Rect;

    .line 344
    iput v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->HISTORY_TEXT_SIZE_LAND:I

    .line 346
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBackKey:Z

    .line 360
    iput-boolean v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    .line 362
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnConfigurationEnable:Z

    .line 370
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->filter:Landroid/content/IntentFilter;

    .line 382
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z

    .line 384
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsCreated:Z

    .line 1200
    new-instance v0, Lcom/sec/android/app/popupcalculator/Calculator$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupcalculator/Calculator$6;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    .line 1887
    new-instance v0, Lcom/sec/android/app/popupcalculator/Calculator$8;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/popupcalculator/Calculator$8;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mVolumeSeekbarFragmentTimer:Landroid/os/CountDownTimer;

    .line 3512
    new-instance v0, Lcom/sec/android/app/popupcalculator/Calculator$9;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator$9;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHandGripChangeObserver:Landroid/database/ContentObserver;

    .line 3573
    new-instance v0, Lcom/sec/android/app/popupcalculator/Calculator$10;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator$10;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAnyScreenObserver:Landroid/database/ContentObserver;

    .line 3621
    new-instance v0, Lcom/sec/android/app/popupcalculator/Calculator$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupcalculator/Calculator$11;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    .line 3721
    new-instance v0, Lcom/sec/android/app/popupcalculator/Calculator$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupcalculator/Calculator$12;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

    return-void

    .line 180
    :array_0
    .array-data 4
        0x7f0e001a
        0x7f0e001e
        0x7f0e001f
        0x7f0e0020
        0x7f0e0025
        0x7f0e0021
        0x7f0e001d
        0x7f0e002b
        0x7f0e0022
        0x7f0e0023
        0x7f0e0024
        0x7f0e001c
        0x7f0e001b
        0x7f0e002a
        0x7f0e0026
        0x7f0e0027
        0x7f0e0028
        0x7f0e0029
        0x7f0e002c
        0x7f0e002d
        0x7f0e0031
        0x7f0e0032
        0x7f0e0033
        0x7f0e0034
        0x7f0e0035
        0x7f0e0036
        0x7f0e002e
        0x7f0e0037
        0x7f0e0038
        0x7f0e0039
        0x7f0e003a
        0x7f0e003b
        0x7f0e003c
        0x7f0e002f
        0x7f0e0030
    .end array-data

    .line 191
    :array_1
    .array-data 4
        0x7f02043a
        0x7f0205a2
        0x7f0205a4
        0x7f0205a6
        0x7f020427
        0x7f02042f
        0x7f02040c
        0x7f020415
        0x7f02059c
        0x7f02059e
        0x7f0205a0
        0x7f020421
        0x7f020413
        0x7f020423
        0x7f020594
        0x7f020596
        0x7f020598
        0x7f02059a
        0x7f02040e
        0x7f020429
        0x7f02043c
        0x7f02042d
        0x7f020411
        0x7f020431
        0x7f02041d
        0x7f02041f
        0x7f020433
        0x7f02041b
        0x7f020417
        0x7f020435
        0x7f020437
        0x7f02040a
        0x7f020425
        0x7f020419
        0x7f02042b
    .end array-data

    .line 212
    :array_2
    .array-data 4
        0x7f020439
        0x7f0205a1
        0x7f0205a3
        0x7f0205a5
        0x7f020426
        0x7f02042e
        0x7f02040b
        0x7f020414
        0x7f02059b
        0x7f02059d
        0x7f02059f
        0x7f020420
        0x7f020412
        0x7f020422
        0x7f020593
        0x7f020595
        0x7f020597
        0x7f020599
        0x7f02040d
        0x7f020428
        0x7f02043b
        0x7f02042c
        0x7f020410
        0x7f020430
        0x7f02041c
        0x7f02041e
        0x7f020432
        0x7f02041a
        0x7f020416
        0x7f020434
        0x7f020436
        0x7f020409
        0x7f020424
        0x7f020418
        0x7f02042a
    .end array-data
.end method

.method private UnregisterFlatMotionListener()V
    .locals 3

    .prologue
    .line 3611
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3612
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v0, :cond_0

    .line 3613
    const-string v0, "scontext"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 3615
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 3616
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v1, "UnregisterFlatMotionListener"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3619
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/popupcalculator/Calculator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isVolumeKeyPressed:Z

    return v0
.end method

.method static synthetic access$1000()I
    .locals 1

    .prologue
    .line 104
    sget v0, Lcom/sec/android/app/popupcalculator/Calculator;->STARTTALKBACK_LONGPRESS:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isVolumeKeyPressed:Z

    return p1
.end method

.method static synthetic access$1102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 104
    sput-boolean p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLongPressed:Z

    return p0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300()Z
    .locals 1

    .prologue
    .line 104
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z

    return v0
.end method

.method static synthetic access$1302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 104
    sput-boolean p0, Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z

    return p0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/popupcalculator/Calculator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isOnehandAnyScreenMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/popupcalculator/Calculator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandRetain:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandRetain:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/popupcalculator/Calculator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isGetReducedSizeMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->savePanelState()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/popupcalculator/Calculator;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;
    .param p1, "x1"    # I

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/sec/android/app/popupcalculator/Calculator;->changeOnehandView(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadPanelState()V

    return-void
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 104
    sput p0, Lcom/sec/android/app/popupcalculator/Calculator;->mAutoRotateSetting:I

    return p0
.end method

.method static synthetic access$2100()Z
    .locals 1

    .prologue
    .line 104
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->saveCurrentText()V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initControls()V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/popupcalculator/Calculator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHapticFeedbackEnabled:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/EventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/EventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/popupcalculator/Calculator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/popupcalculator/Calculator;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/Calculator;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private changeOnehandView(I)V
    .locals 2
    .param p1, "oneHandEnalbe"    # I

    .prologue
    .line 2860
    invoke-direct {p0, p1}, Lcom/sec/android/app/popupcalculator/Calculator;->setOneHandMode(I)V

    .line 2861
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V

    .line 2862
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventListener;->onPanelOpened(Lcom/sec/android/widgetapp/calculator/Panel;)V

    .line 2863
    return-void
.end method

.method public static changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "isEuro"    # Z

    .prologue
    .line 1176
    if-eqz p1, :cond_0

    .line 1178
    const-string v0, "."

    const-string v1, "_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 1179
    const-string v0, ","

    const-string v1, "."

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 1180
    const-string v0, "_"

    const-string v1, ","

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 1188
    :goto_0
    return-object p0

    .line 1183
    :cond_0
    const-string v0, ","

    const-string v1, "_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 1184
    const-string v0, "."

    const-string v1, ","

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 1185
    const-string v0, "_"

    const-string v1, "."

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private change_layout(ZZZ)V
    .locals 22
    .param p1, "iswindow"    # Z
    .param p2, "change"    # Z
    .param p3, "rotate"    # Z

    .prologue
    .line 3042
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 3158
    :cond_0
    :goto_0
    return-void

    .line 3044
    :cond_1
    if-eqz p1, :cond_7

    .line 3045
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/Calculator;->mIsWin:Z

    .line 3046
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsFull:Z

    .line 3061
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsWin:Z

    if-eqz v13, :cond_0

    .line 3062
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v13}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v10

    .line 3063
    .local v10, "rect":Landroid/graphics/Rect;
    const-wide/16 v14, 0x0

    .line 3064
    .local v14, "width":D
    const-wide/16 v6, 0x0

    .line 3065
    .local v6, "height":D
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v13

    int-to-double v14, v13

    .line 3066
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v13

    int-to-double v6, v13

    .line 3068
    const/16 v2, 0x2f0

    .line 3070
    .local v2, "FULL_HEIGHT":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v13, v13, Landroid/content/res/Configuration;->orientation:I

    packed-switch v13, :pswitch_data_0

    .line 3089
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->prev_rect:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v13

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v16

    move/from16 v0, v16

    if-ne v13, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->prev_rect:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v13

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v16

    move/from16 v0, v16

    if-eq v13, v0, :cond_0

    .line 3090
    :cond_3
    const/4 v11, 0x0

    .line 3091
    .local v11, "scientific":Z
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsCreated:Z

    if-nez v13, :cond_e

    .line 3092
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->onSaveRotationData()V

    .line 3095
    :goto_2
    cmpl-double v13, v14, v6

    if-lez v13, :cond_f

    .line 3096
    const/4 v11, 0x1

    .line 3097
    const/4 v13, 0x0

    sput-boolean v13, Lcom/sec/android/app/popupcalculator/Calculator;->mIsGeneral:Z

    .line 3098
    const v13, 0x7f040007

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->setContentView(I)V

    .line 3099
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initControls()V

    .line 3107
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/ActionBar;->getHeight()I

    move-result v3

    .line 3108
    .local v3, "actionbar_height":I
    const/4 v12, 0x0

    .line 3109
    .local v12, "stateBar":I
    if-nez v3, :cond_4

    .line 3110
    const/16 v3, 0x4b

    .line 3112
    :cond_4
    iget v13, v10, Landroid/graphics/Rect;->top:I

    if-nez v13, :cond_5

    .line 3113
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/popupcalculator/Calculator;->checkIs599Mdpi(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 3114
    const/16 v12, 0xf

    .line 3118
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const v16, 0x7f0e000a

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    iget v4, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 3120
    .local v4, "handler_height":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const v16, 0x7f0e0009

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 3124
    .local v9, "params2":Landroid/view/ViewGroup$LayoutParams;
    const/4 v5, 0x0

    .line 3125
    .local v5, "mDisplay_layout_height":I
    const v13, 0x7f0e0006

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    iget v5, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 3127
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v13

    sub-int/2addr v13, v5

    sub-int/2addr v13, v12

    sub-int/2addr v13, v3

    sub-int/2addr v13, v4

    iput v13, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 3130
    iget v13, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ge v13, v0, :cond_6

    .line 3131
    const/4 v13, 0x1

    iput v13, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 3133
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const v16, 0x7f0e0009

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3135
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->prev_rect:Landroid/graphics/Rect;

    invoke-virtual {v13, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 3136
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v13, v13, Landroid/content/res/Configuration;->orientation:I

    packed-switch v13, :pswitch_data_1

    .line 3152
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    .line 3155
    :goto_5
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadPanelState()V

    goto/16 :goto_0

    .line 3047
    .end local v2    # "FULL_HEIGHT":I
    .end local v3    # "actionbar_height":I
    .end local v4    # "handler_height":I
    .end local v5    # "mDisplay_layout_height":I
    .end local v6    # "height":D
    .end local v9    # "params2":Landroid/view/ViewGroup$LayoutParams;
    .end local v10    # "rect":Landroid/graphics/Rect;
    .end local v11    # "scientific":Z
    .end local v12    # "stateBar":I
    .end local v14    # "width":D
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsFull:Z

    move/from16 v16, v0

    if-nez p1, :cond_a

    const/4 v13, 0x1

    :goto_6
    move/from16 v0, v16

    if-ne v0, v13, :cond_8

    if-eqz p2, :cond_2

    .line 3048
    :cond_8
    if-nez p1, :cond_b

    const/4 v13, 0x1

    :goto_7
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsFull:Z

    .line 3049
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsWin:Z

    .line 3050
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v8

    .line 3051
    .local v8, "panelState":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->onSaveRotationData()V

    .line 3052
    const v13, 0x7f040005

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->setContentView(I)V

    .line 3053
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initControls()V

    .line 3054
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v8, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->setOpen(ZZ)Z

    .line 3055
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v13

    if-eqz v13, :cond_9

    .line 3056
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v13}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->requestFocus()Z

    .line 3058
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->prev_rect:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_0

    .line 3047
    .end local v8    # "panelState":Z
    :cond_a
    const/4 v13, 0x0

    goto :goto_6

    .line 3048
    :cond_b
    const/4 v13, 0x0

    goto :goto_7

    .line 3072
    .restart local v2    # "FULL_HEIGHT":I
    .restart local v6    # "height":D
    .restart local v10    # "rect":Landroid/graphics/Rect;
    .restart local v14    # "width":D
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/popupcalculator/Calculator;->checkIs599Mdpi(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 3073
    const/16 v2, 0x3d4

    goto/16 :goto_1

    .line 3075
    :cond_c
    const/16 v2, 0x4d0

    .line 3077
    goto/16 :goto_1

    .line 3079
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/popupcalculator/Calculator;->checkIs599Mdpi(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_d

    .line 3080
    const/16 v2, 0x230

    goto/16 :goto_1

    .line 3082
    :cond_d
    const/16 v2, 0x2f0

    .line 3084
    goto/16 :goto_1

    .line 3094
    .restart local v11    # "scientific":Z
    :cond_e
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsCreated:Z

    goto/16 :goto_2

    .line 3102
    :cond_f
    const/4 v13, 0x1

    sput-boolean v13, Lcom/sec/android/app/popupcalculator/Calculator;->mIsGeneral:Z

    .line 3103
    const v13, 0x7f040006

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->setContentView(I)V

    .line 3104
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initControls()V

    goto/16 :goto_3

    .line 3116
    .restart local v3    # "actionbar_height":I
    .restart local v12    # "stateBar":I
    :cond_10
    const/16 v12, 0x21

    goto/16 :goto_4

    .line 3138
    .restart local v4    # "handler_height":I
    .restart local v5    # "mDisplay_layout_height":I
    .restart local v9    # "params2":Landroid/view/ViewGroup$LayoutParams;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const v16, 0x7f0e0009

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    iget v13, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-double v0, v13

    move-wide/from16 v16, v0

    int-to-double v0, v2

    move-wide/from16 v18, v0

    const-wide v20, 0x3fd3333333333333L    # 0.3

    mul-double v18, v18, v20

    cmpl-double v13, v16, v18

    if-lez v13, :cond_12

    .line 3139
    const/16 v16, 0x0

    if-nez v11, :cond_11

    const/4 v13, 0x1

    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->setSrc(ZZ)V

    goto/16 :goto_5

    :cond_11
    const/4 v13, 0x0

    goto :goto_8

    .line 3141
    :cond_12
    const/16 v16, 0x1

    if-nez v11, :cond_13

    const/4 v13, 0x1

    :goto_9
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->setSrc(ZZ)V

    goto/16 :goto_5

    :cond_13
    const/4 v13, 0x0

    goto :goto_9

    .line 3145
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const v16, 0x7f0e0009

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    iget v13, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    div-int/lit8 v16, v2, 0x3

    move/from16 v0, v16

    if-le v13, v0, :cond_15

    .line 3146
    const/16 v16, 0x0

    if-nez v11, :cond_14

    const/4 v13, 0x1

    :goto_a
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->setSrc(ZZ)V

    goto/16 :goto_5

    :cond_14
    const/4 v13, 0x0

    goto :goto_a

    .line 3148
    :cond_15
    const/16 v16, 0x1

    if-nez v11, :cond_16

    const/4 v13, 0x1

    :goto_b
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v13}, Lcom/sec/android/app/popupcalculator/Calculator;->setSrc(ZZ)V

    goto/16 :goto_5

    :cond_16
    const/4 v13, 0x0

    goto :goto_b

    .line 3070
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 3136
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static checkIs599Mdpi(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 3444
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 3447
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static decimalChar()C
    .locals 1

    .prologue
    .line 1156
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x2c

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2e

    goto :goto_0
.end method

.method private deleteSavedText()V
    .locals 4

    .prologue
    .line 2371
    const-string v1, "backup_dsp"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2374
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "full_display"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2375
    return-void
.end method

.method private getDefaultTheme()I
    .locals 1

    .prologue
    .line 2429
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isBlack(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 2430
    const/4 v0, 0x5

    .line 2432
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private initActionBar()V
    .locals 5

    .prologue
    const v2, 0x106000d

    const/4 v4, 0x1

    .line 994
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_5

    .line 996
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v0

    if-eq v0, v4, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v4, :cond_6

    .line 997
    :cond_0
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 1002
    :goto_0
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1003
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_7

    .line 1005
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 1008
    :goto_1
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMidas(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 1009
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 1011
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 1012
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 1013
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 1014
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setIcon(I)V

    .line 1037
    :cond_2
    :goto_2
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxxhdp(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxhdp(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1040
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/popupcalculator/Calculator$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/popupcalculator/Calculator$5;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1050
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_5

    .line 1051
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 1053
    :cond_5
    return-void

    .line 999
    :cond_6
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1000
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    goto/16 :goto_0

    .line 1007
    :cond_7
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_1

    .line 1015
    :cond_8
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v0

    const-string v1, "capuccino"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1016
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setIcon(I)V

    goto :goto_2
.end method

.method private initButtonTypeface()V
    .locals 4

    .prologue
    .line 3007
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonFontTypeface:Landroid/graphics/Typeface;

    if-nez v2, :cond_0

    .line 3008
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/SamsungCal-4Tv_20141006.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonFontTypeface:Landroid/graphics/Typeface;

    .line 3012
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3013
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 3014
    .local v1, "vw":Landroid/view/View;
    if-nez v1, :cond_2

    .line 3020
    .end local v1    # "vw":Landroid/view/View;
    :cond_1
    return-void

    .line 3017
    .restart local v1    # "vw":Landroid/view/View;
    :cond_2
    instance-of v2, v1, Landroid/widget/Button;

    if-eqz v2, :cond_3

    .line 3018
    check-cast v1, Landroid/widget/Button;

    .end local v1    # "vw":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3012
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private initCalculatorEditText()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 809
    const v0, 0x7f0e0018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 810
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setWritingBuddyEnabled(Z)V

    .line 811
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->forceHideSoftInput(Z)V

    .line 812
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 813
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setNewActionPopupMenu(IZ)V

    .line 814
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const v1, -0x4d140e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setHighlightColor(I)V

    .line 815
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const-string v1, "inputType=PredictionOff"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 816
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f0a0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-nez v0, :cond_0

    const/high16 v0, -0x1000000

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setCursorColor(I)V

    .line 818
    return-void

    .line 816
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private initClearBtn()V
    .locals 2

    .prologue
    .line 924
    const v0, 0x7f0e0016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    .line 925
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 926
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 927
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 928
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 934
    :cond_1
    return-void
.end method

.method private initControls()V
    .locals 8

    .prologue
    .line 1115
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    .line 1116
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->setInitValues()V

    .line 1117
    const/4 v7, 0x0

    .line 1119
    .local v7, "windowMode":I
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    .line 1120
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z

    .line 1123
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initCalculatorEditText()V

    .line 1124
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initHistoryEditText()V

    .line 1125
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initPanel()V

    .line 1127
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-nez v0, :cond_3

    .line 1128
    new-instance v0, Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isWvga(Landroid/content/Context;)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/popupcalculator/EventHandler;-><init>(Landroid/content/Context;Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;ILcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 1134
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isCocktailBarFeature(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1135
    const v0, 0x7f0e0018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/16 v1, 0x258

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1137
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initClearBtn()V

    .line 1138
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initListeners()V

    .line 1139
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadCurrentText()V

    .line 1140
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1141
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadPanelState()V

    .line 1142
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadCurrentText()V

    .line 1144
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initActionBar()V

    .line 1145
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->restoreIntentContainData()V

    .line 1146
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initEuroModeDot()V

    .line 1147
    return-void

    .line 1131
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isWvga(Landroid/content/Context;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setView(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;ILcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V

    goto :goto_0
.end method

.method private initEuroModeDot()V
    .locals 3

    .prologue
    const v2, 0x7f0e002b

    const/4 v1, 0x1

    .line 1056
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v0, :cond_1

    .line 1057
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isBlack(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_2

    .line 1059
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02040f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1068
    :cond_1
    :goto_0
    return-void

    .line 1062
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1063
    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0b0075

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method private initHistoryEditText()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 822
    const v2, 0x7f0e0015

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    .line 823
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxhdp(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxxhdp(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 825
    :cond_0
    const-string v2, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v2}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 826
    .local v1, "tf":Landroid/graphics/Typeface;
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 827
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 829
    .end local v1    # "tf":Landroid/graphics/Typeface;
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v2

    if-eq v2, v4, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v2

    if-eq v2, v4, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v2

    if-eq v2, v4, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKlimt(Landroid/content/Context;)I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 831
    :cond_2
    const v2, 0x7f0e003d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    .line 832
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    if-eqz v2, :cond_3

    .line 833
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    const v3, 0x7f0b005e

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(I)V

    .line 834
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setWritingBuddyEnabled(Z)V

    .line 837
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->forceHideSoftInput(Z)V

    .line 838
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setWritingBuddyEnabled(Z)V

    .line 840
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_4

    .line 841
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 842
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 843
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v2

    if-eq v2, v4, :cond_4

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v2

    if-eq v2, v4, :cond_4

    .line 844
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 845
    .local v0, "history_scroll":Landroid/view/View;
    if-eqz v0, :cond_4

    .line 846
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 852
    .end local v0    # "history_scroll":Landroid/view/View;
    :cond_4
    return-void
.end method

.method private initListeners()V
    .locals 12

    .prologue
    .line 960
    const v0, 0x7f0e0019

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 962
    .local v7, "linear":Landroid/widget/LinearLayout;
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    if-nez v0, :cond_0

    .line 963
    new-instance v0, Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-direct {v0}, Lcom/sec/android/app/popupcalculator/EventListener;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    .line 964
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventListener;->initializePanelSound(Landroid/content/Context;)V

    .line 967
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    .line 968
    .local v11, "frm":Landroid/app/FragmentManager;
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    invoke-virtual/range {v0 .. v11}, Lcom/sec/android/app/popupcalculator/EventListener;->setHandler(Landroid/content/Context;Lcom/sec/android/app/popupcalculator/EventHandler;Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;Landroid/view/View;Landroid/widget/Button;Landroid/view/View;Landroid/widget/EditText;Landroid/app/FragmentManager;)V

    .line 970
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-eqz v0, :cond_1

    .line 971
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->setOnPanelListener(Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;)V

    .line 972
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 974
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHasKeyboard:Z

    if-eqz v0, :cond_2

    .line 975
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 978
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->unregisterbuttonListener()V

    .line 979
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->registerPanelButtonListener()V

    .line 981
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/popupcalculator/Calculator$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/popupcalculator/Calculator$3;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 990
    return-void
.end method

.method private initPanel()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const v8, 0x7f0e0008

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 855
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 856
    invoke-virtual {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/calculator/Panel;

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 858
    const v3, 0x7f0e000a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    .line 917
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v6, :cond_1

    .line 918
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const v4, 0x7f0e0005

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0e0006

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/widgetapp/calculator/Panel;->setAddAnimationLayout(Landroid/view/View;Landroid/view/View;Landroid/widget/EditText;)V

    .line 921
    :cond_1
    return-void

    .line 860
    :cond_2
    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 862
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandModeValue()I

    move-result v3

    if-ne v3, v6, :cond_7

    .line 863
    const v3, 0x7f04000a

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 868
    .local v1, "layout":Landroid/view/View;
    :goto_1
    const v3, 0x7f0e0011

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 870
    .local v2, "parent":Landroid/view/ViewGroup;
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/calculator/Panel;

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 871
    if-eqz v2, :cond_3

    .line 872
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 873
    :cond_3
    const v3, 0x7f0e000a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    .line 874
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const/16 v4, 0x50

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/calculator/Panel;->setGravity(I)V

    .line 876
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.feature.hovering_ui"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 878
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setHoverPopupType(I)V

    .line 880
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandModeValue()I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 881
    const v3, 0x7f0e0046

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    .line 882
    const v3, 0x7f0e0048

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    .line 884
    const v3, 0x7f0e0047

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    .line 885
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 886
    const v3, 0x7f0e0049

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    .line 887
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 889
    iget-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    if-eqz v3, :cond_8

    .line 890
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 891
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 892
    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v3, :cond_5

    .line 893
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 894
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 896
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-eq v3, v6, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 898
    :cond_6
    const v3, 0x7f0e004a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 899
    const v3, 0x7f0e004c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 866
    .end local v1    # "layout":Landroid/view/View;
    .end local v2    # "parent":Landroid/view/ViewGroup;
    :cond_7
    const v3, 0x7f040009

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "layout":Landroid/view/View;
    goto/16 :goto_1

    .line 902
    .restart local v2    # "parent":Landroid/view/ViewGroup;
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 903
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 904
    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v3, :cond_9

    .line 905
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 906
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 908
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-eq v3, v6, :cond_a

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 910
    :cond_a
    const v3, 0x7f0e004a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 911
    const v3, 0x7f0e004c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private isGetReducedSizeMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3552
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "any_screen_enabled"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 3554
    .local v0, "isReducedSizeMode":Z
    :goto_0
    return v0

    .end local v0    # "isReducedSizeMode":Z
    :cond_0
    move v0, v1

    .line 3552
    goto :goto_0
.end method

.method private isJapanModel()Z
    .locals 2

    .prologue
    .line 3666
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    const-string v1, "SGH-N098"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    const-string v1, "SCH-J003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3668
    :cond_0
    const/4 v0, 0x1

    .line 3669
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOnehandAnyScreenMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3544
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "any_screen_running"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 3546
    .local v0, "isOnehandAnyScreen":Z
    :goto_0
    return v0

    .end local v0    # "isOnehandAnyScreen":Z
    :cond_0
    move v0, v1

    .line 3544
    goto :goto_0
.end method

.method private isOnehandMode()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3028
    const/4 v0, 0x0

    .line 3032
    .local v0, "flag":Z
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "onehand_calculator_enabled"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v4, :cond_0

    .line 3035
    const/4 v0, 0x1

    .line 3036
    sput-boolean v4, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandEnabled:Z

    .line 3038
    :cond_0
    return v0
.end method

.method public static isTalkBackEnabled(Landroid/content/Context;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 3756
    const/16 v1, 0x3a

    .line 3757
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string v0, "com.google.android.marvin.talkback"

    .line 3758
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    new-instance v8, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v10, 0x3a

    invoke-direct {v8, v10}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 3759
    .local v8, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    const/4 v2, 0x0

    .line 3760
    .local v2, "classNewName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 3762
    .local v7, "packageNewName":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 3787
    :cond_0
    :goto_0
    return v9

    .line 3765
    :cond_1
    const/4 v6, 0x0

    .line 3766
    .local v6, "enabledServicesSetting":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 3767
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "enabled_accessibility_services"

    invoke-static {v10, v11}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3769
    :cond_2
    if-nez v6, :cond_3

    .line 3770
    const-string v6, ""

    .line 3773
    :cond_3
    move-object v3, v8

    .line 3774
    .local v3, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v3, v6}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 3776
    :cond_4
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 3777
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v4

    .line 3778
    .local v4, "componentNameString":Ljava/lang/String;
    invoke-static {v4}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v5

    .line 3779
    .local v5, "enabledService":Landroid/content/ComponentName;
    if-eqz v5, :cond_4

    .line 3780
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 3781
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 3782
    const-string v10, "com.google.android.marvin.talkback"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const-string v10, "com.google.android.marvin.talkback"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const-string v10, "com.google.android.marvin.talkback.TalkBackService"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 3784
    const/4 v9, 0x1

    goto :goto_0
.end method

.method private loadCurrentText()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2352
    const-string v3, "backup_dsp"

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2354
    .local v1, "sp":Landroid/content/SharedPreferences;
    const-string v3, "full_display"

    const-string v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2356
    .local v2, "strRtn":Ljava/lang/String;
    const-string v3, "error_check"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2357
    .local v0, "error_check":I
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iput v0, v3, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    .line 2358
    sput-boolean v6, Lcom/sec/android/app/popupcalculator/Calculator;->isLoadingSavedText:Z

    .line 2359
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-boolean v6, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 2360
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2361
    iget v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->cursor_position:I

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->cursor_position:I

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    if-gt v3, v4, :cond_0

    .line 2362
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->cursor_position:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 2366
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-boolean v5, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 2367
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->deleteSavedText()V

    .line 2368
    return-void

    .line 2364
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto :goto_0
.end method

.method private loadPanelState()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2294
    const-string v3, "backup_dsp"

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2296
    .local v2, "sp":Landroid/content/SharedPreferences;
    const-string v3, "full_panel"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 2297
    .local v1, "panelRtn":Z
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v3

    if-eq v3, v4, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v3

    if-ne v3, v4, :cond_4

    .line 2298
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->setOpen(ZZ)Z

    .line 2299
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->setNew800mdpi_PortraitPanelState(Z)V

    .line 2309
    :cond_1
    :goto_0
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "full_panel"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2311
    const-string v3, "error_check"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2312
    .local v0, "error_check":I
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-eqz v3, :cond_2

    .line 2313
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iput v0, v3, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    .line 2315
    :cond_2
    const-string v3, "optionMenuShow"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOptionMenuShow:Z

    .line 2316
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "optionMenuShow"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2318
    const-string v3, "editisFocus"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnConfigurationEnable:Z

    if-eqz v3, :cond_3

    .line 2319
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->requestFocus()Z

    .line 2320
    :cond_3
    sput-boolean v5, Lcom/sec/android/app/popupcalculator/Calculator;->isLoadingSavedText:Z

    .line 2321
    return-void

    .line 2302
    .end local v0    # "error_check":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3, v1, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->setOpen(ZZ)Z

    .line 2303
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0015

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    if-eqz v3, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2305
    sget-object v3, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3}, Landroid/app/ActionBar;->hide()V

    goto :goto_0
.end method

.method private nullViewDrawable(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3692
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3698
    :goto_0
    :try_start_1
    move-object v0, p1

    check-cast v0, Landroid/widget/ImageView;

    move-object v1, v0

    .line 3699
    .local v1, "imageView":Landroid/widget/ImageView;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3700
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3704
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :goto_1
    return-void

    .line 3693
    :catch_0
    move-exception v2

    goto :goto_0

    .line 3701
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method private nullViewDrawablesRecursive(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3674
    if-eqz p1, :cond_1

    .line 3676
    :try_start_0
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    move-object v4, v0

    .line 3678
    .local v4, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 3679
    .local v2, "childCount":I
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 3680
    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3681
    .local v1, "child":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->nullViewDrawablesRecursive(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3679
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3683
    .end local v1    # "child":Landroid/view/View;
    .end local v2    # "childCount":I
    .end local v3    # "index":I
    .end local v4    # "viewGroup":Landroid/view/ViewGroup;
    :catch_0
    move-exception v5

    .line 3686
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/popupcalculator/Calculator;->nullViewDrawable(Landroid/view/View;)V

    .line 3688
    :cond_1
    return-void
.end method

.method private refreshView()V
    .locals 13

    .prologue
    .line 2866
    const v9, 0x7f0e0011

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 2867
    .local v5, "parent":Landroid/view/ViewGroup;
    if-eqz v5, :cond_0

    .line 2868
    const v9, 0x7f0e0008

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2869
    :cond_0
    sget-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-nez v9, :cond_1

    .line 2870
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z

    .line 2873
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 2876
    const v9, 0x7f0e0019

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 2877
    .local v3, "lp":Landroid/widget/LinearLayout;
    if-nez v3, :cond_2

    .line 3004
    .end local v3    # "lp":Landroid/widget/LinearLayout;
    :goto_0
    return-void

    .line 2880
    .restart local v3    # "lp":Landroid/widget/LinearLayout;
    :cond_2
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 2881
    .local v4, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandModeValue()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_d

    .line 2882
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090005

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2886
    :goto_1
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 2888
    .end local v3    # "lp":Landroid/widget/LinearLayout;
    .end local v4    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_3
    const-string v9, "layout_inflater"

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2892
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandModeValue()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_e

    .line 2893
    const v9, 0x7f04000a

    const/4 v10, 0x0

    invoke-virtual {v1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2894
    .local v2, "layout":Landroid/view/View;
    const v9, 0x7f0e0008

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/sec/android/widgetapp/calculator/Panel;

    iput-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 2895
    if-eqz v5, :cond_4

    .line 2896
    const v9, 0x7f0e0008

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2898
    :cond_4
    const/4 v9, 0x1

    sput-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandEnabled:Z

    .line 2899
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const/16 v10, 0x50

    invoke-virtual {v9, v10}, Lcom/sec/android/widgetapp/calculator/Panel;->setGravity(I)V

    .line 2911
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandModeValue()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_a

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z

    move-result v9

    if-nez v9, :cond_a

    .line 2912
    const v9, 0x7f0e0046

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    .line 2914
    const v9, 0x7f0e0048

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    .line 2916
    const v9, 0x7f0e0047

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    .line 2917
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    if-eqz v9, :cond_5

    .line 2918
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2919
    :cond_5
    const v9, 0x7f0e0049

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    .line 2920
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    if-eqz v9, :cond_6

    .line 2921
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2923
    :cond_6
    iget-boolean v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    if-eqz v9, :cond_10

    .line 2924
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    if-eqz v9, :cond_7

    .line 2925
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2926
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2928
    :cond_7
    sget-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v9, :cond_8

    .line 2929
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2930
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2932
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0004

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_9

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0006

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_a

    .line 2934
    :cond_9
    const v9, 0x7f0e004a

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_a

    const v9, 0x7f0e004c

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 2935
    const v9, 0x7f0e004a

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2936
    const v9, 0x7f0e004c

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2957
    :cond_a
    :goto_3
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v9, v10}, Lcom/sec/android/app/popupcalculator/EventListener;->setPanel(Lcom/sec/android/widgetapp/calculator/Panel;)V

    .line 2958
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v10, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v9, v10}, Lcom/sec/android/widgetapp/calculator/Panel;->setOnPanelListener(Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;)V

    .line 2960
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->unregisterbuttonListener()V

    .line 2963
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_4
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    array-length v9, v9

    if-ge v0, v9, :cond_b

    .line 2964
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    aget v9, v9, v0

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 2965
    .local v8, "vw":Landroid/view/View;
    if-nez v8, :cond_14

    .line 2966
    sget-object v9, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "refreshView Button ID = null , id index "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2997
    .end local v8    # "vw":Landroid/view/View;
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initButtonTypeface()V

    .line 2999
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v9

    if-eqz v9, :cond_19

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 3001
    .local v7, "view":Landroid/view/View;
    :goto_5
    if-eqz v7, :cond_c

    .line 3002
    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    .line 3003
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->invalidateOptionsMenu()V

    goto/16 :goto_0

    .line 2884
    .end local v0    # "i":I
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "layout":Landroid/view/View;
    .end local v7    # "view":Landroid/view/View;
    .restart local v3    # "lp":Landroid/widget/LinearLayout;
    .restart local v4    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090004

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_1

    .line 2902
    .end local v3    # "lp":Landroid/widget/LinearLayout;
    .end local v4    # "params":Landroid/view/ViewGroup$LayoutParams;
    .restart local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_e
    const v9, 0x7f040009

    const/4 v10, 0x0

    invoke-virtual {v1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2903
    .restart local v2    # "layout":Landroid/view/View;
    const v9, 0x7f0e0008

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/sec/android/widgetapp/calculator/Panel;

    iput-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 2904
    if-eqz v5, :cond_f

    .line 2905
    const v9, 0x7f0e0008

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2906
    :cond_f
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandEnabled:Z

    .line 2907
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const/16 v10, 0x50

    invoke-virtual {v9, v10}, Lcom/sec/android/widgetapp/calculator/Panel;->setGravity(I)V

    goto/16 :goto_2

    .line 2940
    :cond_10
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    if-eqz v9, :cond_11

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    if-eqz v9, :cond_11

    .line 2941
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2942
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2944
    :cond_11
    sget-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v9, :cond_12

    .line 2945
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2946
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2948
    :cond_12
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0004

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_13

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0006

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_a

    .line 2950
    :cond_13
    const v9, 0x7f0e004a

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_a

    const v9, 0x7f0e004c

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_a

    .line 2951
    const v9, 0x7f0e004a

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2952
    const v9, 0x7f0e004c

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 2969
    .restart local v0    # "i":I
    .restart local v8    # "vw":Landroid/view/View;
    :cond_14
    invoke-virtual {v8, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2970
    invoke-virtual {v8, p0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 2971
    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2972
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 2974
    const v9, 0x7f0e001d

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-ne v8, v9, :cond_15

    .line 2975
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2978
    :cond_15
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.sec.feature.hovering_ui"

    invoke-virtual {v9, v10}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_16

    .line 2980
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setHoverPopupType(I)V

    .line 2982
    :cond_16
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_17

    .line 2983
    new-instance v6, Landroid/graphics/drawable/RippleDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x7f080000

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-static {v9}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v6, v9, v10, v11}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2986
    .local v6, "rippleDrawable":Landroid/graphics/drawable/RippleDrawable;
    const/16 v9, 0x46

    const/16 v10, -0xf

    const/16 v11, 0xc3

    const/16 v12, 0x9b

    invoke-virtual {v6, v9, v10, v11, v12}, Landroid/graphics/drawable/RippleDrawable;->setHotspotBounds(IIII)V

    .line 2987
    invoke-virtual {v8, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2989
    .end local v6    # "rippleDrawable":Landroid/graphics/drawable/RippleDrawable;
    :cond_17
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_18

    .line 2990
    new-instance v6, Landroid/graphics/drawable/RippleDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x7f080000

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-static {v9}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v6, v9, v10, v11}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2993
    .restart local v6    # "rippleDrawable":Landroid/graphics/drawable/RippleDrawable;
    invoke-virtual {v8, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2963
    .end local v6    # "rippleDrawable":Landroid/graphics/drawable/RippleDrawable;
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_4

    .line 2999
    .end local v8    # "vw":Landroid/view/View;
    :cond_19
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    goto/16 :goto_5
.end method

.method private registerFlatMotionListener()V
    .locals 3

    .prologue
    const/16 v2, 0x14

    .line 3596
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3597
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v0, :cond_0

    .line 3598
    const-string v0, "scontext"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 3600
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    invoke-virtual {v0, v2}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3601
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v1, "registerFlatMotionListener(). available the flat motion service"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3605
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 3606
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v1, "registerFlatMotionListener"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3608
    :cond_1
    return-void

    .line 3603
    :cond_2
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v1, "registerFlatMotionListener(). UN available the flat motion service"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerOneHandStatusObserver()V
    .locals 4

    .prologue
    .line 3484
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "onehand_switch_state"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHandGripChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3486
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v1, "registerOneHandStatusObserver"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 3487
    return-void
.end method

.method private registerOnehandAnyScreenObserver()V
    .locals 4

    .prologue
    .line 3558
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "any_screen_running"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAnyScreenObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3561
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v1, "registerOnehandAnyScreenObserver"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 3562
    return-void
.end method

.method private registerPanelButtonListener()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 939
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 940
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 942
    .local v1, "vw":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 943
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 944
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 945
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 946
    invoke-virtual {v1, v4}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 947
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 949
    invoke-virtual {v1, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 952
    :cond_0
    const v2, 0x7f0e001d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 953
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 939
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 957
    .end local v1    # "vw":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private restoreIntentContainData()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1071
    iget-boolean v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isIntentContainData:Z

    if-eqz v5, :cond_0

    .line 1072
    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isIntentContainData:Z

    .line 1074
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 1075
    .local v3, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_0

    .line 1076
    const-string v5, "DISPLAY_TEXT"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1077
    iput-boolean v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clear_scr:Z

    .line 1078
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-boolean v8, v5, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 1079
    const-string v5, "DISPLAY_TEXT"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "\n"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1081
    .local v0, "displayText":Ljava/lang/String;
    const-string v5, "="

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 1082
    .local v1, "equal":I
    const-string v5, "="

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 1083
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1084
    .local v4, "result":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-ne v5, v8, :cond_1

    .line 1085
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v6, "DISPLAY_TEXT"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 1086
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v7, "DISPLAY_TEXT"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1088
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 1111
    .end local v0    # "displayText":Ljava/lang/String;
    .end local v1    # "equal":I
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1091
    .restart local v0    # "displayText":Ljava/lang/String;
    .restart local v1    # "equal":I
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v4    # "result":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1092
    .local v2, "fma":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v5, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 1093
    invoke-virtual {v4, v9}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isChar(C)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4, v9}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isLargeChar(C)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1095
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 1097
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1098
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 1104
    .end local v2    # "fma":Ljava/lang/String;
    .end local v4    # "result":Ljava/lang/String;
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-boolean v9, v5, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto :goto_0

    .line 1100
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 1101
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1102
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto :goto_1
.end method

.method private saveCurrentText()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2324
    const-string v2, "backup_dsp"

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2326
    .local v0, "sp":Landroid/content/SharedPreferences;
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2327
    .local v1, "strTxt":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBackKey:Z

    if-eqz v2, :cond_0

    .line 2328
    const-string v1, ""

    .line 2329
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBackKey:Z

    .line 2331
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "full_display"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2332
    return-void
.end method

.method private saveCurrentText(Z)V
    .locals 4
    .param p1, "init"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2335
    const-string v2, "backup_dsp"

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2337
    .local v0, "sp":Landroid/content/SharedPreferences;
    const-string v1, ""

    .line 2338
    .local v1, "strTxt":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 2339
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    if-eqz v2, :cond_1

    .line 2340
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2341
    iget-boolean v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBackKey:Z

    if-eqz v2, :cond_0

    .line 2342
    const-string v1, ""

    .line 2343
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBackKey:Z

    .line 2345
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getSelectionStart()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->cursor_position:I

    .line 2348
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "full_display"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2349
    return-void
.end method

.method private savePanelState()V
    .locals 7

    .prologue
    .line 2273
    const-string v4, "backup_dsp"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 2275
    .local v3, "sp":Landroid/content/SharedPreferences;
    const/4 v2, 0x1

    .line 2276
    .local v2, "panelState":Z
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 2277
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->getNew800mdpi_PortraitPanelState()Z

    move-result v2

    .line 2282
    :cond_0
    :goto_0
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "full_panel"

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2284
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "euro_mode"

    sget-boolean v6, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2286
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget v0, v4, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    .line 2287
    .local v0, "error_check":I
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "error_check"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2289
    iget-boolean v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOptionMenuShow:Z

    .line 2290
    .local v1, "optionMenuShow":Z
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "optionMenuShow"

    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2291
    return-void

    .line 2279
    .end local v0    # "error_check":I
    .end local v1    # "optionMenuShow":Z
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-eqz v4, :cond_0

    .line 2280
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v2

    goto :goto_0
.end method

.method private setInitValues()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 473
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 474
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 475
    const-string v2, "clipboardEx"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/sec/clipboard/ClipboardExManager;

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    .line 477
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    if-nez v2, :cond_0

    .line 478
    new-instance v2, Landroid/media/SoundPool;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v5, v6}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    .line 479
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f060001

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentSoundID:I

    .line 481
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x7f060000

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    sput v2, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentBackkeySoundID:I

    .line 485
    :cond_0
    sget-object v1, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    .line 486
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;

    invoke-direct {v2, p0}, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;)V

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mCustomObserver:Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;

    .line 487
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mCustomObserver:Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->onChange(Z)V

    .line 488
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mCustomObserver:Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;

    invoke-virtual {v2, v1, v5, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 490
    new-instance v2, Lcom/sec/android/app/popupcalculator/Persist;

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/popupcalculator/Persist;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPersist:Lcom/sec/android/app/popupcalculator/Persist;

    .line 491
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBundle:Landroid/os/Bundle;

    .line 492
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPersist:Lcom/sec/android/app/popupcalculator/Persist;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/Persist;->getHistory()Lcom/sec/android/app/popupcalculator/History;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    .line 493
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mAudioManager:Landroid/media/AudioManager;

    .line 494
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->keyboard:I

    if-eq v2, v7, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->keyboard:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 496
    :cond_1
    iput-boolean v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHasKeyboard:Z

    .line 500
    :goto_0
    return-void

    .line 498
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHasKeyboard:Z

    goto :goto_0
.end method

.method private setInitValuesForMultiWindow()V
    .locals 3

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 504
    .local v0, "res":Landroid/content/res/Resources;
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 505
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v1, :cond_0

    .line 506
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v2, "-- mMultiWindowActivity.setStateChangeListener(mActivityStateChangeListener); -- "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mActivityStateChangeListener:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 515
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsWin:Z

    .line 516
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsFull:Z

    .line 543
    return-void
.end method

.method private setOneHandAdaptableOperation(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 3499
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOneHandAdaptableOperation enable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " oldState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOneHandAdaptableOperationState:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3501
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOneHandAdaptableOperationState:Z

    if-eq p1, v0, :cond_0

    .line 3502
    iput-boolean p1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOneHandAdaptableOperationState:Z

    .line 3504
    if-eqz p1, :cond_1

    .line 3505
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->registerOneHandStatusObserver()V

    .line 3510
    :cond_0
    :goto_0
    return-void

    .line 3507
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->unregisterOneHandStatusObserver()V

    goto :goto_0
.end method

.method private setOneHandMode(I)V
    .locals 2
    .param p1, "flag"    # I

    .prologue
    .line 3024
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "onehand_calculator_enabled"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 3025
    return-void
.end method

.method private setSrc(ZZ)V
    .locals 8
    .param p1, "small"    # Z
    .param p2, "portrait"    # Z

    .prologue
    const v7, 0x7f0e001e

    const v6, 0x7f0e001d

    const v5, 0x7f0e001c

    const v4, 0x7f0e001b

    const v3, 0x7f0e001a

    .line 3161
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isLarge(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 3400
    :goto_0
    return-void

    .line 3164
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const v2, 0x7f0e0009

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 3165
    .local v0, "v":Landroid/view/View;
    if-eqz p1, :cond_2

    .line 3166
    if-eqz p2, :cond_1

    .line 3167
    const v1, 0x7f0e002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02056c

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3169
    const v1, 0x7f0e0026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020570

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3171
    const v1, 0x7f0e0027

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020574

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3173
    const v1, 0x7f0e0028

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020578

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3175
    const v1, 0x7f0e0022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02057c

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3177
    const v1, 0x7f0e0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020580

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3179
    const v1, 0x7f0e0024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020584

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3181
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020588

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3183
    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02058c

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3185
    const v1, 0x7f0e0020

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020590

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3188
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02055f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3190
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02052c

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3192
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02053f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3194
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020522

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3196
    const v1, 0x7f0e0021

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020552

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3198
    const v1, 0x7f0e0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020547

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3200
    const v1, 0x7f0e0029

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020526

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3202
    const v1, 0x7f0e002d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020563

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3204
    const v1, 0x7f0e002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02054a

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3206
    const v1, 0x7f0e002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020530

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_0

    .line 3209
    :cond_1
    const v1, 0x7f0e002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02056b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3211
    const v1, 0x7f0e0026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02056f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3213
    const v1, 0x7f0e0027

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020573

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3215
    const v1, 0x7f0e0028

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020577

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3217
    const v1, 0x7f0e0022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02057b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3219
    const v1, 0x7f0e0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02057f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3221
    const v1, 0x7f0e0024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020583

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3223
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020587

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3225
    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02058b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3227
    const v1, 0x7f0e0020

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02058f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3230
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02055e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3232
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02052b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3234
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02053e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3236
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020521

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3238
    const v1, 0x7f0e0021

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020551

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3240
    const v1, 0x7f0e0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020546

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3242
    const v1, 0x7f0e0029

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020525

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3244
    const v1, 0x7f0e002d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020562

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3246
    const v1, 0x7f0e002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020549

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3248
    const v1, 0x7f0e002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02052f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3251
    const v1, 0x7f0e0036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020556

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3253
    const v1, 0x7f0e003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02051e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3255
    const v1, 0x7f0e0032

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020528

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3257
    const v1, 0x7f0e003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020532

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3259
    const v1, 0x7f0e0037

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020534

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3261
    const v1, 0x7f0e002e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020537

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3263
    const v1, 0x7f0e0034

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020539

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3265
    const v1, 0x7f0e0035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02053b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3267
    const v1, 0x7f0e0030

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020541

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3269
    const v1, 0x7f0e003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020543

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3271
    const v1, 0x7f0e002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02054c

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3273
    const v1, 0x7f0e0031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02054e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3275
    const v1, 0x7f0e0033

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020554

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3277
    const v1, 0x7f0e0038

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020558

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3279
    const v1, 0x7f0e0039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02055a

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_0

    .line 3283
    :cond_2
    if-eqz p2, :cond_3

    .line 3284
    const v1, 0x7f0e002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020593

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3286
    const v1, 0x7f0e0026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020595

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3288
    const v1, 0x7f0e0027

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020597

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3290
    const v1, 0x7f0e0028

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020599

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3292
    const v1, 0x7f0e0022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02059b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3294
    const v1, 0x7f0e0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02059d

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3296
    const v1, 0x7f0e0024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02059f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3298
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f0205a1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3300
    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f0205a3

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3302
    const v1, 0x7f0e0020

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f0205a5

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3305
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020439

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3307
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020412

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3309
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020420

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3311
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02040b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3313
    const v1, 0x7f0e0021

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02042e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3315
    const v1, 0x7f0e0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020426

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3317
    const v1, 0x7f0e0029

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02040d

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3319
    const v1, 0x7f0e002d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02043b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3321
    const v1, 0x7f0e002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020428

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3323
    const v1, 0x7f0e002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020414

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_0

    .line 3326
    :cond_3
    const v1, 0x7f0e002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020593

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3328
    const v1, 0x7f0e0026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020595

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3330
    const v1, 0x7f0e0027

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020597

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3332
    const v1, 0x7f0e0028

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020599

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3334
    const v1, 0x7f0e0022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02059b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3336
    const v1, 0x7f0e0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02059d

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3338
    const v1, 0x7f0e0024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02059f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3340
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f0205a1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3342
    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f0205a3

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3344
    const v1, 0x7f0e0020

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f0205a5

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3347
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020439

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3349
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020412

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3351
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020420

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3353
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02040b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3355
    const v1, 0x7f0e0021

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02042e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3357
    const v1, 0x7f0e0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020426

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3359
    const v1, 0x7f0e0029

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02040d

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3361
    const v1, 0x7f0e002d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02043b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3363
    const v1, 0x7f0e002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020428

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3365
    const v1, 0x7f0e002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020414

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3368
    const v1, 0x7f0e0036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020432

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3370
    const v1, 0x7f0e003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020409

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3372
    const v1, 0x7f0e0032

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020410

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3374
    const v1, 0x7f0e003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020418

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3376
    const v1, 0x7f0e0037

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3378
    const v1, 0x7f0e002e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02041a

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3380
    const v1, 0x7f0e0034

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02041c

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3382
    const v1, 0x7f0e0035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02041e

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3384
    const v1, 0x7f0e0030

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020422

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3386
    const v1, 0x7f0e003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020424

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3388
    const v1, 0x7f0e002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02042a

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3390
    const v1, 0x7f0e0031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f02042c

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3392
    const v1, 0x7f0e0033

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020430

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3394
    const v1, 0x7f0e0038

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020434

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 3396
    const v1, 0x7f0e0039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const v2, 0x7f020436

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_0
.end method

.method private setTextSizeValues()V
    .locals 2

    .prologue
    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 453
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0a0034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_MEDIUM_PORT:I

    .line 454
    const v1, 0x7f0a0030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_LARGE_PORT:I

    .line 457
    const v1, 0x7f0a0031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_MEDIUM_LAND:I

    .line 458
    const v1, 0x7f0a002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_LARGE_LAND:I

    .line 459
    const v1, 0x7f0a0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->HISTORY_TEXT_SIZE_LAND:I

    .line 469
    return-void
.end method

.method private switchMode()V
    .locals 5

    .prologue
    .line 2718
    const/4 v2, 0x0

    .line 2721
    .local v2, "txt":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2722
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2723
    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    invoke-static {v2, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 2725
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2729
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2730
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2731
    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    invoke-static {v2, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 2733
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2737
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget v3, v3, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    if-ge v1, v3, :cond_2

    .line 2738
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/popupcalculator/History;->current(I)Lcom/sec/android/app/popupcalculator/HistoryEntry;

    move-result-object v0

    .line 2740
    .local v0, "entry":Lcom/sec/android/app/popupcalculator/HistoryEntry;
    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getEdited()Ljava/lang/String;

    move-result-object v2

    .line 2741
    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    invoke-static {v2, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->setEdited(Ljava/lang/String;)V

    .line 2743
    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getBase()Ljava/lang/String;

    move-result-object v2

    .line 2744
    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    invoke-static {v2, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->changeSymbols(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->setBase(Ljava/lang/String;)V

    .line 2737
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2747
    .end local v0    # "entry":Lcom/sec/android/app/popupcalculator/HistoryEntry;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPersist:Lcom/sec/android/app/popupcalculator/Persist;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/Persist;->save()V

    .line 2748
    return-void
.end method

.method public static thousandSepChar()C
    .locals 1

    .prologue
    .line 1165
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x2e

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2c

    goto :goto_0
.end method

.method private unregisterOneHandStatusObserver()V
    .locals 4

    .prologue
    .line 3491
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHandGripChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3495
    :goto_0
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v2, "unregisterOneHandStatusObserver"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 3496
    return-void

    .line 3492
    :catch_0
    move-exception v0

    .line 3493
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Observer not registered : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private unregisterOnehandAnyScreenObserver()V
    .locals 3

    .prologue
    .line 3566
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAnyScreenObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3570
    :goto_0
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v2, "unregisterOnehandAnyScreenObserver"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 3571
    return-void

    .line 3567
    :catch_0
    move-exception v0

    .line 3568
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v2, "unregisterOnehandAnyScreenObserver   e"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private unregisterbuttonListener()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1855
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1856
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1857
    .local v1, "vw":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 1858
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1859
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1860
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1862
    const v2, 0x7f0e001d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 1863
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1855
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1867
    .end local v1    # "vw":Landroid/view/View;
    :cond_1
    return-void
.end method


# virtual methods
.method public checkHapticFeedbackEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1194
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "haptic_feedback_enabled"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1197
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public checkOrientation()V
    .locals 13

    .prologue
    .line 720
    const-string v10, "backup_dsp"

    const/4 v11, 0x0

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 722
    .local v8, "sp":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "accelerometer_rotation"

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    .line 724
    .local v7, "setting":I
    const-string v10, "window"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 725
    .local v5, "lWindowManager":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getRotation()I

    move-result v6

    .line 729
    .local v6, "rotation":I
    const/4 v10, 0x1

    if-ne v7, v10, :cond_0

    .line 730
    const/4 v10, -0x1

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    .line 772
    :goto_0
    sput v7, Lcom/sec/android/app/popupcalculator/Calculator;->mAutoRotateSetting:I

    .line 773
    return-void

    .line 732
    :cond_0
    sget v10, Lcom/sec/android/app/popupcalculator/Calculator;->mAutoRotateSetting:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 734
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 735
    .local v0, "cfg":Landroid/content/res/Configuration;
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 736
    .local v3, "dm":Landroid/util/DisplayMetrics;
    const/4 v9, 0x0

    .line 737
    .local v9, "width":I
    const/4 v4, 0x0

    .line 738
    .local v4, "height":I
    if-eqz v6, :cond_1

    const/4 v10, 0x2

    if-ne v6, v10, :cond_2

    .line 739
    :cond_1
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getWidth()I

    move-result v9

    .line 740
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getHeight()I

    move-result v4

    .line 745
    :goto_1
    if-le v9, v4, :cond_3

    .line 746
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    .line 747
    const/4 v10, 0x2

    iput v10, v0, Landroid/content/res/Configuration;->orientation:I

    .line 752
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v0, v3}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    goto :goto_0

    .line 742
    :cond_2
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getHeight()I

    move-result v9

    .line 743
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getWidth()I

    move-result v4

    goto :goto_1

    .line 749
    :cond_3
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    .line 750
    const/4 v10, 0x1

    iput v10, v0, Landroid/content/res/Configuration;->orientation:I

    goto :goto_2

    .line 754
    .end local v0    # "cfg":Landroid/content/res/Configuration;
    .end local v3    # "dm":Landroid/util/DisplayMetrics;
    .end local v4    # "height":I
    .end local v9    # "width":I
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v1, v10, Landroid/content/res/Configuration;->orientation:I

    .line 755
    .local v1, "config":I
    const-string v10, "orientationByOptions"

    const/4 v11, 0x0

    invoke-interface {v8, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 756
    .local v2, "configByOptions":I
    if-eqz v2, :cond_5

    .line 757
    move v1, v2

    .line 758
    :cond_5
    packed-switch v1, :pswitch_data_0

    .line 766
    const/4 v10, -0x1

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    goto :goto_0

    .line 760
    :pswitch_0
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    goto :goto_0

    .line 763
    :pswitch_1
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 758
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public filter(Landroid/view/KeyEvent;)V
    .locals 11
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v10, 0x7f0e001f

    const v9, 0x7f0e001e

    const/16 v8, 0xf7

    const/16 v7, 0x2d

    const/16 v6, 0x25

    .line 1934
    const/4 v3, 0x0

    .line 1935
    .local v3, "str":Ljava/lang/String;
    const-string v4, ":\"_0123456789.+-*/\u2212\u00d7\u00f7()!%^=&@#$"

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 1939
    .local v0, "ACCEPTED_CHARS_JPN":[C
    const-string v4, "JP"

    const-string v5, "ro.csc.country_code"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1940
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v4

    invoke-virtual {p1, v0, v4}, Landroid/view/KeyEvent;->getMatch([CI)C

    move-result v1

    .line 1943
    .local v1, "c":C
    :goto_0
    const-string v4, "JP"

    const-string v5, "ro.csc.country_code"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1945
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_2

    const/16 v4, 0x2b

    if-eq v1, v4, :cond_2

    const/16 v4, 0x28

    if-eq v1, v4, :cond_2

    const/16 v4, 0x29

    if-eq v1, v4, :cond_2

    const/16 v4, 0xa

    if-eq v1, v4, :cond_2

    const/16 v4, 0x78

    if-eq v1, v4, :cond_2

    if-eq v1, v8, :cond_2

    const/16 v4, 0x21

    if-eq v1, v4, :cond_2

    if-eq v1, v7, :cond_2

    const/16 v4, 0x2e

    if-eq v1, v4, :cond_2

    const/16 v4, 0x3d

    if-eq v1, v4, :cond_2

    if-eq v1, v6, :cond_2

    const/16 v4, 0x2a

    if-eq v1, v4, :cond_2

    const/16 v4, 0x2f

    if-eq v1, v4, :cond_2

    const/16 v4, 0x5e

    if-eq v1, v4, :cond_2

    const/16 v4, 0x3a

    if-eq v1, v4, :cond_2

    const/16 v4, 0x22

    if-eq v1, v4, :cond_2

    const/16 v4, 0x5f

    if-eq v1, v4, :cond_2

    .line 2270
    :cond_0
    :goto_1
    return-void

    .line 1941
    .end local v1    # "c":C
    :cond_1
    sget-object v4, Lcom/sec/android/app/popupcalculator/Calculator;->ACCEPTED_CHARS:[C

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/view/KeyEvent;->getMatch([CI)C

    move-result v1

    .restart local v1    # "c":C
    goto :goto_0

    .line 1950
    :cond_2
    const/16 v4, 0x22

    if-ne v1, v4, :cond_3

    .line 1951
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1952
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x78

    if-eq v4, v5, :cond_0

    .line 1955
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e001c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 1956
    :cond_3
    if-eq v1, v8, :cond_4

    const/16 v4, 0x2f

    if-ne v1, v4, :cond_5

    .line 1957
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1958
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v8, :cond_0

    .line 1961
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e001b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 1962
    :cond_5
    const/16 v4, 0x3a

    if-ne v1, v4, :cond_6

    .line 1963
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1964
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2b

    if-eq v4, v5, :cond_0

    .line 1967
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0025

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1968
    :cond_6
    const/16 v4, 0x3d

    if-ne v1, v4, :cond_7

    .line 1969
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1970
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5e

    if-eq v4, v5, :cond_0

    .line 1973
    const v4, 0x7f0e0039

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1974
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0039

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1976
    :cond_7
    if-ne v1, v6, :cond_8

    .line 1977
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1978
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v6, :cond_0

    .line 1981
    const v4, 0x7f0e0030

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1982
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0030

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1984
    :cond_8
    if-ne v1, v7, :cond_9

    .line 1985
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1986
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v7, :cond_0

    .line 1989
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0021

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1990
    :cond_9
    const/16 v4, 0x21

    if-ne v1, v4, :cond_a

    .line 1991
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1992
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 1995
    const v4, 0x7f0e002e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1996
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002e

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1998
    :cond_a
    const/16 v4, 0x2a

    if-eq v1, v4, :cond_b

    const/16 v4, 0x28

    if-ne v1, v4, :cond_c

    .line 1999
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0029

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2001
    :cond_c
    const/16 v4, 0x2c

    if-ne v1, v4, :cond_d

    .line 2002
    sget-boolean v4, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v4, :cond_0

    .line 2003
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2004
    :cond_d
    const/16 v4, 0x2e

    if-ne v1, v4, :cond_e

    .line 2005
    sget-boolean v4, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-nez v4, :cond_0

    .line 2007
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2008
    :cond_e
    const/16 v4, 0x5f

    if-ne v1, v4, :cond_f

    .line 2009
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2010
    :cond_f
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 2011
    add-int/lit8 v2, v1, -0x30

    .line 2012
    .local v2, "cNum":I
    packed-switch v2, :pswitch_data_0

    goto/16 :goto_1

    .line 2014
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2017
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0026

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2020
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0027

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2023
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0028

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2026
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0022

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2029
    :pswitch_5
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0023

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2032
    :pswitch_6
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2035
    :pswitch_7
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2038
    :pswitch_8
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2041
    :pswitch_9
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0020

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2045
    .end local v2    # "cNum":I
    :cond_10
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    goto/16 :goto_1

    .line 2047
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2050
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0026

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2053
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0027

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2056
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0028

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2059
    :sswitch_4
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0022

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2062
    :sswitch_5
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0023

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2065
    :sswitch_6
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2068
    :sswitch_7
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2071
    :sswitch_8
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2074
    :sswitch_9
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0020

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2078
    :sswitch_a
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2081
    :sswitch_b
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0026

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2084
    :sswitch_c
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0027

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2087
    :sswitch_d
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0028

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2090
    :sswitch_e
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0022

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2093
    :sswitch_f
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0023

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2096
    :sswitch_10
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2099
    :sswitch_11
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2102
    :sswitch_12
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2105
    :sswitch_13
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0020

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2111
    :cond_11
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_12

    const/16 v4, 0x2b

    if-eq v1, v4, :cond_12

    const/16 v4, 0x28

    if-eq v1, v4, :cond_12

    const/16 v4, 0x29

    if-eq v1, v4, :cond_12

    const/16 v4, 0xa

    if-eq v1, v4, :cond_12

    const/16 v4, 0x78

    if-eq v1, v4, :cond_12

    if-eq v1, v8, :cond_12

    const/16 v4, 0x21

    if-eq v1, v4, :cond_12

    if-eq v1, v7, :cond_12

    const/16 v4, 0x2e

    if-eq v1, v4, :cond_12

    const/16 v4, 0x3d

    if-eq v1, v4, :cond_12

    if-eq v1, v6, :cond_12

    const/16 v4, 0x2a

    if-eq v1, v4, :cond_12

    const/16 v4, 0x2f

    if-eq v1, v4, :cond_12

    const/16 v4, 0x5e

    if-ne v1, v4, :cond_0

    .line 2115
    :cond_12
    const/16 v4, 0x78

    if-eq v1, v4, :cond_13

    const/16 v4, 0x2a

    if-ne v1, v4, :cond_14

    .line 2116
    :cond_13
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2117
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x78

    if-eq v4, v5, :cond_0

    .line 2120
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e001c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2121
    :cond_14
    if-eq v1, v8, :cond_15

    const/16 v4, 0x2f

    if-ne v1, v4, :cond_16

    .line 2122
    :cond_15
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2123
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v8, :cond_0

    .line 2126
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e001b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2127
    :cond_16
    const/16 v4, 0x2b

    if-ne v1, v4, :cond_17

    .line 2128
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2129
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2b

    if-eq v4, v5, :cond_0

    .line 2132
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0025

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2133
    :cond_17
    const/16 v4, 0x5e

    if-ne v1, v4, :cond_18

    .line 2134
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2135
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5e

    if-eq v4, v5, :cond_0

    .line 2138
    const v4, 0x7f0e0039

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2139
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0039

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2141
    :cond_18
    if-ne v1, v6, :cond_19

    .line 2142
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2143
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v6, :cond_0

    .line 2146
    const v4, 0x7f0e0030

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2147
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0030

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2149
    :cond_19
    if-ne v1, v7, :cond_1a

    .line 2150
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2151
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v7, :cond_0

    .line 2154
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0021

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2155
    :cond_1a
    const/16 v4, 0x21

    if-ne v1, v4, :cond_1b

    .line 2156
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2157
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 2160
    const v4, 0x7f0e002e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2161
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002e

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2163
    :cond_1b
    const/16 v4, 0x29

    if-eq v1, v4, :cond_1c

    const/16 v4, 0x28

    if-ne v1, v4, :cond_1d

    .line 2164
    :cond_1c
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0029

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2165
    :cond_1d
    const/16 v4, 0x2e

    if-ne v1, v4, :cond_1e

    .line 2166
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2167
    :cond_1e
    const/16 v4, 0x3d

    if-ne v1, v4, :cond_1f

    .line 2168
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2169
    :cond_1f
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 2170
    add-int/lit8 v2, v1, -0x30

    .line 2171
    .restart local v2    # "cNum":I
    packed-switch v2, :pswitch_data_1

    goto/16 :goto_1

    .line 2173
    :pswitch_a
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2176
    :pswitch_b
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0026

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2179
    :pswitch_c
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0027

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2182
    :pswitch_d
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0028

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2185
    :pswitch_e
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0022

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2188
    :pswitch_f
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0023

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2191
    :pswitch_10
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2194
    :pswitch_11
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2197
    :pswitch_12
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2200
    :pswitch_13
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0020

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2204
    .end local v2    # "cNum":I
    :cond_20
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_1

    goto/16 :goto_1

    .line 2206
    :sswitch_14
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2209
    :sswitch_15
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0026

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2212
    :sswitch_16
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0027

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2215
    :sswitch_17
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0028

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2218
    :sswitch_18
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0022

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2221
    :sswitch_19
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0023

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2224
    :sswitch_1a
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2227
    :sswitch_1b
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2230
    :sswitch_1c
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2233
    :sswitch_1d
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0020

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2237
    :sswitch_1e
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2240
    :sswitch_1f
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0026

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2243
    :sswitch_20
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0027

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2246
    :sswitch_21
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0028

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2249
    :sswitch_22
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0022

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2252
    :sswitch_23
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0023

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2255
    :sswitch_24
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0024

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2258
    :sswitch_25
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2261
    :sswitch_26
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {p0, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2264
    :sswitch_27
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v5, 0x7f0e0020

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2012
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 2045
    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0x9 -> :sswitch_2
        0xa -> :sswitch_3
        0xb -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_6
        0xe -> :sswitch_7
        0xf -> :sswitch_8
        0x10 -> :sswitch_9
        0x90 -> :sswitch_a
        0x91 -> :sswitch_b
        0x92 -> :sswitch_c
        0x93 -> :sswitch_d
        0x94 -> :sswitch_e
        0x95 -> :sswitch_f
        0x96 -> :sswitch_10
        0x97 -> :sswitch_11
        0x98 -> :sswitch_12
        0x99 -> :sswitch_13
    .end sparse-switch

    .line 2171
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 2204
    :sswitch_data_1
    .sparse-switch
        0x7 -> :sswitch_14
        0x8 -> :sswitch_15
        0x9 -> :sswitch_16
        0xa -> :sswitch_17
        0xb -> :sswitch_18
        0xc -> :sswitch_19
        0xd -> :sswitch_1a
        0xe -> :sswitch_1b
        0xf -> :sswitch_1c
        0x10 -> :sswitch_1d
        0x90 -> :sswitch_1e
        0x91 -> :sswitch_1f
        0x92 -> :sswitch_20
        0x93 -> :sswitch_21
        0x94 -> :sswitch_22
        0x95 -> :sswitch_23
        0x96 -> :sswitch_24
        0x97 -> :sswitch_25
        0x98 -> :sswitch_26
        0x99 -> :sswitch_27
    .end sparse-switch
.end method

.method public getCurrentEditText()Landroid/widget/EditText;
    .locals 2

    .prologue
    .line 2855
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 2856
    .local v0, "textView":Landroid/widget/EditText;
    :goto_0
    return-object v0

    .line 2855
    .end local v0    # "textView":Landroid/widget/EditText;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    goto :goto_0
.end method

.method public getOneHandModeValue()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3453
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isOnehandAnyScreenMode()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->mIsKProject:Ljava/lang/String;

    const-string v2, "k3g"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->mIsKProject:Ljava/lang/String;

    const-string v2, "klte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->mIsKProject:Ljava/lang/String;

    const-string v2, "slte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->mIsKProject:Ljava/lang/String;

    const-string v2, "kccat6"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3459
    :cond_0
    :goto_0
    return v0

    .line 3457
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isOnehandMode()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    iput v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOneHandSettingValue:I

    .line 3459
    iget v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOneHandSettingValue:I

    goto :goto_0
.end method

.method public getOneHandSwitchStateValue()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3472
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current OneHand Direction ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "onehand_direction"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3474
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "onehand_direction"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSystemOneHandAdaptableOperationSettingValue()I
    .locals 3

    .prologue
    .line 3480
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "hand_adaptable_operation"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSystemRapidKeyInputSettingValue()I
    .locals 3

    .prologue
    .line 3464
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "rapid_key_input"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public isPenMultiWindow()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3708
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3709
    sget-object v2, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v3, "-- isPenMultiWindow true -- "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3710
    const/4 v1, 0x1

    .line 3717
    :goto_0
    return v1

    .line 3712
    :cond_0
    sget-object v2, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v3, "-- isPenMultiWindow false -- "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3715
    :catch_0
    move-exception v0

    .line 3717
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1872
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1873
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 1874
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/16 v1, 0xf

    const/16 v2, 0x27

    const/16 v3, 0xe

    const/4 v4, 0x5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setPadding(IIII)V

    .line 1876
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mReview:Landroid/view/View;

    .line 1877
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    .line 1878
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1879
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->requestFocus()Z

    .line 1881
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->isTouchsounded:Z

    if-eqz v0, :cond_2

    .line 1882
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->isTouchsounded:Z

    .line 1883
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1884
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->invalidateOptionsMenu()V

    .line 1885
    :cond_3
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v9, 0x9

    const/4 v8, 0x0

    .line 389
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 390
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v1, "-- onConfigurationChanged -- "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 396
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnConfigurationEnable:Z

    .line 403
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->onSaveRotationData()V

    .line 404
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->unregisterbuttonListener()V

    .line 405
    const v0, 0x7f040005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->setContentView(I)V

    .line 406
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    .line 408
    const/4 v7, 0x0

    .line 410
    .local v7, "windowMode":I
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_1

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z

    .line 414
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initCalculatorEditText()V

    .line 415
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initHistoryEditText()V

    .line 416
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initPanel()V

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-nez v0, :cond_4

    .line 419
    new-instance v0, Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isWvga(Landroid/content/Context;)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/popupcalculator/EventHandler;-><init>(Landroid/content/Context;Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;ILcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 425
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isCocktailBarFeature(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 426
    const v0, 0x7f0e0018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/16 v1, 0x258

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 428
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initClearBtn()V

    .line 429
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initListeners()V

    .line 430
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadCurrentText()V

    .line 431
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 432
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadPanelState()V

    .line 433
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadCurrentText()V

    .line 435
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initActionBar()V

    .line 436
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->restoreIntentContainData()V

    .line 437
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initEuroModeDot()V

    .line 439
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V

    .line 440
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->loadPanelState()V

    .line 441
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initEuroModeDot()V

    .line 442
    sget v0, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentTextSize:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->setTextSize(I)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v9, v8}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setNewActionPopupMenu(IZ)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v0, v9, v8}, Landroid/widget/EditText;->setNewActionPopupMenu(IZ)V

    .line 447
    return-void

    .line 422
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isWvga(Landroid/content/Context;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setView(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;ILcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v7, 0x20000

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 548
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 550
    sget-object v3, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v6, "-- onCreate -- "

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    const-string v3, "VerificationLog"

    const-string v6, "-- onCreate -- "

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->context:Landroid/content/Context;

    .line 554
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsCreated:Z

    .line 555
    const-string v3, "backup_dsp"

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 558
    .local v2, "sp":Landroid/content/SharedPreferences;
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->setInitValuesForMultiWindow()V

    .line 559
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v3

    if-eq v3, v4, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v3

    if-eq v3, v4, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTabletUnder10inch(Landroid/content/Context;)I

    move-result v3

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    const-string v6, "I257"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    const-string v6, "P601"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    const-string v6, "P600"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 560
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 563
    :cond_1
    const-string v3, "HistoryListOpenState"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "HistoryListOpenState"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 564
    if-eqz p1, :cond_2

    .line 565
    const-string v3, "EXTRA_PANEL_STATE"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 567
    :cond_2
    if-nez p1, :cond_3

    .line 568
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v6, "dialogShow"

    invoke-interface {v3, v6, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 569
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v6, "orientationByOptions"

    invoke-interface {v3, v6, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 570
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v6, "orientationByUser"

    invoke-interface {v3, v6, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 576
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemOneHandAdaptableOperationSettingValue()I

    move-result v3

    if-ne v3, v4, :cond_d

    move v3, v4

    :goto_0
    sput-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    .line 577
    sget-object v3, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mOnehandAdaptableOperation : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v3, :cond_4

    .line 580
    invoke-direct {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->setOneHandAdaptableOperation(Z)V

    .line 581
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->registerFlatMotionListener()V

    .line 583
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->registerOnehandAnyScreenObserver()V

    .line 585
    const v3, 0x7f040005

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->setContentView(I)V

    .line 587
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandModeValue()I

    move-result v3

    if-ne v3, v4, :cond_5

    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v3, :cond_5

    .line 588
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandSwitchStateValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 601
    :cond_5
    :goto_1
    const-string v3, "euro_mode"

    sget-boolean v6, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    .line 602
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0a000e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v4, :cond_e

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "bg"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "cs"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "de"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "fr"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "hu"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "it"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "nb"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "sk"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v6, "pl"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 612
    :cond_6
    sput-boolean v4, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    .line 621
    :cond_7
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initControls()V

    .line 624
    if-eqz p1, :cond_10

    .line 625
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v6, "EXTRA_ENTER_END"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, v3, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 626
    const-string v3, "EXTRA_PANEL_STATE"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 627
    .local v1, "mState":Z
    const-string v3, "IS_ONE_HAND_RETAIN"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandRetain:Z

    .line 628
    if-eqz v1, :cond_f

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 629
    .local v0, "mEdit":Landroid/widget/EditText;
    :goto_3
    if-nez v1, :cond_a

    .line 630
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->getContent()Landroid/view/View;

    move-result-object v3

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 636
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->model:Ljava/lang/String;

    const-string v6, "G900"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isJapanModel()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0a0005

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v4, :cond_9

    .line 637
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->hide()V

    .line 638
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->getHandle()Landroid/view/View;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->getClosedHandle()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 640
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/EventListener;->onPanelClosedForConfig()V

    .line 647
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v6, "EXTRA_FURMULA"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 648
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v6, "EXTRA_RESULT"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 649
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v6, "EXTRA_LAST_RESULT"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setLastResult(Ljava/lang/String;)V

    .line 650
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v6, "EXTRA_FORMULA_LENGTH_BACKUP"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormulaLengthBackup(I)V

    .line 651
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v6, "EXTRA_FORMULA_BACKUP"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormulaBackup(Ljava/lang/String;)V

    .line 653
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-boolean v4, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 654
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v4, "EXTRA_STRING"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 655
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-boolean v5, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 662
    .end local v0    # "mEdit":Landroid/widget/EditText;
    .end local v1    # "mState":Z
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->setTextSizeValues()V

    .line 663
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxxhdp(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_b

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxhdp(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_c

    .line 665
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/sec/android/app/popupcalculator/Calculator$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/popupcalculator/Calculator$2;-><init>(Lcom/sec/android/app/popupcalculator/Calculator;)V

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "accelerometer_rotation"

    invoke-static {v4, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-nez v4, :cond_12

    const-wide/16 v4, 0x1f4

    :goto_5
    invoke-virtual {v3, v6, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 675
    :cond_c
    return-void

    :cond_d
    move v3, v5

    .line 576
    goto/16 :goto_0

    .line 590
    :pswitch_0
    iput-boolean v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    goto/16 :goto_1

    .line 593
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    goto/16 :goto_1

    .line 614
    :cond_e
    sget-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v3, :cond_7

    .line 615
    sput-boolean v5, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    .line 616
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v6, "euro_mode"

    sget-boolean v7, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_2

    .line 628
    .restart local v1    # "mState":Z
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    goto/16 :goto_3

    .line 657
    .end local v1    # "mState":Z
    :cond_10
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v6, "full_panel"

    invoke-interface {v3, v6, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 658
    iget-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clear_scr:Z

    if-nez v3, :cond_11

    .line 659
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 660
    :cond_11
    iput-boolean v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clear_scr:Z

    goto :goto_4

    .line 665
    :cond_12
    const-wide/16 v4, 0x0

    goto :goto_5

    .line 588
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0206a1

    const v5, 0x7f020698

    const/4 v4, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2457
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTabletUnder10inch(Landroid/content/Context;)I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 2458
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Feature;->CALCULATOR_SUPPORTS_OPENSOURCE_LICENSE:Z

    if-eqz v0, :cond_0

    .line 2459
    const v0, 0x7f0b0022

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2461
    const v0, 0x7f0b0061

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2464
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2491
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 2467
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0d0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 2470
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-nez v0, :cond_2

    .line 2471
    const v0, 0x7f0b0022

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2473
    :cond_2
    const/4 v0, 0x3

    const v1, 0x7f0b001d

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02069d

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2475
    const/4 v0, 0x2

    const v1, 0x7f0b001f

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02069e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2485
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Feature;->CALCULATOR_SUPPORTS_OPENSOURCE_LICENSE:Z

    if-eqz v0, :cond_3

    .line 2486
    const v0, 0x7f0b0061

    invoke-interface {p1, v2, v4, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2489
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 8

    .prologue
    const v7, 0x7f0e0011

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1689
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v2, "-- onDestroy -- "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1691
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v1, :cond_1

    .line 1692
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v1}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1693
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v1}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 1695
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    .line 1698
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBundle:Landroid/os/Bundle;

    if-eqz v1, :cond_2

    .line 1699
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBundle:Landroid/os/Bundle;

    .line 1702
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mReview:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 1703
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mReview:Landroid/view/View;

    .line 1706
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    if-eqz v1, :cond_4

    .line 1707
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    .line 1709
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mCustomObserver:Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1712
    sget-boolean v1, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v1, :cond_5

    .line 1713
    invoke-direct {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->setOneHandAdaptableOperation(Z)V

    .line 1714
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->UnregisterFlatMotionListener()V

    .line 1716
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->unregisterOnehandAnyScreenObserver()V

    .line 1728
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-eqz v1, :cond_6

    .line 1729
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->resetInstance()V

    .line 1730
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 1733
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    if-eqz v1, :cond_7

    .line 1734
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    sget v2, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentSoundID:I

    invoke-virtual {v1, v2}, Landroid/media/SoundPool;->unload(I)Z

    .line 1735
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    sget v2, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentBackkeySoundID:I

    invoke-virtual {v1, v2}, Landroid/media/SoundPool;->unload(I)Z

    .line 1736
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v1}, Landroid/media/SoundPool;->release()V

    .line 1737
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    .line 1740
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-eqz v1, :cond_9

    .line 1741
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v1, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->setOnPanelListener(Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;)V

    .line 1742
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->resetInstance()V

    .line 1743
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->removeAllViews()V

    .line 1744
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->removeAllViewsInLayout()V

    .line 1745
    invoke-virtual {p0, v7}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1746
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_8

    .line 1747
    const v1, 0x7f0e0008

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1748
    :cond_8
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 1751
    .end local v0    # "parent":Landroid/view/ViewGroup;
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    if-eqz v1, :cond_a

    .line 1752
    const v1, 0x7f0e0019

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1753
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1754
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setVisibility(I)V

    .line 1755
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->clearFocus()V

    .line 1756
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->clearSpans()V

    .line 1757
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->clear()V

    .line 1758
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1759
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1760
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1761
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setFocusable(Z)V

    .line 1762
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v2, v2, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->textWhatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1763
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->resetInstance()V

    .line 1764
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 1767
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    if-eqz v1, :cond_b

    .line 1768
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    .line 1771
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    if-eqz v1, :cond_e

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isPacific(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v5, :cond_e

    .line 1772
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    if-eqz v1, :cond_c

    .line 1773
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1774
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1777
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 1778
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewManager;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V

    .line 1779
    :cond_d
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    .line 1782
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v1, :cond_f

    .line 1783
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mAudioManager:Landroid/media/AudioManager;

    .line 1786
    :cond_f
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->unregisterbuttonListener()V

    .line 1787
    const v1, 0x7f0e0016

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1789
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->cTalkbackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_10

    .line 1790
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->cTalkbackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1793
    :cond_10
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mVolumeSeekbarFragmentTimer:Landroid/os/CountDownTimer;

    if-eqz v1, :cond_11

    .line 1794
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mVolumeSeekbarFragmentTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 1795
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mVolumeSeekbarFragmentTimer:Landroid/os/CountDownTimer;

    .line 1798
    :cond_11
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->context:Landroid/content/Context;

    if-eqz v1, :cond_12

    .line 1799
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->context:Landroid/content/Context;

    .line 1802
    :cond_12
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPersist:Lcom/sec/android/app/popupcalculator/Persist;

    if-eqz v1, :cond_13

    .line 1803
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPersist:Lcom/sec/android/app/popupcalculator/Persist;

    .line 1806
    :cond_13
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    if-eqz v1, :cond_14

    .line 1807
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    .line 1810
    :cond_14
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandModeValue()I

    move-result v1

    if-ne v1, v5, :cond_18

    .line 1811
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_15

    .line 1812
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1813
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandLeft:Landroid/widget/LinearLayout;

    .line 1816
    :cond_15
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_16

    .line 1817
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1818
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandRight:Landroid/widget/LinearLayout;

    .line 1821
    :cond_16
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    if-eqz v1, :cond_17

    .line 1822
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1823
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivLeft:Landroid/widget/ImageView;

    .line 1825
    :cond_17
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    if-eqz v1, :cond_18

    .line 1826
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandClickLitener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1827
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->ivRight:Landroid/widget/ImageView;

    .line 1830
    :cond_18
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    if-eqz v1, :cond_19

    .line 1831
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/EventListener;->resetInstance()V

    .line 1832
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    .line 1835
    :cond_19
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_1a

    .line 1836
    sput-object v3, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    .line 1839
    :cond_1a
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    if-eqz v1, :cond_1b

    .line 1840
    iput-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    .line 1842
    :cond_1b
    invoke-virtual {p0, v7}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1843
    .restart local v0    # "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_1c

    .line 1844
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1845
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 1846
    invoke-direct {p0, v0}, Lcom/sec/android/app/popupcalculator/Calculator;->nullViewDrawablesRecursive(Landroid/view/View;)V

    .line 1848
    :cond_1c
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1849
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1850
    return-void
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "mv"    # Landroid/view/MotionEvent;

    .prologue
    .line 1369
    sget v8, Lcom/sec/android/app/popupcalculator/Calculator;->mIsRapidKeyInputValue:I

    if-eqz v8, :cond_0

    const/4 v8, 0x0

    invoke-virtual {p2, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v8

    const/4 v9, 0x3

    if-eq v8, v9, :cond_0

    const/4 v8, 0x0

    invoke-virtual {p2, v8}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v8

    const/4 v9, 0x2

    if-eq v8, v9, :cond_0

    sget-boolean v8, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackSettingOn:Z

    if-nez v8, :cond_1

    .line 1371
    :cond_0
    const/4 v8, 0x0

    .line 1438
    :goto_0
    return v8

    .line 1372
    :cond_1
    const v8, 0x7f0e002d

    invoke-virtual {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v8

    if-eq v8, p1, :cond_2

    const v8, 0x7f0e002c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v8

    if-ne v8, p1, :cond_3

    .line 1374
    :cond_2
    sget v8, Lcom/sec/android/app/popupcalculator/Calculator;->mIsRapidKeyInputValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 1375
    const-string v8, "\u00a0"

    invoke-virtual {p1, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1383
    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 1385
    .local v2, "id":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v8

    const/16 v9, 0x9

    if-ne v8, v9, :cond_8

    .line 1386
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v8}, Lcom/sec/android/app/popupcalculator/EventHandler;->isHistoryScreen()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isBlack(Landroid/content/Context;)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    if-eqz v8, :cond_4

    .line 1387
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1388
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1389
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 1390
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mNoHistory:Landroid/widget/EditText;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 1392
    :cond_4
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Landroid/view/View;->setClickable(Z)V

    .line 1393
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Landroid/view/View;->setLongClickable(Z)V

    .line 1394
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    const v9, 0x7f0e002d

    if-eq v8, v9, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    const v9, 0x7f0e002c

    if-ne v8, v9, :cond_6

    .line 1395
    :cond_5
    const/16 v8, 0x40

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 1396
    :cond_6
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1397
    .local v1, "copyMsg":Landroid/os/Message;
    sget v8, Lcom/sec/android/app/popupcalculator/Calculator;->STARTTALKBACK_LONGPRESS:I

    iput v8, v1, Landroid/os/Message;->what:I

    .line 1398
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1399
    sget-boolean v8, Lcom/sec/android/app/popupcalculator/Calculator;->mLongPressed:Z

    if-nez v8, :cond_8

    .line 1400
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKlimt(Landroid/content/Context;)I

    move-result v8

    if-eqz v8, :cond_7

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isPacific(Landroid/content/Context;)I

    move-result v8

    if-nez v8, :cond_8

    .line 1401
    :cond_7
    sget-object v8, Lcom/sec/android/app/popupcalculator/Calculator;->cTalkbackHandler:Landroid/os/Handler;

    const-wide/16 v10, 0x3e8

    invoke-virtual {v8, v1, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1404
    .end local v1    # "copyMsg":Landroid/os/Message;
    :cond_8
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v8

    const/16 v9, 0xa

    if-ne v8, v9, :cond_10

    .line 1405
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKlimt(Landroid/content/Context;)I

    move-result v8

    const/4 v9, 0x1

    if-eq v8, v9, :cond_9

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isPacific(Landroid/content/Context;)I

    move-result v8

    if-nez v8, :cond_d

    .line 1406
    :cond_9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/Calculator;->onClick(Landroid/view/View;)V

    .line 1407
    sget v8, Lcom/sec/android/app/popupcalculator/Calculator;->mIsRapidKeyInputValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_c

    .line 1408
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1376
    .end local v2    # "id":I
    :cond_a
    const v8, 0x7f0e002d

    invoke-virtual {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v8

    if-ne v8, p1, :cond_b

    .line 1377
    const v8, 0x7f0b003b

    invoke-virtual {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1378
    :cond_b
    const v8, 0x7f0e002c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v8

    if-ne v8, p1, :cond_3

    .line 1379
    const v8, 0x7f0b0049

    invoke-virtual {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1410
    .restart local v2    # "id":I
    :cond_c
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1413
    :cond_d
    sget-object v8, Lcom/sec/android/app/popupcalculator/Calculator;->cTalkbackHandler:Landroid/os/Handler;

    sget v9, Lcom/sec/android/app/popupcalculator/Calculator;->STARTTALKBACK_LONGPRESS:I

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 1414
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->setBackSpaceTouch(Z)V

    .line 1415
    const v8, 0x7f0e000a

    if-eq v2, v8, :cond_f

    const/4 v8, -0x1

    if-eq v2, v8, :cond_f

    .line 1417
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    .line 1418
    .local v3, "left":I
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v9

    sub-int v4, v8, v9

    .line 1419
    .local v4, "right":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    .line 1420
    .local v5, "top":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v9

    sub-int v0, v8, v9

    .line 1421
    .local v0, "bottom":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v6, v8

    .line 1422
    .local v6, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v7, v8

    .line 1423
    .local v7, "y":I
    sget v8, Lcom/sec/android/app/popupcalculator/Calculator;->mIsRapidKeyInputValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_f

    sget-boolean v8, Lcom/sec/android/app/popupcalculator/Calculator;->mLongPressed:Z

    if-nez v8, :cond_f

    .line 1424
    if-le v6, v3, :cond_f

    if-ge v6, v4, :cond_f

    if-le v7, v5, :cond_f

    if-ge v7, v0, :cond_f

    .line 1425
    iget-object v8, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v8, p1}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    .line 1426
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    const v9, 0x7f0e002d

    if-ne v8, v9, :cond_e

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    const v9, 0x7f0e002c

    if-eq v8, v9, :cond_f

    .line 1427
    :cond_e
    const/16 v8, 0x40

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 1431
    .end local v0    # "bottom":I
    .end local v3    # "left":I
    .end local v4    # "right":I
    .end local v5    # "top":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :cond_f
    const/4 v8, 0x0

    sput-boolean v8, Lcom/sec/android/app/popupcalculator/Calculator;->mLongPressed:Z

    .line 1432
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, Landroid/view/View;->setClickable(Z)V

    .line 1433
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, Landroid/view/View;->setLongClickable(Z)V

    .line 1435
    :cond_10
    sget v8, Lcom/sec/android/app/popupcalculator/Calculator;->mIsRapidKeyInputValue:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_11

    .line 1436
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 1438
    :cond_11
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0x42

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1903
    const-string v1, "JP"

    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1904
    const/16 v1, 0x109

    if-ne p2, v1, :cond_1

    .line 1930
    :cond_0
    :goto_0
    return v0

    .line 1909
    :cond_1
    const/16 v1, 0x18

    if-eq p2, v1, :cond_2

    const/16 v1, 0x19

    if-ne p2, v1, :cond_4

    .line 1910
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_3

    .line 1911
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isVolumeKeyPressed:Z

    .line 1913
    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_4

    .line 1914
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mVolumeSeekbarFragmentTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 1917
    :cond_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_5

    const v1, 0x7f0e0018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-ne p1, v1, :cond_5

    if-ne p2, v4, :cond_5

    .line 1919
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/popupcalculator/Calculator;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1921
    :cond_5
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v1

    if-nez v1, :cond_0

    if-eq p2, v4, :cond_0

    const/16 v1, 0x13

    if-eq p2, v1, :cond_0

    const/16 v1, 0x14

    if-eq p2, v1, :cond_0

    const/16 v1, 0x15

    if-eq p2, v1, :cond_0

    const/16 v1, 0x16

    if-eq p2, v1, :cond_0

    const/16 v1, 0x3b

    if-eq p2, v1, :cond_0

    .line 1927
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1928
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/popupcalculator/Calculator;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "keyCode"    # I
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v8, 0x3d

    const/16 v7, 0x13

    const/16 v6, 0x52

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1245
    const/16 v4, 0xe4

    if-ne p1, v4, :cond_1

    .line 1335
    :cond_0
    :goto_0
    return v2

    .line 1247
    :cond_1
    const/16 v4, 0x106

    if-ne p1, v4, :cond_2

    move v2, v3

    .line 1248
    goto :goto_0

    .line 1250
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setBackSpaceTouch(Z)V

    .line 1251
    const/4 v4, 0x4

    if-ne p1, v4, :cond_3

    .line 1252
    iput-boolean v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBackKey:Z

    .line 1254
    :cond_3
    if-ne v6, p1, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1255
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOptionMenuShow:Z

    .line 1256
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->invalidateOptionsMenu()V

    .line 1258
    :cond_4
    if-ne v6, p1, :cond_6

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxxhdp(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isJapanModel()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    if-ne v4, v2, :cond_6

    :cond_5
    sget-object v4, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    if-eqz v4, :cond_6

    sget-object v4, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v4}, Landroid/app/ActionBar;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1261
    :cond_6
    if-ne v6, p1, :cond_9

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-lez v4, :cond_9

    .line 1263
    :cond_7
    iput-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOptionMenuShow:Z

    .line 1264
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->closeOptionsMenu()V

    .line 1272
    :cond_8
    const v4, 0x7f0e0011

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_b

    if-ne p1, v8, :cond_b

    const v4, 0x7f0e0019

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1274
    const v2, 0x7f0e0011

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move v2, v3

    .line 1275
    goto :goto_0

    .line 1265
    :cond_9
    iget-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHasKeyboard:Z

    if-eqz v4, :cond_8

    const/16 v4, 0x17

    if-eq p1, v4, :cond_a

    const/16 v4, 0x42

    if-ne p1, v4, :cond_8

    :cond_a
    sget-object v4, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    if-eqz v4, :cond_8

    .line 1267
    const v4, 0x7f0e001d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1268
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v4, 0x7f0e001d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    move v2, v3

    .line 1269
    goto/16 :goto_0

    .line 1277
    :cond_b
    if-ne p1, v7, :cond_c

    .line 1278
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-eqz v4, :cond_c

    .line 1279
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1280
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1281
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    const v5, 0x7f0e0018

    invoke-virtual {v4, v5}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 1293
    :cond_c
    :goto_1
    const-string v4, "input_method"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1294
    .local v0, "IMManager":Landroid/view/inputmethod/InputMethodManager;
    iget-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHasKeyboard:Z

    if-nez v4, :cond_d

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v4

    if-eqz v4, :cond_e

    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v4

    if-nez v4, :cond_11

    .line 1295
    :cond_e
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    .line 1283
    .end local v0    # "IMManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    if-eqz v4, :cond_10

    .line 1284
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    const v5, 0x7f0e0016

    invoke-virtual {v4, v5}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_1

    .line 1286
    :cond_10
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    const v5, 0x7f0e0015

    invoke-virtual {v4, v5}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_1

    .line 1298
    .restart local v0    # "IMManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_11
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v4

    if-nez v4, :cond_16

    const/16 v4, 0x42

    if-eq p1, v4, :cond_16

    if-eq p1, v7, :cond_16

    const/16 v4, 0x14

    if-eq p1, v4, :cond_16

    const/16 v4, 0x15

    if-eq p1, v4, :cond_16

    const/16 v4, 0x16

    if-eq p1, v4, :cond_16

    const/16 v4, 0x3b

    if-eq p1, v4, :cond_16

    const/16 v4, 0x6f

    if-eq p1, v4, :cond_16

    if-eq p1, v8, :cond_16

    const/16 v4, 0x1d

    if-ne p1, v4, :cond_12

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v4

    if-nez v4, :cond_16

    :cond_12
    const/16 v4, 0x34

    if-ne p1, v4, :cond_13

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v4

    if-nez v4, :cond_16

    :cond_13
    const/16 v4, 0x1f

    if-ne p1, v4, :cond_14

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v4

    if-nez v4, :cond_16

    :cond_14
    const/16 v4, 0x32

    if-ne p1, v4, :cond_15

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v4

    if-nez v4, :cond_16

    :cond_15
    const/16 v4, 0xa0

    if-ne p1, v4, :cond_17

    .line 1311
    :cond_16
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    .line 1313
    :cond_17
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/16 v5, 0x3e

    if-ne v4, v5, :cond_19

    .line 1314
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v4

    if-nez v4, :cond_18

    move v2, v3

    .line 1315
    goto/16 :goto_0

    .line 1316
    :cond_18
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1317
    .local v1, "vw":Landroid/view/View;
    if-eqz v1, :cond_19

    .line 1318
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    .line 1322
    .end local v1    # "vw":Landroid/view/View;
    :cond_19
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v4, 0x43

    if-ne v3, v4, :cond_1a

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1a

    .line 1324
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->onBackspace()V

    goto/16 :goto_0

    .line 1328
    :cond_1a
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v3

    if-eqz v3, :cond_1c

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v4, 0x111

    if-eq v3, v4, :cond_1b

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v4, 0x40

    if-ne v3, v4, :cond_1c

    .line 1330
    :cond_1b
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    .line 1333
    :cond_1c
    invoke-virtual {p0, p2}, Lcom/sec/android/app/popupcalculator/Calculator;->filter(Landroid/view/KeyEvent;)V

    goto/16 :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setBackSpaceTouch(Z)V

    .line 1345
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa0

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1347
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    const v1, 0x7f0e002d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(Landroid/view/View;)V

    .line 1348
    const/4 v0, 0x1

    .line 1350
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 2628
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 2629
    .local v4, "view":Landroid/view/View;
    :goto_0
    const-string v6, "backup_dsp"

    invoke-virtual {p0, v6, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2631
    .local v2, "sp":Landroid/content/SharedPreferences;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 2709
    :cond_0
    :goto_1
    :sswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    :cond_1
    return v5

    .line 2628
    .end local v2    # "sp":Landroid/content/SharedPreferences;
    .end local v4    # "view":Landroid/view/View;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    goto :goto_0

    .line 2634
    .restart local v2    # "sp":Landroid/content/SharedPreferences;
    .restart local v4    # "view":Landroid/view/View;
    :sswitch_1
    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->onHisClear()V

    .line 2635
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    .line 2636
    .local v3, "v":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 2638
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2639
    invoke-interface {p1, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2640
    const v5, 0x7f020699

    invoke-interface {p1, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_1

    .line 2653
    .end local v3    # "v":Landroid/view/View;
    :sswitch_2
    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    .line 2655
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "orientationByOptions"

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2657
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "orientationByUser"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 2673
    :sswitch_3
    invoke-virtual {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->setRequestedOrientation(I)V

    .line 2675
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "orientationByOptions"

    const/4 v7, 0x2

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2677
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "orientationByUser"

    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 2682
    :sswitch_4
    invoke-direct {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->changeOnehandView(I)V

    goto :goto_1

    .line 2686
    :sswitch_5
    invoke-direct {p0, v8}, Lcom/sec/android/app/popupcalculator/Calculator;->changeOnehandView(I)V

    goto :goto_1

    .line 2689
    :sswitch_6
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.samsung.action.MINI_MODE_SERVICE"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2690
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "com.sec.android.app.popupcalculator/com.sec.android.app.popupcalculator.PopupCalculatorService"

    invoke-static {v5}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2692
    const-string v6, "DISPLAY_TEXT"

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_2
    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2694
    const v5, 0x10008000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2697
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2702
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->finish()V

    goto/16 :goto_1

    .line 2692
    :cond_3
    const-string v5, ""

    goto :goto_2

    .line 2698
    :catch_0
    move-exception v0

    .line 2699
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 2705
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "intent":Landroid/content/Intent;
    :sswitch_7
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/app/popupcalculator/OpenSourceLicenseInfoActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 2631
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_7
        0x102002c -> :sswitch_0
        0x7f0e0059 -> :sswitch_1
        0x7f0e005a -> :sswitch_6
    .end sparse-switch
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 2623
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOptionMenuShow:Z

    .line 2624
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1532
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1534
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v2, "-- onPause -- "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1536
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPersist:Lcom/sec/android/app/popupcalculator/Persist;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/Persist;->save()V

    .line 1537
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->savePanelState()V

    .line 1541
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->saveCurrentText()V

    .line 1544
    sput-boolean v3, Lcom/sec/android/app/popupcalculator/Calculator;->mConfigChange:Z

    .line 1545
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1548
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 1549
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1551
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOptionMenuShow:Z

    if-eqz v1, :cond_1

    .line 1552
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->closeOptionsMenu()V

    .line 1553
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->cancelToast()V

    .line 1554
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setEnabled(Z)V

    .line 1555
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setEnabled(Z)V

    .line 1556
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    sput-boolean v4, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    .line 1559
    sget-boolean v1, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v1, :cond_2

    .line 1560
    invoke-direct {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->setOneHandAdaptableOperation(Z)V

    .line 1561
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->UnregisterFlatMotionListener()V

    .line 1563
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->unregisterOnehandAnyScreenObserver()V

    .line 1565
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v9, 0x7f020699

    const v8, 0x7f020698

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 2496
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "accelerometer_rotation"

    invoke-static {v4, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 2499
    .local v3, "setting":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v2, v4, Landroid/content/res/Configuration;->orientation:I

    .line 2500
    .local v2, "config":I
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTabletUnder10inch(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v6, :cond_4

    .line 2501
    invoke-interface {p1, v5}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->isDisplayScreen()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->isHistoryScreen()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move v4, v6

    :goto_0
    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2503
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->isDisplayScreen()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->isHistoryScreen()Z

    move-result v4

    if-nez v4, :cond_3

    .line 2504
    invoke-interface {p1, v5}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2509
    :goto_1
    sget-boolean v4, Lcom/sec/android/app/popupcalculator/Feature;->CALCULATOR_SUPPORTS_OPENSOURCE_LICENSE:Z

    if-eqz v4, :cond_1

    .line 2510
    const/4 v4, 0x7

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2512
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v4

    .line 2619
    :goto_2
    return v4

    :cond_2
    move v4, v5

    .line 2501
    goto :goto_0

    .line 2507
    :cond_3
    invoke-interface {p1, v5}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_1

    .line 2514
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0a000b

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    if-eq v4, v6, :cond_6

    .line 2515
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->isDisplayScreen()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->isHistoryScreen()Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_5
    move v4, v6

    :goto_3
    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2517
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->isDisplayScreen()Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->isHistoryScreen()Z

    move-result v4

    if-nez v4, :cond_9

    .line 2518
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2571
    :cond_6
    :goto_4
    if-nez v3, :cond_a

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKlimt(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v6, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxxhdp(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxhdp(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_a

    :cond_7
    iget-boolean v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z

    if-nez v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z

    move-result v4

    if-nez v4, :cond_a

    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isOnehandAnyScreenMode()Z

    move-result v4

    if-nez v4, :cond_a

    .line 2574
    const/4 v0, 0x0

    .local v0, "a":I
    const/4 v1, 0x0

    .line 2575
    .local v1, "b":I
    packed-switch v2, :pswitch_data_0

    .line 2586
    const/4 v0, 0x2

    .line 2587
    const/4 v1, 0x3

    .line 2591
    :goto_5
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2592
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2619
    .end local v0    # "a":I
    .end local v1    # "b":I
    :goto_6
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v4

    goto/16 :goto_2

    :cond_8
    move v4, v5

    .line 2515
    goto :goto_3

    .line 2521
    :cond_9
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_4

    .line 2577
    .restart local v0    # "a":I
    .restart local v1    # "b":I
    :pswitch_0
    const/4 v0, 0x3

    .line 2578
    const/4 v1, 0x2

    .line 2579
    goto :goto_5

    .line 2594
    .end local v0    # "a":I
    .end local v1    # "b":I
    :cond_a
    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2595
    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_6

    .line 2575
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 14

    .prologue
    .line 1568
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1570
    sget-object v9, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v10, "-- onResume -- "

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1571
    const-string v9, "VerificationLog"

    const-string v10, "-- onResume -- "

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1572
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isPacific(Landroid/content/Context;)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 1574
    sget-object v9, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    if-eqz v9, :cond_b

    const v9, 0x7f0e0018

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_b

    sget-object v9, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v9}, Landroid/app/ActionBar;->isShowing()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 1575
    const v9, 0x7f0e0018

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0xf

    const/16 v11, 0x9f

    const/16 v12, 0x2a

    const/4 v13, 0x5

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/view/View;->setPadding(IIII)V

    .line 1579
    :cond_0
    :goto_0
    const-string v9, "backup_dsp"

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 1582
    .local v6, "sharedPref":Landroid/content/SharedPreferences;
    const-string v9, "accessibility"

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1584
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->context:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/popupcalculator/Calculator;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v9

    sput-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackSettingOn:Z

    .line 1585
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->context:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/popupcalculator/Calculator;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v9

    sput-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackOn:Z

    .line 1586
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemRapidKeyInputSettingValue()I

    move-result v9

    sput v9, Lcom/sec/android/app/popupcalculator/Calculator;->mIsRapidKeyInputValue:I

    .line 1589
    const/4 v8, 0x0

    .line 1591
    .local v8, "windowMode":I
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v9, :cond_1

    .line 1592
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z

    .line 1596
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getSystemOneHandAdaptableOperationSettingValue()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_c

    const/4 v9, 0x1

    :goto_1
    sput-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    .line 1597
    sget-object v9, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mOnehandAdaptableOperation : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1599
    sget-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z

    if-eqz v9, :cond_2

    .line 1600
    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->setOneHandAdaptableOperation(Z)V

    .line 1601
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->registerFlatMotionListener()V

    .line 1603
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->registerOnehandAnyScreenObserver()V

    .line 1605
    const-string v9, "service.camera.running"

    const-string v10, "false"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1606
    .local v1, "cameraRunningCheck":Ljava/lang/String;
    const-string v9, "1"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1607
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mCustomObserver:Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/popupcalculator/Calculator$CustomContentObserver;->onChange(Z)V

    .line 1609
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandModeValue()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    .line 1612
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandSwitchStateValue()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    .line 1624
    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initButtonTypeface()V

    .line 1626
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBundle:Landroid/os/Bundle;

    const-string v10, "EXTRA_REVIEW"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 1628
    .local v5, "review":I
    if-eqz v5, :cond_5

    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBundle:Landroid/os/Bundle;

    const-string v10, "EXTRA_ORIENTATION"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    if-ne v9, v10, :cond_5

    .line 1630
    invoke-virtual {p0, v5}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mReview:Landroid/view/View;

    .line 1631
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v9}, Landroid/os/Bundle;->clear()V

    .line 1634
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-eqz v9, :cond_6

    .line 1635
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-eqz v9, :cond_6

    .line 1636
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v9}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Error"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_d

    .line 1637
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 1644
    :cond_6
    :goto_3
    const-string v9, "Epression"

    invoke-interface {v6, v9}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1645
    const-string v9, "Epression"

    const-string v10, ""

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1646
    .local v2, "displayExp":Ljava/lang/String;
    const v9, 0x7f0e0018

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 1647
    .local v3, "editText":Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const/4 v10, 0x0

    iput-boolean v10, v9, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 1648
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v9, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1649
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 1650
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v9, "Epression"

    invoke-interface {v4, v9}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1651
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1652
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/widgetapp/calculator/Panel;->setOpen(ZZ)Z

    .line 1653
    sget-object v9, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    if-eqz v9, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_7

    .line 1655
    sget-object v9, Lcom/sec/android/app/popupcalculator/Calculator;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v9}, Landroid/app/ActionBar;->show()V

    .line 1658
    .end local v2    # "displayExp":Ljava/lang/String;
    .end local v3    # "editText":Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v9

    if-eqz v9, :cond_e

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 1659
    .local v7, "view":Landroid/view/View;
    :goto_4
    if-eqz v7, :cond_8

    .line 1660
    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    .line 1663
    :cond_8
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->mConfigChange:Z

    .line 1665
    sget v9, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentTextSize:I

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->setTextSize(I)V

    .line 1667
    sget-boolean v9, Lcom/sec/android/app/popupcalculator/Calculator;->isEuroModeOn:Z

    if-eqz v9, :cond_a

    .line 1668
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_9

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_9

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_f

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isBlack(Landroid/content/Context;)I

    move-result v9

    if-nez v9, :cond_f

    .line 1669
    :cond_9
    const v9, 0x7f0e002b

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    const v10, 0x7f02040f

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1674
    :cond_a
    :goto_5
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/16 v10, 0x9

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setNewActionPopupMenu(IZ)V

    .line 1675
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    const/16 v10, 0x9

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/widget/EditText;->setNewActionPopupMenu(IZ)V

    .line 1678
    const-string v9, "VerificationLog"

    const-string v10, "-- Executed -- "

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1679
    return-void

    .line 1576
    .end local v0    # "am":Landroid/view/accessibility/AccessibilityManager;
    .end local v1    # "cameraRunningCheck":Ljava/lang/String;
    .end local v5    # "review":I
    .end local v6    # "sharedPref":Landroid/content/SharedPreferences;
    .end local v7    # "view":Landroid/view/View;
    .end local v8    # "windowMode":I
    :cond_b
    const v9, 0x7f0e0018

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 1577
    const v9, 0x7f0e0018

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0xf

    const/16 v11, 0x27

    const/16 v12, 0x2a

    const/4 v13, 0x5

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_0

    .line 1596
    .restart local v0    # "am":Landroid/view/accessibility/AccessibilityManager;
    .restart local v6    # "sharedPref":Landroid/content/SharedPreferences;
    .restart local v8    # "windowMode":I
    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 1614
    .restart local v1    # "cameraRunningCheck":Ljava/lang/String;
    :pswitch_0
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    goto/16 :goto_2

    .line 1617
    :pswitch_1
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z

    goto/16 :goto_2

    .line 1639
    .restart local v5    # "review":I
    :cond_d
    iget-object v9, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const/4 v10, 0x0

    iput-boolean v10, v9, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    goto/16 :goto_3

    .line 1658
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    goto/16 :goto_4

    .line 1671
    .restart local v7    # "view":Landroid/view/View;
    :cond_f
    const v9, 0x7f0e002b

    invoke-virtual {p0, v9}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    const v10, 0x7f0b0075

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setText(I)V

    goto :goto_5

    .line 1612
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1512
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->mConfigChange:Z

    .line 1513
    invoke-super {p0}, Landroid/app/Activity;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 777
    const-string v1, "EXTRA_PANEL_STATE"

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v3

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 778
    const-string v1, "EXTRA_ENTER_END"

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-boolean v3, v3, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 779
    const-string v1, "IS_ONE_HAND_RETAIN"

    iget-boolean v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOneHandRetain:Z

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 780
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBundle:Landroid/os/Bundle;

    const-string v4, "EXTRA_REVIEW"

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mReview:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mReview:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    :goto_0
    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 781
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mBundle:Landroid/os/Bundle;

    const-string v3, "EXTRA_ORIENTATION"

    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 783
    const-string v3, "EXTRA_STRING"

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 786
    .local v0, "displayText":Ljava/lang/String;
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 787
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    const-string v4, "="

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 789
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 791
    :cond_0
    const-string v1, "EXTRA_FURMULA"

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    const-string v1, "EXTRA_RESULT"

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    const-string v1, "EXTRA_IS_TOUCHMODE"

    iget-boolean v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTouchMode:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 797
    const-string v1, "EXTRA_CURRENT_INDEX"

    iget v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentIndex:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 799
    const-string v1, "EXTRA_LAST_RESULT"

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getLastResult()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    const-string v1, "EXTRA_FORMULA_LENGTH_BACKUP"

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormulaLengthBackup()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 801
    const-string v1, "EXTRA_FORMULA_BACKUP"

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormulaBackup()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 804
    return-void

    .end local v0    # "displayText":Ljava/lang/String;
    :cond_1
    move v1, v2

    .line 780
    goto/16 :goto_0

    .line 783
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public onSaveRotationData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3422
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPersist:Lcom/sec/android/app/popupcalculator/Persist;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/Persist;->save()V

    .line 3423
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->savePanelState()V

    .line 3427
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->saveCurrentText()V

    .line 3430
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 3433
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 3434
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 3437
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setEnabled(Z)V

    .line 3438
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setEnabled(Z)V

    .line 3439
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getSelectionStart()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->cursor_position:I

    .line 3440
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1682
    sget-object v0, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v1, "-- onStop -- "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1684
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 1685
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const v3, 0x7f0e0018

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 1442
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-nez v0, :cond_1

    .line 1507
    :cond_0
    :goto_0
    return v4

    .line 1445
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackOn:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsRapidKeyInputValue:I

    if-ne v0, v8, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 1448
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1449
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mClipboardExManager:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 1451
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->isStateReady()Z

    move-result v0

    if-nez v0, :cond_4

    move v4, v8

    .line 1452
    goto :goto_0

    .line 1454
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v3, :cond_7

    .line 1455
    sput-boolean v8, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    .line 1459
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/inputmethod/InputMethodManager;

    .line 1462
    .local v7, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v7, :cond_5

    .line 1463
    invoke-virtual {p1}, Landroid/view/View;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {v7, v0, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1465
    :cond_5
    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v3, :cond_8

    .line 1466
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_6

    .line 1467
    invoke-virtual {p1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 1468
    invoke-virtual {p1, v8}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1469
    invoke-virtual {p1, v8}, Landroid/view/View;->setFocusable(Z)V

    .line 1470
    invoke-virtual {p1, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 1472
    :cond_6
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v8, :cond_0

    goto :goto_0

    .line 1457
    .end local v7    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_7
    sput-boolean v4, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    goto :goto_1

    .line 1478
    .restart local v7    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_8
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 1480
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/Calculator;->onVibrator(Landroid/view/View;)V

    .line 1481
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_effects_enabled"

    invoke-static {v0, v1, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v8, :cond_a

    .line 1483
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    .line 1484
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_9

    .line 1485
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e001d

    if-ne v0, v1, :cond_b

    .line 1486
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    sget v1, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentBackkeySoundID:I

    move v3, v2

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 1490
    :cond_9
    :goto_2
    sput-boolean v8, Lcom/sec/android/app/popupcalculator/Calculator;->isTouchsounded:Z

    .line 1494
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/popupcalculator/EventHandler;->setBackSpaceTouch(Z)V

    .line 1495
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mReview:Landroid/view/View;

    goto/16 :goto_0

    .line 1488
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mButtonSoundPool:Landroid/media/SoundPool;

    sget v1, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentSoundID:I

    move v3, v2

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto :goto_2

    .line 1500
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-eqz v0, :cond_0

    .line 1501
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setBackSpaceTouch(Z)V

    goto/16 :goto_0

    .line 1478
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onVibrator(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1522
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_FRAMEWORK_SUPPORT_VIBETONZ"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHapticFeedbackEnabled:Z

    if-eqz v0, :cond_0

    .line 1525
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    .line 1526
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 1528
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 2437
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->isMultiSelection(Landroid/widget/EditText;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2438
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V

    .line 2439
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->isTaskRoot()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->isOptionMenuShow:Z

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2440
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->openOptionsMenu()V

    .line 2443
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHasKeyboard:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0e0008

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2444
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->requestFocus()Z

    .line 2447
    :cond_2
    const-string v1, "backup_dsp"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2450
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "editisFocus"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2451
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v1

    const-string v2, "capuccino"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2452
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->invalidateOptionsMenu()V

    .line 2453
    :cond_3
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 2454
    return-void
.end method

.method public onWindowStatusChanged(ZZZ)V
    .locals 4
    .param p1, "isMaximized"    # Z
    .param p2, "isMinimized"    # Z
    .param p3, "isPinup"    # Z

    .prologue
    const/4 v3, 0x1

    .line 3404
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;

    invoke-direct {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;-><init>()V

    .line 3405
    .local v0, "mMultiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindow;
    if-nez p2, :cond_0

    .line 3406
    sget-object v1, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v2, "onWindowStatusChanged isMaximized false  "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3408
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;->isFeatureEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3410
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->onSaveRotationData()V

    .line 3411
    const v1, 0x7f040005

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->setContentView(I)V

    .line 3412
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->initControls()V

    .line 3419
    :cond_0
    :goto_0
    return-void

    .line 3415
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v3, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->change_layout(ZZZ)V

    goto :goto_0
.end method

.method public setTextSize(I)V
    .locals 6
    .param p1, "nType"    # I

    .prologue
    const/4 v5, 0x1

    .line 2378
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v0, v3, Landroid/content/res/Configuration;->orientation:I

    .line 2379
    .local v0, "ori":I
    sget v3, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_LARGE_PORT:I

    int-to-float v1, v3

    .line 2380
    .local v1, "textSize":F
    sget v3, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_MEDIUM_PORT:I

    int-to-float v2, v3

    .line 2399
    .local v2, "textSizeHistory":F
    if-ne v0, v5, :cond_2

    .line 2400
    sget v3, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_LARGE_PORT:I

    int-to-float v1, v3

    .line 2401
    sget v3, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_MEDIUM_PORT:I

    int-to-float v2, v3

    .line 2412
    :goto_0
    sput p1, Lcom/sec/android/app/popupcalculator/Calculator;->mCurrentTextSize:I

    .line 2413
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-eqz v3, :cond_0

    .line 2414
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mLogic:Lcom/sec/android/app/popupcalculator/EventHandler;

    iput v1, v3, Lcom/sec/android/app/popupcalculator/EventHandler;->SelectedTextSize:F

    .line 2416
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    if-eqz v3, :cond_1

    .line 2417
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2418
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v3

    if-ne v3, v5, :cond_5

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isEspresso800dp_mdpi(Landroid/content/Context;)I

    move-result v3

    if-nez v3, :cond_5

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v3

    if-nez v3, :cond_5

    .line 2419
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    sget v4, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_MEDIUM_PORT:I

    int-to-float v4, v4

    invoke-virtual {v3, v5, v4}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 2423
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    if-eqz v3, :cond_1

    .line 2424
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/EventListener;->updateHistory()V

    .line 2426
    :cond_1
    return-void

    .line 2403
    :cond_2
    sget v3, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_LARGE_LAND:I

    int-to-float v1, v3

    .line 2404
    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v3

    if-eq v3, v5, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v3

    if-eq v3, v5, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKlimt(Landroid/content/Context;)I

    move-result v3

    if-ne v3, v5, :cond_4

    .line 2405
    :cond_3
    iget v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->HISTORY_TEXT_SIZE_LAND:I

    int-to-float v2, v3

    goto :goto_0

    .line 2407
    :cond_4
    sget v3, Lcom/sec/android/app/popupcalculator/Calculator;->TEXT_SIZE_MEDIUM_LAND:I

    int-to-float v2, v3

    goto :goto_0

    .line 2421
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/Calculator;->mHistoryScreen:Landroid/widget/EditText;

    invoke-virtual {v3, v5, v2}, Landroid/widget/EditText;->setTextSize(IF)V

    goto :goto_1
.end method

.method public setViewWithActionBar()V
    .locals 21

    .prologue
    .line 2804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v11, v0

    .line 2805
    .local v11, "lastIndexOfBasic":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0025

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 2806
    .local v5, "basicGap":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0024

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 2807
    .local v4, "basicButtonH":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0023

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 2809
    .local v3, "advancedGap":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0022

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 2810
    .local v2, "advancedButtonH":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0026

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    .line 2811
    .local v14, "panelH":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/calculator/Panel;->getHandle()Landroid/view/View;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getHeight()I

    move-result v15

    .line 2812
    .local v15, "panelHandleH":I
    const/4 v7, 0x0

    .line 2813
    .local v7, "buttonH":I
    const/4 v8, 0x0

    .line 2814
    .local v8, "buttonY":I
    const v18, 0x7f0e0045

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 2816
    .local v12, "panelBg":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/calculator/Panel;->getParent()Landroid/view/ViewParent;

    move-result-object v18

    check-cast v18, Landroid/widget/FrameLayout;

    invoke-virtual/range {v18 .. v18}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v18

    sub-int v18, v18, v14

    sub-int v9, v18, v15

    .line 2817
    .local v9, "editTextHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v18, v0

    int-to-float v0, v9

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const/high16 v20, 0x3f000000    # 0.5f

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setHeight(I)V

    .line 2819
    invoke-virtual {v12}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    .line 2820
    .local v13, "panelBgParams":Landroid/view/ViewGroup$LayoutParams;
    add-int/lit8 v18, v14, 0x5

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f000000    # 0.5f

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2821
    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/calculator/Panel;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .line 2824
    .local v16, "panelParams":Landroid/view/ViewGroup$LayoutParams;
    add-int v18, v14, v15

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f000000    # 0.5f

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2826
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2828
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v10, v0, :cond_0

    .line 2829
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    move-object/from16 v18, v0

    aget v18, v18, v10

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageButton;

    .line 2830
    .local v17, "vw":Landroid/widget/ImageButton;
    if-nez v17, :cond_1

    .line 2831
    sget-object v18, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v19, "setViewWithActionBar Button ID = null"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2851
    .end local v17    # "vw":Landroid/widget/ImageButton;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/calculator/Panel;->requestLayout()V

    .line 2852
    return-void

    .line 2834
    .restart local v17    # "vw":Landroid/widget/ImageButton;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    move-object/from16 v18, v0

    aget v18, v18, v10

    const v19, 0x7f0e002d

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 2835
    move v11, v10

    .line 2837
    :cond_2
    if-gt v10, v11, :cond_3

    .line 2838
    sget v18, Lcom/sec/android/app/popupcalculator/Calculator;->NB_BUTTON_COLUMNS:I

    div-int v18, v10, v18

    add-int v19, v5, v4

    mul-int v8, v18, v19

    .line 2839
    move v7, v4

    .line 2844
    :goto_1
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/AbsoluteLayout$LayoutParams;

    .line 2846
    .local v6, "btnParams":Landroid/widget/AbsoluteLayout$LayoutParams;
    int-to-float v0, v7

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f000000    # 0.5f

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v6, Landroid/widget/AbsoluteLayout$LayoutParams;->height:I

    .line 2847
    int-to-float v0, v8

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f000000    # 0.5f

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v6, Landroid/widget/AbsoluteLayout$LayoutParams;->y:I

    .line 2848
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2849
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_small_src_id:[I

    move-object/from16 v18, v0

    aget v18, v18, v10

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2828
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 2841
    .end local v6    # "btnParams":Landroid/widget/AbsoluteLayout$LayoutParams;
    :cond_3
    sub-int v18, v10, v11

    add-int/lit8 v18, v18, -0x1

    sget v19, Lcom/sec/android/app/popupcalculator/Calculator;->NB_BUTTON_COLUMNS:I

    div-int v18, v18, v19

    add-int v19, v3, v2

    mul-int v8, v18, v19

    .line 2842
    move v7, v2

    goto :goto_1
.end method

.method public setViewWithoutActionBar()V
    .locals 21

    .prologue
    .line 2752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v11, v0

    .line 2753
    .local v11, "lastIndexOfBasic":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0020

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 2754
    .local v5, "basicGap":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a001f

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 2755
    .local v4, "basicButtonH":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a001e

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 2757
    .local v3, "advancedGap":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a001d

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 2758
    .local v2, "advancedButtonH":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0a0021

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    .line 2759
    .local v14, "panelH":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/calculator/Panel;->getHandle()Landroid/view/View;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getHeight()I

    move-result v15

    .line 2760
    .local v15, "panelHandleH":I
    const/4 v7, 0x0

    .line 2761
    .local v7, "buttonH":I
    const/4 v8, 0x0

    .line 2762
    .local v8, "buttonY":I
    const v18, 0x7f0e0045

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 2764
    .local v12, "panelBg":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/calculator/Panel;->getParent()Landroid/view/ViewParent;

    move-result-object v18

    check-cast v18, Landroid/widget/FrameLayout;

    invoke-virtual/range {v18 .. v18}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v18

    sub-int v18, v18, v14

    sub-int v9, v18, v15

    .line 2765
    .local v9, "editTextHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v18, v0

    int-to-float v0, v9

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const/high16 v20, 0x3f000000    # 0.5f

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setHeight(I)V

    .line 2767
    invoke-virtual {v12}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    .line 2768
    .local v13, "panelBgParams":Landroid/view/ViewGroup$LayoutParams;
    add-int/lit8 v18, v14, 0x5

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f000000    # 0.5f

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v13, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2769
    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/calculator/Panel;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .line 2772
    .local v16, "panelParams":Landroid/view/ViewGroup$LayoutParams;
    add-int v18, v14, v15

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f000000    # 0.5f

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2774
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2776
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v10, v0, :cond_0

    .line 2777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    move-object/from16 v18, v0

    aget v18, v18, v10

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageButton;

    .line 2778
    .local v17, "vw":Landroid/widget/ImageButton;
    if-nez v17, :cond_1

    .line 2779
    sget-object v18, Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;

    const-string v19, "setViewWithoutActionBar Button ID = null"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2799
    .end local v17    # "vw":Landroid/widget/ImageButton;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/calculator/Panel;->requestLayout()V

    .line 2800
    return-void

    .line 2782
    .restart local v17    # "vw":Landroid/widget/ImageButton;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_id:[I

    move-object/from16 v18, v0

    aget v18, v18, v10

    const v19, 0x7f0e002d

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 2783
    move v11, v10

    .line 2785
    :cond_2
    if-gt v10, v11, :cond_3

    .line 2786
    sget v18, Lcom/sec/android/app/popupcalculator/Calculator;->NB_BUTTON_COLUMNS:I

    div-int v18, v10, v18

    add-int v19, v5, v4

    mul-int v8, v18, v19

    .line 2787
    move v7, v4

    .line 2792
    :goto_1
    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/AbsoluteLayout$LayoutParams;

    .line 2794
    .local v6, "btnParams":Landroid/widget/AbsoluteLayout$LayoutParams;
    int-to-float v0, v7

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f000000    # 0.5f

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v6, Landroid/widget/AbsoluteLayout$LayoutParams;->height:I

    .line 2795
    int-to-float v0, v8

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupcalculator/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    const/high16 v19, 0x3f000000    # 0.5f

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    iput v0, v6, Landroid/widget/AbsoluteLayout$LayoutParams;->y:I

    .line 2796
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->bt_large_src_id:[I

    move-object/from16 v18, v0

    aget v18, v18, v10

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2776
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 2789
    .end local v6    # "btnParams":Landroid/widget/AbsoluteLayout$LayoutParams;
    :cond_3
    sub-int v18, v10, v11

    add-int/lit8 v18, v18, -0x1

    sget v19, Lcom/sec/android/app/popupcalculator/Calculator;->NB_BUTTON_COLUMNS:I

    div-int v18, v18, v19

    add-int v19, v3, v2

    mul-int v8, v18, v19

    .line 2790
    move v7, v2

    goto :goto_1
.end method
