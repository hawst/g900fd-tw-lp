.class Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;
.super Ljava/lang/Object;
.source "CalculatorEditText.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->onTextChanged(Ljava/lang/CharSequence;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 288
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getSelectionStart()I

    move-result v2

    .line 289
    .local v2, "startCursor":I
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 290
    .local v1, "beforeText":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-eqz v3, :cond_0

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 292
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 293
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v5, v5, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v5, v5, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v6, v6, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v6}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 296
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "afterText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v3, v4, :cond_1

    .line 298
    const-string v3, ""

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "0"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 300
    add-int/lit8 v2, v2, -0x1

    .line 303
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v3

    if-gt v2, v3, :cond_6

    .line 304
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_5

    .line 307
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 308
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v5, v5, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 314
    :cond_3
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 319
    :goto_1
    return-void

    .line 310
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_3

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v5, v5, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    .line 316
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto :goto_1

    .line 318
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;->this$1:Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;

    iget-object v4, v4, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto :goto_1
.end method
