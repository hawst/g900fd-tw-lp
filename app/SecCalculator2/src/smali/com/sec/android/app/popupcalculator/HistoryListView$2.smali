.class Lcom/sec/android/app/popupcalculator/HistoryListView$2;
.super Ljava/lang/Object;
.source "HistoryListView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupcalculator/HistoryListView;->createDiscardDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

.field final synthetic val$input:Ljava/lang/String;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/HistoryListView;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    iput-object p2, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->val$input:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->val$input:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/popupcalculator/HistoryListView;->saveToSharedPref(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/HistoryListView;->access$000(Lcom/sec/android/app/popupcalculator/HistoryListView;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/popupcalculator/HistoryListView;->isAlertShown:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/HistoryListView;->access$102(Lcom/sec/android/app/popupcalculator/HistoryListView;Z)Z

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/HistoryListView;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->this$0:Lcom/sec/android/app/popupcalculator/HistoryListView;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/HistoryListView$2;->val$intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/HistoryListView;->startActivity(Landroid/content/Intent;)V

    .line 134
    :cond_0
    return-void
.end method
