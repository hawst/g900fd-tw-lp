.class Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;
.super Ljava/lang/Object;
.source "CalculatorEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/CalculatorEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 595
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getLineCount()I

    move-result v0

    if-le v0, v4, :cond_1

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iput-boolean v3, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mEnterEnd:Z

    .line 603
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackSettingOn:Z

    if-eqz v0, :cond_2

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setFocusable(Z)V

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setFocusableInTouchMode(Z)V

    .line 611
    :goto_0
    return-void

    .line 607
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setFocusable(Z)V

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setFocusableInTouchMode(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 591
    return-void
.end method

.method public getEqualString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 8
    .param p1, "originalText"    # Ljava/lang/String;
    .param p2, "start"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 620
    const-string v2, ""

    .line 621
    .local v2, "text":Ljava/lang/String;
    const-string v3, "="

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 622
    .local v0, "index":I
    const-string v3, "="

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 623
    .local v1, "lastIndex":I
    const-string v3, "="

    const-string v4, ""

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 624
    if-ne v0, v1, :cond_0

    .line 625
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 638
    :goto_0
    return-object v2

    .line 626
    :cond_0
    if-nez v0, :cond_1

    .line 627
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 628
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_2

    .line 629
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 630
    :cond_2
    if-ge v0, p2, :cond_3

    .line 631
    const-string v3, "%s=%s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 634
    :cond_3
    const-string v3, "%s=%s"

    new-array v4, v5, [Ljava/lang/Object;

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v2, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 29
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v25

    check-cast v25, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/Calculator;->invalidateOptionsMenu()V

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v25

    const-string v26, "input_method"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 113
    .local v3, "IMManager":Landroid/view/inputmethod/InputMethodManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    move/from16 v25, v0

    if-nez v25, :cond_0

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v25

    if-nez v25, :cond_31

    .line 114
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    move/from16 v25, v0

    if-nez v25, :cond_1

    if-gtz p4, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    move/from16 v25, v0

    if-nez v25, :cond_1e

    if-nez p4, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isSpaceBefore:Z
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$200(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Z

    move-result v25

    if-eqz v25, :cond_1e

    .line 115
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isSpaceBefore:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$202(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 116
    add-int v25, p2, p4

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, v25

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 118
    .local v8, "addedText":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v25

    const-string v26, "capuccino"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0b0016

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 122
    .local v16, "errorString":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v25

    const-string v26, "="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_3

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setCursor(I)V

    .line 127
    :cond_3
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-nez v25, :cond_7

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isSpaceBefore:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$202(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 129
    const-string v25, "\n"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-nez v25, :cond_4

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 132
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    if-nez v25, :cond_5

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_5

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v26

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    .line 586
    .end local v8    # "addedText":Ljava/lang/String;
    .end local v16    # "errorString":Ljava/lang/String;
    :cond_5
    :goto_1
    return-void

    .line 121
    .restart local v8    # "addedText":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0b0057

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .restart local v16    # "errorString":Ljava/lang/String;
    goto/16 :goto_0

    .line 136
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    if-eqz v25, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    if-eqz v25, :cond_16

    .line 138
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v25

    const-string v26, "="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_8

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 143
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 144
    .local v21, "result":Ljava/lang/String;
    const-string v13, ""

    .line 145
    .local v13, "displayText":Ljava/lang/String;
    const/4 v15, 0x0

    .line 146
    .local v15, "errorCode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$302(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 147
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_c

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_c

    .line 150
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    .line 152
    .local v23, "subAdd":Ljava/lang/String;
    const-string v25, "Error"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_9

    const-string v25, "Error:"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_9

    .line 153
    const-string v25, "r"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v26

    move-object/from16 v0, v21

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 155
    :cond_9
    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v25

    if-nez v25, :cond_a

    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v25

    if-eqz v25, :cond_b

    :cond_a
    if-eqz v15, :cond_c

    .line 157
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$302(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 160
    .end local v23    # "subAdd":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$300(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Z

    move-result v25

    if-eqz v25, :cond_11

    const-string v25, "Error"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_11

    const-string v25, "Error:"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_11

    .line 162
    const-string v25, "r"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v26

    move-object/from16 v0, v21

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 164
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\n="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 168
    const-string v25, "%d"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_e

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0b0005

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0b0010

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_f

    .line 172
    :cond_d
    const-string v25, "%d"

    const-string v26, "100"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    .line 179
    :cond_e
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 173
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0b0007

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0b0012

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_e

    .line 176
    :cond_10
    const-string v25, "%d"

    const-string v26, "20"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_2

    .line 185
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 186
    .local v12, "bufferString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 188
    .local v11, "bufferFormula":Ljava/lang/String;
    const/16 v25, 0x3d

    :try_start_0
    move/from16 v0, v25

    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_12

    .line 189
    const/16 v25, 0x0

    const/16 v26, 0xa

    move/from16 v0, v26

    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 200
    :cond_12
    :goto_3
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v12, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_14

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_14

    if-nez p3, :cond_14

    .line 202
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v25

    const-string v26, "capuccino"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0b0016

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 206
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$302(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 192
    :catch_0
    move-exception v14

    .line 194
    .local v14, "e":Ljava/lang/StringIndexOutOfBoundsException;
    const/16 v25, 0x0

    const/16 v26, 0x3d

    :try_start_1
    move/from16 v0, v26

    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v26

    add-int/lit8 v26, v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    goto/16 :goto_3

    .line 196
    :catch_1
    move-exception v4

    .line 197
    .local v4, "_e":Ljava/lang/StringIndexOutOfBoundsException;
    const-string v11, ""

    goto/16 :goto_3

    .line 205
    .end local v4    # "_e":Ljava/lang/StringIndexOutOfBoundsException;
    .end local v14    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0b0057

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_4

    .line 213
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 273
    .end local v11    # "bufferFormula":Ljava/lang/String;
    .end local v12    # "bufferString":Ljava/lang/String;
    .end local v13    # "displayText":Ljava/lang/String;
    .end local v15    # "errorCode":I
    .end local v21    # "result":Ljava/lang/String;
    :cond_15
    :goto_5
    sget-boolean v25, Lcom/sec/android/app/popupcalculator/Calculator;->mConfigChange:Z

    if-eqz v25, :cond_1e

    .line 274
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0b0057

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_1d

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x3d

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->indexOf(I)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1d

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 216
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    if-nez v25, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    if-eqz v25, :cond_18

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 219
    .restart local v21    # "result":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$302(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 220
    const-string v13, ""

    .line 221
    .restart local v13    # "displayText":Ljava/lang/String;
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_17

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_17

    .line 223
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    .line 225
    .restart local v23    # "subAdd":Ljava/lang/String;
    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v25

    if-nez v25, :cond_17

    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v25

    if-nez v25, :cond_17

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$302(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 230
    .end local v23    # "subAdd":Ljava/lang/String;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$300(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Z

    move-result v25

    if-eqz v25, :cond_15

    const-string v25, "Error"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_15

    const-string v25, "Error:"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_15

    .line 231
    const-string v25, "r"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v26

    move-object/from16 v0, v21

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 233
    .restart local v15    # "errorCode":I
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\n="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mContext:Landroid/content/Context;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$100(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Landroid/content/Context;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 243
    .end local v13    # "displayText":Ljava/lang/String;
    .end local v15    # "errorCode":I
    .end local v21    # "result":Ljava/lang/String;
    :cond_18
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-eqz v25, :cond_15

    .line 244
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    if-nez v25, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    if-nez v25, :cond_1b

    .line 246
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 247
    .local v24, "subString":[Ljava/lang/String;
    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_1a

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aget-object v26, v24, v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    aget-object v26, v24, v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/text/Editable;->length()I

    move-result v25

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_19

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 254
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_5

    .line 256
    :cond_1a
    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v25, v0

    if-lez v25, :cond_15

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aget-object v26, v24, v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 260
    .end local v24    # "subString":[Ljava/lang/String;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 261
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x20

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->indexOf(I)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_15

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/text/Editable;->length()I

    move-result v25

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v26

    add-int v26, v26, p2

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_1c

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v26

    add-int v26, v26, p2

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 268
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_5

    .line 279
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 284
    .end local v8    # "addedText":Ljava/lang/String;
    .end local v16    # "errorString":Ljava/lang/String;
    :cond_1e
    if-lez p3, :cond_1f

    if-nez p4, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    move/from16 v25, v0

    if-nez v25, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1f

    .line 285
    new-instance v25, Landroid/os/Handler;

    invoke-direct/range {v25 .. v25}, Landroid/os/Handler;-><init>()V

    new-instance v26, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1$1;-><init>(Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;)V

    invoke-virtual/range {v25 .. v26}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 323
    :cond_1f
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 324
    .local v5, "_tmp":Ljava/lang/String;
    const/16 v25, 0x1

    move/from16 v0, p4

    move/from16 v1, v25

    if-lt v0, v1, :cond_2c

    const/16 v25, 0x20

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_2c

    sget-boolean v25, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mBlankCheckResult:Z

    if-nez v25, :cond_2c

    .line 325
    const/16 v25, 0x1

    sput-boolean v25, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mBlankCheckResult:Z

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0b0057

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_20

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    const-string v26, "="

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_25

    .line 328
    :cond_20
    const/16 v25, 0x3d

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0b0057

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    const/16 v26, 0x3d

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_23

    .line 331
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f0b0057

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setmResult(Ljava/lang/String;)V

    .line 337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 377
    :cond_22
    :goto_6
    const/16 v25, 0x0

    sput-boolean v25, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mBlankCheckResult:Z

    goto/16 :goto_1

    .line 340
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    move/from16 v25, v0

    const/high16 v26, 0x7f0b0000

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->error_check:I

    move/from16 v25, v0

    const v26, 0x7f0b000b

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_22

    .line 341
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 344
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getSelectionStart()I

    move-result v22

    .line 345
    .local v22, "startCursor":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getSelectionEnd()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v25

    if-ne v0, v1, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->length()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v25

    if-ge v0, v1, :cond_26

    const-string v25, "\n"

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    move/from16 v1, v22

    if-eq v0, v1, :cond_26

    .line 348
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    add-int/lit8 v26, v22, 0x1

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2

    .line 353
    :cond_26
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$300(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Z

    move-result v25

    if-nez v25, :cond_27

    const/16 v25, 0x20

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_2b

    .line 354
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$300(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Z

    move-result v25

    if-nez v25, :cond_29

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_29

    .line 355
    const-string v25, " "

    const-string v26, ""

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 357
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    .line 359
    .local v6, "a":I
    const/16 v25, 0x1

    move/from16 v0, v25

    if-le v6, v0, :cond_28

    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v25

    const/16 v26, 0x30

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_28

    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v25

    const/16 v26, 0x2e

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_28

    .line 360
    add-int/lit8 v22, v22, -0x1

    .line 362
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v10

    .line 363
    .local v10, "b":I
    sub-int v25, v6, v10

    add-int/lit8 v25, v25, -0x1

    sub-int v22, v22, v25

    .line 365
    .end local v6    # "a":I
    .end local v10    # "b":I
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/text/Editable;->length()I

    move-result v25

    move/from16 v0, v22

    move/from16 v1, v25

    if-gt v0, v1, :cond_2a

    if-ltz v22, :cond_2a

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto/16 :goto_6

    .line 349
    :catch_2
    move-exception v14

    .line 350
    .local v14, "e":Ljava/lang/IndexOutOfBoundsException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto/16 :goto_7

    .line 370
    .end local v14    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto/16 :goto_6

    .line 374
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    new-instance v26, Ljava/lang/StringBuilder;

    move-object/from16 v0, v26

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->setText(Ljava/lang/StringBuilder;Z)V

    goto/16 :goto_6

    .line 380
    .end local v22    # "startCursor":I
    :cond_2c
    add-int v25, p2, p4

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, v25

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 381
    .local v7, "addedTempText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$302(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 383
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v25

    if-eqz v25, :cond_2d

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_2f

    .line 384
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->getResult()Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 385
    .restart local v21    # "result":Ljava/lang/String;
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_2e

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_2e

    .line 387
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    .line 389
    .restart local v23    # "subAdd":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->autoTextSize(Ljava/lang/String;)V

    .line 390
    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOprator(C)Z

    move-result v25

    if-nez v25, :cond_2e

    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->isOnlyDigit(C)Z

    move-result v25

    if-nez v25, :cond_2e

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$302(Lcom/sec/android/app/popupcalculator/CalculatorEditText;Z)Z

    .line 395
    .end local v23    # "subAdd":Ljava/lang/String;
    :cond_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isError:Z
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$300(Lcom/sec/android/app/popupcalculator/CalculatorEditText;)Z

    move-result v25

    if-nez v25, :cond_2f

    const-string v25, "Error"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_2f

    .line 396
    const/16 v25, 0x1

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mLineCount:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$402(I)I

    .line 400
    .end local v21    # "result":Ljava/lang/String;
    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getLineCount()I

    move-result v25

    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mLineCount:I
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$400()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_5

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v25

    if-eqz v25, :cond_5

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getLineCount()I

    move-result v25

    if-eqz v25, :cond_5

    if-nez p2, :cond_5

    .line 403
    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mLineCount:I
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$400()I

    move-result v18

    .line 409
    .local v18, "mTempLineCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getLineCount()I

    move-result v25

    # setter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mLineCount:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$402(I)I

    .line 411
    # getter for: Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mLineCount:I
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->access$400()I

    move-result v25

    move/from16 v0, v18

    move/from16 v1, v25

    if-ge v0, v1, :cond_5

    .line 412
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-static {v7, v0}, Lcom/sec/android/app/popupcalculator/Cmyfunc;->whereLastTokenArithmetic(Ljava/lang/String;Z)I

    move-result v17

    .line 415
    .local v17, "lastToken":I
    if-lez v17, :cond_5

    .line 416
    move/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    .line 417
    .local v19, "mTempString":Ljava/lang/String;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "\n"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v17

    move/from16 v1, v26

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 421
    const-string v25, "\n\n"

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/sec/android/app/popupcalculator/EventHandler;->mSmallFontCheck:Z

    move/from16 v25, v0

    if-eqz v25, :cond_5

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 425
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-gt v0, v1, :cond_30

    .line 426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 429
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/popupcalculator/EventHandler;->setNewLineCheck()V

    goto/16 :goto_1

    .line 428
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto :goto_8

    .line 437
    .end local v5    # "_tmp":Ljava/lang/String;
    .end local v7    # "addedTempText":Ljava/lang/String;
    .end local v17    # "lastToken":I
    .end local v18    # "mTempLineCount":I
    .end local v19    # "mTempString":Ljava/lang/String;
    :cond_31
    add-int v25, p2, p4

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, v25

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 439
    .restart local v8    # "addedText":Ljava/lang/String;
    const-string v9, ""

    .line 441
    .local v9, "afterText":Ljava/lang/String;
    if-eqz p4, :cond_5

    .line 445
    move/from16 v0, p3

    move/from16 v1, p2

    if-gt v0, v1, :cond_33

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v25

    if-lez v25, :cond_33

    .line 446
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, p2

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    .line 447
    .local v20, "originalText":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v25

    if-lez v25, :cond_32

    .line 448
    add-int/lit8 v25, p2, 0x1

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v26

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 455
    :cond_32
    :goto_9
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    if-eqz v25, :cond_5

    .line 459
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_34

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->getFormula()Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventHandler;->setFormula(Ljava/lang/String;)V

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/sec/android/app/popupcalculator/EventHandler;->refreshText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 450
    .end local v20    # "originalText":Ljava/lang/String;
    :cond_33
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    .line 451
    .restart local v20    # "originalText":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v25

    if-lez v25, :cond_32

    .line 452
    const/16 v25, 0x1

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v26

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_9

    .line 474
    :cond_34
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_35

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 476
    const-string v20, ""

    .line 477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 498
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    if-eqz v25, :cond_5

    .line 501
    const-string v25, "0"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_38

    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e002a

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 480
    :cond_35
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_36

    .line 481
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->getEqualString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v20

    .line 482
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e002d

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 489
    :cond_36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    move-object/from16 v26, v0

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setText(Ljava/lang/CharSequence;)V

    .line 491
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_37

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Landroid/text/Editable;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 495
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_a

    .line 494
    :cond_37
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    goto :goto_b

    .line 505
    :cond_38
    const-string v25, "1"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_39

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0026

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 509
    :cond_39
    const-string v25, "2"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3a

    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0027

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 513
    :cond_3a
    const-string v25, "3"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3b

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0028

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 517
    :cond_3b
    const-string v25, "4"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3c

    .line 518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 519
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0022

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 521
    :cond_3c
    const-string v25, "5"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3d

    if-nez p3, :cond_3d

    .line 522
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 523
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0023

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 525
    :cond_3d
    const-string v25, "6"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3e

    .line 526
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0024

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 529
    :cond_3e
    const-string v25, "7"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3f

    .line 530
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 531
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e001e

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 533
    :cond_3f
    const-string v25, "8"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_40

    .line 534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e001f

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 536
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 537
    :cond_40
    const-string v25, "9"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_41

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0020

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 541
    :cond_41
    const-string v25, "*"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_42

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 543
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e001c

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 544
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 545
    :cond_42
    const-string v25, "-"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_43

    .line 546
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 547
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0021

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 548
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 549
    :cond_43
    const-string v25, "/"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_44

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e001b

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 553
    :cond_44
    const-string v25, "+"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_45

    .line 554
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0025

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 556
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 557
    :cond_45
    const-string v25, "("

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_46

    .line 558
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0029

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 561
    :cond_46
    const-string v25, ")"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_47

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0029

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 565
    :cond_47
    const-string v25, "."

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_48

    .line 566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 567
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e002b

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 569
    :cond_48
    const-string v25, "!"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_49

    .line 570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e002e

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 573
    :cond_49
    const-string v25, "%"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4a

    .line 574
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 575
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0030

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 576
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 577
    :cond_4a
    const-string v25, "^"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4b

    .line 578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    move-object/from16 v25, v0

    const v26, 0x7f0e0039

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/popupcalculator/EventListener;->onClick(I)V

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1

    .line 584
    :cond_4b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText$1;->this$0:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->isInitKeyboard:Z

    goto/16 :goto_1
.end method
