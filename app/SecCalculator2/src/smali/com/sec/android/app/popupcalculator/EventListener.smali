.class public Lcom/sec/android/app/popupcalculator/EventListener;
.super Ljava/lang/Object;
.source "EventListener.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/popupcalculator/EventListener$DelKeyEvent;
    }
.end annotation


# static fields
.field private static final CURRENT_DISPLAY_FILE:Ljava/lang/String; = "backup_dsp"

.field private static final DOUBLE_NEW_LINE:Ljava/lang/String; = "\n\n"

.field public static flagClearHis:Z


# instance fields
.field private but_id:[I

.field private historyListFragment:Lcom/sec/android/app/popupcalculator/HistoryListView;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mButtonCheck:Z

.field private mButtonSoundPool:Landroid/media/SoundPool;

.field private mClearBtn:Landroid/widget/Button;

.field private mClearHistoryButtonDisableColor:I

.field private mClearHistoryButtonEnableColor:I

.field private mContext:Landroid/content/Context;

.field private mCurrentBackkeySoundID:I

.field private mCurrentSoundID:I

.field private mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

.field private mHisScreen:Landroid/widget/EditText;

.field private mHisScreenParentScrollView:Landroid/widget/ScrollView;

.field private mHistory:Lcom/sec/android/app/popupcalculator/History;

.field private mNoHistory:Landroid/widget/EditText;

.field private mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

.field private mPanelHandle:Landroid/view/View;

.field private model:Ljava/lang/String;

.field private sb:Ljava/lang/StringBuilder;

.field private textLinear:Landroid/view/View;

.field private tts:Landroid/speech/tts/TextToSpeech;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/popupcalculator/EventListener;->flagClearHis:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonEnableColor:I

    .line 69
    iput v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonDisableColor:I

    .line 79
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->model:Ljava/lang/String;

    .line 96
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentSoundID:I

    .line 98
    iput v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentBackkeySoundID:I

    .line 102
    iput-boolean v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonCheck:Z

    .line 108
    const/16 v0, 0x23

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    return-void

    .line 108
    :array_0
    .array-data 4
        0x7f0e001a
        0x7f0e001e
        0x7f0e001f
        0x7f0e0020
        0x7f0e0025
        0x7f0e0021
        0x7f0e001d
        0x7f0e002b
        0x7f0e0022
        0x7f0e0023
        0x7f0e0024
        0x7f0e001c
        0x7f0e001b
        0x7f0e002a
        0x7f0e0026
        0x7f0e0027
        0x7f0e0028
        0x7f0e0029
        0x7f0e002c
        0x7f0e002d
        0x7f0e0031
        0x7f0e0032
        0x7f0e0033
        0x7f0e0034
        0x7f0e0035
        0x7f0e0036
        0x7f0e002e
        0x7f0e0037
        0x7f0e0038
        0x7f0e0039
        0x7f0e003a
        0x7f0e003b
        0x7f0e003c
        0x7f0e002f
        0x7f0e0030
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/EventListener;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/popupcalculator/EventListener;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/EventListener;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/popupcalculator/EventListener;)Lcom/sec/android/widgetapp/calculator/Panel;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/EventListener;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/popupcalculator/EventListener;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/EventListener;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreenParentScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/popupcalculator/EventListener;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupcalculator/EventListener;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    return-object v0
.end method

.method private clearSB()V
    .locals 3

    .prologue
    .line 246
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 247
    .local v0, "len":I
    if-nez v0, :cond_0

    .line 250
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private speakOut(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "isOverride"    # Z

    .prologue
    .line 804
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->mIsTalkBackSettingOn:Z

    if-eqz v0, :cond_0

    .line 805
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->tts:Landroid/speech/tts/TextToSpeech;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 806
    :cond_0
    return-void
.end method


# virtual methods
.method public clearHistoryList()V
    .locals 7

    .prologue
    .line 770
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventListener;->updateHistory()V

    .line 771
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mFragmentManager:Landroid/app/FragmentManager;

    if-eqz v4, :cond_0

    .line 772
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    .line 774
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 775
    .local v1, "fragTrans":Landroid/app/FragmentTransaction;
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 776
    .local v2, "mHistoryFrame":Landroid/widget/FrameLayout;
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 777
    new-instance v4, Lcom/sec/android/app/popupcalculator/HistoryListView;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/popupcalculator/HistoryListView;-><init>(Lcom/sec/android/app/popupcalculator/History;Lcom/sec/android/app/popupcalculator/EventHandler;)V

    iput-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->historyListFragment:Lcom/sec/android/app/popupcalculator/HistoryListView;

    .line 778
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getId()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->historyListFragment:Lcom/sec/android/app/popupcalculator/HistoryListView;

    invoke-virtual {v1, v4, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 779
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 780
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "backup_dsp"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 782
    .local v3, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 783
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "HistoryListOpenState"

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 784
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 787
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "fragTrans":Landroid/app/FragmentTransaction;
    .end local v2    # "mHistoryFrame":Landroid/widget/FrameLayout;
    .end local v3    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public initClick(I)V
    .locals 8
    .param p1, "id"    # I

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 268
    sput-boolean v4, Lcom/sec/android/app/popupcalculator/Calculator;->isEqualPressed:Z

    .line 269
    iget-boolean v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonCheck:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 456
    :goto_0
    return-void

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_effects_enabled"

    invoke-static {v0, v1, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v7, :cond_1

    sget-boolean v0, Lcom/sec/android/app/popupcalculator/Calculator;->isTouchsounded:Z

    if-nez v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 278
    const v0, 0x7f0e001d

    if-ne p1, v0, :cond_5

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentBackkeySoundID:I

    move v3, v2

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 286
    :cond_1
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 440
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->insert(Ljava/lang/StringBuilder;)V

    .line 443
    :cond_3
    const v0, 0x7f0e002c

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getTextForAccessibility()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getTextForAccessibility()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v7}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    .line 447
    :cond_4
    sparse-switch p1, :sswitch_data_0

    .line 455
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventListener;->clearSB()V

    goto :goto_0

    .line 281
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentSoundID:I

    move v3, v2

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto :goto_1

    .line 288
    :pswitch_0
    sput-boolean v7, Lcom/sec/android/app/popupcalculator/Calculator;->isEqualPressed:Z

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->onEnter()V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v7}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 299
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->clear()V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0033

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 303
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/EventHandler;->onBackspace()V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0032

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 307
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0045

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 311
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0038

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->decimalChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 317
    :pswitch_5
    sput-boolean v7, Lcom/sec/android/app/popupcalculator/Calculator;->isEqualPressed:Z

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "(\u2212"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 321
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "1\u00f7"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0025

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 325
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "abs("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b002e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 329
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0023

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 333
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0024

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 337
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0026

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 341
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0027

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 345
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0028

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 349
    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0029

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 353
    :pswitch_e
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b002a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 357
    :pswitch_f
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b002b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 361
    :pswitch_10
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b002c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 365
    :pswitch_11
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b002d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 369
    :pswitch_12
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b002f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 373
    :pswitch_13
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "\u2212"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 377
    :pswitch_14
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "\u00d7"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0042

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 381
    :pswitch_15
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "\u00f7"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0037

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 385
    :pswitch_16
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "\u221a("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b004d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 389
    :pswitch_17
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0047

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 393
    :pswitch_18
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b003d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 397
    :pswitch_19
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "sin("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 401
    :pswitch_1a
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "tan("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 405
    :pswitch_1b
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "cos("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0035

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 409
    :pswitch_1c
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "log("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0041

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 413
    :pswitch_1d
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "ln("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0040

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 417
    :pswitch_1e
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "e^("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b003c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 421
    :pswitch_1f
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "^(2)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0052

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 425
    :pswitch_20
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "^("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0053

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 429
    :pswitch_21
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "\u03c0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0048

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 433
    :pswitch_22
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    const-string v1, "e"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0039

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/sec/android/app/popupcalculator/EventListener;->speakOut(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 286
    :pswitch_data_0
    .packed-switch 0x7f0e001a
        :pswitch_1
        :pswitch_15
        :pswitch_14
        :pswitch_2
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_13
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_12
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_3
        :pswitch_8
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_18
        :pswitch_16
        :pswitch_17
        :pswitch_19
        :pswitch_1b
        :pswitch_1a
        :pswitch_1d
        :pswitch_1c
        :pswitch_6
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_7
        :pswitch_21
        :pswitch_22
    .end packed-switch

    .line 447
    :sswitch_data_0
    .sparse-switch
        0x7f0e0015 -> :sswitch_0
        0x7f0e0018 -> :sswitch_0
        0x7f0e002d -> :sswitch_0
    .end sparse-switch
.end method

.method public initializePanelSound(Landroid/content/Context;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    const v1, 0x7f060001

    invoke-virtual {v0, p1, v1, v3}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentSoundID:I

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, p1, v1, v3}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentBackkeySoundID:I

    .line 132
    return-void
.end method

.method public onClick(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 261
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 262
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupcalculator/EventListener;->initClick(I)V

    .line 264
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 254
    if-eqz p1, :cond_0

    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 256
    .local v0, "id":I
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupcalculator/EventListener;->initClick(I)V

    .line 258
    .end local v0    # "id":I
    :cond_0
    return-void
.end method

.method public onInit(I)V
    .locals 0
    .param p1, "status"    # I

    .prologue
    .line 801
    return-void
.end method

.method public onKeypressClick(C)V
    .locals 2
    .param p1, "sign"    # C

    .prologue
    .line 790
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/EventHandler;->insert(Ljava/lang/StringBuilder;)V

    .line 792
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/popupcalculator/EventListener;->clearSB()V

    .line 793
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 233
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 234
    .local v0, "id":I
    const v3, 0x7f0e001d

    if-eq v0, v3, :cond_0

    .line 240
    :goto_0
    return v1

    .line 238
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setBackSpaceTouch(Z)V

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    iget-object v3, v3, Lcom/sec/android/app/popupcalculator/EventHandler;->mBackSpaceHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    move v1, v2

    .line 240
    goto :goto_0
.end method

.method public onPanelClicked(Lcom/sec/android/widgetapp/calculator/Panel;)Z
    .locals 11
    .param p1, "panel"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 733
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sound_effects_enabled"

    invoke-static {v0, v1, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v10, :cond_0

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentSoundID:I

    const/high16 v6, 0x3f800000    # 1.0f

    move v3, v2

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 738
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    .line 740
    .local v8, "mImm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/calculator/Panel;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {v8, v0, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v0

    if-nez v0, :cond_3

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/calculator/Panel;->setAniEnd(Z)V

    .line 762
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/popupcalculator/EventListener;->flagClearHis:Z

    if-eqz v0, :cond_2

    .line 763
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventListener;->clearHistoryList()V

    .line 764
    sput-boolean v4, Lcom/sec/android/app/popupcalculator/EventListener;->flagClearHis:Z

    .line 766
    :cond_2
    return v10

    .line 748
    :cond_3
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I

    array-length v0, v0

    if-ge v7, v0, :cond_1

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I

    aget v1, v1, v7

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 750
    .local v9, "vw":Landroid/view/View;
    if-eqz v9, :cond_4

    .line 751
    invoke-virtual {v9, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 748
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public onPanelClosed(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 8
    .param p1, "panel"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 461
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 462
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0031

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 464
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    if-eqz v3, :cond_1

    .line 466
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v3}, Lcom/sec/android/app/popupcalculator/History;->isHistory()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 467
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 468
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 469
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setClickable(Z)V

    .line 470
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    iget v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonEnableColor:I

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setTextColor(I)V

    .line 477
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 479
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 480
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 481
    .local v1, "history_scroll":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 482
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 486
    .end local v1    # "history_scroll":Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->isFocusable()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 487
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->setClose()V

    .line 490
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 491
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 492
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 493
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->textLinear:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 495
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v0

    .line 496
    .local v0, "cursor":I
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v3

    if-ne v3, v6, :cond_7

    .line 497
    if-eqz v0, :cond_6

    .line 498
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 499
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setVisibility(I)V

    .line 509
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    if-nez v0, :cond_4

    move v0, v2

    .end local v0    # "cursor":I
    :cond_4
    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 511
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    if-nez v3, :cond_8

    .line 529
    :goto_2
    return-void

    .line 472
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 473
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 474
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 475
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    iget v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonDisableColor:I

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setTextColor(I)V

    goto/16 :goto_0

    .line 501
    .restart local v0    # "cursor":I
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setVisibility(I)V

    .line 502
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    const v4, 0x7f0b005e

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(I)V

    .line 503
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_1

    .line 506
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_1

    .line 514
    .end local v0    # "cursor":I
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->hasFocus()Z

    move-result v3

    if-nez v3, :cond_9

    .line 515
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 516
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 517
    iput-boolean v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonCheck:Z

    .line 518
    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/popupcalculator/EventHandler;->setBackSpaceTouch(Z)V

    .line 519
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    if-eqz v2, :cond_a

    .line 520
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setFocusable(Z)V

    .line 521
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 522
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setNextFocusDownId(I)V

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 528
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/popupcalculator/EventListener;->clearHistoryList()V

    goto :goto_2

    .line 525
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 526
    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_3
.end method

.method public onPanelClosedForConfig()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 635
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I

    array-length v5, v5

    if-ge v1, v5, :cond_1

    .line 636
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I

    aget v7, v7, v1

    invoke-virtual {v5, v7}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 637
    .local v4, "vw":Landroid/view/View;
    if-eqz v4, :cond_0

    .line 638
    invoke-virtual {v4, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 635
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 642
    .end local v4    # "vw":Landroid/view/View;
    :cond_1
    const/4 v3, 0x0

    .line 643
    .local v3, "start":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 644
    .local v2, "s":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->isHistoryScreen()Z

    move-result v5

    if-nez v5, :cond_3

    .line 645
    move v1, v3

    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget v5, v5, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    if-ge v1, v5, :cond_3

    .line 646
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/popupcalculator/History;->current(I)Lcom/sec/android/app/popupcalculator/HistoryEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getBase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 647
    const-string v5, "\n="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 648
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/popupcalculator/History;->current(I)Lcom/sec/android/app/popupcalculator/HistoryEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getEdited()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget v5, v5, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    add-int/lit8 v5, v5, -0x1

    if-ne v1, v5, :cond_2

    const-string v5, ""

    :goto_2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 645
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 649
    :cond_2
    const-string v5, "\n"

    goto :goto_2

    .line 651
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 652
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->textLinear:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 653
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setVisibility(I)V

    .line 654
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v0

    .line 655
    .local v0, "cursor":I
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    if-nez v0, :cond_4

    move v0, v6

    .end local v0    # "cursor":I
    :cond_4
    invoke-virtual {v5, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 656
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->requestFocus()Z

    .line 657
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    if-eqz v5, :cond_5

    .line 658
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 659
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setNextFocusUpId(I)V

    .line 663
    :goto_3
    return-void

    .line 661
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_3
.end method

.method public onPanelOpened(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 10
    .param p1, "panel"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 667
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 668
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const v6, 0x7f0b0030

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 671
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 672
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->but_id:[I

    aget v5, v5, v2

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 673
    .local v3, "vw":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 674
    invoke-virtual {v3, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 671
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 677
    .end local v3    # "vw":Landroid/view/View;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 678
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 679
    .local v1, "history_scroll":Landroid/view/View;
    if-eqz v1, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_3

    .line 680
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 684
    .end local v1    # "history_scroll":Landroid/view/View;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4, v7}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 685
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4, v7}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 686
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4, v9}, Landroid/widget/EditText;->setVisibility(I)V

    .line 687
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->textLinear:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 690
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->historyListFragment:Lcom/sec/android/app/popupcalculator/HistoryListView;

    if-eqz v4, :cond_4

    .line 691
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 692
    .local v0, "fragTrans":Landroid/app/FragmentTransaction;
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->historyListFragment:Lcom/sec/android/app/popupcalculator/HistoryListView;

    invoke-virtual {v0, v4}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 693
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 698
    .end local v0    # "fragTrans":Landroid/app/FragmentTransaction;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v4

    if-ne v4, v8, :cond_9

    .line 699
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    invoke-virtual {v4, v9}, Landroid/widget/EditText;->setVisibility(I)V

    .line 704
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    if-eqz v4, :cond_5

    .line 705
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 707
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-eqz v4, :cond_6

    .line 708
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v4, v8}, Lcom/sec/android/widgetapp/calculator/Panel;->setAniEnd(Z)V

    .line 712
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->hasSelection()Z

    move-result v4

    if-nez v4, :cond_7

    .line 713
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setSelection(I)V

    .line 716
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    if-nez v4, :cond_a

    .line 728
    :cond_8
    :goto_2
    return-void

    .line 701
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 719
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    move-result v4

    if-nez v4, :cond_b

    .line 720
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->requestFocus()Z

    .line 721
    :cond_b
    iput-boolean v7, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonCheck:Z

    .line 722
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 725
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxxhdp(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_c

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->is360Xxhdp(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_8

    .line 727
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/16 v5, 0xf

    const/16 v6, 0x27

    const/16 v7, 0x2a

    const/4 v8, 0x5

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setPadding(IIII)V

    goto :goto_2
.end method

.method public resetInstance()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 813
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    if-eqz v0, :cond_0

    .line 814
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 817
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    if-eqz v0, :cond_1

    .line 818
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->setOnPanelListener(Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;)V

    .line 819
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->resetInstance()V

    .line 820
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->removeAllViews()V

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->removeAllViewsInLayout()V

    .line 822
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 825
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    if-eqz v0, :cond_2

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 827
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 828
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->setFocusable(Z)V

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->resetInstance()V

    .line 831
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 834
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    .line 835
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    .line 838
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_4

    .line 839
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentSoundID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->unload(I)Z

    .line 840
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mCurrentBackkeySoundID:I

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->unload(I)Z

    .line 841
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 842
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mButtonSoundPool:Landroid/media/SoundPool;

    .line 845
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_5

    .line 846
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    .line 849
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_6

    .line 850
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mAudioManager:Landroid/media/AudioManager;

    .line 853
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_7

    .line 854
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 855
    iput-object v2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->tts:Landroid/speech/tts/TextToSpeech;

    .line 857
    :cond_7
    return-void
.end method

.method public setHandler(Landroid/content/Context;Lcom/sec/android/app/popupcalculator/EventHandler;Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;Landroid/view/View;Landroid/widget/Button;Landroid/view/View;Landroid/app/FragmentManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Lcom/sec/android/app/popupcalculator/EventHandler;
    .param p3, "panel"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p4, "display"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .param p5, "hisscr"    # Landroid/widget/EditText;
    .param p6, "history"    # Lcom/sec/android/app/popupcalculator/History;
    .param p7, "view"    # Landroid/view/View;
    .param p8, "btn"    # Landroid/widget/Button;
    .param p9, "_mPanelHandle"    # Landroid/view/View;
    .param p10, "fragmentManager"    # Landroid/app/FragmentManager;

    .prologue
    const/4 v1, 0x1

    .line 185
    iput-object p7, p0, Lcom/sec/android/app/popupcalculator/EventListener;->textLinear:Landroid/view/View;

    .line 186
    iput-object p2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 187
    iput-object p3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 188
    iput-object p6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    .line 189
    iput-object p5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    .line 190
    iput-object p4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 191
    iput-object p10, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mFragmentManager:Landroid/app/FragmentManager;

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->forceHideSoftInput(Z)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->forceHideSoftInput(Z)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-object p0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-object p2, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 199
    iput-object p8, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    .line 200
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonEnableColor:I

    .line 202
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonDisableColor:I

    .line 211
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mAudioManager:Landroid/media/AudioManager;

    .line 215
    iput-object p9, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    .line 217
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->tts:Landroid/speech/tts/TextToSpeech;

    .line 218
    return-void
.end method

.method public setHandler(Landroid/content/Context;Lcom/sec/android/app/popupcalculator/EventHandler;Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/app/popupcalculator/CalculatorEditText;Landroid/widget/EditText;Lcom/sec/android/app/popupcalculator/History;Landroid/view/View;Landroid/widget/Button;Landroid/view/View;Landroid/widget/EditText;Landroid/app/FragmentManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Lcom/sec/android/app/popupcalculator/EventHandler;
    .param p3, "panel"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p4, "display"    # Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    .param p5, "hisscr"    # Landroid/widget/EditText;
    .param p6, "history"    # Lcom/sec/android/app/popupcalculator/History;
    .param p7, "view"    # Landroid/view/View;
    .param p8, "btn"    # Landroid/widget/Button;
    .param p9, "_mPanelHandle"    # Landroid/view/View;
    .param p10, "nohistory"    # Landroid/widget/EditText;
    .param p11, "fragmentManager"    # Landroid/app/FragmentManager;

    .prologue
    const/4 v1, 0x1

    .line 146
    iput-object p7, p0, Lcom/sec/android/app/popupcalculator/EventListener;->textLinear:Landroid/view/View;

    .line 147
    iput-object p2, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 148
    iput-object p3, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 149
    iput-object p6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    .line 150
    iput-object p5, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    .line 151
    iput-object p10, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    .line 152
    iput-object p4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    .line 153
    iput-object p11, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mFragmentManager:Landroid/app/FragmentManager;

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->forceHideSoftInput(Z)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->forceHideSoftInput(Z)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-object p0, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    iput-object p2, v0, Lcom/sec/android/app/popupcalculator/CalculatorEditText;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    .line 161
    iput-object p8, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonEnableColor:I

    .line 164
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonDisableColor:I

    .line 173
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mAudioManager:Landroid/media/AudioManager;

    .line 177
    iput-object p9, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanelHandle:Landroid/view/View;

    .line 179
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/sec/android/app/popupcalculator/EventListener;->tts:Landroid/speech/tts/TextToSpeech;

    .line 180
    return-void
.end method

.method public setPanel(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 0
    .param p1, "panel"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 796
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;

    .line 797
    return-void
.end method

.method public updateHistory()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 536
    const/4 v3, 0x0

    .line 538
    .local v3, "start":I
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/sec/android/app/popupcalculator/EventHandler;->setUpdatingHistoryCheck(Ljava/lang/Boolean;)V

    .line 539
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    if-eqz v4, :cond_0

    .line 540
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/History;->isHistory()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 541
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 542
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setClickable(Z)V

    .line 543
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    iget v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonEnableColor:I

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setTextColor(I)V

    .line 550
    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 551
    .local v2, "s":Ljava/lang/StringBuilder;
    move v1, v3

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget v4, v4, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    if-ge v1, v4, :cond_3

    .line 552
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/popupcalculator/History;->current(I)Lcom/sec/android/app/popupcalculator/HistoryEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getBase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    const-string v4, "\n="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/popupcalculator/History;->current(I)Lcom/sec/android/app/popupcalculator/HistoryEntry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/popupcalculator/HistoryEntry;->getEdited()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHistory:Lcom/sec/android/app/popupcalculator/History;

    iget v4, v4, Lcom/sec/android/app/popupcalculator/History;->mPos:I

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_2

    const-string v4, ""

    :goto_2
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 545
    .end local v1    # "i":I
    .end local v2    # "s":Ljava/lang/StringBuilder;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 546
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setClickable(Z)V

    .line 547
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearBtn:Landroid/widget/Button;

    iget v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mClearHistoryButtonDisableColor:I

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 556
    .restart local v1    # "i":I
    .restart local v2    # "s":Ljava/lang/StringBuilder;
    :cond_2
    const-string v4, "\n\n"

    goto :goto_2

    .line 558
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 559
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_c

    .line 560
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKlimt(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v4

    if-ne v4, v8, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v7, :cond_5

    .line 565
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 566
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    invoke-virtual {v4, v9}, Landroid/widget/EditText;->setVisibility(I)V

    .line 568
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isBlack(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isTabletUnder10inch(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isHistoryColorChange(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v4

    if-ne v4, v8, :cond_b

    .line 572
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 589
    :cond_7
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    .line 590
    .local v0, "cursor":I
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    if-nez v0, :cond_8

    move v0, v5

    .end local v0    # "cursor":I
    :cond_8
    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 591
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isNew800dp_mdpi(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_9

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v4

    if-ne v4, v8, :cond_a

    .line 593
    :cond_9
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreenParentScrollView:Landroid/widget/ScrollView;

    .line 594
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/widget/ScrollView;

    iput-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreenParentScrollView:Landroid/widget/ScrollView;

    .line 595
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 596
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreenParentScrollView:Landroid/widget/ScrollView;

    if-eqz v4, :cond_a

    .line 597
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreenParentScrollView:Landroid/widget/ScrollView;

    new-instance v6, Lcom/sec/android/app/popupcalculator/EventListener$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/popupcalculator/EventListener$1;-><init>(Lcom/sec/android/app/popupcalculator/EventListener;)V

    const-wide/16 v8, 0x64

    invoke-virtual {v4, v6, v8, v9}, Landroid/widget/ScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 628
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/popupcalculator/EventHandler;->setUpdatingHistoryCheck(Ljava/lang/Boolean;)V

    .line 629
    return-void

    .line 574
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeHistoryColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 575
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_d

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMatisse(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_d

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKlimt(Landroid/content/Context;)I

    move-result v4

    if-eq v4, v8, :cond_d

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v4

    if-ne v4, v8, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v7, :cond_7

    .line 580
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    invoke-virtual {v4, v9}, Landroid/widget/EditText;->setVisibility(I)V

    .line 581
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    if-eqz v4, :cond_7

    .line 582
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    const v6, 0x7f0b005e

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setText(I)V

    .line 583
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mNoHistory:Landroid/widget/EditText;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setVisibility(I)V

    goto/16 :goto_3

    .line 587
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHisScreen:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/sec/android/app/popupcalculator/EventListener;->mHandler:Lcom/sec/android/app/popupcalculator/EventHandler;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/popupcalculator/EventHandler;->changeColor(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method
