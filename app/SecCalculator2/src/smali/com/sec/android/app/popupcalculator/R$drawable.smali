.class public final Lcom/sec/android/app/popupcalculator/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final arrow_selector:I = 0x7f020000

.field public static final bg:I = 0x7f020001

.field public static final bg_dark:I = 0x7f020002

.field public static final bg_landscape:I = 0x7f020003

.field public static final btn_background_color_selector:I = 0x7f020004

.field public static final btn_bg_01:I = 0x7f020005

.field public static final btn_bg_01_2d:I = 0x7f020006

.field public static final btn_bg_01_2d_focus:I = 0x7f020007

.field public static final btn_bg_01_2d_press:I = 0x7f020008

.field public static final btn_bg_01_focus:I = 0x7f020009

.field public static final btn_bg_01_l:I = 0x7f02000a

.field public static final btn_bg_01_l_focus:I = 0x7f02000b

.field public static final btn_bg_01_l_press:I = 0x7f02000c

.field public static final btn_bg_01_one:I = 0x7f02000d

.field public static final btn_bg_01_one_focus:I = 0x7f02000e

.field public static final btn_bg_01_one_press:I = 0x7f02000f

.field public static final btn_bg_01_press:I = 0x7f020010

.field public static final btn_bg_02:I = 0x7f020011

.field public static final btn_bg_02_2d:I = 0x7f020012

.field public static final btn_bg_02_2d_focus:I = 0x7f020013

.field public static final btn_bg_02_2d_press:I = 0x7f020014

.field public static final btn_bg_02_focus:I = 0x7f020015

.field public static final btn_bg_02_l:I = 0x7f020016

.field public static final btn_bg_02_l_focus:I = 0x7f020017

.field public static final btn_bg_02_l_press:I = 0x7f020018

.field public static final btn_bg_02_one:I = 0x7f020019

.field public static final btn_bg_02_one_focus:I = 0x7f02001a

.field public static final btn_bg_02_one_press:I = 0x7f02001b

.field public static final btn_bg_02_press:I = 0x7f02001c

.field public static final btn_bg_03:I = 0x7f02001d

.field public static final btn_bg_03_2d:I = 0x7f02001e

.field public static final btn_bg_03_2d_focus:I = 0x7f02001f

.field public static final btn_bg_03_2d_press:I = 0x7f020020

.field public static final btn_bg_03_focus:I = 0x7f020021

.field public static final btn_bg_03_l:I = 0x7f020022

.field public static final btn_bg_03_l_focus:I = 0x7f020023

.field public static final btn_bg_03_l_press:I = 0x7f020024

.field public static final btn_bg_03_one:I = 0x7f020025

.field public static final btn_bg_03_one_focus:I = 0x7f020026

.field public static final btn_bg_03_one_press:I = 0x7f020027

.field public static final btn_bg_03_press:I = 0x7f020028

.field public static final btn_bg_04:I = 0x7f020029

.field public static final btn_bg_04_2d:I = 0x7f02002a

.field public static final btn_bg_04_2d_focus:I = 0x7f02002b

.field public static final btn_bg_04_2d_press:I = 0x7f02002c

.field public static final btn_bg_04_focus:I = 0x7f02002d

.field public static final btn_bg_04_l:I = 0x7f02002e

.field public static final btn_bg_04_l_focus:I = 0x7f02002f

.field public static final btn_bg_04_l_press:I = 0x7f020030

.field public static final btn_bg_04_one:I = 0x7f020031

.field public static final btn_bg_04_one_focus:I = 0x7f020032

.field public static final btn_bg_04_one_press:I = 0x7f020033

.field public static final btn_bg_04_press:I = 0x7f020034

.field public static final btn_dark_bg_01:I = 0x7f020035

.field public static final btn_dark_bg_01_2d_focus:I = 0x7f020036

.field public static final btn_dark_bg_01_2d_press:I = 0x7f020037

.field public static final btn_dark_bg_02:I = 0x7f020038

.field public static final btn_dark_bg_02_2d_focus:I = 0x7f020039

.field public static final btn_dark_bg_02_2d_press:I = 0x7f02003a

.field public static final btn_dark_bg_03:I = 0x7f02003b

.field public static final btn_dark_bg_03_2d_focus:I = 0x7f02003c

.field public static final btn_dark_bg_03_2d_press:I = 0x7f02003d

.field public static final btn_dark_bg_04:I = 0x7f02003e

.field public static final btn_dark_bg_04_2d_focus:I = 0x7f02003f

.field public static final btn_dark_bg_04_2d_press:I = 0x7f020040

.field public static final btn_dark_land_bg_01:I = 0x7f020041

.field public static final btn_dark_land_bg_01_2d_focus:I = 0x7f020042

.field public static final btn_dark_land_bg_01_2d_press:I = 0x7f020043

.field public static final btn_dark_land_bg_02:I = 0x7f020044

.field public static final btn_dark_land_bg_02_2d_focus:I = 0x7f020045

.field public static final btn_dark_land_bg_02_2d_press:I = 0x7f020046

.field public static final btn_dark_land_bg_03:I = 0x7f020047

.field public static final btn_dark_land_bg_03_2d_focus:I = 0x7f020048

.field public static final btn_dark_land_bg_03_2d_press:I = 0x7f020049

.field public static final btn_dark_land_bg_04:I = 0x7f02004a

.field public static final btn_dark_land_bg_04_2d_focus:I = 0x7f02004b

.field public static final btn_dark_land_bg_04_2d_press:I = 0x7f02004c

.field public static final btn_dark_land_num_01:I = 0x7f02004d

.field public static final btn_dark_land_num_01_press:I = 0x7f02004e

.field public static final btn_dark_land_num_02:I = 0x7f02004f

.field public static final btn_dark_land_num_02_press:I = 0x7f020050

.field public static final btn_dark_land_num_03:I = 0x7f020051

.field public static final btn_dark_land_num_03_press:I = 0x7f020052

.field public static final btn_dark_land_num_04:I = 0x7f020053

.field public static final btn_dark_land_num_05:I = 0x7f020054

.field public static final btn_dark_land_num_06:I = 0x7f020055

.field public static final btn_dark_land_num_07:I = 0x7f020056

.field public static final btn_dark_land_num_08:I = 0x7f020057

.field public static final btn_dark_land_num_08_press:I = 0x7f020058

.field public static final btn_dark_land_num_09:I = 0x7f020059

.field public static final btn_dark_land_num_09_press:I = 0x7f02005a

.field public static final btn_dark_land_num_10:I = 0x7f02005b

.field public static final btn_dark_land_num_10_press:I = 0x7f02005c

.field public static final btn_dark_land_num_11:I = 0x7f02005d

.field public static final btn_dark_land_num_11_press:I = 0x7f02005e

.field public static final btn_dark_land_num_12:I = 0x7f02005f

.field public static final btn_dark_land_num_12_press:I = 0x7f020060

.field public static final btn_dark_land_num_13:I = 0x7f020061

.field public static final btn_dark_land_num_13_press:I = 0x7f020062

.field public static final btn_dark_land_num_14:I = 0x7f020063

.field public static final btn_dark_land_num_15:I = 0x7f020064

.field public static final btn_dark_land_num_15_press:I = 0x7f020065

.field public static final btn_dark_land_num_16:I = 0x7f020066

.field public static final btn_dark_land_num_16_press:I = 0x7f020067

.field public static final btn_dark_land_num_17:I = 0x7f020068

.field public static final btn_dark_land_num_17_press:I = 0x7f020069

.field public static final btn_dark_land_num_18:I = 0x7f02006a

.field public static final btn_dark_land_num_18_press:I = 0x7f02006b

.field public static final btn_dark_land_num_19:I = 0x7f02006c

.field public static final btn_dark_land_num_19_press:I = 0x7f02006d

.field public static final btn_dark_land_num_20:I = 0x7f02006e

.field public static final btn_dark_land_num_20_press:I = 0x7f02006f

.field public static final btn_dark_land_num_21:I = 0x7f020070

.field public static final btn_dark_land_num_22:I = 0x7f020071

.field public static final btn_dark_land_num_22_press:I = 0x7f020072

.field public static final btn_dark_land_num_23:I = 0x7f020073

.field public static final btn_dark_land_num_23_press:I = 0x7f020074

.field public static final btn_dark_land_num_24:I = 0x7f020075

.field public static final btn_dark_land_num_24_press:I = 0x7f020076

.field public static final btn_dark_land_num_25:I = 0x7f020077

.field public static final btn_dark_land_num_25_press:I = 0x7f020078

.field public static final btn_dark_land_num_26:I = 0x7f020079

.field public static final btn_dark_land_num_26_press:I = 0x7f02007a

.field public static final btn_dark_land_num_27:I = 0x7f02007b

.field public static final btn_dark_land_num_27_press:I = 0x7f02007c

.field public static final btn_dark_land_num_28:I = 0x7f02007d

.field public static final btn_dark_land_num_29:I = 0x7f02007e

.field public static final btn_dark_land_num_29_press:I = 0x7f02007f

.field public static final btn_dark_land_num_30:I = 0x7f020080

.field public static final btn_dark_land_num_30_press:I = 0x7f020081

.field public static final btn_dark_land_num_32:I = 0x7f020082

.field public static final btn_dark_land_num_32_press:I = 0x7f020083

.field public static final btn_dark_land_num_33:I = 0x7f020084

.field public static final btn_dark_land_num_33_press:I = 0x7f020085

.field public static final btn_dark_land_num_34:I = 0x7f020086

.field public static final btn_dark_land_num_34_press:I = 0x7f020087

.field public static final btn_dark_land_num_35:I = 0x7f020088

.field public static final btn_dark_num_01:I = 0x7f020089

.field public static final btn_dark_num_02:I = 0x7f02008a

.field public static final btn_dark_num_03:I = 0x7f02008b

.field public static final btn_dark_num_04:I = 0x7f02008c

.field public static final btn_dark_num_05:I = 0x7f02008d

.field public static final btn_dark_num_05_press:I = 0x7f02008e

.field public static final btn_dark_num_06:I = 0x7f02008f

.field public static final btn_dark_num_06_press:I = 0x7f020090

.field public static final btn_dark_num_07:I = 0x7f020091

.field public static final btn_dark_num_07_press:I = 0x7f020092

.field public static final btn_dark_num_08:I = 0x7f020093

.field public static final btn_dark_num_09:I = 0x7f020094

.field public static final btn_dark_num_09_press:I = 0x7f020095

.field public static final btn_dark_num_10:I = 0x7f020096

.field public static final btn_dark_num_10_press:I = 0x7f020097

.field public static final btn_dark_num_11:I = 0x7f020098

.field public static final btn_dark_num_11_press:I = 0x7f020099

.field public static final btn_dark_num_12:I = 0x7f02009a

.field public static final btn_dark_num_13:I = 0x7f02009b

.field public static final btn_dark_num_13_press:I = 0x7f02009c

.field public static final btn_dark_num_14:I = 0x7f02009d

.field public static final btn_dark_num_14_press:I = 0x7f02009e

.field public static final btn_dark_num_15:I = 0x7f02009f

.field public static final btn_dark_num_15_press:I = 0x7f0200a0

.field public static final btn_dark_num_16:I = 0x7f0200a1

.field public static final btn_dark_num_17:I = 0x7f0200a2

.field public static final btn_dark_num_17_press:I = 0x7f0200a3

.field public static final btn_dark_num_18:I = 0x7f0200a4

.field public static final btn_dark_num_18_press:I = 0x7f0200a5

.field public static final btn_dark_num_19:I = 0x7f0200a6

.field public static final btn_dark_num_19_press:I = 0x7f0200a7

.field public static final btn_dark_num_20:I = 0x7f0200a8

.field public static final btn_focus_01:I = 0x7f0200a9

.field public static final btn_focus_02:I = 0x7f0200aa

.field public static final btn_focus_03:I = 0x7f0200ab

.field public static final btn_focus_04:I = 0x7f0200ac

.field public static final btn_num_01:I = 0x7f0200ad

.field public static final btn_num_01_l:I = 0x7f0200ae

.field public static final btn_num_01_l_press:I = 0x7f0200af

.field public static final btn_num_01_one:I = 0x7f0200b0

.field public static final btn_num_01_press:I = 0x7f0200b1

.field public static final btn_num_02:I = 0x7f0200b2

.field public static final btn_num_02_l:I = 0x7f0200b3

.field public static final btn_num_02_l_press:I = 0x7f0200b4

.field public static final btn_num_02_one:I = 0x7f0200b5

.field public static final btn_num_02_press:I = 0x7f0200b6

.field public static final btn_num_03:I = 0x7f0200b7

.field public static final btn_num_03_l:I = 0x7f0200b8

.field public static final btn_num_03_l_press:I = 0x7f0200b9

.field public static final btn_num_03_one:I = 0x7f0200ba

.field public static final btn_num_03_press:I = 0x7f0200bb

.field public static final btn_num_04:I = 0x7f0200bc

.field public static final btn_num_04_l:I = 0x7f0200bd

.field public static final btn_num_04_one:I = 0x7f0200be

.field public static final btn_num_04_press:I = 0x7f0200bf

.field public static final btn_num_05:I = 0x7f0200c0

.field public static final btn_num_05_l:I = 0x7f0200c1

.field public static final btn_num_05_one:I = 0x7f0200c2

.field public static final btn_num_05_press:I = 0x7f0200c3

.field public static final btn_num_06:I = 0x7f0200c4

.field public static final btn_num_06_l:I = 0x7f0200c5

.field public static final btn_num_06_one:I = 0x7f0200c6

.field public static final btn_num_06_press:I = 0x7f0200c7

.field public static final btn_num_07:I = 0x7f0200c8

.field public static final btn_num_07_l:I = 0x7f0200c9

.field public static final btn_num_07_one:I = 0x7f0200ca

.field public static final btn_num_07_press:I = 0x7f0200cb

.field public static final btn_num_08:I = 0x7f0200cc

.field public static final btn_num_08_l:I = 0x7f0200cd

.field public static final btn_num_08_l_press:I = 0x7f0200ce

.field public static final btn_num_08_one:I = 0x7f0200cf

.field public static final btn_num_08_press:I = 0x7f0200d0

.field public static final btn_num_09:I = 0x7f0200d1

.field public static final btn_num_09_l:I = 0x7f0200d2

.field public static final btn_num_09_l_press:I = 0x7f0200d3

.field public static final btn_num_09_one:I = 0x7f0200d4

.field public static final btn_num_09_press:I = 0x7f0200d5

.field public static final btn_num_10:I = 0x7f0200d6

.field public static final btn_num_10_l:I = 0x7f0200d7

.field public static final btn_num_10_l_press:I = 0x7f0200d8

.field public static final btn_num_10_one:I = 0x7f0200d9

.field public static final btn_num_10_press:I = 0x7f0200da

.field public static final btn_num_11:I = 0x7f0200db

.field public static final btn_num_11_l:I = 0x7f0200dc

.field public static final btn_num_11_l_press:I = 0x7f0200dd

.field public static final btn_num_11_one:I = 0x7f0200de

.field public static final btn_num_11_press:I = 0x7f0200df

.field public static final btn_num_12:I = 0x7f0200e0

.field public static final btn_num_12_l:I = 0x7f0200e1

.field public static final btn_num_12_l_press:I = 0x7f0200e2

.field public static final btn_num_12_one:I = 0x7f0200e3

.field public static final btn_num_12_press:I = 0x7f0200e4

.field public static final btn_num_13:I = 0x7f0200e5

.field public static final btn_num_13_l:I = 0x7f0200e6

.field public static final btn_num_13_l_press:I = 0x7f0200e7

.field public static final btn_num_13_one:I = 0x7f0200e8

.field public static final btn_num_13_press:I = 0x7f0200e9

.field public static final btn_num_14:I = 0x7f0200ea

.field public static final btn_num_14_l:I = 0x7f0200eb

.field public static final btn_num_14_one:I = 0x7f0200ec

.field public static final btn_num_14_press:I = 0x7f0200ed

.field public static final btn_num_15:I = 0x7f0200ee

.field public static final btn_num_15_l:I = 0x7f0200ef

.field public static final btn_num_15_l_press:I = 0x7f0200f0

.field public static final btn_num_15_one:I = 0x7f0200f1

.field public static final btn_num_15_press:I = 0x7f0200f2

.field public static final btn_num_16:I = 0x7f0200f3

.field public static final btn_num_16_l:I = 0x7f0200f4

.field public static final btn_num_16_l_press:I = 0x7f0200f5

.field public static final btn_num_16_one:I = 0x7f0200f6

.field public static final btn_num_16_press:I = 0x7f0200f7

.field public static final btn_num_17:I = 0x7f0200f8

.field public static final btn_num_17_l:I = 0x7f0200f9

.field public static final btn_num_17_l_press:I = 0x7f0200fa

.field public static final btn_num_17_one:I = 0x7f0200fb

.field public static final btn_num_17_press:I = 0x7f0200fc

.field public static final btn_num_18:I = 0x7f0200fd

.field public static final btn_num_18_l:I = 0x7f0200fe

.field public static final btn_num_18_l_press:I = 0x7f0200ff

.field public static final btn_num_18_one:I = 0x7f020100

.field public static final btn_num_18_press:I = 0x7f020101

.field public static final btn_num_19:I = 0x7f020102

.field public static final btn_num_19_l:I = 0x7f020103

.field public static final btn_num_19_l_press:I = 0x7f020104

.field public static final btn_num_19_one:I = 0x7f020105

.field public static final btn_num_19_press:I = 0x7f020106

.field public static final btn_num_20:I = 0x7f020107

.field public static final btn_num_20_l:I = 0x7f020108

.field public static final btn_num_20_l_press:I = 0x7f020109

.field public static final btn_num_20_one:I = 0x7f02010a

.field public static final btn_num_20_press:I = 0x7f02010b

.field public static final btn_num_21_l:I = 0x7f02010c

.field public static final btn_num_22_l:I = 0x7f02010d

.field public static final btn_num_22_l_press:I = 0x7f02010e

.field public static final btn_num_23_l:I = 0x7f02010f

.field public static final btn_num_23_l_press:I = 0x7f020110

.field public static final btn_num_24_l:I = 0x7f020111

.field public static final btn_num_24_l_press:I = 0x7f020112

.field public static final btn_num_25_l:I = 0x7f020113

.field public static final btn_num_25_l_press:I = 0x7f020114

.field public static final btn_num_26_l:I = 0x7f020115

.field public static final btn_num_26_l_press:I = 0x7f020116

.field public static final btn_num_27_l:I = 0x7f020117

.field public static final btn_num_27_l_press:I = 0x7f020118

.field public static final btn_num_28_l:I = 0x7f020119

.field public static final btn_num_29_l:I = 0x7f02011a

.field public static final btn_num_29_l_press:I = 0x7f02011b

.field public static final btn_num_30_l:I = 0x7f02011c

.field public static final btn_num_30_l_press:I = 0x7f02011d

.field public static final btn_num_31:I = 0x7f02011e

.field public static final btn_num_31_l:I = 0x7f02011f

.field public static final btn_num_31_l_press:I = 0x7f020120

.field public static final btn_num_31_press:I = 0x7f020121

.field public static final btn_num_32_l:I = 0x7f020122

.field public static final btn_num_32_l_press:I = 0x7f020123

.field public static final btn_num_33_l:I = 0x7f020124

.field public static final btn_num_33_l_press:I = 0x7f020125

.field public static final btn_num_34_l:I = 0x7f020126

.field public static final btn_num_34_l_press:I = 0x7f020127

.field public static final btn_num_35_l:I = 0x7f020128

.field public static final btn_onehand_bg_01:I = 0x7f020129

.field public static final btn_onehand_bg_02:I = 0x7f02012a

.field public static final btn_onehand_bg_03:I = 0x7f02012b

.field public static final btn_onehand_bg_04:I = 0x7f02012c

.field public static final btn_onehand_left:I = 0x7f02012d

.field public static final btn_onehand_left_press:I = 0x7f02012e

.field public static final btn_onehand_num_01:I = 0x7f02012f

.field public static final btn_onehand_num_02:I = 0x7f020130

.field public static final btn_onehand_num_03:I = 0x7f020131

.field public static final btn_onehand_num_04:I = 0x7f020132

.field public static final btn_onehand_num_05:I = 0x7f020133

.field public static final btn_onehand_num_06:I = 0x7f020134

.field public static final btn_onehand_num_07:I = 0x7f020135

.field public static final btn_onehand_num_08:I = 0x7f020136

.field public static final btn_onehand_num_09:I = 0x7f020137

.field public static final btn_onehand_num_10:I = 0x7f020138

.field public static final btn_onehand_num_11:I = 0x7f020139

.field public static final btn_onehand_num_12:I = 0x7f02013a

.field public static final btn_onehand_num_13:I = 0x7f02013b

.field public static final btn_onehand_num_14:I = 0x7f02013c

.field public static final btn_onehand_num_15:I = 0x7f02013d

.field public static final btn_onehand_num_16:I = 0x7f02013e

.field public static final btn_onehand_num_17:I = 0x7f02013f

.field public static final btn_onehand_num_18:I = 0x7f020140

.field public static final btn_onehand_num_19:I = 0x7f020141

.field public static final btn_onehand_num_20:I = 0x7f020142

.field public static final btn_onehand_right:I = 0x7f020143

.field public static final btn_onehand_right_press:I = 0x7f020144

.field public static final btn_overflow_menu_selector:I = 0x7f020145

.field public static final btn_press:I = 0x7f020146

.field public static final btn_press_01:I = 0x7f020147

.field public static final btn_press_02:I = 0x7f020148

.field public static final btn_press_03:I = 0x7f020149

.field public static final btn_press_04:I = 0x7f02014a

.field public static final btn_press_1:I = 0x7f02014b

.field public static final button_bg_01:I = 0x7f02014c

.field public static final button_bg_01_l:I = 0x7f02014d

.field public static final button_bg_01_onehand:I = 0x7f02014e

.field public static final button_bg_02:I = 0x7f02014f

.field public static final button_bg_02_l:I = 0x7f020150

.field public static final button_bg_02_onehand:I = 0x7f020151

.field public static final button_bg_03:I = 0x7f020152

.field public static final button_bg_03_l:I = 0x7f020153

.field public static final button_bg_03_onehand:I = 0x7f020154

.field public static final button_bg_04:I = 0x7f020155

.field public static final button_bg_04_l:I = 0x7f020156

.field public static final button_bg_04_onehand:I = 0x7f020157

.field public static final button_digit_selector:I = 0x7f020158

.field public static final button_operator_selector:I = 0x7f020159

.field public static final calc_btn_bg_selector:I = 0x7f02015a

.field public static final calculator_action_bar_bg:I = 0x7f02015b

.field public static final calculator_arrow_down:I = 0x7f02015c

.field public static final calculator_arrow_down_h:I = 0x7f02015d

.field public static final calculator_arrow_down_selector:I = 0x7f02015e

.field public static final calculator_arrow_down_selector_h:I = 0x7f02015f

.field public static final calculator_arrow_up:I = 0x7f020160

.field public static final calculator_arrow_up_h:I = 0x7f020161

.field public static final calculator_arrow_up_selector:I = 0x7f020162

.field public static final calculator_arrow_up_selector_h:I = 0x7f020163

.field public static final calculator_bg:I = 0x7f020164

.field public static final calculator_bg_actionbar:I = 0x7f020165

.field public static final calculator_bg_h:I = 0x7f020166

.field public static final calculator_bg_w:I = 0x7f020167

.field public static final calculator_btn_03:I = 0x7f020168

.field public static final calculator_btn_17_2:I = 0x7f020169

.field public static final calculator_btn_bg:I = 0x7f02016a

.field public static final calculator_btn_bg_focus:I = 0x7f02016b

.field public static final calculator_btn_clear:I = 0x7f02016c

.field public static final calculator_btn_clear_focus:I = 0x7f02016d

.field public static final calculator_btn_clear_focus_m:I = 0x7f02016e

.field public static final calculator_btn_clear_focus_p:I = 0x7f02016f

.field public static final calculator_btn_clear_m:I = 0x7f020170

.field public static final calculator_btn_clear_p:I = 0x7f020171

.field public static final calculator_btn_clear_press:I = 0x7f020172

.field public static final calculator_btn_clear_press_p:I = 0x7f020173

.field public static final calculator_btn_function:I = 0x7f020174

.field public static final calculator_btn_function_focus:I = 0x7f020175

.field public static final calculator_btn_function_focus_m:I = 0x7f020176

.field public static final calculator_btn_function_focus_p:I = 0x7f020177

.field public static final calculator_btn_function_m:I = 0x7f020178

.field public static final calculator_btn_function_p:I = 0x7f020179

.field public static final calculator_btn_function_press:I = 0x7f02017a

.field public static final calculator_btn_function_press_p:I = 0x7f02017b

.field public static final calculator_btn_icon:I = 0x7f02017c

.field public static final calculator_btn_icon_focus:I = 0x7f02017d

.field public static final calculator_btn_icon_focus_m:I = 0x7f02017e

.field public static final calculator_btn_icon_focus_p:I = 0x7f02017f

.field public static final calculator_btn_icon_m:I = 0x7f020180

.field public static final calculator_btn_icon_p:I = 0x7f020181

.field public static final calculator_btn_icon_press:I = 0x7f020182

.field public static final calculator_btn_icon_press_m:I = 0x7f020183

.field public static final calculator_btn_icon_press_p:I = 0x7f020184

.field public static final calculator_btn_num:I = 0x7f020185

.field public static final calculator_btn_num_focus:I = 0x7f020186

.field public static final calculator_btn_num_focus_m:I = 0x7f020187

.field public static final calculator_btn_num_focus_p:I = 0x7f020188

.field public static final calculator_btn_num_m:I = 0x7f020189

.field public static final calculator_btn_num_p:I = 0x7f02018a

.field public static final calculator_btn_num_press:I = 0x7f02018b

.field public static final calculator_btn_num_press_m:I = 0x7f02018c

.field public static final calculator_btn_num_press_p:I = 0x7f02018d

.field public static final calculator_btn_press:I = 0x7f02018e

.field public static final calculator_clear_btn:I = 0x7f02018f

.field public static final calculator_clear_btn_press:I = 0x7f020190

.field public static final calculator_down_handle:I = 0x7f020191

.field public static final calculator_down_handle_focus:I = 0x7f020192

.field public static final calculator_down_handle_h:I = 0x7f020193

.field public static final calculator_down_handle_h_focus:I = 0x7f020194

.field public static final calculator_down_handle_h_press:I = 0x7f020195

.field public static final calculator_down_handle_press:I = 0x7f020196

.field public static final calculator_full:I = 0x7f020197

.field public static final calculator_full_focus:I = 0x7f020198

.field public static final calculator_full_focused:I = 0x7f020199

.field public static final calculator_full_focused_h:I = 0x7f02019a

.field public static final calculator_full_h:I = 0x7f02019b

.field public static final calculator_full_h_focus:I = 0x7f02019c

.field public static final calculator_function_icon_c:I = 0x7f02019d

.field public static final calculator_function_icon_c_h:I = 0x7f02019e

.field public static final calculator_function_icon_c_p:I = 0x7f02019f

.field public static final calculator_function_icon_c_p_press:I = 0x7f0201a0

.field public static final calculator_function_icon_c_press:I = 0x7f0201a1

.field public static final calculator_function_icon_c_press_h:I = 0x7f0201a2

.field public static final calculator_function_icon_m_c:I = 0x7f0201a3

.field public static final calculator_function_icon_m_c_h:I = 0x7f0201a4

.field public static final calculator_function_icon_m_c_h_s:I = 0x7f0201a5

.field public static final calculator_function_icon_m_c_press:I = 0x7f0201a6

.field public static final calculator_function_icon_m_c_press_h:I = 0x7f0201a7

.field public static final calculator_function_icon_m_c_press_h_s:I = 0x7f0201a8

.field public static final calculator_function_icon_m_c_press_s:I = 0x7f0201a9

.field public static final calculator_function_icon_m_c_s:I = 0x7f0201aa

.field public static final calculator_function_icon_m_sum:I = 0x7f0201ab

.field public static final calculator_function_icon_m_sum_h:I = 0x7f0201ac

.field public static final calculator_function_icon_m_sum_h_s:I = 0x7f0201ad

.field public static final calculator_function_icon_m_sum_press:I = 0x7f0201ae

.field public static final calculator_function_icon_m_sum_press_h:I = 0x7f0201af

.field public static final calculator_function_icon_m_sum_press_h_s:I = 0x7f0201b0

.field public static final calculator_function_icon_m_sum_press_s:I = 0x7f0201b1

.field public static final calculator_function_icon_m_sum_s:I = 0x7f0201b2

.field public static final calculator_function_icon_sum:I = 0x7f0201b3

.field public static final calculator_function_icon_sum_h:I = 0x7f0201b4

.field public static final calculator_function_icon_sum_p:I = 0x7f0201b5

.field public static final calculator_function_icon_sum_p_press:I = 0x7f0201b6

.field public static final calculator_function_icon_sum_press:I = 0x7f0201b7

.field public static final calculator_function_icon_sum_press_h:I = 0x7f0201b8

.field public static final calculator_handler_bg:I = 0x7f0201b9

.field public static final calculator_handler_bg_h:I = 0x7f0201ba

.field public static final calculator_handler_down:I = 0x7f0201bb

.field public static final calculator_handler_down_h:I = 0x7f0201bc

.field public static final calculator_handler_up:I = 0x7f0201bd

.field public static final calculator_handler_up_h:I = 0x7f0201be

.field public static final calculator_handwriting_bg:I = 0x7f0201bf

.field public static final calculator_handwriting_bg_v:I = 0x7f0201c0

.field public static final calculator_handwriting_button:I = 0x7f0201c1

.field public static final calculator_handwriting_button_ic_delete:I = 0x7f0201c2

.field public static final calculator_handwriting_button_ic_eraser:I = 0x7f0201c3

.field public static final calculator_handwriting_button_ic_pen:I = 0x7f0201c4

.field public static final calculator_handwriting_button_ic_redo:I = 0x7f0201c5

.field public static final calculator_handwriting_button_ic_undo:I = 0x7f0201c6

.field public static final calculator_handwriting_button_press_01:I = 0x7f0201c7

.field public static final calculator_handwriting_button_press_02:I = 0x7f0201c8

.field public static final calculator_handwriting_button_press_03:I = 0x7f0201c9

.field public static final calculator_header_background_holo_dark:I = 0x7f0201ca

.field public static final calculator_header_background_holo_light:I = 0x7f0201cb

.field public static final calculator_hide_icon_down_press:I = 0x7f0201cc

.field public static final calculator_hide_icon_up_press:I = 0x7f0201cd

.field public static final calculator_icon_abs:I = 0x7f0201ce

.field public static final calculator_icon_abs_h:I = 0x7f0201cf

.field public static final calculator_icon_abs_press:I = 0x7f0201d0

.field public static final calculator_icon_abs_press_h:I = 0x7f0201d1

.field public static final calculator_icon_back:I = 0x7f0201d2

.field public static final calculator_icon_back_h:I = 0x7f0201d3

.field public static final calculator_icon_back_p:I = 0x7f0201d4

.field public static final calculator_icon_back_p_press:I = 0x7f0201d5

.field public static final calculator_icon_back_press:I = 0x7f0201d6

.field public static final calculator_icon_back_press_h:I = 0x7f0201d7

.field public static final calculator_icon_bracket:I = 0x7f0201d8

.field public static final calculator_icon_bracket_h:I = 0x7f0201d9

.field public static final calculator_icon_bracket_p:I = 0x7f0201da

.field public static final calculator_icon_bracket_p_press:I = 0x7f0201db

.field public static final calculator_icon_bracket_press:I = 0x7f0201dc

.field public static final calculator_icon_bracket_press_h:I = 0x7f0201dd

.field public static final calculator_icon_comma:I = 0x7f0201de

.field public static final calculator_icon_comma_press:I = 0x7f0201df

.field public static final calculator_icon_cos:I = 0x7f0201e0

.field public static final calculator_icon_cos_h:I = 0x7f0201e1

.field public static final calculator_icon_cos_press:I = 0x7f0201e2

.field public static final calculator_icon_cos_press_h:I = 0x7f0201e3

.field public static final calculator_icon_division:I = 0x7f0201e4

.field public static final calculator_icon_division_h:I = 0x7f0201e5

.field public static final calculator_icon_division_p:I = 0x7f0201e6

.field public static final calculator_icon_division_p_press:I = 0x7f0201e7

.field public static final calculator_icon_division_press:I = 0x7f0201e8

.field public static final calculator_icon_division_press_h:I = 0x7f0201e9

.field public static final calculator_icon_dot:I = 0x7f0201ea

.field public static final calculator_icon_dot_h:I = 0x7f0201eb

.field public static final calculator_icon_dot_p:I = 0x7f0201ec

.field public static final calculator_icon_dot_p_press:I = 0x7f0201ed

.field public static final calculator_icon_dot_press:I = 0x7f0201ee

.field public static final calculator_icon_dot_press_h:I = 0x7f0201ef

.field public static final calculator_icon_e:I = 0x7f0201f0

.field public static final calculator_icon_e_h:I = 0x7f0201f1

.field public static final calculator_icon_e_press:I = 0x7f0201f2

.field public static final calculator_icon_e_press_h:I = 0x7f0201f3

.field public static final calculator_icon_ex:I = 0x7f0201f4

.field public static final calculator_icon_ex_h:I = 0x7f0201f5

.field public static final calculator_icon_ex_press:I = 0x7f0201f6

.field public static final calculator_icon_ex_press_h:I = 0x7f0201f7

.field public static final calculator_icon_factorial:I = 0x7f0201f8

.field public static final calculator_icon_factorial_h:I = 0x7f0201f9

.field public static final calculator_icon_factorial_press:I = 0x7f0201fa

.field public static final calculator_icon_factorial_press_h:I = 0x7f0201fb

.field public static final calculator_icon_in:I = 0x7f0201fc

.field public static final calculator_icon_in_h:I = 0x7f0201fd

.field public static final calculator_icon_in_press:I = 0x7f0201fe

.field public static final calculator_icon_in_press_h:I = 0x7f0201ff

.field public static final calculator_icon_log:I = 0x7f020200

.field public static final calculator_icon_log_h:I = 0x7f020201

.field public static final calculator_icon_log_press:I = 0x7f020202

.field public static final calculator_icon_log_press_h:I = 0x7f020203

.field public static final calculator_icon_m_abs:I = 0x7f020204

.field public static final calculator_icon_m_abs_press:I = 0x7f020205

.field public static final calculator_icon_m_abs_press_s:I = 0x7f020206

.field public static final calculator_icon_m_abs_s:I = 0x7f020207

.field public static final calculator_icon_m_back:I = 0x7f020208

.field public static final calculator_icon_m_back_h:I = 0x7f020209

.field public static final calculator_icon_m_back_h_s:I = 0x7f02020a

.field public static final calculator_icon_m_back_press:I = 0x7f02020b

.field public static final calculator_icon_m_back_press_h:I = 0x7f02020c

.field public static final calculator_icon_m_back_press_h_s:I = 0x7f02020d

.field public static final calculator_icon_m_back_press_s:I = 0x7f02020e

.field public static final calculator_icon_m_back_s:I = 0x7f02020f

.field public static final calculator_icon_m_bracket:I = 0x7f020210

.field public static final calculator_icon_m_bracket_h:I = 0x7f020211

.field public static final calculator_icon_m_bracket_h_s:I = 0x7f020212

.field public static final calculator_icon_m_bracket_press:I = 0x7f020213

.field public static final calculator_icon_m_bracket_press_h:I = 0x7f020214

.field public static final calculator_icon_m_bracket_press_h_s:I = 0x7f020215

.field public static final calculator_icon_m_bracket_press_s:I = 0x7f020216

.field public static final calculator_icon_m_bracket_s:I = 0x7f020217

.field public static final calculator_icon_m_cos:I = 0x7f020218

.field public static final calculator_icon_m_cos_press:I = 0x7f020219

.field public static final calculator_icon_m_cos_press_s:I = 0x7f02021a

.field public static final calculator_icon_m_cos_s:I = 0x7f02021b

.field public static final calculator_icon_m_division:I = 0x7f02021c

.field public static final calculator_icon_m_division_h:I = 0x7f02021d

.field public static final calculator_icon_m_division_h_s:I = 0x7f02021e

.field public static final calculator_icon_m_division_press:I = 0x7f02021f

.field public static final calculator_icon_m_division_press_h:I = 0x7f020220

.field public static final calculator_icon_m_division_press_h_s:I = 0x7f020221

.field public static final calculator_icon_m_division_press_s:I = 0x7f020222

.field public static final calculator_icon_m_division_s:I = 0x7f020223

.field public static final calculator_icon_m_dot:I = 0x7f020224

.field public static final calculator_icon_m_dot_h:I = 0x7f020225

.field public static final calculator_icon_m_dot_h_s:I = 0x7f020226

.field public static final calculator_icon_m_dot_press:I = 0x7f020227

.field public static final calculator_icon_m_dot_press_h:I = 0x7f020228

.field public static final calculator_icon_m_dot_press_h_s:I = 0x7f020229

.field public static final calculator_icon_m_dot_press_s:I = 0x7f02022a

.field public static final calculator_icon_m_dot_s:I = 0x7f02022b

.field public static final calculator_icon_m_e:I = 0x7f02022c

.field public static final calculator_icon_m_e_press:I = 0x7f02022d

.field public static final calculator_icon_m_e_press_s:I = 0x7f02022e

.field public static final calculator_icon_m_e_s:I = 0x7f02022f

.field public static final calculator_icon_m_ex:I = 0x7f020230

.field public static final calculator_icon_m_ex_press:I = 0x7f020231

.field public static final calculator_icon_m_ex_press_s:I = 0x7f020232

.field public static final calculator_icon_m_ex_s:I = 0x7f020233

.field public static final calculator_icon_m_factorial:I = 0x7f020234

.field public static final calculator_icon_m_factorial_press:I = 0x7f020235

.field public static final calculator_icon_m_factorial_press_s:I = 0x7f020236

.field public static final calculator_icon_m_factorial_s:I = 0x7f020237

.field public static final calculator_icon_m_in:I = 0x7f020238

.field public static final calculator_icon_m_in_press:I = 0x7f020239

.field public static final calculator_icon_m_in_press_s:I = 0x7f02023a

.field public static final calculator_icon_m_in_s:I = 0x7f02023b

.field public static final calculator_icon_m_log:I = 0x7f02023c

.field public static final calculator_icon_m_log_press:I = 0x7f02023d

.field public static final calculator_icon_m_log_press_s:I = 0x7f02023e

.field public static final calculator_icon_m_log_s:I = 0x7f02023f

.field public static final calculator_icon_m_multiplication:I = 0x7f020240

.field public static final calculator_icon_m_multiplication_h:I = 0x7f020241

.field public static final calculator_icon_m_multiplication_h_s:I = 0x7f020242

.field public static final calculator_icon_m_multiplication_press:I = 0x7f020243

.field public static final calculator_icon_m_multiplication_press_h:I = 0x7f020244

.field public static final calculator_icon_m_multiplication_press_h_s:I = 0x7f020245

.field public static final calculator_icon_m_multiplication_press_s:I = 0x7f020246

.field public static final calculator_icon_m_multiplication_s:I = 0x7f020247

.field public static final calculator_icon_m_percent:I = 0x7f020248

.field public static final calculator_icon_m_percent_press:I = 0x7f020249

.field public static final calculator_icon_m_percent_press_s:I = 0x7f02024a

.field public static final calculator_icon_m_percent_s:I = 0x7f02024b

.field public static final calculator_icon_m_pie:I = 0x7f02024c

.field public static final calculator_icon_m_pie_press:I = 0x7f02024d

.field public static final calculator_icon_m_pie_press_s:I = 0x7f02024e

.field public static final calculator_icon_m_pie_s:I = 0x7f02024f

.field public static final calculator_icon_m_plus:I = 0x7f020250

.field public static final calculator_icon_m_plus_h:I = 0x7f020251

.field public static final calculator_icon_m_plus_h_s:I = 0x7f020252

.field public static final calculator_icon_m_plus_press:I = 0x7f020253

.field public static final calculator_icon_m_plus_press_h:I = 0x7f020254

.field public static final calculator_icon_m_plus_press_h_s:I = 0x7f020255

.field public static final calculator_icon_m_plus_press_s:I = 0x7f020256

.field public static final calculator_icon_m_plus_s:I = 0x7f020257

.field public static final calculator_icon_m_plusminus:I = 0x7f020258

.field public static final calculator_icon_m_plusminus_h:I = 0x7f020259

.field public static final calculator_icon_m_plusminus_press:I = 0x7f02025a

.field public static final calculator_icon_m_plusminus_press_h:I = 0x7f02025b

.field public static final calculator_icon_m_plusminus_press_s:I = 0x7f02025c

.field public static final calculator_icon_m_plusminus_s:I = 0x7f02025d

.field public static final calculator_icon_m_root:I = 0x7f02025e

.field public static final calculator_icon_m_root_press:I = 0x7f02025f

.field public static final calculator_icon_m_root_press_s:I = 0x7f020260

.field public static final calculator_icon_m_root_s:I = 0x7f020261

.field public static final calculator_icon_m_sin:I = 0x7f020262

.field public static final calculator_icon_m_sin_press:I = 0x7f020263

.field public static final calculator_icon_m_sin_press_s:I = 0x7f020264

.field public static final calculator_icon_m_sin_s:I = 0x7f020265

.field public static final calculator_icon_m_subtraction:I = 0x7f020266

.field public static final calculator_icon_m_subtraction_h:I = 0x7f020267

.field public static final calculator_icon_m_subtraction_h_s:I = 0x7f020268

.field public static final calculator_icon_m_subtraction_press:I = 0x7f020269

.field public static final calculator_icon_m_subtraction_press_h:I = 0x7f02026a

.field public static final calculator_icon_m_subtraction_press_h_s:I = 0x7f02026b

.field public static final calculator_icon_m_subtraction_press_s:I = 0x7f02026c

.field public static final calculator_icon_m_subtraction_s:I = 0x7f02026d

.field public static final calculator_icon_m_tan:I = 0x7f02026e

.field public static final calculator_icon_m_tan_press:I = 0x7f02026f

.field public static final calculator_icon_m_tan_press_s:I = 0x7f020270

.field public static final calculator_icon_m_tan_s:I = 0x7f020271

.field public static final calculator_icon_m_throughput:I = 0x7f020272

.field public static final calculator_icon_m_throughput_press:I = 0x7f020273

.field public static final calculator_icon_m_throughput_press_s:I = 0x7f020274

.field public static final calculator_icon_m_throughput_s:I = 0x7f020275

.field public static final calculator_icon_m_x2:I = 0x7f020276

.field public static final calculator_icon_m_x2_press:I = 0x7f020277

.field public static final calculator_icon_m_x2_press_s:I = 0x7f020278

.field public static final calculator_icon_m_x2_s:I = 0x7f020279

.field public static final calculator_icon_m_xy:I = 0x7f02027a

.field public static final calculator_icon_m_xy_press:I = 0x7f02027b

.field public static final calculator_icon_m_xy_press_s:I = 0x7f02027c

.field public static final calculator_icon_m_xy_s:I = 0x7f02027d

.field public static final calculator_icon_multiplication:I = 0x7f02027e

.field public static final calculator_icon_multiplication_h:I = 0x7f02027f

.field public static final calculator_icon_multiplication_p:I = 0x7f020280

.field public static final calculator_icon_multiplication_p_press:I = 0x7f020281

.field public static final calculator_icon_multiplication_press:I = 0x7f020282

.field public static final calculator_icon_multiplication_press_h:I = 0x7f020283

.field public static final calculator_icon_percent:I = 0x7f020284

.field public static final calculator_icon_percent_h:I = 0x7f020285

.field public static final calculator_icon_percent_press:I = 0x7f020286

.field public static final calculator_icon_percent_press_h:I = 0x7f020287

.field public static final calculator_icon_pie:I = 0x7f020288

.field public static final calculator_icon_pie_h:I = 0x7f020289

.field public static final calculator_icon_pie_press:I = 0x7f02028a

.field public static final calculator_icon_pie_press_h:I = 0x7f02028b

.field public static final calculator_icon_plus:I = 0x7f02028c

.field public static final calculator_icon_plus_h:I = 0x7f02028d

.field public static final calculator_icon_plus_p:I = 0x7f02028e

.field public static final calculator_icon_plus_p_press:I = 0x7f02028f

.field public static final calculator_icon_plus_press:I = 0x7f020290

.field public static final calculator_icon_plus_press_h:I = 0x7f020291

.field public static final calculator_icon_plusminus:I = 0x7f020292

.field public static final calculator_icon_plusminus_h:I = 0x7f020293

.field public static final calculator_icon_plusminus_p:I = 0x7f020294

.field public static final calculator_icon_plusminus_p_press:I = 0x7f020295

.field public static final calculator_icon_plusminus_press:I = 0x7f020296

.field public static final calculator_icon_plusminus_press_h:I = 0x7f020297

.field public static final calculator_icon_root:I = 0x7f020298

.field public static final calculator_icon_root_h:I = 0x7f020299

.field public static final calculator_icon_root_press:I = 0x7f02029a

.field public static final calculator_icon_root_press_h:I = 0x7f02029b

.field public static final calculator_icon_sin:I = 0x7f02029c

.field public static final calculator_icon_sin_h:I = 0x7f02029d

.field public static final calculator_icon_sin_press:I = 0x7f02029e

.field public static final calculator_icon_sin_press_h:I = 0x7f02029f

.field public static final calculator_icon_subtraction:I = 0x7f0202a0

.field public static final calculator_icon_subtraction_h:I = 0x7f0202a1

.field public static final calculator_icon_subtraction_p:I = 0x7f0202a2

.field public static final calculator_icon_subtraction_p_press:I = 0x7f0202a3

.field public static final calculator_icon_subtraction_press:I = 0x7f0202a4

.field public static final calculator_icon_subtraction_press_h:I = 0x7f0202a5

.field public static final calculator_icon_tan:I = 0x7f0202a6

.field public static final calculator_icon_tan_h:I = 0x7f0202a7

.field public static final calculator_icon_tan_press:I = 0x7f0202a8

.field public static final calculator_icon_tan_press_h:I = 0x7f0202a9

.field public static final calculator_icon_throughput:I = 0x7f0202aa

.field public static final calculator_icon_throughput_h:I = 0x7f0202ab

.field public static final calculator_icon_throughput_press:I = 0x7f0202ac

.field public static final calculator_icon_throughput_press_h:I = 0x7f0202ad

.field public static final calculator_icon_x2:I = 0x7f0202ae

.field public static final calculator_icon_x2_h:I = 0x7f0202af

.field public static final calculator_icon_x2_press:I = 0x7f0202b0

.field public static final calculator_icon_x2_press_h:I = 0x7f0202b1

.field public static final calculator_icon_xy:I = 0x7f0202b2

.field public static final calculator_icon_xy_h:I = 0x7f0202b3

.field public static final calculator_icon_xy_press:I = 0x7f0202b4

.field public static final calculator_icon_xy_press_h:I = 0x7f0202b5

.field public static final calculator_keypad_bg:I = 0x7f0202b6

.field public static final calculator_keypad_bg_h:I = 0x7f0202b7

.field public static final calculator_l_btn_06:I = 0x7f0202b8

.field public static final calculator_l_btn_32:I = 0x7f0202b9

.field public static final calculator_l_btn_32_2:I = 0x7f0202ba

.field public static final calculator_lcd_bg:I = 0x7f0202bb

.field public static final calculator_lcd_bg_black:I = 0x7f0202bc

.field public static final calculator_lcd_bg_black_nokeypad:I = 0x7f0202bd

.field public static final calculator_lcd_bg_bright:I = 0x7f0202be

.field public static final calculator_lcd_bg_m:I = 0x7f0202bf

.field public static final calculator_left_arrow_selector_h:I = 0x7f0202c0

.field public static final calculator_left_handle_h:I = 0x7f0202c1

.field public static final calculator_left_handle_h_focus:I = 0x7f0202c2

.field public static final calculator_left_handle_h_press:I = 0x7f0202c3

.field public static final calculator_list_background_holo_dark:I = 0x7f0202c4

.field public static final calculator_list_background_holo_dark_h:I = 0x7f0202c5

.field public static final calculator_moreoverflow_n:I = 0x7f0202c6

.field public static final calculator_moreoverflow_p:I = 0x7f0202c7

.field public static final calculator_multiwindow_bg_actionbar:I = 0x7f0202c8

.field public static final calculator_multiwindow_bg_actionbar_h:I = 0x7f0202c9

.field public static final calculator_multiwindow_bg_under_actionbar:I = 0x7f0202ca

.field public static final calculator_multiwindow_bg_under_actionbar_h:I = 0x7f0202cb

.field public static final calculator_multiwindow_full_down:I = 0x7f0202cc

.field public static final calculator_multiwindow_full_down_h:I = 0x7f0202cd

.field public static final calculator_multiwindow_full_down_h_press:I = 0x7f0202ce

.field public static final calculator_multiwindow_full_down_press:I = 0x7f0202cf

.field public static final calculator_multiwindow_full_up:I = 0x7f0202d0

.field public static final calculator_multiwindow_full_up_h:I = 0x7f0202d1

.field public static final calculator_multiwindow_full_up_h_press:I = 0x7f0202d2

.field public static final calculator_multiwindow_full_up_press:I = 0x7f0202d3

.field public static final calculator_multiwindow_keypad_bg:I = 0x7f0202d4

.field public static final calculator_multiwindow_keypad_bg_h:I = 0x7f0202d5

.field public static final calculator_number_icon_00:I = 0x7f0202d6

.field public static final calculator_number_icon_00_h:I = 0x7f0202d7

.field public static final calculator_number_icon_00_p:I = 0x7f0202d8

.field public static final calculator_number_icon_00_p_press:I = 0x7f0202d9

.field public static final calculator_number_icon_00_press:I = 0x7f0202da

.field public static final calculator_number_icon_00_press_h:I = 0x7f0202db

.field public static final calculator_number_icon_01:I = 0x7f0202dc

.field public static final calculator_number_icon_01_h:I = 0x7f0202dd

.field public static final calculator_number_icon_01_p:I = 0x7f0202de

.field public static final calculator_number_icon_01_p_press:I = 0x7f0202df

.field public static final calculator_number_icon_01_press:I = 0x7f0202e0

.field public static final calculator_number_icon_01_press_h:I = 0x7f0202e1

.field public static final calculator_number_icon_02:I = 0x7f0202e2

.field public static final calculator_number_icon_02_h:I = 0x7f0202e3

.field public static final calculator_number_icon_02_p:I = 0x7f0202e4

.field public static final calculator_number_icon_02_p_press:I = 0x7f0202e5

.field public static final calculator_number_icon_02_press:I = 0x7f0202e6

.field public static final calculator_number_icon_02_press_h:I = 0x7f0202e7

.field public static final calculator_number_icon_03:I = 0x7f0202e8

.field public static final calculator_number_icon_03_h:I = 0x7f0202e9

.field public static final calculator_number_icon_03_p:I = 0x7f0202ea

.field public static final calculator_number_icon_03_p_press:I = 0x7f0202eb

.field public static final calculator_number_icon_03_press:I = 0x7f0202ec

.field public static final calculator_number_icon_03_press_h:I = 0x7f0202ed

.field public static final calculator_number_icon_04:I = 0x7f0202ee

.field public static final calculator_number_icon_04_h:I = 0x7f0202ef

.field public static final calculator_number_icon_04_p:I = 0x7f0202f0

.field public static final calculator_number_icon_04_p_press:I = 0x7f0202f1

.field public static final calculator_number_icon_04_press:I = 0x7f0202f2

.field public static final calculator_number_icon_04_press_h:I = 0x7f0202f3

.field public static final calculator_number_icon_05:I = 0x7f0202f4

.field public static final calculator_number_icon_05_h:I = 0x7f0202f5

.field public static final calculator_number_icon_05_p:I = 0x7f0202f6

.field public static final calculator_number_icon_05_p_press:I = 0x7f0202f7

.field public static final calculator_number_icon_05_press:I = 0x7f0202f8

.field public static final calculator_number_icon_05_press_h:I = 0x7f0202f9

.field public static final calculator_number_icon_06:I = 0x7f0202fa

.field public static final calculator_number_icon_06_h:I = 0x7f0202fb

.field public static final calculator_number_icon_06_p:I = 0x7f0202fc

.field public static final calculator_number_icon_06_p_press:I = 0x7f0202fd

.field public static final calculator_number_icon_06_press:I = 0x7f0202fe

.field public static final calculator_number_icon_06_press_h:I = 0x7f0202ff

.field public static final calculator_number_icon_07:I = 0x7f020300

.field public static final calculator_number_icon_07_h:I = 0x7f020301

.field public static final calculator_number_icon_07_p:I = 0x7f020302

.field public static final calculator_number_icon_07_p_press:I = 0x7f020303

.field public static final calculator_number_icon_07_press:I = 0x7f020304

.field public static final calculator_number_icon_07_press_h:I = 0x7f020305

.field public static final calculator_number_icon_08:I = 0x7f020306

.field public static final calculator_number_icon_08_h:I = 0x7f020307

.field public static final calculator_number_icon_08_p:I = 0x7f020308

.field public static final calculator_number_icon_08_p_press:I = 0x7f020309

.field public static final calculator_number_icon_08_press:I = 0x7f02030a

.field public static final calculator_number_icon_08_press_h:I = 0x7f02030b

.field public static final calculator_number_icon_09:I = 0x7f02030c

.field public static final calculator_number_icon_09_h:I = 0x7f02030d

.field public static final calculator_number_icon_09_p:I = 0x7f02030e

.field public static final calculator_number_icon_09_p_press:I = 0x7f02030f

.field public static final calculator_number_icon_09_press:I = 0x7f020310

.field public static final calculator_number_icon_09_press_h:I = 0x7f020311

.field public static final calculator_number_icon_m_00:I = 0x7f020312

.field public static final calculator_number_icon_m_00_h:I = 0x7f020313

.field public static final calculator_number_icon_m_00_h_s:I = 0x7f020314

.field public static final calculator_number_icon_m_00_press:I = 0x7f020315

.field public static final calculator_number_icon_m_00_press_h:I = 0x7f020316

.field public static final calculator_number_icon_m_00_press_h_s:I = 0x7f020317

.field public static final calculator_number_icon_m_00_press_s:I = 0x7f020318

.field public static final calculator_number_icon_m_00_s:I = 0x7f020319

.field public static final calculator_number_icon_m_01:I = 0x7f02031a

.field public static final calculator_number_icon_m_01_h:I = 0x7f02031b

.field public static final calculator_number_icon_m_01_h_s:I = 0x7f02031c

.field public static final calculator_number_icon_m_01_press:I = 0x7f02031d

.field public static final calculator_number_icon_m_01_press_h:I = 0x7f02031e

.field public static final calculator_number_icon_m_01_press_h_s:I = 0x7f02031f

.field public static final calculator_number_icon_m_01_press_s:I = 0x7f020320

.field public static final calculator_number_icon_m_01_s:I = 0x7f020321

.field public static final calculator_number_icon_m_02:I = 0x7f020322

.field public static final calculator_number_icon_m_02_h:I = 0x7f020323

.field public static final calculator_number_icon_m_02_h_s:I = 0x7f020324

.field public static final calculator_number_icon_m_02_press:I = 0x7f020325

.field public static final calculator_number_icon_m_02_press_h:I = 0x7f020326

.field public static final calculator_number_icon_m_02_press_h_s:I = 0x7f020327

.field public static final calculator_number_icon_m_02_press_s:I = 0x7f020328

.field public static final calculator_number_icon_m_02_s:I = 0x7f020329

.field public static final calculator_number_icon_m_03:I = 0x7f02032a

.field public static final calculator_number_icon_m_03_h:I = 0x7f02032b

.field public static final calculator_number_icon_m_03_h_s:I = 0x7f02032c

.field public static final calculator_number_icon_m_03_press:I = 0x7f02032d

.field public static final calculator_number_icon_m_03_press_h:I = 0x7f02032e

.field public static final calculator_number_icon_m_03_press_h_s:I = 0x7f02032f

.field public static final calculator_number_icon_m_03_press_s:I = 0x7f020330

.field public static final calculator_number_icon_m_03_s:I = 0x7f020331

.field public static final calculator_number_icon_m_04:I = 0x7f020332

.field public static final calculator_number_icon_m_04_h:I = 0x7f020333

.field public static final calculator_number_icon_m_04_h_s:I = 0x7f020334

.field public static final calculator_number_icon_m_04_press:I = 0x7f020335

.field public static final calculator_number_icon_m_04_press_h:I = 0x7f020336

.field public static final calculator_number_icon_m_04_press_h_s:I = 0x7f020337

.field public static final calculator_number_icon_m_04_press_s:I = 0x7f020338

.field public static final calculator_number_icon_m_04_s:I = 0x7f020339

.field public static final calculator_number_icon_m_05:I = 0x7f02033a

.field public static final calculator_number_icon_m_05_h:I = 0x7f02033b

.field public static final calculator_number_icon_m_05_h_s:I = 0x7f02033c

.field public static final calculator_number_icon_m_05_press:I = 0x7f02033d

.field public static final calculator_number_icon_m_05_press_h:I = 0x7f02033e

.field public static final calculator_number_icon_m_05_press_h_s:I = 0x7f02033f

.field public static final calculator_number_icon_m_05_press_s:I = 0x7f020340

.field public static final calculator_number_icon_m_05_s:I = 0x7f020341

.field public static final calculator_number_icon_m_06:I = 0x7f020342

.field public static final calculator_number_icon_m_06_h:I = 0x7f020343

.field public static final calculator_number_icon_m_06_h_s:I = 0x7f020344

.field public static final calculator_number_icon_m_06_press:I = 0x7f020345

.field public static final calculator_number_icon_m_06_press_h:I = 0x7f020346

.field public static final calculator_number_icon_m_06_press_h_s:I = 0x7f020347

.field public static final calculator_number_icon_m_06_press_s:I = 0x7f020348

.field public static final calculator_number_icon_m_06_s:I = 0x7f020349

.field public static final calculator_number_icon_m_07:I = 0x7f02034a

.field public static final calculator_number_icon_m_07_h:I = 0x7f02034b

.field public static final calculator_number_icon_m_07_h_s:I = 0x7f02034c

.field public static final calculator_number_icon_m_07_press:I = 0x7f02034d

.field public static final calculator_number_icon_m_07_press_h:I = 0x7f02034e

.field public static final calculator_number_icon_m_07_press_h_s:I = 0x7f02034f

.field public static final calculator_number_icon_m_07_press_s:I = 0x7f020350

.field public static final calculator_number_icon_m_07_s:I = 0x7f020351

.field public static final calculator_number_icon_m_08:I = 0x7f020352

.field public static final calculator_number_icon_m_08_h:I = 0x7f020353

.field public static final calculator_number_icon_m_08_h_s:I = 0x7f020354

.field public static final calculator_number_icon_m_08_press:I = 0x7f020355

.field public static final calculator_number_icon_m_08_press_h:I = 0x7f020356

.field public static final calculator_number_icon_m_08_press_h_s:I = 0x7f020357

.field public static final calculator_number_icon_m_08_press_s:I = 0x7f020358

.field public static final calculator_number_icon_m_08_s:I = 0x7f020359

.field public static final calculator_number_icon_m_09:I = 0x7f02035a

.field public static final calculator_number_icon_m_09_h:I = 0x7f02035b

.field public static final calculator_number_icon_m_09_h_s:I = 0x7f02035c

.field public static final calculator_number_icon_m_09_press:I = 0x7f02035d

.field public static final calculator_number_icon_m_09_press_h:I = 0x7f02035e

.field public static final calculator_number_icon_m_09_press_h_s:I = 0x7f02035f

.field public static final calculator_number_icon_m_09_press_s:I = 0x7f020360

.field public static final calculator_number_icon_m_09_s:I = 0x7f020361

.field public static final calculator_number_icon_press_00:I = 0x7f020362

.field public static final calculator_number_icon_press_01:I = 0x7f020363

.field public static final calculator_number_icon_press_02:I = 0x7f020364

.field public static final calculator_number_icon_press_03:I = 0x7f020365

.field public static final calculator_number_icon_press_04:I = 0x7f020366

.field public static final calculator_number_icon_press_05:I = 0x7f020367

.field public static final calculator_number_icon_press_06:I = 0x7f020368

.field public static final calculator_number_icon_press_07:I = 0x7f020369

.field public static final calculator_number_icon_press_08:I = 0x7f02036a

.field public static final calculator_number_icon_press_09:I = 0x7f02036b

.field public static final calculator_onehand_arrow_left_f:I = 0x7f02036c

.field public static final calculator_onehand_arrow_left_n:I = 0x7f02036d

.field public static final calculator_onehand_arrow_left_p:I = 0x7f02036e

.field public static final calculator_onehand_arrow_right_f:I = 0x7f02036f

.field public static final calculator_onehand_arrow_right_n:I = 0x7f020370

.field public static final calculator_onehand_arrow_right_p:I = 0x7f020371

.field public static final calculator_panel_bg:I = 0x7f020372

.field public static final calculator_panel_button_focused:I = 0x7f020373

.field public static final calculator_panel_button_focused_01:I = 0x7f020374

.field public static final calculator_panel_button_focused_02:I = 0x7f020375

.field public static final calculator_panel_button_l_focused:I = 0x7f020376

.field public static final calculator_panel_button_l_pressed:I = 0x7f020377

.field public static final calculator_panel_button_pressed:I = 0x7f020378

.field public static final calculator_panel_button_pressed_01:I = 0x7f020379

.field public static final calculator_panel_button_pressed_02:I = 0x7f02037a

.field public static final calculator_panel_handle_bg:I = 0x7f02037b

.field public static final calculator_panel_handler_down_focused:I = 0x7f02037c

.field public static final calculator_panel_handler_down_l_focused:I = 0x7f02037d

.field public static final calculator_panel_handler_down_l_pressed:I = 0x7f02037e

.field public static final calculator_panel_handler_down_pressed:I = 0x7f02037f

.field public static final calculator_panel_handler_pressed:I = 0x7f020380

.field public static final calculator_panel_handler_up_focused:I = 0x7f020381

.field public static final calculator_panel_handler_up_l_focused:I = 0x7f020382

.field public static final calculator_panel_handler_up_l_pressed:I = 0x7f020383

.field public static final calculator_panel_handler_up_pressed:I = 0x7f020384

.field public static final calculator_panel_l_bg:I = 0x7f020385

.field public static final calculator_panel_l_handler_pressed:I = 0x7f020386

.field public static final calculator_pressed_down:I = 0x7f020387

.field public static final calculator_pressed_down_h:I = 0x7f020388

.field public static final calculator_pressed_up:I = 0x7f020389

.field public static final calculator_pressed_up_h:I = 0x7f02038a

.field public static final calculator_right_arrow_selector_h:I = 0x7f02038b

.field public static final calculator_right_handle_h:I = 0x7f02038c

.field public static final calculator_right_handle_h_focus:I = 0x7f02038d

.field public static final calculator_right_handle_h_press:I = 0x7f02038e

.field public static final calculator_tray:I = 0x7f02038f

.field public static final calculator_up_handle:I = 0x7f020390

.field public static final calculator_up_handle_focus:I = 0x7f020391

.field public static final calculator_up_handle_h:I = 0x7f020392

.field public static final calculator_up_handle_h_focus:I = 0x7f020393

.field public static final calculator_up_handle_h_press:I = 0x7f020394

.field public static final calculator_up_handle_press:I = 0x7f020395

.field public static final calcultor_hide_icon_down:I = 0x7f020396

.field public static final calcultor_hide_icon_press:I = 0x7f020397

.field public static final calcultor_hide_icon_up:I = 0x7f020398

.field public static final cc_mini_btn_back_focus:I = 0x7f020399

.field public static final cc_mini_btn_back_focus_softkey:I = 0x7f02039a

.field public static final cc_mini_btn_back_normal:I = 0x7f02039b

.field public static final cc_mini_btn_back_normal_softkey:I = 0x7f02039c

.field public static final cc_mini_btn_back_press:I = 0x7f02039d

.field public static final cc_mini_btn_back_press_softkey:I = 0x7f02039e

.field public static final cc_mini_btn_equal_focus:I = 0x7f02039f

.field public static final cc_mini_btn_equal_normal:I = 0x7f0203a0

.field public static final cc_mini_btn_equal_press:I = 0x7f0203a1

.field public static final cc_mini_btn_function_focus:I = 0x7f0203a2

.field public static final cc_mini_btn_function_focus_softkey:I = 0x7f0203a3

.field public static final cc_mini_btn_function_normal:I = 0x7f0203a4

.field public static final cc_mini_btn_function_normal_softkey:I = 0x7f0203a5

.field public static final cc_mini_btn_function_press:I = 0x7f0203a6

.field public static final cc_mini_btn_function_press_softkey:I = 0x7f0203a7

.field public static final cc_mini_btn_icon_focus:I = 0x7f0203a8

.field public static final cc_mini_btn_icon_focus_softkey:I = 0x7f0203a9

.field public static final cc_mini_btn_icon_normal:I = 0x7f0203aa

.field public static final cc_mini_btn_icon_normal_softkey:I = 0x7f0203ab

.field public static final cc_mini_btn_icon_press_softkey:I = 0x7f0203ac

.field public static final cc_mini_btn_number_focus:I = 0x7f0203ad

.field public static final cc_mini_btn_number_focus_softkey:I = 0x7f0203ae

.field public static final cc_mini_btn_number_normal:I = 0x7f0203af

.field public static final cc_mini_btn_number_normal_softkey:I = 0x7f0203b0

.field public static final cc_mini_btn_number_press:I = 0x7f0203b1

.field public static final cc_mini_btn_number_press_softkey:I = 0x7f0203b2

.field public static final cc_mini_btn_press:I = 0x7f0203b3

.field public static final cc_mini_function_icon_sum:I = 0x7f0203b4

.field public static final cc_mini_function_icon_sum_press:I = 0x7f0203b5

.field public static final cc_mini_function_icon_sum_selector:I = 0x7f0203b6

.field public static final cc_mini_icon_back:I = 0x7f0203b7

.field public static final cc_mini_icon_back_press:I = 0x7f0203b8

.field public static final cc_mini_icon_back_selector:I = 0x7f0203b9

.field public static final cc_mini_icon_bracket:I = 0x7f0203ba

.field public static final cc_mini_icon_bracket_press:I = 0x7f0203bb

.field public static final cc_mini_icon_bracket_selector:I = 0x7f0203bc

.field public static final cc_mini_icon_c:I = 0x7f0203bd

.field public static final cc_mini_icon_c_press:I = 0x7f0203be

.field public static final cc_mini_icon_c_selector:I = 0x7f0203bf

.field public static final cc_mini_icon_division:I = 0x7f0203c0

.field public static final cc_mini_icon_division_press:I = 0x7f0203c1

.field public static final cc_mini_icon_division_selector:I = 0x7f0203c2

.field public static final cc_mini_icon_multiplication:I = 0x7f0203c3

.field public static final cc_mini_icon_multiplication_press:I = 0x7f0203c4

.field public static final cc_mini_icon_multiplication_selector:I = 0x7f0203c5

.field public static final cc_mini_icon_plus:I = 0x7f0203c6

.field public static final cc_mini_icon_plus_press:I = 0x7f0203c7

.field public static final cc_mini_icon_plus_selector:I = 0x7f0203c8

.field public static final cc_mini_icon_subtraction:I = 0x7f0203c9

.field public static final cc_mini_icon_subtraction_press:I = 0x7f0203ca

.field public static final cc_mini_icon_subtraction_selector:I = 0x7f0203cb

.field public static final cc_mini_number_icon_00:I = 0x7f0203cc

.field public static final cc_mini_number_icon_00_press:I = 0x7f0203cd

.field public static final cc_mini_number_icon_00_selector:I = 0x7f0203ce

.field public static final cc_mini_number_icon_01:I = 0x7f0203cf

.field public static final cc_mini_number_icon_01_press:I = 0x7f0203d0

.field public static final cc_mini_number_icon_01_selector:I = 0x7f0203d1

.field public static final cc_mini_number_icon_02:I = 0x7f0203d2

.field public static final cc_mini_number_icon_02_press:I = 0x7f0203d3

.field public static final cc_mini_number_icon_02_selector:I = 0x7f0203d4

.field public static final cc_mini_number_icon_03:I = 0x7f0203d5

.field public static final cc_mini_number_icon_03_press:I = 0x7f0203d6

.field public static final cc_mini_number_icon_03_selector:I = 0x7f0203d7

.field public static final cc_mini_number_icon_04:I = 0x7f0203d8

.field public static final cc_mini_number_icon_04_press:I = 0x7f0203d9

.field public static final cc_mini_number_icon_04_selector:I = 0x7f0203da

.field public static final cc_mini_number_icon_05:I = 0x7f0203db

.field public static final cc_mini_number_icon_05_press:I = 0x7f0203dc

.field public static final cc_mini_number_icon_05_selector:I = 0x7f0203dd

.field public static final cc_mini_number_icon_06:I = 0x7f0203de

.field public static final cc_mini_number_icon_06_press:I = 0x7f0203df

.field public static final cc_mini_number_icon_06_selector:I = 0x7f0203e0

.field public static final cc_mini_number_icon_07:I = 0x7f0203e1

.field public static final cc_mini_number_icon_07_press:I = 0x7f0203e2

.field public static final cc_mini_number_icon_07_selector:I = 0x7f0203e3

.field public static final cc_mini_number_icon_08:I = 0x7f0203e4

.field public static final cc_mini_number_icon_08_press:I = 0x7f0203e5

.field public static final cc_mini_number_icon_08_selector:I = 0x7f0203e6

.field public static final cc_mini_number_icon_09:I = 0x7f0203e7

.field public static final cc_mini_number_icon_09_press:I = 0x7f0203e8

.field public static final cc_mini_number_icon_09_selector:I = 0x7f0203e9

.field public static final cc_mini_number_icon_dot:I = 0x7f0203ea

.field public static final cc_mini_number_icon_dot_press:I = 0x7f0203eb

.field public static final cc_mini_number_icon_dot_selector:I = 0x7f0203ec

.field public static final cc_mini_number_icon_plusminus:I = 0x7f0203ed

.field public static final cc_mini_number_icon_plusminus_press:I = 0x7f0203ee

.field public static final cc_mini_number_icon_plusminus_selector:I = 0x7f0203ef

.field public static final clear_btn_bg_selector:I = 0x7f0203f0

.field public static final clear_button_back:I = 0x7f0203f1

.field public static final clear_button_back_selector:I = 0x7f0203f2

.field public static final clear_select:I = 0x7f0203f3

.field public static final close_select:I = 0x7f0203f4

.field public static final common_popup_bg:I = 0x7f0203f5

.field public static final common_title_bg:I = 0x7f0203f6

.field public static final common_title_btn_arrow_focus:I = 0x7f0203f7

.field public static final common_title_btn_arrow_normal:I = 0x7f0203f8

.field public static final common_title_btn_arrow_press:I = 0x7f0203f9

.field public static final common_title_btn_clear_multi_normal:I = 0x7f0203fa

.field public static final common_title_btn_clear_multi_press:I = 0x7f0203fb

.field public static final common_title_btn_clear_normal:I = 0x7f0203fc

.field public static final common_title_btn_clear_press:I = 0x7f0203fd

.field public static final common_title_btn_close_focus:I = 0x7f0203fe

.field public static final common_title_btn_close_normal:I = 0x7f0203ff

.field public static final common_title_btn_close_press:I = 0x7f020400

.field public static final digits_button_back:I = 0x7f020401

.field public static final equal_button_back:I = 0x7f020402

.field public static final ext_btn_bg_selector:I = 0x7f020403

.field public static final ext_btn_handwriting_bg_selector_delete:I = 0x7f020404

.field public static final ext_btn_handwriting_bg_selector_eraser:I = 0x7f020405

.field public static final ext_btn_handwriting_bg_selector_pen:I = 0x7f020406

.field public static final ext_btn_handwriting_bg_selector_redo:I = 0x7f020407

.field public static final ext_btn_handwriting_bg_selector_undo:I = 0x7f020408

.field public static final ext_icon_abs_selector:I = 0x7f020409

.field public static final ext_icon_abs_selector_h:I = 0x7f02040a

.field public static final ext_icon_back_selector:I = 0x7f02040b

.field public static final ext_icon_back_selector_h:I = 0x7f02040c

.field public static final ext_icon_bracket_selector:I = 0x7f02040d

.field public static final ext_icon_bracket_selector_h:I = 0x7f02040e

.field public static final ext_icon_comma_selector:I = 0x7f02040f

.field public static final ext_icon_cos_selector:I = 0x7f020410

.field public static final ext_icon_cos_selector_h:I = 0x7f020411

.field public static final ext_icon_divison_selector:I = 0x7f020412

.field public static final ext_icon_divison_selector_h:I = 0x7f020413

.field public static final ext_icon_dot_selector:I = 0x7f020414

.field public static final ext_icon_dot_selector_h:I = 0x7f020415

.field public static final ext_icon_ex_selector:I = 0x7f020416

.field public static final ext_icon_ex_selector_h:I = 0x7f020417

.field public static final ext_icon_exp_selector:I = 0x7f020418

.field public static final ext_icon_exp_selector_h:I = 0x7f020419

.field public static final ext_icon_factorial_selector:I = 0x7f02041a

.field public static final ext_icon_factorial_selector_h:I = 0x7f02041b

.field public static final ext_icon_in_selector:I = 0x7f02041c

.field public static final ext_icon_in_selector_h:I = 0x7f02041d

.field public static final ext_icon_log_selector:I = 0x7f02041e

.field public static final ext_icon_log_selector_h:I = 0x7f02041f

.field public static final ext_icon_multi_selector:I = 0x7f020420

.field public static final ext_icon_multi_selector_h:I = 0x7f020421

.field public static final ext_icon_percent_selector:I = 0x7f020422

.field public static final ext_icon_percent_selector_h:I = 0x7f020423

.field public static final ext_icon_pie_selector:I = 0x7f020424

.field public static final ext_icon_pie_selector_h:I = 0x7f020425

.field public static final ext_icon_plus_selector:I = 0x7f020426

.field public static final ext_icon_plus_selector_h:I = 0x7f020427

.field public static final ext_icon_plusminus_selector:I = 0x7f020428

.field public static final ext_icon_plusminus_selector_h:I = 0x7f020429

.field public static final ext_icon_root_selector:I = 0x7f02042a

.field public static final ext_icon_root_selector_h:I = 0x7f02042b

.field public static final ext_icon_sin_selector:I = 0x7f02042c

.field public static final ext_icon_sin_selector_h:I = 0x7f02042d

.field public static final ext_icon_sub_selector:I = 0x7f02042e

.field public static final ext_icon_sub_selector_h:I = 0x7f02042f

.field public static final ext_icon_tan_selector:I = 0x7f020430

.field public static final ext_icon_tan_selector_h:I = 0x7f020431

.field public static final ext_icon_throughput_selector:I = 0x7f020432

.field public static final ext_icon_throughput_selector_h:I = 0x7f020433

.field public static final ext_icon_x2_selector:I = 0x7f020434

.field public static final ext_icon_x2_selector_h:I = 0x7f020435

.field public static final ext_icon_xy_selector:I = 0x7f020436

.field public static final ext_icon_xy_selector_h:I = 0x7f020437

.field public static final func_btn_bg_selector:I = 0x7f020438

.field public static final func_icon_c_selector:I = 0x7f020439

.field public static final func_icon_c_selector_h:I = 0x7f02043a

.field public static final func_icon_sum_selector:I = 0x7f02043b

.field public static final func_icon_sum_selector_h:I = 0x7f02043c

.field public static final h_button_selector_clear_button:I = 0x7f02043d

.field public static final h_button_selector_clear_button_l:I = 0x7f02043e

.field public static final handler_bar_focus:I = 0x7f02043f

.field public static final handler_bar_focus_h:I = 0x7f020440

.field public static final handler_bar_l_focus:I = 0x7f020441

.field public static final handler_bar_l_normal:I = 0x7f020442

.field public static final handler_bar_land_focus:I = 0x7f020443

.field public static final handler_bar_land_normal:I = 0x7f020444

.field public static final handler_bar_normal:I = 0x7f020445

.field public static final handler_bar_normal_h:I = 0x7f020446

.field public static final handler_close_selector:I = 0x7f020447

.field public static final handler_close_selector_h:I = 0x7f020448

.field public static final handler_down_nomal:I = 0x7f020449

.field public static final handler_down_press:I = 0x7f02044a

.field public static final handler_line:I = 0x7f02044b

.field public static final handler_open_close_selector:I = 0x7f02044c

.field public static final handler_open_close_selector_h:I = 0x7f02044d

.field public static final handler_open_selector:I = 0x7f02044e

.field public static final handler_open_selector_h:I = 0x7f02044f

.field public static final handler_selector:I = 0x7f020450

.field public static final handler_selector_down:I = 0x7f020451

.field public static final handler_selector_down_h:I = 0x7f020452

.field public static final handler_selector_h:I = 0x7f020453

.field public static final handler_selector_up:I = 0x7f020454

.field public static final handler_selector_up_h:I = 0x7f020455

.field public static final handler_up_nomal:I = 0x7f020456

.field public static final handler_up_press:I = 0x7f020457

.field public static final history_clear_button_selector:I = 0x7f020458

.field public static final history_clear_selector:I = 0x7f020459

.field public static final icon:I = 0x7f02045a

.field public static final keypad_bg:I = 0x7f02045b

.field public static final land_btn_bg_01:I = 0x7f02045c

.field public static final land_btn_bg_02:I = 0x7f02045d

.field public static final land_btn_bg_03:I = 0x7f02045e

.field public static final land_btn_bg_04:I = 0x7f02045f

.field public static final land_btn_focus_01:I = 0x7f020460

.field public static final land_btn_focus_02:I = 0x7f020461

.field public static final land_btn_focus_03:I = 0x7f020462

.field public static final land_btn_focus_04:I = 0x7f020463

.field public static final land_btn_num_01:I = 0x7f020464

.field public static final land_btn_num_01_press:I = 0x7f020465

.field public static final land_btn_num_02:I = 0x7f020466

.field public static final land_btn_num_02_press:I = 0x7f020467

.field public static final land_btn_num_03:I = 0x7f020468

.field public static final land_btn_num_03_press:I = 0x7f020469

.field public static final land_btn_num_04:I = 0x7f02046a

.field public static final land_btn_num_04_press:I = 0x7f02046b

.field public static final land_btn_num_05:I = 0x7f02046c

.field public static final land_btn_num_05_press:I = 0x7f02046d

.field public static final land_btn_num_06:I = 0x7f02046e

.field public static final land_btn_num_06_press:I = 0x7f02046f

.field public static final land_btn_num_07:I = 0x7f020470

.field public static final land_btn_num_07_press:I = 0x7f020471

.field public static final land_btn_num_08:I = 0x7f020472

.field public static final land_btn_num_08_press:I = 0x7f020473

.field public static final land_btn_num_09:I = 0x7f020474

.field public static final land_btn_num_09_press:I = 0x7f020475

.field public static final land_btn_num_10:I = 0x7f020476

.field public static final land_btn_num_10_press:I = 0x7f020477

.field public static final land_btn_num_11:I = 0x7f020478

.field public static final land_btn_num_11_press:I = 0x7f020479

.field public static final land_btn_num_12:I = 0x7f02047a

.field public static final land_btn_num_12_press:I = 0x7f02047b

.field public static final land_btn_num_13:I = 0x7f02047c

.field public static final land_btn_num_13_press:I = 0x7f02047d

.field public static final land_btn_num_14:I = 0x7f02047e

.field public static final land_btn_num_14_press:I = 0x7f02047f

.field public static final land_btn_num_15:I = 0x7f020480

.field public static final land_btn_num_15_press:I = 0x7f020481

.field public static final land_btn_num_16:I = 0x7f020482

.field public static final land_btn_num_16_press:I = 0x7f020483

.field public static final land_btn_num_17:I = 0x7f020484

.field public static final land_btn_num_17_press:I = 0x7f020485

.field public static final land_btn_num_18:I = 0x7f020486

.field public static final land_btn_num_18_press:I = 0x7f020487

.field public static final land_btn_num_19:I = 0x7f020488

.field public static final land_btn_num_19_press:I = 0x7f020489

.field public static final land_btn_num_20:I = 0x7f02048a

.field public static final land_btn_num_20_press:I = 0x7f02048b

.field public static final land_btn_num_21:I = 0x7f02048c

.field public static final land_btn_num_21_press:I = 0x7f02048d

.field public static final land_btn_num_22:I = 0x7f02048e

.field public static final land_btn_num_22_press:I = 0x7f02048f

.field public static final land_btn_num_23:I = 0x7f020490

.field public static final land_btn_num_23_press:I = 0x7f020491

.field public static final land_btn_num_24:I = 0x7f020492

.field public static final land_btn_num_24_press:I = 0x7f020493

.field public static final land_btn_num_25:I = 0x7f020494

.field public static final land_btn_num_25_press:I = 0x7f020495

.field public static final land_btn_num_26:I = 0x7f020496

.field public static final land_btn_num_26_press:I = 0x7f020497

.field public static final land_btn_num_27:I = 0x7f020498

.field public static final land_btn_num_27_press:I = 0x7f020499

.field public static final land_btn_num_28:I = 0x7f02049a

.field public static final land_btn_num_28_press:I = 0x7f02049b

.field public static final land_btn_num_29:I = 0x7f02049c

.field public static final land_btn_num_29_press:I = 0x7f02049d

.field public static final land_btn_num_30:I = 0x7f02049e

.field public static final land_btn_num_30_press:I = 0x7f02049f

.field public static final land_btn_num_31:I = 0x7f0204a0

.field public static final land_btn_num_31_press:I = 0x7f0204a1

.field public static final land_btn_num_32:I = 0x7f0204a2

.field public static final land_btn_num_32_press:I = 0x7f0204a3

.field public static final land_btn_num_33:I = 0x7f0204a4

.field public static final land_btn_num_33_press:I = 0x7f0204a5

.field public static final land_btn_num_34:I = 0x7f0204a6

.field public static final land_btn_num_34_press:I = 0x7f0204a7

.field public static final land_btn_num_35:I = 0x7f0204a8

.field public static final land_btn_num_35_press:I = 0x7f0204a9

.field public static final land_btn_press:I = 0x7f0204aa

.field public static final land_btn_press_01:I = 0x7f0204ab

.field public static final land_btn_press_02:I = 0x7f0204ac

.field public static final land_btn_press_03:I = 0x7f0204ad

.field public static final land_btn_press_04:I = 0x7f0204ae

.field public static final land_btn_press_1:I = 0x7f0204af

.field public static final land_clear_button_back:I = 0x7f0204b0

.field public static final land_dark_ui_btn_blue_bg_selector:I = 0x7f0204b1

.field public static final land_dark_ui_btn_dark_bg_selector:I = 0x7f0204b2

.field public static final land_dark_ui_btn_light_bg_selector:I = 0x7f0204b3

.field public static final land_dark_ui_btn_orange_bg_selector:I = 0x7f0204b4

.field public static final land_dark_ui_num_01_selector:I = 0x7f0204b5

.field public static final land_dark_ui_num_02_selector:I = 0x7f0204b6

.field public static final land_dark_ui_num_03_selector:I = 0x7f0204b7

.field public static final land_dark_ui_num_04_selector:I = 0x7f0204b8

.field public static final land_dark_ui_num_05_selector:I = 0x7f0204b9

.field public static final land_dark_ui_num_06_selector:I = 0x7f0204ba

.field public static final land_dark_ui_num_07_selector:I = 0x7f0204bb

.field public static final land_dark_ui_num_08_selector:I = 0x7f0204bc

.field public static final land_dark_ui_num_09_selector:I = 0x7f0204bd

.field public static final land_dark_ui_num_10_selector:I = 0x7f0204be

.field public static final land_dark_ui_num_11_selector:I = 0x7f0204bf

.field public static final land_dark_ui_num_12_selector:I = 0x7f0204c0

.field public static final land_dark_ui_num_13_selector:I = 0x7f0204c1

.field public static final land_dark_ui_num_14_selector:I = 0x7f0204c2

.field public static final land_dark_ui_num_15_selector:I = 0x7f0204c3

.field public static final land_dark_ui_num_16_selector:I = 0x7f0204c4

.field public static final land_dark_ui_num_17_selector:I = 0x7f0204c5

.field public static final land_dark_ui_num_18_selector:I = 0x7f0204c6

.field public static final land_dark_ui_num_19_selector:I = 0x7f0204c7

.field public static final land_dark_ui_num_20_selector:I = 0x7f0204c8

.field public static final land_dark_ui_num_21_selector:I = 0x7f0204c9

.field public static final land_dark_ui_num_22_selector:I = 0x7f0204ca

.field public static final land_dark_ui_num_23_selector:I = 0x7f0204cb

.field public static final land_dark_ui_num_24_selector:I = 0x7f0204cc

.field public static final land_dark_ui_num_25_selector:I = 0x7f0204cd

.field public static final land_dark_ui_num_26_selector:I = 0x7f0204ce

.field public static final land_dark_ui_num_27_selector:I = 0x7f0204cf

.field public static final land_dark_ui_num_28_selector:I = 0x7f0204d0

.field public static final land_dark_ui_num_29_selector:I = 0x7f0204d1

.field public static final land_dark_ui_num_30_selector:I = 0x7f0204d2

.field public static final land_dark_ui_num_31_selector:I = 0x7f0204d3

.field public static final land_dark_ui_num_32_selector:I = 0x7f0204d4

.field public static final land_dark_ui_num_33_selector:I = 0x7f0204d5

.field public static final land_dark_ui_num_34_selector:I = 0x7f0204d6

.field public static final land_dark_ui_num_35_selector:I = 0x7f0204d7

.field public static final land_dark_ui_panel_handle_selector:I = 0x7f0204d8

.field public static final land_digits_button_back_03:I = 0x7f0204d9

.field public static final land_ext_btn_bg_selector:I = 0x7f0204da

.field public static final land_ext_btn_bg_selector_blue:I = 0x7f0204db

.field public static final land_ext_btn_bg_selector_dark:I = 0x7f0204dc

.field public static final land_ext_btn_bg_selector_light:I = 0x7f0204dd

.field public static final land_ext_btn_bg_selector_orange:I = 0x7f0204de

.field public static final land_white_ui_btn_abs_selector:I = 0x7f0204df

.field public static final land_white_ui_btn_blue_bg_selector:I = 0x7f0204e0

.field public static final land_white_ui_btn_cos_selector:I = 0x7f0204e1

.field public static final land_white_ui_btn_dark_bg_selector:I = 0x7f0204e2

.field public static final land_white_ui_btn_e_selector:I = 0x7f0204e3

.field public static final land_white_ui_btn_ex_selector:I = 0x7f0204e4

.field public static final land_white_ui_btn_factorial_selector:I = 0x7f0204e5

.field public static final land_white_ui_btn_fn_clear_selector:I = 0x7f0204e6

.field public static final land_white_ui_btn_fn_equal_selector:I = 0x7f0204e7

.field public static final land_white_ui_btn_icon_back_selector:I = 0x7f0204e8

.field public static final land_white_ui_btn_icon_bracket_selector:I = 0x7f0204e9

.field public static final land_white_ui_btn_icon_division_selector:I = 0x7f0204ea

.field public static final land_white_ui_btn_icon_multiplication_selector:I = 0x7f0204eb

.field public static final land_white_ui_btn_icon_plus_selector:I = 0x7f0204ec

.field public static final land_white_ui_btn_icon_subtraction_selector:I = 0x7f0204ed

.field public static final land_white_ui_btn_in_selector:I = 0x7f0204ee

.field public static final land_white_ui_btn_light_bg_selector:I = 0x7f0204ef

.field public static final land_white_ui_btn_log_selector:I = 0x7f0204f0

.field public static final land_white_ui_btn_num_00_selector:I = 0x7f0204f1

.field public static final land_white_ui_btn_num_01_selector:I = 0x7f0204f2

.field public static final land_white_ui_btn_num_02_selector:I = 0x7f0204f3

.field public static final land_white_ui_btn_num_03_selector:I = 0x7f0204f4

.field public static final land_white_ui_btn_num_04_selector:I = 0x7f0204f5

.field public static final land_white_ui_btn_num_05_selector:I = 0x7f0204f6

.field public static final land_white_ui_btn_num_06_selector:I = 0x7f0204f7

.field public static final land_white_ui_btn_num_07_selector:I = 0x7f0204f8

.field public static final land_white_ui_btn_num_08_selector:I = 0x7f0204f9

.field public static final land_white_ui_btn_num_09_selector:I = 0x7f0204fa

.field public static final land_white_ui_btn_num_dot_selector:I = 0x7f0204fb

.field public static final land_white_ui_btn_num_plusminus_selector:I = 0x7f0204fc

.field public static final land_white_ui_btn_orange_bg_selector:I = 0x7f0204fd

.field public static final land_white_ui_btn_percent_selector:I = 0x7f0204fe

.field public static final land_white_ui_btn_pie_selector:I = 0x7f0204ff

.field public static final land_white_ui_btn_root_selector:I = 0x7f020500

.field public static final land_white_ui_btn_sin_selector:I = 0x7f020501

.field public static final land_white_ui_btn_tan_selector:I = 0x7f020502

.field public static final land_white_ui_btn_throughput_selector:I = 0x7f020503

.field public static final land_white_ui_btn_x2_selector:I = 0x7f020504

.field public static final land_white_ui_btn_xy_selector:I = 0x7f020505

.field public static final land_white_ui_handler_close_selector:I = 0x7f020506

.field public static final land_white_ui_handler_open_selector:I = 0x7f020507

.field public static final land_white_ui_panel_handle_selector:I = 0x7f020508

.field public static final listview_selector:I = 0x7f020509

.field public static final magazine_icon_calculator_bg:I = 0x7f02050a

.field public static final mainmenu_icon_calculator:I = 0x7f02050b

.field public static final menu_ic_handwriting:I = 0x7f02050c

.field public static final menu_ic_keypad:I = 0x7f02050d

.field public static final mg_calculator:I = 0x7f02050e

.field public static final mini_backspace_btn_bg_selector:I = 0x7f02050f

.field public static final mini_clear_btn_bg_selector:I = 0x7f020510

.field public static final mini_equal_btn_bg_selector:I = 0x7f020511

.field public static final mini_ext_btn_bg_selector:I = 0x7f020512

.field public static final mini_func_btn_bg_selector:I = 0x7f020513

.field public static final mini_handler_close_selector:I = 0x7f020514

.field public static final mini_handler_open_selector:I = 0x7f020515

.field public static final mini_nor_btn_bg_selector:I = 0x7f020516

.field public static final mini_tray_calculator_keypad_bg:I = 0x7f020517

.field public static final mini_tray_calculator_lcd:I = 0x7f020518

.field public static final mini_tray_calculator_lcd_bg_full:I = 0x7f020519

.field public static final multi_clear_btn_bg_selector:I = 0x7f02051a

.field public static final multi_equal_btn_bg_selector:I = 0x7f02051b

.field public static final multi_ext_btn_bg_selector:I = 0x7f02051c

.field public static final multi_ext_icon_abs_selector:I = 0x7f02051d

.field public static final multi_ext_icon_abs_selector_s:I = 0x7f02051e

.field public static final multi_ext_icon_back_selector:I = 0x7f02051f

.field public static final multi_ext_icon_back_selector_h:I = 0x7f020520

.field public static final multi_ext_icon_back_selector_h_s:I = 0x7f020521

.field public static final multi_ext_icon_back_selector_s:I = 0x7f020522

.field public static final multi_ext_icon_bracket_selector:I = 0x7f020523

.field public static final multi_ext_icon_bracket_selector_h:I = 0x7f020524

.field public static final multi_ext_icon_bracket_selector_h_s:I = 0x7f020525

.field public static final multi_ext_icon_bracket_selector_s:I = 0x7f020526

.field public static final multi_ext_icon_cos_selector:I = 0x7f020527

.field public static final multi_ext_icon_cos_selector_s:I = 0x7f020528

.field public static final multi_ext_icon_divison_selector:I = 0x7f020529

.field public static final multi_ext_icon_divison_selector_h:I = 0x7f02052a

.field public static final multi_ext_icon_divison_selector_h_s:I = 0x7f02052b

.field public static final multi_ext_icon_divison_selector_s:I = 0x7f02052c

.field public static final multi_ext_icon_dot_selector:I = 0x7f02052d

.field public static final multi_ext_icon_dot_selector_h:I = 0x7f02052e

.field public static final multi_ext_icon_dot_selector_h_s:I = 0x7f02052f

.field public static final multi_ext_icon_dot_selector_s:I = 0x7f020530

.field public static final multi_ext_icon_e_selector:I = 0x7f020531

.field public static final multi_ext_icon_e_selector_s:I = 0x7f020532

.field public static final multi_ext_icon_ex_selector:I = 0x7f020533

.field public static final multi_ext_icon_ex_selector_s:I = 0x7f020534

.field public static final multi_ext_icon_exp_selector:I = 0x7f020535

.field public static final multi_ext_icon_factorial_selector:I = 0x7f020536

.field public static final multi_ext_icon_factorial_selector_s:I = 0x7f020537

.field public static final multi_ext_icon_in_selector:I = 0x7f020538

.field public static final multi_ext_icon_in_selector_s:I = 0x7f020539

.field public static final multi_ext_icon_log_selector:I = 0x7f02053a

.field public static final multi_ext_icon_log_selector_s:I = 0x7f02053b

.field public static final multi_ext_icon_multi_selector:I = 0x7f02053c

.field public static final multi_ext_icon_multi_selector_h:I = 0x7f02053d

.field public static final multi_ext_icon_multi_selector_h_s:I = 0x7f02053e

.field public static final multi_ext_icon_multi_selector_s:I = 0x7f02053f

.field public static final multi_ext_icon_percent_selector:I = 0x7f020540

.field public static final multi_ext_icon_percent_selector_s:I = 0x7f020541

.field public static final multi_ext_icon_pie_selector:I = 0x7f020542

.field public static final multi_ext_icon_pie_selector_s:I = 0x7f020543

.field public static final multi_ext_icon_plus_selector:I = 0x7f020544

.field public static final multi_ext_icon_plus_selector_h:I = 0x7f020545

.field public static final multi_ext_icon_plus_selector_h_s:I = 0x7f020546

.field public static final multi_ext_icon_plus_selector_s:I = 0x7f020547

.field public static final multi_ext_icon_plusminus_selector:I = 0x7f020548

.field public static final multi_ext_icon_plusminus_selector_h:I = 0x7f020549

.field public static final multi_ext_icon_plusminus_selector_s:I = 0x7f02054a

.field public static final multi_ext_icon_root_selector:I = 0x7f02054b

.field public static final multi_ext_icon_root_selector_s:I = 0x7f02054c

.field public static final multi_ext_icon_sin_selector:I = 0x7f02054d

.field public static final multi_ext_icon_sin_selector_s:I = 0x7f02054e

.field public static final multi_ext_icon_sub_selector:I = 0x7f02054f

.field public static final multi_ext_icon_sub_selector_h:I = 0x7f020550

.field public static final multi_ext_icon_sub_selector_h_s:I = 0x7f020551

.field public static final multi_ext_icon_sub_selector_s:I = 0x7f020552

.field public static final multi_ext_icon_tan_selector:I = 0x7f020553

.field public static final multi_ext_icon_tan_selector_s:I = 0x7f020554

.field public static final multi_ext_icon_throughput_selector:I = 0x7f020555

.field public static final multi_ext_icon_throughput_selector_s:I = 0x7f020556

.field public static final multi_ext_icon_x2_selector:I = 0x7f020557

.field public static final multi_ext_icon_x2_selector_s:I = 0x7f020558

.field public static final multi_ext_icon_xy_selector:I = 0x7f020559

.field public static final multi_ext_icon_xy_selector_s:I = 0x7f02055a

.field public static final multi_func_btn_bg_selector:I = 0x7f02055b

.field public static final multi_func_icon_c_selector:I = 0x7f02055c

.field public static final multi_func_icon_c_selector_h:I = 0x7f02055d

.field public static final multi_func_icon_c_selector_h_s:I = 0x7f02055e

.field public static final multi_func_icon_c_selector_s:I = 0x7f02055f

.field public static final multi_func_icon_sum_selector:I = 0x7f020560

.field public static final multi_func_icon_sum_selector_h:I = 0x7f020561

.field public static final multi_func_icon_sum_selector_h_s:I = 0x7f020562

.field public static final multi_func_icon_sum_selector_s:I = 0x7f020563

.field public static final multi_handler_close_selector:I = 0x7f020564

.field public static final multi_handler_close_selector_h:I = 0x7f020565

.field public static final multi_handler_open_selector:I = 0x7f020566

.field public static final multi_handler_open_selector_h:I = 0x7f020567

.field public static final multi_nor_btn_bg_selector:I = 0x7f020568

.field public static final multi_nor_icon_0_selector:I = 0x7f020569

.field public static final multi_nor_icon_0_selector_h:I = 0x7f02056a

.field public static final multi_nor_icon_0_selector_h_s:I = 0x7f02056b

.field public static final multi_nor_icon_0_selector_s:I = 0x7f02056c

.field public static final multi_nor_icon_1_selector:I = 0x7f02056d

.field public static final multi_nor_icon_1_selector_h:I = 0x7f02056e

.field public static final multi_nor_icon_1_selector_h_s:I = 0x7f02056f

.field public static final multi_nor_icon_1_selector_s:I = 0x7f020570

.field public static final multi_nor_icon_2_selector:I = 0x7f020571

.field public static final multi_nor_icon_2_selector_h:I = 0x7f020572

.field public static final multi_nor_icon_2_selector_h_s:I = 0x7f020573

.field public static final multi_nor_icon_2_selector_s:I = 0x7f020574

.field public static final multi_nor_icon_3_selector:I = 0x7f020575

.field public static final multi_nor_icon_3_selector_h:I = 0x7f020576

.field public static final multi_nor_icon_3_selector_h_s:I = 0x7f020577

.field public static final multi_nor_icon_3_selector_s:I = 0x7f020578

.field public static final multi_nor_icon_4_selector:I = 0x7f020579

.field public static final multi_nor_icon_4_selector_h:I = 0x7f02057a

.field public static final multi_nor_icon_4_selector_h_s:I = 0x7f02057b

.field public static final multi_nor_icon_4_selector_s:I = 0x7f02057c

.field public static final multi_nor_icon_5_selector:I = 0x7f02057d

.field public static final multi_nor_icon_5_selector_h:I = 0x7f02057e

.field public static final multi_nor_icon_5_selector_h_s:I = 0x7f02057f

.field public static final multi_nor_icon_5_selector_s:I = 0x7f020580

.field public static final multi_nor_icon_6_selector:I = 0x7f020581

.field public static final multi_nor_icon_6_selector_h:I = 0x7f020582

.field public static final multi_nor_icon_6_selector_h_s:I = 0x7f020583

.field public static final multi_nor_icon_6_selector_s:I = 0x7f020584

.field public static final multi_nor_icon_7_selector:I = 0x7f020585

.field public static final multi_nor_icon_7_selector_h:I = 0x7f020586

.field public static final multi_nor_icon_7_selector_h_s:I = 0x7f020587

.field public static final multi_nor_icon_7_selector_s:I = 0x7f020588

.field public static final multi_nor_icon_8_selector:I = 0x7f020589

.field public static final multi_nor_icon_8_selector_h:I = 0x7f02058a

.field public static final multi_nor_icon_8_selector_h_s:I = 0x7f02058b

.field public static final multi_nor_icon_8_selector_s:I = 0x7f02058c

.field public static final multi_nor_icon_9_selector:I = 0x7f02058d

.field public static final multi_nor_icon_9_selector_h:I = 0x7f02058e

.field public static final multi_nor_icon_9_selector_h_s:I = 0x7f02058f

.field public static final multi_nor_icon_9_selector_s:I = 0x7f020590

.field public static final multiwindow_icon:I = 0x7f020591

.field public static final nor_btn_bg_selector:I = 0x7f020592

.field public static final nor_icon_0_selector:I = 0x7f020593

.field public static final nor_icon_0_selector_h:I = 0x7f020594

.field public static final nor_icon_1_selector:I = 0x7f020595

.field public static final nor_icon_1_selector_h:I = 0x7f020596

.field public static final nor_icon_2_selector:I = 0x7f020597

.field public static final nor_icon_2_selector_h:I = 0x7f020598

.field public static final nor_icon_3_selector:I = 0x7f020599

.field public static final nor_icon_3_selector_h:I = 0x7f02059a

.field public static final nor_icon_4_selector:I = 0x7f02059b

.field public static final nor_icon_4_selector_h:I = 0x7f02059c

.field public static final nor_icon_5_selector:I = 0x7f02059d

.field public static final nor_icon_5_selector_h:I = 0x7f02059e

.field public static final nor_icon_6_selector:I = 0x7f02059f

.field public static final nor_icon_6_selector_h:I = 0x7f0205a0

.field public static final nor_icon_7_selector:I = 0x7f0205a1

.field public static final nor_icon_7_selector_h:I = 0x7f0205a2

.field public static final nor_icon_8_selector:I = 0x7f0205a3

.field public static final nor_icon_8_selector_h:I = 0x7f0205a4

.field public static final nor_icon_9_selector:I = 0x7f0205a5

.field public static final nor_icon_9_selector_h:I = 0x7f0205a6

.field public static final onehand_btn_00:I = 0x7f0205a7

.field public static final onehand_btn_01:I = 0x7f0205a8

.field public static final onehand_btn_02:I = 0x7f0205a9

.field public static final onehand_btn_03:I = 0x7f0205aa

.field public static final onehand_btn_04:I = 0x7f0205ab

.field public static final onehand_btn_05:I = 0x7f0205ac

.field public static final onehand_btn_06:I = 0x7f0205ad

.field public static final onehand_btn_07:I = 0x7f0205ae

.field public static final onehand_btn_08:I = 0x7f0205af

.field public static final onehand_btn_09:I = 0x7f0205b0

.field public static final onehand_btn_10:I = 0x7f0205b1

.field public static final onehand_btn_11:I = 0x7f0205b2

.field public static final onehand_btn_12:I = 0x7f0205b3

.field public static final onehand_btn_13:I = 0x7f0205b4

.field public static final onehand_btn_14:I = 0x7f0205b5

.field public static final onehand_btn_15:I = 0x7f0205b6

.field public static final onehand_btn_16:I = 0x7f0205b7

.field public static final onehand_btn_17:I = 0x7f0205b8

.field public static final onehand_btn_18:I = 0x7f0205b9

.field public static final onehand_btn_19:I = 0x7f0205ba

.field public static final onehand_btn_num_01:I = 0x7f0205bb

.field public static final onehand_btn_num_02:I = 0x7f0205bc

.field public static final onehand_btn_num_03:I = 0x7f0205bd

.field public static final onehand_btn_num_04:I = 0x7f0205be

.field public static final onehand_btn_num_05:I = 0x7f0205bf

.field public static final onehand_btn_num_05_press:I = 0x7f0205c0

.field public static final onehand_btn_num_06:I = 0x7f0205c1

.field public static final onehand_btn_num_06_press:I = 0x7f0205c2

.field public static final onehand_btn_num_07:I = 0x7f0205c3

.field public static final onehand_btn_num_07_press:I = 0x7f0205c4

.field public static final onehand_btn_num_08:I = 0x7f0205c5

.field public static final onehand_btn_num_09:I = 0x7f0205c6

.field public static final onehand_btn_num_09_press:I = 0x7f0205c7

.field public static final onehand_btn_num_10:I = 0x7f0205c8

.field public static final onehand_btn_num_10_press:I = 0x7f0205c9

.field public static final onehand_btn_num_11:I = 0x7f0205ca

.field public static final onehand_btn_num_11_press:I = 0x7f0205cb

.field public static final onehand_btn_num_12:I = 0x7f0205cc

.field public static final onehand_btn_num_13:I = 0x7f0205cd

.field public static final onehand_btn_num_13_press:I = 0x7f0205ce

.field public static final onehand_btn_num_14:I = 0x7f0205cf

.field public static final onehand_btn_num_14_press:I = 0x7f0205d0

.field public static final onehand_btn_num_15:I = 0x7f0205d1

.field public static final onehand_btn_num_15_press:I = 0x7f0205d2

.field public static final onehand_btn_num_16:I = 0x7f0205d3

.field public static final onehand_btn_num_17:I = 0x7f0205d4

.field public static final onehand_btn_num_17_press:I = 0x7f0205d5

.field public static final onehand_btn_num_18:I = 0x7f0205d6

.field public static final onehand_btn_num_18_press:I = 0x7f0205d7

.field public static final onehand_btn_num_19:I = 0x7f0205d8

.field public static final onehand_btn_num_19_press:I = 0x7f0205d9

.field public static final onehand_btn_num_20:I = 0x7f0205da

.field public static final onehand_btn_selector:I = 0x7f0205db

.field public static final onehand_ext_icon_back:I = 0x7f0205dc

.field public static final onehand_ext_icon_back_press:I = 0x7f0205dd

.field public static final onehand_ext_icon_back_selector:I = 0x7f0205de

.field public static final onehand_ext_icon_bracket:I = 0x7f0205df

.field public static final onehand_ext_icon_bracket_press:I = 0x7f0205e0

.field public static final onehand_ext_icon_bracket_selector:I = 0x7f0205e1

.field public static final onehand_ext_icon_divison:I = 0x7f0205e2

.field public static final onehand_ext_icon_divison_press:I = 0x7f0205e3

.field public static final onehand_ext_icon_divison_selector:I = 0x7f0205e4

.field public static final onehand_ext_icon_dot:I = 0x7f0205e5

.field public static final onehand_ext_icon_dot_press:I = 0x7f0205e6

.field public static final onehand_ext_icon_dot_selector:I = 0x7f0205e7

.field public static final onehand_ext_icon_multi:I = 0x7f0205e8

.field public static final onehand_ext_icon_multi_press:I = 0x7f0205e9

.field public static final onehand_ext_icon_multi_selector:I = 0x7f0205ea

.field public static final onehand_ext_icon_plus:I = 0x7f0205eb

.field public static final onehand_ext_icon_plus_press:I = 0x7f0205ec

.field public static final onehand_ext_icon_plus_selector:I = 0x7f0205ed

.field public static final onehand_ext_icon_plusminus:I = 0x7f0205ee

.field public static final onehand_ext_icon_plusminus_press:I = 0x7f0205ef

.field public static final onehand_ext_icon_plusminus_selector:I = 0x7f0205f0

.field public static final onehand_ext_icon_sub:I = 0x7f0205f1

.field public static final onehand_ext_icon_sub_press:I = 0x7f0205f2

.field public static final onehand_ext_icon_sub_selector:I = 0x7f0205f3

.field public static final onehand_func_icon_c:I = 0x7f0205f4

.field public static final onehand_func_icon_c_press:I = 0x7f0205f5

.field public static final onehand_func_icon_c_selector:I = 0x7f0205f6

.field public static final onehand_func_icon_sum:I = 0x7f0205f7

.field public static final onehand_func_icon_sum_press:I = 0x7f0205f8

.field public static final onehand_func_icon_sum_selector:I = 0x7f0205f9

.field public static final onehand_handler_left:I = 0x7f0205fa

.field public static final onehand_handler_left_focused:I = 0x7f0205fb

.field public static final onehand_handler_left_pressed:I = 0x7f0205fc

.field public static final onehand_handler_right:I = 0x7f0205fd

.field public static final onehand_handler_right_focused:I = 0x7f0205fe

.field public static final onehand_handler_right_pressed:I = 0x7f0205ff

.field public static final onehand_left_selector:I = 0x7f020600

.field public static final onehand_nor_icon_0:I = 0x7f020601

.field public static final onehand_nor_icon_0_press:I = 0x7f020602

.field public static final onehand_nor_icon_0_selector:I = 0x7f020603

.field public static final onehand_nor_icon_1:I = 0x7f020604

.field public static final onehand_nor_icon_1_press:I = 0x7f020605

.field public static final onehand_nor_icon_1_selector:I = 0x7f020606

.field public static final onehand_nor_icon_2:I = 0x7f020607

.field public static final onehand_nor_icon_2_press:I = 0x7f020608

.field public static final onehand_nor_icon_2_selector:I = 0x7f020609

.field public static final onehand_nor_icon_3:I = 0x7f02060a

.field public static final onehand_nor_icon_3_press:I = 0x7f02060b

.field public static final onehand_nor_icon_3_selector:I = 0x7f02060c

.field public static final onehand_nor_icon_4:I = 0x7f02060d

.field public static final onehand_nor_icon_4_press:I = 0x7f02060e

.field public static final onehand_nor_icon_4_selector:I = 0x7f02060f

.field public static final onehand_nor_icon_5:I = 0x7f020610

.field public static final onehand_nor_icon_5_press:I = 0x7f020611

.field public static final onehand_nor_icon_5_selector:I = 0x7f020612

.field public static final onehand_nor_icon_6:I = 0x7f020613

.field public static final onehand_nor_icon_6_press:I = 0x7f020614

.field public static final onehand_nor_icon_6_selector:I = 0x7f020615

.field public static final onehand_nor_icon_7:I = 0x7f020616

.field public static final onehand_nor_icon_7_press:I = 0x7f020617

.field public static final onehand_nor_icon_7_selector:I = 0x7f020618

.field public static final onehand_nor_icon_8:I = 0x7f020619

.field public static final onehand_nor_icon_8_press:I = 0x7f02061a

.field public static final onehand_nor_icon_8_selector:I = 0x7f02061b

.field public static final onehand_nor_icon_9:I = 0x7f02061c

.field public static final onehand_nor_icon_9_press:I = 0x7f02061d

.field public static final onehand_nor_icon_9_selector:I = 0x7f02061e

.field public static final onehand_panel_bg:I = 0x7f02061f

.field public static final onehand_panel_button:I = 0x7f020620

.field public static final onehand_panel_button_focused:I = 0x7f020621

.field public static final onehand_panel_button_pressed:I = 0x7f020622

.field public static final onehand_right_selector:I = 0x7f020623

.field public static final panel_bg:I = 0x7f020624

.field public static final panel_bg_land:I = 0x7f020625

.field public static final panel_dark_bg:I = 0x7f020626

.field public static final pannel:I = 0x7f020627

.field public static final pannel_light:I = 0x7f020628

.field public static final port_clear_button_back:I = 0x7f020629

.field public static final port_digits_button_back_03:I = 0x7f02062a

.field public static final pt_clear_btn_bg_selector:I = 0x7f02062b

.field public static final pt_dark_ui_btn_blue_bg_selector:I = 0x7f02062c

.field public static final pt_dark_ui_btn_dark_bg_selector:I = 0x7f02062d

.field public static final pt_dark_ui_btn_light_bg_selector:I = 0x7f02062e

.field public static final pt_dark_ui_btn_num_01_bg_selector:I = 0x7f02062f

.field public static final pt_dark_ui_btn_num_02_bg_selector:I = 0x7f020630

.field public static final pt_dark_ui_btn_num_03_bg_selector:I = 0x7f020631

.field public static final pt_dark_ui_btn_num_04_bg_selector:I = 0x7f020632

.field public static final pt_dark_ui_btn_num_05_bg_selector:I = 0x7f020633

.field public static final pt_dark_ui_btn_num_06_bg_selector:I = 0x7f020634

.field public static final pt_dark_ui_btn_num_07_bg_selector:I = 0x7f020635

.field public static final pt_dark_ui_btn_num_08_bg_selector:I = 0x7f020636

.field public static final pt_dark_ui_btn_num_09_bg_selector:I = 0x7f020637

.field public static final pt_dark_ui_btn_num_10_bg_selector:I = 0x7f020638

.field public static final pt_dark_ui_btn_num_11_bg_selector:I = 0x7f020639

.field public static final pt_dark_ui_btn_num_12_bg_selector:I = 0x7f02063a

.field public static final pt_dark_ui_btn_num_13_bg_selector:I = 0x7f02063b

.field public static final pt_dark_ui_btn_num_14_bg_selector:I = 0x7f02063c

.field public static final pt_dark_ui_btn_num_15_bg_selector:I = 0x7f02063d

.field public static final pt_dark_ui_btn_num_16_bg_selector:I = 0x7f02063e

.field public static final pt_dark_ui_btn_num_17_bg_selector:I = 0x7f02063f

.field public static final pt_dark_ui_btn_num_18_bg_selector:I = 0x7f020640

.field public static final pt_dark_ui_btn_num_19_bg_selector:I = 0x7f020641

.field public static final pt_dark_ui_btn_num_20_bg_selector:I = 0x7f020642

.field public static final pt_dark_ui_btn_onehand_num_05_bg_selector:I = 0x7f020643

.field public static final pt_dark_ui_btn_onehand_num_06_bg_selector:I = 0x7f020644

.field public static final pt_dark_ui_btn_onehand_num_07_bg_selector:I = 0x7f020645

.field public static final pt_dark_ui_btn_onehand_num_09_bg_selector:I = 0x7f020646

.field public static final pt_dark_ui_btn_onehand_num_10_bg_selector:I = 0x7f020647

.field public static final pt_dark_ui_btn_onehand_num_11_bg_selector:I = 0x7f020648

.field public static final pt_dark_ui_btn_onehand_num_13_bg_selector:I = 0x7f020649

.field public static final pt_dark_ui_btn_onehand_num_14_bg_selector:I = 0x7f02064a

.field public static final pt_dark_ui_btn_onehand_num_15_bg_selector:I = 0x7f02064b

.field public static final pt_dark_ui_btn_onehand_num_17_bg_selector:I = 0x7f02064c

.field public static final pt_dark_ui_btn_onehand_num_18_bg_selector:I = 0x7f02064d

.field public static final pt_dark_ui_btn_onehand_num_19_bg_selector:I = 0x7f02064e

.field public static final pt_dark_ui_btn_orange_bg_selector:I = 0x7f02064f

.field public static final pt_dark_ui_panel_handle_selector:I = 0x7f020650

.field public static final pt_ext_btn_bg_selector:I = 0x7f020651

.field public static final pt_ext_icon_back_selector:I = 0x7f020652

.field public static final pt_ext_icon_bracket_selector:I = 0x7f020653

.field public static final pt_ext_icon_divison_selector:I = 0x7f020654

.field public static final pt_ext_icon_dot_selector:I = 0x7f020655

.field public static final pt_ext_icon_multi_selector:I = 0x7f020656

.field public static final pt_ext_icon_plus_selector:I = 0x7f020657

.field public static final pt_ext_icon_plusminus_selector:I = 0x7f020658

.field public static final pt_ext_icon_sub_selector:I = 0x7f020659

.field public static final pt_func_btn_bg_selector:I = 0x7f02065a

.field public static final pt_func_icon_c_selector:I = 0x7f02065b

.field public static final pt_func_icon_sum_selector:I = 0x7f02065c

.field public static final pt_new_ui_btn_bg_selector:I = 0x7f02065d

.field public static final pt_new_ui_btn_bg_selector_blue:I = 0x7f02065e

.field public static final pt_new_ui_btn_bg_selector_dark:I = 0x7f02065f

.field public static final pt_new_ui_btn_bg_selector_light:I = 0x7f020660

.field public static final pt_new_ui_btn_bg_selector_orange:I = 0x7f020661

.field public static final pt_new_ui_onehand_btn_bg_selector_blue:I = 0x7f020662

.field public static final pt_new_ui_onehand_btn_bg_selector_dark:I = 0x7f020663

.field public static final pt_new_ui_onehand_btn_bg_selector_light:I = 0x7f020664

.field public static final pt_new_ui_onehand_btn_bg_selector_orange:I = 0x7f020665

.field public static final pt_nor_btn_bg_selector:I = 0x7f020666

.field public static final pt_nor_icon_0_selector:I = 0x7f020667

.field public static final pt_nor_icon_1_selector:I = 0x7f020668

.field public static final pt_nor_icon_2_selector:I = 0x7f020669

.field public static final pt_nor_icon_3_selector:I = 0x7f02066a

.field public static final pt_nor_icon_4_selector:I = 0x7f02066b

.field public static final pt_nor_icon_5_selector:I = 0x7f02066c

.field public static final pt_nor_icon_6_selector:I = 0x7f02066d

.field public static final pt_nor_icon_7_selector:I = 0x7f02066e

.field public static final pt_nor_icon_8_selector:I = 0x7f02066f

.field public static final pt_nor_icon_9_selector:I = 0x7f020670

.field public static final pt_white_ui_btn_blue_bg_selector:I = 0x7f020671

.field public static final pt_white_ui_btn_dark_bg_selector:I = 0x7f020672

.field public static final pt_white_ui_btn_fn_clear_selector:I = 0x7f020673

.field public static final pt_white_ui_btn_fn_equal_selector:I = 0x7f020674

.field public static final pt_white_ui_btn_icon_back_selector:I = 0x7f020675

.field public static final pt_white_ui_btn_icon_bracket_selector:I = 0x7f020676

.field public static final pt_white_ui_btn_icon_division_selector:I = 0x7f020677

.field public static final pt_white_ui_btn_icon_multiplication_selector:I = 0x7f020678

.field public static final pt_white_ui_btn_icon_plus_selector:I = 0x7f020679

.field public static final pt_white_ui_btn_icon_subtraction_selector:I = 0x7f02067a

.field public static final pt_white_ui_btn_light_bg_selector:I = 0x7f02067b

.field public static final pt_white_ui_btn_num_00_selector:I = 0x7f02067c

.field public static final pt_white_ui_btn_num_01_selector:I = 0x7f02067d

.field public static final pt_white_ui_btn_num_02_selector:I = 0x7f02067e

.field public static final pt_white_ui_btn_num_03_selector:I = 0x7f02067f

.field public static final pt_white_ui_btn_num_04_selector:I = 0x7f020680

.field public static final pt_white_ui_btn_num_05_selector:I = 0x7f020681

.field public static final pt_white_ui_btn_num_06_selector:I = 0x7f020682

.field public static final pt_white_ui_btn_num_07_selector:I = 0x7f020683

.field public static final pt_white_ui_btn_num_08_selector:I = 0x7f020684

.field public static final pt_white_ui_btn_num_09_selector:I = 0x7f020685

.field public static final pt_white_ui_btn_num_dot_selector:I = 0x7f020686

.field public static final pt_white_ui_btn_num_plusminus_selector:I = 0x7f020687

.field public static final pt_white_ui_btn_orange_bg_selector:I = 0x7f020688

.field public static final pt_white_ui_handler_close_selector:I = 0x7f020689

.field public static final pt_white_ui_handler_open_selector:I = 0x7f02068a

.field public static final pt_white_ui_panel_handle_selector:I = 0x7f02068b

.field public static final ripple_effect_button:I = 0x7f02068c

.field public static final ripple_effect_clear_button:I = 0x7f02068d

.field public static final ripple_effect_panel_button:I = 0x7f02068e

.field public static final traybar_icon:I = 0x7f02068f

.field public static final traybar_icon_calculator:I = 0x7f020690

.field public static final traybar_icon_calculator_focus:I = 0x7f020691

.field public static final traybar_icon_calculator_focus_w:I = 0x7f020692

.field public static final traybar_icon_calculator_w:I = 0x7f020693

.field public static final traybar_thumbnail_calculator:I = 0x7f020694

.field public static final tw_ab_transparent_dark_holo:I = 0x7f020695

.field public static final tw_ab_transparent_h_holo_dark:I = 0x7f020696

.field public static final tw_ab_transparent_light_holo:I = 0x7f020697

.field public static final tw_action_bar_icon_clear_history:I = 0x7f020698

.field public static final tw_action_bar_icon_dim_clear_history:I = 0x7f020699

.field public static final tw_action_bar_icon_more_holo_light:I = 0x7f02069a

.field public static final tw_action_bar_icon_onehand_off:I = 0x7f02069b

.field public static final tw_action_bar_icon_onehand_on:I = 0x7f02069c

.field public static final tw_action_bar_icon_sciencific:I = 0x7f02069d

.field public static final tw_action_bar_icon_simple_calculator:I = 0x7f02069e

.field public static final tw_action_bar_icon_text_size:I = 0x7f02069f

.field public static final tw_action_bar_icon_text_size_dim:I = 0x7f0206a0

.field public static final tw_action_bar_info:I = 0x7f0206a1

.field public static final tw_btn_default_focused_holo_dark:I = 0x7f0206a2

.field public static final tw_btn_default_focused_holo_light:I = 0x7f0206a3

.field public static final tw_btn_default_mtrl:I = 0x7f0206a4

.field public static final tw_btn_default_normal_holo_dark:I = 0x7f0206a5

.field public static final tw_btn_default_normal_holo_light:I = 0x7f0206a6

.field public static final tw_btn_default_pressed_holo_dark:I = 0x7f0206a7

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f0206a8

.field public static final tw_ic_menu_menu_holo_light:I = 0x7f0206a9

.field public static final tw_ic_menu_moreoverflow_normal_holo_dark:I = 0x7f0206aa

.field public static final tw_list_divider_holo_dark:I = 0x7f0206ab

.field public static final tw_list_divider_holo_light:I = 0x7f0206ac

.field public static final tw_list_pressed_holo_light:I = 0x7f0206ad


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
