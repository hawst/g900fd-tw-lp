.class Lcom/sec/android/app/popupcalculator/Calculator$12;
.super Ljava/lang/Object;
.source "Calculator.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/Calculator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 0

    .prologue
    .line 3721
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 2
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 3745
    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-- onModeChanged -- "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3746
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->savePanelState()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1800(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3747
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$2400(Lcom/sec/android/app/popupcalculator/Calculator;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3748
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1400(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3749
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->loadPanelState()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$2000(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3750
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v1

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mIsScaleWindow:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$2402(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 3751
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->invalidateOptionsMenu()V

    .line 3752
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 3731
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->isPenMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3732
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mDisplay:Lcom/sec/android/app/popupcalculator/CalculatorEditText;
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$000(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/CalculatorEditText;

    move-result-object v0

    if-nez v0, :cond_1

    .line 3741
    :cond_0
    :goto_0
    return-void

    .line 3734
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->savePanelState()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1800(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3735
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->saveCurrentText()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$2200(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3736
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    const v1, 0x7f040005

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupcalculator/Calculator;->setContentView(I)V

    .line 3737
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->initControls()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$2300(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3738
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1400(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3739
    iget-object v0, p0, Lcom/sec/android/app/popupcalculator/Calculator$12;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->loadPanelState()V
    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->access$2000(Lcom/sec/android/app/popupcalculator/Calculator;)V

    goto :goto_0
.end method

.method public onZoneChanged(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 3725
    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-- onZoneChanged -- "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 3726
    return-void
.end method
