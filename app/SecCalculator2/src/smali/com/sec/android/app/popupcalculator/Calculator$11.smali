.class Lcom/sec/android/app/popupcalculator/Calculator$11;
.super Ljava/lang/Object;
.source "Calculator.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupcalculator/Calculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupcalculator/Calculator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupcalculator/Calculator;)V
    .locals 0

    .prologue
    .line 3621
    iput-object p1, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3623
    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mOnehandAdaptableOperation:Z
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->access$2100()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3625
    iget-object v1, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    invoke-virtual {v1}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 3661
    :cond_0
    :goto_0
    return-void

    .line 3628
    :pswitch_0
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getFlatMotionContext()Landroid/hardware/scontext/SContextFlatMotion;

    move-result-object v0

    .line 3629
    .local v0, "flatMotionContext":Landroid/hardware/scontext/SContextFlatMotion;
    invoke-virtual {v0}, Landroid/hardware/scontext/SContextFlatMotion;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 3654
    :goto_1
    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1200()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSContextChanged() flat motion mIsFlatMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z
    invoke-static {}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1300()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3631
    :pswitch_1
    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z
    invoke-static {v3}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1302(Z)Z

    goto :goto_1

    .line 3636
    :pswitch_2
    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->isFlatMode:Z
    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1302(Z)Z

    .line 3637
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->getOneHandSwitchStateValue()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    goto :goto_1

    .line 3639
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/popupcalculator/Calculator;->access$702(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 3640
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1400(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3641
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$500(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/EventListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupcalculator/EventListener;->onPanelOpened(Lcom/sec/android/widgetapp/calculator/Panel;)V

    goto :goto_1

    .line 3644
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # setter for: Lcom/sec/android/app/popupcalculator/Calculator;->mLeft:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/popupcalculator/Calculator;->access$702(Lcom/sec/android/app/popupcalculator/Calculator;Z)Z

    .line 3645
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # invokes: Lcom/sec/android/app/popupcalculator/Calculator;->refreshView()V
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$1400(Lcom/sec/android/app/popupcalculator/Calculator;)V

    .line 3646
    iget-object v1, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mListener:Lcom/sec/android/app/popupcalculator/EventListener;
    invoke-static {v1}, Lcom/sec/android/app/popupcalculator/Calculator;->access$500(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/app/popupcalculator/EventListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/popupcalculator/Calculator$11;->this$0:Lcom/sec/android/app/popupcalculator/Calculator;

    # getter for: Lcom/sec/android/app/popupcalculator/Calculator;->mPanel:Lcom/sec/android/widgetapp/calculator/Panel;
    invoke-static {v2}, Lcom/sec/android/app/popupcalculator/Calculator;->access$600(Lcom/sec/android/app/popupcalculator/Calculator;)Lcom/sec/android/widgetapp/calculator/Panel;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupcalculator/EventListener;->onPanelOpened(Lcom/sec/android/widgetapp/calculator/Panel;)V

    goto :goto_1

    .line 3625
    nop

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
    .end packed-switch

    .line 3629
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 3637
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
