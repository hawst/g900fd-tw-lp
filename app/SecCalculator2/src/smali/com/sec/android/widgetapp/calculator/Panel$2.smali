.class Lcom/sec/android/widgetapp/calculator/Panel$2;
.super Ljava/lang/Object;
.source "Panel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/calculator/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/calculator/Panel;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel$2;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 530
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$2;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mBringToFront:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$800(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$2;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->bringToFront()V

    .line 533
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$2;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->initChange()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$2;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$2;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v1, v1, Lcom/sec/android/widgetapp/calculator/Panel;->startAnimation:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->post(Ljava/lang/Runnable;)Z

    .line 535
    :cond_1
    return-void
.end method
