.class Lcom/sec/android/widgetapp/calculator/Panel$7;
.super Ljava/lang/Object;
.source "Panel.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/calculator/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/calculator/Panel;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 0

    .prologue
    .line 1023
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x1

    .line 1025
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1026
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1800(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1027
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mLeftArrow:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3300(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1028
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mRightArrow:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3400(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1029
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsPanelSwitching:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3502(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z

    .line 1030
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1033
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 1036
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1037
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1800(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1038
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mLeftArrow:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3300(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1039
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mRightArrow:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3400(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1040
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/popupcalculator/Calculator;

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1041
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$7;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsPanelSwitching:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3502(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z

    .line 1042
    return-void
.end method
