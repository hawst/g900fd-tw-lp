.class Lcom/sec/android/widgetapp/calculator/Panel$1;
.super Ljava/lang/Object;
.source "Panel.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/calculator/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field initX:I

.field initY:I

.field setInitialPosition:Z

.field final synthetic this$0:Lcom/sec/android/widgetapp/calculator/Panel;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 451
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v3

    sget-object v4, Lcom/sec/android/widgetapp/calculator/Panel$State;->ANIMATING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v3, v4, :cond_1

    .line 524
    :cond_0
    :goto_0
    return v1

    .line 453
    :cond_1
    sget-boolean v3, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    if-eqz v3, :cond_2

    .line 454
    sput-boolean v1, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    .line 455
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->invalidate()V

    .line 457
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 458
    .local v0, "action":I
    if-nez v0, :cond_6

    .line 460
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$100(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 461
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$100(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-interface {v4, v5}, Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;->onPanelClicked(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v4

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTouchable:Z
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$202(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z

    .line 462
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTouchable:Z
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$200(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 463
    goto :goto_0

    .line 478
    :cond_4
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$300(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 479
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z
    invoke-static {v3, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$302(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z

    .line 517
    :cond_5
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mGestureDetector:Landroid/view/GestureDetector;
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$700(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/GestureDetector;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mGestureDetector:Landroid/view/GestureDetector;
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$700(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/GestureDetector;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 518
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTouchable:Z
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$200(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 519
    goto :goto_0

    .line 492
    :cond_6
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTouchable:Z
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$200(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 493
    goto :goto_0

    .line 495
    :cond_7
    if-ne v0, v2, :cond_8

    .line 496
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$300(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 497
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z
    invoke-static {v3, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$302(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z

    .line 500
    :cond_8
    const/4 v3, 0x2

    if-ne v0, v3, :cond_a

    .line 501
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v3

    sget-object v4, Lcom/sec/android/widgetapp/calculator/Panel$State;->ABOUT_TO_ANIMATE:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v3, v4, :cond_9

    move v1, v2

    .line 502
    goto/16 :goto_0

    .line 503
    :cond_9
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$300(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 507
    :cond_a
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->setInitialPosition:Z

    if-eqz v3, :cond_b

    .line 508
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initX:I

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentWidth:I
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$400(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v4

    mul-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initX:I

    .line 509
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initY:I

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentHeight:I
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$500(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v4

    mul-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initY:I

    .line 510
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mGestureListener:Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;
    invoke-static {v3}, Lcom/sec/android/widgetapp/calculator/Panel;->access$600(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initX:I

    iget v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initY:I

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->setScroll(II)V

    .line 511
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->setInitialPosition:Z

    .line 512
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initX:I

    neg-int v3, v3

    iput v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initX:I

    .line 513
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initY:I

    neg-int v3, v3

    iput v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initY:I

    .line 515
    :cond_b
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->initY:I

    int-to-float v4, v4

    invoke-virtual {p2, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    goto/16 :goto_1

    .line 521
    :cond_c
    if-eq v0, v2, :cond_d

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 522
    :cond_d
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$1;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v3, v3, Lcom/sec/android/widgetapp/calculator/Panel;->startAnimation:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method
