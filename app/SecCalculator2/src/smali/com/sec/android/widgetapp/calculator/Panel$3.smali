.class Lcom/sec/android/widgetapp/calculator/Panel$3;
.super Ljava/lang/Object;
.source "Panel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/calculator/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/calculator/Panel;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 555
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setClickable(Z)V

    .line 558
    const/4 v12, 0x0

    .local v12, "fromXDelta":I
    const/4 v15, 0x0

    .local v15, "toXDelta":I
    const/4 v13, 0x0

    .local v13, "fromYDelta":I
    const/16 v16, 0x0

    .line 560
    .local v16, "toYDelta":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->FLYING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_1

    .line 561
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_8

    :cond_0
    const/4 v5, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mVelocity:F
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1200(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v6

    const/4 v8, 0x0

    cmpl-float v6, v6, v8

    if-lez v6, :cond_9

    const/4 v6, 0x1

    :goto_1
    xor-int/2addr v5, v6

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v7, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1002(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z

    .line 564
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1300(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_10

    .line 565
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentHeight:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$500(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v14

    .line 566
    .local v14, "height":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 567
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    if-nez v5, :cond_a

    neg-int v13, v14

    .line 571
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->TRACKING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_e

    .line 572
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    int-to-float v6, v13

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v6

    move/from16 v0, v16

    int-to-float v7, v0

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 573
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-nez v5, :cond_d

    const/4 v5, 0x1

    :goto_3
    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v6, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1002(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z

    .line 574
    move/from16 v16, v13

    .line 576
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    float-to-int v13, v5

    .line 580
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->FLYING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mLinearFlying:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1500(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 582
    const/high16 v5, 0x447a0000    # 1000.0f

    sub-int v6, v16, v13

    int-to-float v6, v6

    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mVelocity:F
    invoke-static {v7}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1200(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v7

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F
    :try_end_0
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v10, v5

    .line 588
    .local v10, "calculatedDuration":I
    :goto_5
    const/16 v5, 0x14

    invoke-static {v10, v5}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 631
    .end local v14    # "height":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1402(Lcom/sec/android/widgetapp/calculator/Panel;F)F

    move-result v6

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F
    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1702(Lcom/sec/android/widgetapp/calculator/Panel;F)F

    .line 632
    if-nez v10, :cond_1a

    .line 633
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->READY:Lcom/sec/android/widgetapp/calculator/Panel$State;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$002(Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/widgetapp/calculator/Panel$State;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    .line 634
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 635
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1800(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 637
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 638
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0e0015

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_19

    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mNoHistory:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2200(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v5

    if-nez v5, :cond_19

    .line 640
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyHeight:F
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2300(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v6

    float-to-int v6, v6

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 641
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->requestLayout()V

    .line 647
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # invokes: Lcom/sec/android/widgetapp/calculator/Panel;->postProcess()V
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2500(Lcom/sec/android/widgetapp/calculator/Panel;)V

    .line 692
    :goto_8
    return-void

    .line 561
    .end local v10    # "calculatedDuration":I
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_1

    .restart local v14    # "height":I
    :cond_a
    move v13, v14

    .line 567
    goto/16 :goto_2

    .line 569
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    if-nez v5, :cond_c

    neg-int v0, v14

    move/from16 v16, v0

    :goto_9
    goto/16 :goto_2

    :cond_c
    move/from16 v16, v14

    goto :goto_9

    .line 573
    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 577
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->FLYING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_3

    .line 578
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    float-to-int v13, v5

    goto/16 :goto_4

    .line 584
    :catch_0
    move-exception v11

    .line 585
    .local v11, "e":Ljava/lang/ArithmeticException;
    sub-int v5, v16, v13

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    mul-int/lit16 v10, v5, 0x3e8

    .restart local v10    # "calculatedDuration":I
    goto/16 :goto_5

    .line 591
    .end local v10    # "calculatedDuration":I
    .end local v11    # "e":Ljava/lang/ArithmeticException;
    :cond_f
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1600(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    sub-int v6, v16, v13

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    mul-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentHeight:I
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$500(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v6

    div-int v10, v5, v6
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    .restart local v10    # "calculatedDuration":I
    goto/16 :goto_6

    .line 593
    .end local v10    # "calculatedDuration":I
    :catch_1
    move-exception v11

    .line 594
    .restart local v11    # "e":Ljava/lang/ArithmeticException;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1600(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    sub-int v6, v16, v13

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    mul-int v10, v5, v6

    .restart local v10    # "calculatedDuration":I
    goto/16 :goto_6

    .line 598
    .end local v10    # "calculatedDuration":I
    .end local v11    # "e":Ljava/lang/ArithmeticException;
    .end local v14    # "height":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentWidth:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$400(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v17

    .line 599
    .local v17, "width":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 600
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_13

    move/from16 v0, v17

    neg-int v12, v0

    .line 604
    :goto_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->TRACKING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_17

    .line 605
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1700(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    int-to-float v6, v12

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1700(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v6

    int-to-float v7, v15

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_11

    .line 606
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-nez v5, :cond_16

    const/4 v5, 0x1

    :goto_b
    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v6, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1002(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z

    .line 607
    move v15, v12

    .line 609
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1700(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    float-to-int v12, v5

    .line 613
    :cond_12
    :goto_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->FLYING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_18

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mLinearFlying:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1500(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 615
    const/high16 v5, 0x447a0000    # 1000.0f

    sub-int v6, v15, v12

    int-to-float v6, v6

    :try_start_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mVelocity:F
    invoke-static {v7}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1200(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v7

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F
    :try_end_2
    .catch Ljava/lang/ArithmeticException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v10, v5

    .line 620
    .restart local v10    # "calculatedDuration":I
    :goto_d
    const/16 v5, 0x14

    invoke-static {v10, v5}, Ljava/lang/Math;->max(II)I

    move-result v10

    goto/16 :goto_6

    .end local v10    # "calculatedDuration":I
    :cond_13
    move/from16 v12, v17

    .line 600
    goto :goto_a

    .line 602
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_15

    move/from16 v0, v17

    neg-int v15, v0

    :goto_e
    goto/16 :goto_a

    :cond_15
    move/from16 v15, v17

    goto :goto_e

    .line 606
    :cond_16
    const/4 v5, 0x0

    goto :goto_b

    .line 610
    :cond_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->FLYING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_12

    .line 611
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1700(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    float-to-int v12, v5

    goto :goto_c

    .line 617
    :catch_2
    move-exception v11

    .line 618
    .restart local v11    # "e":Ljava/lang/ArithmeticException;
    sub-int v5, v15, v12

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    mul-int/lit16 v10, v5, 0x3e8

    .restart local v10    # "calculatedDuration":I
    goto :goto_d

    .line 623
    .end local v10    # "calculatedDuration":I
    .end local v11    # "e":Ljava/lang/ArithmeticException;
    :cond_18
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1600(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    sub-int v6, v15, v12

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    mul-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentWidth:I
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$400(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v6

    div-int v10, v5, v6
    :try_end_3
    .catch Ljava/lang/ArithmeticException; {:try_start_3 .. :try_end_3} :catch_3

    .restart local v10    # "calculatedDuration":I
    goto/16 :goto_6

    .line 625
    .end local v10    # "calculatedDuration":I
    :catch_3
    move-exception v11

    .line 626
    .restart local v11    # "e":Ljava/lang/ArithmeticException;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1600(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v5

    sub-int v6, v15, v12

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    mul-int v10, v5, v6

    .restart local v10    # "calculatedDuration":I
    goto/16 :goto_6

    .line 643
    .end local v11    # "e":Ljava/lang/ArithmeticException;
    .end local v17    # "width":I
    :cond_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinearHeight:F
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v6

    float-to-int v6, v6

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 644
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->requestLayout()V

    goto/16 :goto_7

    .line 653
    :cond_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_21

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_21

    .line 654
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0e0015

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isVienna(Landroid/content/Context;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_20

    :cond_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mNoHistory:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2200(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v5

    if-nez v5, :cond_20

    .line 656
    :cond_1c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v3

    .line 657
    .local v3, "addAnimationView":Landroid/view/View;
    const/4 v4, 0x0

    .line 663
    .local v4, "txtCalc_visible_check":Z
    :goto_f
    new-instance v2, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinearHeight:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    int-to-float v6, v12

    int-to-float v7, v15

    int-to-float v8, v13

    move/from16 v0, v16

    int-to-float v9, v0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;-><init>(Landroid/view/View;ZFFFFF)V

    .line 670
    .end local v3    # "addAnimationView":Landroid/view/View;
    .end local v4    # "txtCalc_visible_check":Z
    .local v2, "animation":Landroid/view/animation/TranslateAnimation;
    :goto_10
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/view/animation/TranslateAnimation;->setFillEnabled(Z)V

    .line 672
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->ABOUT_TO_ANIMATE:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_1d

    .line 673
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/animation/TranslateAnimation;->setFillBefore(Z)V

    .line 675
    :cond_1d
    int-to-long v6, v10

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 676
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->animationListener:Landroid/view/animation/Animation$AnimationListener;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2600(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 678
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v5

    sget-object v6, Lcom/sec/android/widgetapp/calculator/Panel$State;->FLYING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v5, v6, :cond_22

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mLinearFlying:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1500(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-eqz v5, :cond_22

    .line 679
    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v5}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 682
    :cond_1e
    :goto_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 683
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a000b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1f

    .line 684
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ActionBar;->hide()V

    .line 691
    :cond_1f
    :goto_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v5, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_8

    .line 659
    .end local v2    # "animation":Landroid/view/animation/TranslateAnimation;
    :cond_20
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v3

    .line 660
    .restart local v3    # "addAnimationView":Landroid/view/View;
    const/4 v4, 0x1

    .restart local v4    # "txtCalc_visible_check":Z
    goto/16 :goto_f

    .line 667
    .end local v3    # "addAnimationView":Landroid/view/View;
    .end local v4    # "txtCalc_visible_check":Z
    :cond_21
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    int-to-float v5, v12

    int-to-float v6, v15

    int-to-float v7, v13

    move/from16 v0, v16

    int-to-float v8, v0

    invoke-direct {v2, v5, v6, v7, v8}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v2    # "animation":Landroid/view/animation/TranslateAnimation;
    goto/16 :goto_10

    .line 680
    :cond_22
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mInterpolator:Landroid/view/animation/Interpolator;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2700(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/animation/Interpolator;

    move-result-object v5

    if-eqz v5, :cond_1e

    .line 681
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mInterpolator:Landroid/view/animation/Interpolator;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2700(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    goto :goto_11

    .line 686
    :cond_23
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a000b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1f

    .line 687
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsLandscapeMode:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2800(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v5

    if-nez v5, :cond_1f

    .line 688
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/calculator/Panel$3;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v5}, Lcom/sec/android/app/popupcalculator/Calculator;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ActionBar;->show()V

    goto :goto_12
.end method
