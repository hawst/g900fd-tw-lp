.class Lcom/sec/android/widgetapp/calculator/Panel$4;
.super Ljava/lang/Object;
.source "Panel.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/calculator/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/calculator/Panel;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 0

    .prologue
    .line 758
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x1

    .line 760
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 761
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1800(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 765
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 766
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0e0015

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isMillet(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mNoHistory:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2200(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyHeight:F
    invoke-static {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2300(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 769
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 775
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # invokes: Lcom/sec/android/widgetapp/calculator/Panel;->postProcess()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2500(Lcom/sec/android/widgetapp/calculator/Panel;)V

    .line 776
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    sget-object v1, Lcom/sec/android/widgetapp/calculator/Panel$State;->READY:Lcom/sec/android/widgetapp/calculator/Panel$State;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$002(Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/widgetapp/calculator/Panel$State;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    .line 777
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 778
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 779
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1800(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 780
    return-void

    .line 763
    :cond_2
    sput-boolean v2, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    goto/16 :goto_0

    .line 771
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinearHeight:F
    invoke-static {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 772
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_1
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 783
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 786
    sget-boolean v0, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    if-eqz v0, :cond_0

    .line 787
    sput-boolean v2, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    .line 788
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->invalidate()V

    .line 790
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 791
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$100(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 792
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$100(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-interface {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;->onPanelClosed(Lcom/sec/android/widgetapp/calculator/Panel;)V

    .line 796
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    sget-object v1, Lcom/sec/android/widgetapp/calculator/Panel$State;->ANIMATING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$002(Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/widgetapp/calculator/Panel$State;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    .line 797
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 798
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 799
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1800(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 800
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/popupcalculator/Calculator;

    iget-object v0, v0, Lcom/sec/android/app/popupcalculator/Calculator;->clearBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 801
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$4;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 802
    return-void
.end method
