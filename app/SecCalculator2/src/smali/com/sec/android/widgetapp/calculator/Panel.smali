.class public Lcom/sec/android/widgetapp/calculator/Panel;
.super Landroid/widget/LinearLayout;
.source "Panel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;,
        Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;,
        Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;,
        Lcom/sec/android/widgetapp/calculator/Panel$State;
    }
.end annotation


# static fields
.field private static final ADVANCED_PANEL:I = 0x1

.field private static final BASIC_PANEL:I = 0x0

.field public static final BOTTOM:I = 0x1

.field public static final LEFT:I = 0x2

.field private static final MAJOR_MOVE:I = 0x3c

.field private static final NB_PANEL:I = 0x2

.field public static final RIGHT:I = 0x3

.field public static final TOP:I

.field public static mIsLarge:I

.field public static mIsTablet:I

.field public static stopInvalidate:Z


# instance fields
.field private animationListener:Landroid/view/animation/Animation$AnimationListener;

.field arrowBtnClickListener:Landroid/view/View$OnClickListener;

.field clickListener:Landroid/view/View$OnClickListener;

.field private inLeft:Landroid/view/animation/TranslateAnimation;

.field private inRight:Landroid/view/animation/TranslateAnimation;

.field private mAniEnd:Z

.field private mBringToFront:Z

.field private mChildPanels:[Landroid/view/View;

.field private mClosedHandle:Landroid/graphics/drawable/Drawable;

.field private mContent:Landroid/view/View;

.field private mContentHeight:I

.field private mContentId:I

.field private mContentWidth:I

.field private mContext:Landroid/content/Context;

.field private mCurrentView:I

.field private mDuration:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGestureListener:Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;

.field private mHandle:Landroid/view/View;

.field private mHandleId:I

.field private mHistroyFrame:Landroid/view/View;

.field private mHistroyHeight:F

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mIsLandscapeMode:Z

.field private mIsPanelSwitching:Z

.field private mIsShrinking:Z

.field private mLeftArrow:Landroid/view/View;

.field private mLinearFlying:Z

.field private mMeasureCheck:Z

.field private mMoveDirection:I

.field private mNoHistory:Landroid/widget/EditText;

.field private mOpenedHandle:Landroid/graphics/drawable/Drawable;

.field private mOrientation:I

.field private mPanelHandlePressed:Z

.field private mPanelState:Z

.field private mPosition:I

.field private mPreviousMove:I

.field private mRightArrow:Landroid/view/View;

.field private mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

.field private mTouchable:Z

.field private mTrackX:F

.field private mTrackY:F

.field private mTxtCalcLinear:Landroid/view/View;

.field private mTxtCalcLinearHeight:F

.field private mVelocity:F

.field private mWeight:F

.field private model:Ljava/lang/String;

.field private outLeft:Landroid/view/animation/TranslateAnimation;

.field private outRight:Landroid/view/animation/TranslateAnimation;

.field private panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

.field panelSwitchAnimation:Ljava/lang/Runnable;

.field private panelSwitchAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field startAnimation:Ljava/lang/Runnable;

.field touchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 185
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mAniEnd:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTouchable:Z

    .line 85
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z

    .line 122
    iput v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    .line 142
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsLandscapeMode:Z

    .line 144
    const-string v4, "ro.product.model"

    const-string v5, "Unknown"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;

    .line 146
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPanelHandlePressed:Z

    .line 153
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsPanelSwitching:Z

    .line 155
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPanelState:Z

    .line 174
    iput-object v7, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;

    .line 176
    iput-object v7, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;

    .line 180
    iput v6, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyHeight:F

    .line 182
    iput v6, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinearHeight:F

    .line 442
    new-instance v4, Lcom/sec/android/widgetapp/calculator/Panel$1;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/calculator/Panel$1;-><init>(Lcom/sec/android/widgetapp/calculator/Panel;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->touchListener:Landroid/view/View$OnTouchListener;

    .line 528
    new-instance v4, Lcom/sec/android/widgetapp/calculator/Panel$2;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/calculator/Panel$2;-><init>(Lcom/sec/android/widgetapp/calculator/Panel;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->clickListener:Landroid/view/View$OnClickListener;

    .line 551
    new-instance v4, Lcom/sec/android/widgetapp/calculator/Panel$3;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/calculator/Panel$3;-><init>(Lcom/sec/android/widgetapp/calculator/Panel;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->startAnimation:Ljava/lang/Runnable;

    .line 758
    new-instance v4, Lcom/sec/android/widgetapp/calculator/Panel$4;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/calculator/Panel$4;-><init>(Lcom/sec/android/widgetapp/calculator/Panel;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 983
    new-instance v4, Lcom/sec/android/widgetapp/calculator/Panel$5;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/calculator/Panel$5;-><init>(Lcom/sec/android/widgetapp/calculator/Panel;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->arrowBtnClickListener:Landroid/view/View$OnClickListener;

    .line 1013
    new-instance v4, Lcom/sec/android/widgetapp/calculator/Panel$6;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/calculator/Panel$6;-><init>(Lcom/sec/android/widgetapp/calculator/Panel;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelSwitchAnimation:Ljava/lang/Runnable;

    .line 1023
    new-instance v4, Lcom/sec/android/widgetapp/calculator/Panel$7;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/calculator/Panel$7;-><init>(Lcom/sec/android/widgetapp/calculator/Panel;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelSwitchAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 187
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0018

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    sput v4, Lcom/sec/android/widgetapp/calculator/Panel;->mIsTablet:I

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0027

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    sput v4, Lcom/sec/android/widgetapp/calculator/Panel;->mIsLarge:I

    .line 192
    sget-object v4, Lcom/sec/android/app/popupcalculator/R$styleable;->Panel:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 193
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v4, 0x12c

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I

    .line 194
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    .line 195
    const/4 v4, 0x4

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mLinearFlying:Z

    .line 196
    const/4 v4, 0x5

    invoke-virtual {v0, v4, v3, v2, v6}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v4

    iput v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mWeight:F

    .line 198
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mWeight:F

    cmpg-float v4, v4, v6

    if-ltz v4, :cond_0

    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mWeight:F

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 199
    :cond_0
    iput v6, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mWeight:F

    .line 201
    :cond_1
    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOpenedHandle:Landroid/graphics/drawable/Drawable;

    .line 202
    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mClosedHandle:Landroid/graphics/drawable/Drawable;

    .line 203
    const/4 v1, 0x0

    .line 204
    .local v1, "e":Ljava/lang/RuntimeException;
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandleId:I

    .line 206
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandleId:I

    if-nez v4, :cond_2

    .line 207
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .end local v1    # "e":Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": The handle attribute is required and must refer to a valid child."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 211
    .restart local v1    # "e":Ljava/lang/RuntimeException;
    :cond_2
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentId:I

    .line 213
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentId:I

    if-nez v4, :cond_3

    .line 214
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .end local v1    # "e":Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": The content attribute is required and must refer to a valid child."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 218
    .restart local v1    # "e":Ljava/lang/RuntimeException;
    :cond_3
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 220
    if-eqz v1, :cond_4

    .line 221
    throw v1

    .line 223
    :cond_4
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    if-eqz v4, :cond_5

    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    if-ne v4, v2, :cond_6

    :cond_5
    :goto_0
    iput v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I

    .line 224
    iget v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->setOrientation(I)V

    .line 225
    sget-object v2, Lcom/sec/android/widgetapp/calculator/Panel$State;->READY:Lcom/sec/android/widgetapp/calculator/Panel$State;

    iput-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    .line 226
    new-instance v2, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;-><init>(Lcom/sec/android/widgetapp/calculator/Panel;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureListener:Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;

    .line 227
    new-instance v2, Landroid/view/GestureDetector;

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureListener:Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;

    invoke-direct {v2, v4, v5}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureDetector:Landroid/view/GestureDetector;

    .line 228
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, v3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 229
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->setBaselineAligned(Z)V

    .line 230
    return-void

    :cond_6
    move v2, v3

    .line 223
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/widgetapp/calculator/Panel$State;)Lcom/sec/android/widgetapp/calculator/Panel$State;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/calculator/Panel$State;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/calculator/Panel;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mVelocity:F

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/widgetapp/calculator/Panel;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # F

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mVelocity:F

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/widgetapp/calculator/Panel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/widgetapp/calculator/Panel;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/widgetapp/calculator/Panel;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # F

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/widgetapp/calculator/Panel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mLinearFlying:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/widgetapp/calculator/Panel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/widgetapp/calculator/Panel;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/widgetapp/calculator/Panel;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # F

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/calculator/Panel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTouchable:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTouchable:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mNoHistory:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/widgetapp/calculator/Panel;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyHeight:F

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/widgetapp/calculator/Panel;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinearHeight:F

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->postProcess()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/animation/Animation$AnimationListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/animation/Interpolator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/widgetapp/calculator/Panel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsLandscapeMode:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/calculator/Panel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/widgetapp/calculator/Panel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->isJapanModel()Z

    move-result v0

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/widgetapp/calculator/Panel;FII)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # F
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/calculator/Panel;->ensureRange(FII)F

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/widgetapp/calculator/Panel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mMoveDirection:I

    return v0
.end method

.method static synthetic access$3202(Lcom/sec/android/widgetapp/calculator/Panel;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mMoveDirection:I

    return p1
.end method

.method static synthetic access$3300(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mLeftArrow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mRightArrow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/widgetapp/calculator/Panel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsPanelSwitching:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/calculator/Panel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentWidth:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/calculator/Panel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureListener:Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/calculator/Panel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mBringToFront:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/calculator/Panel;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    return-object v0
.end method

.method private ensureRange(FII)F
    .locals 1
    .param p1, "v"    # F
    .param p2, "min"    # I
    .param p3, "max"    # I

    .prologue
    .line 437
    int-to-float v0, p2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result p1

    .line 438
    int-to-float v0, p3

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p1

    .line 439
    return p1
.end method

.method private isJapanModel()Z
    .locals 2

    .prologue
    .line 1144
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;

    const-string v1, "SGH-N098"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;

    const-string v1, "SC-04F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;

    const-string v1, "SCH-J003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;

    const-string v1, "SCL23"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1146
    :cond_0
    const/4 v0, 0x1

    .line 1147
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private postProcess()V
    .locals 5

    .prologue
    const v4, 0x7f0a0005

    const/4 v3, 0x1

    .line 807
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mClosedHandle:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    .line 808
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0031

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 813
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v0

    const-string v1, "capuccino"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v0

    const-string v1, "americano"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->isJapanModel()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "kqlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 814
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mClosedHandle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 830
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    if-eqz v0, :cond_2

    .line 831
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    if-eqz v0, :cond_7

    .line 832
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    invoke-interface {v0, p0}, Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;->onPanelClosed(Lcom/sec/android/widgetapp/calculator/Panel;)V

    .line 833
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPanelState:Z

    .line 841
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 851
    return-void

    .line 816
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mClosedHandle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 818
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOpenedHandle:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 819
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0030

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 824
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v0

    const-string v1, "capuccino"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v0

    const-string v1, "americano"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->isJapanModel()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "kqlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-ne v0, v3, :cond_6

    .line 825
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOpenedHandle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 827
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOpenedHandle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 835
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    invoke-interface {v0, p0}, Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;->onPanelOpened(Lcom/sec/android/widgetapp/calculator/Panel;)V

    .line 836
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPanelState:Z

    goto :goto_1
.end method


# virtual methods
.method public PanelOpenwhenClearHistory()V
    .locals 1

    .prologue
    .line 976
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->startAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->post(Ljava/lang/Runnable;)Z

    .line 977
    invoke-direct {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->postProcess()V

    .line 978
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 414
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    sget-object v2, Lcom/sec/android/widgetapp/calculator/Panel$State;->ABOUT_TO_ANIMATE:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    if-nez v1, :cond_2

    .line 415
    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I

    if-ne v1, v3, :cond_6

    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentHeight:I

    .line 416
    .local v0, "delta":I
    :goto_0
    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    if-nez v1, :cond_1

    .line 417
    :cond_0
    neg-int v0, v0

    .line 419
    :cond_1
    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I

    if-ne v1, v3, :cond_7

    .line 420
    int-to-float v1, v0

    invoke-virtual {p1, v4, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 424
    .end local v0    # "delta":I
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    sget-object v2, Lcom/sec/android/widgetapp/calculator/Panel$State;->TRACKING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    sget-object v2, Lcom/sec/android/widgetapp/calculator/Panel$State;->FLYING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v1, v2, :cond_4

    .line 425
    :cond_3
    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F

    iget v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 429
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->isHardwareAccelerated()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-boolean v1, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    if-nez v1, :cond_5

    .line 430
    sput-boolean v3, Lcom/sec/android/widgetapp/calculator/Panel;->stopInvalidate:Z

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->invalidate()V

    .line 433
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 434
    return-void

    .line 415
    :cond_6
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentWidth:I

    goto :goto_0

    .line 422
    .restart local v0    # "delta":I
    :cond_7
    int-to-float v1, v0

    invoke-virtual {p1, v1, v4}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_1
.end method

.method public getAniEnd()Z
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mAniEnd:Z

    return v0
.end method

.method public getClosedHandle()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mClosedHandle:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getContent()Landroid/view/View;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    return-object v0
.end method

.method public getCurrentIndex()I
    .locals 1

    .prologue
    .line 1127
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    return v0
.end method

.method public getHandle()Landroid/view/View;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    return-object v0
.end method

.method public getNew800mdpi_PortraitPanelState()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPanelState:Z

    return v0
.end method

.method public initChange()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 539
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    sget-object v3, Lcom/sec/android/widgetapp/calculator/Panel$State;->READY:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    sget-object v3, Lcom/sec/android/widgetapp/calculator/Panel$State;->ABOUT_TO_ANIMATE:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v0, v3, :cond_2

    :cond_0
    move v1, v2

    .line 547
    :cond_1
    :goto_0
    return v1

    .line 542
    :cond_2
    sget-object v0, Lcom/sec/android/widgetapp/calculator/Panel$State;->ABOUT_TO_ANIMATE:Lcom/sec/android/widgetapp/calculator/Panel$State;

    iput-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    .line 543
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    .line 544
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    if-nez v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 543
    goto :goto_1
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPanelSwitching()Z
    .locals 1

    .prologue
    .line 1101
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsPanelSwitching:Z

    return v0
.end method

.method public isStateReady()Z
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    sget-object v1, Lcom/sec/android/widgetapp/calculator/Panel$State;->READY:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method moveLeft()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1069
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPreviousMove:I

    if-eq v0, v2, :cond_0

    .line 1070
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsPanelSwitching:Z

    .line 1071
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1072
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    aget-object v0, v0, v1

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1073
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1074
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1076
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    .line 1077
    iput v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPreviousMove:I

    .line 1079
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getCurrentEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1081
    :cond_0
    return-void
.end method

.method moveRight()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 1085
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPreviousMove:I

    if-eq v0, v2, :cond_0

    .line 1086
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsPanelSwitching:Z

    .line 1087
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1088
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    aget-object v0, v0, v1

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1089
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1090
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mChildPanels:[Landroid/view/View;

    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1092
    iget v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mCurrentView:I

    .line 1093
    iput v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPreviousMove:I

    .line 1095
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getCurrentEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1097
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 385
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 386
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 387
    .local v0, "parent":Landroid/view/ViewParent;
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    .line 388
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mBringToFront:Z

    .line 389
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 301
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 302
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandleId:I

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    .line 303
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    if-nez v3, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandleId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    .line 305
    .local v1, "name":Ljava/lang/String;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Your Panel must have a child View whose id attribute is \'R.id."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 308
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/widgetapp/calculator/Panel;->touchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 309
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/widgetapp/calculator/Panel;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 311
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentId:I

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    .line 312
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    if-nez v3, :cond_1

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandleId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    .line 314
    .restart local v1    # "name":Ljava/lang/String;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Your Panel must have a child View whose id attribute is \'R.id."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 319
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v7, :cond_7

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsLandscapeMode:Z

    .line 321
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->removeView(Landroid/view/View;)V

    .line 322
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->removeView(Landroid/view/View;)V

    .line 324
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    if-ne v3, v7, :cond_8

    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    :goto_1
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->addView(Landroid/view/View;)V

    .line 325
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I

    if-ne v3, v7, :cond_9

    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    :goto_2
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/calculator/Panel;->addView(Landroid/view/View;)V

    .line 327
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOpenedHandle:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_5

    .line 331
    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v3

    const-string v6, "capuccino"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v3

    const-string v6, "americano"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-direct {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->isJapanModel()Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "kqlte"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0a0005

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    if-ne v3, v4, :cond_a

    .line 332
    :cond_4
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    check-cast v3, Landroid/widget/ImageButton;

    iget-object v6, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOpenedHandle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 358
    :cond_5
    :goto_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setClickable(Z)V

    .line 359
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 361
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 362
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_6

    .line 363
    const/16 v3, 0x3031

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 366
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 367
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    const v6, 0x7f0b0030

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 372
    :goto_4
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mWeight:F

    const/4 v5, 0x0

    cmpg-float v3, v3, v5

    if-gtz v3, :cond_c

    .line 382
    :goto_5
    return-void

    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_7
    move v3, v5

    .line 319
    goto/16 :goto_0

    .line 324
    :cond_8
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    goto/16 :goto_1

    .line 325
    :cond_9
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    goto/16 :goto_2

    .line 334
    :cond_a
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOpenedHandle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 369
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_b
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    const v6, 0x7f0b0031

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 375
    :cond_c
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 376
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I

    if-ne v3, v4, :cond_d

    .line 377
    iput v8, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 381
    :goto_6
    iget-object v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_5

    .line 379
    :cond_d
    iput v8, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_6
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1063
    const/4 v0, 0x0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 408
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentWidth:I

    .line 410
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContentHeight:I

    .line 411
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x1

    .line 392
    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mWeight:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 394
    .local v0, "parent":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 395
    iget v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I

    if-ne v1, v3, :cond_1

    .line 396
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mWeight:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 403
    .end local v0    # "parent":Landroid/view/View;
    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mMeasureCheck:Z

    .line 404
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 405
    return-void

    .line 399
    .restart local v0    # "parent":Landroid/view/View;
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mWeight:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0
.end method

.method public onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldW"    # I
    .param p4, "oldH"    # I

    .prologue
    const/4 v3, 0x0

    .line 995
    move v0, p1

    .line 997
    .local v0, "mWidth":I
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v0

    invoke-direct {v1, v2, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inLeft:Landroid/view/animation/TranslateAnimation;

    .line 998
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    neg-int v2, v0

    int-to-float v2, v2

    invoke-direct {v1, v3, v2, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outLeft:Landroid/view/animation/TranslateAnimation;

    .line 999
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    neg-int v2, v0

    int-to-float v2, v2

    invoke-direct {v1, v2, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inRight:Landroid/view/animation/TranslateAnimation;

    .line 1000
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v0

    invoke-direct {v1, v3, v2, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outRight:Landroid/view/animation/TranslateAnimation;

    .line 1002
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inLeft:Landroid/view/animation/TranslateAnimation;

    iget v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1003
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outLeft:Landroid/view/animation/TranslateAnimation;

    iget v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1004
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inRight:Landroid/view/animation/TranslateAnimation;

    iget v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1005
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outRight:Landroid/view/animation/TranslateAnimation;

    iget v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1007
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inLeft:Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelSwitchAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1008
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outLeft:Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelSwitchAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1009
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inRight:Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelSwitchAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1010
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outRight:Landroid/view/animation/TranslateAnimation;

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelSwitchAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1011
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1052
    const/4 v0, 0x0

    return v0
.end method

.method public resetInstance()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1152
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureListener:Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;

    if-eqz v0, :cond_0

    .line 1153
    iput-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureListener:Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;

    .line 1156
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_1

    .line 1157
    iput-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1160
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inLeft:Landroid/view/animation/TranslateAnimation;

    if-eqz v0, :cond_2

    .line 1161
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1164
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outLeft:Landroid/view/animation/TranslateAnimation;

    if-eqz v0, :cond_3

    .line 1165
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outLeft:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1168
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inRight:Landroid/view/animation/TranslateAnimation;

    if-eqz v0, :cond_4

    .line 1169
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->inRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1172
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outRight:Landroid/view/animation/TranslateAnimation;

    if-eqz v0, :cond_5

    .line 1173
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->outRight:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1176
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 1177
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHandle:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->removeView(Landroid/view/View;)V

    .line 1179
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 1180
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->removeView(Landroid/view/View;)V

    .line 1183
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_8

    .line 1184
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/popupcalculator/Calculator;

    invoke-virtual {v0}, Lcom/sec/android/app/popupcalculator/Calculator;->getCurrentEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 1185
    iput-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContext:Landroid/content/Context;

    .line 1188
    :cond_8
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->startAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1189
    return-void
.end method

.method public setAddAnimationLayout(Landroid/view/View;Landroid/view/View;Landroid/widget/EditText;)V
    .locals 1
    .param p1, "histroyFrame"    # Landroid/view/View;
    .param p2, "txtCalcLinear"    # Landroid/view/View;
    .param p3, "nohistory"    # Landroid/widget/EditText;

    .prologue
    .line 1133
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1134
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;

    .line 1135
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyHeight:F

    .line 1137
    iput-object p2, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;

    .line 1138
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinearHeight:F

    .line 1139
    iput-object p3, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mNoHistory:Landroid/widget/EditText;

    .line 1141
    :cond_0
    return-void
.end method

.method public setAniEnd(Z)V
    .locals 0
    .param p1, "start"    # Z

    .prologue
    .line 292
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mAniEnd:Z

    .line 293
    return-void
.end method

.method public setClose()V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 277
    return-void
.end method

.method public setCurrentView(IZ)V
    .locals 0
    .param p1, "viewIndex"    # I
    .param p2, "animate"    # Z

    .prologue
    .line 1124
    return-void
.end method

.method public setNew800mdpi_PortraitPanelState(Z)V
    .locals 0
    .param p1, "panelState"    # Z

    .prologue
    .line 288
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mPanelState:Z

    .line 289
    return-void
.end method

.method public setOnPanelListener(Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;)V
    .locals 0
    .param p1, "onPanelListener"    # Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel;->panelListener:Lcom/sec/android/widgetapp/calculator/Panel$OnPanelListener;

    .line 240
    return-void
.end method

.method public setOpen(ZZ)Z
    .locals 4
    .param p1, "open"    # Z
    .param p2, "animate"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 258
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    sget-object v3, Lcom/sec/android/widgetapp/calculator/Panel$State;->READY:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v0, v3, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->isOpen()Z

    move-result v0

    xor-int/2addr v0, p1

    if-eqz v0, :cond_4

    .line 259
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    .line 260
    if-eqz p2, :cond_2

    .line 261
    sget-object v0, Lcom/sec/android/widgetapp/calculator/Panel$State;->ABOUT_TO_ANIMATE:Lcom/sec/android/widgetapp/calculator/Panel$State;

    iput-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;

    .line 262
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z

    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->startAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->post(Ljava/lang/Runnable;)Z

    .line 272
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 259
    goto :goto_0

    .line 267
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel;->mContent:Landroid/view/View;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 268
    invoke-direct {p0}, Lcom/sec/android/widgetapp/calculator/Panel;->postProcess()V

    goto :goto_1

    .line 267
    :cond_3
    const/16 v2, 0x8

    goto :goto_2

    :cond_4
    move v1, v2

    .line 272
    goto :goto_1
.end method
