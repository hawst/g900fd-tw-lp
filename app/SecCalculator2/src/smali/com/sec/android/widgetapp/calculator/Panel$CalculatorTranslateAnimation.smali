.class public Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;
.super Landroid/view/animation/TranslateAnimation;
.source "Panel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/calculator/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CalculatorTranslateAnimation"
.end annotation


# instance fields
.field default_height:F

.field mView:Landroid/view/View;

.field mfromYDelta:F

.field mtoYDelta:F

.field mtxtCalc_visible_check:Z

.field temp_y:F


# direct methods
.method public constructor <init>(Landroid/view/View;ZFFFFF)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "visible_check"    # Z
    .param p3, "txtCalc_height"    # F
    .param p4, "fromXDelta"    # F
    .param p5, "toXDelta"    # F
    .param p6, "fromYDelta"    # F
    .param p7, "toYDelta"    # F

    .prologue
    const/4 v1, 0x0

    .line 710
    invoke-direct {p0, p7, p7, p7, p7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 696
    iput v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mfromYDelta:F

    .line 698
    iput v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtoYDelta:F

    .line 700
    iput v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->temp_y:F

    .line 702
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mView:Landroid/view/View;

    .line 704
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtxtCalc_visible_check:Z

    .line 706
    iput v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->default_height:F

    .line 711
    iput p6, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mfromYDelta:F

    .line 712
    iput p7, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtoYDelta:F

    .line 713
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mView:Landroid/view/View;

    .line 714
    iput-boolean p2, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtxtCalc_visible_check:Z

    .line 715
    iput p3, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->default_height:F

    .line 717
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 7
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    const/4 v6, 0x0

    .line 722
    invoke-super {p0, p1, p2}, Landroid/view/animation/TranslateAnimation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 724
    const/4 v0, 0x0

    .line 726
    .local v0, "height":F
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtoYDelta:F

    cmpl-float v4, v4, v6

    if-nez v4, :cond_2

    .line 727
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mfromYDelta:F

    iget v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mfromYDelta:F

    mul-float/2addr v5, p1

    sub-float v3, v4, v5

    .line 728
    .local v3, "temp_y":F
    cmpg-float v4, v3, v6

    if-gtz v4, :cond_0

    .line 729
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtoYDelta:F

    .line 737
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v4, v6, v3}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 739
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mView:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 740
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtxtCalc_visible_check:Z

    if-eqz v4, :cond_3

    .line 741
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->default_height:F

    add-float v0, v4, v3

    .line 742
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 743
    .local v1, "p":Landroid/widget/LinearLayout$LayoutParams;
    float-to-int v4, v0

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 745
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestLayout()V

    .line 755
    .end local v1    # "p":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    :goto_1
    return-void

    .line 732
    .end local v3    # "temp_y":F
    :cond_2
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mfromYDelta:F

    iget v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtoYDelta:F

    mul-float/2addr v5, p1

    add-float v3, v4, v5

    .line 733
    .restart local v3    # "temp_y":F
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtoYDelta:F

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_0

    .line 734
    iget v3, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mtoYDelta:F

    goto :goto_0

    .line 747
    :cond_3
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->default_height:F

    add-float v0, v4, v3

    .line 748
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 750
    .local v2, "p1":Landroid/view/ViewGroup$LayoutParams;
    float-to-int v4, v0

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 752
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$CalculatorTranslateAnimation;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestLayout()V

    goto :goto_1
.end method
