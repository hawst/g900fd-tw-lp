.class Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;
.super Ljava/lang/Object;
.source "Panel.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/calculator/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PanelOnGestureListener"
.end annotation


# instance fields
.field scrollX:F

.field scrollY:F

.field final synthetic this$0:Lcom/sec/android/widgetapp/calculator/Panel;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/calculator/Panel;)V
    .locals 0

    .prologue
    .line 853
    iput-object p1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 870
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollY:F

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollX:F

    .line 871
    iget-object v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/calculator/Panel;->initChange()Z

    .line 872
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v0, 0x1

    .line 904
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v1

    sget-object v2, Lcom/sec/android/widgetapp/calculator/Panel$State;->ABOUT_TO_ANIMATE:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "I9300"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "I9301"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "T217"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N9005"

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-N900"

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-P601"

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-P600"

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-P605"

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-P605M"

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v1

    const-string v2, "capuccino"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v1

    const-string v2, "americano"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # invokes: Lcom/sec/android/widgetapp/calculator/Panel;->isJapanModel()Z
    invoke-static {v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "kqlte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 906
    :cond_0
    const/4 v0, 0x0

    .line 912
    .end local p4    # "velocityY":F
    :goto_0
    return v0

    .line 909
    .restart local p4    # "velocityY":F
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    sget-object v2, Lcom/sec/android/widgetapp/calculator/Panel$State;->FLYING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$002(Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/widgetapp/calculator/Panel$State;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    .line 910
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1300(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v2

    if-ne v2, v0, :cond_2

    .end local p4    # "velocityY":F
    :goto_1
    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mVelocity:F
    invoke-static {v1, p4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1202(Lcom/sec/android/widgetapp/calculator/Panel;F)F

    .line 911
    iget-object v1, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget-object v2, v2, Lcom/sec/android/widgetapp/calculator/Panel;->startAnimation:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .restart local p4    # "velocityY":F
    :cond_2
    move p4, p3

    .line 910
    goto :goto_1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 917
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 927
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$000(Lcom/sec/android/widgetapp/calculator/Panel;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    move-result-object v4

    sget-object v5, Lcom/sec/android/widgetapp/calculator/Panel$State;->ABOUT_TO_ANIMATE:Lcom/sec/android/widgetapp/calculator/Panel$State;

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "I9300"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "I9301"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "T217"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "SM-N9005"

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "SM-N900"

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "SM-P601"

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "SM-P600"

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "SM-P605"

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "SM-P605M"

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->model:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2900(Lcom/sec/android/widgetapp/calculator/Panel;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v4

    const-string v5, "capuccino"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/popupcalculator/CalculatorUtils;->isKProject()Ljava/lang/String;

    move-result-object v4

    const-string v5, "americano"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # invokes: Lcom/sec/android/widgetapp/calculator/Panel;->isJapanModel()Z
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "ro.product.name"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "kqlte"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 961
    :cond_0
    :goto_0
    return v2

    .line 931
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    sget-object v5, Lcom/sec/android/widgetapp/calculator/Panel$State;->TRACKING:Lcom/sec/android/widgetapp/calculator/Panel$State;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mState:Lcom/sec/android/widgetapp/calculator/Panel$State;
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$002(Lcom/sec/android/widgetapp/calculator/Panel;Lcom/sec/android/widgetapp/calculator/Panel$State;)Lcom/sec/android/widgetapp/calculator/Panel$State;

    .line 932
    const/4 v1, 0x0

    .local v1, "tmpY":F
    const/4 v0, 0x0

    .line 933
    .local v0, "tmpX":F
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mOrientation:I
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1300(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v4

    if-ne v4, v3, :cond_6

    .line 934
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollY:F

    sub-float/2addr v4, p4

    iput v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollY:F

    .line 935
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v4

    if-nez v4, :cond_5

    .line 936
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollY:F

    iget-object v6, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentHeight:I
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$500(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v6

    neg-int v6, v6

    # invokes: Lcom/sec/android/widgetapp/calculator/Panel;->ensureRange(FII)F
    invoke-static {v4, v5, v6, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3100(Lcom/sec/android/widgetapp/calculator/Panel;FII)F

    move-result v1

    .line 946
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1700(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v2

    cmpl-float v2, v0, v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v2

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_4

    .line 947
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackX:F
    invoke-static {v2, v0}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1702(Lcom/sec/android/widgetapp/calculator/Panel;F)F

    .line 948
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # setter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v2, v1}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1402(Lcom/sec/android/widgetapp/calculator/Panel;F)F

    .line 950
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 951
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mIsShrinking:Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1000(Lcom/sec/android/widgetapp/calculator/Panel;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 952
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinearHeight:F
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 953
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinear:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2000(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 959
    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->invalidate()V

    :cond_4
    move v2, v3

    .line 961
    goto/16 :goto_0

    .line 938
    :cond_5
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollY:F

    iget-object v6, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentHeight:I
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$500(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v6

    # invokes: Lcom/sec/android/widgetapp/calculator/Panel;->ensureRange(FII)F
    invoke-static {v4, v5, v2, v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3100(Lcom/sec/android/widgetapp/calculator/Panel;FII)F

    move-result v1

    goto :goto_1

    .line 940
    :cond_6
    iget v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollX:F

    sub-float/2addr v4, p3

    iput v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollX:F

    .line 941
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mPosition:I
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1100(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    .line 942
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollX:F

    iget-object v6, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentWidth:I
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$400(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v6

    neg-int v6, v6

    # invokes: Lcom/sec/android/widgetapp/calculator/Panel;->ensureRange(FII)F
    invoke-static {v4, v5, v6, v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3100(Lcom/sec/android/widgetapp/calculator/Panel;FII)F

    move-result v0

    goto/16 :goto_1

    .line 944
    :cond_7
    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    iget v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollX:F

    iget-object v6, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mContentWidth:I
    invoke-static {v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$400(Lcom/sec/android/widgetapp/calculator/Panel;)I

    move-result v6

    # invokes: Lcom/sec/android/widgetapp/calculator/Panel;->ensureRange(FII)F
    invoke-static {v4, v5, v2, v6}, Lcom/sec/android/widgetapp/calculator/Panel;->access$3100(Lcom/sec/android/widgetapp/calculator/Panel;FII)F

    move-result v0

    goto/16 :goto_1

    .line 955
    :cond_8
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTxtCalcLinearHeight:F
    invoke-static {v4}, Lcom/sec/android/widgetapp/calculator/Panel;->access$2400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mTrackY:F
    invoke-static {v5}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1400(Lcom/sec/android/widgetapp/calculator/Panel;)F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 956
    iget-object v2, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->this$0:Lcom/sec/android/widgetapp/calculator/Panel;

    # getter for: Lcom/sec/android/widgetapp/calculator/Panel;->mHistroyFrame:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/widgetapp/calculator/Panel;->access$1900(Lcom/sec/android/widgetapp/calculator/Panel;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    goto :goto_2
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 966
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 969
    const/4 v0, 0x0

    return v0
.end method

.method public setScroll(II)V
    .locals 1
    .param p1, "initScrollX"    # I
    .param p2, "initScrollY"    # I

    .prologue
    .line 859
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollX:F

    .line 860
    int-to-float v0, p2

    iput v0, p0, Lcom/sec/android/widgetapp/calculator/Panel$PanelOnGestureListener;->scrollY:F

    .line 861
    return-void
.end method
