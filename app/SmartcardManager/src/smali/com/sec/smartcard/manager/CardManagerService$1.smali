.class Lcom/sec/smartcard/manager/CardManagerService$1;
.super Lcom/sec/smartcard/manager/ISmartCardService$Stub;
.source "CardManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/CardManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field public static final ADAPTER_SERVICE_PERMISSION:Ljava/lang/String; = "com.sec.smartcard.permission.ISmartCardAdapterService"


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/CardManagerService;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/CardManagerService;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    invoke-direct {p0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public connectCryptoService(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Z
    .locals 7
    .param p1, "arg0"    # Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v3, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    const-string v4, "connectCryptoService"

    # invokes: Lcom/sec/smartcard/manager/CardManagerService;->enforceSmartCardPermission(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/smartcard/manager/CardManagerService;->access$000(Lcom/sec/smartcard/manager/CardManagerService;Ljava/lang/String;)V

    .line 153
    invoke-static {}, Lcom/sec/smartcard/manager/CardManagerService$1;->getCallingUid()I

    move-result v0

    .line 155
    .local v0, "caller":I
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v1

    .line 156
    .local v1, "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    iget-object v3, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    # getter for: Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/smartcard/manager/CardManagerService;->access$100(Lcom/sec/smartcard/manager/CardManagerService;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.smartcard.adapter.BIND_SMARTCARD_SANDBOXED_SERVICE"

    sget v5, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DEFAULT_ADAPTER_SELECTION_MODE:I

    sget v6, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->AUTO_CONNECTION_MODE:I

    or-int/2addr v5, v6

    sget v6, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->HANDLE_SMARTCARD_PREF_SELECTION:I

    or-int/2addr v5, v6

    invoke-virtual {v1, v3, v0, v4, v5}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getAvailableConnection(Landroid/content/Context;ILjava/lang/String;I)Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    move-result-object v2

    .line 159
    .local v2, "srvConn":Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    if-eqz v2, :cond_0

    .line 160
    iget-object v3, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    # getter for: Lcom/sec/smartcard/manager/CardManagerService;->mCrytoServiceClients:Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;
    invoke-static {v3}, Lcom/sec/smartcard/manager/CardManagerService;->access$200(Lcom/sec/smartcard/manager/CardManagerService;)Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;

    move-result-object v3

    invoke-virtual {v3, p1, v2}, Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 161
    invoke-virtual {v2, p1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->connect(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Z

    move-result v3

    .line 163
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public disconnectCryptoService()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 168
    iget-object v3, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    const-string v4, "disconnectCryptoService"

    # invokes: Lcom/sec/smartcard/manager/CardManagerService;->enforceSmartCardPermission(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/smartcard/manager/CardManagerService;->access$000(Lcom/sec/smartcard/manager/CardManagerService;Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v1

    .line 170
    .local v1, "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    invoke-static {}, Lcom/sec/smartcard/manager/CardManagerService$1;->getCallingUid()I

    move-result v0

    .line 171
    .local v0, "caller":I
    invoke-virtual {v1, v0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getSandboxedProcessServiceConnection(I)Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    move-result-object v2

    .line 172
    .local v2, "srvConn":Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    if-eqz v2, :cond_0

    .line 173
    iget-object v3, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    # getter for: Lcom/sec/smartcard/manager/CardManagerService;->mCrytoServiceClients:Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;
    invoke-static {v3}, Lcom/sec/smartcard/manager/CardManagerService;->access$200(Lcom/sec/smartcard/manager/CardManagerService;)Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->getClient()Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;->unregister(Landroid/os/IInterface;)Z

    .line 174
    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->disconnect()V

    .line 176
    :cond_0
    return-void
.end method

.method public getCryptoServiceSocket()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v2, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    const-string v3, "getCryptoServiceSocket"

    # invokes: Lcom/sec/smartcard/manager/CardManagerService;->enforceSmartCardPermission(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/smartcard/manager/CardManagerService;->access$000(Lcom/sec/smartcard/manager/CardManagerService;Ljava/lang/String;)V

    .line 182
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v1

    .line 183
    .local v1, "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    invoke-static {}, Lcom/sec/smartcard/manager/CardManagerService$1;->getCallingUid()I

    move-result v0

    .line 184
    .local v0, "caller":I
    iget-object v2, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    # getter for: Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/smartcard/manager/CardManagerService;->access$100(Lcom/sec/smartcard/manager/CardManagerService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getServiceSocket(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getPin(Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;)V
    .locals 3
    .param p1, "callback"    # Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v1, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    const-string v2, "getPin"

    # invokes: Lcom/sec/smartcard/manager/CardManagerService;->enforceSmartCardPermission(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/CardManagerService;->access$000(Lcom/sec/smartcard/manager/CardManagerService;Ljava/lang/String;)V

    .line 138
    iget-object v1, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    # getter for: Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/smartcard/manager/CardManagerService;->access$100(Lcom/sec/smartcard/manager/CardManagerService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v0

    .line 139
    .local v0, "pinService":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    invoke-virtual {v0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getPin(Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;)V

    .line 140
    return-void
.end method

.method public getSlotID()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v1, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    const-string v2, "getSlotID"

    # invokes: Lcom/sec/smartcard/manager/CardManagerService;->enforceSmartCardPermission(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/CardManagerService;->access$000(Lcom/sec/smartcard/manager/CardManagerService;Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    # getter for: Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/smartcard/manager/CardManagerService;->access$100(Lcom/sec/smartcard/manager/CardManagerService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v0

    .line 146
    .local v0, "pinService":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSlotID()J

    move-result-wide v2

    return-wide v2
.end method

.method public getVersion()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/smartcard/manager/CardManagerService$1;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    const-string v1, "getVersion"

    # invokes: Lcom/sec/smartcard/manager/CardManagerService;->enforceSmartCardPermission(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/smartcard/manager/CardManagerService;->access$000(Lcom/sec/smartcard/manager/CardManagerService;Ljava/lang/String;)V

    .line 132
    const/4 v0, 0x0

    return v0
.end method
