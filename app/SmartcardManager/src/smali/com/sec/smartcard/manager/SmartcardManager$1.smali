.class Lcom/sec/smartcard/manager/SmartcardManager$1;
.super Ljava/lang/Object;
.source "SmartcardManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/smartcard/manager/SmartcardManager;->initializeService()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/SmartcardManager;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/SmartcardManager;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/smartcard/manager/SmartcardManager$1;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitComplete()V
    .locals 3

    .prologue
    .line 78
    iget-object v1, p0, Lcom/sec/smartcard/manager/SmartcardManager$1;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    const/4 v2, 0x1

    # setter for: Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/SmartcardManager;->access$002(Lcom/sec/smartcard/manager/SmartcardManager;Z)Z

    .line 79
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v0

    .line 81
    .local v0, "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    iget-object v1, p0, Lcom/sec/smartcard/manager/SmartcardManager$1;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    # getter for: Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SmartcardManager;->access$100(Lcom/sec/smartcard/manager/SmartcardManager;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getSmartCardId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->loadPkcs11(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    .line 83
    return-void
.end method

.method public onStatusChanged(I)V
    .locals 5
    .param p1, "status"    # I

    .prologue
    .line 53
    const-string v2, "SmartcardManager App"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStatusChanged : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v1

    .line 57
    .local v1, "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    packed-switch p1, :pswitch_data_0

    .line 68
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardConnectionError()V

    .line 71
    :goto_0
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->resetPkcs11()V

    .line 72
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager$1;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    const/4 v3, 0x0

    # setter for: Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z
    invoke-static {v2, v3}, Lcom/sec/smartcard/manager/SmartcardManager;->access$002(Lcom/sec/smartcard/manager/SmartcardManager;Z)Z

    .line 73
    return-void

    .line 59
    :pswitch_0
    const-string v2, "SmartcardManager App"

    const-string v3, "broadcast intent"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.enterprise.mdm.sc.action.INVALIDATE_CACHE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager$1;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManager;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    const-string v2, "cardId"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager$1;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v2, v0}, Lcom/sec/smartcard/manager/SmartcardManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 64
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardConnectionError()V

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method
