.class Lcom/sec/smartcard/manager/SmartcardManager$2;
.super Lcom/sec/smartcard/adapter/ICryptoServiceListener$Stub;
.source "SmartcardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/SmartcardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/SmartcardManager;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/SmartcardManager;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ICryptoServiceListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onSmartCardConnected(Ljava/lang/String;)V
    .locals 7
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 167
    const-string v4, "SmartcardManager App"

    const-string v5, "onSmartCardConnected"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v4, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    const/4 v5, 0x1

    # setter for: Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z
    invoke-static {v4, v5}, Lcom/sec/smartcard/manager/SmartcardManager;->access$002(Lcom/sec/smartcard/manager/SmartcardManager;Z)Z

    .line 169
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v2

    .line 171
    .local v2, "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v1

    .line 172
    .local v1, "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    iget-object v4, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v4}, Lcom/sec/smartcard/manager/SmartcardManager;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v0, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 173
    .local v0, "caller":I
    iget-object v4, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v4}, Lcom/sec/smartcard/manager/SmartcardManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getServiceSocket(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 174
    .local v3, "serviceSocket":Ljava/lang/String;
    const-string v4, "SmartcardManager App"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Server socket: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-virtual {v2, v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->loadPkcs11(Ljava/lang/String;)V

    .line 176
    invoke-virtual {v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->onSmartCardConnected()V

    .line 177
    return-void
.end method

.method public onSmartCardConnectionError(Ljava/lang/String;I)V
    .locals 6
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "reasonCode"    # I

    .prologue
    .line 184
    const-string v3, "SmartcardManager App"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSmartCardConnectionError"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v2

    .line 187
    .local v2, "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    sparse-switch p2, :sswitch_data_0

    .line 205
    invoke-virtual {v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardConnectionError()V

    .line 208
    :goto_0
    iget-object v3, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v3}, Lcom/sec/smartcard/manager/SmartcardManager;->disconnectService()V

    .line 209
    iget-object v3, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    const/4 v4, 0x0

    # setter for: Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z
    invoke-static {v3, v4}, Lcom/sec/smartcard/manager/SmartcardManager;->access$002(Lcom/sec/smartcard/manager/SmartcardManager;Z)Z

    .line 210
    return-void

    .line 189
    :sswitch_0
    const-string v3, "SmartcardManager App"

    const-string v4, "broadcast intent"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.enterprise.mdm.sc.action.INVALIDATE_CACHE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 191
    .local v0, "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v3}, Lcom/sec/smartcard/manager/SmartcardManager;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v3, "cardId"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    iget-object v3, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v3, v0}, Lcom/sec/smartcard/manager/SmartcardManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 194
    invoke-virtual {v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardConnectionError()V

    goto :goto_0

    .line 197
    .end local v0    # "i":Landroid/content/Intent;
    :sswitch_1
    const-string v3, "SmartcardManager App"

    const-string v4, "broadcast intent"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.enterprise.mdm.sc.action.INVALIDATE_CACHE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 199
    .local v1, "i1":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v3}, Lcom/sec/smartcard/manager/SmartcardManager;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const-string v3, "cardId"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    iget-object v3, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v3, v1}, Lcom/sec/smartcard/manager/SmartcardManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 202
    invoke-virtual {v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardInitResourceError()V

    goto :goto_0

    .line 187
    :sswitch_data_0
    .sparse-switch
        0x49 -> :sswitch_0
        0x65 -> :sswitch_1
    .end sparse-switch
.end method

.method public onSmartCardConnectionProgress(Ljava/lang/String;I)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "state"    # I

    .prologue
    .line 180
    const-string v0, "SmartcardManager App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSmartCardConnectionProgress"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    return-void
.end method

.method public onSmartCardDisconnected(Ljava/lang/String;I)V
    .locals 5
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "reasonCode"    # I

    .prologue
    .line 213
    const-string v2, "SmartcardManager App"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSmartCardDisconnected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v1

    .line 216
    .local v1, "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    packed-switch p2, :pswitch_data_0

    .line 227
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardConnectionError()V

    .line 230
    :goto_0
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManager;->disconnectService()V

    .line 231
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    const/4 v3, 0x0

    # setter for: Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z
    invoke-static {v2, v3}, Lcom/sec/smartcard/manager/SmartcardManager;->access$002(Lcom/sec/smartcard/manager/SmartcardManager;Z)Z

    .line 232
    return-void

    .line 218
    :pswitch_0
    const-string v2, "SmartcardManager App"

    const-string v3, "broadcast intent"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.enterprise.mdm.sc.action.INVALIDATE_CACHE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 220
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManager;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    const-string v2, "cardId"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager$2;->this$0:Lcom/sec/smartcard/manager/SmartcardManager;

    invoke-virtual {v2, v0}, Lcom/sec/smartcard/manager/SmartcardManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 223
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardConnectionError()V

    goto :goto_0

    .line 216
    nop

    :pswitch_data_0
    .packed-switch 0x49
        :pswitch_0
    .end packed-switch
.end method
