.class public Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;
.super Ljava/lang/Object;
.source "SandboxedProcessConnectionSettings.java"


# static fields
.field public static AUTO_CONNECTION_MODE:I

.field public static DEFAULT_ADAPTER_SELECTION_MODE:I

.field public static DISPLAY_ADAPTER_LIST:I

.field public static DISPLAY_CONNECTION_LIST:I

.field public static DISPLAY_NONE:I

.field public static HANDLE_SMARTCARD_PREF_SELECTION:I


# instance fields
.field private mAutoConnectionMode:Z

.field private mDefaultAdapterSelectionMode:Z

.field private mDisplayAdapterList:Z

.field private mDisplayConnectionList:Z

.field private mHandleSmartcardPrefSelection:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DISPLAY_NONE:I

    .line 41
    const/4 v0, 0x1

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DISPLAY_ADAPTER_LIST:I

    .line 44
    const/4 v0, 0x2

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DISPLAY_CONNECTION_LIST:I

    .line 47
    const/4 v0, 0x4

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->HANDLE_SMARTCARD_PREF_SELECTION:I

    .line 51
    const/16 v0, 0x8

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->AUTO_CONNECTION_MODE:I

    .line 55
    const/16 v0, 0x10

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DEFAULT_ADAPTER_SELECTION_MODE:I

    return-void
.end method

.method constructor <init>(I)V
    .locals 3
    .param p1, "settings"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDisplayAdapterList:Z

    .line 61
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDisplayConnectionList:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mHandleSmartcardPrefSelection:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mAutoConnectionMode:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDefaultAdapterSelectionMode:Z

    .line 77
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DISPLAY_ADAPTER_LIST:I

    and-int/2addr v0, p1

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDisplayAdapterList:Z

    .line 78
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DISPLAY_CONNECTION_LIST:I

    and-int/2addr v0, p1

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDisplayConnectionList:Z

    .line 79
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->HANDLE_SMARTCARD_PREF_SELECTION:I

    and-int/2addr v0, p1

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mHandleSmartcardPrefSelection:Z

    .line 80
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->AUTO_CONNECTION_MODE:I

    and-int/2addr v0, p1

    if-lez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mAutoConnectionMode:Z

    .line 81
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DEFAULT_ADAPTER_SELECTION_MODE:I

    and-int/2addr v0, p1

    if-lez v0, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDefaultAdapterSelectionMode:Z

    .line 82
    return-void

    :cond_0
    move v0, v2

    .line 77
    goto :goto_0

    :cond_1
    move v0, v2

    .line 78
    goto :goto_1

    :cond_2
    move v0, v2

    .line 79
    goto :goto_2

    :cond_3
    move v0, v2

    .line 80
    goto :goto_3

    :cond_4
    move v1, v2

    .line 81
    goto :goto_4
.end method


# virtual methods
.method handleSmartcardPrefSelection()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mHandleSmartcardPrefSelection:Z

    return v0
.end method

.method isAutoConnectionMode()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mAutoConnectionMode:Z

    return v0
.end method

.method isDefaultAdapterSelectionMode()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDefaultAdapterSelectionMode:Z

    return v0
.end method

.method shouldDisplayAdapterList()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDisplayAdapterList:Z

    return v0
.end method

.method shouldDisplayConnectionList()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->mDisplayConnectionList:Z

    return v0
.end method
