.class public Lcom/sec/smartcard/manager/ConnectionListActivity;
.super Landroid/app/Activity;
.source "ConnectionListActivity.java"


# static fields
.field static final TAG:Ljava/lang/String; = "ConnectionListActivity"


# instance fields
.field private mConnectionNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/manager/ConnectionListActivity;->mConnectionNames:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/manager/ConnectionListActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/ConnectionListActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/smartcard/manager/ConnectionListActivity;->mConnectionNames:Ljava/util/List;

    return-object v0
.end method

.method private displayConnectionsList()V
    .locals 5

    .prologue
    .line 106
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 107
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v1, "Select Connection"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/smartcard/manager/CustomArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/smartcard/manager/ConnectionListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/smartcard/manager/ConnectionListActivity;->mConnectionNames:Ljava/util/List;

    invoke-direct {v2, v3, v4}, Lcom/sec/smartcard/manager/CustomArrayAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    new-instance v3, Lcom/sec/smartcard/manager/ConnectionListActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/smartcard/manager/ConnectionListActivity$1;-><init>(Lcom/sec/smartcard/manager/ConnectionListActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 123
    new-instance v1, Lcom/sec/smartcard/manager/ConnectionListActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/smartcard/manager/ConnectionListActivity$2;-><init>(Lcom/sec/smartcard/manager/ConnectionListActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 134
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 135
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/smartcard/manager/ConnectionListActivity;->mDialog:Landroid/app/AlertDialog;

    .line 136
    iget-object v1, p0, Lcom/sec/smartcard/manager/ConnectionListActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 137
    return-void
.end method

.method private displayErrorDialog()V
    .locals 5

    .prologue
    .line 144
    new-instance v1, Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x4

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 146
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f050038

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1080027

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f05003a

    new-instance v4, Lcom/sec/smartcard/manager/ConnectionListActivity$3;

    invoke-direct {v4, p0}, Lcom/sec/smartcard/manager/ConnectionListActivity$3;-><init>(Lcom/sec/smartcard/manager/ConnectionListActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 159
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 161
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 162
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/manager/ConnectionListActivity;->requestWindowFeature(I)Z

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/smartcard/manager/ConnectionListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "Connections"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/ConnectionListActivity;->mConnectionNames:Ljava/util/List;

    .line 69
    iget-object v0, p0, Lcom/sec/smartcard/manager/ConnectionListActivity;->mConnectionNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-direct {p0}, Lcom/sec/smartcard/manager/ConnectionListActivity;->displayErrorDialog()V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-direct {p0}, Lcom/sec/smartcard/manager/ConnectionListActivity;->displayConnectionsList()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 79
    const-string v0, "ConnectionListActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 91
    const-string v0, "ConnectionListActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 85
    const-string v0, "ConnectionListActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 97
    const-string v0, "ConnectionListActivity"

    const-string v1, "onUserLeaveHint"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/sec/smartcard/manager/ConnectionListActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 99
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/ConnectionListActivity;->finish()V

    .line 100
    return-void
.end method
