.class Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;
.super Landroid/content/BroadcastReceiver;
.source "SandboxedProcessServiceConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)V
    .locals 0

    .prologue
    .line 689
    iput-object p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 693
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.smartcard.manager.CONNECTION_SELECTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 695
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$400(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v2

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1000()I

    move-result v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$000(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoService;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 698
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "Connections"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 699
    .local v1, "selectedConnection":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 700
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->setConnectionPreference(Ljava/lang/String;)V

    .line 703
    :cond_0
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mPrevConnectionState:I
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v3

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v2, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$402(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;I)I

    .line 706
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1600(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/BroadcastReceiver;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 707
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsConnSelReceiverRegistered:Z
    invoke-static {v2, v5}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1702(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Z)Z

    .line 711
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # invokes: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->connectInternal()Z
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$900(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 712
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$400(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v2

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1000()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 714
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 716
    :try_start_0
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x49

    invoke-interface {v2, v3, v4}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 727
    .end local v1    # "selectedConnection":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.smartcard.manager.CONNECTION_NOT_SELECTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 728
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$400(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v2

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1000()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$000(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoService;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 731
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mPrevConnectionState:I
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v3

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v2, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$402(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;I)I

    .line 734
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1600(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/BroadcastReceiver;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 735
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsConnSelReceiverRegistered:Z
    invoke-static {v2, v5}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1702(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Z)Z

    .line 737
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 739
    :try_start_1
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x49

    invoke-interface {v2, v3, v4}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 747
    :cond_2
    :goto_1
    return-void

    .line 717
    .restart local v1    # "selectedConnection":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 718
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 740
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "selectedConnection":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 741
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
