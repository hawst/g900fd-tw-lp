.class final Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;
.super Landroid/os/RemoteCallbackList;
.source "CardManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/CardManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RemoteCallbackListWithDiedCB"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Landroid/os/IInterface;",
        ">",
        "Landroid/os/RemoteCallbackList",
        "<TE;>;"
    }
.end annotation


# instance fields
.field mRemoteCallbackDied:Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied",
            "<TE;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/smartcard/manager/CardManagerService;


# direct methods
.method public constructor <init>(Lcom/sec/smartcard/manager/CardManagerService;Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 203
    .local p0, "this":Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;, "Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB<TE;>;"
    .local p2, "mRemoteCallbackDied":Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;, "Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied<TE;>;"
    iput-object p1, p0, Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    invoke-direct {p0}, Landroid/os/RemoteCallbackList;-><init>()V

    .line 204
    iput-object p2, p0, Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;->mRemoteCallbackDied:Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;

    .line 205
    return-void
.end method


# virtual methods
.method public onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .locals 1
    .param p2, "cookie"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 208
    .local p0, "this":Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;, "Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB<TE;>;"
    .local p1, "callback":Landroid/os/IInterface;, "TE;"
    invoke-super {p0, p1, p2}, Landroid/os/RemoteCallbackList;->onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V

    .line 209
    iget-object v0, p0, Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;->mRemoteCallbackDied:Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;

    invoke-interface {v0, p1, p2}, Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;->remoteCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V

    .line 210
    return-void
.end method
