.class public Lcom/sec/smartcard/manager/SmartcardManagerPreferences;
.super Ljava/lang/Object;
.source "SmartcardManagerPreferences.java"


# static fields
.field public static ADAPTER_PREF_KEY:Ljava/lang/String;

.field public static CONNECTION_PREF_KEY:Ljava/lang/String;

.field private static mSmartcardManagerPrefs:Lcom/sec/smartcard/manager/SmartcardManagerPreferences;


# instance fields
.field private PREF_NAME:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mSmartcardPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardManagerPrefs:Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    .line 66
    const-string v0, "com.sec.smartcard.manager.ADAPTERKEY"

    sput-object v0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->ADAPTER_PREF_KEY:Ljava/lang/String;

    .line 71
    const-string v0, "com.sec.smartcard.manager.CONNECTIONKEY"

    sput-object v0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->CONNECTION_PREF_KEY:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;

    .line 56
    iput-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mContext:Landroid/content/Context;

    .line 61
    const-string v0, "com.sec.smartcard.manager"

    iput-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->PREF_NAME:Ljava/lang/String;

    .line 77
    if-eqz p1, :cond_0

    .line 78
    iput-object p1, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mContext:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->PREF_NAME:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;

    .line 81
    :cond_0
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 89
    sget-object v0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardManagerPrefs:Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;-><init>(Landroid/content/Context;)V

    .line 92
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardManagerPrefs:Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getAdapterPreference()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 115
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 118
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->ADAPTER_PREF_KEY:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getConnectionPreference()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 148
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 151
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->CONNECTION_PREF_KEY:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeAdapterPreference()V
    .locals 2

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->ADAPTER_PREF_KEY:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    monitor-exit p0

    return-void

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeConnectionPreference()V
    .locals 2

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->CONNECTION_PREF_KEY:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    monitor-exit p0

    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAdapterPreference(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 106
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 103
    :cond_1
    if-eqz p1, :cond_0

    .line 104
    :try_start_1
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->ADAPTER_PREF_KEY:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setConnectionPreference(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 139
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 136
    :cond_1
    if-eqz p1, :cond_0

    .line 137
    :try_start_1
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->mSmartcardPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->CONNECTION_PREF_KEY:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
