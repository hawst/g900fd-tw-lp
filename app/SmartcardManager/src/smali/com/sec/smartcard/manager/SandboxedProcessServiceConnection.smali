.class public Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
.super Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;
.source "SandboxedProcessServiceConnection.java"


# static fields
.field public static final ADAPTERS_LIST_DATA:Ljava/lang/String; = "Adapters"

.field private static final BIND_SMARTCARD_SANDBOXED_SERVICE:Ljava/lang/String; = "com.sec.smartcard.adapter.BIND_SMARTCARD_SANDBOXED_SERVICE"

.field public static final CONNECTIONS_LIST_DATA:Ljava/lang/String; = "Connections"

.field public static final INTENT_ACTION_ADAPTER_NOT_SELECTED:Ljava/lang/String; = "com.sec.smartcard.manager.ADAPTER_NOT_SELECTED"

.field public static final INTENT_ACTION_ADAPTER_SELECTED:Ljava/lang/String; = "com.sec.smartcard.manager.ADAPTER_SELECTED"

.field public static final INTENT_ACTION_CONNECTION_NOT_SELECTED:Ljava/lang/String; = "com.sec.smartcard.manager.CONNECTION_NOT_SELECTED"

.field public static final INTENT_ACTION_CONNECTION_SELECTED:Ljava/lang/String; = "com.sec.smartcard.manager.CONNECTION_SELECTED"

.field private static STATE_CONNECTION_CONNECTED:I = 0x0

.field private static STATE_CONNECTION_FAILED:I = 0x0

.field private static STATE_CONNECTION_INTIALIZED:I = 0x0

.field private static STATE_CONNECTION_NONE:I = 0x0

.field private static STATE_CONNECTION_START:I = 0x0

.field private static STATE_CONNECTION_WAIT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SandboxedProcessServiceConnection"


# instance fields
.field private mAdapterConnection:Landroid/content/ServiceConnection;

.field private mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

.field private mAutoConnectionName:Ljava/lang/String;

.field private mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

.field private mClientUid:I

.field private mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

.field private mConnectionState:I

.field private mContext:Landroid/content/Context;

.field private mCryptoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

.field private mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

.field private mIsAdapterSelReceiverRegistered:Z

.field private mIsConnSelReceiverRegistered:Z

.field private mIsCryptoServiceBounded:Z

.field private mPrevConnectionState:I

.field private mSandboxSlot:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, -0x1

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_NONE:I

    .line 89
    const/4 v0, 0x0

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    .line 90
    const/4 v0, 0x1

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    .line 91
    const/4 v0, 0x2

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_INTIALIZED:I

    .line 92
    const/4 v0, 0x3

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_CONNECTED:I

    .line 93
    const/4 v0, 0x4

    sput v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_FAILED:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;III)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "clientUid"    # I
    .param p3, "slot"    # I
    .param p4, "settings"    # I

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0, p4}, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;-><init>(I)V

    .line 65
    iput v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    .line 70
    iput v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mClientUid:I

    .line 75
    iput-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    .line 80
    iput-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    .line 85
    iput-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAutoConnectionName:Ljava/lang/String;

    .line 95
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_NONE:I

    iput v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 96
    iget v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    iput v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mPrevConnectionState:I

    .line 449
    iput-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    .line 450
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsCryptoServiceBounded:Z

    .line 451
    new-instance v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAdapterConnection:Landroid/content/ServiceConnection;

    .line 489
    new-instance v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    .line 624
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsAdapterSelReceiverRegistered:Z

    .line 625
    new-instance v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$3;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$3;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

    .line 688
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsConnSelReceiverRegistered:Z

    .line 689
    new-instance v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$4;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

    .line 118
    const-string v0, "SandboxedProcessServiceConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Constructor with slot: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iput-object p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    .line 120
    iput p3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    .line 121
    iput p2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mClientUid:I

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Lcom/sec/smartcard/adapter/ICryptoService;)Lcom/sec/smartcard/adapter/ICryptoService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    .param p1, "x1"    # Lcom/sec/smartcard/adapter/ICryptoService;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->performNextConnectOperation()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000()I
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    return v0
.end method

.method static synthetic access$1100()I
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_FAILED:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mPrevConnectionState:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsAdapterSelReceiverRegistered:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->initializeCryptoService()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsConnSelReceiverRegistered:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    return p1
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsCryptoServiceBounded:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    return v0
.end method

.method static synthetic access$800()I
    .locals 1

    .prologue
    .line 61
    sget v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_INTIALIZED:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->connectInternal()Z

    move-result v0

    return v0
.end method

.method private connectInternal()Z
    .locals 10

    .prologue
    .line 346
    const/4 v6, 0x0

    .line 350
    .local v6, "ret":Z
    iget v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v8, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_NONE:I

    if-eq v7, v8, :cond_0

    iget v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v8, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    if-ne v7, v8, :cond_1

    .line 352
    :cond_0
    const/4 v7, 0x0

    .line 414
    :goto_0
    return v7

    .line 354
    :cond_1
    const-string v7, "SandboxedProcessServiceConnection"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Inside Connect internal function for slot: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :try_start_0
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v5

    .line 358
    .local v5, "mgrPrefs":Lcom/sec/smartcard/manager/SmartcardManagerPreferences;
    const/4 v1, 0x0

    .line 359
    .local v1, "connectionPref":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v1

    .line 361
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->isAutoConnectionMode()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 363
    const-string v7, "SandboxedProcessServiceConnection"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Auto connecting for slot: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    iget-object v8, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v7, v8}, Lcom/sec/smartcard/adapter/ICryptoService;->connectAvailableTransport(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAutoConnectionName:Ljava/lang/String;

    .line 366
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAutoConnectionName:Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 367
    sget v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_CONNECTED:I

    iput v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 368
    const-string v7, "SandboxedProcessServiceConnection"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Setting connection state to: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", for connection object: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    const/4 v6, 0x1

    .end local v1    # "connectionPref":Ljava/lang/String;
    .end local v5    # "mgrPrefs":Lcom/sec/smartcard/manager/SmartcardManagerPreferences;
    :cond_2
    :goto_1
    move v7, v6

    .line 414
    goto :goto_0

    .line 371
    .restart local v1    # "connectionPref":Ljava/lang/String;
    .restart local v5    # "mgrPrefs":Lcom/sec/smartcard/manager/SmartcardManagerPreferences;
    :cond_3
    sget v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_FAILED:I

    iput v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 372
    const-string v7, "SandboxedProcessServiceConnection"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Setting connection state to: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", for connection object: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 411
    .end local v1    # "connectionPref":Ljava/lang/String;
    .end local v5    # "mgrPrefs":Lcom/sec/smartcard/manager/SmartcardManagerPreferences;
    :catch_0
    move-exception v2

    .line 412
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 374
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "connectionPref":Ljava/lang/String;
    .restart local v5    # "mgrPrefs":Lcom/sec/smartcard/manager/SmartcardManagerPreferences;
    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->shouldDisplayConnectionList()Z

    move-result v7

    if-nez v7, :cond_5

    if-nez v1, :cond_7

    .line 377
    :cond_5
    new-instance v3, Landroid/content/IntentFilter;

    const-string v7, "com.sec.smartcard.manager.CONNECTION_SELECTED"

    invoke-direct {v3, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 378
    .local v3, "filter":Landroid/content/IntentFilter;
    const-string v7, "com.sec.smartcard.manager.CONNECTION_NOT_SELECTED"

    invoke-virtual {v3, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 381
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->handleSmartcardPrefSelection()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 382
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v7, v8, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 383
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsConnSelReceiverRegistered:Z

    .line 386
    :cond_6
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    invoke-interface {v7}, Lcom/sec/smartcard/adapter/ICryptoService;->getSupportedConnections()Ljava/util/List;

    move-result-object v0

    .line 387
    .local v0, "connectionNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_2

    .line 389
    new-instance v4, Landroid/content/Intent;

    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    const-class v8, Lcom/sec/smartcard/manager/ConnectionListActivity;

    invoke-direct {v4, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 390
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "Connections"

    check-cast v0, Ljava/util/ArrayList;

    .end local v0    # "connectionNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 391
    const/high16 v7, 0x10000000

    invoke-virtual {v4, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 392
    const v7, 0x8000

    invoke-virtual {v4, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 394
    iget v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    iput v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mPrevConnectionState:I

    .line 395
    sget v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    iput v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 396
    const-string v7, "SandboxedProcessServiceConnection"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Setting connection state to: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", for connection object: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 400
    .end local v3    # "filter":Landroid/content/IntentFilter;
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_7
    const-string v7, "SandboxedProcessServiceConnection"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Using stored connection preference: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " for slot: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    iget-object v8, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v7, v1, v8}, Lcom/sec/smartcard/adapter/ICryptoService;->registerListener(Ljava/lang/String;Lcom/sec/smartcard/adapter/ICryptoServiceListener;)V

    .line 402
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    invoke-interface {v7, v1}, Lcom/sec/smartcard/adapter/ICryptoService;->connect(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 403
    sget v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_CONNECTED:I

    iput v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 404
    const-string v7, "SandboxedProcessServiceConnection"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Setting connection state to: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", for connection object: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    const/4 v6, 0x1

    goto/16 :goto_1

    .line 407
    :cond_8
    sget v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_FAILED:I

    iput v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 408
    const-string v7, "SandboxedProcessServiceConnection"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Setting connection state to: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", for connection object: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private getAdapter()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v9, 0x0

    .line 563
    iget-object v10, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    if-nez v10, :cond_0

    move-object v0, v9

    .line 618
    :goto_0
    return-object v0

    .line 566
    :cond_0
    iget-object v10, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v6

    .line 569
    .local v6, "mgrPrefs":Lcom/sec/smartcard/manager/SmartcardManagerPreferences;
    const/4 v0, 0x0

    .line 570
    .local v0, "adapterPackageName":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getAdapterPreference()Ljava/lang/String;

    move-result-object v0

    .line 573
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->isDefaultAdapterSelectionMode()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 574
    iget-object v10, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 578
    .local v8, "pm":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_1

    .line 580
    const/16 v10, 0x80

    :try_start_0
    invoke-virtual {v8, v0, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 581
    :catch_0
    move-exception v3

    .line 582
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v6}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->removeAdapterPreference()V

    move-object v0, v9

    .line 583
    goto :goto_0

    .line 589
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    .line 590
    .local v2, "applist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 591
    .local v5, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/pm/PackageInfo;>;"
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 592
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageInfo;

    .line 593
    .local v7, "pk":Landroid/content/pm/PackageInfo;
    const-string v10, "com.sec.smartcard.permission.SMARTCARD_ADAPTER"

    iget-object v11, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v10, v11}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_2

    .line 595
    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v9}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->setAdapterPreference(Ljava/lang/String;)V

    .line 596
    iget-object v0, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    .line 600
    .end local v2    # "applist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v5    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/pm/PackageInfo;>;"
    .end local v7    # "pk":Landroid/content/pm/PackageInfo;
    .end local v8    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->shouldDisplayAdapterList()Z

    move-result v10

    if-nez v10, :cond_4

    if-nez v0, :cond_6

    .line 603
    :cond_4
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->handleSmartcardPrefSelection()Z

    move-result v10

    invoke-direct {p0, v10}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->getAdapterList_Permission(Z)Ljava/util/List;

    move-result-object v1

    .line 606
    .local v1, "adapterPkgNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Landroid/content/Intent;

    iget-object v10, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    const-class v11, Lcom/sec/smartcard/manager/AdapterListActivity;

    invoke-direct {v4, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 607
    .local v4, "intent":Landroid/content/Intent;
    const-string v10, "Adapters"

    check-cast v1, Ljava/util/ArrayList;

    .end local v1    # "adapterPkgNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v4, v10, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 608
    const/high16 v10, 0x10000000

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 609
    const v10, 0x8000

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 610
    iget v10, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    iput v10, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mPrevConnectionState:I

    .line 611
    sget v10, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    iput v10, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 612
    const-string v10, "SandboxedProcessServiceConnection"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Setting connection state to: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", for connection object: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    iget-object v10, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v10, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .end local v4    # "intent":Landroid/content/Intent;
    :cond_5
    move-object v0, v9

    .line 618
    goto/16 :goto_0

    .line 615
    :cond_6
    const-string v9, "SandboxedProcessServiceConnection"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Using stored adapter package: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " for slot: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private getAdapterList_Permission(Z)Ljava/util/List;
    .locals 8
    .param p1, "handlePrefSelection"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 532
    new-instance v1, Landroid/content/IntentFilter;

    const-string v5, "com.sec.smartcard.manager.ADAPTER_SELECTED"

    invoke-direct {v1, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 533
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v5, "com.sec.smartcard.manager.ADAPTER_NOT_SELECTED"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 534
    if-eqz p1, :cond_0

    .line 535
    iget-object v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v5, v6, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 536
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsAdapterSelReceiverRegistered:Z

    .line 538
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 540
    .local v4, "results":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 541
    .local v0, "applist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 542
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/pm/PackageInfo;>;"
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 543
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    .line 545
    .local v3, "pk":Landroid/content/pm/PackageInfo;
    iget-object v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.smartcard.permission.SMARTCARD_ADAPTER"

    iget-object v7, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    .line 547
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 548
    const-string v5, "SandboxedProcessServiceConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Selected adapter package: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for slot: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 552
    .end local v3    # "pk":Landroid/content/pm/PackageInfo;
    :cond_2
    return-object v4
.end method

.method private initializeCryptoService()Z
    .locals 7

    .prologue
    .line 422
    const/4 v2, 0x0

    .line 424
    .local v2, "ret":Z
    :try_start_0
    iget v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v5, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v5, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    if-ne v4, v5, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    if-nez v4, :cond_2

    :cond_1
    move v3, v2

    .line 446
    .end local v2    # "ret":Z
    .local v3, "ret":I
    :goto_0
    return v3

    .line 429
    .end local v3    # "ret":I
    .restart local v2    # "ret":Z
    :cond_2
    const-string v4, "SandboxedProcessServiceConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Initializing Crypto service for slot: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->getAdapter()Ljava/lang/String;

    move-result-object v0

    .line 432
    .local v0, "adapter":Ljava/lang/String;
    if-eqz v0, :cond_3

    iget v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v5, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    if-ne v4, v5, :cond_4

    :cond_3
    move v3, v2

    .line 434
    .restart local v3    # "ret":I
    goto :goto_0

    .line 436
    .end local v3    # "ret":I
    :cond_4
    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    iget-object v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    invoke-interface {v4, v5, v0}, Lcom/sec/smartcard/adapter/ICryptoService;->initialize(Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 437
    const-string v4, "SandboxedProcessServiceConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cryptoservice init failed for slot:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    sget v4, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_FAILED:I

    iput v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 439
    const-string v4, "SandboxedProcessServiceConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting connection state to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", for connection object: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "adapter":Ljava/lang/String;
    :goto_1
    move v3, v2

    .line 446
    .restart local v3    # "ret":I
    goto :goto_0

    .line 441
    .end local v3    # "ret":I
    .restart local v0    # "adapter":Ljava/lang/String;
    :cond_5
    const/4 v2, 0x1

    goto :goto_1

    .line 443
    .end local v0    # "adapter":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 444
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method private performNextConnectOperation()Z
    .locals 4

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 285
    .local v0, "ret":Z
    const-string v1, "SandboxedProcessServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Entering performNextConnectOperation with state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and slot:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    if-ne v1, v2, :cond_2

    .line 290
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->initializeCryptoService()Z

    move-result v1

    if-nez v1, :cond_1

    .line 291
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    if-ne v1, v2, :cond_0

    .line 292
    const/4 v0, 0x1

    .line 337
    :cond_0
    :goto_0
    return v0

    .line 295
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 297
    :cond_2
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_INTIALIZED:I

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_CONNECTED:I

    if-ne v1, v2, :cond_5

    .line 301
    :cond_3
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->connectInternal()Z

    move-result v1

    if-nez v1, :cond_4

    .line 302
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    if-ne v1, v2, :cond_0

    .line 303
    const/4 v0, 0x1

    goto :goto_0

    .line 306
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 308
    :cond_5
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    if-ne v1, v2, :cond_9

    .line 312
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mPrevConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    if-ne v1, v2, :cond_7

    .line 314
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->initializeCryptoService()Z

    move-result v1

    if-nez v1, :cond_6

    .line 315
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    if-ne v1, v2, :cond_0

    .line 316
    const/4 v0, 0x1

    goto :goto_0

    .line 319
    :cond_6
    const/4 v0, 0x1

    goto :goto_0

    .line 321
    :cond_7
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mPrevConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_INTIALIZED:I

    if-ne v1, v2, :cond_0

    .line 324
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->connectInternal()Z

    move-result v1

    if-nez v1, :cond_8

    .line 325
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I

    if-ne v1, v2, :cond_0

    .line 326
    const/4 v0, 0x1

    goto :goto_0

    .line 329
    :cond_8
    const/4 v0, 0x1

    goto :goto_0

    .line 332
    :cond_9
    iget v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_FAILED:I

    if-ne v1, v2, :cond_0

    .line 334
    sget v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    iput v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 335
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->performNextConnectOperation()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public connect()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 169
    const-string v3, "SandboxedProcessServiceConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Connect with slot: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v3, -0x1

    iget v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    if-ne v3, v4, :cond_1

    .line 187
    :cond_0
    :goto_0
    return v1

    .line 174
    :cond_1
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    if-eqz v3, :cond_2

    .line 175
    invoke-direct {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->performNextConnectOperation()Z

    move-result v1

    goto :goto_0

    .line 177
    :cond_2
    new-instance v0, Landroid/content/Intent;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "com.sec.smartcard.adapter.BIND_SMARTCARD_SANDBOXED_SERVICE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 178
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 179
    sget v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    iput v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 180
    const-string v1, "SandboxedProcessServiceConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Setting connection state to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", for connection object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAdapterConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v3, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 182
    iput-boolean v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsCryptoServiceBounded:Z

    move v1, v2

    .line 183
    goto :goto_0
.end method

.method public connect(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    .line 161
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->connect()Z

    move-result v0

    return v0
.end method

.method public disconnect()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 194
    const-string v3, "SandboxedProcessServiceConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inside disconnect function for slot: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    sget v4, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_NONE:I

    if-ne v3, v4, :cond_0

    .line 249
    :goto_0
    return-void

    .line 200
    :cond_0
    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsCryptoServiceBounded:Z

    if-ne v3, v4, :cond_2

    .line 202
    :try_start_0
    const-string v3, "SandboxedProcessServiceConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cleaning up cryptoservice binding for slot: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    if-eqz v3, :cond_1

    .line 206
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->isAutoConnectionMode()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 207
    const-string v3, "SandboxedProcessServiceConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deregistering the listener for slot: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in auto connect mode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAutoConnectionName:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v3, v4, v5}, Lcom/sec/smartcard/adapter/ICryptoService;->deregisterListener(Ljava/lang/String;Lcom/sec/smartcard/adapter/ICryptoServiceListener;)V

    .line 209
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAutoConnectionName:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/sec/smartcard/adapter/ICryptoService;->disconnect(Ljava/lang/String;)V

    .line 219
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    invoke-interface {v3}, Lcom/sec/smartcard/adapter/ICryptoService;->deinitialize()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_2
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAdapterConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 225
    iput-boolean v6, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsCryptoServiceBounded:Z

    .line 229
    :cond_2
    iget-boolean v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsAdapterSelReceiverRegistered:Z

    if-eqz v3, :cond_3

    .line 230
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 231
    iput-boolean v6, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsAdapterSelReceiverRegistered:Z

    .line 235
    :cond_3
    iget-boolean v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsConnSelReceiverRegistered:Z

    if-eqz v3, :cond_4

    .line 236
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 237
    iput-boolean v6, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsConnSelReceiverRegistered:Z

    .line 241
    :cond_4
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v2

    .line 242
    .local v2, "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    iget v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    iget v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mClientUid:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->freeService(II)Z

    .line 244
    iput v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    .line 245
    iput v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mClientUid:I

    .line 246
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    .line 247
    sget v3, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_NONE:I

    iput v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 248
    const-string v3, "SandboxedProcessServiceConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting connection state to: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", for connection object: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 211
    .end local v2    # "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    :cond_5
    :try_start_1
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "connectionName":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 213
    const-string v3, "SandboxedProcessServiceConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deregistering the listener for slot: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v3, v0, v4}, Lcom/sec/smartcard/adapter/ICryptoService;->deregisterListener(Ljava/lang/String;Lcom/sec/smartcard/adapter/ICryptoServiceListener;)V

    .line 215
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    invoke-interface {v3, v0}, Lcom/sec/smartcard/adapter/ICryptoService;->disconnect(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 220
    .end local v0    # "connectionName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public getClient()Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    return-object v0
.end method

.method public getLastUsageTime()J
    .locals 4

    .prologue
    .line 268
    const-wide/16 v2, -0x1

    .line 269
    .local v2, "lastUsedTime":J
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    if-eqz v1, :cond_0

    .line 271
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    invoke-interface {v1}, Lcom/sec/smartcard/adapter/ICryptoService;->getLastUsageTimeStamp()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 276
    :cond_0
    :goto_0
    return-wide v2

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSlot()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    return v0
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 129
    sget v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    iget v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    if-eq v1, v2, :cond_0

    sget v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_NONE:I

    iget v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    if-ne v1, v2, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    sget v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I

    iput v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I

    .line 134
    const-string v1, "SandboxedProcessServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calling reset connection for slot: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    if-eqz v1, :cond_2

    .line 139
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x49

    invoke-interface {v1, v2, v3}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 146
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    if-eqz v1, :cond_0

    .line 148
    :try_start_1
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;

    invoke-interface {v1}, Lcom/sec/smartcard/adapter/ICryptoService;->deinitialize()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 140
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 141
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
