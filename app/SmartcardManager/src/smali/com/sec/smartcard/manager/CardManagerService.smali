.class public Lcom/sec/smartcard/manager/CardManagerService;
.super Landroid/app/Service;
.source "CardManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;,
        Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;
    }
.end annotation


# static fields
.field public static final BIND_PIN_SERVICE:Ljava/lang/String; = "com.sec.smartcard.pinservice.action.BIND_SMART_CARD_PIN_SERVICE"

.field public static final BIND_SMARTCARD_SANDBOXED_SERVICE:Ljava/lang/String; = "com.sec.smartcard.adapter.BIND_SMARTCARD_SANDBOXED_SERVICE"

.field public static final BIND_SMARTCARD_SERVICE:Ljava/lang/String; = "com.sec.smartcard.manager.ISmartCardService"

.field private static final CARD_MANAGER_SERVICE:Ljava/lang/String; = "CardManagerService"

.field private static final SMARTCARD_PREMISSION:Ljava/lang/String; = "com.sec.smartcard.pinservice.permission.SMARTCARD_CRYPTO"

.field private static final SMARTCARD_PREMISSION_ERROR:Ljava/lang/String; = "Need com.sec.smartcard.pinservice.permission.SMARTCARD_CRYPTO permission"

.field private static final TAG:Ljava/lang/String; = "CardManagerService"


# instance fields
.field private final mBinder:Lcom/sec/smartcard/manager/ISmartCardService$Stub;

.field private mContext:Landroid/content/Context;

.field private mCrytoServiceClients:Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB",
            "<",
            "Lcom/sec/smartcard/adapter/ICryptoServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoteCallbackDied:Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied",
            "<",
            "Lcom/sec/smartcard/adapter/ICryptoServiceListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 123
    new-instance v0, Lcom/sec/smartcard/manager/CardManagerService$1;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/CardManagerService$1;-><init>(Lcom/sec/smartcard/manager/CardManagerService;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/CardManagerService;->mBinder:Lcom/sec/smartcard/manager/ISmartCardService$Stub;

    .line 214
    new-instance v0, Lcom/sec/smartcard/manager/CardManagerService$2;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/CardManagerService$2;-><init>(Lcom/sec/smartcard/manager/CardManagerService;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/CardManagerService;->mRemoteCallbackDied:Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;

    .line 226
    new-instance v0, Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;

    iget-object v1, p0, Lcom/sec/smartcard/manager/CardManagerService;->mRemoteCallbackDied:Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;

    invoke-direct {v0, p0, v1}, Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;-><init>(Lcom/sec/smartcard/manager/CardManagerService;Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/CardManagerService;->mCrytoServiceClients:Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/manager/CardManagerService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/manager/CardManagerService;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/smartcard/manager/CardManagerService;->enforceSmartCardPermission(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/smartcard/manager/CardManagerService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/CardManagerService;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/smartcard/manager/CardManagerService;)Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/CardManagerService;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/smartcard/manager/CardManagerService;->mCrytoServiceClients:Lcom/sec/smartcard/manager/CardManagerService$RemoteCallbackListWithDiedCB;

    return-object v0
.end method

.method private enforceSmartCardPermission(Ljava/lang/String;)V
    .locals 9
    .param p1, "method_nm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 231
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 232
    .local v5, "uid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    .line 233
    .local v2, "pid":I
    iget-object v6, p0, Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "caller":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "PkgName":Ljava/lang/String;
    const-string v6, "CardManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "caller: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "PkgName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 245
    :cond_0
    return-void

    .line 241
    :cond_1
    iget-object v6, p0, Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.smartcard.pinservice.permission.SMARTCARD_CRYPTO"

    invoke-virtual {v6, v7}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v3

    .line 242
    .local v3, "ret_val":I
    iget-object v6, p0, Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/enterprise/knox/seams/SEAMS;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/seams/SEAMS;

    move-result-object v6

    const-string v7, "CardManagerService"

    invoke-virtual {v6, v2, v5, v7, p1}, Lcom/sec/enterprise/knox/seams/SEAMS;->isAuthorized(IILjava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 244
    .local v4, "returnValue":I
    if-eqz v3, :cond_0

    if-gez v4, :cond_0

    .line 247
    new-instance v6, Ljava/lang/SecurityException;

    const-string v7, "No SmartCard Permission"

    invoke-direct {v6, v7}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v6
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 90
    const/4 v1, 0x0

    .line 91
    .local v1, "binder":Landroid/os/IBinder;
    if-nez p1, :cond_1

    .line 92
    const-string v2, "CardManagerService"

    const-string v3, "onBind : intent is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    .end local v1    # "binder":Landroid/os/IBinder;
    :cond_0
    :goto_0
    return-object v1

    .line 96
    .restart local v1    # "binder":Landroid/os/IBinder;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 98
    const-string v2, "com.sec.smartcard.pinservice.action.BIND_SMART_CARD_PIN_SERVICE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 99
    const-string v2, "CardManagerService"

    const-string v3, "onBind : SmartcardPin serveice connected"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/CardManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v1

    goto :goto_0

    .line 101
    :cond_2
    const-string v2, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const-string v2, "CardManagerService"

    const-string v3, "onBind : Binding to smart card manager service"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v1, p0, Lcom/sec/smartcard/manager/CardManagerService;->mBinder:Lcom/sec/smartcard/manager/ISmartCardService$Stub;

    goto :goto_0

    .line 107
    :cond_3
    const-string v2, "CardManagerService"

    const-string v3, "onBind : action is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 81
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/CardManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/manager/CardManagerService;->mContext:Landroid/content/Context;

    .line 82
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
