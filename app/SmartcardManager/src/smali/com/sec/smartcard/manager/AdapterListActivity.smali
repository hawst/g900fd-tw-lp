.class public Lcom/sec/smartcard/manager/AdapterListActivity;
.super Landroid/app/Activity;
.source "AdapterListActivity.java"


# static fields
.field static final TAG:Ljava/lang/String; = "AdapterListActivity"


# instance fields
.field private mAdapterPkgNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/manager/AdapterListActivity;->mAdapterPkgNames:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/manager/AdapterListActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/AdapterListActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/smartcard/manager/AdapterListActivity;->mAdapterPkgNames:Ljava/util/List;

    return-object v0
.end method

.method private displayErrorDialog()V
    .locals 5

    .prologue
    .line 142
    new-instance v1, Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x4

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 144
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f050039

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1080027

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f05003a

    new-instance v4, Lcom/sec/smartcard/manager/AdapterListActivity$3;

    invoke-direct {v4, p0}, Lcom/sec/smartcard/manager/AdapterListActivity$3;-><init>(Lcom/sec/smartcard/manager/AdapterListActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 157
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 159
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 161
    return-void
.end method


# virtual methods
.method public displayAdapterList()V
    .locals 5

    .prologue
    .line 104
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 105
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f050036

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/smartcard/manager/CustomArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/smartcard/manager/AdapterListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/smartcard/manager/AdapterListActivity;->mAdapterPkgNames:Ljava/util/List;

    invoke-direct {v2, v3, v4}, Lcom/sec/smartcard/manager/CustomArrayAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    new-instance v3, Lcom/sec/smartcard/manager/AdapterListActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/smartcard/manager/AdapterListActivity$1;-><init>(Lcom/sec/smartcard/manager/AdapterListActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 121
    new-instance v1, Lcom/sec/smartcard/manager/AdapterListActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/smartcard/manager/AdapterListActivity$2;-><init>(Lcom/sec/smartcard/manager/AdapterListActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 132
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 134
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/smartcard/manager/AdapterListActivity;->mDialog:Landroid/app/AlertDialog;

    .line 135
    iget-object v1, p0, Lcom/sec/smartcard/manager/AdapterListActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 136
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/manager/AdapterListActivity;->requestWindowFeature(I)Z

    .line 66
    const-string v0, "AdapterListActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/smartcard/manager/AdapterListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "Adapters"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/AdapterListActivity;->mAdapterPkgNames:Ljava/util/List;

    .line 70
    iget-object v0, p0, Lcom/sec/smartcard/manager/AdapterListActivity;->mAdapterPkgNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/sec/smartcard/manager/AdapterListActivity;->displayErrorDialog()V

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/AdapterListActivity;->displayAdapterList()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 80
    const-string v0, "AdapterListActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 92
    const-string v0, "AdapterListActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 86
    const-string v0, "AdapterListActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 98
    const-string v0, "AdapterListActivity"

    const-string v1, "onUserLeaveHint"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/sec/smartcard/manager/AdapterListActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 100
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/AdapterListActivity;->finish()V

    .line 101
    return-void
.end method
