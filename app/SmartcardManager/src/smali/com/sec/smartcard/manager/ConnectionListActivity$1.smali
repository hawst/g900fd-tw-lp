.class Lcom/sec/smartcard/manager/ConnectionListActivity$1;
.super Ljava/lang/Object;
.source "ConnectionListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/smartcard/manager/ConnectionListActivity;->displayConnectionsList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/ConnectionListActivity;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/ConnectionListActivity;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/smartcard/manager/ConnectionListActivity$1;->this$0:Lcom/sec/smartcard/manager/ConnectionListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 112
    const-string v1, "ConnectionListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selected item position is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 114
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.smartcard.manager.CONNECTION_SELECTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const-string v2, "Connections"

    iget-object v1, p0, Lcom/sec/smartcard/manager/ConnectionListActivity$1;->this$0:Lcom/sec/smartcard/manager/ConnectionListActivity;

    # getter for: Lcom/sec/smartcard/manager/ConnectionListActivity;->mConnectionNames:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/smartcard/manager/ConnectionListActivity;->access$000(Lcom/sec/smartcard/manager/ConnectionListActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    iget-object v1, p0, Lcom/sec/smartcard/manager/ConnectionListActivity$1;->this$0:Lcom/sec/smartcard/manager/ConnectionListActivity;

    invoke-virtual {v1, v0}, Lcom/sec/smartcard/manager/ConnectionListActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 119
    iget-object v1, p0, Lcom/sec/smartcard/manager/ConnectionListActivity$1;->this$0:Lcom/sec/smartcard/manager/ConnectionListActivity;

    invoke-virtual {v1}, Lcom/sec/smartcard/manager/ConnectionListActivity;->finish()V

    .line 120
    return-void
.end method
