.class public Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
.super Ljava/lang/Object;
.source "SandboxedProcessServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;,
        Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;
    }
.end annotation


# static fields
.field private static final MAX_PROCESS_SERVICE:I = 0x6

.field private static final TAG:Ljava/lang/String; = "SanboxedProcessServiceManager"

.field static mServiceManager:Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;


# instance fields
.field private SOCKET_NAME_PREFIX:Ljava/lang/String;

.field private mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    iput-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    .line 48
    const-string v0, "sec.socket.user."

    iput-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->SOCKET_NAME_PREFIX:Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    invoke-direct {v2, p0, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;)V

    aput-object v2, v0, v1

    .line 54
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    invoke-direct {v2, p0, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;)V

    aput-object v2, v0, v1

    .line 55
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    invoke-direct {v2, p0, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;)V

    aput-object v2, v0, v1

    .line 56
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    const/4 v1, 0x3

    new-instance v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    invoke-direct {v2, p0, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;)V

    aput-object v2, v0, v1

    .line 57
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    const/4 v1, 0x4

    new-instance v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    invoke-direct {v2, p0, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;)V

    aput-object v2, v0, v1

    .line 58
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    const/4 v1, 0x5

    new-instance v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    invoke-direct {v2, p0, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;)V

    aput-object v2, v0, v1

    .line 59
    return-void
.end method

.method public static getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mServiceManager:Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    invoke-direct {v0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;-><init>()V

    sput-object v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mServiceManager:Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    .line 65
    :cond_0
    sget-object v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mServiceManager:Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    return-object v0
.end method


# virtual methods
.method public allocateService(Landroid/content/Context;III)Z
    .locals 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "slot"    # I
    .param p3, "uid"    # I
    .param p4, "settings"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 142
    if-gez p3, :cond_0

    .line 153
    :goto_0
    return v0

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    monitor-enter v2

    .line 146
    :try_start_0
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v3, v3, p2

    iget v3, v3, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 147
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v0, v0, p2

    iput p3, v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    .line 148
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v0, v0, p2

    new-instance v3, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-direct {v3, p1, p3, p2, p4}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;-><init>(Landroid/content/Context;III)V

    iput-object v3, v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .line 149
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v0, v0, p2

    const/4 v3, 0x1

    iput v3, v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->state:I

    .line 150
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 152
    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public freeService(II)Z
    .locals 5
    .param p1, "slot"    # I
    .param p2, "uid"    # I

    .prologue
    const/4 v0, 0x0

    .line 205
    if-gez p2, :cond_0

    .line 218
    :goto_0
    return v0

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    monitor-enter v1

    .line 209
    :try_start_0
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v2, v2, p1

    iget v2, v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    if-ne v2, p2, :cond_1

    .line 210
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v0, v0, p1

    const/4 v2, -0x1

    iput v2, v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    .line 211
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v0, v0, p1

    const/4 v2, 0x0

    iput v2, v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->state:I

    .line 212
    iget-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v0, v0, p1

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .line 213
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 215
    :cond_1
    :try_start_1
    const-string v2, "SanboxedProcessServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "freeService not matching with the occupier, occupier:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getAvailableConnection(Landroid/content/Context;ILjava/lang/String;I)Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "callerUid"    # I
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "settings"    # I

    .prologue
    .line 222
    const/4 v1, 0x0

    .line 223
    .local v1, "srvConn":Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    invoke-virtual {p0, p1, p2, p4}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getAvailableService(Landroid/content/Context;II)I

    move-result v0

    .line 224
    .local v0, "serviceNumber":I
    if-ltz v0, :cond_0

    .line 225
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v2, v2, v0

    iget-object v1, v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .line 227
    :cond_0
    return-object v1
.end method

.method public getAvailableService(Landroid/content/Context;II)I
    .locals 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "uid"    # I
    .param p3, "settings"    # I

    .prologue
    const/4 v2, -0x1

    .line 112
    if-gez p2, :cond_0

    move v0, v2

    .line 138
    :goto_0
    return v0

    .line 115
    :cond_0
    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    monitor-enter v3

    .line 116
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v4, 0x6

    if-ge v0, v4, :cond_3

    .line 118
    :try_start_0
    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    if-ne v4, p2, :cond_1

    .line 119
    monitor-exit v3

    goto :goto_0

    .line 137
    .end local v0    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 122
    .restart local v0    # "i":I
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    if-gez v4, :cond_2

    .line 124
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->allocateService(Landroid/content/Context;III)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 125
    monitor-exit v3

    goto :goto_0

    .line 116
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 130
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getReusableSlot(Landroid/content/Context;)I

    move-result v1

    .line 131
    .local v1, "reuseSlot":I
    if-eq v2, v1, :cond_4

    .line 132
    iget-object v4, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    if-eqz v4, :cond_4

    .line 133
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->reset()V

    .line 134
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getAvailableService(Landroid/content/Context;II)I

    move-result v0

    .end local v0    # "i":I
    monitor-exit v3

    goto :goto_0

    .line 137
    .restart local v0    # "i":I
    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v2

    .line 138
    goto :goto_0
.end method

.method public getReusableSlot(Landroid/content/Context;)I
    .locals 13
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v12, -0x1

    .line 75
    const/4 v1, -0x1

    .line 76
    .local v1, "slot":I
    const-wide/16 v4, 0x0

    .line 78
    .local v4, "lastUsedTimeHolder":J
    if-nez p1, :cond_0

    move v6, v1

    .line 108
    .end local v1    # "slot":I
    .local v6, "slot":I
    :goto_0
    return v6

    .line 81
    .end local v6    # "slot":I
    .restart local v1    # "slot":I
    :cond_0
    iget-object v8, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    monitor-enter v8

    .line 82
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v7, 0x6

    if-ge v0, v7, :cond_4

    .line 83
    :try_start_0
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v7, v7, v0

    iget v7, v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    if-eq v7, v12, :cond_1

    .line 85
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v7, v7, v0

    iget v7, v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v7, v9, :cond_2

    .line 82
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 88
    :cond_2
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    if-eqz v7, :cond_1

    .line 89
    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-virtual {v7}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->getLastUsageTime()J

    move-result-wide v2

    .line 92
    .local v2, "lastUsedTime":J
    const-wide/16 v10, -0x1

    cmp-long v7, v10, v2

    if-eqz v7, :cond_1

    .line 93
    if-ne v12, v1, :cond_3

    .line 94
    move-wide v4, v2

    .line 95
    move v1, v0

    goto :goto_2

    .line 98
    :cond_3
    cmp-long v7, v2, v4

    if-gez v7, :cond_1

    .line 99
    move-wide v4, v2

    .line 100
    move v1, v0

    goto :goto_2

    .line 107
    .end local v2    # "lastUsedTime":J
    :cond_4
    monitor-exit v8

    move v6, v1

    .line 108
    .end local v1    # "slot":I
    .restart local v6    # "slot":I
    goto :goto_0

    .line 107
    .end local v6    # "slot":I
    .restart local v1    # "slot":I
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7
.end method

.method public getSandboxedProcessServiceConnection(I)Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    .locals 3
    .param p1, "uid"    # I

    .prologue
    .line 182
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    monitor-enter v2

    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    if-ne v1, p1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    monitor-exit v2

    .line 190
    :goto_1
    return-object v1

    .line 183
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_1
    monitor-exit v2

    .line 190
    const/4 v1, 0x0

    goto :goto_1

    .line 189
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getServiceSocket(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "uid"    # I

    .prologue
    .line 157
    iget-object v6, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    monitor-enter v6

    .line 158
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v5, 0x6

    if-ge v0, v5, :cond_2

    .line 160
    :try_start_0
    iget-object v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v5, v5, v0

    iget v5, v5, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    if-ne v5, p2, :cond_1

    .line 161
    iget-object v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    if-eqz v5, :cond_1

    .line 162
    iget-object v5, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-virtual {v5}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->getSlot()I

    move-result v1

    .line 163
    .local v1, "slot":I
    if-ltz v1, :cond_1

    .line 164
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    .line 165
    .local v3, "user":I
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 166
    .local v4, "userName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 167
    .local v2, "socket":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 168
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->SOCKET_NAME_PREFIX:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "."

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172
    :goto_1
    monitor-exit v6

    .line 178
    .end local v1    # "slot":I
    .end local v2    # "socket":Ljava/lang/String;
    .end local v3    # "user":I
    .end local v4    # "userName":Ljava/lang/String;
    :goto_2
    return-object v2

    .line 170
    .restart local v1    # "slot":I
    .restart local v2    # "socket":Ljava/lang/String;
    .restart local v3    # "user":I
    .restart local v4    # "userName":Ljava/lang/String;
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->SOCKET_NAME_PREFIX:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 158
    .end local v1    # "slot":I
    .end local v2    # "socket":Ljava/lang/String;
    .end local v3    # "user":I
    .end local v4    # "userName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_2
    monitor-exit v6

    .line 178
    const/4 v2, 0x0

    goto :goto_2

    .line 177
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public lockedService(I)Z
    .locals 4
    .param p1, "slot"    # I

    .prologue
    const/4 v0, 0x1

    .line 195
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    monitor-enter v1

    .line 196
    :try_start_0
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v2, v2, p1

    iget v2, v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->state:I

    if-ne v2, v0, :cond_0

    .line 197
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v2, v2, p1

    const/4 v3, 0x2

    iput v3, v2, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->state:I

    .line 198
    monitor-exit v1

    .line 201
    :goto_0
    return v0

    .line 200
    :cond_0
    monitor-exit v1

    .line 201
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resetSandboxProcessServiceConnections()V
    .locals 3

    .prologue
    .line 253
    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    monitor-enter v2

    .line 254
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_2

    .line 256
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    if-gez v1, :cond_1

    .line 254
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 261
    :cond_1
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    if-eqz v1, :cond_0

    .line 262
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->mConnections:[Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-virtual {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->reset()V

    goto :goto_1

    .line 265
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266
    return-void
.end method

.method public startService(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 241
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 242
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 243
    const/4 v1, 0x1

    .line 244
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public stopService(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 248
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 249
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method
