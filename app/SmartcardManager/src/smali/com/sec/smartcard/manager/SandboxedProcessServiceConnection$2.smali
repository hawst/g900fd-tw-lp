.class Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;
.super Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback$Stub;
.source "SandboxedProcessServiceConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)V
    .locals 0

    .prologue
    .line 489
    iput-object p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitComplete()V
    .locals 4

    .prologue
    .line 492
    const-string v1, "SandboxedProcessServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Crypto service initialized for slot: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$700(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_INTIALIZED:I
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$800()I

    move-result v2

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$402(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;I)I

    .line 494
    const-string v1, "SandboxedProcessServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting connection state to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$400(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", for connection object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # invokes: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->connectInternal()Z
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$900(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 498
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$400(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v1

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_WAIT:I
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1000()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 500
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 502
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x49

    invoke-interface {v1, v2, v3}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 503
    :catch_0
    move-exception v0

    .line 504
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onInitFailed()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 513
    const-string v1, "SandboxedProcessServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Crypto service initialization failed for slot: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$700(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_FAILED:I
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$1100()I

    move-result v2

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$402(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;I)I

    .line 515
    const-string v1, "SandboxedProcessServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting connection state to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$400(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", for connection object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 519
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$2;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x65

    invoke-interface {v1, v2, v3}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 525
    :cond_0
    :goto_0
    return-void

    .line 521
    :catch_0
    move-exception v0

    .line 522
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
