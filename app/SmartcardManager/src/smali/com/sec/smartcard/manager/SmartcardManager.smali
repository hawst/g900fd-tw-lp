.class public Lcom/sec/smartcard/manager/SmartcardManager;
.super Landroid/app/Application;
.source "SmartcardManager.java"


# static fields
.field public static final BIND_SMARTCARD_SANDBOXED_SERVICE:Ljava/lang/String; = "com.sec.smartcard.adapter.BIND_SMARTCARD_SANDBOXED_SERVICE"

.field private static final TAG:Ljava/lang/String; = "SmartcardManager App"

.field private static singleton:Lcom/sec/smartcard/manager/SmartcardManager;


# instance fields
.field private mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

.field private mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

.field private mInitialized:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    .line 164
    new-instance v0, Lcom/sec/smartcard/manager/SmartcardManager$2;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/SmartcardManager$2;-><init>(Lcom/sec/smartcard/manager/SmartcardManager;)V

    iput-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/smartcard/manager/SmartcardManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SmartcardManager;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/smartcard/manager/SmartcardManager;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/manager/SmartcardManager;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/smartcard/manager/SmartcardManager;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/smartcard/manager/SmartcardManager;->singleton:Lcom/sec/smartcard/manager/SmartcardManager;

    return-object v0
.end method


# virtual methods
.method public connectToService(I)Z
    .locals 5
    .param p1, "settings"    # I

    .prologue
    .line 118
    iget-boolean v2, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    if-nez v2, :cond_0

    .line 119
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v0

    .line 120
    .local v0, "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SmartcardManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SmartcardManager;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v4, "com.sec.smartcard.adapter.BIND_SMARTCARD_SANDBOXED_SERVICE"

    invoke-virtual {v0, v2, v3, v4, p1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getAvailableConnection(Landroid/content/Context;ILjava/lang/String;I)Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    move-result-object v1

    .line 122
    .local v1, "srvConn":Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    if-eqz v1, :cond_0

    .line 123
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-virtual {v1, v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->connect(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Z

    .line 126
    .end local v0    # "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    .end local v1    # "srvConn":Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    :cond_0
    iget-boolean v2, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    return v2
.end method

.method public disconnectService()V
    .locals 4

    .prologue
    .line 150
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v0

    .line 151
    .local v0, "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SmartcardManager;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getSandboxedProcessServiceConnection(I)Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    move-result-object v2

    .line 152
    .local v2, "srvConn":Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    if-eqz v2, :cond_0

    .line 153
    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->disconnect()V

    .line 154
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v1

    .line 156
    .local v1, "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->resetPkcs11()V

    .line 157
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    .line 159
    .end local v1    # "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    :cond_0
    return-void
.end method

.method public disconnectServiceAndNotifyClient()V
    .locals 4

    .prologue
    .line 134
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v0

    .line 135
    .local v0, "mgr":Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/SmartcardManager;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getSandboxedProcessServiceConnection(I)Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    move-result-object v2

    .line 136
    .local v2, "srvConn":Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
    if-eqz v2, :cond_0

    .line 137
    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->disconnect()V

    .line 138
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v1

    .line 140
    .local v1, "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardConnectionError()V

    .line 141
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->resetPkcs11()V

    .line 142
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    .line 144
    .end local v1    # "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    :cond_0
    return-void
.end method

.method public finalizeService()V
    .locals 3

    .prologue
    .line 93
    iget-object v1, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-eqz v1, :cond_0

    .line 94
    iget-object v2, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    monitor-enter v2

    .line 95
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->deinitialize()V

    .line 96
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v0

    .line 98
    .local v0, "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->resetPkcs11()V

    .line 99
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    .line 100
    monitor-exit v2

    .line 102
    .end local v0    # "pinServer":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    :cond_0
    return-void

    .line 100
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public initializeService()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v0, :cond_0

    .line 47
    invoke-static {p0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    .line 48
    :cond_0
    iget-boolean v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    if-nez v0, :cond_1

    .line 49
    const-string v0, "SmartcardManager App"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    new-instance v1, Lcom/sec/smartcard/manager/SmartcardManager$1;

    invoke-direct {v1, p0}, Lcom/sec/smartcard/manager/SmartcardManager$1;-><init>(Lcom/sec/smartcard/manager/SmartcardManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->initialize(Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;)V

    .line 86
    :cond_1
    iget-boolean v0, p0, Lcom/sec/smartcard/manager/SmartcardManager;->mInitialized:Z

    return v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 36
    sput-object p0, Lcom/sec/smartcard/manager/SmartcardManager;->singleton:Lcom/sec/smartcard/manager/SmartcardManager;

    .line 37
    return-void
.end method

.method public resetSandboxedProcessServiceConnections()V
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->resetSandboxProcessServiceConnections()V

    .line 109
    return-void
.end method
