.class Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;
.super Ljava/lang/Object;
.source "SandboxedProcessServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SlotProcessInfo"
.end annotation


# static fields
.field protected static final SERVICE_ALLOCATED:I = 0x1

.field protected static final SERVICE_STOPPED:I = 0x0

.field protected static final SERVICE_USED:I = 0x2


# instance fields
.field protected srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

.field protected state:I

.field final synthetic this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

.field protected uid:I


# direct methods
.method private constructor <init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;)V
    .locals 1

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->uid:I

    .line 236
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->state:I

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;->srvConn:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;
    .param p2, "x1"    # Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$1;

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager$SlotProcessInfo;-><init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;)V

    return-void
.end method
