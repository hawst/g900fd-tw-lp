.class Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;
.super Ljava/lang/Object;
.source "SandboxedProcessServiceConnection.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .prologue
    .line 455
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    invoke-static {p2}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ICryptoService;

    move-result-object v2

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$002(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Lcom/sec/smartcard/adapter/ICryptoService;)Lcom/sec/smartcard/adapter/ICryptoService;

    .line 457
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$000(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 458
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # invokes: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->performNextConnectOperation()Z
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$100(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 459
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 461
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x49

    invoke-interface {v1, v2, v3}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 462
    :catch_0
    move-exception v0

    .line 463
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 475
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->STATE_CONNECTION_START:I
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$500()I

    move-result v2

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mConnectionState:I
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$402(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;I)I

    .line 476
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    const/4 v2, 0x0

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mBoundCryptoService:Lcom/sec/smartcard/adapter/ICryptoService;
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$002(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Lcom/sec/smartcard/adapter/ICryptoService;)Lcom/sec/smartcard/adapter/ICryptoService;

    .line 477
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    const/4 v2, 0x0

    # setter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mIsCryptoServiceBounded:Z
    invoke-static {v1, v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$602(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;Z)Z

    .line 478
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 480
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-static {v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$200(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$300(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x49

    invoke-interface {v1, v2, v3}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :cond_0
    :goto_0
    const-string v1, "SandboxedProcessServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "On service disconnected for slot:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection$1;->this$0:Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    # getter for: Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->mSandboxSlot:I
    invoke-static {v3}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->access$700(Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    return-void

    .line 481
    :catch_0
    move-exception v0

    .line 482
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
