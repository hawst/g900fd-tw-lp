.class Lcom/sec/smartcard/manager/AdapterListActivity$1;
.super Ljava/lang/Object;
.source "AdapterListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/smartcard/manager/AdapterListActivity;->displayAdapterList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/AdapterListActivity;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/AdapterListActivity;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/smartcard/manager/AdapterListActivity$1;->this$0:Lcom/sec/smartcard/manager/AdapterListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 110
    const-string v1, "AdapterListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selected item position is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 112
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.smartcard.manager.ADAPTER_SELECTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v2, "Adapters"

    iget-object v1, p0, Lcom/sec/smartcard/manager/AdapterListActivity$1;->this$0:Lcom/sec/smartcard/manager/AdapterListActivity;

    # getter for: Lcom/sec/smartcard/manager/AdapterListActivity;->mAdapterPkgNames:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/smartcard/manager/AdapterListActivity;->access$000(Lcom/sec/smartcard/manager/AdapterListActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    iget-object v1, p0, Lcom/sec/smartcard/manager/AdapterListActivity$1;->this$0:Lcom/sec/smartcard/manager/AdapterListActivity;

    invoke-virtual {v1, v0}, Lcom/sec/smartcard/manager/AdapterListActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 117
    iget-object v1, p0, Lcom/sec/smartcard/manager/AdapterListActivity$1;->this$0:Lcom/sec/smartcard/manager/AdapterListActivity;

    invoke-virtual {v1}, Lcom/sec/smartcard/manager/AdapterListActivity;->finish()V

    .line 118
    return-void
.end method
