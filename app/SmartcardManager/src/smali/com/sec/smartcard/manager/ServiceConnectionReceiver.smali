.class public Lcom/sec/smartcard/manager/ServiceConnectionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ServiceConnectionReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 10
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 17
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 18
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 20
    const-string v2, "TODO SERVICE BOUND"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 22
    const-string v2, "service"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 23
    .local v1, "serviceName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 24
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->startService(Landroid/content/Context;Ljava/lang/String;)Z

    .line 37
    .end local v1    # "serviceName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    const-string v2, "TODO SERVICE UNBOUND"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    const-string v2, "service"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 29
    .restart local v1    # "serviceName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 30
    invoke-static {}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->getInstance()Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/sec/smartcard/manager/SandboxedProcessServiceManager;->stopService(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 35
    .end local v1    # "serviceName":Ljava/lang/String;
    :cond_2
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "Not yet implemented"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
