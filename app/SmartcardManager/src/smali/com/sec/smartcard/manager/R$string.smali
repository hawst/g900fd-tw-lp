.class public final Lcom/sec/smartcard/manager/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Ok:I = 0x7f05003a

.field public static final app_name:I = 0x7f050000

.field public static final clear:I = 0x7f050012

.field public static final enterprise_device_admin:I = 0x7f050001

.field public static final enterprise_device_admin_description:I = 0x7f050003

.field public static final enterprise_device_admin_summary:I = 0x7f050002

.field public static final image_icon:I = 0x7f050037

.field public static final num_0:I = 0x7f05003b

.field public static final num_1:I = 0x7f05003c

.field public static final num_2:I = 0x7f05003d

.field public static final num_3:I = 0x7f05003e

.field public static final num_4:I = 0x7f05003f

.field public static final num_5:I = 0x7f050040

.field public static final num_6:I = 0x7f050041

.field public static final num_7:I = 0x7f050042

.field public static final num_8:I = 0x7f050043

.field public static final num_9:I = 0x7f050044

.field public static final permdesc_smartcard_crypto:I = 0x7f05001e

.field public static final permdesc_smartcard_pin_access:I = 0x7f05001c

.field public static final permlab_smartcard_crypto:I = 0x7f05001d

.field public static final permlab_smartcard_pin_access:I = 0x7f05001b

.field public static final sc_choose_your_pin_header:I = 0x7f050019

.field public static final sc_pin_dialog:I = 0x7f050005

.field public static final sc_pin_dialog_cardconnectionerror:I = 0x7f05000e

.field public static final sc_pin_dialog_carderror:I = 0x7f050009

.field public static final sc_pin_dialog_cardexpired:I = 0x7f05000c

.field public static final sc_pin_dialog_cardlocked:I = 0x7f050007

.field public static final sc_pin_dialog_connectionerror:I = 0x7f050008

.field public static final sc_pin_dialog_invalidpin:I = 0x7f050006

.field public static final sc_pin_dialog_register_message:I = 0x7f050016

.field public static final sc_pin_dialog_registererror:I = 0x7f05000a

.field public static final sc_pin_dialog_registersuccess:I = 0x7f050010

.field public static final sc_pin_dialog_title:I = 0x7f050004

.field public static final sc_pin_dialog_unregister_message:I = 0x7f050017

.field public static final sc_pin_dialog_unregistererror:I = 0x7f05000b

.field public static final sc_pin_dialog_unregistersuccess:I = 0x7f050011

.field public static final sc_pin_dialog_wait_message:I = 0x7f050018

.field public static final sc_pin_dialog_wrongcard:I = 0x7f05000d

.field public static final sc_pin_register_errordialog:I = 0x7f05000f

.field public static final sc_view_certs_title:I = 0x7f05001a

.field public static final smartcard_adapter_no_connections:I = 0x7f050038

.field public static final smartcard_association_title:I = 0x7f050013

.field public static final smartcard_cancel_label:I = 0x7f050014

.field public static final smartcard_config:I = 0x7f050024

.field public static final smartcard_configuration:I = 0x7f05002c

.field public static final smartcard_continue_label:I = 0x7f050015

.field public static final smartcard_credential_settings:I = 0x7f05001f

.field public static final smartcard_cuid:I = 0x7f05002e

.field public static final smartcard_fw_version:I = 0x7f050034

.field public static final smartcard_hw_version:I = 0x7f050033

.field public static final smartcard_info:I = 0x7f05002d

.field public static final smartcard_label:I = 0x7f05002f

.field public static final smartcard_manufacture:I = 0x7f050030

.field public static final smartcard_model:I = 0x7f050031

.field public static final smartcard_no_adapters:I = 0x7f050039

.field public static final smartcard_notregisterd:I = 0x7f050021

.field public static final smartcard_opt_warning_lockscreen_policy_set:I = 0x7f05002b

.field public static final smartcard_opt_warning_lockscreen_set:I = 0x7f05002a

.field public static final smartcard_register:I = 0x7f050025

.field public static final smartcard_register_summary:I = 0x7f050026

.field public static final smartcard_registered:I = 0x7f050027

.field public static final smartcard_sel_adapter:I = 0x7f050028

.field public static final smartcard_sel_connection:I = 0x7f050029

.field public static final smartcard_sel_netadapter:I = 0x7f050036

.field public static final smartcard_serial_num:I = 0x7f050032

.field public static final smartcard_unregister:I = 0x7f050023

.field public static final smartcard_unregister_summary:I = 0x7f050022

.field public static final smartcard_view_cert:I = 0x7f050035

.field public static final stms_appgroup:I = 0x7f050046

.field public static final stms_version:I = 0x7f050045

.field public static final wifi_status:I = 0x7f050020


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
