.class Lcom/sec/smartcard/manager/CardManagerService$2;
.super Ljava/lang/Object;
.source "CardManagerService.java"

# interfaces
.implements Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/CardManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/smartcard/manager/CardManagerService$IRemoteCallbackDied",
        "<",
        "Lcom/sec/smartcard/adapter/ICryptoServiceListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/manager/CardManagerService;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/manager/CardManagerService;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/sec/smartcard/manager/CardManagerService$2;->this$0:Lcom/sec/smartcard/manager/CardManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic remoteCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/IInterface;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 214
    check-cast p1, Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    .end local p1    # "x0":Landroid/os/IInterface;
    invoke-virtual {p0, p1, p2}, Lcom/sec/smartcard/manager/CardManagerService$2;->remoteCallbackDied(Lcom/sec/smartcard/adapter/ICryptoServiceListener;Ljava/lang/Object;)V

    return-void
.end method

.method public remoteCallbackDied(Lcom/sec/smartcard/adapter/ICryptoServiceListener;Ljava/lang/Object;)V
    .locals 3
    .param p1, "cryptoServiceListener"    # Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .param p2, "cookie"    # Ljava/lang/Object;

    .prologue
    .line 216
    const-string v0, "CardManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "remote callback died, cryptoservice listener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const-string v0, "CardManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "remote callback died, service connection : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    if-eqz p2, :cond_0

    .line 220
    check-cast p2, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;

    .end local p2    # "cookie":Ljava/lang/Object;
    invoke-virtual {p2}, Lcom/sec/smartcard/manager/SandboxedProcessServiceConnection;->disconnect()V

    .line 222
    :cond_0
    return-void
.end method
