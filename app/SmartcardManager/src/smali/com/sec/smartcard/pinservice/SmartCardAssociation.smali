.class public Lcom/sec/smartcard/pinservice/SmartCardAssociation;
.super Landroid/app/Activity;
.source "SmartCardAssociation.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field private static mCurrent:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/smartcard/pinservice/SmartCardAssociation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mDialog:Landroid/app/ProgressDialog;

.field private mHandler:Landroid/os/Handler;

.field private mHeaderText:Landroid/widget/TextView;

.field private mIsOptProgress:Z

.field private mMessage:Ljava/lang/String;

.field private mNextButton:Landroid/widget/Button;

.field private mOperation:I

.field private mPasswordEntry:Landroid/widget/TextView;

.field private mSmartCardPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

.field private mTitle:Ljava/lang/String;

.field registerCallback:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

.field unRegisterCallback:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mCurrent:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    .line 40
    iput-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mSmartCardPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .line 45
    iput v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mOperation:I

    .line 49
    iput-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z

    .line 53
    new-instance v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation$1;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation$1;-><init>(Lcom/sec/smartcard/pinservice/SmartCardAssociation;)V

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;

    .line 250
    new-instance v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation$2;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation$2;-><init>(Lcom/sec/smartcard/pinservice/SmartCardAssociation;)V

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->registerCallback:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    .line 264
    new-instance v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation$3;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation$3;-><init>(Lcom/sec/smartcard/pinservice/SmartCardAssociation;)V

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->unRegisterCallback:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/pinservice/SmartCardAssociation;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardAssociation;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->handleAssociationResults(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/smartcard/pinservice/SmartCardAssociation;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardAssociation;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/smartcard/pinservice/SmartCardAssociation;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardAssociation;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->postMessage(Landroid/os/Message;)V

    return-void
.end method

.method private getEditTextValue(I)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 302
    invoke-virtual {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 303
    .local v0, "tempEditText":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private handleAssociationResults(II)V
    .locals 6
    .param p1, "optType"    # I
    .param p2, "result"    # I

    .prologue
    const/4 v5, 0x1

    .line 308
    const-string v2, "SmartCardAssociation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleAssociationResults: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    const/4 v0, 0x0

    .line 312
    .local v0, "errorId":I
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_0

    .line 313
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 314
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    .line 317
    :cond_0
    const/16 v2, 0xb

    if-ne p2, v2, :cond_2

    .line 319
    const-string v2, "SmartCardAssociation"

    const-string v3, " VERIFY_PIN_INITRESOURCEERROR "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->finish()V

    .line 387
    :cond_1
    :goto_0
    return-void

    .line 324
    :cond_2
    packed-switch p2, :pswitch_data_0

    .line 348
    if-ne p1, v5, :cond_3

    .line 349
    const v0, 0x7f05000a

    .line 358
    :goto_1
    :pswitch_0
    if-eqz v0, :cond_4

    .line 360
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mPasswordEntry:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 362
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 364
    const v2, 0x7f05000a

    if-ne v0, v2, :cond_1

    .line 365
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManager;->disconnectService()V

    goto :goto_0

    .line 328
    :pswitch_1
    const v0, 0x7f050006

    .line 329
    goto :goto_1

    .line 332
    :pswitch_2
    const v0, 0x7f050007

    .line 333
    goto :goto_1

    .line 336
    :pswitch_3
    const v0, 0x7f05000c

    .line 337
    goto :goto_1

    .line 340
    :pswitch_4
    const v0, 0x7f050008

    .line 341
    goto :goto_1

    .line 344
    :pswitch_5
    const v0, 0x7f050009

    .line 345
    goto :goto_1

    .line 351
    :cond_3
    const v0, 0x7f05000b

    goto :goto_1

    .line 368
    :cond_4
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, ""

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 370
    .local v1, "toast":Landroid/widget/Toast;
    if-ne p1, v5, :cond_5

    .line 371
    const v2, 0x7f050010

    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setText(I)V

    .line 383
    :goto_2
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 384
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->finish()V

    goto :goto_0

    .line 375
    :cond_5
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->removeAdapterPreference()V

    .line 376
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->removeConnectionPreference()V

    .line 379
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManager;->resetSandboxedProcessServiceConnections()V

    .line 380
    const v2, 0x7f050011

    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setText(I)V

    goto :goto_2

    .line 324
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private handleNext()V
    .locals 10

    .prologue
    const v6, 0x7f050018

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 188
    const v4, 0x7f070014

    invoke-direct {p0, v4}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getEditTextValue(I)Ljava/lang/String;

    move-result-object v3

    .line 189
    .local v3, "pin":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 236
    :goto_0
    return-void

    .line 193
    :cond_0
    iget v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mOperation:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 195
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050016

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mTitle:Ljava/lang/String;

    .line 196
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mMessage:Ljava/lang/String;

    .line 199
    :try_start_0
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mSmartCardPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->registerCallback:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    invoke-virtual {v4, v5, v6}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->registerCard([CLcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;)V

    .line 201
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_1
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->showDialog()V

    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 204
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8, v9, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    .line 206
    .local v2, "newMsg":Landroid/os/Message;
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 207
    iput-boolean v7, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z

    goto :goto_1

    .line 213
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "newMsg":Landroid/os/Message;
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050017

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mTitle:Ljava/lang/String;

    .line 214
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mMessage:Ljava/lang/String;

    .line 217
    :try_start_1
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mSmartCardPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->unRegisterCallback:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    invoke-virtual {v4, v5, v6}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->unRegisterCard([CLcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;)V

    .line 219
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 227
    :goto_2
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->showDialog()V

    goto :goto_0

    .line 220
    :catch_1
    move-exception v1

    .line 221
    .local v1, "e2":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 222
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    .line 224
    .restart local v2    # "newMsg":Landroid/os/Message;
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 225
    iput-boolean v7, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z

    goto :goto_2

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private postMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 279
    const/4 v0, 0x0

    .line 280
    .local v0, "sa":Lcom/sec/smartcard/pinservice/SmartCardAssociation;
    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mCurrent:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_0

    .line 281
    const-string v1, "SmartCardAssociation"

    const-string v2, "postMessage: mCurrent is null "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->finish()V

    .line 299
    :goto_0
    return-void

    .line 285
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mCurrent:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "sa":Lcom/sec/smartcard/pinservice/SmartCardAssociation;
    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;

    .restart local v0    # "sa":Lcom/sec/smartcard/pinservice/SmartCardAssociation;
    if-nez v0, :cond_1

    .line 286
    const-string v1, "SmartCardAssociation"

    const-string v2, "postMessage: activity is null "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->finish()V

    goto :goto_0

    .line 291
    :cond_1
    iget-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_2

    .line 292
    const-string v1, "SmartCardAssociation"

    const-string v2, "postMessage: mHandler is null "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->finish()V

    goto :goto_0

    .line 296
    :cond_2
    iget-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 297
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z

    goto :goto_0
.end method

.method private showDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 404
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mMessage:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 405
    :cond_0
    const-string v0, "SmartCardAssociation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showDialog : mDialog: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mTitle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mMessage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_2

    .line 410
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    .line 412
    :cond_2
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 413
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 414
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 415
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 416
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 418
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 174
    :goto_0
    return-void

    .line 165
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->handleNext()V

    goto :goto_0

    .line 169
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->finish()V

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x7f070004
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 73
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const-string v0, "SmartCardAssociation"

    const-string v1, "onCreate:  "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mCurrent:Ljava/lang/ref/WeakReference;

    .line 78
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mSmartCardPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .line 80
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->setContentView(I)V

    .line 81
    const v0, 0x7f070004

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mCancelButton:Landroid/widget/Button;

    .line 82
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 84
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 85
    const v0, 0x7f070005

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mNextButton:Landroid/widget/Button;

    .line 86
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v0, 0x7f070014

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mPasswordEntry:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 90
    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHeaderText:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHeaderText:Landroid/widget/TextView;

    const v1, 0x7f050019

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 94
    if-eqz p1, :cond_0

    .line 95
    const-string v0, "title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mTitle:Ljava/lang/String;

    .line 96
    const-string v0, "msg"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mMessage:Ljava/lang/String;

    .line 97
    const-string v0, "operation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z

    .line 100
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 391
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 392
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 394
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mDialog:Landroid/app/ProgressDialog;

    .line 398
    :cond_0
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 241
    if-eqz p2, :cond_0

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 244
    :cond_0
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->handleNext()V

    .line 245
    const/4 v0, 0x1

    .line 247
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 104
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 105
    const-string v5, "SmartCardAssociation"

    const-string v6, "onResume:  "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 108
    .local v2, "intent":Landroid/content/Intent;
    if-nez v2, :cond_1

    .line 109
    const-string v5, "SmartCardAssociation"

    const-string v6, "onResume:  intent is null"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 115
    const-string v5, "com.sec.smartcard.pinservice.action.REGISTERSMARTCARD"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 116
    iput v7, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mOperation:I

    .line 121
    :cond_2
    :goto_1
    const-string v5, "smartcard_operation"

    const/4 v6, -0x1

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 122
    .local v4, "optType":I
    if-ne v4, v7, :cond_6

    .line 123
    iput v7, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mOperation:I

    .line 129
    :cond_3
    :goto_2
    const/4 v3, 0x0

    .line 131
    .local v3, "isTransportReady":Z
    :try_start_0
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mSmartCardPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-virtual {v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isDeviceConnectedWithCard()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 136
    :goto_3
    if-nez v3, :cond_4

    .line 154
    :cond_4
    iget-boolean v5, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z

    if-eqz v5, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->showDialog()V

    goto :goto_0

    .line 117
    .end local v3    # "isTransportReady":Z
    .end local v4    # "optType":I
    :cond_5
    const-string v5, "com.sec.smartcard.pinservice.action.UNREGISTERSMARTCARD"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 118
    iput v8, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mOperation:I

    goto :goto_1

    .line 124
    .restart local v4    # "optType":I
    :cond_6
    if-ne v4, v8, :cond_3

    .line 125
    iput v8, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mOperation:I

    goto :goto_2

    .line 132
    .restart local v3    # "isTransportReady":Z
    :catch_0
    move-exception v1

    .line 134
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 178
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 179
    iget-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "title"

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v0, "msg"

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "operation"

    iget-boolean v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mIsOptProgress:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 184
    :cond_0
    return-void
.end method
