.class public Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;
.super Landroid/app/Activity;
.source "CardNotRegisteredErrorDialogActivity.java"


# instance fields
.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;->mDialog:Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;->mDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1040014

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f05000f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;->mDialog:Landroid/app/AlertDialog;

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 32
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;->mDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity$1;-><init>(Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 40
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 50
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;->finish()V

    .line 52
    return-void
.end method
