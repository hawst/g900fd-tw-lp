.class Lcom/sec/smartcard/pinservice/PinDialogHostActivity$1;
.super Ljava/lang/Object;
.source "PinDialogHostActivity.java"

# interfaces
.implements Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/pinservice/PinDialogHostActivity;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/PinDialogHostActivity;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity$1;->this$0:Lcom/sec/smartcard/pinservice/PinDialogHostActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPinEntered([C)V
    .locals 1
    .param p1, "pin"    # [C

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity$1;->this$0:Lcom/sec/smartcard/pinservice/PinDialogHostActivity;

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->finish()V

    .line 71
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity$1;->this$0:Lcom/sec/smartcard/pinservice/PinDialogHostActivity;

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->setPin([C)V

    .line 74
    return-void
.end method
