.class public Lcom/sec/smartcard/pinservice/SmartCardInformation;
.super Landroid/preference/PreferenceActivity;
.source "SmartCardInformation.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private display_info_category()V
    .locals 15

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardInformation;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v10

    .line 90
    .local v10, "root":Landroid/preference/PreferenceScreen;
    const-string v12, "smartcardcuid"

    invoke-virtual {v10, v12}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 91
    .local v3, "cuid":Landroid/preference/Preference;
    const-string v12, "smartcardlabel"

    invoke-virtual {v10, v12}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .line 92
    .local v7, "label":Landroid/preference/Preference;
    const-string v12, "smartcardmanufacture"

    invoke-virtual {v10, v12}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    .line 93
    .local v8, "manufacture":Landroid/preference/Preference;
    const-string v12, "smartcardmodel"

    invoke-virtual {v10, v12}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    .line 94
    .local v9, "model":Landroid/preference/Preference;
    const-string v12, "smartcardserialnum"

    invoke-virtual {v10, v12}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    .line 95
    .local v11, "serialnum":Landroid/preference/Preference;
    const-string v12, "smartcardhwversion"

    invoke-virtual {v10, v12}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 96
    .local v6, "hwversion":Landroid/preference/Preference;
    const-string v12, "smartcardfwversion"

    invoke-virtual {v10, v12}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 97
    .local v5, "fwversion":Landroid/preference/Preference;
    const-string v12, "smartcardcerts"

    invoke-virtual {v10, v12}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 99
    .local v1, "certs":Landroid/preference/Preference;
    const/4 v12, 0x2

    new-array v2, v12, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "CardCUID"

    aput-object v13, v2, v12

    const/4 v12, 0x1

    const-string v13, "CardInfo"

    aput-object v13, v2, v12

    .line 100
    .local v2, "columns":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardInformation;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v12

    const-string v13, "SMRegistration"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v2, v14}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    .line 103
    .local v4, "cv":Landroid/content/ContentValues;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/content/ContentValues;->size()I

    move-result v12

    if-gtz v12, :cond_1

    .line 105
    :cond_0
    const-string v12, "SmartCardInformation"

    const-string v13, "Unable to get cardinfo details for getpin"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :goto_0
    return-void

    .line 110
    :cond_1
    const-string v12, "CardInfo"

    invoke-virtual {v4, v12}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v12

    invoke-static {v12}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->deserializeObject([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;

    .line 113
    .local v0, "cardinfo":Lcom/sec/smartcard/pinservice/SmartCardInfo;
    const-string v12, "smartcardcerts"

    invoke-virtual {v1, v12}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 114
    const v12, 0x7f050035

    invoke-virtual {v1, v12}, Landroid/preference/Preference;->setTitle(I)V

    .line 115
    const-string v12, "View smart card certificates"

    invoke-virtual {v1, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 116
    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 120
    const-string v12, "smartcardcuid"

    invoke-virtual {v3, v12}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 121
    const v12, 0x7f05002e

    invoke-virtual {v3, v12}, Landroid/preference/Preference;->setTitle(I)V

    .line 123
    const-string v12, "CardCUID"

    invoke-virtual {v4, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 124
    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 128
    const-string v12, "smartcardlabel"

    invoke-virtual {v7, v12}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 129
    const v12, 0x7f05002f

    invoke-virtual {v7, v12}, Landroid/preference/Preference;->setTitle(I)V

    .line 131
    iget-object v12, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->label:Ljava/lang/String;

    invoke-virtual {v7, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 132
    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 136
    const-string v12, "smartcardmanufacture"

    invoke-virtual {v8, v12}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 137
    const v12, 0x7f050030

    invoke-virtual {v8, v12}, Landroid/preference/Preference;->setTitle(I)V

    .line 138
    iget-object v12, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->manufacture:Ljava/lang/String;

    invoke-virtual {v8, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 139
    const/4 v12, 0x0

    invoke-virtual {v8, v12}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 143
    const-string v12, "smartcardmodel"

    invoke-virtual {v9, v12}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 144
    const v12, 0x7f050031

    invoke-virtual {v9, v12}, Landroid/preference/Preference;->setTitle(I)V

    .line 145
    iget-object v12, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->model:Ljava/lang/String;

    invoke-virtual {v9, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 146
    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 150
    const-string v12, "smartcardserialnum"

    invoke-virtual {v11, v12}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 151
    const v12, 0x7f050032

    invoke-virtual {v11, v12}, Landroid/preference/Preference;->setTitle(I)V

    .line 152
    iget-object v12, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->serialNumber:Ljava/lang/String;

    invoke-virtual {v11, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 153
    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 157
    const-string v12, "smartcardhwversion"

    invoke-virtual {v6, v12}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 158
    const v12, 0x7f050033

    invoke-virtual {v6, v12}, Landroid/preference/Preference;->setTitle(I)V

    .line 159
    iget-object v12, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->hwVersion:Ljava/lang/String;

    invoke-virtual {v6, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 160
    const/4 v12, 0x0

    invoke-virtual {v6, v12}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 163
    const-string v12, "smartcardfwversion"

    invoke-virtual {v5, v12}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 164
    const v12, 0x7f050034

    invoke-virtual {v5, v12}, Landroid/preference/Preference;->setTitle(I)V

    .line 165
    iget-object v12, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->fwVersion:Ljava/lang/String;

    invoke-virtual {v5, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 166
    const/4 v12, 0x0

    invoke-virtual {v5, v12}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardInformation;->addPreferencesFromResource(I)V

    .line 74
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 173
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, "key":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 177
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "smartcardcerts"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 178
    const-string v2, "com.sec.smartcard.pinservice.action.SMARTCARDVIEWCERTS"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardInformation;->startActivity(Landroid/content/Intent;)V

    .line 182
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 82
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardInformation;->display_info_category()V

    .line 84
    return-void
.end method
