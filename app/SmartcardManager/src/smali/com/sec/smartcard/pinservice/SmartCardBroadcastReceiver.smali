.class public Lcom/sec/smartcard/pinservice/SmartCardBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmartCardBroadcastReceiver.java"


# static fields
.field private static final mVendorPackages:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.baimobile.android.pcsclite.service"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardBroadcastReceiver;->mVendorPackages:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 31
    const-string v7, "SmartCardBroadcastReceiver"

    const-string v8, "SmartCardBroadcastReceiver - onReceive() "

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    if-eqz p2, :cond_0

    .line 33
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v7, "com.sec.enterprise.mdm.sc.action.INVALIDATE_CACHE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 37
    const-string v7, "SmartCardBroadcastReceiver"

    const-string v8, "Broadcast received invalidate cache request "

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const-string v7, "cardId"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "id":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v7

    invoke-virtual {v7, v0, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->handleBroadcastReceiver(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "id":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 41
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    if-eqz v0, :cond_2

    const-string v7, "com.sec.smartcard.pinservice.action.SMARTCARD_PIN_REMOVED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 42
    const-string v7, "SmartCardBroadcastReceiver"

    const-string v8, "Broadcast received for smartcard pin removed "

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 47
    :cond_2
    if-eqz v0, :cond_4

    const-string v7, "com.sec.smartcard.pinservice.action.SMARTCARD_LOCKTYPE_CHANGED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 48
    const-string v7, "SmartCardBroadcastReceiver"

    const-string v8, "Broadcast received for locktype changed "

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    .line 52
    .local v6, "user":I
    const/4 v3, 0x0

    .line 54
    .local v3, "isPersona":Z
    const-string v7, "Type"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "type":Ljava/lang/String;
    if-eqz v3, :cond_3

    const-string v7, "Smartcard"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 63
    :cond_3
    const-string v7, "MyPrefsFile"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 65
    .local v4, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 66
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v7, "lockscreen_type"

    invoke-interface {v1, v7, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 68
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 72
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "isPersona":Z
    .end local v4    # "settings":Landroid/content/SharedPreferences;
    .end local v5    # "type":Ljava/lang/String;
    .end local v6    # "user":I
    :cond_4
    if-eqz v0, :cond_5

    const-string v7, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 94
    :cond_5
    if-eqz v0, :cond_0

    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    const-string v7, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 97
    :cond_6
    const-string v7, "SmartCardBroadcastReceiver"

    const-string v8, "Broadcast received for package removed"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
