.class Lcom/sec/smartcard/pinservice/SmartCardPinDialog$1;
.super Ljava/lang/Object;
.source "SmartCardPinDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$1;->this$0:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$1;->this$0:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mListener:Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;
    invoke-static {v0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->access$100(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$1;->this$0:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mPinView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->access$000(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;->onPinEntered([C)V

    .line 94
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$1;->this$0:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->clearPinView()V
    invoke-static {v0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->access$200(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)V

    .line 96
    return-void
.end method
