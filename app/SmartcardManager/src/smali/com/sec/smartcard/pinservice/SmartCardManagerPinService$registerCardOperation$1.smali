.class Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;
.super Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback$Stub;
.source "SmartCardManagerPinService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;)V
    .locals 0

    .prologue
    .line 2272
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(I[CLandroid/os/Bundle;)V
    .locals 14
    .param p1, "status"    # I
    .param p2, "pin"    # [C
    .param p3, "cardInfo"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2278
    const-string v11, "SmartcardManagerPinService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "registerCardOperation:onComplete: status: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2281
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->mRegisterOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$000()Ljava/util/Queue;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 2282
    const-string v11, "SmartcardManagerPinService"

    const-string v12, "queue element and callback element are not matching"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2284
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-virtual {v11}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    .line 2371
    :goto_0
    return-void

    .line 2288
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 2363
    :cond_1
    :goto_1
    :try_start_0
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->mRegisterOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    invoke-interface {v11, p1}, Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;->onComplete(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2367
    :goto_2
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    const/4 v12, 0x0

    iput-object v12, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->mRegisterOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .line 2368
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V
    invoke-static {v11}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$800(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)V

    goto :goto_0

    .line 2293
    :pswitch_0
    const/4 v9, 0x0

    .line 2295
    .local v9, "ret":Z
    if-eqz p3, :cond_3

    .line 2296
    const-string v11, "cardInfo"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2298
    .local v2, "cuid":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-object/from16 v0, p3

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getCardInfo(Landroid/os/Bundle;)Lcom/sec/smartcard/pinservice/SmartCardInfo;
    invoke-static {v11, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$900(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Landroid/os/Bundle;)Lcom/sec/smartcard/pinservice/SmartCardInfo;

    move-result-object v10

    .line 2301
    .local v10, "smInfo":Lcom/sec/smartcard/pinservice/SmartCardInfo;
    if-eqz v2, :cond_2

    if-nez v10, :cond_4

    .line 2302
    :cond_2
    const-string v11, "SmartcardManagerPinService"

    const-string v12, "sminfo is null"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2303
    const/4 p1, 0x5

    .line 2346
    .end local v2    # "cuid":Ljava/lang/String;
    .end local v10    # "smInfo":Lcom/sec/smartcard/pinservice/SmartCardInfo;
    :cond_3
    :goto_3
    if-eqz v9, :cond_1

    .line 2348
    new-instance v7, Landroid/content/Intent;

    const-string v11, "com.sec.smartcard.pinservice.action.SMARTCARD_REGISTRATION_STATUS"

    invoke-direct {v7, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2350
    .local v7, "intent":Landroid/content/Intent;
    const-string v11, "com.sec.smartcard.pinservice.extra.registration.enabled"

    const/4 v12, 0x1

    invoke-virtual {v7, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2351
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Landroid/content/Context;

    move-result-object v11

    const-string v12, "com.sec.smartcard.pinservice.permission.SMARTCARD_PIN_ACCESS"

    invoke-virtual {v11, v7, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2352
    const-string v11, "SmartcardManagerPinService"

    const-string v12, "broadcast send"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2304
    .end local v7    # "intent":Landroid/content/Intent;
    .restart local v2    # "cuid":Ljava/lang/String;
    .restart local v10    # "smInfo":Lcom/sec/smartcard/pinservice/SmartCardInfo;
    :cond_4
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isCardAlreadyRegistered(Ljava/lang/String;)Z
    invoke-static {v11, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1000(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2306
    const-string v11, "SmartcardManagerPinService"

    const-string v12, "Card already registered"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2307
    const/16 p1, 0xa

    goto :goto_3

    .line 2311
    :cond_5
    invoke-static {v10}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->serializeObject(Ljava/lang/Object;)[B

    move-result-object v1

    .line 2314
    .local v1, "cardInfoBlob":[B
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2315
    .local v3, "cv":Landroid/content/ContentValues;
    const-string v11, "CardCUID"

    invoke-virtual {v3, v11, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2317
    const-string v11, "CardInfo"

    invoke-virtual {v3, v11, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2318
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v11

    const-string v12, "SMRegistration"

    invoke-virtual {v11, v12, v3}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->add(Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v9

    .line 2320
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_4
    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_alias_names:Ljava/util/List;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1100()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v6, v11, :cond_6

    .line 2322
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2323
    .local v4, "cv1":Landroid/content/ContentValues;
    const-string v11, "CardCUID"

    invoke-virtual {v4, v11, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    const-string v12, "CertificateAlias"

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_alias_names:Ljava/util/List;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1100()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v4, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325
    const-string v12, "CertificateKeyUsage"

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_key_usage:Ljava/util/List;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1200()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v4, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2326
    const-string v12, "CardCertificate"

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert:Ljava/util/List;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1300()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [B

    invoke-virtual {v4, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2327
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v11

    const-string v12, "SMCertificates"

    invoke-virtual {v11, v12, v4}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->add(Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v9

    .line 2320
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 2333
    .end local v4    # "cv1":Landroid/content/ContentValues;
    :cond_6
    if-nez v9, :cond_7

    .line 2334
    const-string v11, "SmartcardManagerPinService"

    const-string v12, "Error storing cuid and certificates"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2336
    const/4 p1, 0x5

    goto/16 :goto_3

    .line 2339
    :cond_7
    new-instance v8, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->mpin:[C

    const/4 v12, 0x0

    invoke-direct {v8, v2, v11, v12}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;-><init>(Ljava/lang/String;[CZ)V

    .line 2341
    .local v8, "pinEntry":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    iget-object v11, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v11, v11, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueAddEntry(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z
    invoke-static {v11, v8}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$700(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z

    goto/16 :goto_3

    .line 2364
    .end local v1    # "cardInfoBlob":[B
    .end local v2    # "cuid":Ljava/lang/String;
    .end local v3    # "cv":Landroid/content/ContentValues;
    .end local v6    # "i":I
    .end local v8    # "pinEntry":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    .end local v9    # "ret":Z
    .end local v10    # "smInfo":Lcom/sec/smartcard/pinservice/SmartCardInfo;
    :catch_0
    move-exception v5

    .line 2365
    .local v5, "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2

    .line 2288
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
