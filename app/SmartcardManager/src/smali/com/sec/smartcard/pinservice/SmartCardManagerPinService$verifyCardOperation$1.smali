.class Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;
.super Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback$Stub;
.source "SmartCardManagerPinService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;)V
    .locals 0

    .prologue
    .line 2604
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(I)V
    .locals 5
    .param p1, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2610
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1500()Ljava/util/Queue;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2611
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "queue element and callback element are not matching"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2613
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-virtual {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteVerifyOperation()Z

    .line 2651
    :goto_0
    return-void

    .line 2617
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 2641
    :cond_1
    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;

    if-eqz v3, :cond_2

    .line 2642
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;

    invoke-interface {v3, p1}, Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;->onComplete(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2648
    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteVerifyOperation()V
    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1800(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)V

    goto :goto_0

    .line 2620
    :pswitch_0
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->mId:Ljava/lang/String;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueGetEntry(Ljava/lang/String;)Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    invoke-static {v3, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1600(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    move-result-object v1

    .line 2621
    .local v1, "entry":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    if-eqz v1, :cond_1

    .line 2623
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueUpdateAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z
    invoke-static {v3, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1700(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z

    move-result v2

    .line 2624
    .local v2, "ret":Z
    if-nez v2, :cond_1

    .line 2625
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "Error occured storing asscociation verification info"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2627
    const/4 p1, 0x5

    goto :goto_1

    .line 2644
    .end local v1    # "entry":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    .end local v2    # "ret":Z
    :catch_0
    move-exception v0

    .line 2646
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 2617
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
