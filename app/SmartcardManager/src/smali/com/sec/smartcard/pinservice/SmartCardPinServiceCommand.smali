.class public Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
.super Ljava/lang/Object;
.source "SmartCardPinServiceCommand.java"


# instance fields
.field public appUid:I

.field public getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

.field public id:Ljava/lang/String;

.field public infoCb:Lcom/sec/smartcard/pinservice/ISmartCardInfoCallback;

.field public operation:I

.field public pin:[C

.field public registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

.field public state:I

.field public verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "operation"    # I

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    .line 40
    iput p1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->operation:I

    .line 41
    iput-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    .line 42
    iput-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    .line 44
    iput-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    .line 45
    iput-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->infoCb:Lcom/sec/smartcard/pinservice/ISmartCardInfoCallback;

    .line 46
    iput-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    .line 47
    iput-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;

    .line 48
    return-void
.end method

.method private static getOperation(I)Ljava/lang/String;
    .locals 1
    .param p0, "operation"    # I

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 87
    .local v0, "str":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 114
    :goto_0
    return-object v0

    .line 89
    :pswitch_0
    const-string v0, "OPT_REGISTERCARD"

    .line 90
    goto :goto_0

    .line 93
    :pswitch_1
    const-string v0, "OPT_UNREGISTERCARD"

    .line 94
    goto :goto_0

    .line 97
    :pswitch_2
    const-string v0, "OPT_GETPIN"

    .line 98
    goto :goto_0

    .line 101
    :pswitch_3
    const-string v0, "OPT_VERIFYCARD"

    .line 102
    goto :goto_0

    .line 105
    :pswitch_4
    const-string v0, "OPT_LOGINATTEMPT"

    .line 106
    goto :goto_0

    .line 109
    :pswitch_5
    const-string v0, "OPT_ISDEVICECONNECTED"

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static getState(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 57
    .local v0, "str":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 80
    :goto_0
    return-object v0

    .line 59
    :pswitch_0
    const-string v0, "STATE_IDLE"

    .line 60
    goto :goto_0

    .line 63
    :pswitch_1
    const-string v0, "STATE_UI_PROGRESS"

    .line 64
    goto :goto_0

    .line 67
    :pswitch_2
    const-string v0, "STATE_UI_COMPLETE"

    .line 68
    goto :goto_0

    .line 71
    :pswitch_3
    const-string v0, "STATE_CARD_PROGRESS"

    .line 72
    goto :goto_0

    .line 75
    :pswitch_4
    const-string v0, "STATE_CARD_COMPLETE"

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static print(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 3
    .param p0, "e"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 51
    const-string v0, "SmartcardPinServiceCommand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "appUid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "operation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->operation:I

    invoke-static {v2}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getOperation(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    invoke-static {v2}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getState(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return-void
.end method
