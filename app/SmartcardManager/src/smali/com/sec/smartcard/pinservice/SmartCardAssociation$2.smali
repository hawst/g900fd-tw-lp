.class Lcom/sec/smartcard/pinservice/SmartCardAssociation$2;
.super Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback$Stub;
.source "SmartCardAssociation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartCardAssociation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/pinservice/SmartCardAssociation;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardAssociation;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation$2;->this$0:Lcom/sec/smartcard/pinservice/SmartCardAssociation;

    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(I)V
    .locals 4
    .param p1, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 255
    const-string v1, "SmartCardAssociation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ISmartCardRegisterCallback:onComplete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation$2;->this$0:Lcom/sec/smartcard/pinservice/SmartCardAssociation;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardAssociation;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->access$100(Lcom/sec/smartcard/pinservice/SmartCardAssociation;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 258
    .local v0, "newMsg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation$2;->this$0:Lcom/sec/smartcard/pinservice/SmartCardAssociation;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardAssociation;->postMessage(Landroid/os/Message;)V
    invoke-static {v1, v0}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->access$200(Lcom/sec/smartcard/pinservice/SmartCardAssociation;Landroid/os/Message;)V

    .line 260
    return-void
.end method
