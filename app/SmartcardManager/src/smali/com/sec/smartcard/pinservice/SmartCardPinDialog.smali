.class public Lcom/sec/smartcard/pinservice/SmartCardPinDialog;
.super Ljava/lang/Object;
.source "SmartCardPinDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;,
        Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mListener:Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;

.field private mPinView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mListener:Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mPinView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mListener:Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->clearPinView()V

    return-void
.end method

.method private clearPinView()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mPinView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearComposingText()V

    .line 168
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mPinView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 170
    return-void
.end method


# virtual methods
.method public removeDialog()V
    .locals 2

    .prologue
    .line 119
    const-string v0, "SmartCardPinDialog"

    const-string v1, "removeDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const-string v0, "SmartCardPinDialog"

    const-string v1, "dismissDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 123
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->setPin([C)V

    .line 124
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->clearPinView()V

    .line 126
    :cond_0
    return-void
.end method

.method public showDialog()V
    .locals 6

    .prologue
    .line 69
    const-string v3, "SmartCardPinDialog"

    const-string v4, "showDialog()"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 71
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v3, 0x7f030002

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 72
    .local v2, "textEntryView":Landroid/view/View;
    const v3, 0x7f070007

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mPinView:Landroid/widget/TextView;

    .line 73
    const v3, 0x7f070006

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 74
    .local v1, "statusTextView":Landroid/widget/TextView;
    const v3, 0x7f050005

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 75
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;

    invoke-direct {v3, p0, v2}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;-><init>(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;Landroid/view/View;)V

    .line 76
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1080027

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f050004

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$4;

    invoke-direct {v4, p0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$4;-><init>(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    new-instance v5, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$3;

    invoke-direct {v5, p0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$3;-><init>(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f050012

    new-instance v5, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$2;

    invoke-direct {v5, p0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$2;-><init>(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    new-instance v5, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$1;

    invoke-direct {v5, p0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$1;-><init>(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mDialog:Landroid/app/Dialog;

    .line 99
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mDialog:Landroid/app/Dialog;

    new-instance v4, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$5;

    invoke-direct {v4, p0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$5;-><init>(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)V

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 114
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    .line 116
    return-void
.end method
