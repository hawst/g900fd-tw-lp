.class Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation$1;
.super Lcom/sec/smartcard/adapter/ISmartcardAdapterInfoCallback$Stub;
.source "SmartCardManagerPinService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;)V
    .locals 0

    .prologue
    .line 2691
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterInfoCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "cardInfo"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2697
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;

    iget-object v0, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;->mGetCardInfoOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$000()Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2698
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "queue element and callback element are not matching"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2700
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;

    iget-object v0, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    .line 2709
    :cond_0
    :goto_0
    return-void

    .line 2704
    :cond_1
    if-nez p1, :cond_0

    .line 2705
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "No card info received"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
