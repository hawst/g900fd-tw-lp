.class public Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;
.super Ljava/lang/Object;
.source "SmartCardPinDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartCardPinDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DialPadListener"
.end annotation


# instance fields
.field btnMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;


# direct methods
.method public constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;Landroid/view/View;)V
    .locals 11
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const v10, 0x7f07000d

    const v9, 0x7f07000c

    const v8, 0x7f07000b

    const v7, 0x7f07000a

    const v6, 0x7f070009

    .line 133
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->this$0:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    .line 135
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    .line 139
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    const v3, 0x7f070012

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const v3, 0x7f070012

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    const v3, 0x7f07000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const v3, 0x7f07000e

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    const v3, 0x7f07000f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const v3, 0x7f07000f

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    const v3, 0x7f070010

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const v3, 0x7f070010

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    const v3, 0x7f070011

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const v3, 0x7f070011

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 151
    .local v1, "buttons":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/widget/Button;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 152
    .local v0, "btn":Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 154
    .end local v0    # "btn":Landroid/widget/Button;
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 158
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->btnMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 159
    .local v0, "button":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 160
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog$DialPadListener;->this$0:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->mPinView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->access$000(Lcom/sec/smartcard/pinservice/SmartCardPinDialog;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 162
    :cond_0
    return-void
.end method
