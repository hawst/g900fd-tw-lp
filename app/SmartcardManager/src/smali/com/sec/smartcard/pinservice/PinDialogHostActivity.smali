.class public Lcom/sec/smartcard/pinservice/PinDialogHostActivity;
.super Landroid/app/Activity;
.source "PinDialogHostActivity.java"


# instance fields
.field private mDialog:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cancelNotification()V

    .line 57
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 64
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    new-instance v1, Lcom/sec/smartcard/pinservice/PinDialogHostActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/smartcard/pinservice/PinDialogHostActivity$1;-><init>(Lcom/sec/smartcard/pinservice/PinDialogHostActivity;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;-><init>(Landroid/content/Context;Lcom/sec/smartcard/pinservice/SmartCardPinDialog$PinListener;)V

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    .line 76
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->showDialog()V

    .line 79
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 92
    const-string v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    .line 94
    .local v0, "bKeyGuardLocked":Z
    if-nez v0, :cond_0

    .line 95
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardPinDialog;->removeDialog()V

    .line 96
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/SmartCardPinDialog;

    .line 98
    :cond_0
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 105
    const-string v0, "PinDialogHostActivity"

    const-string v1, "onUserLeaveHint"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;->finish()V

    .line 107
    return-void
.end method
