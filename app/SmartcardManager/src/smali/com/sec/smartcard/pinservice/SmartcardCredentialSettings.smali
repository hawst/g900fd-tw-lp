.class public Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;
.super Landroid/preference/PreferenceActivity;
.source "SmartcardCredentialSettings.java"


# instance fields
.field private mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

.field private mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

.field private mDialog:Landroid/app/AlertDialog;

.field private mIsAdapterSelectionReceiverEnabled:Z

.field private mIsConnectionSelectionReceiverEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mDialog:Landroid/app/AlertDialog;

    .line 65
    iput-boolean v1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z

    .line 66
    iput-boolean v1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z

    .line 331
    new-instance v0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;-><init>(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)V

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

    .line 363
    new-instance v0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;-><init>(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)V

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method private showDialog_error_message(I)V
    .locals 3
    .param p1, "opt"    # I

    .prologue
    const v2, 0x1040014

    const v1, 0x1010355

    .line 296
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 302
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 303
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f05002a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mDialog:Landroid/app/AlertDialog;

    .line 317
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 319
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$1;

    invoke-direct {v1, p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$1;-><init>(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 326
    return-void

    .line 309
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 310
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f05002b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mDialog:Landroid/app/AlertDialog;

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const-string v0, "SmartcardCredentialSettings_PinService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->addPreferencesFromResource(I)V

    .line 82
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 198
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 200
    iget-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 202
    iput-boolean v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z

    .line 205
    :cond_0
    iget-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z

    if-eqz v0, :cond_1

    .line 206
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 207
    iput-boolean v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z

    .line 210
    :cond_1
    const-string v0, "SmartcardCredentialSettings_PinService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 193
    const-string v0, "SmartcardCredentialSettings_PinService"

    const-string v1, "onPause called"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 10
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v9, 0x1

    .line 229
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 230
    .local v2, "key":Ljava/lang/String;
    const-string v6, "SmartcardCredentialSettings_PinService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "key "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "MyPrefsFile"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 234
    .local v4, "settings":Landroid/content/SharedPreferences;
    const-string v6, "lockscreen_type"

    const-string v7, "Other"

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 235
    .local v5, "type":Ljava/lang/String;
    const-string v6, "SmartcardCredentialSettings_PinService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "lock_key type = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 238
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "smartcardregister"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 239
    const-string v6, "com.sec.smartcard.pinservice.action.REGISTERSMARTCARD"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    invoke-virtual {p0, v1}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->startActivity(Landroid/content/Intent;)V

    .line 241
    const-string v6, "SmartcardCredentialSettings_PinService"

    const-string v7, "activity started com.sec.smartcard.pinservice.action.REGISTERSMARTCARD"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    :cond_0
    :goto_0
    return v9

    .line 242
    :cond_1
    const-string v6, "smartcardunregister"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 244
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "device_policy"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/admin/DevicePolicyManager;

    .line 248
    .local v3, "mDevicePolicyManager":Landroid/app/admin/DevicePolicyManager;
    const-string v6, "Other"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 250
    const-string v6, "com.sec.smartcard.pinservice.action.UNREGISTERSMARTCARD"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    invoke-virtual {p0, v1}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 252
    :cond_2
    const-string v6, "Smartcard"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 253
    invoke-direct {p0, v9}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->showDialog_error_message(I)V

    goto :goto_0

    .line 256
    :cond_3
    const/4 v6, 0x2

    invoke-direct {p0, v6}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->showDialog_error_message(I)V

    goto :goto_0

    .line 258
    .end local v3    # "mDevicePolicyManager":Landroid/app/admin/DevicePolicyManager;
    :cond_4
    const-string v6, "smartcardstatus"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 260
    const-string v6, "com.sec.smartcard.pinservice.action.SMARTCARDINFORMATION"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    invoke-virtual {p0, v1}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 263
    :cond_5
    const-string v6, "smartcard_select_adapter"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 265
    iget-boolean v6, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z

    if-nez v6, :cond_6

    .line 266
    new-instance v0, Landroid/content/IntentFilter;

    const-string v6, "com.sec.smartcard.manager.ADAPTER_SELECTED"

    invoke-direct {v0, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 267
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v6, v7, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 268
    iput-boolean v9, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z

    .line 270
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_6
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getAdapterPreference()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 272
    const-string v6, "SmartcardCredentialSettings_PinService"

    const-string v7, "adapter selection preference ... already selected"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_7
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/smartcard/manager/SmartcardManager;->disconnectService()V

    .line 275
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v6

    sget v7, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DISPLAY_ADAPTER_LIST:I

    invoke-virtual {v6, v7}, Lcom/sec/smartcard/manager/SmartcardManager;->connectToService(I)Z

    goto/16 :goto_0

    .line 277
    :cond_8
    const-string v6, "smartcard_select_connection"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 279
    iget-boolean v6, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z

    if-nez v6, :cond_9

    .line 280
    new-instance v0, Landroid/content/IntentFilter;

    const-string v6, "com.sec.smartcard.manager.CONNECTION_SELECTED"

    invoke-direct {v0, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 281
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v6, v7, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 282
    iput-boolean v9, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z

    .line 284
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_9
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 286
    const-string v6, "SmartcardCredentialSettings_PinService"

    const-string v7, "connection selection preference ... already selected"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :cond_a
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/smartcard/manager/SmartcardManager;->disconnectService()V

    .line 289
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v6

    sget v7, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DISPLAY_CONNECTION_LIST:I

    invoke-virtual {v6, v7}, Lcom/sec/smartcard/manager/SmartcardManager;->connectToService(I)Z

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 88
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 89
    const-string v8, "SmartcardCredentialSettings_PinService"

    const-string v9, "onResume"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 92
    .local v3, "root":Landroid/preference/PreferenceScreen;
    const-string v8, "smartcard_association"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/PreferenceCategory;

    .line 94
    .local v4, "smartcardCategory":Landroid/preference/PreferenceCategory;
    const-string v8, "smartcard_information"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceCategory;

    .line 96
    .local v5, "smartcardInfo":Landroid/preference/PreferenceCategory;
    const-string v8, "smartcardregister"

    invoke-virtual {v4, v8}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 97
    .local v1, "regCard":Landroid/preference/Preference;
    const-string v8, "smartcardunregister"

    invoke-virtual {v4, v8}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 99
    .local v6, "unregCard":Landroid/preference/Preference;
    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v4, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 102
    :cond_0
    if-eqz v6, :cond_1

    .line 103
    invoke-virtual {v4, v6}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 105
    :cond_1
    const-string v8, "smartcardstatus"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 106
    const-string v8, "smartcardstatus"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getAdapterPreference()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    .line 125
    :cond_2
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getConnectionPreference()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_3

    .line 139
    :cond_3
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isCardRegistered()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 141
    new-instance v7, Landroid/preference/Preference;

    invoke-direct {v7, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 149
    .local v7, "unregisterCard":Landroid/preference/Preference;
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 150
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 152
    const-string v8, "smartcardstatus"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 153
    const-string v8, "smartcardstatus"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 155
    const-string v8, "smartcardunregister"

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 156
    const v8, 0x7f050023

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setTitle(I)V

    .line 157
    const v8, 0x7f050022

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setSummary(I)V

    .line 158
    invoke-virtual {v4, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 159
    const-string v8, "smartcardstatus"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    const v9, 0x7f050027

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setSummary(I)V

    .line 188
    .end local v7    # "unregisterCard":Landroid/preference/Preference;
    :goto_0
    return-void

    .line 162
    :cond_4
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 170
    .local v2, "registerCard":Landroid/preference/Preference;
    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 171
    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 173
    const-string v8, "smartcardstatus"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 174
    const-string v8, "smartcardstatus"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 175
    const-string v8, "smartcardregister"

    invoke-virtual {v2, v8}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 176
    const v8, 0x7f050025

    invoke-virtual {v2, v8}, Landroid/preference/Preference;->setTitle(I)V

    .line 177
    const v8, 0x7f050026

    invoke-virtual {v2, v8}, Landroid/preference/Preference;->setSummary(I)V

    .line 178
    invoke-virtual {v4, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 179
    const-string v8, "smartcardstatus"

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    const v9, 0x7f050021

    invoke-virtual {v8, v9}, Landroid/preference/Preference;->setSummary(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 182
    .end local v2    # "registerCard":Landroid/preference/Preference;
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 224
    const-string v0, "SmartcardCredentialSettings_PinService"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 215
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 216
    const-string v0, "SmartcardCredentialSettings_PinService"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    return-void
.end method
