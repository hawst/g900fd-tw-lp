.class public Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;
.super Landroid/app/Activity;
.source "PinVerifierStatusDialogHostActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;
    }
.end annotation


# instance fields
.field private mDialog:Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 82
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 57
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 60
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "status"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 61
    .local v2, "statusMsgId":I
    const-string v3, "showPinDialog"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 63
    .local v1, "showPinDialog":I
    new-instance v3, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;

    invoke-direct {v3, p0, p0, v2, v1}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;-><init>(Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;Landroid/content/Context;II)V

    iput-object v3, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;

    .line 66
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;

    # invokes: Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->showDialog()V
    invoke-static {v3}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->access$000(Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cancelNotification()V

    .line 69
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 74
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;->mDialog:Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->removeDialog()V

    .line 75
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 80
    return-void
.end method
