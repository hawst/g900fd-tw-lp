.class Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;
.super Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback$Stub;
.source "SmartCardManagerPinService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;)V
    .locals 0

    .prologue
    .line 2508
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(I[CLandroid/os/Bundle;)V
    .locals 6
    .param p1, "status"    # I
    .param p2, "pin"    # [C
    .param p3, "cardInfo"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2514
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->mRegisterOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$000()Ljava/util/Queue;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2515
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "queue element and callback element are not matching"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2517
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-virtual {v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    .line 2567
    :goto_0
    return-void

    .line 2521
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 2560
    :goto_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->mRegisterOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    invoke-interface {v4, p1}, Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;->onComplete(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2564
    :goto_2
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V
    invoke-static {v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$800(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)V

    goto :goto_0

    .line 2525
    :pswitch_0
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v5, v5, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->mId:Ljava/lang/String;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueRemoveEntry(Ljava/lang/String;)Z
    invoke-static {v4, v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)Z

    .line 2528
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2529
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v4, "CardCUID"

    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v5, v5, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->mId:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2530
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v4

    const-string v5, "SMRegistration"

    invoke-virtual {v4, v5, v0}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->remove(Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v3

    .line 2533
    .local v3, "ret":Z
    if-nez v3, :cond_1

    .line 2534
    const/4 p1, 0x5

    .line 2535
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "Error occurred while deleting registration info"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2540
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.smartcard.pinservice.action.SMARTCARD_REGISTRATION_STATUS"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2542
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "com.sec.smartcard.pinservice.extra.registration.enabled"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2543
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.smartcard.pinservice.permission.SMARTCARD_PIN_ACCESS"

    invoke-virtual {v4, v2, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2544
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "broadcast send"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2547
    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_alias_names:Ljava/util/List;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1100()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2548
    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_key_usage:Ljava/util/List;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1200()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2549
    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert:Ljava/util/List;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$1300()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    goto :goto_1

    .line 2561
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "ret":Z
    :catch_0
    move-exception v1

    .line 2562
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 2521
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
