.class public Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
.super Lcom/sec/smartcard/pinservice/ISmartCardPinService$Stub;
.source "SmartCardManagerPinService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;,
        Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;,
        Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;,
        Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;,
        Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;,
        Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;
    }
.end annotation


# static fields
.field private static cert:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private static cert_alias_names:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static cert_key_usage:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mPinCacheInterval:I

.field private static mPinQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/sec/smartcard/pinservice/SmartCardManagerPin;",
            ">;"
        }
    .end annotation
.end field

.field private static mPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

.field private static mSvcCommandQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static mVerifyCommandQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLastError:I

.field private mpinDialogDisplayCheckNeed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x5

    sput v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinCacheInterval:I

    .line 120
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .line 121
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    .line 122
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    .line 123
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_alias_names:Ljava/util/List;

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_key_usage:Ljava/util/List;

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/ISmartCardPinService$Stub;-><init>()V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mpinDialogDisplayCheckNeed:Z

    .line 385
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/ISmartCardPinService$Stub;-><init>()V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mpinDialogDisplayCheckNeed:Z

    .line 388
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    .line 389
    return-void
.end method

.method static synthetic access$000()Ljava/util/Queue;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mpinDialogDisplayCheckNeed:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isCardAlreadyRegistered(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100()Ljava/util/List;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_alias_names:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1200()Ljava/util/List;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_key_usage:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1300()Ljava/util/List;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueRemoveEntry(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500()Ljava/util/Queue;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueGetEntry(Ljava/lang/String;)Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueUpdateAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteVerifyOperation()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isHomeActivityVisible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isCallingApplicationVisibile(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getStatusMessageId(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->showNotifcation(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueAddEntry(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Landroid/os/Bundle;)Lcom/sec/smartcard/pinservice/SmartCardInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getCardInfo(Landroid/os/Bundle;)Lcom/sec/smartcard/pinservice/SmartCardInfo;

    move-result-object v0

    return-object v0
.end method

.method private addAndExecuteOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)Z
    .locals 5
    .param p1, "opt"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 639
    const/4 v1, 0x0

    .line 641
    .local v1, "ret":Z
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "addAndExecuteOperation(): "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    if-eqz p1, :cond_0

    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    .line 663
    .end local v1    # "ret":Z
    .local v2, "ret":I
    :goto_0
    return v2

    .line 648
    .end local v2    # "ret":I
    .restart local v1    # "ret":Z
    :cond_1
    :try_start_0
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v3, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 649
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "New request is queued"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 658
    iget v3, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    invoke-direct {p0, v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->connectToService(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 659
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    move-result v1

    :goto_1
    move v2, v1

    .line 663
    .restart local v2    # "ret":I
    goto :goto_0

    .line 650
    .end local v2    # "ret":I
    :catch_0
    move-exception v0

    .line 651
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v2, v1

    .line 652
    .restart local v2    # "ret":I
    goto :goto_0

    .line 661
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "ret":I
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private addAndExecuteVerifyOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)Z
    .locals 5
    .param p1, "opt"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 668
    const/4 v1, 0x0

    .line 670
    .local v1, "ret":Z
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "addAndExecuteVerifyOperation(): "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    if-eqz p1, :cond_0

    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    .line 692
    .end local v1    # "ret":Z
    .local v2, "ret":I
    :goto_0
    return v2

    .line 677
    .end local v2    # "ret":I
    .restart local v1    # "ret":Z
    :cond_1
    :try_start_0
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    invoke-interface {v3, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 678
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "mVerifyCommandQueue New request is queued"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 687
    iget v3, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    invoke-direct {p0, v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->connectToService(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 688
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteVerifyOperation()Z

    move-result v1

    :goto_1
    move v2, v1

    .line 692
    .restart local v2    # "ret":I
    goto :goto_0

    .line 679
    .end local v2    # "ret":I
    :catch_0
    move-exception v0

    .line 680
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v2, v1

    .line 681
    .restart local v2    # "ret":I
    goto :goto_0

    .line 690
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "ret":I
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private anyCardRegistered()Z
    .locals 6

    .prologue
    .line 2425
    const/4 v2, 0x0

    .line 2427
    .local v2, "ret":Z
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "CardCUID"

    aput-object v4, v0, v3

    .line 2428
    .local v0, "coulmns":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v3

    const-string v4, "SMRegistration"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v0, v5}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    .line 2431
    .local v1, "cv":Landroid/content/ContentValues;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/ContentValues;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 2433
    const/4 v2, 0x1

    .line 2436
    :cond_0
    return v2
.end method

.method private static bytesToHex([B)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B

    .prologue
    .line 1821
    const/16 v4, 0x10

    new-array v0, v4, [C

    fill-array-data v0, :array_0

    .line 1823
    .local v0, "hexArray":[C
    array-length v4, p0

    mul-int/lit8 v4, v4, 0x2

    new-array v1, v4, [C

    .line 1825
    .local v1, "hexChars":[C
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_0

    .line 1826
    aget-byte v4, p0, v2

    and-int/lit16 v3, v4, 0xff

    .line 1827
    .local v3, "v":I
    mul-int/lit8 v4, v2, 0x2

    ushr-int/lit8 v5, v3, 0x4

    aget-char v5, v0, v5

    aput-char v5, v1, v4

    .line 1828
    mul-int/lit8 v4, v2, 0x2

    add-int/lit8 v4, v4, 0x1

    and-int/lit8 v5, v3, 0xf

    aget-char v5, v0, v5

    aput-char v5, v1, v4

    .line 1825
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1830
    .end local v3    # "v":I
    :cond_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>([C)V

    return-object v4

    .line 1821
    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method private cancelCacheAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)V
    .locals 6
    .param p1, "entry"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .prologue
    .line 332
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "cancelCacheAlarm"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 335
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getIntent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 336
    .local v1, "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 337
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    const/high16 v5, 0x20000000

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 339
    .local v2, "pi":Landroid/app/PendingIntent;
    if-eqz v2, :cond_0

    .line 340
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "cancelCacheAlarm: Found Pending alarm and canceling it "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 344
    :cond_0
    return-void
.end method

.method private connectToService(I)Z
    .locals 4
    .param p1, "uid"    # I

    .prologue
    .line 696
    const/4 v0, 0x1

    .line 700
    .local v0, "ret":Z
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v1

    .line 701
    .local v1, "sm":Lcom/sec/smartcard/manager/SmartcardManager;
    sget v2, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->DEFAULT_ADAPTER_SELECTION_MODE:I

    sget v3, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->HANDLE_SMARTCARD_PREF_SELECTION:I

    or-int/2addr v2, v3

    sget v3, Lcom/sec/smartcard/manager/SandboxedProcessConnectionSettings;->AUTO_CONNECTION_MODE:I

    or-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/smartcard/manager/SmartcardManager;->connectToService(I)Z

    move-result v2

    return v2
.end method

.method private getActiveCardId()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2397
    const/4 v2, 0x0

    .line 2398
    .local v2, "id":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "CardCUID"

    aput-object v4, v0, v3

    .line 2399
    .local v0, "coulmns":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v3

    const-string v4, "SMRegistration"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v0, v5}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    .line 2402
    .local v1, "cv":Landroid/content/ContentValues;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/ContentValues;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 2404
    const-string v3, "CardCUID"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2406
    :cond_0
    return-object v2
.end method

.method private getAllAliasNames(J)V
    .locals 17
    .param p1, "sessionHandle"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;
        }
    .end annotation

    .prologue
    .line 2052
    const/4 v2, 0x0

    .line 2054
    .local v2, "alias":Ljava/lang/String;
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "getAllAliasNames(): "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2056
    const-wide/16 v4, -0x1

    cmp-long v3, p1, v4

    if-nez v3, :cond_0

    .line 2060
    :cond_0
    const-wide/16 v12, 0x1

    .line 2062
    .local v12, "keyClass":J
    const/4 v3, 0x1

    :try_start_0
    new-array v9, v3, [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 2063
    .local v9, "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    const/4 v3, 0x0

    new-instance v4, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v6, 0x0

    invoke-direct {v4, v6, v7, v12, v13}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JJ)V

    aput-object v4, v9, v3

    .line 2065
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1, v9}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsInit(J[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V

    .line 2067
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-wide/16 v4, 0x1

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjects(JJ)[J

    move-result-object v15

    .line 2068
    .local v15, "pCertObject":[J
    if-eqz v15, :cond_1

    array-length v3, v15

    if-nez v3, :cond_3

    .line 2069
    :cond_1
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 2070
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Unable to find certificate object"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2094
    .end local v9    # "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v15    # "pCertObject":[J
    :catch_0
    move-exception v10

    .line 2095
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 2096
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 2097
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v3

    .line 2087
    .end local v10    # "e":Ljava/lang/Exception;
    .local v8, "certAttrib":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .restart local v9    # "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .local v11, "label":[B
    .restart local v15    # "pCertObject":[J
    :cond_2
    const/4 v3, 0x0

    :try_start_1
    aget-object v3, v8, v3

    iget-object v3, v3, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v3, [C

    move-object v0, v3

    check-cast v0, [C

    move-object v14, v0

    .line 2089
    .local v14, "lbl":[C
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_alias_names:Ljava/util/List;

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v14}, Ljava/lang/String;-><init>([C)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2090
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-wide/16 v4, 0x1

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjects(JJ)[J

    move-result-object v15

    .line 2073
    .end local v8    # "certAttrib":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v11    # "label":[B
    .end local v14    # "lbl":[C
    :cond_3
    array-length v3, v15

    if-lez v3, :cond_5

    .line 2075
    const/4 v11, 0x0

    .line 2076
    .restart local v11    # "label":[B
    const/4 v3, 0x1

    new-array v8, v3, [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 2077
    .restart local v8    # "certAttrib":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    const/4 v3, 0x0

    new-instance v4, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v6, 0x3

    invoke-direct {v4, v6, v7, v11}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JLjava/lang/Object;)V

    aput-object v4, v8, v3

    .line 2078
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v4, 0x0

    aget-wide v6, v15, v4

    move-wide/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetAttributeValue(JJ[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V

    .line 2081
    if-eqz v8, :cond_4

    const/4 v3, 0x0

    aget-object v3, v8, v3

    iget-object v3, v3, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    if-nez v3, :cond_2

    .line 2082
    :cond_4
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 2083
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    const-string v4, "Unable to read certificate object"

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v3

    .line 2093
    .end local v8    # "certAttrib":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v11    # "label":[B
    :cond_5
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsFinal(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2100
    return-void
.end method

.method private getAllKeyUsage()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 2162
    const-string v9, "SmartcardManagerPinService"

    const-string v10, "getKeyUsage"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2163
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_0
    sget-object v9, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v6, v9, :cond_c

    .line 2165
    const/4 v8, 0x0

    .line 2166
    .local v8, "pubcert":Ljava/security/cert/X509Certificate;
    const-string v7, ""

    .line 2169
    .local v7, "key_usage_str":Ljava/lang/String;
    :try_start_0
    const-string v9, "X.509"

    invoke-static {v9}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v2

    .line 2170
    .local v2, "certFactory":Ljava/security/cert/CertificateFactory;
    new-instance v5, Ljava/io/ByteArrayInputStream;

    sget-object v9, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [B

    invoke-direct {v5, v9}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 2171
    .local v5, "in":Ljava/io/InputStream;
    invoke-virtual {v2, v5}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v8, v0
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2177
    .end local v2    # "certFactory":Ljava/security/cert/CertificateFactory;
    .end local v5    # "in":Ljava/io/InputStream;
    :goto_1
    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getKeyUsage()[Z

    move-result-object v1

    .line 2189
    .local v1, "b":[Z
    if-eqz v1, :cond_b

    .line 2190
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    const/16 v9, 0x9

    if-ge v4, v9, :cond_a

    .line 2192
    aget-boolean v9, v1, v4

    if-ne v9, v11, :cond_9

    .line 2194
    const-string v9, ""

    if-eq v7, v9, :cond_0

    .line 2195
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2196
    :cond_0
    if-nez v4, :cond_1

    .line 2197
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "digitalSignature"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2198
    :cond_1
    if-ne v4, v11, :cond_2

    .line 2199
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "nonRepudiation"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2200
    :cond_2
    const/4 v9, 0x2

    if-ne v4, v9, :cond_3

    .line 2201
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "keyEncipherment"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2202
    :cond_3
    const/4 v9, 0x3

    if-ne v4, v9, :cond_4

    .line 2203
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "dataEncipherment"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2204
    :cond_4
    const/4 v9, 0x4

    if-ne v4, v9, :cond_5

    .line 2205
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "keyAgreement"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2206
    :cond_5
    const/4 v9, 0x5

    if-ne v4, v9, :cond_6

    .line 2207
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "keyCertSign"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2208
    :cond_6
    const/4 v9, 0x6

    if-ne v4, v9, :cond_7

    .line 2209
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "cRLSign"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2210
    :cond_7
    const/4 v9, 0x7

    if-ne v4, v9, :cond_8

    .line 2211
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "encipherOnly"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2212
    :cond_8
    const/16 v9, 0x8

    if-ne v4, v9, :cond_9

    .line 2213
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "decipherOnly"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2190
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 2172
    .end local v1    # "b":[Z
    .end local v4    # "i":I
    :catch_0
    move-exception v3

    .line 2174
    .local v3, "e":Ljava/security/cert/CertificateException;
    invoke-virtual {v3}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto/16 :goto_1

    .line 2217
    .end local v3    # "e":Ljava/security/cert/CertificateException;
    .restart local v1    # "b":[Z
    .restart local v4    # "i":I
    :cond_a
    sget-object v9, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_key_usage:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2163
    .end local v4    # "i":I
    :cond_b
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 2222
    .end local v1    # "b":[Z
    .end local v7    # "key_usage_str":Ljava/lang/String;
    .end local v8    # "pubcert":Ljava/security/cert/X509Certificate;
    :cond_c
    return-void
.end method

.method private getCardInfo(J)Landroid/os/Bundle;
    .locals 19
    .param p1, "sessionHandle"    # J

    .prologue
    .line 1964
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "getCardInfo(): "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1965
    const-wide/16 v14, -0x1

    cmp-long v14, p1, v14

    if-eqz v14, :cond_0

    sget-object v14, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-nez v14, :cond_2

    .line 1966
    :cond_0
    const/4 v3, 0x0

    .line 2045
    :cond_1
    :goto_0
    return-object v3

    .line 1968
    :cond_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1972
    .local v3, "cardInfo":Landroid/os/Bundle;
    const-wide/16 v8, -0x1

    .line 1973
    .local v8, "localSlotId":J
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSlotID()J

    move-result-wide v14

    const-wide/16 v16, -0x1

    cmp-long v14, v14, v16

    if-eqz v14, :cond_a

    .line 1974
    invoke-virtual/range {p0 .. p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSlotID()J

    move-result-wide v8

    .line 2004
    :cond_3
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "slot id used is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2005
    sget-object v14, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-virtual {v14, v8, v9}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetTokenInfo(J)Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;

    move-result-object v13

    .line 2006
    .local v13, "tokenInfo":Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;
    if-eqz v13, :cond_1

    .line 2008
    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->label:[C

    if-eqz v14, :cond_4

    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->label:[C

    const/4 v15, 0x0

    aget-char v14, v14, v15

    if-eqz v14, :cond_4

    .line 2010
    const-string v14, "label"

    new-instance v15, Ljava/lang/String;

    iget-object v0, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->label:[C

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getCardInfo label :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->label:[C

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2013
    :cond_4
    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->manufacturerID:[C

    if-eqz v14, :cond_5

    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->manufacturerID:[C

    const/4 v15, 0x0

    aget-char v14, v14, v15

    if-eqz v14, :cond_5

    .line 2014
    const-string v14, "manufacture"

    new-instance v15, Ljava/lang/String;

    iget-object v0, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->manufacturerID:[C

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2016
    :cond_5
    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->model:[C

    if-eqz v14, :cond_6

    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->model:[C

    const/4 v15, 0x0

    aget-char v14, v14, v15

    if-eqz v14, :cond_6

    .line 2017
    const-string v14, "model"

    new-instance v15, Ljava/lang/String;

    iget-object v0, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->model:[C

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018
    :cond_6
    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->serialNumber:[C

    if-eqz v14, :cond_7

    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->serialNumber:[C

    const/4 v15, 0x0

    aget-char v14, v14, v15

    if-eqz v14, :cond_7

    .line 2019
    const-string v14, "serialnumber"

    new-instance v15, Ljava/lang/String;

    iget-object v0, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->serialNumber:[C

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2022
    :cond_7
    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->firmwareVersion:Lcom/sec/smartcard/pkcs11/CK_VERSION;

    if-eqz v14, :cond_8

    .line 2023
    const-string v14, "fwversion"

    iget-object v15, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->firmwareVersion:Lcom/sec/smartcard/pkcs11/CK_VERSION;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getVersion(Lcom/sec/smartcard/pkcs11/CK_VERSION;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2025
    :cond_8
    iget-object v14, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->hardwareVersion:Lcom/sec/smartcard/pkcs11/CK_VERSION;

    if-eqz v14, :cond_9

    .line 2026
    const-string v14, "hwversion"

    iget-object v15, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->hardwareVersion:Lcom/sec/smartcard/pkcs11/CK_VERSION;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getVersion(Lcom/sec/smartcard/pkcs11/CK_VERSION;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2029
    :cond_9
    const-string v14, "maxpinlen"

    iget-wide v0, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->ulMaxPinLen:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v3, v14, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2030
    const-string v14, "minpinlen"

    iget-wide v0, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->ulMinPinLen:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v3, v14, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2031
    const-string v14, "flags"

    iget-wide v0, v13, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->flags:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Lcom/sec/smartcard/pkcs11/Functions;->tokenInfoFlagsToString(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 2036
    .end local v13    # "tokenInfo":Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;
    :catch_0
    move-exception v4

    .line 2038
    .local v4, "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " e.getMessage ..."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v4}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2039
    invoke-virtual {v4}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1976
    .end local v4    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    :cond_a
    :try_start_1
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "C_GetSlotList ...\n"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1977
    sget-object v14, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetSlotList(Z)[J

    move-result-object v12

    .line 1978
    .local v12, "slotList":[J
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "C_GetSlotList: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1979
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "C_GetSlotList Success\n"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1981
    if-eqz v12, :cond_3

    array-length v14, v12

    if-lez v14, :cond_3

    .line 1986
    move-object v2, v12

    .local v2, "arr$":[J
    array-length v6, v2

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_d

    aget-wide v10, v2, v5

    .line 1987
    .local v10, "slot":J
    sget-object v14, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-virtual {v14, v10, v11}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetSlotInfo(J)Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;

    move-result-object v7

    .line 1988
    .local v7, "slotInfo":Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;
    if-eqz v7, :cond_b

    .line 1989
    iget-wide v14, v7, Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;->flags:J

    const-wide/16 v16, 0x1

    and-long v14, v14, v16

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_c

    .line 1986
    :cond_b
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1993
    :cond_c
    move-wide v8, v10

    .line 1998
    .end local v7    # "slotInfo":Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;
    .end local v10    # "slot":J
    :cond_d
    const-wide/16 v14, 0x0

    cmp-long v14, v8, v14

    if-gez v14, :cond_3

    .line 1999
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "None of the slots has a token\n"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2000
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2040
    .end local v2    # "arr$":[J
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v12    # "slotList":[J
    :catch_1
    move-exception v4

    .line 2041
    .local v4, "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private getCardInfo(Landroid/os/Bundle;)Lcom/sec/smartcard/pinservice/SmartCardInfo;
    .locals 3
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 2378
    if-nez p1, :cond_0

    .line 2379
    const-string v1, "SmartcardManagerPinService"

    const-string v2, "Bundle is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2380
    const/4 v0, 0x0

    .line 2393
    :goto_0
    return-object v0

    .line 2383
    :cond_0
    new-instance v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;

    invoke-direct {v0}, Lcom/sec/smartcard/pinservice/SmartCardInfo;-><init>()V

    .line 2385
    .local v0, "cardInfo":Lcom/sec/smartcard/pinservice/SmartCardInfo;
    const-string v1, "label"

    const-string v2, "Not Available"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->label:Ljava/lang/String;

    .line 2386
    const-string v1, "manufacture"

    const-string v2, "Not Available"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->manufacture:Ljava/lang/String;

    .line 2387
    const-string v1, "model"

    const-string v2, "Not Available"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->model:Ljava/lang/String;

    .line 2388
    const-string v1, "serialnumber"

    const-string v2, "Not Available"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->serialNumber:Ljava/lang/String;

    .line 2389
    const-string v1, "flags"

    const-string v2, "Not Available"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->flags:Ljava/lang/String;

    .line 2390
    const-string v1, "hwversion"

    const-string v2, "Not Available"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->fwVersion:Ljava/lang/String;

    .line 2391
    const-string v1, "fwversion"

    const-string v2, "Not Available"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/smartcard/pinservice/SmartCardInfo;->hwVersion:Ljava/lang/String;

    goto :goto_0
.end method

.method private getCertificate(Ljava/lang/String;[B)Ljava/security/cert/X509Certificate;
    .locals 6
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "certificate"    # [B

    .prologue
    .line 1317
    const/4 v4, 0x0

    .line 1319
    .local v4, "signateCertificate":Ljava/security/cert/X509Certificate;
    if-eqz p2, :cond_0

    .line 1320
    const/4 v2, 0x0

    .line 1322
    .local v2, "certFactory":Ljava/security/cert/CertificateFactory;
    :try_start_0
    const-string v5, "X.509"

    invoke-static {v5}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v2

    .line 1323
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1324
    .local v1, "bIs":Ljava/io/ByteArrayInputStream;
    invoke-virtual {v2, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v4, v0
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1331
    .end local v1    # "bIs":Ljava/io/ByteArrayInputStream;
    .end local v2    # "certFactory":Ljava/security/cert/CertificateFactory;
    :cond_0
    :goto_0
    return-object v4

    .line 1326
    .restart local v2    # "certFactory":Ljava/security/cert/CertificateFactory;
    :catch_0
    move-exception v3

    .line 1328
    .local v3, "e":Ljava/security/cert/CertificateException;
    invoke-virtual {v3}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_0
.end method

.method private getCertificates(J)V
    .locals 17
    .param p1, "sessionHandle"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;
        }
    .end annotation

    .prologue
    .line 2106
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "getCertificates(): "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2110
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    :try_start_0
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_alias_names:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v12, v3, :cond_4

    .line 2112
    const/16 v16, 0x0

    .line 2116
    .local v16, "signature":[B
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert_alias_names:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    .line 2120
    .local v10, "curLabel":[B
    const-wide/16 v14, 0x1

    .line 2122
    .local v14, "keyClass":J
    const/4 v3, 0x2

    new-array v9, v3, [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 2123
    .local v9, "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    const/4 v3, 0x0

    new-instance v4, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v6, 0x0

    invoke-direct {v4, v6, v7, v14, v15}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JJ)V

    aput-object v4, v9, v3

    .line 2124
    const/4 v3, 0x1

    new-instance v4, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v6, 0x3

    invoke-direct {v4, v6, v7, v10}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JLjava/lang/Object;)V

    aput-object v4, v9, v3

    .line 2125
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1, v9}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsInit(J[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V

    .line 2127
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-wide/16 v4, 0x1

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjects(JJ)[J

    move-result-object v13

    .line 2128
    .local v13, "pCertObject":[J
    if-eqz v13, :cond_0

    array-length v3, v13

    if-nez v3, :cond_1

    .line 2129
    :cond_0
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 2130
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Unable to find certificate object"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2150
    .end local v9    # "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v10    # "curLabel":[B
    .end local v13    # "pCertObject":[J
    .end local v14    # "keyClass":J
    .end local v16    # "signature":[B
    :catch_0
    move-exception v11

    .line 2151
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 2152
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 2153
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v3

    .line 2133
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v9    # "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .restart local v10    # "curLabel":[B
    .restart local v13    # "pCertObject":[J
    .restart local v14    # "keyClass":J
    .restart local v16    # "signature":[B
    :cond_1
    const/16 v3, 0x2710

    :try_start_1
    new-array v2, v3, [B

    .line 2134
    .local v2, "bCertificate":[B
    const/4 v3, 0x1

    new-array v8, v3, [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 2135
    .local v8, "certAttrib":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    const/4 v3, 0x0

    new-instance v4, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v6, 0x11

    invoke-direct {v4, v6, v7, v2}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JLjava/lang/Object;)V

    aput-object v4, v8, v3

    .line 2136
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v4, 0x0

    aget-wide v6, v13, v4

    move-wide/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetAttributeValue(JJ[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V

    .line 2139
    if-eqz v8, :cond_2

    const/4 v3, 0x0

    aget-object v3, v8, v3

    iget-object v3, v3, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    if-nez v3, :cond_3

    .line 2140
    :cond_2
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 2141
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    const-string v4, "Unable to read certificate object"

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v3

    .line 2144
    :cond_3
    const/4 v3, 0x0

    aget-object v3, v8, v3

    invoke-virtual {v3}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->getByteArray()[B

    move-result-object v16

    .line 2145
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cert:Ljava/util/List;

    move-object/from16 v0, v16

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2147
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsFinal(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2110
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 2155
    .end local v2    # "bCertificate":[B
    .end local v8    # "certAttrib":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v9    # "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v10    # "curLabel":[B
    .end local v13    # "pCertObject":[J
    .end local v14    # "keyClass":J
    .end local v16    # "signature":[B
    :cond_4
    return-void
.end method

.method private getCuidFromCard(J)Ljava/lang/String;
    .locals 19
    .param p1, "sessionHandle"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;
        }
    .end annotation

    .prologue
    .line 1889
    const/4 v9, 0x0

    .line 1890
    .local v9, "cuid":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1891
    .local v14, "serial_number":Ljava/lang/String;
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "getCuidFromCard(): "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1893
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-eqz v3, :cond_4

    .line 1896
    :try_start_0
    const-string v3, "CUID"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    .line 1898
    .local v10, "cuidLabel":[B
    const/4 v3, 0x2

    new-array v11, v3, [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 1899
    .local v11, "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    const/4 v3, 0x0

    new-instance v4, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v6, 0x0

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-direct {v4, v6, v7, v0, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JJ)V

    aput-object v4, v11, v3

    .line 1900
    const/4 v3, 0x1

    new-instance v4, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v6, 0x3

    invoke-direct {v4, v6, v7, v10}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JLjava/lang/Object;)V

    aput-object v4, v11, v3

    .line 1901
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1, v11}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsInit(J[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V

    .line 1903
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-wide/16 v4, 0x1

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjects(JJ)[J

    move-result-object v13

    .line 1905
    .local v13, "objId":[J
    if-eqz v13, :cond_0

    array-length v3, v13

    if-nez v3, :cond_1

    .line 1907
    :cond_0
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "cuid is null ... trying to get serial_number from Token_Info "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1908
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsFinal(J)V

    .line 1909
    invoke-direct/range {p0 .. p2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSerialNumberFromCard(J)Ljava/lang/String;

    move-result-object v14

    move-object v3, v14

    .line 1943
    .end local v10    # "cuidLabel":[B
    .end local v11    # "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v13    # "objId":[J
    :goto_0
    return-object v3

    .line 1914
    .restart local v10    # "cuidLabel":[B
    .restart local v11    # "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .restart local v13    # "objId":[J
    :cond_1
    const/16 v3, 0x200

    new-array v2, v3, [B

    .line 1915
    .local v2, "bCuid":[B
    const/4 v3, 0x1

    new-array v8, v3, [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 1916
    .local v8, "cuidAttrib":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    const/4 v3, 0x0

    new-instance v4, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v6, 0x11

    invoke-direct {v4, v6, v7, v2}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JLjava/lang/Object;)V

    aput-object v4, v8, v3

    .line 1917
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v4, 0x0

    aget-wide v6, v13, v4

    move-wide/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetAttributeValue(JJ[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V

    .line 1919
    if-eqz v8, :cond_2

    const/4 v3, 0x0

    aget-object v3, v8, v3

    iget-object v3, v3, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    if-nez v3, :cond_3

    .line 1921
    :cond_2
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "cuid is null ... trying to get serial_number from Token_Info "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1922
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsFinal(J)V

    .line 1923
    invoke-direct/range {p0 .. p2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSerialNumberFromCard(J)Ljava/lang/String;

    move-result-object v14

    move-object v3, v14

    .line 1924
    goto :goto_0

    .line 1928
    :cond_3
    const/4 v3, 0x0

    aget-object v3, v8, v3

    invoke-virtual {v3}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->getByteArray()[B

    move-result-object v3

    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->bytesToHex([B)Ljava/lang/String;

    move-result-object v9

    .line 1929
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsFinal(J)V
    :try_end_0
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1942
    .end local v2    # "bCuid":[B
    .end local v8    # "cuidAttrib":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v10    # "cuidLabel":[B
    .end local v11    # "cuidTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    .end local v13    # "objId":[J
    :cond_4
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "returning cuid"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v9

    .line 1943
    goto :goto_0

    .line 1931
    :catch_0
    move-exception v12

    .line 1932
    .local v12, "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    const-string v3, "SmartcardManagerPinService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " e.getMessage ..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1933
    invoke-virtual {v12}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V

    .line 1935
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1936
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v12}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v3
.end method

.method private getEntry()Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    .locals 2

    .prologue
    .line 730
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 731
    :cond_0
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "Queue is empty"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    const/4 v0, 0x0

    .line 735
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 392
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    if-nez v0, :cond_0

    .line 393
    new-instance v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    .line 397
    :cond_0
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinService:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    return-object v0
.end method

.method private getIntent(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.sec.smartcard.pinservice.INVALIDATE_PIN."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLoginAttemptRemain(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 4
    .param p1, "command"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 2666
    new-instance v1, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;Ljava/lang/String;)V

    .line 2671
    .local v1, "gOpt":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getCardInfoOperation;
    :try_start_0
    iget-object v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;

    const/4 v3, 0x5

    invoke-interface {v2, v3}, Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;->onComplete(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2676
    :goto_0
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V

    .line 2679
    return-void

    .line 2672
    :catch_0
    move-exception v0

    .line 2674
    .local v0, "e1":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private getPin(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 4
    .param p1, "command"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 1238
    iget v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    if-nez v2, :cond_5

    .line 1240
    iget v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    invoke-direct {p0, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isCardRegistered(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1241
    const-string v2, "SmartcardManagerPinService"

    const-string v3, " Card is not Registered and shown error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->showCardNotRegisteredDialog()V

    .line 1245
    :try_start_0
    iget-object v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    const/16 v3, 0x9

    invoke-interface {v2, v3}, Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;->onUserPinError(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1251
    :goto_0
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V

    .line 1287
    :cond_0
    :goto_1
    return-void

    .line 1246
    :catch_0
    move-exception v0

    .line 1248
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1255
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    iget v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    invoke-direct {p0, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getPinForCaller(I)[C

    move-result-object v1

    .line 1258
    .local v1, "pin":[C
    if-eqz v1, :cond_2

    .line 1260
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "PIN is cached"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    :try_start_1
    iget-object v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    invoke-interface {v2, v1}, Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;->onUserEnteredPin([C)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1267
    :goto_2
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V

    goto :goto_1

    .line 1263
    :catch_1
    move-exception v0

    .line 1265
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 1271
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    iget-boolean v2, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mpinDialogDisplayCheckNeed:Z

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isHomeActivityVisible()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isCallingApplicationVisibile(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1273
    :cond_3
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->showPinDialog()V

    goto :goto_1

    .line 1276
    :cond_4
    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->showNotifcation(I)V

    goto :goto_1

    .line 1278
    .end local v1    # "pin":[C
    :cond_5
    iget v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 1279
    const/4 v2, 0x3

    iput v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    .line 1283
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->update(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    .line 1284
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getPinVerifyCard(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    goto :goto_1
.end method

.method private getPinCacheIntervalMils()J
    .locals 4

    .prologue
    const-wide/32 v0, 0x6ddd00

    .line 348
    sget v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinCacheInterval:I

    packed-switch v2, :pswitch_data_0

    .line 358
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "getPinCacheIntervalMils return default"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :goto_0
    :pswitch_0
    return-wide v0

    .line 350
    :pswitch_1
    const-wide/32 v0, 0xdbba0

    goto :goto_0

    .line 352
    :pswitch_2
    const-wide/32 v0, 0x1b7740

    goto :goto_0

    .line 354
    :pswitch_3
    const-wide/32 v0, 0x36ee80

    goto :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private getPinForCaller(I)[C
    .locals 7
    .param p1, "appUid"    # I

    .prologue
    .line 1593
    const/4 v3, 0x0

    .line 1595
    .local v3, "pin":[C
    sget-object v5, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v5}, Ljava/util/Queue;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move-object v4, v3

    .line 1610
    .end local v3    # "pin":[C
    .local v4, "pin":[C
    :goto_0
    return-object v4

    .line 1598
    .end local v4    # "pin":[C
    .restart local v3    # "pin":[C
    :cond_1
    sget-object v5, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v5}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    invoke-virtual {v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v2

    .line 1599
    .local v2, "id":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueGetEntry(Ljava/lang/String;)Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    move-result-object v1

    .line 1600
    .local v1, "entry":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    if-eqz v1, :cond_2

    .line 1601
    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->decryptPin()[C

    move-result-object v0

    .line 1602
    .local v0, "decryptPin":[C
    if-nez v0, :cond_3

    .line 1604
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "decryptPin error, clear entry"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1605
    invoke-direct {p0, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueRemoveEntry(Ljava/lang/String;)Z

    .end local v0    # "decryptPin":[C
    :cond_2
    :goto_1
    move-object v4, v3

    .line 1610
    .end local v3    # "pin":[C
    .restart local v4    # "pin":[C
    goto :goto_0

    .line 1607
    .end local v4    # "pin":[C
    .restart local v0    # "decryptPin":[C
    .restart local v3    # "pin":[C
    :cond_3
    invoke-virtual {v0}, [C->clone()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "pin":[C
    check-cast v3, [C

    .restart local v3    # "pin":[C
    goto :goto_1
.end method

.method private getPinVerifyCard(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 7
    .param p1, "command"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 1292
    const/4 v4, 0x1

    new-array v1, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "CardCUID"

    aput-object v5, v1, v4

    .line 1293
    .local v1, "coulmns":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v4

    const-string v5, "SMRegistration"

    iget-object v6, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    invoke-virtual {v4, v5, v1, v6}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    .line 1296
    .local v2, "cv":Landroid/content/ContentValues;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 1298
    :cond_0
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "Unable to get cardinfo details for getpin"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    :goto_0
    return-void

    .line 1302
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1303
    .local v0, "cardInfo":Landroid/os/Bundle;
    const-string v4, "cardInfo"

    const-string v5, "CardCUID"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1307
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    invoke-direct {v3, p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    .line 1311
    .local v3, "gOpt":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;
    iget-object v4, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    iget-object v5, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->mGetPinVerify:Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;

    invoke-direct {p0, v4, v0, v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->verify_card([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;)V

    goto :goto_0
.end method

.method private getSerialNumberFromCard(J)Ljava/lang/String;
    .locals 23
    .param p1, "sessionHandle"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;
        }
    .end annotation

    .prologue
    .line 1836
    const-string v17, "SmartcardManagerPinService"

    const-string v18, "getSerialNumberFromCard(): "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1837
    const-wide/16 v18, -0x1

    cmp-long v17, p1, v18

    if-eqz v17, :cond_0

    sget-object v17, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-nez v17, :cond_2

    .line 1838
    :cond_0
    const-string v17, "SmartcardManagerPinService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "getSerialNumberFromCard(): sessionHandle : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1839
    const/4 v10, 0x0

    .line 1885
    :cond_1
    :goto_0
    return-object v10

    .line 1841
    :cond_2
    const/4 v10, 0x0

    .line 1843
    .local v10, "serial_num":Ljava/lang/String;
    const-wide/16 v8, -0x1

    .line 1844
    .local v8, "localSlotId":J
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSlotID()J

    move-result-wide v18

    const-wide/16 v20, -0x1

    cmp-long v17, v18, v20

    if-eqz v17, :cond_4

    .line 1845
    invoke-virtual/range {p0 .. p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSlotID()J

    move-result-wide v8

    .line 1870
    :cond_3
    const-string v17, "SmartcardManagerPinService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "slot id used is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1871
    sget-object v17, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetTokenInfo(J)Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;

    move-result-object v16

    .line 1872
    .local v16, "tokenInfo":Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;
    if-eqz v16, :cond_1

    .line 1873
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->serialNumber:[C

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 1874
    new-instance v11, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;->serialNumber:[C

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Ljava/lang/String;-><init>([C)V

    .end local v10    # "serial_num":Ljava/lang/String;
    .local v11, "serial_num":Ljava/lang/String;
    move-object v10, v11

    .end local v11    # "serial_num":Ljava/lang/String;
    .restart local v10    # "serial_num":Ljava/lang/String;
    goto :goto_0

    .line 1847
    .end local v16    # "tokenInfo":Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;
    :cond_4
    const-string v17, "SmartcardManagerPinService"

    const-string v18, "C_GetSlotList ...\n"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1848
    sget-object v17, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetSlotList(Z)[J

    move-result-object v15

    .line 1849
    .local v15, "slotList":[J
    const-string v17, "SmartcardManagerPinService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "C_GetSlotList: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1850
    const-string v17, "SmartcardManagerPinService"

    const-string v18, "C_GetSlotList Success\n"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1851
    if-eqz v15, :cond_3

    array-length v0, v15

    move/from16 v17, v0

    if-lez v17, :cond_3

    .line 1852
    move-object v4, v15

    .local v4, "arr$":[J
    array-length v7, v4

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_7

    aget-wide v12, v4, v6

    .line 1853
    .local v12, "slot":J
    sget-object v17, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetSlotInfo(J)Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;

    move-result-object v14

    .line 1854
    .local v14, "slotInfo":Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;
    if-eqz v14, :cond_5

    .line 1855
    iget-wide v0, v14, Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;->flags:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x1

    and-long v18, v18, v20

    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-nez v17, :cond_6

    .line 1852
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1859
    :cond_6
    move-wide v8, v12

    .line 1864
    .end local v12    # "slot":J
    .end local v14    # "slotInfo":Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;
    :cond_7
    const-wide/16 v18, 0x0

    cmp-long v17, v8, v18

    if-gez v17, :cond_3

    .line 1865
    const-string v17, "SmartcardManagerPinService"

    const-string v18, "None of the slots has a token\n"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1866
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 1877
    .end local v4    # "arr$":[J
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v15    # "slotList":[J
    :catch_0
    move-exception v5

    .line 1878
    .local v5, "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    const-string v17, "SmartcardManagerPinService"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " e.getMessage ..."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v5}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1879
    invoke-virtual {v5}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V

    .line 1880
    const/16 v17, 0x5

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1881
    new-instance v17, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v5}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v17

    .line 1882
    .end local v5    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    :catch_1
    move-exception v5

    .line 1883
    .local v5, "e":Landroid/os/RemoteException;
    invoke-virtual {v5}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private getStatusMessageId(I)I
    .locals 1
    .param p1, "lError"    # I

    .prologue
    .line 1182
    const v0, 0x7f050005

    .line 1185
    .local v0, "id":I
    packed-switch p1, :pswitch_data_0

    .line 1208
    :goto_0
    :pswitch_0
    return v0

    .line 1188
    :pswitch_1
    const v0, 0x7f050006

    .line 1189
    goto :goto_0

    .line 1191
    :pswitch_2
    const v0, 0x7f050007

    .line 1192
    goto :goto_0

    .line 1194
    :pswitch_3
    const v0, 0x7f050009

    .line 1195
    goto :goto_0

    .line 1197
    :pswitch_4
    const v0, 0x7f05000c

    .line 1198
    goto :goto_0

    .line 1201
    :pswitch_5
    const v0, 0x7f05000e

    .line 1202
    goto :goto_0

    .line 1205
    :pswitch_6
    const v0, 0x7f05000d

    goto :goto_0

    .line 1185
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private getVersion(Lcom/sec/smartcard/pkcs11/CK_VERSION;)Ljava/lang/String;
    .locals 4
    .param p1, "v"    # Lcom/sec/smartcard/pkcs11/CK_VERSION;

    .prologue
    .line 1948
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1949
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string v1, "SmartcardManagerPinService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVersion major :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-byte v3, p1, Lcom/sec/smartcard/pkcs11/CK_VERSION;->major:B

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1950
    const-string v1, "SmartcardManagerPinService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVersion minor :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-byte v3, p1, Lcom/sec/smartcard/pkcs11/CK_VERSION;->minor:B

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1951
    iget-byte v1, p1, Lcom/sec/smartcard/pkcs11/CK_VERSION;->major:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1952
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1953
    iget-byte v1, p1, Lcom/sec/smartcard/pkcs11/CK_VERSION;->minor:B

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    .line 1954
    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1956
    :cond_0
    iget-byte v1, p1, Lcom/sec/smartcard/pkcs11/CK_VERSION;->minor:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1958
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private isCallingApplicationVisibile(I)Z
    .locals 14
    .param p1, "uid"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1085
    iget-object v12, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-string v13, "activity"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1087
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v10}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v9

    .line 1089
    .local v9, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v9, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1090
    .local v7, "rt":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v12, v7, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v12}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 1091
    .local v4, "packageName":Ljava/lang/String;
    iget-object v12, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 1092
    .local v6, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v6, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v5

    .line 1093
    .local v5, "packages":[Ljava/lang/String;
    move-object v1, v5

    .local v1, "arr$":[Ljava/lang/String;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v8, v1, v2

    .line 1094
    .local v8, "s":Ljava/lang/String;
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1095
    const-string v11, "SmartcardManagerPinService"

    const-string v12, "Calling Application is visible"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    const-string v11, "SmartcardManagerPinService"

    const-string v12, "isCallingApplicationVisibile: true"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    .end local v8    # "s":Ljava/lang/String;
    :goto_1
    return v10

    .line 1093
    .restart local v8    # "s":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v8    # "s":Ljava/lang/String;
    :cond_1
    move v10, v11

    .line 1100
    goto :goto_1
.end method

.method private isCardAlreadyRegistered(Ljava/lang/String;)Z
    .locals 6
    .param p1, "cuid"    # Ljava/lang/String;

    .prologue
    .line 2410
    const/4 v2, 0x0

    .line 2411
    .local v2, "ret":Z
    if-nez p1, :cond_0

    move v3, v2

    .line 2421
    .end local v2    # "ret":Z
    .local v3, "ret":I
    :goto_0
    return v3

    .line 2413
    .end local v3    # "ret":I
    .restart local v2    # "ret":Z
    :cond_0
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "CardCUID"

    aput-object v5, v0, v4

    .line 2414
    .local v0, "coulmns":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v4

    const-string v5, "SMRegistration"

    invoke-virtual {v4, v5, v0, p1}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    .line 2417
    .local v1, "cv":Landroid/content/ContentValues;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 2418
    const/4 v2, 0x1

    :cond_1
    move v3, v2

    .line 2421
    .restart local v3    # "ret":I
    goto :goto_0
.end method

.method private isCardNotRegisteredErrorDialogShown()Z
    .locals 1

    .prologue
    .line 1042
    const/4 v0, 0x0

    return v0
.end method

.method private isCardRegistered(I)Z
    .locals 1
    .param p1, "appUid"    # I

    .prologue
    .line 1620
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->anyCardRegistered()Z

    move-result v0

    return v0
.end method

.method private isDialogShown()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1114
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1116
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 1118
    .local v2, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1119
    .local v1, "rt":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v5, v1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1121
    const-string v4, "SmartcardManagerPinService"

    const-string v5, " PIN Dialog is currently shown"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method private isHomeActivityVisible()Z
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1053
    iget-object v8, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-string v11, "activity"

    invoke-virtual {v8, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 1055
    .local v1, "am":Landroid/app/ActivityManager;
    invoke-virtual {v1, v9}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v7

    .line 1057
    .local v7, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v7, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1058
    .local v6, "rt":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v8, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 1059
    .local v5, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1060
    .local v2, "i":Landroid/content/Intent;
    const-string v8, "android.intent.category.HOME"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1061
    const-string v8, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1062
    const/high16 v8, 0x10000

    invoke-virtual {v5, v2, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 1064
    .local v4, "lst":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1065
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/content/pm/ResolveInfo;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1066
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    iget-object v0, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 1067
    .local v0, "ai":Landroid/content/pm/ActivityInfo;
    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iget-object v11, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1068
    const-string v8, "SmartcardManagerPinService"

    const-string v10, "Device in Home Activity"

    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    const-string v8, "SmartcardManagerPinService"

    const-string v10, "isHomeActivityVisible: true"

    invoke-static {v8, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 1073
    .end local v0    # "ai":Landroid/content/pm/ActivityInfo;
    :goto_0
    return v8

    :cond_1
    move v8, v10

    goto :goto_0
.end method

.method private loginToCard([C)J
    .locals 26
    .param p1, "mPin"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;
        }
    .end annotation

    .prologue
    .line 1739
    const-wide/16 v18, 0x0

    .line 1740
    .local v18, "sessionHandle":J
    const-string v3, "SmartcardManagerPinService"

    const-string v6, "loginToCard(): "

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1741
    if-eqz p1, :cond_0

    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-nez v3, :cond_1

    .line 1742
    :cond_0
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1743
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    const-string v6, "Mpin or mPkcs are null"

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v6}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v3

    .line 1746
    :cond_1
    :try_start_0
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_Initialize(Ljava/lang/Object;)V

    .line 1747
    const-string v3, "SmartcardManagerPinService"

    const-string v6, "C_Initialize Success\n"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1748
    const-wide/16 v4, -0x1

    .line 1749
    .local v4, "localSlotId":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSlotID()J

    move-result-wide v6

    const-wide/16 v24, -0x1

    cmp-long v3, v6, v24

    if-eqz v3, :cond_3

    .line 1750
    invoke-virtual/range {p0 .. p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getSlotID()J

    move-result-wide v4

    .line 1773
    :cond_2
    const-string v3, "SmartcardManagerPinService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "slot id used is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1774
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-wide/16 v6, 0x4

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v3 .. v9}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_OpenSession(JJLjava/lang/Object;Lcom/sec/smartcard/pkcs11/CK_NOTIFY;)J
    :try_end_0
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v8

    .line 1775
    .end local v18    # "sessionHandle":J
    .local v8, "sessionHandle":J
    :try_start_1
    const-string v3, "SmartcardManagerPinService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sessionHandle :  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1778
    const-wide/16 v10, 0x1

    .line 1779
    .local v10, "utype":J
    sget-object v7, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-object/from16 v12, p1

    invoke-virtual/range {v7 .. v12}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_Login(JJ[C)V

    .line 1781
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I
    :try_end_1
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1815
    .end local v4    # "localSlotId":J
    .end local v10    # "utype":J
    :goto_0
    const-string v3, "SmartcardManagerPinService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sessionHandle returned from login :  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1816
    return-wide v8

    .line 1752
    .end local v8    # "sessionHandle":J
    .restart local v4    # "localSlotId":J
    .restart local v18    # "sessionHandle":J
    :cond_3
    :try_start_2
    const-string v3, "SmartcardManagerPinService"

    const-string v6, "C_GetSlotList ...\n"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1753
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetSlotList(Z)[J

    move-result-object v22

    .line 1754
    .local v22, "slotList":[J
    const-string v3, "SmartcardManagerPinService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "C_GetSlotList: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1755
    const-string v3, "SmartcardManagerPinService"

    const-string v6, "C_GetSlotList Success\n"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1756
    const-string v3, "SmartcardManagerPinService"

    const-string v6, "C_GetSlotInfo ...\n"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1757
    move-object/from16 v2, v22

    .local v2, "arr$":[J
    array-length v15, v2

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_1
    if-ge v14, v15, :cond_6

    aget-wide v20, v2, v14

    .line 1758
    .local v20, "slot":J
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GetSlotInfo(J)Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;

    move-result-object v17

    .line 1759
    .local v17, "slotInfo":Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;
    if-eqz v17, :cond_4

    .line 1760
    move-object/from16 v0, v17

    iget-wide v6, v0, Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;->flags:J

    const-wide/16 v24, 0x1

    and-long v6, v6, v24

    const-wide/16 v24, 0x0

    cmp-long v3, v6, v24

    if-nez v3, :cond_5

    .line 1757
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 1763
    :cond_5
    move-wide/from16 v4, v20

    .line 1768
    .end local v17    # "slotInfo":Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;
    .end local v20    # "slot":J
    :cond_6
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 1769
    const-string v3, "SmartcardManagerPinService"

    const-string v6, "None of the slots has a token\n"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1770
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    const-string v6, "No Token Present"

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v6}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1782
    .end local v2    # "arr$":[J
    .end local v4    # "localSlotId":J
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v22    # "slotList":[J
    :catch_0
    move-exception v13

    move-wide/from16 v8, v18

    .line 1784
    .end local v18    # "sessionHandle":J
    .restart local v8    # "sessionHandle":J
    .local v13, "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    :goto_2
    const-string v3, "SmartcardManagerPinService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " e.getMessage ..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v13}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1785
    invoke-virtual {v13}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V

    .line 1787
    invoke-virtual {v13}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getErrorCode()J

    move-result-wide v6

    long-to-int v0, v6

    move/from16 v16, v0

    .line 1788
    .local v16, "ret":I
    const-string v3, "SmartcardManagerPinService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " errorCode :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1790
    packed-switch v16, :pswitch_data_0

    .line 1803
    :pswitch_0
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1808
    :goto_3
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v13}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v6}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v3

    .line 1792
    :pswitch_1
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    goto :goto_3

    .line 1797
    :pswitch_2
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    goto :goto_3

    .line 1800
    :pswitch_3
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    goto :goto_3

    .line 1812
    .end local v8    # "sessionHandle":J
    .end local v13    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    .end local v16    # "ret":I
    .restart local v18    # "sessionHandle":J
    :catch_1
    move-exception v13

    move-wide/from16 v8, v18

    .line 1813
    .end local v18    # "sessionHandle":J
    .restart local v8    # "sessionHandle":J
    .local v13, "e":Landroid/os/RemoteException;
    :goto_4
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 1812
    .end local v13    # "e":Landroid/os/RemoteException;
    .restart local v4    # "localSlotId":J
    :catch_2
    move-exception v13

    goto :goto_4

    .line 1782
    :catch_3
    move-exception v13

    goto :goto_2

    .line 1790
    nop

    :pswitch_data_0
    .packed-switch 0xa0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private logoutCard(J[C)V
    .locals 5
    .param p1, "sessionHandle"    # J
    .param p3, "mPin"    # [C

    .prologue
    .line 1647
    const-string v1, "SmartcardManagerPinService"

    const-string v2, "logoutCard(): "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1648
    if-eqz p3, :cond_0

    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-eqz v1, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 1671
    :cond_0
    :goto_0
    return-void

    .line 1652
    :cond_1
    :try_start_0
    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-virtual {v1, p1, p2}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_Logout(J)V

    .line 1655
    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-virtual {v1, p1, p2}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_CloseSession(J)V
    :try_end_0
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1663
    :try_start_1
    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_Finalize(Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1664
    :catch_0
    move-exception v0

    .line 1666
    .local v0, "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    invoke-virtual {v0}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V

    goto :goto_0

    .line 1657
    .end local v0    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    :catch_1
    move-exception v0

    .line 1659
    .restart local v0    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    :try_start_2
    invoke-virtual {v0}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1663
    :try_start_3
    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_Finalize(Ljava/lang/Object;)V
    :try_end_3
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 1664
    :catch_2
    move-exception v0

    .line 1666
    invoke-virtual {v0}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V

    goto :goto_0

    .line 1662
    .end local v0    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    :catchall_0
    move-exception v1

    .line 1663
    :try_start_4
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_Finalize(Ljava/lang/Object;)V
    :try_end_4
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 1668
    :goto_1
    throw v1

    .line 1664
    :catch_3
    move-exception v0

    .line 1666
    .restart local v0    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    invoke-virtual {v0}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private pinQueueAddEntry(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z
    .locals 3
    .param p1, "entry"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .prologue
    const/4 v0, 0x0

    .line 180
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    if-nez v1, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v0

    .line 183
    :cond_1
    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 184
    sget-object v1, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 185
    const-string v1, "SmartcardManagerPinService"

    const-string v2, "entery already present on the queue"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 189
    :cond_2
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->setCacheAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)V

    .line 194
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private pinQueueGetEntry(Ljava/lang/String;)Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    .locals 5
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 255
    const/4 v2, 0x0

    .line 257
    .local v2, "ret":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    if-eqz p1, :cond_0

    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move-object v3, v2

    .line 270
    .end local v2    # "ret":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    .local v3, "ret":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    :goto_0
    return-object v3

    .line 260
    .end local v3    # "ret":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    .restart local v2    # "ret":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    :cond_1
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 261
    .local v1, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/smartcard/pinservice/SmartCardManagerPin;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 262
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .line 263
    .local v0, "e":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 264
    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 265
    move-object v2, v0

    .end local v0    # "e":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    :cond_3
    move-object v3, v2

    .line 270
    .end local v2    # "ret":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    .restart local v3    # "ret":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    goto :goto_0
.end method

.method private pinQueueRemoveAllEntry()Z
    .locals 7

    .prologue
    .line 294
    const/4 v2, 0x0

    .line 296
    .local v2, "ret":Z
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "id empty"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v3, v2

    .line 311
    .end local v2    # "ret":Z
    .local v3, "ret":I
    :goto_0
    return v3

    .line 300
    .end local v3    # "ret":I
    .restart local v2    # "ret":Z
    :cond_1
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 301
    .local v1, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/smartcard/pinservice/SmartCardManagerPin;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 302
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .line 303
    .local v0, "e":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 304
    const-string v4, "SmartcardManagerPinService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "item removed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 306
    invoke-direct {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cancelCacheAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)V

    .line 307
    const/4 v2, 0x1

    .end local v0    # "e":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    :cond_3
    move v3, v2

    .line 311
    .restart local v3    # "ret":I
    goto :goto_0
.end method

.method private pinQueueRemoveEntry(Ljava/lang/String;)Z
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 274
    const/4 v2, 0x0

    .line 276
    .local v2, "ret":Z
    if-eqz p1, :cond_0

    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v3, v2

    .line 290
    .end local v2    # "ret":Z
    .local v3, "ret":I
    :goto_0
    return v3

    .line 279
    .end local v3    # "ret":I
    .restart local v2    # "ret":Z
    :cond_1
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 280
    .local v1, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/smartcard/pinservice/SmartCardManagerPin;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 281
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .line 282
    .local v0, "e":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 283
    const-string v4, "SmartcardManagerPinService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "item removed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 285
    invoke-direct {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cancelCacheAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)V

    .line 286
    const/4 v2, 0x1

    .end local v0    # "e":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    :cond_3
    move v3, v2

    .line 290
    .restart local v3    # "ret":I
    goto :goto_0
.end method

.method private pinQueueUpdateAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z
    .locals 6
    .param p1, "entry"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .prologue
    .line 198
    const/4 v2, 0x0

    .line 199
    .local v2, "ret":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v3, v2

    .line 217
    .end local v2    # "ret":Z
    .local v3, "ret":I
    :goto_0
    return v3

    .line 203
    .end local v3    # "ret":I
    .restart local v2    # "ret":Z
    :cond_1
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mPinQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 204
    .local v1, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/smartcard/pinservice/SmartCardManagerPin;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 205
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .line 206
    .local v0, "e":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 207
    invoke-virtual {p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 208
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "pinQueueUpdateAlarm"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-direct {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->cancelCacheAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)V

    .line 211
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->setCacheAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)V

    .line 212
    const/4 v2, 0x1

    goto :goto_1

    .end local v0    # "e":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    :cond_3
    move v3, v2

    .line 217
    .restart local v3    # "ret":I
    goto :goto_0
.end method

.method private registerCard(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 4
    .param p1, "command"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 1626
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "registerCard(): "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1627
    new-instance v1, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;

    iget-object v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    invoke-direct {v1, p0, p1, v2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;[C)V

    .line 1630
    .local v1, "rOpt":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;
    :try_start_0
    iget-object v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    iget-object v3, v1, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$registerCardOperation;->mRegisterCallback:Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;

    invoke-direct {p0, v2, v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->register_card_pkcs11([CLcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1635
    :goto_0
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V

    .line 1636
    return-void

    .line 1631
    :catch_0
    move-exception v0

    .line 1633
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private register_card_pkcs11([CLcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;)V
    .locals 9
    .param p1, "pin"    # [C
    .param p2, "callback"    # Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2228
    const/4 v0, 0x0

    .line 2229
    .local v0, "cardInfo":Landroid/os/Bundle;
    const-wide/16 v6, -0x1

    .line 2230
    .local v6, "sessionHandle":J
    const/4 v2, 0x0

    .line 2231
    .local v2, "cuid":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2232
    .local v4, "key_usage":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2236
    .local v5, "sigCert":[B
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->loginToCard([C)J

    move-result-wide v6

    .line 2237
    invoke-direct {p0, v6, v7}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getCuidFromCard(J)Ljava/lang/String;

    move-result-object v2

    .line 2239
    invoke-direct {p0, v6, v7}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getCardInfo(J)Landroid/os/Bundle;

    move-result-object v0

    .line 2240
    invoke-direct {p0, v6, v7}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getAllAliasNames(J)V

    .line 2241
    invoke-direct {p0, v6, v7}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getCertificates(J)V

    .line 2242
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getAllKeyUsage()V

    .line 2243
    if-nez v0, :cond_0

    .line 2244
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .end local v0    # "cardInfo":Landroid/os/Bundle;
    .local v1, "cardInfo":Landroid/os/Bundle;
    move-object v0, v1

    .line 2248
    .end local v1    # "cardInfo":Landroid/os/Bundle;
    .restart local v0    # "cardInfo":Landroid/os/Bundle;
    :cond_0
    const-string v8, "cardInfo"

    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2254
    invoke-direct {p0, v6, v7, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V

    .line 2256
    :goto_0
    if-eqz p2, :cond_1

    .line 2257
    iget v8, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    invoke-interface {p2, v8, p1, v0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;->onComplete(I[CLandroid/os/Bundle;)V

    .line 2258
    :cond_1
    return-void

    .line 2251
    :catch_0
    move-exception v3

    .line 2252
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2254
    invoke-direct {p0, v6, v7, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V

    goto :goto_0

    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    invoke-direct {p0, v6, v7, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V

    throw v8
.end method

.method private removeAndExecuteOpertion()V
    .locals 2

    .prologue
    .line 708
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "removeAndExecuteOpertion: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 710
    :cond_0
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "Queue is empty"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    :goto_0
    return-void

    .line 713
    :cond_1
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 714
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    goto :goto_0
.end method

.method private removeAndExecuteVerifyOperation()V
    .locals 2

    .prologue
    .line 719
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "removeAndExecuteVerifyOpertion: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 721
    :cond_0
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "removeAndExecuteVerifyOpertion Queue is empty"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    :goto_0
    return-void

    .line 724
    :cond_1
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 725
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteVerifyOperation()Z

    goto :goto_0
.end method

.method private setCacheAlarm(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)V
    .locals 8
    .param p1, "entry"    # Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    .prologue
    .line 319
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "setCacheAlarm"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 322
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.enterprise.mdm.sc.action.INVALIDATE_CACHE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 323
    .local v1, "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 324
    const-string v3, "cardId"

    invoke-virtual {p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    const/high16 v5, 0x10000000

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 327
    .local v2, "pi":Landroid/app/PendingIntent;
    const/4 v3, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getPinCacheIntervalMils()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 329
    return-void
.end method

.method private showNotifcation(I)V
    .locals 12
    .param p1, "lError"    # I

    .prologue
    const/high16 v11, 0x10000000

    const/high16 v10, 0x8000000

    const/4 v9, 0x1

    const/4 v7, 0x0

    const v8, 0x7f050004

    .line 1130
    const v2, 0x108008a

    .line 1133
    .local v2, "iconId":I
    if-eqz p1, :cond_0

    .line 1134
    const v2, 0x1080027

    .line 1136
    :cond_0
    if-eq p1, v9, :cond_1

    if-nez p1, :cond_2

    .line 1137
    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-class v6, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1138
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {v1, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1139
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v5, v7, v1, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1141
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    new-instance v5, Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getStatusMessageId(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v4

    .line 1168
    .local v4, "notification":Landroid/app/Notification;
    :goto_0
    iget v5, v4, Landroid/app/Notification;->flags:I

    or-int/lit16 v5, v5, 0xa0

    iput v5, v4, Landroid/app/Notification;->flags:I

    .line 1170
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-string v6, "notification"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 1172
    .local v3, "mgr":Landroid/app/NotificationManager;
    invoke-virtual {v3, v9, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1173
    return-void

    .line 1151
    .end local v0    # "contentIntent":Landroid/app/PendingIntent;
    .end local v1    # "i":Landroid/content/Intent;
    .end local v3    # "mgr":Landroid/app/NotificationManager;
    .end local v4    # "notification":Landroid/app/Notification;
    :cond_2
    new-instance v1, Landroid/content/Intent;

    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-class v6, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1152
    .restart local v1    # "i":Landroid/content/Intent;
    invoke-virtual {v1, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1153
    const-string v5, "status"

    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getStatusMessageId(I)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1154
    const-string v5, "showPinDialog"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1155
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v5, v7, v1, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1157
    .restart local v0    # "contentIntent":Landroid/app/PendingIntent;
    new-instance v5, Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getStatusMessageId(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v4

    .restart local v4    # "notification":Landroid/app/Notification;
    goto :goto_0
.end method

.method private showPinDialog()V
    .locals 3

    .prologue
    .line 1104
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isDialogShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1105
    const-string v1, "SmartcardManagerPinService"

    const-string v2, " Show PIN Dialog"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1106
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1107
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1108
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1109
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1111
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private signMessage(J[BLjava/lang/String;)[B
    .locals 17
    .param p1, "sessionHandle"    # J
    .param p3, "data"    # [B
    .param p4, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;
        }
    .end annotation

    .prologue
    .line 1341
    :try_start_0
    new-instance v7, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;

    const-wide/16 v4, 0x6

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v7, v4, v5, v6}, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;-><init>(JLjava/lang/Object;)V

    .line 1343
    .local v7, "mechanism_rsa":Lcom/sec/smartcard/pkcs11/CK_MECHANISM;
    const-wide/16 v12, 0x3

    .line 1345
    .local v12, "privKeyClass":J
    const/4 v4, 0x2

    new-array v14, v4, [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 1346
    .local v14, "privKeyTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    const/4 v4, 0x0

    new-instance v5, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v8, 0x0

    invoke-direct {v5, v8, v9, v12, v13}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JJ)V

    aput-object v5, v14, v4

    .line 1347
    const/4 v4, 0x1

    new-instance v5, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v8, 0x3

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-direct {v5, v8, v9, v6}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JLjava/lang/Object;)V

    aput-object v5, v14, v4

    .line 1353
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1, v14}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsInit(J[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V

    .line 1355
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-wide/16 v8, 0x1

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1, v8, v9}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjects(JJ)[J

    move-result-object v11

    .line 1357
    .local v11, "pPrivKeyObject":[J
    if-eqz v11, :cond_0

    array-length v4, v11

    if-nez v4, :cond_1

    .line 1358
    :cond_0
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1359
    new-instance v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    const-string v5, "Unable to get Private key handle"

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1368
    .end local v7    # "mechanism_rsa":Lcom/sec/smartcard/pkcs11/CK_MECHANISM;
    .end local v11    # "pPrivKeyObject":[J
    .end local v12    # "privKeyClass":J
    .end local v14    # "privKeyTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    :catch_0
    move-exception v10

    .line 1369
    .local v10, "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    invoke-virtual {v10}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V

    .line 1370
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1371
    new-instance v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v10}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v4

    .line 1362
    .end local v10    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    .restart local v7    # "mechanism_rsa":Lcom/sec/smartcard/pkcs11/CK_MECHANISM;
    .restart local v11    # "pPrivKeyObject":[J
    .restart local v12    # "privKeyClass":J
    .restart local v14    # "privKeyTemplate":[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    :cond_1
    :try_start_1
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const/4 v5, 0x0

    aget-wide v8, v11, v5

    move-wide/from16 v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_SignInit(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J)V

    .line 1364
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    move-object/from16 v2, p3

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_Sign(J[B)[B

    move-result-object v15

    .line 1366
    .local v15, "signedMsg":[B
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_FindObjectsFinal(J)V
    :try_end_1
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1373
    return-object v15
.end method

.method private unregisterCard(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 8
    .param p1, "command"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 2441
    const/4 v5, 0x1

    new-array v1, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "CardCUID"

    aput-object v6, v1, v5

    .line 2442
    .local v1, "coulmns":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v5

    const-string v6, "SMRegistration"

    iget-object v7, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    invoke-virtual {v5, v6, v1, v7}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    .line 2445
    .local v2, "cv":Landroid/content/ContentValues;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v5

    if-gtz v5, :cond_1

    .line 2447
    :cond_0
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "Unable to get cardinfo details for unregister"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2449
    :try_start_0
    iget-object v5, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    const/16 v6, 0x9

    invoke-interface {v5, v6}, Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;->onComplete(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2470
    :goto_0
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V

    .line 2471
    return-void

    .line 2450
    :catch_0
    move-exception v3

    .line 2452
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 2459
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2460
    .local v0, "cardInfo":Landroid/os/Bundle;
    const-string v5, "cardInfo"

    const-string v6, "CardCUID"

    invoke-virtual {v2, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2465
    new-instance v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;

    const-string v5, "cardInfo"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p0, p1, v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;Ljava/lang/String;)V

    .line 2467
    .local v4, "rOpt":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;
    iget-object v5, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    iget-object v6, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$unregisterCardOperation;->mRegisterCallback:Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;

    invoke-direct {p0, v5, v0, v6}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->unregister_card([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;)V

    goto :goto_0
.end method

.method private unregister_card([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;)V
    .locals 5
    .param p1, "pin"    # [C
    .param p2, "cardInfo"    # Landroid/os/Bundle;
    .param p3, "callback"    # Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;

    .prologue
    .line 2476
    const-string v4, "cardInfo"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2477
    .local v3, "storedCuid":Ljava/lang/String;
    invoke-direct {p0, v3, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->verifyCardAssociation(Ljava/lang/String;[C)I

    move-result v1

    .line 2478
    .local v1, "error":I
    iput v1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 2480
    if-eqz p3, :cond_1

    .line 2485
    if-nez v1, :cond_0

    .line 2486
    :try_start_0
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v2

    .line 2487
    .local v2, "sm":Lcom/sec/smartcard/manager/SmartcardManager;
    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManager;->disconnectService()V

    .line 2489
    .end local v2    # "sm":Lcom/sec/smartcard/manager/SmartcardManager;
    :cond_0
    invoke-interface {p3, v1, p1, p2}, Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;->onComplete(I[CLandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2496
    :cond_1
    :goto_0
    return-void

    .line 2491
    :catch_0
    move-exception v0

    .line 2493
    .local v0, "e2":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private update(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 6
    .param p1, "entry"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 746
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    if-nez p1, :cond_1

    .line 748
    :cond_0
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "Queue is empty"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    :goto_0
    return-void

    .line 752
    :cond_1
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 753
    .local v3, "tempPinQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;>;"
    invoke-interface {v3, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 755
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 756
    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;>;"
    const/4 v1, 0x0

    .line 758
    .local v1, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 759
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .line 760
    .local v0, "e":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    if-lez v1, :cond_2

    .line 761
    invoke-interface {v3, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 763
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 764
    goto :goto_1

    .line 766
    .end local v0    # "e":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    :cond_3
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->clear()V

    .line 767
    sget-object v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v4, v3}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private updateAndExecuteOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 0
    .param p1, "entry"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 740
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->update(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    .line 741
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    .line 742
    return-void
.end method

.method private verifyCard(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V
    .locals 7
    .param p1, "command"    # Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .prologue
    .line 2573
    const/4 v4, 0x1

    new-array v1, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "CardCUID"

    aput-object v5, v1, v4

    .line 2574
    .local v1, "coulmns":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v4

    const-string v5, "SMRegistration"

    iget-object v6, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    invoke-virtual {v4, v5, v1, v6}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    .line 2577
    .local v2, "cv":Landroid/content/ContentValues;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 2579
    :cond_0
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "Unable to get cardinfo details for unregister"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2592
    :goto_0
    return-void

    .line 2583
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2584
    .local v0, "cardInfo":Landroid/os/Bundle;
    const-string v4, "cardInfo"

    const-string v5, "CardCUID"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2586
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;

    const-string v4, "cardInfo"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, p1, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;Ljava/lang/String;)V

    .line 2590
    .local v3, "vOpt":Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;
    iget-object v4, p1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    iget-object v5, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$verifyCardOperation;->mVerifyCallback:Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;

    invoke-direct {p0, v4, v0, v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->verify_card([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;)V

    .line 2591
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteVerifyOperation()V

    goto :goto_0
.end method

.method private declared-synchronized verifyCardAssociation(Ljava/lang/String;[C)I
    .locals 18
    .param p1, "storedCuid"    # Ljava/lang/String;
    .param p2, "pin"    # [C

    .prologue
    .line 1427
    monitor-enter p0

    const-wide/16 v10, -0x1

    .line 1428
    .local v10, "sessionHandle":J
    :try_start_0
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "verifyCardAssociation"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1429
    const/4 v6, 0x5

    .line 1431
    .local v6, "error":I
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->loginToCard([C)J

    move-result-wide v10

    .line 1434
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getCuidFromCard(J)Ljava/lang/String;

    move-result-object v3

    .line 1436
    .local v3, "cuid":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 1437
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 1438
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "VERIFY_PIN_CARDASSOCIATEERROR cuid are not matching"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1439
    const/16 v6, 0x8

    .line 1500
    :try_start_2
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "finally logoutCard with final error:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v7, v6

    .line 1503
    .end local v3    # "cuid":Ljava/lang/String;
    .end local v6    # "error":I
    .local v7, "error":I
    :goto_0
    monitor-exit p0

    return v7

    .line 1444
    .end local v7    # "error":I
    .restart local v3    # "cuid":Ljava/lang/String;
    .restart local v6    # "error":I
    :cond_0
    :try_start_3
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "VERIFY_PIN_CARDASSOCIATEERROR cuid are not found"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1445
    const/16 v6, 0x8

    .line 1500
    :try_start_4
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "finally logoutCard with final error:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v7, v6

    .end local v6    # "error":I
    .restart local v7    # "error":I
    goto :goto_0

    .line 1450
    .end local v7    # "error":I
    .restart local v6    # "error":I
    :cond_1
    const/4 v14, 0x4

    :try_start_5
    new-array v2, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "CardCUID"

    aput-object v15, v2, v14

    const/4 v14, 0x1

    const-string v15, "CertificateAlias"

    aput-object v15, v2, v14

    const/4 v14, 0x2

    const-string v15, "CertificateKeyUsage"

    aput-object v15, v2, v14

    const/4 v14, 0x3

    const-string v15, "CardCertificate"

    aput-object v15, v2, v14

    .line 1451
    .local v2, "coulmns":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v14

    const-string v15, "SMCertificates"

    invoke-virtual {v14, v15, v2, v3}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get_cert(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v4

    .line 1454
    .local v4, "cv2":Landroid/content/ContentValues;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/content/ContentValues;->size()I

    move-result v14

    if-gtz v14, :cond_3

    .line 1456
    :cond_2
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "Unable to get certificate details"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1500
    :try_start_6
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "finally logoutCard with final error:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v7, v6

    .end local v6    # "error":I
    .restart local v7    # "error":I
    goto :goto_0

    .line 1462
    .end local v7    # "error":I
    .restart local v6    # "error":I
    :cond_3
    :try_start_7
    const-string v14, "CertificateAlias"

    invoke-virtual {v4, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "CardCertificate"

    invoke-virtual {v4, v15}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getCertificate(Ljava/lang/String;[B)Ljava/security/cert/X509Certificate;

    move-result-object v9

    .line 1463
    .local v9, "sigCertificate":Ljava/security/cert/X509Certificate;
    if-eqz v9, :cond_4

    if-nez p1, :cond_6

    .line 1464
    :cond_4
    const/16 v6, 0x8

    .line 1465
    if-nez p1, :cond_5

    .line 1466
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "unable to read cuid from device"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1467
    const/4 v6, 0x5

    .line 1500
    :goto_1
    :try_start_8
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "finally logoutCard with final error:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move v7, v6

    .end local v6    # "error":I
    .restart local v7    # "error":I
    goto/16 :goto_0

    .line 1469
    .end local v7    # "error":I
    .restart local v6    # "error":I
    :cond_5
    :try_start_9
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "unable to read cert from device"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1

    .line 1492
    .end local v2    # "coulmns":[Ljava/lang/String;
    .end local v3    # "cuid":Ljava/lang/String;
    .end local v4    # "cv2":Landroid/content/ContentValues;
    .end local v9    # "sigCertificate":Ljava/security/cert/X509Certificate;
    :catch_0
    move-exception v5

    .line 1493
    .local v5, "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    :try_start_a
    invoke-virtual {v5}, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1494
    const/4 v6, 0x5

    .line 1500
    :try_start_b
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "finally logoutCard with final error:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .end local v5    # "e":Lcom/sec/smartcard/pkcs11/PKCS11Exception;
    :goto_2
    move v7, v6

    .line 1503
    .end local v6    # "error":I
    .restart local v7    # "error":I
    goto/16 :goto_0

    .line 1475
    .end local v7    # "error":I
    .restart local v2    # "coulmns":[Ljava/lang/String;
    .restart local v3    # "cuid":Ljava/lang/String;
    .restart local v4    # "cv2":Landroid/content/ContentValues;
    .restart local v6    # "error":I
    .restart local v9    # "sigCertificate":Ljava/security/cert/X509Certificate;
    :cond_6
    :try_start_c
    sget-object v14, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-wide/16 v16, 0x80

    move-wide/from16 v0, v16

    invoke-virtual {v14, v10, v11, v0, v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->C_GenerateRandom(JJ)[B

    move-result-object v8

    .line 1478
    .local v8, "random":[B
    const-string v14, "CertificateAlias"

    invoke-virtual {v4, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v8, v14}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->signMessage(J[BLjava/lang/String;)[B

    move-result-object v12

    .line 1481
    .local v12, "signedMsg":[B
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v12, v9}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->verifySignedMessage([B[BLjava/security/cert/X509Certificate;)Z

    move-result v13

    .line 1484
    .local v13, "verified":Z
    const/4 v14, 0x1

    if-ne v13, v14, :cond_7

    .line 1485
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "SmartCardService.VERIFY_PIN_SUCCESS verified"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 1486
    const/4 v6, 0x0

    .line 1500
    :goto_3
    :try_start_d
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "finally logoutCard with final error:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_2

    .line 1427
    .end local v2    # "coulmns":[Ljava/lang/String;
    .end local v3    # "cuid":Ljava/lang/String;
    .end local v4    # "cv2":Landroid/content/ContentValues;
    .end local v6    # "error":I
    .end local v8    # "random":[B
    .end local v9    # "sigCertificate":Ljava/security/cert/X509Certificate;
    .end local v12    # "signedMsg":[B
    .end local v13    # "verified":Z
    :catchall_0
    move-exception v14

    monitor-exit p0

    throw v14

    .line 1489
    .restart local v2    # "coulmns":[Ljava/lang/String;
    .restart local v3    # "cuid":Ljava/lang/String;
    .restart local v4    # "cv2":Landroid/content/ContentValues;
    .restart local v6    # "error":I
    .restart local v8    # "random":[B
    .restart local v9    # "sigCertificate":Ljava/security/cert/X509Certificate;
    .restart local v12    # "signedMsg":[B
    .restart local v13    # "verified":Z
    :cond_7
    :try_start_e
    const-string v14, "SmartcardManagerPinService"

    const-string v15, "SmartCardService.VERIFY_PIN_CARDASSOCIATEERROR not verified"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catch Lcom/sec/smartcard/pkcs11/PKCS11Exception; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 1490
    const/16 v6, 0x8

    goto :goto_3

    .line 1496
    .end local v2    # "coulmns":[Ljava/lang/String;
    .end local v3    # "cuid":Ljava/lang/String;
    .end local v4    # "cv2":Landroid/content/ContentValues;
    .end local v8    # "random":[B
    .end local v9    # "sigCertificate":Ljava/security/cert/X509Certificate;
    .end local v12    # "signedMsg":[B
    .end local v13    # "verified":Z
    :catch_1
    move-exception v5

    .line 1497
    .local v5, "e":Ljava/lang/Exception;
    const/4 v6, 0x5

    .line 1498
    :try_start_f
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 1500
    :try_start_10
    const-string v14, "SmartcardManagerPinService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "finally logoutCard with final error:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V

    goto :goto_2

    .line 1500
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v14

    const-string v15, "SmartcardManagerPinService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "finally logoutCard with final error:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1501
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->logoutCard(J[C)V

    throw v14
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0
.end method

.method private verifySignedMessage([B[BLjava/security/cert/X509Certificate;)Z
    .locals 10
    .param p1, "data"    # [B
    .param p2, "cardSignedMsg"    # [B
    .param p3, "signCert"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x5

    .line 1380
    const/4 v4, 0x0

    .line 1382
    .local v4, "ret":Z
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v5, v4

    .line 1422
    .end local v4    # "ret":Z
    .local v5, "ret":I
    :goto_0
    return v5

    .line 1386
    .end local v5    # "ret":I
    .restart local v4    # "ret":Z
    :cond_1
    const-string v6, "SHA1withRSA"

    .line 1387
    .local v6, "signAlgorithm":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v3

    .line 1393
    .local v3, "pubKey":Ljava/security/PublicKey;
    :try_start_0
    const-string v8, "BC"

    invoke-static {v6, v8}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v7

    .line 1394
    .local v7, "verifyObj":Ljava/security/Signature;
    invoke-virtual {v7, v3}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 1395
    invoke-virtual {v7, p1}, Ljava/security/Signature;->update([B)V

    .line 1396
    invoke-virtual {v7, p2}, Ljava/security/Signature;->verify([B)Z
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v4

    move v5, v4

    .line 1422
    .restart local v5    # "ret":I
    goto :goto_0

    .line 1398
    .end local v5    # "ret":I
    .end local v7    # "verifyObj":Ljava/security/Signature;
    :catch_0
    move-exception v2

    .line 1400
    .local v2, "e2":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 1401
    iput v9, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1402
    new-instance v8, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v8

    .line 1403
    .end local v2    # "e2":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v2

    .line 1405
    .local v2, "e2":Ljava/security/NoSuchProviderException;
    invoke-virtual {v2}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    .line 1406
    iput v9, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1407
    new-instance v8, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v2}, Ljava/security/NoSuchProviderException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v8

    .line 1408
    .end local v2    # "e2":Ljava/security/NoSuchProviderException;
    :catch_2
    move-exception v1

    .line 1410
    .local v1, "e1":Ljava/security/InvalidKeyException;
    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 1411
    iput v9, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1412
    new-instance v8, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v8

    .line 1413
    .end local v1    # "e1":Ljava/security/InvalidKeyException;
    :catch_3
    move-exception v0

    .line 1415
    .local v0, "e":Ljava/security/SignatureException;
    invoke-virtual {v0}, Ljava/security/SignatureException;->printStackTrace()V

    .line 1416
    const/16 v8, 0x8

    iput v8, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mLastError:I

    .line 1417
    new-instance v8, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;

    invoke-virtual {v0}, Ljava/security/SignatureException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$SmartCardException;-><init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Ljava/lang/String;)V

    throw v8
.end method

.method private verify_card([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;)V
    .locals 4
    .param p1, "pin"    # [C
    .param p2, "cardInfo"    # Landroid/os/Bundle;
    .param p3, "callback"    # Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;

    .prologue
    .line 1510
    const-string v3, "cardInfo"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1511
    .local v2, "storedCuid":Ljava/lang/String;
    invoke-direct {p0, v2, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->verifyCardAssociation(Ljava/lang/String;[C)I

    move-result v1

    .line 1513
    .local v1, "error":I
    if-eqz p3, :cond_0

    .line 1515
    :try_start_0
    invoke-interface {p3, v1}, Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;->onComplete(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1522
    :cond_0
    :goto_0
    return-void

    .line 1517
    :catch_0
    move-exception v0

    .line 1519
    .local v0, "e2":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public ExecuteOperation()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 916
    const/4 v1, 0x0

    .line 918
    .local v1, "ret":Z
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "ExecuteOperation(): "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 921
    :cond_0
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Queue is empty"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 966
    :goto_0
    :pswitch_0
    return v2

    .line 926
    :cond_1
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .line 927
    .local v0, "curOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    if-nez v0, :cond_2

    .line 928
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Error occured processing queue"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 929
    goto :goto_0

    .line 932
    :cond_2
    invoke-static {v0}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->print(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    .line 934
    iget v3, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    if-eq v3, v2, :cond_3

    iget v3, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 936
    :cond_3
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "Currently other request is in progress and new request will be queued"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    const/4 v1, 0x1

    goto :goto_0

    .line 941
    :cond_4
    iget v3, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->operation:I

    packed-switch v3, :pswitch_data_0

    .line 960
    :pswitch_1
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "invalid operation"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V

    goto :goto_0

    .line 943
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->registerCard(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    goto :goto_0

    .line 946
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->unregisterCard(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    goto :goto_0

    .line 949
    :pswitch_4
    invoke-direct {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getPin(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    goto :goto_0

    .line 955
    :pswitch_5
    invoke-direct {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getLoginAttemptRemain(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    goto :goto_0

    .line 941
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method public ExecuteVerifyOperation()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 971
    const/4 v1, 0x0

    .line 973
    .local v1, "ret":Z
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "ExecuteVerifyOperation(): "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 976
    :cond_0
    const-string v2, "SmartcardManagerPinService"

    const-string v3, " mVerifyCommandQueue Queue is empty"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1007
    :goto_0
    return v2

    .line 981
    :cond_1
    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .line 982
    .local v0, "curOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    if-nez v0, :cond_2

    .line 983
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Error occured processing queue"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 984
    goto :goto_0

    .line 987
    :cond_2
    invoke-static {v0}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->print(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    .line 989
    iget v3, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    if-eq v3, v2, :cond_3

    iget v3, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 991
    :cond_3
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "Currently other request is in progress and new request will be queued"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    const/4 v1, 0x1

    goto :goto_0

    .line 996
    :cond_4
    iget v3, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->operation:I

    packed-switch v3, :pswitch_data_0

    .line 1001
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "invalid operation"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteVerifyOperation()V

    goto :goto_0

    .line 998
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->verifyCard(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    goto :goto_0

    .line 996
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public OnSmartCardConnectionError()V
    .locals 4

    .prologue
    .line 772
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "OnSmartCardConnectionError(): "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardVerifyConnectionError()V

    .line 774
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 775
    :cond_0
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Queue is empty"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    :goto_0
    return-void

    .line 779
    :cond_1
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .line 780
    .local v0, "curOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    if-nez v0, :cond_2

    .line 781
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Error occured processing queue"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 784
    :cond_2
    iget v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    iget v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 786
    :cond_3
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Currently other request is in progress and new request will be queued"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 789
    :cond_4
    iget v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->operation:I

    packed-switch v2, :pswitch_data_0

    .line 821
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "invalid operation"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 794
    :pswitch_0
    :try_start_0
    iget-object v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;->onComplete(I)V

    .line 795
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 796
    :catch_0
    move-exception v1

    .line 797
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 803
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_1
    :try_start_1
    iget-object v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;->onComplete(I)V

    .line 804
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 805
    :catch_1
    move-exception v1

    .line 806
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 813
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_2
    :try_start_2
    iget-object v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    invoke-interface {v2}, Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;->onUserCancelled()V

    .line 814
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 815
    :catch_2
    move-exception v1

    .line 816
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 789
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public OnSmartCardInitResourceError()V
    .locals 4

    .prologue
    .line 830
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "OnSmartCardInitResourceError(): "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->OnSmartCardVerifyConnectionError()V

    .line 833
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 834
    :cond_0
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Queue is empty"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    :goto_0
    return-void

    .line 838
    :cond_1
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .line 839
    .local v0, "curOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    if-nez v0, :cond_2

    .line 840
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Error occured processing queue"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 843
    :cond_2
    iget v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    iget v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 845
    :cond_3
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Currently other request is in progress and new request will be queued"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 848
    :cond_4
    iget v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->operation:I

    packed-switch v2, :pswitch_data_0

    .line 880
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "invalid operation"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 853
    :pswitch_0
    :try_start_0
    iget-object v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    const/16 v3, 0xb

    invoke-interface {v2, v3}, Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;->onComplete(I)V

    .line 854
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 855
    :catch_0
    move-exception v1

    .line 856
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 862
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_1
    :try_start_1
    iget-object v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;->onComplete(I)V

    .line 863
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 864
    :catch_1
    move-exception v1

    .line 865
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 872
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_2
    :try_start_2
    iget-object v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    invoke-interface {v2}, Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;->onUserCancelled()V

    .line 873
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 874
    :catch_2
    move-exception v1

    .line 875
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 848
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public OnSmartCardVerifyConnectionError()V
    .locals 4

    .prologue
    .line 889
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "OnSmartCardVerifyConnectionError(): "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 892
    :cond_0
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "OnSmartCardVerifyConnectionError Queue is empty"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    :goto_0
    return-void

    .line 896
    :cond_1
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    .line 897
    .local v0, "curOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    if-nez v0, :cond_2

    .line 898
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "OnSmartCardVerifyConnectionError Error occured processing queue"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 902
    :cond_2
    :try_start_0
    iget-object v2, v0, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;

    const/4 v3, 0x4

    invoke-interface {v2, v3}, Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;->onComplete(I)V

    .line 903
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mVerifyCommandQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 904
    :catch_0
    move-exception v1

    .line 905
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method cancelNotification()V
    .locals 3

    .prologue
    .line 1176
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1178
    .local v0, "mgr":Landroid/app/NotificationManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1179
    return-void
.end method

.method public getCardLoginAttemptRemain(Lcom/sec/smartcard/pinservice/ISmartCardInfoCallback;)V
    .locals 4
    .param p1, "callback"    # Lcom/sec/smartcard/pinservice/ISmartCardInfoCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 568
    if-nez p1, :cond_0

    .line 569
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "invalid input"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    new-instance v2, Landroid/os/RemoteException;

    const-string v3, "Invalid Input"

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 573
    :cond_0
    new-instance v1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;-><init>(I)V

    .line 575
    .local v1, "smOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    iput-object p1, v1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->infoCb:Lcom/sec/smartcard/pinservice/ISmartCardInfoCallback;

    .line 576
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    iput v2, v1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    .line 581
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getActiveCardId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    .line 583
    invoke-direct {p0, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->addAndExecuteOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)Z

    move-result v0

    .line 584
    .local v0, "ret":Z
    if-nez v0, :cond_1

    .line 585
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Error occured while processing request"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    new-instance v2, Landroid/os/RemoteException;

    const-string v3, "Error occured while processing request"

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 589
    :cond_1
    return-void
.end method

.method public getPin(Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;)V
    .locals 6
    .param p1, "callback"    # Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 431
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "getPin()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    if-nez p1, :cond_0

    .line 435
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "invalid input"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    new-instance v4, Landroid/os/RemoteException;

    const-string v5, "Invalid Input"

    invoke-direct {v4, v5}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 440
    :cond_0
    move-object v0, p1

    .line 442
    .local v0, "callbck":Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;
    new-instance v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;-><init>(I)V

    .line 444
    .local v3, "smOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    iput-object v0, v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    .line 445
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    iput v4, v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    .line 446
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getActiveCardId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    .line 447
    invoke-direct {p0, v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->addAndExecuteOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)Z

    move-result v2

    .line 448
    .local v2, "ret":Z
    if-nez v2, :cond_1

    .line 449
    const-string v4, "SmartcardManagerPinService"

    const-string v5, "Error occured while processing request"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    :try_start_0
    new-instance v4, Landroid/os/RemoteException;

    const-string v5, "Error occured while processing request"

    invoke-direct {v4, v5}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    :catch_0
    move-exception v1

    .line 454
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 457
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    return-void
.end method

.method public getSlotID()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 422
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-string v4, "MyPrefsFile"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 423
    .local v0, "settings":Landroid/content/SharedPreferences;
    const-string v1, "slot_id"

    const-wide/16 v4, -0x1

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 424
    .local v2, "slot_id":J
    const-string v1, "SmartcardManagerPinService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "slot ID is = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    return-wide v2
.end method

.method handleBroadcastReceiver(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 601
    const-string v1, "SmartcardManagerPinService"

    const-string v2, "handleBroadcastReceiver:"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    const/4 v0, 0x0

    .line 603
    .local v0, "ret":Z
    if-nez p1, :cond_1

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    const-string v1, "com.sec.enterprise.mdm.sc.action.INVALIDATE_CACHE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 608
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ""

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 610
    :cond_2
    const-string v1, "SmartcardManagerPinService"

    const-string v2, "id empty"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueRemoveAllEntry()Z

    move-result v0

    .line 615
    :goto_1
    if-nez v0, :cond_0

    .line 617
    const-string v1, "SmartcardManagerPinService"

    const-string v2, "Unable to reset PIN cache timer"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 614
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueRemoveEntry(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method

.method public isCardRegistered()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 594
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->anyCardRegistered()Z

    move-result v0

    return v0
.end method

.method public isDeviceConnectedWithCard()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2731
    const/4 v0, 0x1

    return v0
.end method

.method public isSmartCardAuthenticationAvailable()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2737
    const/4 v0, 0x0

    return v0
.end method

.method public loadPkcs11(Ljava/lang/String;)V
    .locals 4
    .param p1, "socket_domain"    # Ljava/lang/String;

    .prologue
    .line 402
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "loadPkcs11 called"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-nez v0, :cond_0

    .line 404
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "loadPkcs11 mPkcs11 is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    new-instance v0, Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-direct {v0}, Lcom/sec/smartcard/pkcs11/Pkcs11;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 406
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-string v1, "libSamsungPkcs11Wrapper.so"

    const-string v2, "C_GetFunctionList"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Load(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 408
    :cond_0
    return-void
.end method

.method public onSmartCardConnected()V
    .locals 2

    .prologue
    .line 910
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "onSmartCardConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteVerifyOperation()Z

    .line 912
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    .line 913
    return-void
.end method

.method public registerCard([CLcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;)V
    .locals 7
    .param p1, "pin"    # [C
    .param p2, "callback"    # Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 464
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "registerCard "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 469
    :cond_0
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "invalid input"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    new-instance v5, Landroid/os/RemoteException;

    const-string v6, "Invalid Input"

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 474
    :cond_1
    move-object v0, p2

    .line 475
    .local v0, "callbck":Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;
    move-object v2, p1

    .line 476
    .local v2, "p":[C
    new-instance v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;-><init>(I)V

    .line 478
    .local v4, "smOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    iput-object v0, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    .line 479
    invoke-virtual {v2}, [C->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [C

    iput-object v5, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    .line 480
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    iput v5, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    .line 481
    invoke-direct {p0, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->addAndExecuteOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)Z

    move-result v3

    .line 482
    .local v3, "ret":Z
    if-nez v3, :cond_2

    .line 483
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "Error occured while processing request"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :try_start_0
    new-instance v5, Landroid/os/RemoteException;

    const-string v6, "Error occured while processing request"

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    :catch_0
    move-exception v1

    .line 488
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 492
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    return-void
.end method

.method public resetPkcs11()V
    .locals 2

    .prologue
    .line 412
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "resetPkcs11 called"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-eqz v0, :cond_0

    .line 414
    const-string v0, "SmartcardManagerPinService"

    const-string v1, "resetPkcs11 mPkcs11"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    sget-object v0, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-virtual {v0}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Unload()V

    .line 416
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 418
    :cond_0
    return-void
.end method

.method setPin([C)V
    .locals 4
    .param p1, "pin"    # [C

    .prologue
    .line 1212
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getEntry()Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    move-result-object v1

    .line 1213
    .local v1, "entry":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    if-nez v1, :cond_0

    .line 1214
    const-string v2, "SmartcardManagerPinService"

    const-string v3, "Error occured processing queue"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    :goto_0
    return-void

    .line 1218
    :cond_0
    if-nez p1, :cond_1

    .line 1220
    :try_start_0
    iget-object v2, v1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    invoke-interface {v2}, Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;->onUserCancelled()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1225
    :goto_1
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V

    goto :goto_0

    .line 1221
    :catch_0
    move-exception v0

    .line 1223
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 1228
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    iput-object p1, v1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    .line 1229
    const/4 v2, 0x2

    iput v2, v1, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->state:I

    .line 1230
    invoke-direct {p0, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->updateAndExecuteOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)V

    goto :goto_0
.end method

.method public showCardNotRegisteredDialog()V
    .locals 3

    .prologue
    .line 1011
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isCardNotRegisteredErrorDialogShown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1012
    const-string v1, "SmartcardManagerPinService"

    const-string v2, " Show Card Not Register Error Dialog"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/smartcard/pinservice/CardNotRegisteredErrorDialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1015
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1016
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1019
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public unRegisterCard([CLcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;)V
    .locals 7
    .param p1, "pin"    # [C
    .param p2, "callback"    # Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 500
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 501
    :cond_0
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "invalid input"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    new-instance v5, Landroid/os/RemoteException;

    const-string v6, "Invalid Input"

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 506
    :cond_1
    move-object v0, p2

    .line 507
    .local v0, "callbck":Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;
    move-object v2, p1

    .line 508
    .local v2, "p":[C
    new-instance v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;-><init>(I)V

    .line 510
    .local v4, "smOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    iput-object v0, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->registerCb:Lcom/sec/smartcard/pinservice/ISmartCardRegisterCallback;

    .line 511
    iput-object v2, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    .line 512
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    iput v5, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    .line 513
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getActiveCardId()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    .line 514
    invoke-direct {p0, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->addAndExecuteOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)Z

    move-result v3

    .line 515
    .local v3, "ret":Z
    if-nez v3, :cond_2

    .line 516
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "Error occured while processing request"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    :try_start_0
    new-instance v5, Landroid/os/RemoteException;

    const-string v6, "Error occured while processing request"

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 519
    :catch_0
    move-exception v1

    .line 521
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 524
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    return-void
.end method

.method public verifyCard([CLcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;)V
    .locals 7
    .param p1, "pin"    # [C
    .param p2, "callback"    # Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 532
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 533
    :cond_0
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "invalid input"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    new-instance v5, Landroid/os/RemoteException;

    const-string v6, "Invalid Input"

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 538
    :cond_1
    move-object v0, p2

    .line 539
    .local v0, "callbck":Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;
    move-object v2, p1

    .line 540
    .local v2, "p":[C
    new-instance v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    const/4 v5, 0x4

    invoke-direct {v4, v5}, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;-><init>(I)V

    .line 542
    .local v4, "smOpt":Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;
    iput-object v0, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->verifyCb:Lcom/sec/smartcard/pinservice/ISmartCardVerifyCallback;

    .line 543
    iput-object v2, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    .line 544
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    iput v5, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->appUid:I

    .line 549
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getActiveCardId()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    .line 551
    invoke-direct {p0, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->addAndExecuteVerifyOperation(Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;)Z

    move-result v3

    .line 552
    .local v3, "ret":Z
    if-nez v3, :cond_2

    .line 553
    const-string v5, "SmartcardManagerPinService"

    const-string v6, "Error occured while processing addAndExecuteVerifyOperation request"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    :try_start_0
    new-instance v5, Landroid/os/RemoteException;

    const-string v6, "Error occured while processing request"

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 556
    :catch_0
    move-exception v1

    .line 558
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 561
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    return-void
.end method
