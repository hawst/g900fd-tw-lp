.class Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;
.super Landroid/content/BroadcastReceiver;
.source "SmartcardCredentialSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 367
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.smartcard.manager.CONNECTION_SELECTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 369
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "Connections"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 370
    .local v0, "selectedConnection":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 372
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->setConnectionPreference(Ljava/lang/String;)V

    .line 374
    :cond_0
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/smartcard/manager/SmartcardManager;->disconnectService()V

    .line 376
    .end local v0    # "selectedConnection":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    # getter for: Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z
    invoke-static {v1}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->access$200(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 377
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    invoke-virtual {v1}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    # getter for: Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mConnectionSelectionReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v2}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->access$300(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 378
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$3;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    const/4 v2, 0x0

    # setter for: Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsConnectionSelectionReceiverEnabled:Z
    invoke-static {v1, v2}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->access$202(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;Z)Z

    .line 380
    :cond_2
    return-void
.end method
