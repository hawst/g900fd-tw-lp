.class Lcom/sec/smartcard/pinservice/SmartCardAssociation$1;
.super Landroid/os/Handler;
.source "SmartCardAssociation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartCardAssociation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/pinservice/SmartCardAssociation;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardAssociation;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation$1;->this$0:Lcom/sec/smartcard/pinservice/SmartCardAssociation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 57
    const-string v0, "SmartCardAssociation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage:what:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 67
    :goto_0
    return-void

    .line 63
    :pswitch_0
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardAssociation$1;->this$0:Lcom/sec/smartcard/pinservice/SmartCardAssociation;

    iget v1, p1, Landroid/os/Message;->what:I

    iget v2, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardAssociation;->handleAssociationResults(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/smartcard/pinservice/SmartCardAssociation;->access$000(Lcom/sec/smartcard/pinservice/SmartCardAssociation;II)V

    goto :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
