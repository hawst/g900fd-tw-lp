.class Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;
.super Landroid/content/BroadcastReceiver;
.source "SmartcardCredentialSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 335
    const-string v2, "SmartcardCredentialSettings_PinService"

    const-string v3, "adapter selection receiver"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.smartcard.manager.ADAPTER_SELECTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 337
    const-string v2, "SmartcardCredentialSettings_PinService"

    const-string v3, "adapter selection done"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "Adapters"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 340
    .local v1, "selectedAdapter":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 343
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    invoke-virtual {v2}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getAdapterPreference()Ljava/lang/String;

    move-result-object v0

    .line 344
    .local v0, "currentAdapter":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 345
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 347
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManager;->resetSandboxedProcessServiceConnections()V

    .line 349
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    invoke-virtual {v2}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->removeConnectionPreference()V

    .line 352
    :cond_0
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    invoke-virtual {v2}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/manager/SmartcardManagerPreferences;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/smartcard/manager/SmartcardManagerPreferences;->setAdapterPreference(Ljava/lang/String;)V

    .line 354
    .end local v0    # "currentAdapter":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/sec/smartcard/manager/SmartcardManager;->getInstance()Lcom/sec/smartcard/manager/SmartcardManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/manager/SmartcardManager;->disconnectService()V

    .line 355
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    # getter for: Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z
    invoke-static {v2}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->access$000(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 356
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    invoke-virtual {v2}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    # getter for: Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mAdapterSelectionReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->access$100(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;)Landroid/content/BroadcastReceiver;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 357
    iget-object v2, p0, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings$2;->this$0:Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;

    const/4 v3, 0x0

    # setter for: Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->mIsAdapterSelectionReceiverEnabled:Z
    invoke-static {v2, v3}, Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;->access$002(Lcom/sec/smartcard/pinservice/SmartcardCredentialSettings;Z)Z

    .line 360
    .end local v1    # "selectedAdapter":Ljava/lang/String;
    :cond_2
    return-void
.end method
