.class public Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;
.super Landroid/preference/PreferenceActivity;
.source "SmartCardViewCertificates.java"


# static fields
.field private static cert:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private static cert_alias_nm:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cert_info:Landroid/widget/TextView;

.field private mDialog:Landroid/app/AlertDialog;

.field private temp:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert_alias_nm:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->mDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private showDialog([BLjava/lang/String;)V
    .locals 7
    .param p1, "req_cert"    # [B
    .param p2, "cert_nm"    # Ljava/lang/String;

    .prologue
    .line 120
    const/4 v4, 0x0

    .line 122
    .local v4, "pubcert":Ljava/security/cert/X509Certificate;
    :try_start_0
    const-string v5, "X.509"

    invoke-static {v5}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 123
    .local v1, "certFactory":Ljava/security/cert/CertificateFactory;
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 124
    .local v3, "in":Ljava/io/InputStream;
    invoke-virtual {v1, v3}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v4, v0
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v1    # "certFactory":Ljava/security/cert/CertificateFactory;
    .end local v3    # "in":Ljava/io/InputStream;
    :goto_0
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert_info:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/security/cert/X509Certificate;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->mDialog:Landroid/app/AlertDialog;

    if-nez v5, :cond_0

    .line 137
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->temp:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->mDialog:Landroid/app/AlertDialog;

    .line 147
    :goto_1
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 149
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->mDialog:Landroid/app/AlertDialog;

    new-instance v6, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates$1;

    invoke-direct {v6, p0}, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates$1;-><init>(Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;)V

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 157
    return-void

    .line 125
    :catch_0
    move-exception v2

    .line 127
    .local v2, "e":Ljava/security/cert/CertificateException;
    invoke-virtual {v2}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_0

    .line 145
    .end local v2    # "e":Ljava/security/cert/CertificateException;
    :cond_0
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5, p2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->addPreferencesFromResource(I)V

    .line 62
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 102
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 105
    .local v1, "key":Ljava/lang/String;
    const/4 v0, 0x0

    .line 106
    .local v0, "i":I
    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert_alias_nm:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 108
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert_alias_nm:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    sget-object v2, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    sget-object v3, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert_alias_nm:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->showDialog([BLjava/lang/String;)V

    .line 106
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 68
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 70
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030004

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->temp:Landroid/view/View;

    .line 71
    iget-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->temp:Landroid/view/View;

    const v6, 0x7f070017

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert_info:Landroid/widget/TextView;

    .line 72
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    .line 73
    .local v4, "root":Landroid/preference/PreferenceScreen;
    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 75
    const/4 v5, 0x3

    new-array v0, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "CardCUID"

    aput-object v6, v0, v5

    const-string v5, "CertificateAlias"

    aput-object v5, v0, v8

    const/4 v5, 0x2

    const-string v6, "CardCertificate"

    aput-object v6, v0, v5

    .line 76
    .local v0, "columns":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->getInstance(Landroid/content/Context;)Lcom/sec/smartcard/pinservice/SmartCardStorage;

    move-result-object v5

    const-string v6, "SMCertificates"

    invoke-virtual {v5, v6, v0, v7}, Lcom/sec/smartcard/pinservice/SmartCardStorage;->get_all_card_certs_info(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 79
    .local v1, "cvList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gtz v5, :cond_2

    .line 81
    :cond_0
    const-string v5, "SmartCardViewCertificates"

    const-string v6, "Unable to get cardinfo details for getpin"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_1
    return-void

    .line 86
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 88
    sget-object v6, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert_alias_nm:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ContentValues;

    const-string v7, "CertificateAlias"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v6, Lcom/sec/smartcard/pinservice/SmartCardViewCertificates;->cert:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ContentValues;

    const-string v7, "CardCertificate"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 91
    .local v3, "p":Landroid/preference/Preference;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ContentValues;

    const-string v6, "CertificateAlias"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ContentValues;

    const-string v6, "CertificateAlias"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 94
    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 95
    invoke-virtual {v4, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 86
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
