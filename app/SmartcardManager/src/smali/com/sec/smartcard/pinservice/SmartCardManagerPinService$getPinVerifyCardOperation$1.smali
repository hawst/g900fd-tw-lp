.class Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;
.super Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback$Stub;
.source "SmartCardManagerPinService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;)V
    .locals 0

    .prologue
    .line 1532
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(I)V
    .locals 7
    .param p1, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 1537
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mSvcCommandQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$000()Ljava/util/Queue;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1538
    const-string v3, "SmartcardManagerPinService"

    const-string v4, "queue element and callback element are not matching"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1540
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-virtual {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->ExecuteOperation()Z

    .line 1583
    :cond_0
    :goto_0
    return-void

    .line 1544
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 1581
    :goto_1
    :pswitch_0
    if-eq p1, v6, :cond_0

    .line 1582
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->removeAndExecuteOpertion()V
    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$800(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)V

    goto :goto_0

    .line 1551
    :pswitch_1
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    invoke-interface {v3, p1}, Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;->onUserPinError(I)V

    .line 1554
    :pswitch_2
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mpinDialogDisplayCheckNeed:Z
    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$100(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isHomeActivityVisible()Z
    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$200(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->isCallingApplicationVisibile(I)Z
    invoke-static {v3, v4}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$300(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1558
    :cond_2
    const/4 v2, 0x0

    .line 1559
    .local v2, "showPinDialog":I
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1561
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "status"

    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->getStatusMessageId(I)I
    invoke-static {v4, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$500(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;I)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1562
    if-ne p1, v6, :cond_3

    .line 1563
    const/4 v2, 0x1

    .line 1564
    :cond_3
    const-string v3, "showPinDialog"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1565
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1566
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # getter for: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$400(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 1568
    .end local v0    # "i":Landroid/content/Intent;
    .end local v2    # "showPinDialog":I
    :cond_4
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->showNotifcation(I)V
    invoke-static {v3, p1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$600(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;I)V

    goto :goto_1

    .line 1573
    :pswitch_3
    new-instance v1, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;

    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->id:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    const/4 v5, 0x0

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;-><init>(Ljava/lang/String;[CZ)V

    .line 1575
    .local v1, "pinEntry":Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->this$0:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;

    # invokes: Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->pinQueueAddEntry(Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z
    invoke-static {v3, v1}, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;->access$700(Lcom/sec/smartcard/pinservice/SmartCardManagerPinService;Lcom/sec/smartcard/pinservice/SmartCardManagerPin;)Z

    .line 1576
    iget-object v3, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v3, v3, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->getPin:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation$1;->this$1:Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardManagerPinService$getPinVerifyCardOperation;->mVerifyOperation:Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;

    iget-object v4, v4, Lcom/sec/smartcard/pinservice/SmartCardPinServiceCommand;->pin:[C

    invoke-interface {v3, v4}, Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;->onUserEnteredPin([C)V

    goto/16 :goto_1

    .line 1544
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
