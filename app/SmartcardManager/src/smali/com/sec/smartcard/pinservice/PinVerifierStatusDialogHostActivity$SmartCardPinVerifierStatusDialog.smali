.class Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;
.super Ljava/lang/Object;
.source "PinVerifierStatusDialogHostActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartCardPinVerifierStatusDialog"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mShowPinDialog:I

.field private mStatustextId:I

.field final synthetic this$0:Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;


# direct methods
.method public constructor <init>(Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;Landroid/content/Context;II)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "msgId"    # I
    .param p4, "showPinDialog"    # I

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->this$0:Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mShowPinDialog:I

    .line 92
    iput-object p2, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mContext:Landroid/content/Context;

    .line 93
    iput p3, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mStatustextId:I

    .line 94
    iput p4, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mShowPinDialog:I

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->showDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mShowPinDialog:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->showPinDialog()V

    return-void
.end method

.method private showDialog()V
    .locals 3

    .prologue
    .line 99
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f050004

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget v1, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mStatustextId:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog$1;-><init>(Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mDialog:Landroid/app/Dialog;

    .line 116
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog$2;

    invoke-direct {v1, p0}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog$2;-><init>(Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 137
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 139
    return-void
.end method

.method private showPinDialog()V
    .locals 3

    .prologue
    .line 151
    const-string v1, "SmartCardPinVerifierStatusDialog"

    const-string v2, " Show PIN Dialog"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/smartcard/pinservice/PinDialogHostActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 153
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 154
    iget-object v1, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 156
    return-void
.end method


# virtual methods
.method public removeDialog()V
    .locals 2

    .prologue
    .line 142
    const-string v0, "SmartCardPinVerifierStatusDialog"

    const-string v1, "removeDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const-string v0, "SmartCardPinVerifierStatusDialog"

    const-string v1, "dismissDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 146
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity$SmartCardPinVerifierStatusDialog;->this$0:Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;

    invoke-virtual {v0}, Lcom/sec/smartcard/pinservice/PinVerifierStatusDialogHostActivity;->finish()V

    .line 148
    :cond_0
    return-void
.end method
