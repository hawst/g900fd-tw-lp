.class public Lcom/sec/smartcard/pinservice/SmartCardManagerPin;
.super Ljava/lang/Object;
.source "SmartCardManagerPin.java"


# instance fields
.field private mAssociationVerified:Z

.field private mEncryptedPin:[B

.field private mId:Ljava/lang/String;

.field private mKey:[B


# direct methods
.method constructor <init>(Ljava/lang/String;[CZ)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "pin"    # [C
    .param p3, "associationVerified"    # Z

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mKey:[B

    .line 53
    iput-object p1, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mId:Ljava/lang/String;

    .line 58
    :try_start_0
    const-string v2, "AES"

    invoke-static {v2}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v1

    .line 59
    .local v1, "keygenerator":Ljavax/crypto/KeyGenerator;
    invoke-virtual {v1}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v2

    iput-object v2, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mKey:[B

    .line 60
    invoke-direct {p0, p2}, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->encryptPin([C)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    .end local v1    # "keygenerator":Ljavax/crypto/KeyGenerator;
    :goto_0
    iget-boolean v2, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mAssociationVerified:Z

    iput-boolean v2, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mAssociationVerified:Z

    .line 66
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method

.method private encryptPin([C)V
    .locals 6
    .param p1, "pin"    # [C

    .prologue
    .line 188
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mKey:[B

    const-string v5, "AES"

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 192
    .local v3, "secretKey":Ljavax/crypto/spec/SecretKeySpec;
    :try_start_0
    const-string v4, "AES/ECB/PKCS5Padding"

    const-string v5, "BC"

    invoke-static {v4, v5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 195
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 196
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 197
    .local v2, "pinToEncrypt":[B
    invoke-virtual {v0, v2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    iput-object v4, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mEncryptedPin:[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_5

    .line 217
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v2    # "pinToEncrypt":[B
    :goto_0
    return-void

    .line 198
    :catch_0
    move-exception v1

    .line 200
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 201
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 203
    .local v1, "e":Ljava/security/NoSuchProviderException;
    invoke-virtual {v1}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    goto :goto_0

    .line 204
    .end local v1    # "e":Ljava/security/NoSuchProviderException;
    :catch_2
    move-exception v1

    .line 206
    .local v1, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v1}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    goto :goto_0

    .line 207
    .end local v1    # "e":Ljavax/crypto/NoSuchPaddingException;
    :catch_3
    move-exception v1

    .line 209
    .local v1, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_0

    .line 210
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :catch_4
    move-exception v1

    .line 212
    .local v1, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v1}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    goto :goto_0

    .line 213
    .end local v1    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_5
    move-exception v1

    .line 215
    .local v1, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v1}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public decryptPin()[C
    .locals 8

    .prologue
    .line 144
    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mEncryptedPin:[B

    if-nez v6, :cond_0

    .line 145
    const/4 v1, 0x0

    .line 180
    :goto_0
    return-object v1

    .line 147
    :cond_0
    const/4 v1, 0x0

    .line 149
    .local v1, "decryptPinStr":[C
    new-instance v5, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mKey:[B

    const-string v7, "AES"

    invoke-direct {v5, v6, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 152
    .local v5, "secretKey":Ljavax/crypto/spec/SecretKeySpec;
    const/4 v2, 0x0

    .line 154
    .local v2, "decryptedPin":[B
    :try_start_0
    const-string v6, "AES/ECB/PKCS5Padding"

    const-string v7, "BC"

    invoke-static {v6, v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 155
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 156
    iget-object v6, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mEncryptedPin:[B

    invoke-virtual {v0, v6}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    .line 157
    new-instance v6, Ljava/lang/String;

    const-string v7, "US-ASCII"

    invoke-direct {v6, v2, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_6

    move-result-object v1

    goto :goto_0

    .line 158
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    :catch_0
    move-exception v3

    .line 160
    .local v3, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v3}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_0

    .line 161
    .end local v3    # "e":Ljava/security/InvalidKeyException;
    :catch_1
    move-exception v4

    .line 163
    .local v4, "e1":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 164
    .end local v4    # "e1":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v4

    .line 166
    .local v4, "e1":Ljava/security/NoSuchProviderException;
    invoke-virtual {v4}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    goto :goto_0

    .line 167
    .end local v4    # "e1":Ljava/security/NoSuchProviderException;
    :catch_3
    move-exception v4

    .line 169
    .local v4, "e1":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v4}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    goto :goto_0

    .line 170
    .end local v4    # "e1":Ljavax/crypto/NoSuchPaddingException;
    :catch_4
    move-exception v3

    .line 172
    .local v3, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v3}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    goto :goto_0

    .line 173
    .end local v3    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_5
    move-exception v3

    .line 175
    .local v3, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v3}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    goto :goto_0

    .line 176
    .end local v3    # "e":Ljavax/crypto/BadPaddingException;
    :catch_6
    move-exception v3

    .line 178
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/smartcard/pinservice/SmartCardManagerPin;->mId:Ljava/lang/String;

    return-object v0
.end method
