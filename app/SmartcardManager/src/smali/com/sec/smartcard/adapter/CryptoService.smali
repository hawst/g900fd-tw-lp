.class public Lcom/sec/smartcard/adapter/CryptoService;
.super Lcom/sec/smartcard/adapter/ICryptoService$Stub;
.source "CryptoService.java"


# static fields
.field public static mAdapterPkgName:Ljava/lang/String;

.field private static mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;


# instance fields
.field private SOCKET_NAME_PREFIX:Ljava/lang/String;

.field public mAdapterCallback:Lcom/sec/smartcard/adapter/ISmartcardAdapterCallback;

.field private mCallbackList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/sec/smartcard/adapter/ICryptoServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCrypptoLibsLoaded:Z

.field private mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

.field private mListener:Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;

.field private mSlot:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    sput-object v0, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    .line 95
    sput-object v0, Lcom/sec/smartcard/adapter/CryptoService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;-><init>()V

    .line 65
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCrypptoLibsLoaded:Z

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->mSlot:I

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    .line 86
    const-string v0, "sec.socket.user."

    iput-object v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->SOCKET_NAME_PREFIX:Ljava/lang/String;

    .line 357
    new-instance v0, Lcom/sec/smartcard/adapter/CryptoService$1;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/adapter/CryptoService$1;-><init>(Lcom/sec/smartcard/adapter/CryptoService;)V

    iput-object v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterCallback:Lcom/sec/smartcard/adapter/ISmartcardAdapterCallback;

    .line 472
    new-instance v0, Lcom/sec/smartcard/adapter/CryptoService$2;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/adapter/CryptoService$2;-><init>(Lcom/sec/smartcard/adapter/CryptoService;)V

    iput-object v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->mListener:Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;

    .line 101
    iput-object p1, p0, Lcom/sec/smartcard/adapter/CryptoService;->mContext:Landroid/content/Context;

    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/adapter/CryptoService;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/adapter/CryptoService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/smartcard/adapter/CryptoService;Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/adapter/CryptoService;
    .param p1, "x1"    # Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/adapter/CryptoService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method private checkIfAdapterInstalled(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 684
    :try_start_0
    iget-object v3, p0, Lcom/sec/smartcard/adapter/CryptoService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 685
    .local v1, "pkgInfo":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 686
    const/4 v2, 0x1

    .line 691
    .end local v1    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return v2

    .line 687
    :catch_0
    move-exception v0

    .line 689
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private getConnectedAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    .locals 3

    .prologue
    .line 459
    const/4 v0, 0x0

    .line 460
    .local v0, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    sget-object v2, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 461
    sget-object v2, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/smartcard/adapter/AdapterLoader;->getAdapterWrapper(Ljava/lang/String;)Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v1

    .line 462
    .local v1, "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v1, :cond_0

    .line 463
    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v0

    .line 466
    .end local v1    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    :cond_0
    return-object v0
.end method

.method private getConnectedAdapterWrapper()Lcom/sec/smartcard/adapter/AdapterWrapper;
    .locals 2

    .prologue
    .line 448
    const/4 v0, 0x0

    .line 449
    .local v0, "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    sget-object v1, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 450
    sget-object v1, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/smartcard/adapter/AdapterLoader;->getAdapterWrapper(Ljava/lang/String;)Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v0

    .line 452
    :cond_0
    return-object v0
.end method

.method private getSocketForProcess()Ljava/lang/String;
    .locals 5

    .prologue
    .line 662
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 663
    .local v1, "user":I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 665
    .local v2, "userName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 666
    .local v0, "socket":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 667
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/smartcard/adapter/CryptoService;->SOCKET_NAME_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/smartcard/adapter/CryptoService;->mSlot:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 672
    :goto_0
    return-object v0

    .line 669
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/smartcard/adapter/CryptoService;->SOCKET_NAME_PREFIX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/smartcard/adapter/CryptoService;->mSlot:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private reloadAdapter(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V
    .locals 3
    .param p1, "adapterPkg"    # Ljava/lang/String;
    .param p2, "wrapper"    # Lcom/sec/smartcard/adapter/AdapterWrapper;

    .prologue
    .line 706
    const-string v0, "CryptoService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reloadAdapter called for adapter:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 708
    sget v0, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_READY:I

    invoke-virtual {p2, v0}, Lcom/sec/smartcard/adapter/AdapterWrapper;->setState(I)V

    .line 709
    invoke-static {p1, p2}, Lcom/sec/smartcard/adapter/AdapterLoader;->update(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 711
    :cond_0
    return-void
.end method

.method public static declared-synchronized unloadPkcs11()V
    .locals 2

    .prologue
    .line 438
    const-class v1, Lcom/sec/smartcard/adapter/CryptoService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/smartcard/adapter/CryptoService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-eqz v0, :cond_0

    .line 439
    sget-object v0, Lcom/sec/smartcard/adapter/CryptoService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-virtual {v0}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Unload()V

    .line 440
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/smartcard/adapter/CryptoService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442
    :cond_0
    monitor-exit v1

    return-void

    .line 438
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public connect(Ljava/lang/String;)Z
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 207
    const-string v4, "CryptoService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Connect called for adapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", connection: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapterWrapper()Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v1

    .line 209
    .local v1, "connectedAdapterWrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v1, :cond_2

    .line 210
    sget v4, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_DISABLED:I

    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 211
    const-string v4, "CryptoService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is disabled."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    sget-object v4, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/smartcard/adapter/CryptoService;->checkIfAdapterInstalled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 213
    sget-object v4, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-direct {p0, v4, v1}, Lcom/sec/smartcard/adapter/CryptoService;->reloadAdapter(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 219
    :cond_0
    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v2

    .line 220
    .local v2, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v2, :cond_2

    .line 221
    invoke-interface {v2, p1}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getConnection(Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;

    move-result-object v0

    .line 222
    .local v0, "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    if-eqz v0, :cond_2

    .line 223
    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->connect()Z

    move-result v3

    .line 228
    .end local v0    # "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    .end local v2    # "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :cond_1
    :goto_0
    return v3

    .line 227
    :cond_2
    const-string v4, "CryptoService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Connect called for adapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", connection: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " has failed"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public connectAvailableTransport(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Ljava/lang/String;
    .locals 7
    .param p1, "arg0"    # Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 239
    const-string v4, "CryptoService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connectAvailableTransport called for adapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    if-nez p1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-object v3

    .line 243
    :cond_1
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapterWrapper()Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v1

    .line 244
    .local v1, "connectedAdapterWrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v1, :cond_3

    .line 245
    sget v4, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_DISABLED:I

    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 246
    const-string v4, "CryptoService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is disabled."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    sget-object v4, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/smartcard/adapter/CryptoService;->checkIfAdapterInstalled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 248
    sget-object v4, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-direct {p0, v4, v1}, Lcom/sec/smartcard/adapter/CryptoService;->reloadAdapter(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 254
    :cond_2
    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v2

    .line 255
    .local v2, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v2, :cond_3

    .line 256
    const/4 v0, 0x0

    .line 258
    .local v0, "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    invoke-interface {v2}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getAvailableTransport()Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_3

    .line 260
    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, p1}, Lcom/sec/smartcard/adapter/CryptoService;->registerListener(Ljava/lang/String;Lcom/sec/smartcard/adapter/ICryptoServiceListener;)V

    .line 261
    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->connect()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 262
    const-string v3, "CryptoService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connectAvailableTransport for adapter: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "returning connection: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->getName()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 268
    .end local v0    # "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    .end local v2    # "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :cond_3
    const-string v4, "CryptoService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connectAvailableTransport for adapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "has failed."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public deinitialize()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 180
    const-string v0, "CryptoService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deinitialzing adapter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-static {}, Lcom/sec/smartcard/adapter/CryptoService;->unloadPkcs11()V

    .line 182
    const/4 v0, 0x1

    return v0
.end method

.method public deregisterListener(Ljava/lang/String;Lcom/sec/smartcard/adapter/ICryptoServiceListener;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v1

    .line 597
    .local v1, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 598
    invoke-interface {v1, p1}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getConnection(Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;

    move-result-object v0

    .line 599
    .local v0, "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    if-eqz v0, :cond_0

    .line 600
    iget-object v2, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, p2}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 601
    iget-object v2, p0, Lcom/sec/smartcard/adapter/CryptoService;->mListener:Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;

    invoke-interface {v0, v2}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->unregisterListener(Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;)V

    .line 604
    .end local v0    # "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    :cond_0
    return-void
.end method

.method public disconnect(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 280
    const-string v3, "CryptoService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "disconnect for adapter: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and connection: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapterWrapper()Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v1

    .line 283
    .local v1, "connectedAdapterWrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v1, :cond_1

    .line 284
    sget v3, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_DISABLED:I

    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 285
    const-string v3, "CryptoService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Adapter: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is disabled."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    sget-object v3, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/smartcard/adapter/CryptoService;->checkIfAdapterInstalled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 287
    sget-object v3, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-direct {p0, v3, v1}, Lcom/sec/smartcard/adapter/CryptoService;->reloadAdapter(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 292
    :cond_0
    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v2

    .line 293
    .local v2, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    .line 294
    invoke-interface {v2, p1}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getConnection(Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;

    move-result-object v0

    .line 295
    .local v0, "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    if-eqz v0, :cond_1

    .line 296
    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->disconnect()V

    .line 300
    .end local v0    # "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    .end local v2    # "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :cond_1
    return-void
.end method

.method public getConnectionInfo(Ljava/lang/String;)Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 614
    const/4 v2, 0x0

    .line 615
    .local v2, "info":Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v1

    .line 616
    .local v1, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v1, :cond_0

    .line 617
    invoke-interface {v1, p1}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getConnection(Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;

    move-result-object v0

    .line 618
    .local v0, "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    if-eqz v0, :cond_0

    .line 619
    new-instance v2, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;

    .end local v2    # "info":Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->getDetails()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    .end local v0    # "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    .restart local v2    # "info":Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
    :cond_0
    return-object v2
.end method

.method public getConnectionState(Ljava/lang/String;)I
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 310
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapterWrapper()Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v1

    .line 311
    .local v1, "connectedAdapterWrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v1, :cond_1

    .line 312
    sget v4, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_DISABLED:I

    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 313
    sget-object v4, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/smartcard/adapter/CryptoService;->checkIfAdapterInstalled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 314
    sget-object v4, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-direct {p0, v4, v1}, Lcom/sec/smartcard/adapter/CryptoService;->reloadAdapter(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 319
    :cond_0
    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v2

    .line 320
    .local v2, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v2, :cond_1

    .line 321
    invoke-interface {v2, p1}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getConnection(Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;

    move-result-object v0

    .line 322
    .local v0, "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    if-nez v0, :cond_2

    .line 328
    .end local v0    # "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    .end local v2    # "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :cond_1
    :goto_0
    return v3

    .line 325
    .restart local v0    # "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    .restart local v2    # "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :cond_2
    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->getConnectionState()I

    move-result v3

    goto :goto_0
.end method

.method public getLastUsageTimeStamp()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 631
    invoke-static {}, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;->getInstance()Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;->getLastUsageTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSupportedConnections()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v0

    .line 193
    .local v0, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v0, :cond_0

    .line 194
    invoke-interface {v0}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getSupportedConnections()Ljava/util/List;

    move-result-object v1

    .line 195
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVersion()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public initialize(Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;Ljava/lang/String;)Z
    .locals 7
    .param p1, "callback"    # Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    .param p2, "adapterName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 120
    const/4 v1, 0x0

    .line 121
    .local v1, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    const/4 v2, 0x1

    .line 122
    .local v2, "ret":Z
    iput-object p1, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    .line 124
    const-string v4, "CryptoService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Initializing adapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-static {p2}, Lcom/sec/smartcard/adapter/AdapterLoader;->getAdapterWrapper(Ljava/lang/String;)Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v3

    .line 130
    .local v3, "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v3, :cond_0

    .line 131
    invoke-virtual {v3}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v1

    .line 132
    if-eqz v1, :cond_0

    sget v4, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_DISABLED:I

    invoke-virtual {v3}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 135
    const-string v4, "CryptoService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adapter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is disabled."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    invoke-direct {p0, p2}, Lcom/sec/smartcard/adapter/CryptoService;->checkIfAdapterInstalled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 137
    invoke-direct {p0, p2, v3}, Lcom/sec/smartcard/adapter/CryptoService;->reloadAdapter(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 145
    :cond_0
    if-nez v1, :cond_1

    .line 146
    iget-object v4, p0, Lcom/sec/smartcard/adapter/CryptoService;->mContext:Landroid/content/Context;

    invoke-static {v4, p2}, Lcom/sec/smartcard/adapter/AdapterLoader;->loadSymbols(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v1

    .line 152
    :cond_1
    if-eqz v1, :cond_6

    .line 153
    sput-object p2, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    .line 154
    invoke-static {p2}, Lcom/sec/smartcard/adapter/AdapterLoader;->getAdapterWrapper(Ljava/lang/String;)Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v3

    .line 155
    if-eqz v3, :cond_2

    .line 156
    iget-object v4, p0, Lcom/sec/smartcard/adapter/CryptoService;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterCallback:Lcom/sec/smartcard/adapter/ISmartcardAdapterCallback;

    invoke-interface {v1, v4, v5}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->onInit(Landroid/content/Context;Lcom/sec/smartcard/adapter/ISmartcardAdapterCallback;)I

    move-result v0

    .line 157
    .local v0, "err":I
    if-eqz v0, :cond_4

    .line 158
    const/4 v1, 0x0

    .line 159
    const/4 v2, 0x0

    .end local v0    # "err":I
    :cond_2
    :goto_0
    move v4, v2

    .line 171
    :goto_1
    return v4

    .line 139
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    .line 161
    .restart local v0    # "err":I
    :cond_4
    sget v4, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_NONE:I

    invoke-virtual {v3}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v5

    if-ne v4, v5, :cond_5

    .line 162
    sget v4, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_READY:I

    invoke-virtual {v3, v4}, Lcom/sec/smartcard/adapter/AdapterWrapper;->setState(I)V

    .line 163
    invoke-static {p2, v3}, Lcom/sec/smartcard/adapter/AdapterLoader;->update(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 165
    :cond_5
    invoke-virtual {p0}, Lcom/sec/smartcard/adapter/CryptoService;->loadPkcs11()Z

    move-result v4

    goto :goto_1

    .line 169
    .end local v0    # "err":I
    :cond_6
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public declared-synchronized loadPkcs11()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 335
    monitor-enter p0

    :try_start_0
    const-string v3, "CryptoService"

    const-string v4, "loadPkcs11 called"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    sget-object v3, Lcom/sec/smartcard/adapter/CryptoService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-nez v3, :cond_0

    .line 337
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v1

    .line 338
    .local v1, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v1, :cond_1

    .line 339
    invoke-interface {v1}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getCryptokiInfo()Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;

    move-result-object v0

    .line 340
    .local v0, "cryptokiInfo":Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;
    if-eqz v0, :cond_1

    .line 341
    const-string v3, "CryptoService"

    const-string v4, "Got adapter cryptoki info for loading Pkcs11"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    new-instance v3, Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-direct {v3}, Lcom/sec/smartcard/pkcs11/Pkcs11;-><init>()V

    sput-object v3, Lcom/sec/smartcard/adapter/CryptoService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 343
    sget-object v3, Lcom/sec/smartcard/adapter/CryptoService;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-virtual {v0}, Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;->getLibraryPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;->getGetFunctionListMethod()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getSocketForProcess()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Load(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    .end local v0    # "cryptokiInfo":Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;
    .end local v1    # "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .restart local v1    # "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 335
    .end local v1    # "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public registerListener(Ljava/lang/String;Lcom/sec/smartcard/adapter/ICryptoServiceListener;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 576
    invoke-direct {p0}, Lcom/sec/smartcard/adapter/CryptoService;->getConnectedAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v1

    .line 577
    .local v1, "iAdapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    if-eqz v1, :cond_0

    .line 578
    invoke-interface {v1, p1}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->getConnection(Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;

    move-result-object v0

    .line 580
    .local v0, "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    if-eqz v0, :cond_0

    .line 581
    iget-object v2, p0, Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 582
    iget-object v2, p0, Lcom/sec/smartcard/adapter/CryptoService;->mListener:Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;

    invoke-interface {v0, v2}, Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;->registerListener(Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;)V

    .line 585
    .end local v0    # "conn":Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
    :cond_0
    return-void
.end method

.method protected setSandboxedSlot(I)V
    .locals 0
    .param p1, "slot"    # I

    .prologue
    .line 638
    iput p1, p0, Lcom/sec/smartcard/adapter/CryptoService;->mSlot:I

    .line 639
    return-void
.end method
