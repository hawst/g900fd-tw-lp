.class public abstract Lcom/sec/smartcard/adapter/SandboxedProcessService;
.super Landroid/app/Service;
.source "SandboxedProcessService.java"


# instance fields
.field private final mBinder:Lcom/sec/smartcard/adapter/CryptoService;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 131
    new-instance v0, Lcom/sec/smartcard/adapter/CryptoService;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/adapter/CryptoService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService;->mBinder:Lcom/sec/smartcard/adapter/CryptoService;

    .line 139
    new-instance v0, Lcom/sec/smartcard/adapter/SandboxedProcessService$1;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/adapter/SandboxedProcessService$1;-><init>(Lcom/sec/smartcard/adapter/SandboxedProcessService;)V

    iput-object v0, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/adapter/SandboxedProcessService;)Lcom/sec/smartcard/adapter/CryptoService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/adapter/SandboxedProcessService;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService;->mBinder:Lcom/sec/smartcard/adapter/CryptoService;

    return-object v0
.end method


# virtual methods
.method protected finalize()V
    .locals 0

    .prologue
    .line 119
    invoke-static {}, Lcom/sec/smartcard/adapter/CryptoService;->unloadPkcs11()V

    .line 121
    return-void
.end method

.method protected abstract getSandBoxedSlot()I
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 6
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 84
    const/4 v1, 0x0

    .line 85
    .local v1, "binder":Landroid/os/IBinder;
    if-nez p1, :cond_0

    .line 86
    const-string v3, "SandboxedProcessService"

    const-string v4, "onBind : intent is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 100
    .end local v1    # "binder":Landroid/os/IBinder;
    .local v2, "binder":Landroid/os/IBinder;
    :goto_0
    return-object v2

    .line 90
    .end local v2    # "binder":Landroid/os/IBinder;
    .restart local v1    # "binder":Landroid/os/IBinder;
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 92
    const-string v3, "SandboxedProcessService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onBind : Connected to sandboxed service using action"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v1, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService;->mBinder:Lcom/sec/smartcard/adapter/CryptoService;

    .line 94
    iget-object v3, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService;->mBinder:Lcom/sec/smartcard/adapter/CryptoService;

    invoke-virtual {p0}, Lcom/sec/smartcard/adapter/SandboxedProcessService;->getSandBoxedSlot()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/smartcard/adapter/CryptoService;->setSandboxedSlot(I)V

    :goto_1
    move-object v2, v1

    .line 100
    .end local v1    # "binder":Landroid/os/IBinder;
    .restart local v2    # "binder":Landroid/os/IBinder;
    goto :goto_0

    .line 97
    .end local v2    # "binder":Landroid/os/IBinder;
    .restart local v1    # "binder":Landroid/os/IBinder;
    :cond_1
    const-string v3, "SandboxedProcessService"

    const-string v4, "onBind : action is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 62
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 63
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 64
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 65
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/smartcard/adapter/SandboxedProcessService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 67
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 75
    iget-object v0, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/smartcard/adapter/SandboxedProcessService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 76
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 110
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
