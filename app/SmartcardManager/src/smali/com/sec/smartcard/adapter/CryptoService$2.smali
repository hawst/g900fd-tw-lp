.class Lcom/sec/smartcard/adapter/CryptoService$2;
.super Ljava/lang/Object;
.source "CryptoService.java"

# interfaces
.implements Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/adapter/CryptoService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/adapter/CryptoService;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/adapter/CryptoService;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 477
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    monitor-enter v2

    .line 478
    :try_start_0
    const-string v1, "CryptoService"

    const-string v3, "ISmartcardConnectionAdapterListener.onConnected called"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 480
    .local v0, "i":I
    :cond_0
    :goto_0
    if-lez v0, :cond_2

    .line 481
    add-int/lit8 v0, v0, -0x1

    .line 482
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    :try_start_1
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/CryptoService;->loadPkcs11()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 487
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v1, p1}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnected(Ljava/lang/String;)V

    goto :goto_0

    .line 491
    :catch_0
    move-exception v1

    goto :goto_0

    .line 489
    :cond_1
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    const/16 v3, 0x49

    invoke-interface {v1, p1, v3}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 498
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 497
    .restart local v0    # "i":I
    :cond_2
    :try_start_3
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 498
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 499
    return-void
.end method

.method public onConnectionFailed(Ljava/lang/String;I)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "err"    # I

    .prologue
    .line 504
    const-string v1, "CryptoService"

    const-string v2, "ISmartcardConnectionAdapterListener.onConnectionFailed called"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    monitor-enter v2

    .line 506
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 507
    .local v0, "i":I
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    .line 508
    add-int/lit8 v0, v0, -0x1

    .line 509
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    :try_start_1
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v1, p1, p2}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 513
    :catch_0
    move-exception v1

    goto :goto_0

    .line 519
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 520
    monitor-exit v2

    .line 521
    return-void

    .line 520
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public onConnectionProgress(Ljava/lang/String;I)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "state"    # I

    .prologue
    .line 525
    const-string v1, "CryptoService"

    const-string v2, "ISmartcardConnectionAdapterListener.onConnectionProgress called"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    monitor-enter v2

    .line 527
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 528
    .local v0, "i":I
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    .line 529
    add-int/lit8 v0, v0, -0x1

    .line 530
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 532
    :try_start_1
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v1, p1, p2}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionProgress(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 533
    :catch_0
    move-exception v1

    goto :goto_0

    .line 539
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 540
    monitor-exit v2

    .line 541
    return-void

    .line 540
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public onDisconnected(Ljava/lang/String;I)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "err"    # I

    .prologue
    .line 546
    const-string v1, "CryptoService"

    const-string v2, "ISmartcardConnectionAdapterListener.onDisconnected called"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    monitor-enter v2

    .line 548
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 549
    .local v0, "i":I
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    .line 550
    add-int/lit8 v0, v0, -0x1

    .line 551
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 553
    :try_start_1
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v1, p1, p2}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardDisconnected(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 554
    :catch_0
    move-exception v1

    goto :goto_0

    .line 560
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$2;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 561
    monitor-exit v2

    .line 562
    return-void

    .line 561
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
