.class public Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;
.super Ljava/lang/Object;
.source "SandboxedProcessUsageStatistics.java"


# static fields
.field private static mSBUsageStatistics:Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;->mSBUsageStatistics:Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method public static getInstance()Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;->mSBUsageStatistics:Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;

    invoke-direct {v0}, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;-><init>()V

    sput-object v0, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;->mSBUsageStatistics:Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;

    .line 61
    :cond_0
    sget-object v0, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;->mSBUsageStatistics:Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;

    return-object v0
.end method


# virtual methods
.method public getLastUsageTime()J
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/smartcard/adapter/SandboxedProcessUsageStatistics;->native_getLastUsageTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public native native_getLastUsageTime()J
.end method
