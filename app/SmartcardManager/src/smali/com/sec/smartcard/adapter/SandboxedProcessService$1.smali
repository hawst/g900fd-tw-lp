.class Lcom/sec/smartcard/adapter/SandboxedProcessService$1;
.super Landroid/content/BroadcastReceiver;
.source "SandboxedProcessService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/adapter/SandboxedProcessService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/adapter/SandboxedProcessService;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/adapter/SandboxedProcessService;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService$1;->this$0:Lcom/sec/smartcard/adapter/SandboxedProcessService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0x8

    .line 142
    sget-object v3, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 147
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "installedPackage":Ljava/lang/String;
    const-string v3, "SandboxedProcessService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Installed package: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-static {v0}, Lcom/sec/smartcard/adapter/AdapterLoader;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 151
    invoke-static {v0}, Lcom/sec/smartcard/adapter/AdapterLoader;->getAdapterWrapper(Ljava/lang/String;)Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v2

    .line 152
    .local v2, "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v2, :cond_2

    .line 154
    sget v3, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_DISABLED:I

    invoke-virtual {v2}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 161
    sget v3, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_READY:I

    invoke-virtual {v2, v3}, Lcom/sec/smartcard/adapter/AdapterWrapper;->setState(I)V

    .line 162
    invoke-static {v0, v2}, Lcom/sec/smartcard/adapter/AdapterLoader;->update(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 169
    .end local v0    # "installedPackage":Ljava/lang/String;
    .end local v2    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 170
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "removedPackage":Ljava/lang/String;
    const-string v3, "SandboxedProcessService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Removed Package package: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-static {v1}, Lcom/sec/smartcard/adapter/AdapterLoader;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 176
    invoke-static {v1}, Lcom/sec/smartcard/adapter/AdapterLoader;->getAdapterWrapper(Ljava/lang/String;)Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v2

    .line 177
    .restart local v2    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v2, :cond_4

    .line 179
    sget v3, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_READY:I

    invoke-virtual {v2}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v4

    if-ne v3, v4, :cond_4

    .line 180
    sget v3, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_DISABLED:I

    invoke-virtual {v2, v3}, Lcom/sec/smartcard/adapter/AdapterWrapper;->setState(I)V

    .line 182
    invoke-virtual {v2}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 183
    invoke-virtual {v2}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/smartcard/adapter/ISmartcardAdapter;->onDestroy()Z

    .line 185
    :cond_3
    invoke-static {v1, v2}, Lcom/sec/smartcard/adapter/AdapterLoader;->update(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V

    .line 192
    .end local v2    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    :cond_4
    sget-object v3, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterPkgName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 193
    iget-object v3, p0, Lcom/sec/smartcard/adapter/SandboxedProcessService$1;->this$0:Lcom/sec/smartcard/adapter/SandboxedProcessService;

    # getter for: Lcom/sec/smartcard/adapter/SandboxedProcessService;->mBinder:Lcom/sec/smartcard/adapter/CryptoService;
    invoke-static {v3}, Lcom/sec/smartcard/adapter/SandboxedProcessService;->access$000(Lcom/sec/smartcard/adapter/SandboxedProcessService;)Lcom/sec/smartcard/adapter/CryptoService;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/smartcard/adapter/CryptoService;->mAdapterCallback:Lcom/sec/smartcard/adapter/ISmartcardAdapterCallback;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/sec/smartcard/adapter/ISmartcardAdapterCallback;->onDisconnected(Ljava/lang/String;)V

    .line 194
    invoke-static {}, Lcom/sec/smartcard/adapter/CryptoService;->unloadPkcs11()V

    goto/16 :goto_0
.end method
