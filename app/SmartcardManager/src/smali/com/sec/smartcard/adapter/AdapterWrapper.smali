.class public Lcom/sec/smartcard/adapter/AdapterWrapper;
.super Ljava/lang/Object;
.source "AdapterWrapper.java"


# static fields
.field public static STATE_DISABLED:I

.field public static STATE_NONE:I

.field public static STATE_READY:I


# instance fields
.field mAdapter:Lcom/sec/smartcard/adapter/ISmartcardAdapter;

.field mAdapterState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, -0x1

    sput v0, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_NONE:I

    .line 58
    const/4 v0, 0x0

    sput v0, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_READY:I

    .line 62
    const/4 v0, 0x1

    sput v0, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_DISABLED:I

    return-void
.end method

.method constructor <init>(Lcom/sec/smartcard/adapter/ISmartcardAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/adapter/AdapterWrapper;->mAdapter:Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    .line 49
    sget v0, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_NONE:I

    iput v0, p0, Lcom/sec/smartcard/adapter/AdapterWrapper;->mAdapterState:I

    .line 69
    iput-object p1, p0, Lcom/sec/smartcard/adapter/AdapterWrapper;->mAdapter:Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    .line 70
    return-void
.end method


# virtual methods
.method public declared-synchronized getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    .locals 1

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/smartcard/adapter/AdapterWrapper;->mAdapter:Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getState()I
    .locals 1

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/smartcard/adapter/AdapterWrapper;->mAdapterState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/sec/smartcard/adapter/AdapterWrapper;->mAdapterState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
