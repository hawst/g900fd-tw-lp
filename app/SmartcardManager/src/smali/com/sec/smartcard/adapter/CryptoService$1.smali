.class Lcom/sec/smartcard/adapter/CryptoService$1;
.super Ljava/lang/Object;
.source "CryptoService.java"

# interfaces
.implements Lcom/sec/smartcard/adapter/ISmartcardAdapterCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/adapter/CryptoService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/adapter/CryptoService;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/adapter/CryptoService;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 363
    :try_start_0
    const-string v1, "CryptoService"

    const-string v2, "ISmartcardAdapterCallback.onConnected called"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    invoke-virtual {v1}, Lcom/sec/smartcard/adapter/CryptoService;->loadPkcs11()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 367
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$000(Lcom/sec/smartcard/adapter/CryptoService;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 368
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$000(Lcom/sec/smartcard/adapter/CryptoService;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;->onInitComplete()V

    .line 370
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    const/4 v2, 0x0

    # setter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v1, v2}, Lcom/sec/smartcard/adapter/CryptoService;->access$002(Lcom/sec/smartcard/adapter/CryptoService;Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$000(Lcom/sec/smartcard/adapter/CryptoService;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 374
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v1}, Lcom/sec/smartcard/adapter/CryptoService;->access$000(Lcom/sec/smartcard/adapter/CryptoService;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;->onInitFailed()V

    .line 376
    iget-object v1, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    const/4 v2, 0x0

    # setter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v1, v2}, Lcom/sec/smartcard/adapter/CryptoService;->access$002(Lcom/sec/smartcard/adapter/CryptoService;Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 379
    :catch_0
    move-exception v0

    .line 380
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDisconnected(Ljava/lang/String;)V
    .locals 8
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 387
    :try_start_0
    const-string v5, "CryptoService"

    const-string v6, "ISmartcardAdapterCallback.onDisconnected called"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v5}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    if-nez v5, :cond_1

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v5}, Lcom/sec/smartcard/adapter/CryptoService;->access$000(Lcom/sec/smartcard/adapter/CryptoService;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 394
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v5}, Lcom/sec/smartcard/adapter/CryptoService;->access$000(Lcom/sec/smartcard/adapter/CryptoService;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;->onInitFailed()V

    .line 396
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    const/4 v6, 0x0

    # setter for: Lcom/sec/smartcard/adapter/CryptoService;->mCrytoServiceInitCallback:Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-static {v5, v6}, Lcom/sec/smartcard/adapter/CryptoService;->access$002(Lcom/sec/smartcard/adapter/CryptoService;Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    .line 425
    :cond_2
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    invoke-virtual {v5}, Lcom/sec/smartcard/adapter/CryptoService;->deinitialize()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 427
    :catch_0
    move-exception v2

    .line 429
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 399
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    invoke-virtual {v5}, Lcom/sec/smartcard/adapter/CryptoService;->getSupportedConnections()Ljava/util/List;

    move-result-object v1

    .line 400
    .local v1, "connections":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 405
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 406
    .local v0, "conn":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v5}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v6

    monitor-enter v6
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 407
    :try_start_2
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v5}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v3

    .line 408
    .local v3, "i":I
    :cond_4
    :goto_2
    if-lez v3, :cond_5

    .line 409
    add-int/lit8 v3, v3, -0x1

    .line 410
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v5}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    if-eqz v5, :cond_4

    .line 412
    :try_start_3
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v5}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v5

    check-cast v5, Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    const/16 v7, 0x49

    invoke-interface {v5, v0, v7}, Lcom/sec/smartcard/adapter/ICryptoServiceListener;->onSmartCardConnectionError(Ljava/lang/String;I)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 414
    :catch_1
    move-exception v5

    goto :goto_2

    .line 420
    :cond_5
    :try_start_4
    iget-object v5, p0, Lcom/sec/smartcard/adapter/CryptoService$1;->this$0:Lcom/sec/smartcard/adapter/CryptoService;

    # getter for: Lcom/sec/smartcard/adapter/CryptoService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {v5}, Lcom/sec/smartcard/adapter/CryptoService;->access$100(Lcom/sec/smartcard/adapter/CryptoService;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 421
    monitor-exit v6

    goto :goto_1

    .end local v3    # "i":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v5
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
.end method
