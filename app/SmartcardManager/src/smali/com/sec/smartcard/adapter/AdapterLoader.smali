.class public Lcom/sec/smartcard/adapter/AdapterLoader;
.super Ljava/lang/Object;
.source "AdapterLoader.java"


# static fields
.field private static mAdapterMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/smartcard/adapter/AdapterWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/smartcard/adapter/AdapterLoader;->mAdapterMap:Ljava/util/Map;

    return-void
.end method

.method public static contains(Ljava/lang/String;)Z
    .locals 1
    .param p0, "adapterPkgName"    # Ljava/lang/String;

    .prologue
    .line 188
    if-eqz p0, :cond_0

    .line 189
    sget-object v0, Lcom/sec/smartcard/adapter/AdapterLoader;->mAdapterMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 191
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static containsInstalledAdapter(Ljava/lang/String;)Z
    .locals 3
    .param p0, "adapterPkgName"    # Ljava/lang/String;

    .prologue
    .line 199
    if-eqz p0, :cond_0

    .line 200
    sget-object v1, Lcom/sec/smartcard/adapter/AdapterLoader;->mAdapterMap:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    sget-object v1, Lcom/sec/smartcard/adapter/AdapterLoader;->mAdapterMap:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/adapter/AdapterWrapper;

    .line 202
    .local v0, "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    if-eqz v0, :cond_0

    .line 203
    sget v1, Lcom/sec/smartcard/adapter/AdapterWrapper;->STATE_READY:I

    invoke-virtual {v0}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getState()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 204
    const/4 v1, 0x1

    .line 208
    .end local v0    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getAdapterWrapper(Ljava/lang/String;)Lcom/sec/smartcard/adapter/AdapterWrapper;
    .locals 1
    .param p0, "adapterPkgName"    # Ljava/lang/String;

    .prologue
    .line 143
    if-eqz p0, :cond_0

    .line 144
    sget-object v0, Lcom/sec/smartcard/adapter/AdapterLoader;->mAdapterMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/smartcard/adapter/AdapterWrapper;

    .line 146
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getClasses(Landroid/content/pm/ApplicationInfo;)Ljava/util/List;
    .locals 6
    .param p0, "appInfo"    # Landroid/content/pm/ApplicationInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/ApplicationInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    new-instance v1, Ldalvik/system/DexFile;

    iget-object v5, p0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v1, v5}, Ldalvik/system/DexFile;-><init>(Ljava/lang/String;)V

    .line 68
    .local v1, "df":Ldalvik/system/DexFile;
    invoke-virtual {v1}, Ldalvik/system/DexFile;->entries()Ljava/util/Enumeration;

    move-result-object v3

    .line 69
    .local v3, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 70
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 71
    .local v0, "clz":Ljava/lang/String;
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 74
    .end local v0    # "clz":Ljava/lang/String;
    .end local v1    # "df":Ldalvik/system/DexFile;
    .end local v3    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :catch_0
    move-exception v2

    .line 75
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 77
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    return-object v4

    .line 73
    .restart local v1    # "df":Ldalvik/system/DexFile;
    .restart local v3    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ldalvik/system/DexFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static loadSymbols(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    .locals 16
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 86
    const/4 v1, 0x0

    .line 87
    .local v1, "adapter":Lcom/sec/smartcard/adapter/ISmartcardAdapter;
    const/4 v10, 0x0

    .line 88
    .local v10, "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    const/4 v2, 0x0

    .line 92
    .local v2, "appInfo":Landroid/content/pm/ApplicationInfo;
    invoke-static/range {p1 .. p1}, Lcom/sec/smartcard/adapter/AdapterLoader;->containsInstalledAdapter(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 93
    invoke-static/range {p1 .. p1}, Lcom/sec/smartcard/adapter/AdapterLoader;->getAdapterWrapper(Ljava/lang/String;)Lcom/sec/smartcard/adapter/AdapterWrapper;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/smartcard/adapter/AdapterWrapper;->getAdapter()Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-result-object v12

    .line 135
    :goto_0
    return-object v12

    .line 97
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 101
    :goto_1
    if-nez v2, :cond_1

    .line 102
    new-instance v12, Ljava/lang/RuntimeException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Cannot find Application Info for dependency package : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 98
    :catch_0
    move-exception v7

    .line 99
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 106
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    invoke-static {v2}, Lcom/sec/smartcard/adapter/AdapterLoader;->getClasses(Landroid/content/pm/ApplicationInfo;)Ljava/util/List;

    move-result-object v3

    .line 108
    .local v3, "classes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "_dex"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v9

    .line 110
    .local v9, "optimizedDexOutputPath":Ljava/io/File;
    new-instance v6, Ldalvik/system/DexClassLoader;

    iget-object v12, v2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    iget-object v14, v2, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v15

    invoke-direct {v6, v12, v13, v14, v15}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 115
    .local v6, "dexClassLoader":Ldalvik/system/DexClassLoader;
    const/4 v5, 0x0

    .line 116
    .local v5, "clz":Ljava/lang/Class;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 118
    .local v4, "cls":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v6, v4}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 119
    const-class v12, Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    invoke-virtual {v12, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 120
    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Lcom/sec/smartcard/adapter/ISmartcardAdapter;

    move-object v1, v0

    .line 121
    if-eqz v1, :cond_3

    .line 125
    new-instance v11, Lcom/sec/smartcard/adapter/AdapterWrapper;

    invoke-direct {v11, v1}, Lcom/sec/smartcard/adapter/AdapterWrapper;-><init>(Lcom/sec/smartcard/adapter/ISmartcardAdapter;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 126
    .end local v10    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    .local v11, "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    :try_start_2
    sget-object v12, Lcom/sec/smartcard/adapter/AdapterLoader;->mAdapterMap:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v12, v0, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    move-object v10, v11

    .end local v4    # "cls":Ljava/lang/String;
    .end local v11    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    .restart local v10    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    :cond_3
    move-object v12, v1

    .line 135
    goto/16 :goto_0

    .line 130
    .restart local v4    # "cls":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 131
    .local v7, "e":Ljava/lang/Throwable;
    :goto_3
    invoke-virtual {v7}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    .line 130
    .end local v7    # "e":Ljava/lang/Throwable;
    .end local v10    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    .restart local v11    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    :catch_2
    move-exception v7

    move-object v10, v11

    .end local v11    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    .restart local v10    # "wrapper":Lcom/sec/smartcard/adapter/AdapterWrapper;
    goto :goto_3
.end method

.method public static update(Ljava/lang/String;Lcom/sec/smartcard/adapter/AdapterWrapper;)V
    .locals 1
    .param p0, "adapterPkgName"    # Ljava/lang/String;
    .param p1, "wrapper"    # Lcom/sec/smartcard/adapter/AdapterWrapper;

    .prologue
    .line 215
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 216
    sget-object v0, Lcom/sec/smartcard/adapter/AdapterLoader;->mAdapterMap:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_0
    return-void
.end method
