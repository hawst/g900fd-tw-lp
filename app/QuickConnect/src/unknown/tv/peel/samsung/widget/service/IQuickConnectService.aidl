package tv.peel.samsung.widget.service;
import tv.peel.samsung.widget.service.Room;
import tv.peel.samsung.widget.service.Device;
import tv.peel.samsung.widget.service.Command;

interface IQuickConnectService {
    Room getCurrentRoom();
    List<Device> getRemoteControlList(String roomId);
    Room[] getRoomList();
    void sendRemocon(String roomid, in Device device, in Command command);
    boolean isRemoteControlSetuped(String roomid);
}