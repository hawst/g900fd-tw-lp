package com.sec.msc.android.yosemite.service.remocon;

import java.util.List;
import com.sec.msc.android.yosemite.infrastructure.model.remotecontrol.RemoteControlSetting;
import com.sec.msc.android.yosemite.infrastructure.model.room.Room;

interface IBasicRemoconService
{
	// Remocon
	void sendRemocon(String roomId, String deviceTypeString, String keyString, boolean isLongClick);
	boolean isRemoteControlSetuped(String roomId);
	
	List<RemoteControlSetting> getRemoteControlList(String roomId);
	
	// Room
	List<Room> getRoomList();
	Room getCurrentRoom();
}