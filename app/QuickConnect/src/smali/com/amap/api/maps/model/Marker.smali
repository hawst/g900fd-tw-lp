.class public final Lcom/amap/api/maps/model/Marker;
.super Ljava/lang/Object;
.source "Marker.java"


# instance fields
.field private a:Lcom/amap/api/mapcore/v;


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/v;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    .line 22
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->p()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 250
    instance-of v0, p1, Lcom/amap/api/maps/model/Marker;

    if-nez v0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 254
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    check-cast p1, Lcom/amap/api/maps/model/Marker;

    iget-object v1, p1, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/v;->a(Lcom/amap/api/mapcore/v;)Z

    move-result v0

    goto :goto_0
.end method

.method public getIcons()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps/model/BitmapDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->v()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->r()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getPeriod()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->u()I

    move-result v0

    return v0
.end method

.method public getPosition()Lcom/amap/api/maps/model/LatLng;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->d()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->q()I

    move-result v0

    return v0
.end method

.method public hideInfoWindow()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->m()V

    .line 220
    return-void
.end method

.method public isDraggable()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->k()Z

    move-result v0

    return v0
.end method

.method public isInfoWindowShown()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->n()Z

    move-result v0

    return v0
.end method

.method public isPerspective()Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->t()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->o()Z

    move-result v0

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 67
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->a()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public setAnchor(FF)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1, p2}, Lcom/amap/api/mapcore/v;->a(FF)V

    .line 194
    return-void
.end method

.method public setDraggable(Z)V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(Z)V

    .line 198
    return-void
.end method

.method public setIcon(Lcom/amap/api/maps/model/BitmapDescriptor;)V
    .locals 1

    .prologue
    .line 178
    if-eqz p1, :cond_0

    .line 179
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(Lcom/amap/api/maps/model/BitmapDescriptor;)V

    .line 181
    :cond_0
    return-void
.end method

.method public setIcons(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps/model/BitmapDescriptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(Ljava/util/ArrayList;)V

    .line 48
    return-void
.end method

.method public setObject(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(Ljava/lang/Object;)V

    .line 269
    return-void
.end method

.method public setPeriod(I)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(I)V

    .line 34
    return-void
.end method

.method public setPerspective(Z)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->c(Z)V

    .line 107
    return-void
.end method

.method public setPosition(Lcom/amap/api/maps/model/LatLng;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 122
    return-void
.end method

.method public setRotateAngle(F)V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(F)V

    .line 283
    return-void
.end method

.method public setSnippet(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->b(Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->a(Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public setVisible(Z)V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/v;->b(Z)V

    .line 242
    return-void
.end method

.method public showInfoWindow()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->l()V

    .line 211
    return-void
.end method
