.class public final Lcom/amap/api/maps/AMap;
.super Ljava/lang/Object;
.source "AMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/maps/AMap$OnMapScreenShotListener;,
        Lcom/amap/api/maps/AMap$onMapPrintScreenListener;,
        Lcom/amap/api/maps/AMap$OnMapLoadedListener;,
        Lcom/amap/api/maps/AMap$OnMapClickListener;,
        Lcom/amap/api/maps/AMap$OnMapLongClickListener;,
        Lcom/amap/api/maps/AMap$OnCameraChangeListener;,
        Lcom/amap/api/maps/AMap$OnMarkerClickListener;,
        Lcom/amap/api/maps/AMap$OnMarkerDragListener;,
        Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;,
        Lcom/amap/api/maps/AMap$CancelableCallback;,
        Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;,
        Lcom/amap/api/maps/AMap$InfoWindowAdapter;
    }
.end annotation


# static fields
.field public static final MAP_TYPE_NORMAL:I = 0x1

.field public static final MAP_TYPE_SATELLITE:I = 0x2


# instance fields
.field private final a:Lcom/amap/api/mapcore/r;

.field private b:Lcom/amap/api/maps/UiSettings;

.field private c:Lcom/amap/api/maps/Projection;

.field private d:Lcom/amap/api/maps/model/MyTrafficStyle;


# direct methods
.method protected constructor <init>(Lcom/amap/api/mapcore/r;)V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-object p1, p0, Lcom/amap/api/maps/AMap;->a:Lcom/amap/api/mapcore/r;

    .line 233
    return-void
.end method


# virtual methods
.method a()Lcom/amap/api/mapcore/r;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->a:Lcom/amap/api/mapcore/r;

    return-object v0
.end method

.method public final addCircle(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/maps/model/Circle;
    .locals 2

    .prologue
    .line 362
    :try_start_0
    new-instance v0, Lcom/amap/api/maps/model/Circle;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/s;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/Circle;-><init>(Lcom/amap/api/mapcore/s;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 363
    :catch_0
    move-exception v0

    .line 364
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addGroundOverlay(Lcom/amap/api/maps/model/GroundOverlayOptions;)Lcom/amap/api/maps/model/GroundOverlay;
    .locals 2

    .prologue
    .line 394
    :try_start_0
    new-instance v0, Lcom/amap/api/maps/model/GroundOverlay;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/GroundOverlayOptions;)Lcom/amap/api/mapcore/t;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/GroundOverlay;-><init>(Lcom/amap/api/mapcore/t;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 395
    :catch_0
    move-exception v0

    .line 396
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addMarker(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;
    .locals 2

    .prologue
    .line 415
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 416
    :catch_0
    move-exception v0

    .line 417
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addPolygon(Lcom/amap/api/maps/model/PolygonOptions;)Lcom/amap/api/maps/model/Polygon;
    .locals 2

    .prologue
    .line 378
    :try_start_0
    new-instance v0, Lcom/amap/api/maps/model/Polygon;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/PolygonOptions;)Lcom/amap/api/mapcore/x;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/Polygon;-><init>(Lcom/amap/api/mapcore/x;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 379
    :catch_0
    move-exception v0

    .line 380
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addPolyline(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/maps/model/Polyline;
    .locals 2

    .prologue
    .line 347
    :try_start_0
    new-instance v0, Lcom/amap/api/maps/model/Polyline;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/mapcore/y;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/Polyline;-><init>(Lcom/amap/api/mapcore/y;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 348
    :catch_0
    move-exception v0

    .line 349
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addTileOverlay(Lcom/amap/api/maps/model/TileOverlayOptions;)Lcom/amap/api/maps/model/TileOverlay;
    .locals 2

    .prologue
    .line 432
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/TileOverlayOptions;)Lcom/amap/api/maps/model/TileOverlay;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 433
    :catch_0
    move-exception v0

    .line 434
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps/CameraUpdate;)V
    .locals 2

    .prologue
    .line 297
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/CameraUpdate;->a()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->b(Lcom/amap/api/mapcore/i;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    return-void

    .line 299
    :catch_0
    move-exception v0

    .line 300
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps/CameraUpdate;JLcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 2

    .prologue
    .line 319
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string v1, "durationMs must be positive"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/a;->b(ZLjava/lang/Object;)V

    .line 321
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/CameraUpdate;->a()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    return-void

    .line 319
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps/CameraUpdate;Lcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 2

    .prologue
    .line 307
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/CameraUpdate;->a()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    return-void

    .line 311
    :catch_0
    move-exception v0

    .line 312
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 445
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->r()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 449
    return-void

    .line 446
    :catch_0
    move-exception v0

    .line 447
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;
    .locals 2

    .prologue
    .line 247
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->n()Lcom/amap/api/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 248
    :catch_0
    move-exception v0

    .line 249
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getMapPrintScreen(Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)V
    .locals 1

    .prologue
    .line 840
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)V

    .line 841
    return-void
.end method

.method public final getMapScreenMarkers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->I()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMapScreenShot(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V
    .locals 1

    .prologue
    .line 844
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V

    .line 845
    return-void
.end method

.method public final getMapTextZIndex()I
    .locals 1

    .prologue
    .line 894
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->N()I

    move-result v0

    return v0
.end method

.method public final getMapType()I
    .locals 2

    .prologue
    .line 459
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->s()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 460
    :catch_0
    move-exception v0

    .line 461
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final getMaxZoomLevel()F
    .locals 1

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->o()F

    move-result v0

    return v0
.end method

.method public final getMinZoomLevel()F
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->p()F

    move-result v0

    return v0
.end method

.method public final getMyLocation()Landroid/location/Location;
    .locals 2

    .prologue
    .line 588
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->v()Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 589
    :catch_0
    move-exception v0

    .line 590
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getMyTrafficStyle()Lcom/amap/api/maps/model/MyTrafficStyle;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->d:Lcom/amap/api/maps/model/MyTrafficStyle;

    return-object v0
.end method

.method public final getProjection()Lcom/amap/api/maps/Projection;
    .locals 2

    .prologue
    .line 665
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->c:Lcom/amap/api/maps/Projection;

    if-nez v0, :cond_0

    .line 666
    new-instance v0, Lcom/amap/api/maps/Projection;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/r;->x()Lcom/amap/api/mapcore/z;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/Projection;-><init>(Lcom/amap/api/mapcore/z;)V

    iput-object v0, p0, Lcom/amap/api/maps/AMap;->c:Lcom/amap/api/maps/Projection;

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->c:Lcom/amap/api/maps/Projection;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 670
    :catch_0
    move-exception v0

    .line 671
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getScalePerPixel()F
    .locals 1

    .prologue
    .line 848
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->G()F

    move-result v0

    return v0
.end method

.method public final getUiSettings()Lcom/amap/api/maps/UiSettings;
    .locals 2

    .prologue
    .line 646
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->b:Lcom/amap/api/maps/UiSettings;

    if-nez v0, :cond_0

    .line 647
    new-instance v0, Lcom/amap/api/maps/UiSettings;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/r;->w()Lcom/amap/api/mapcore/ac;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/UiSettings;-><init>(Lcom/amap/api/mapcore/ac;)V

    iput-object v0, p0, Lcom/amap/api/maps/AMap;->b:Lcom/amap/api/maps/UiSettings;

    .line 650
    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->b:Lcom/amap/api/maps/UiSettings;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 651
    :catch_0
    move-exception v0

    .line 652
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 903
    const-string v0, "V2.2.0.1"

    return-object v0
.end method

.method public final isMyLocationEnabled()Z
    .locals 2

    .prologue
    .line 554
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->u()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 555
    :catch_0
    move-exception v0

    .line 556
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final isTrafficEnabled()Z
    .locals 2

    .prologue
    .line 489
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->t()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 490
    :catch_0
    move-exception v0

    .line 491
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final moveCamera(Lcom/amap/api/maps/CameraUpdate;)V
    .locals 2

    .prologue
    .line 288
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/CameraUpdate;->a()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/mapcore/i;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 291
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public runOnDrawFrame()V
    .locals 2

    .prologue
    .line 857
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 858
    return-void
.end method

.method public setCustomRenderer(Lcom/amap/api/maps/CustomRenderer;)V
    .locals 1

    .prologue
    .line 869
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/CustomRenderer;)V

    .line 870
    return-void
.end method

.method public final setInfoWindowAdapter(Lcom/amap/api/maps/AMap$InfoWindowAdapter;)V
    .locals 2

    .prologue
    .line 801
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$InfoWindowAdapter;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 805
    return-void

    .line 802
    :catch_0
    move-exception v0

    .line 803
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setLocationSource(Lcom/amap/api/maps/LocationSource;)V
    .locals 2

    .prologue
    .line 603
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/LocationSource;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 607
    return-void

    .line 604
    :catch_0
    move-exception v0

    .line 605
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMapTextZIndex(I)V
    .locals 1

    .prologue
    .line 886
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->e(I)V

    .line 887
    return-void
.end method

.method public final setMapType(I)V
    .locals 2

    .prologue
    .line 475
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    return-void

    .line 476
    :catch_0
    move-exception v0

    .line 477
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationEnabled(Z)V
    .locals 2

    .prologue
    .line 572
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->g(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 576
    return-void

    .line 573
    :catch_0
    move-exception v0

    .line 574
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationRotateAngle(F)V
    .locals 2

    .prologue
    .line 632
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 636
    return-void

    .line 633
    :catch_0
    move-exception v0

    .line 634
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationStyle(Lcom/amap/api/maps/model/MyLocationStyle;)V
    .locals 2

    .prologue
    .line 618
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/MyLocationStyle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 622
    return-void

    .line 619
    :catch_0
    move-exception v0

    .line 620
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setMyTrafficStyle(Lcom/amap/api/maps/model/MyTrafficStyle;)V
    .locals 5

    .prologue
    .line 516
    iput-object p1, p0, Lcom/amap/api/maps/AMap;->d:Lcom/amap/api/maps/model/MyTrafficStyle;

    .line 517
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MyTrafficStyle;->getSmoothColor()I

    move-result v1

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MyTrafficStyle;->getSlowColor()I

    move-result v2

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MyTrafficStyle;->getCongestedColor()I

    move-result v3

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MyTrafficStyle;->getSeriousCongestedColor()I

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/amap/api/mapcore/r;->a(IIII)V

    .line 520
    return-void
.end method

.method public final setOnCameraChangeListener(Lcom/amap/api/maps/AMap$OnCameraChangeListener;)V
    .locals 2

    .prologue
    .line 685
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnCameraChangeListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 689
    return-void

    .line 686
    :catch_0
    move-exception v0

    .line 687
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnInfoWindowClickListener(Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;)V
    .locals 2

    .prologue
    .line 781
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 785
    return-void

    .line 782
    :catch_0
    move-exception v0

    .line 783
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapClickListener(Lcom/amap/api/maps/AMap$OnMapClickListener;)V
    .locals 2

    .prologue
    .line 700
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnMapClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 704
    return-void

    .line 701
    :catch_0
    move-exception v0

    .line 702
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapLoadedListener(Lcom/amap/api/maps/AMap$OnMapLoadedListener;)V
    .locals 2

    .prologue
    .line 814
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnMapLoadedListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 818
    return-void

    .line 815
    :catch_0
    move-exception v0

    .line 816
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapLongClickListener(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V
    .locals 2

    .prologue
    .line 733
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 737
    return-void

    .line 734
    :catch_0
    move-exception v0

    .line 735
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMarkerClickListener(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V
    .locals 2

    .prologue
    .line 750
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 754
    return-void

    .line 751
    :catch_0
    move-exception v0

    .line 752
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMarkerDragListener(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V
    .locals 2

    .prologue
    .line 764
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 768
    return-void

    .line 765
    :catch_0
    move-exception v0

    .line 766
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMyLocationChangeListener(Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;)V
    .locals 2

    .prologue
    .line 716
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 720
    return-void

    .line 717
    :catch_0
    move-exception v0

    .line 718
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setPointToCenter(II)V
    .locals 1

    .prologue
    .line 878
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/amap/api/mapcore/r;->a(II)V

    .line 879
    return-void
.end method

.method public setTrafficEnabled(Z)V
    .locals 2

    .prologue
    .line 504
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->f(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    return-void

    .line 505
    :catch_0
    move-exception v0

    .line 506
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final stopAnimation()V
    .locals 2

    .prologue
    .line 331
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->q()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    return-void

    .line 332
    :catch_0
    move-exception v0

    .line 333
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
