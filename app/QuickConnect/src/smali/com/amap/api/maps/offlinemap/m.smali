.class Lcom/amap/api/maps/offlinemap/m;
.super Ljava/lang/Object;
.source "UpdateItem.java"


# instance fields
.field public a:I

.field b:J

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:J

.field private j:J

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Z

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x3

    iput v0, p0, Lcom/amap/api/maps/offlinemap/m;->a:I

    .line 11
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->c:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->d:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->g:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->h:Ljava/lang/String;

    .line 17
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->i:J

    .line 18
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->j:J

    .line 22
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->b:J

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/m;->n:Z

    .line 63
    return-void
.end method

.method public constructor <init>(Lcom/amap/api/maps/offlinemap/OfflineMapCity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x3

    iput v0, p0, Lcom/amap/api/maps/offlinemap/m;->a:I

    .line 11
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->c:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->d:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->g:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->h:Ljava/lang/String;

    .line 17
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->i:J

    .line 18
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->j:J

    .line 22
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->b:J

    .line 23
    iput-boolean v4, p0, Lcom/amap/api/maps/offlinemap/m;->n:Z

    .line 27
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapCity;->getCity()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->c:Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapCity;->getAdcode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    .line 29
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapCity;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->d:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapCity;->getSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/amap/api/maps/offlinemap/m;->j:J

    .line 31
    invoke-virtual {p0}, Lcom/amap/api/maps/offlinemap/m;->a()V

    .line 32
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapCity;->getVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->g:Ljava/lang/String;

    .line 33
    iput-boolean v4, p0, Lcom/amap/api/maps/offlinemap/m;->n:Z

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/amap/api/maps/offlinemap/OfflineMapProvince;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x3

    iput v0, p0, Lcom/amap/api/maps/offlinemap/m;->a:I

    .line 11
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->c:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->d:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->g:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->h:Ljava/lang/String;

    .line 17
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->i:J

    .line 18
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->j:J

    .line 22
    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->b:J

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/m;->n:Z

    .line 37
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapProvince;->getProvinceName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->c:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapProvince;->getProvinceCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapProvince;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->d:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapProvince;->getSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/amap/api/maps/offlinemap/m;->j:J

    .line 41
    invoke-virtual {p0}, Lcom/amap/api/maps/offlinemap/m;->a()V

    .line 42
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/OfflineMapProvince;->getVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->g:Ljava/lang/String;

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/m;->n:Z

    .line 44
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    .line 47
    invoke-static {}, Lcom/amap/api/maps/offlinemap/d;->a()Ljava/lang/String;

    move-result-object v0

    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".zip"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    .line 50
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".zip"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".tmp"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 126
    iput p1, p0, Lcom/amap/api/maps/offlinemap/m;->m:I

    .line 127
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 138
    iput-wide p1, p0, Lcom/amap/api/maps/offlinemap/m;->i:J

    .line 139
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/amap/api/maps/offlinemap/m;->k:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 170
    iput p1, p0, Lcom/amap/api/maps/offlinemap/m;->o:I

    .line 171
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/amap/api/maps/offlinemap/m;->l:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->g:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 180
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 181
    const-string v1, "file"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 182
    if-nez v0, :cond_0

    .line 201
    :goto_0
    return-void

    .line 185
    :cond_0
    const-string v1, "title"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->c:Ljava/lang/String;

    .line 186
    const-string v1, "code"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    .line 187
    const-string v1, "url"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->d:Ljava/lang/String;

    .line 188
    const-string v1, "fileName"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    .line 189
    const-string v1, "lLocalLength"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->i:J

    .line 190
    const-string v1, "lRemoteLength"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->j:J

    .line 191
    const-string v1, "mState"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/amap/api/maps/offlinemap/m;->a:I

    .line 192
    const-string v1, "Schedule"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/amap/api/maps/offlinemap/m;->b:J

    .line 193
    const-string v1, "version"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->g:Ljava/lang/String;

    .line 194
    const-string v1, "localPath"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->l:Ljava/lang/String;

    .line 195
    const-string v1, "vMapFileNames"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/m;->k:Ljava/lang/String;

    .line 196
    const-string v1, "isSheng"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/amap/api/maps/offlinemap/m;->n:Z

    .line 197
    const-string v1, "mCompleteCode"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/amap/api/maps/offlinemap/m;->o:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/m;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/amap/api/maps/offlinemap/m;->j:J

    return-wide v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/amap/api/maps/offlinemap/m;->m:I

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/amap/api/maps/offlinemap/m;->n:Z

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/amap/api/maps/offlinemap/m;->o:I

    return v0
.end method

.method public k()V
    .locals 6

    .prologue
    .line 204
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 207
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 208
    const-string v2, "title"

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 209
    const-string v2, "code"

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 210
    const-string v2, "url"

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 211
    const-string v2, "fileName"

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 212
    const-string v2, "lLocalLength"

    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/m;->i:J

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 213
    const-string v2, "lRemoteLength"

    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/m;->j:J

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 214
    const-string v2, "mState"

    iget v3, p0, Lcom/amap/api/maps/offlinemap/m;->a:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 215
    const-string v2, "Schedule"

    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/m;->b:J

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 216
    const-string v2, "version"

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 217
    const-string v2, "localPath"

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->l:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 218
    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/m;->k:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 219
    const-string v2, "vMapFileNames"

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 222
    :cond_0
    const-string v2, "isSheng"

    iget-boolean v3, p0, Lcom/amap/api/maps/offlinemap/m;->n:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 223
    const-string v2, "mCompleteCode"

    iget v3, p0, Lcom/amap/api/maps/offlinemap/m;->o:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 224
    const-string v2, "file"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 225
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/m;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".dt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 228
    :try_start_1
    new-instance v2, Ljava/io/FileWriter;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    .line 229
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 230
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 237
    :goto_0
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 234
    :catch_1
    move-exception v0

    .line 235
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
