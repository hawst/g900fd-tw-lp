.class public final Lcom/amap/api/maps/MapsInitializer;
.super Ljava/lang/Object;
.source "MapsInitializer.java"


# static fields
.field private static a:Z

.field public static amapResourceDir:Ljava/lang/String;

.field public static sdcardDir:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, ""

    sput-object v0, Lcom/amap/api/maps/MapsInitializer;->sdcardDir:Ljava/lang/String;

    .line 17
    const/4 v0, 0x1

    sput-boolean v0, Lcom/amap/api/maps/MapsInitializer;->a:Z

    .line 18
    const-string v0, ""

    sput-object v0, Lcom/amap/api/maps/MapsInitializer;->amapResourceDir:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 50
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 51
    invoke-static {v3}, Lcom/amap/api/maps/MapsInitializer;->a(Ljava/io/File;)V

    .line 52
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 57
    :cond_1
    return-void
.end method

.method public static getNetWorkEnable()Z
    .locals 1

    .prologue
    .line 36
    sget-boolean v0, Lcom/amap/api/maps/MapsInitializer;->a:Z

    return v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    .line 14
    return-void
.end method

.method public static removecache(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 41
    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 43
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/amap/api/maps/MapsInitializer;->a(Ljava/io/File;)V

    .line 44
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/amap/api/maps/MapsInitializer;->a(Ljava/io/File;)V

    .line 45
    return-void
.end method

.method public static setNetWorkEnable(Z)V
    .locals 0

    .prologue
    .line 27
    sput-boolean p0, Lcom/amap/api/maps/MapsInitializer;->a:Z

    .line 28
    return-void
.end method
