.class public final Lcom/amap/api/maps/model/TileOverlay;
.super Ljava/lang/Object;
.source "TileOverlay.java"


# instance fields
.field private a:Lcom/amap/api/mapcore/ab;


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/ab;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    .line 10
    return-void
.end method


# virtual methods
.method public clearTileCache()V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->b()V

    .line 18
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    iget-object v1, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ab;->a(Lcom/amap/api/mapcore/ab;)Z

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getZIndex()F
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->d()F

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->f()I

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->e()Z

    move-result v0

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0}, Lcom/amap/api/mapcore/ab;->a()V

    .line 14
    return-void
.end method

.method public setVisible(Z)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/ab;->a(Z)V

    .line 34
    return-void
.end method

.method public setZIndex(F)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/amap/api/maps/model/TileOverlay;->a:Lcom/amap/api/mapcore/ab;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/ab;->a(F)V

    .line 26
    return-void
.end method
