.class Lcom/amap/api/mapcore/b/g;
.super Ljava/lang/Object;
.source "DiskLruCache.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/b/f;


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/b/f;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/amap/api/mapcore/b/g;->a:Lcom/amap/api/mapcore/b/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 274
    iget-object v1, p0, Lcom/amap/api/mapcore/b/g;->a:Lcom/amap/api/mapcore/b/f;

    monitor-enter v1

    .line 275
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b/g;->a:Lcom/amap/api/mapcore/b/f;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f;->a(Lcom/amap/api/mapcore/b/f;)Ljava/io/Writer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 276
    monitor-exit v1

    .line 284
    :goto_0
    return-object v3

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b/g;->a:Lcom/amap/api/mapcore/b/f;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f;->b(Lcom/amap/api/mapcore/b/f;)V

    .line 279
    iget-object v0, p0, Lcom/amap/api/mapcore/b/g;->a:Lcom/amap/api/mapcore/b/f;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f;->c(Lcom/amap/api/mapcore/b/f;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/amap/api/mapcore/b/g;->a:Lcom/amap/api/mapcore/b/f;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f;->d(Lcom/amap/api/mapcore/b/f;)V

    .line 281
    iget-object v0, p0, Lcom/amap/api/mapcore/b/g;->a:Lcom/amap/api/mapcore/b/f;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/amap/api/mapcore/b/f;->a(Lcom/amap/api/mapcore/b/f;I)I

    .line 283
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b/g;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
