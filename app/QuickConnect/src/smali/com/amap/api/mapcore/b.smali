.class Lcom/amap/api/mapcore/b;
.super Landroid/opengl/GLSurfaceView;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;
.implements Lcom/amap/api/mapcore/r;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/mapcore/b$a;,
        Lcom/amap/api/mapcore/b$d;,
        Lcom/amap/api/mapcore/b$b;,
        Lcom/amap/api/mapcore/b$c;,
        Lcom/amap/api/mapcore/b$e;,
        Lcom/amap/api/mapcore/b$f;
    }
.end annotation


# static fields
.field private static final aq:D

.field private static au:Landroid/os/Handler;


# instance fields
.field private A:Lcom/amap/api/mapcore/au;

.field private B:Lcom/amap/api/mapcore/ad;

.field private C:Lcom/amap/api/mapcore/k;

.field private D:Lcom/amap/api/mapcore/ap;

.field private E:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

.field private F:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

.field private G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

.field private H:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

.field private I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

.field private J:Lcom/amap/api/maps/AMap$OnMapClickListener;

.field private K:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

.field private L:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

.field private M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

.field private N:Landroid/view/View;

.field private O:Lcom/amap/api/mapcore/v;

.field private P:Lcom/amap/api/mapcore/z;

.field private Q:Lcom/amap/api/mapcore/ac;

.field private R:Lcom/amap/api/maps/LocationSource;

.field private S:Landroid/graphics/Rect;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Lcom/amap/api/mapcore/f;

.field private X:Lcom/amap/api/mapcore/a/b;

.field private Y:Lcom/amap/api/mapcore/ak;

.field private Z:Lcom/amap/api/mapcore/g;

.field a:Lcom/amap/api/mapcore/ag;

.field private aA:Z

.field private aB:Lcom/amap/api/maps/model/Marker;

.field private aC:Lcom/amap/api/mapcore/v;

.field private aD:Z

.field private aE:Z

.field private aF:Z

.field private aG:I

.field private aH:Z

.field private aI:Ljava/lang/Thread;

.field private aJ:Lcom/amap/api/maps/model/LatLngBounds;

.field private aK:Z

.field private aL:Z

.field private aM:Z

.field private aN:I

.field private aO:I

.field private aa:Lcom/amap/api/mapcore/h;

.field private ab:I

.field private ac:I

.field private ad:Lcom/amap/api/maps/AMap$CancelableCallback;

.field private ae:Z

.field private af:I

.field private ag:Z

.field private ah:Z

.field private ai:Landroid/graphics/drawable/Drawable;

.field private aj:Landroid/location/Location;

.field private ak:Ljava/lang/Boolean;

.field private final al:[I

.field private am:Z

.field private an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

.field private ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

.field private ap:Ljava/util/Timer;

.field private ar:Lcom/amap/api/mapcore/m;

.field private as:Z

.field private at:Z

.field private av:Ljava/lang/Runnable;

.field private aw:Z

.field private ax:Z

.field private ay:Z

.field private az:Z

.field b:Lcom/amap/api/mapcore/av;

.field c:Lcom/amap/api/mapcore/ar;

.field d:Z

.field e:Lcom/amap/api/maps/CustomRenderer;

.field f:Lcom/amap/api/mapcore/p;

.field g:Ljava/lang/Runnable;

.field final h:Landroid/os/Handler;

.field private i:I

.field private j:I

.field private k:Landroid/graphics/Bitmap;

.field private l:Landroid/graphics/Bitmap;

.field private m:Z

.field private n:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field private q:Lcom/autonavi/amap/mapcore/MapCore;

.field private r:Landroid/content/Context;

.field private s:Lcom/amap/api/mapcore/a;

.field private t:Lcom/autonavi/amap/mapcore/MapProjection;

.field private u:Landroid/view/GestureDetector;

.field private v:Landroid/view/ScaleGestureDetector;

.field private w:Lcom/amap/api/mapcore/a/c;

.field private x:Landroid/view/SurfaceHolder;

.field private y:Lcom/amap/api/mapcore/ai;

.field private z:Lcom/amap/api/mapcore/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 198
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/amap/api/mapcore/b;->aq:D

    .line 878
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 249
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/amap/api/mapcore/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 251
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    .line 252
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->x:Landroid/view/SurfaceHolder;

    .line 253
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Landroid/view/SurfaceHolder;

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 254
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 256
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    .line 289
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->i:I

    .line 103
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->j:I

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->m:Z

    .line 126
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 127
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 132
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->p:I

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    .line 140
    new-instance v0, Lcom/amap/api/mapcore/ag;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ag;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->x:Landroid/view/SurfaceHolder;

    .line 166
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->S:Landroid/graphics/Rect;

    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->T:Z

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->U:Z

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->V:Z

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->d:Z

    .line 178
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b;->ab:I

    .line 179
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b;->ac:I

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ae:Z

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b;->af:I

    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ag:Z

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ah:Z

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    .line 187
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Ljava/lang/Boolean;

    .line 188
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->al:[I

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    .line 214
    new-instance v0, Lcom/amap/api/mapcore/p;

    invoke-direct {v0}, Lcom/amap/api/mapcore/p;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    .line 432
    new-instance v0, Lcom/amap/api/mapcore/m;

    invoke-direct {v0}, Lcom/amap/api/mapcore/m;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Lcom/amap/api/mapcore/m;

    .line 876
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->as:Z

    .line 877
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    .line 879
    new-instance v0, Lcom/amap/api/mapcore/c;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/c;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->av:Ljava/lang/Runnable;

    .line 1138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aw:Z

    .line 1850
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ax:Z

    .line 2120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->ay:Z

    .line 2124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->az:Z

    .line 2126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aA:Z

    .line 2127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    .line 2128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    .line 2129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aD:Z

    .line 2130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aE:Z

    .line 2131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aF:Z

    .line 2179
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b;->aG:I

    .line 2183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aH:Z

    .line 2888
    new-instance v0, Lcom/amap/api/mapcore/d;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/d;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    .line 2916
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aJ:Lcom/amap/api/maps/model/LatLngBounds;

    .line 2927
    new-instance v0, Lcom/amap/api/mapcore/e;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/e;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    .line 3136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aK:Z

    .line 3138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aL:Z

    .line 3139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    .line 290
    const/16 v0, 0xff

    const/16 v1, 0xeb

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->setBackgroundColor(I)V

    .line 291
    new-instance v0, Lcom/amap/api/mapcore/g;

    invoke-direct {v0}, Lcom/amap/api/mapcore/g;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Z:Lcom/amap/api/mapcore/g;

    .line 292
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    .line 293
    new-instance v0, Lcom/autonavi/amap/mapcore/MapCore;

    invoke-direct {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    .line 294
    new-instance v0, Lcom/amap/api/mapcore/a;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/a;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    .line 295
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0, v1}, Lcom/autonavi/amap/mapcore/MapCore;->setMapCallback(Lcom/autonavi/amap/mapcore/IMapCallback;)V

    .line 297
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapCore;->getMapstate()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    .line 307
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    const-wide v2, 0x4043f64cb5bb3850L    # 39.924216

    const-wide v4, 0x405d1976a004eda6L    # 116.3978653

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/high16 v2, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;FFF)Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 310
    new-instance v0, Lcom/amap/api/mapcore/ao;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ao;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->P:Lcom/amap/api/mapcore/z;

    .line 311
    new-instance v0, Lcom/amap/api/mapcore/f;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/f;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->W:Lcom/amap/api/mapcore/f;

    .line 312
    new-instance v0, Lcom/amap/api/mapcore/as;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/as;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Q:Lcom/amap/api/mapcore/ac;

    .line 321
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$c;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->u:Landroid/view/GestureDetector;

    .line 326
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Landroid/view/GestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$b;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 327
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Landroid/view/GestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 332
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$e;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$e;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->v:Landroid/view/ScaleGestureDetector;

    .line 338
    new-instance v0, Lcom/amap/api/mapcore/a/c;

    new-instance v1, Lcom/amap/api/mapcore/b$d;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$d;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/a/c;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/a/c$a;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/a/c;

    .line 344
    new-instance v0, Lcom/amap/api/mapcore/a/b;

    new-instance v1, Lcom/amap/api/mapcore/b$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/amap/api/mapcore/b$a;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/a/b;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/a/b$a;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->X:Lcom/amap/api/mapcore/a/b;

    .line 349
    new-instance v0, Lcom/amap/api/mapcore/ai;

    invoke-direct {v0, p1, p0}, Lcom/amap/api/mapcore/ai;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    .line 350
    new-instance v0, Lcom/amap/api/mapcore/au;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/au;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    .line 351
    new-instance v0, Lcom/amap/api/mapcore/ap;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/ap;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    .line 352
    new-instance v0, Lcom/amap/api/mapcore/ar;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/ar;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    .line 353
    new-instance v0, Lcom/amap/api/mapcore/av;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/av;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    .line 355
    new-instance v0, Lcom/amap/api/mapcore/ad;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-direct {v0, v1, v2, p0}, Lcom/amap/api/mapcore/ad;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ag;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    .line 356
    new-instance v0, Lcom/amap/api/mapcore/k;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-direct {v0, v1, v2, p0}, Lcom/amap/api/mapcore/k;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ag;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    .line 357
    new-instance v0, Lcom/amap/api/mapcore/ah;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p0}, Lcom/amap/api/mapcore/ah;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    .line 359
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/au;->setBackgroundColor(I)V

    .line 360
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->setBackgroundColor(I)V

    .line 361
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ai;->setBackgroundColor(I)V

    .line 362
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ar;->setBackgroundColor(I)V

    .line 363
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setBackgroundColor(I)V

    .line 364
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ah;->setBackgroundColor(I)V

    .line 365
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/16 v1, 0xff

    const/16 v2, 0xeb

    const/16 v3, 0xeb

    const/16 v4, 0xeb

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setBackgroundColor(I)V

    .line 367
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 370
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 372
    new-instance v1, Lcom/amap/api/mapcore/ai$a;

    invoke-direct {v1, v0}, Lcom/amap/api/mapcore/ai$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 375
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v2, v3, v1}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 377
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 379
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 381
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 383
    new-instance v0, Lcom/amap/api/mapcore/ai$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x53

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 387
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 389
    new-instance v0, Lcom/amap/api/mapcore/ai$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x53

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 394
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 396
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->w()Lcom/amap/api/mapcore/ac;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/ac;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setVisibility(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    :cond_0
    :goto_0
    new-instance v0, Lcom/amap/api/mapcore/ai$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x33

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 408
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 409
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    .line 411
    new-instance v0, Lcom/amap/api/mapcore/h;

    invoke-direct {v0, p1}, Lcom/amap/api/mapcore/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    .line 413
    new-instance v0, Lcom/amap/api/mapcore/ak;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ak;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    .line 415
    invoke-virtual {p0, p0}, Lcom/amap/api/mapcore/b;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 418
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->ZoomControllerViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setId(I)V

    .line 419
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->ScaleControlsViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->setId(I)V

    .line 420
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->MyLocationViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setId(I)V

    .line 421
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->CompassViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setId(I)V

    .line 430
    return-void

    .line 399
    :catch_0
    move-exception v0

    .line 400
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 188
    nop

    :array_0
    .array-data 4
        0x989680
        0x4c4b40
        0x1e8480
        0xf4240
        0x7a120
        0x30d40
        0x186a0
        0xc350
        0x7530
        0x4e20
        0x2710
        0x1388
        0x7d0
        0x3e8
        0x1f4
        0xc8
        0x64
        0x32
        0x19
        0xa
        0x5
    .end array-data
.end method

.method static synthetic A(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapClickListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Lcom/amap/api/maps/AMap$OnMapClickListener;

    return-object v0
.end method

.method static synthetic B(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aE:Z

    return v0
.end method

.method static synthetic C(Lcom/amap/api/mapcore/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic D(Lcom/amap/api/mapcore/b;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic E(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/g;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Z:Lcom/amap/api/mapcore/g;

    return-object v0
.end method

.method static synthetic F(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnCameraChangeListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    return-object v0
.end method

.method static synthetic G(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLoadedListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->H:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

    return-object v0
.end method

.method static synthetic H(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/k;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    return-object v0
.end method

.method static synthetic I(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/au;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    return-object v0
.end method

.method static synthetic J(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/v;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    return-object v0
.end method

.method static synthetic K(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    return-object v0
.end method

.method static synthetic L(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    return-object v0
.end method

.method static synthetic M(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ae:Z

    return v0
.end method

.method static synthetic N(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ah:Z

    return v0
.end method

.method static synthetic P()D
    .locals 2

    .prologue
    .line 94
    sget-wide v0, Lcom/amap/api/mapcore/b;->aq:D

    return-wide v0
.end method

.method private Q()V
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapCore;->destroy()V

    .line 946
    :cond_0
    return-void
.end method

.method private declared-synchronized R()V
    .locals 6

    .prologue
    .line 1122
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1123
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->S()V

    .line 1125
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 1126
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    .line 1128
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    new-instance v1, Lcom/amap/api/mapcore/b$f;

    invoke-direct {v1, p0, p0}, Lcom/amap/api/mapcore/b$f;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x14

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1129
    monitor-exit p0

    return-void

    .line 1122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized S()V
    .locals 1

    .prologue
    .line 1132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1133
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/util/Timer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1136
    :cond_0
    monitor-exit p0

    return-void

    .line 1132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized T()V
    .locals 3

    .prologue
    .line 1145
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aw:Z

    if-nez v0, :cond_0

    .line 1146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aw:Z

    .line 1149
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "bk.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1152
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1153
    const/16 v1, 0xf0

    if-lt v0, v1, :cond_1

    .line 1154
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v1, "icn_h.data"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->a([B)[B

    move-result-object v0

    .line 1156
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1165
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "roadarrow.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1168
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "LineRound.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1171
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "tgl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1174
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "trl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1177
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "tyl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1180
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v2, "dash.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1187
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 1159
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v1, "icn.data"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->a([B)[B

    move-result-object v0

    .line 1161
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1184
    :catch_0
    move-exception v0

    .line 1185
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private U()Lcom/amap/api/maps/model/LatLng;
    .locals 6

    .prologue
    .line 2089
    new-instance v0, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2090
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2091
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 2092
    iget v2, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v2, v1, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2093
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v0, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v4, v0, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 2094
    return-object v1
.end method

.method private V()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2136
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aH:Z

    if-eqz v0, :cond_0

    .line 2137
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->aH:Z

    .line 2138
    :cond_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aD:Z

    if-eqz v0, :cond_1

    .line 2139
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->aD:Z

    .line 2140
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2142
    iput-boolean v3, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 2143
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 2145
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ay:Z

    if-eqz v0, :cond_2

    .line 2146
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->ay:Z

    .line 2147
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2149
    iput-boolean v3, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 2150
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 2152
    :cond_2
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->az:Z

    .line 2153
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->aA:Z

    .line 2154
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_3

    .line 2155
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$OnMarkerDragListener;->onMarkerDragEnd(Lcom/amap/api/maps/model/Marker;)V

    .line 2156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    .line 2158
    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 94
    iput p1, p0, Lcom/amap/api/mapcore/b;->aG:I

    return p1
.end method

.method public static a(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 3168
    mul-int v0, p2, p3

    new-array v9, v0, [I

    .line 3169
    mul-int v0, p2, p3

    new-array v10, v0, [I

    .line 3170
    invoke-static {v9}, Ljava/nio/IntBuffer;->wrap([I)Ljava/nio/IntBuffer;

    move-result-object v7

    .line 3171
    invoke-virtual {v7, v8}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 3172
    const/16 v5, 0x1908

    const/16 v6, 0x1401

    move-object v0, p4

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    move v1, v8

    .line 3178
    :goto_0
    if-ge v1, p3, :cond_1

    move v0, v8

    .line 3179
    :goto_1
    if-ge v0, p2, :cond_0

    .line 3180
    mul-int v2, v1, p2

    add-int/2addr v2, v0

    aget v2, v9, v2

    .line 3181
    shr-int/lit8 v3, v2, 0x10

    and-int/lit16 v3, v3, 0xff

    .line 3182
    shl-int/lit8 v4, v2, 0x10

    const/high16 v5, 0xff0000

    and-int/2addr v4, v5

    .line 3183
    const v5, -0xff0100

    and-int/2addr v2, v5

    or-int/2addr v2, v4

    or-int/2addr v2, v3

    .line 3184
    sub-int v3, p3, v1

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, p2

    add-int/2addr v3, v0

    aput v2, v10, v3

    .line 3179
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3178
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3190
    :cond_1
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 3191
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v10

    move v3, p2

    move v6, p2

    move v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 3196
    :goto_2
    return-object v0

    .line 3192
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 3193
    const/4 v0, 0x0

    .line 3194
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/v;)Lcom/amap/api/mapcore/v;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$CancelableCallback;)Lcom/amap/api/maps/AMap$CancelableCallback;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;)Lcom/amap/api/maps/model/LatLngBounds;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->aJ:Lcom/amap/api/maps/model/LatLngBounds;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/Marker;)Lcom/amap/api/maps/model/Marker;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    return-object p1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 10

    .prologue
    .line 2161
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aA:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_0

    .line 2162
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 2163
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    const/high16 v2, 0x42700000    # 60.0f

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 2164
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    invoke-interface {v2}, Lcom/amap/api/mapcore/v;->e()Lcom/amap/api/maps/model/LatLng;

    move-result-object v2

    .line 2165
    iget-object v3, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    invoke-interface {v3}, Lcom/amap/api/mapcore/v;->d()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    .line 2166
    new-instance v4, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2167
    invoke-virtual {p0, v0, v1, v4}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2168
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v8, v4, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    add-double/2addr v6, v8

    iget-wide v8, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    sub-double/2addr v6, v8

    iget-wide v8, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-wide v4, v4, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    add-double/2addr v4, v8

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    sub-double v2, v4, v2

    invoke-direct {v0, v6, v7, v2, v3}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 2171
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/model/Marker;->setPosition(Lcom/amap/api/maps/model/LatLng;)V

    .line 2172
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$OnMarkerDragListener;->onMarkerDrag(Lcom/amap/api/maps/model/Marker;)V

    .line 2174
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->Q()V

    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->as:Z

    return p1
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 94
    iput p1, p0, Lcom/amap/api/mapcore/b;->ab:I

    return p1
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    return v0
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->at:Z

    return p1
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 94
    iput p1, p0, Lcom/amap/api/mapcore/b;->ac:I

    return p1
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ay:Z

    return v0
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->az:Z

    return p1
.end method

.method static synthetic d(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->az:Z

    return v0
.end method

.method static synthetic d(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aD:Z

    return p1
.end method

.method static synthetic e(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ac;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Q:Lcom/amap/api/mapcore/ac;

    return-object v0
.end method

.method static synthetic e(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aF:Z

    return p1
.end method

.method static synthetic f(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    return v0
.end method

.method static synthetic f(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aH:Z

    return p1
.end method

.method static synthetic g(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/amap/api/mapcore/b;->aN:I

    return v0
.end method

.method static synthetic g(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aA:Z

    return p1
.end method

.method static synthetic h(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/amap/api/mapcore/b;->aO:I

    return v0
.end method

.method static synthetic h(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aE:Z

    return p1
.end method

.method static synthetic i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    return-object v0
.end method

.method static synthetic i(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ay:Z

    return p1
.end method

.method static synthetic j(Lcom/amap/api/mapcore/b;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->V()V

    return-void
.end method

.method static synthetic j(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ag:Z

    return p1
.end method

.method static synthetic k(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aF:Z

    return v0
.end method

.method static synthetic k(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ah:Z

    return p1
.end method

.method static synthetic l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    return-object v0
.end method

.method static synthetic m(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$CancelableCallback;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    return-object v0
.end method

.method static synthetic n(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/a/b;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->X:Lcom/amap/api/mapcore/a/b;

    return-object v0
.end method

.method static synthetic o(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/amap/api/mapcore/b;->ab:I

    return v0
.end method

.method static synthetic p(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/amap/api/mapcore/b;->ac:I

    return v0
.end method

.method static synthetic q(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLongClickListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    return-object v0
.end method

.method static synthetic r(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ah;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    return-object v0
.end method

.method static synthetic s(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerDragListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    return-object v0
.end method

.method static synthetic t(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/v;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aC:Lcom/amap/api/mapcore/v;

    return-object v0
.end method

.method static synthetic u(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/model/Marker;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aB:Lcom/amap/api/maps/model/Marker;

    return-object v0
.end method

.method static synthetic v(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/amap/api/mapcore/b;->aG:I

    return v0
.end method

.method static synthetic w(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aH:Z

    return v0
.end method

.method static synthetic x(Lcom/amap/api/mapcore/b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    return-object v0
.end method

.method static synthetic y(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->L:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

    return-object v0
.end method

.method static synthetic z(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerClickListener;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->F:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2624
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2625
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 2626
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ai;->removeView(Landroid/view/View;)V

    .line 2627
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2628
    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2629
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2638
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    .line 2640
    :cond_0
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    .line 2641
    return-void
.end method

.method public B()F
    .locals 1

    .prologue
    .line 2885
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v0

    return v0
.end method

.method C()V
    .locals 2

    .prologue
    .line 2919
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2920
    return-void
.end method

.method public D()Lcom/amap/api/maps/model/LatLngBounds;
    .locals 1

    .prologue
    .line 2924
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aJ:Lcom/amap/api/maps/model/LatLngBounds;

    return-object v0
.end method

.method E()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3147
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aK:Z

    if-nez v0, :cond_1

    .line 3148
    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->setBackgroundColor(I)V

    .line 3149
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/au;->setBackgroundColor(I)V

    .line 3150
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->setBackgroundColor(I)V

    .line 3151
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ai;->setBackgroundColor(I)V

    .line 3152
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ar;->setBackgroundColor(I)V

    .line 3153
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    if-eqz v0, :cond_0

    .line 3154
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setBackgroundColor(I)V

    .line 3156
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ah;->setBackgroundColor(I)V

    .line 3157
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setBackgroundColor(I)V

    .line 3158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aK:Z

    .line 3159
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->postInvalidate()V

    .line 3161
    :cond_1
    return-void
.end method

.method F()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 3164
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/au;->c()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public G()F
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 3252
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    .line 3253
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 3254
    new-instance v2, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 3255
    invoke-virtual {p0, v3, v3, v1}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 3256
    invoke-virtual {p0, v0, v3, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 3257
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v2, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v2, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/amap/api/mapcore/b/r;->a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)D

    move-result-wide v2

    .line 3260
    int-to-double v0, v0

    div-double v0, v2, v0

    double-to-float v0, v0

    return v0
.end method

.method public H()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3301
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 3302
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3303
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 3304
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    .line 3305
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3307
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public I()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3312
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "\u5730\u56fe\u672a\u521d\u59cb\u5316\u5b8c\u6210\uff01"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/a;->a(ZLjava/lang/Object;)V

    .line 3314
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ah;->f()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 3312
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()V
    .locals 1

    .prologue
    .line 3319
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->c()V

    .line 3320
    return-void
.end method

.method public K()V
    .locals 1

    .prologue
    .line 3329
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aL:Z

    .line 3330
    return-void
.end method

.method public L()Z
    .locals 1

    .prologue
    .line 3334
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aL:Z

    return v0
.end method

.method public M()V
    .locals 1

    .prologue
    .line 3339
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    if-eqz v0, :cond_0

    .line 3340
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ah;->g()V

    .line 3342
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aL:Z

    .line 3343
    return-void
.end method

.method public N()I
    .locals 1

    .prologue
    .line 3367
    iget v0, p0, Lcom/amap/api/mapcore/b;->af:I

    return v0
.end method

.method public O()Z
    .locals 1

    .prologue
    .line 3372
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->U:Z

    return v0
.end method

.method public a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/s;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1702
    if-nez p1, :cond_0

    .line 1703
    const/4 v0, 0x0

    .line 1715
    :goto_0
    return-object v0

    .line 1705
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/j;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/j;-><init>(Lcom/amap/api/mapcore/r;)V

    .line 1706
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->b(I)V

    .line 1707
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getCenter()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 1708
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(Z)V

    .line 1709
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->b(F)V

    .line 1710
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(F)V

    .line 1711
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(I)V

    .line 1712
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getRadius()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/amap/api/mapcore/j;->a(D)V

    .line 1713
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/w;)V

    .line 1714
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/GroundOverlayOptions;)Lcom/amap/api/mapcore/t;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1721
    if-nez p1, :cond_0

    .line 1722
    const/4 v0, 0x0

    .line 1739
    :goto_0
    return-object v0

    .line 1724
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/q;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/q;-><init>(Lcom/amap/api/mapcore/r;)V

    .line 1726
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getAnchorU()F

    move-result v1

    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getAnchorV()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/mapcore/q;->b(FF)V

    .line 1728
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getWidth()F

    move-result v1

    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getHeight()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/mapcore/q;->a(FF)V

    .line 1730
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getImage()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/BitmapDescriptor;)V

    .line 1731
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getLocation()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 1732
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getBounds()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/LatLngBounds;)V

    .line 1733
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getBearing()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->c(F)V

    .line 1734
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getTransparency()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->d(F)V

    .line 1735
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(Z)V

    .line 1736
    invoke-virtual {p1}, Lcom/amap/api/maps/model/GroundOverlayOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/q;->a(F)V

    .line 1737
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/w;)V

    .line 1738
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/PolygonOptions;)Lcom/amap/api/mapcore/x;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1684
    if-nez p1, :cond_0

    .line 1685
    const/4 v0, 0x0

    .line 1696
    :goto_0
    return-object v0

    .line 1687
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/am;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/am;-><init>(Lcom/amap/api/mapcore/r;)V

    .line 1688
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->a(I)V

    .line 1689
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->a(Ljava/util/List;)V

    .line 1690
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->a(Z)V

    .line 1691
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->b(F)V

    .line 1692
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->a(F)V

    .line 1693
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/am;->b(I)V

    .line 1694
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/w;)V

    .line 1695
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/mapcore/y;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1655
    if-nez p1, :cond_0

    .line 1656
    const/4 v0, 0x0

    .line 1678
    :goto_0
    return-object v0

    .line 1659
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/an;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/an;-><init>(Lcom/amap/api/mapcore/r;)V

    .line 1660
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(I)V

    .line 1661
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isGeodesic()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->b(Z)V

    .line 1662
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isDottedLine()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->c(Z)V

    .line 1663
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(Ljava/util/List;)V

    .line 1664
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(Z)V

    .line 1665
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->b(F)V

    .line 1666
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(F)V

    .line 1667
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isUseTexture()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->d(Z)V

    .line 1669
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getCustomTexture()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1670
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getCustomTexture()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/maps/model/BitmapDescriptor;->clone()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v1

    .line 1671
    invoke-virtual {v1}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1672
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 1673
    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/an;->a(Landroid/graphics/Bitmap;)V

    .line 1676
    :cond_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/w;)V

    .line 1677
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method a()Lcom/amap/api/maps/CustomRenderer;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    return-object v0
.end method

.method public a(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1744
    if-nez p1, :cond_0

    .line 1745
    const/4 v0, 0x0

    .line 1751
    :goto_0
    return-object v0

    .line 1747
    :cond_0
    new-instance v1, Lcom/amap/api/mapcore/aj;

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-direct {v1, p1, v0}, Lcom/amap/api/mapcore/aj;-><init>(Lcom/amap/api/maps/model/MarkerOptions;Lcom/amap/api/mapcore/ah;)V

    .line 1749
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ah;->a(Lcom/amap/api/mapcore/v;)V

    .line 1750
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1751
    new-instance v0, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/v;)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/TileOverlayOptions;)Lcom/amap/api/maps/model/TileOverlay;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1777
    if-nez p1, :cond_0

    .line 1778
    const/4 v0, 0x0

    .line 1784
    :goto_0
    return-object v0

    .line 1780
    :cond_0
    new-instance v1, Lcom/amap/api/mapcore/aq;

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-direct {v1, p1, v0}, Lcom/amap/api/mapcore/aq;-><init>(Lcom/amap/api/maps/model/TileOverlayOptions;Lcom/amap/api/mapcore/ar;)V

    .line 1782
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ar;->a(Lcom/amap/api/mapcore/ab;)V

    .line 1783
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1784
    new-instance v0, Lcom/amap/api/maps/model/TileOverlay;

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/TileOverlay;-><init>(Lcom/amap/api/mapcore/ab;)V

    goto :goto_0
.end method

.method public a(DDLcom/autonavi/amap/mapcore/FPoint;)V
    .locals 3

    .prologue
    .line 2035
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2037
    invoke-static {p3, p4, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2038
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v1, v2, v0, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2039
    return-void
.end method

.method public a(DDLcom/autonavi/amap/mapcore/IPoint;)V
    .locals 1

    .prologue
    .line 2044
    invoke-static {p3, p4, p1, p2, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2045
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 598
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ak;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ak;->a(F)V

    .line 600
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x7db

    const/4 v0, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1827
    if-ne p1, v0, :cond_0

    .line 1828
    iput v0, p0, Lcom/amap/api/mapcore/b;->p:I

    .line 1829
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    new-instance v1, Lcom/amap/api/mapcore/af;

    invoke-direct {v1, v4}, Lcom/amap/api/mapcore/af;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/af;->a(Z)Lcom/amap/api/mapcore/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/af;)V

    .line 1832
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/au;->a(Z)V

    .line 1840
    :goto_0
    return-void

    .line 1834
    :cond_0
    iput v2, p0, Lcom/amap/api/mapcore/b;->p:I

    .line 1835
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    new-instance v1, Lcom/amap/api/mapcore/af;

    invoke-direct {v1, v4}, Lcom/amap/api/mapcore/af;-><init>(I)V

    invoke-virtual {v1, v3}, Lcom/amap/api/mapcore/af;->a(Z)Lcom/amap/api/mapcore/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/af;)V

    .line 1838
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0, v3}, Lcom/amap/api/mapcore/au;->a(Z)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 3347
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_0

    .line 3348
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    .line 3349
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0, p1, p2}, Lcom/amap/api/mapcore/a;->a(II)V

    .line 3350
    iput p1, p0, Lcom/amap/api/mapcore/b;->aN:I

    .line 3351
    iput p2, p0, Lcom/amap/api/mapcore/b;->aO:I

    .line 3353
    :cond_0
    return-void
.end method

.method public a(IIII)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1109
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v1, 0x899

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1113
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v1, 0x89a

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1119
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/DPoint;)V
    .locals 4

    .prologue
    .line 2018
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2020
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2021
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2022
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v2, v3, v0, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2023
    iget v0, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v0, v1, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2024
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/FPoint;)V
    .locals 1

    .prologue
    .line 2049
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0, p1, p2, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2050
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/IPoint;)V
    .locals 3

    .prologue
    .line 2028
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2029
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2030
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v1, v2, v0, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2031
    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 609
    if-nez p1, :cond_0

    .line 645
    :goto_0
    return-void

    .line 612
    :cond_0
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 615
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    if-nez v0, :cond_5

    .line 616
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ak;->a()V

    .line 617
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 629
    :catch_0
    move-exception v0

    .line 630
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 632
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/amap/api/mapcore/ak;->a(Lcom/amap/api/maps/model/LatLng;D)V

    .line 634
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    if-eqz v0, :cond_4

    .line 635
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_4

    .line 640
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    invoke-interface {v0, p1}, Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;->onMyLocationChange(Landroid/location/Location;)V

    .line 643
    :cond_4
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    .line 644
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0

    .line 620
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    if-nez v0, :cond_2

    .line 621
    :cond_6
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    if-nez v0, :cond_7

    .line 622
    new-instance v0, Lcom/amap/api/mapcore/ak;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ak;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    .line 624
    :cond_7
    if-eqz v1, :cond_2

    .line 625
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v0

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;F)Lcom/amap/api/mapcore/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public a(Lcom/amap/api/mapcore/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1297
    iget-object v0, p1, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v2, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-ne v0, v2, :cond_0

    .line 1298
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "the map must have a size"

    invoke-static {v0, v2}, Lcom/amap/api/mapcore/b/a;->a(ZLjava/lang/Object;)V

    .line 1301
    :cond_0
    iput-boolean v1, p1, Lcom/amap/api/mapcore/i;->p:Z

    .line 1302
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    iput-boolean v0, p1, Lcom/amap/api/mapcore/i;->n:Z

    .line 1303
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 1304
    return-void

    .line 1298
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1326
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_0

    .line 1327
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v2

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    const-string v3, "the map must have a size"

    invoke-static {v2, v3}, Lcom/amap/api/mapcore/b/a;->a(ZLjava/lang/Object;)V

    .line 1330
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Z)V

    .line 1332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    if-eqz v2, :cond_1

    .line 1333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-interface {v2}, Lcom/amap/api/maps/AMap$CancelableCallback;->onCancel()V

    .line 1335
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->b(Z)V

    .line 1336
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1337
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/amap/api/mapcore/b;->ag:Z

    if-eqz v2, :cond_2

    .line 1338
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->ah:Z

    .line 1340
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->ae:Z

    .line 1341
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->k:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_5

    .line 1342
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->c:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    .line 1344
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1637
    :goto_1
    return-void

    .line 1327
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1347
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->b(Z)V

    .line 1348
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1349
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1350
    new-instance v10, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v10}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1351
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p1

    iget v4, v0, Lcom/amap/api/mapcore/i;->b:F

    float-to-int v4, v4

    add-int/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p1

    iget v5, v0, Lcom/amap/api/mapcore/i;->c:F

    float-to-int v5, v5

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v10}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1354
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1356
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v8}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    iget v9, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v11, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v9, v11

    iget v10, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v10, v2

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    .line 1636
    :goto_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_1

    .line 1360
    :cond_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->b:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_8

    .line 1361
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1362
    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v2, v6

    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v2

    sub-float v11, v2, v6

    .line 1363
    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_6

    .line 1364
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1367
    :cond_6
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1368
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_7

    .line 1369
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1373
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1374
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto :goto_2

    .line 1371
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_3

    .line 1377
    :cond_8
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->h:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_b

    .line 1378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1379
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v6, v2

    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v2

    sub-float v11, v2, v6

    .line 1380
    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_9

    .line 1381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1384
    :cond_9
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1385
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_a

    .line 1386
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1390
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1391
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1388
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_4

    .line 1394
    :cond_b
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->i:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_e

    .line 1395
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1396
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->d:F

    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v2

    sub-float v11, v2, v6

    .line 1398
    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_c

    .line 1399
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1402
    :cond_c
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1403
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_d

    .line 1404
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1408
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1409
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1406
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_5

    .line 1412
    :cond_e
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->j:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_12

    .line 1413
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->b(Z)V

    .line 1414
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->e:F

    .line 1415
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1416
    add-float v3, v6, v2

    invoke-static {v3}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v3

    sub-float v11, v3, v6

    .line 1417
    const/4 v3, 0x0

    cmpl-float v3, v11, v3

    if-nez v3, :cond_f

    .line 1418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1421
    :cond_f
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->m:Landroid/graphics/Point;

    .line 1422
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1423
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v4, v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1424
    const/4 v9, 0x0

    .line 1425
    const/4 v10, 0x0

    .line 1426
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1427
    if-eqz v3, :cond_11

    .line 1428
    iget v7, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3, v4}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1429
    iget v3, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v3, v7

    .line 1430
    iget v7, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v4, v7, v4

    .line 1431
    int-to-double v8, v3

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    float-to-double v14, v2

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    div-double/2addr v8, v12

    int-to-double v12, v3

    sub-double/2addr v8, v12

    double-to-int v9, v8

    .line 1432
    int-to-double v12, v4

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    float-to-double v2, v2

    invoke-static {v14, v15, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double v2, v12, v2

    int-to-double v12, v4

    sub-double/2addr v2, v12

    double-to-int v10, v2

    .line 1442
    :cond_10
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1443
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1434
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_10

    .line 1435
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7, v4}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1436
    iget v3, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v3, v7

    .line 1437
    iget v7, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v4, v7, v4

    .line 1438
    int-to-double v8, v3

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    float-to-double v14, v2

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    div-double/2addr v8, v12

    int-to-double v12, v3

    sub-double/2addr v8, v12

    double-to-int v9, v8

    .line 1439
    int-to-double v12, v4

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    float-to-double v2, v2

    invoke-static {v14, v15, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double v2, v12, v2

    int-to-double v12, v4

    sub-double/2addr v2, v12

    double-to-int v10, v2

    goto :goto_6

    .line 1446
    :cond_12
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->l:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_16

    .line 1447
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1448
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_14

    .line 1449
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1453
    :goto_7
    new-instance v3, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1454
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/amap/api/mapcore/i;->h:Lcom/amap/api/maps/model/CameraPosition;

    .line 1455
    iget-object v5, v4, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v5, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-object v5, v4, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v5, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-static {v6, v7, v8, v9, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1457
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    .line 1458
    iget v5, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v9, v5, v7

    .line 1459
    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v10, v3, v5

    .line 1460
    iget v3, v4, Lcom/amap/api/maps/model/CameraPosition;->zoom:F

    invoke-static {v3}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v3

    sub-float v11, v3, v6

    .line 1461
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    .line 1462
    iget v3, v4, Lcom/amap/api/maps/model/CameraPosition;->bearing:F

    const/high16 v5, 0x43b40000    # 360.0f

    rem-float/2addr v3, v5

    const/high16 v5, 0x43b40000    # 360.0f

    rem-float v5, v7, v5

    sub-float v12, v3, v5

    .line 1463
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v5, 0x43340000    # 180.0f

    cmpl-float v3, v3, v5

    if-ltz v3, :cond_13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v3

    const/high16 v5, 0x43b40000    # 360.0f

    mul-float/2addr v3, v5

    sub-float/2addr v12, v3

    .line 1465
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    .line 1466
    iget v3, v4, Lcom/amap/api/maps/model/CameraPosition;->tilt:F

    invoke-static {v3}, Lcom/amap/api/mapcore/b/r;->a(F)F

    move-result v3

    sub-float v13, v3, v8

    .line 1467
    if-nez v9, :cond_15

    if-nez v10, :cond_15

    const/4 v3, 0x0

    cmpl-float v3, v11, v3

    if-nez v3, :cond_15

    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-nez v3, :cond_15

    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-nez v3, :cond_15

    .line 1469
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1451
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto/16 :goto_7

    .line 1472
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1473
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1475
    :cond_16
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->e:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_1a

    .line 1476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    .line 1477
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->g:F

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v2, v3

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float v3, v7, v3

    sub-float v12, v2, v3

    .line 1479
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x43340000    # 180.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_17

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    sub-float/2addr v12, v2

    .line 1481
    :cond_17
    const/4 v2, 0x0

    cmpl-float v2, v12, v2

    if-nez v2, :cond_18

    .line 1482
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1485
    :cond_18
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1486
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_19

    .line 1487
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1491
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1492
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1489
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_8

    .line 1495
    :cond_1a
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->d:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_1d

    .line 1496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    .line 1497
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->f:F

    sub-float v13, v2, v8

    .line 1498
    const/4 v2, 0x0

    cmpl-float v2, v13, v2

    if-nez v2, :cond_1b

    .line 1499
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1502
    :cond_1b
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1503
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_1c

    .line 1504
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1508
    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1509
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1506
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_9

    .line 1512
    :cond_1d
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->c:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_20

    .line 1513
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1514
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/amap/api/mapcore/b;->aM:Z

    if-eqz v3, :cond_1e

    .line 1515
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/b;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/amap/api/mapcore/b;->aO:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1519
    :goto_a
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->o:Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v9, v3, v4

    .line 1520
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->o:Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v10, v3, v4

    .line 1521
    if-nez v9, :cond_1f

    if-nez v10, :cond_1f

    .line 1522
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1517
    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_a

    .line 1525
    :cond_1f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1527
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v8

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1530
    :cond_20
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-eq v2, v3, :cond_21

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->n:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_2a

    .line 1532
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->b(Z)V

    .line 1535
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_23

    .line 1536
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v3

    .line 1537
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v2

    move v15, v2

    move/from16 v16, v3

    .line 1542
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float v18, v2, v3

    .line 1543
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v19

    .line 1544
    const/4 v2, 0x0

    cmpl-float v2, v18, v2

    if-nez v2, :cond_22

    const/4 v2, 0x0

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_24

    :cond_22
    const/4 v2, 0x1

    .line 1545
    :goto_c
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/amap/api/mapcore/i;->i:Lcom/amap/api/maps/model/LatLngBounds;

    .line 1546
    move-object/from16 v0, p1

    iget v0, v0, Lcom/amap/api/mapcore/i;->j:I

    move/from16 v20, v0

    .line 1547
    new-instance v21, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v21 .. v21}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1548
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1549
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v17

    .line 1550
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1551
    if-nez v2, :cond_28

    .line 1552
    new-instance v22, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v22 .. v22}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1553
    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-object v4, v9, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    iget-object v4, v9, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v6, v9, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps/model/LatLng;->latitude:D

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    move-object/from16 v0, v22

    invoke-static {v2, v3, v4, v5, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1558
    new-instance v8, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1559
    new-instance v14, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v14}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1560
    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/amap/api/mapcore/b;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1562
    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v10, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v2, v9, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v12, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v14}, Lcom/amap/api/mapcore/b;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1565
    iget v2, v8, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v3, v14, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v2, v3

    .line 1566
    iget v3, v14, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v8, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v3, v4

    .line 1568
    if-nez v2, :cond_25

    if-nez v3, :cond_25

    .line 1569
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1539
    :cond_23
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->k:I

    .line 1540
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->l:I

    move v15, v2

    move/from16 v16, v3

    goto/16 :goto_b

    .line 1544
    :cond_24
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 1573
    :cond_25
    int-to-float v2, v2

    mul-int/lit8 v4, v20, 0x2

    sub-int v4, v16, v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    .line 1575
    int-to-float v3, v3

    mul-int/lit8 v4, v20, 0x2

    sub-int v4, v15, v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 1578
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1580
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_26

    .line 1581
    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->c(F)I

    move-result v2

    int-to-float v2, v2

    sub-float v2, v17, v2

    .line 1583
    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v2

    .line 1589
    :goto_d
    move-object/from16 v0, v22

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v9, v3, v4

    .line 1590
    move-object/from16 v0, v22

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v10, v3, v4

    .line 1591
    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v2

    sub-float v11, v2, v17

    .line 1592
    if-nez v9, :cond_27

    if-nez v10, :cond_27

    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_27

    .line 1593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1584
    :cond_26
    float-to-double v4, v2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2b

    .line 1585
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v2, v3, v2

    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->c(F)I

    move-result v2

    int-to-float v2, v2

    add-float v2, v2, v17

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    .line 1587
    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v2

    goto :goto_d

    .line 1596
    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1597
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    move-object/from16 v0, v21

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v21

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    const/4 v12, 0x0

    const/4 v13, 0x0

    move/from16 v6, v17

    move/from16 v7, v18

    move/from16 v8, v19

    move-wide/from16 v14, p2

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1600
    :cond_28
    move/from16 v0, v18

    neg-float v12, v0

    .line 1601
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x43340000    # 180.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_29

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    sub-float/2addr v12, v2

    .line 1603
    :cond_29
    move/from16 v0, v19

    neg-float v13, v0

    .line 1605
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1606
    new-instance v2, Lcom/amap/api/mapcore/b$3;

    move-object/from16 v3, p0

    move-object v4, v9

    move/from16 v5, v16

    move v6, v15

    move/from16 v7, v20

    move-wide/from16 v8, p2

    invoke-direct/range {v2 .. v10}, Lcom/amap/api/mapcore/b$3;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;IIIJLcom/amap/api/maps/AMap$CancelableCallback;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1627
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->ae:Z

    .line 1628
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    move-object/from16 v0, v21

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v21

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v14, 0xfa

    move/from16 v6, v17

    move/from16 v7, v18

    move/from16 v8, v19

    invoke-virtual/range {v3 .. v15}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1633
    :cond_2a
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 1634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    goto/16 :goto_2

    :cond_2b
    move/from16 v2, v17

    goto/16 :goto_d
.end method

.method public a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1317
    const-wide/16 v0, 0xfa

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V

    .line 1319
    return-void
.end method

.method public a(Lcom/amap/api/mapcore/o;)V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Lcom/amap/api/mapcore/m;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/m;->a(Lcom/amap/api/mapcore/o;)Z

    .line 441
    return-void
.end method

.method public a(Lcom/amap/api/mapcore/v;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, -0x2

    const/high16 v5, -0x1000000

    .line 2521
    .line 2522
    if-nez p1, :cond_1

    .line 2606
    :cond_0
    :goto_0
    return-void

    .line 2525
    :cond_1
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2529
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    if-eqz v0, :cond_0

    .line 2530
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V

    .line 2531
    new-instance v1, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v1, p1}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/v;)V

    .line 2532
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$InfoWindowAdapter;->getInfoWindow(Lcom/amap/api/maps/model/Marker;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    .line 2535
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_3

    .line 2536
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v3, "infowindow_bg.9.png"

    invoke-static {v0, v3}, Lcom/amap/api/mapcore/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2544
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-nez v0, :cond_4

    .line 2545
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$InfoWindowAdapter;->getInfoContents(Lcom/amap/api/maps/model/Marker;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    .line 2547
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 2548
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2556
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2586
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2587
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 2588
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setDrawingCacheQuality(I)V

    .line 2589
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->c()Landroid/graphics/Rect;

    move-result-object v1

    .line 2590
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->f()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v3

    .line 2591
    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    iget v5, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v4, v5

    .line 2592
    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v5, v1, v3

    .line 2595
    if-eqz v0, :cond_7

    .line 2596
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2597
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2599
    :goto_3
    new-instance v0, Lcom/amap/api/mapcore/ai$a;

    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->e()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    add-int/lit8 v5, v5, 0x2

    const/16 v6, 0x51

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ai$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 2603
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    .line 2604
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ai;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 2540
    :catch_0
    move-exception v0

    .line 2541
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    .line 2560
    :cond_6
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2562
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2563
    new-instance v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2564
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2565
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2566
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2567
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2568
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2569
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2570
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2571
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2573
    iput-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    goto/16 :goto_2

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method public a(Lcom/amap/api/maps/AMap$InfoWindowAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1992
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    .line 1993
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnCameraChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1936
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    .line 1937
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1986
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->L:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

    .line 1987
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1956
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->J:Lcom/amap/api/maps/AMap$OnMapClickListener;

    .line 1957
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapLoadedListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1980
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->H:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

    .line 1981
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1962
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    .line 1963
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V
    .locals 1

    .prologue
    .line 3226
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ao:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    .line 3227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 3228
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1968
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->F:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    .line 1969
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1974
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    .line 1975
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;)V
    .locals 0

    .prologue
    .line 550
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    .line 551
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)V
    .locals 1

    .prologue
    .line 3220
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->an:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    .line 3221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 3222
    return-void
.end method

.method public a(Lcom/amap/api/maps/CustomRenderer;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    .line 208
    return-void
.end method

.method public a(Lcom/amap/api/maps/LocationSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1915
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    .line 1916
    if-eqz p1, :cond_0

    .line 1917
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->a(Z)V

    .line 1921
    :goto_0
    return-void

    .line 1919
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->a(Z)V

    goto :goto_0
.end method

.method a(Lcom/amap/api/maps/model/CameraPosition;)V
    .locals 2

    .prologue
    .line 1941
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1942
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1943
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1944
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1945
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/MyLocationStyle;)V
    .locals 1

    .prologue
    .line 590
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 591
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ak;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ak;->a(Lcom/amap/api/maps/model/MyLocationStyle;)V

    .line 593
    :cond_0
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    const/16 v2, 0x1f4

    const/4 v0, 0x0

    .line 232
    iget-boolean v1, p0, Lcom/amap/api/mapcore/b;->m:Z

    if-eqz v1, :cond_0

    .line 240
    :goto_0
    return-void

    .line 234
    :cond_0
    new-array v1, v2, [I

    .line 235
    invoke-interface {p1, v2, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 236
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 237
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    aget v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 239
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->m:Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 650
    if-eqz p1, :cond_0

    .line 651
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setVisibility(I)V

    .line 655
    :goto_0
    return-void

    .line 653
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/av;->setVisibility(I)V

    goto :goto_0
.end method

.method protected a(ZLcom/amap/api/maps/model/CameraPosition;)V
    .locals 1

    .prologue
    .line 3270
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    if-nez v0, :cond_1

    .line 3290
    :cond_0
    :goto_0
    return-void

    .line 3273
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3276
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3279
    if-nez p2, :cond_2

    .line 3281
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->n()Lcom/amap/api/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 3286
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    invoke-interface {v0, p2}, Lcom/amap/api/maps/AMap$OnCameraChangeListener;->onCameraChangeFinish(Lcom/amap/api/maps/model/CameraPosition;)V

    goto :goto_0

    .line 3282
    :catch_0
    move-exception v0

    .line 3283
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 864
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 865
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/p;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(F)F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2002
    invoke-static {p1}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v0

    return v0
.end method

.method public b(I)F
    .locals 1

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapLenWithWin(I)F

    move-result v0

    return v0
.end method

.method public b(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/mapcore/aj;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1757
    if-nez p1, :cond_0

    .line 1758
    const/4 v0, 0x0

    .line 1764
    :goto_0
    return-object v0

    .line 1760
    :cond_0
    new-instance v0, Lcom/amap/api/mapcore/aj;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/aj;-><init>(Lcom/amap/api/maps/model/MarkerOptions;Lcom/amap/api/mapcore/ah;)V

    .line 1762
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ah;->a(Lcom/amap/api/mapcore/v;)V

    .line 1763
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0
.end method

.method public b()Lcom/autonavi/amap/mapcore/MapCore;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    return-object v0
.end method

.method public b(DDLcom/autonavi/amap/mapcore/IPoint;)V
    .locals 5

    .prologue
    .line 2072
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2073
    new-instance v1, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2075
    invoke-static {p3, p4, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2076
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v0, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2077
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v1, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v1, v1, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v0, v2, v1, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Win(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2080
    return-void
.end method

.method public b(IILcom/autonavi/amap/mapcore/DPoint;)V
    .locals 0

    .prologue
    .line 2067
    invoke-static {p1, p2, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2068
    return-void
.end method

.method public b(IILcom/autonavi/amap/mapcore/FPoint;)V
    .locals 1

    .prologue
    .line 2055
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0, p2, p1, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2056
    return-void
.end method

.method public b(Lcom/amap/api/mapcore/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1310
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V

    .line 1311
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 784
    if-eqz p1, :cond_0

    .line 785
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setVisibility(I)V

    .line 789
    :goto_0
    return-void

    .line 787
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->setVisibility(I)V

    goto :goto_0
.end method

.method public b(Lcom/amap/api/mapcore/v;)Z
    .locals 2

    .prologue
    .line 2616
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2617
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2619
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1804
    const/4 v2, 0x0

    .line 1806
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v1, p1}, Lcom/amap/api/mapcore/ah;->a(Ljava/lang/String;)Lcom/amap/api/mapcore/v;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1810
    :goto_0
    if-eqz v1, :cond_0

    .line 1811
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1812
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ah;->b(Lcom/amap/api/mapcore/v;)Z

    move-result v0

    .line 1814
    :cond_0
    return v0

    .line 1807
    :catch_0
    move-exception v1

    .line 1808
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v1, v2

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/amap/api/mapcore/b;->i:I

    return v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 3241
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    if-eqz v0, :cond_0

    .line 3242
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/au;->a(I)V

    .line 3243
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/au;->invalidate()V

    .line 3244
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ap;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 3245
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ap;->invalidate()V

    .line 3248
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 793
    if-eqz p1, :cond_0

    .line 797
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    .line 798
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/k;->b()V

    .line 803
    :goto_0
    return-void

    .line 801
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    goto :goto_0
.end method

.method public d()Lcom/autonavi/amap/mapcore/MapProjection;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapCore;->getMapstate()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    return-object v0
.end method

.method public d(I)V
    .locals 3

    .prologue
    .line 3293
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3294
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3295
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    .line 3297
    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 822
    if-eqz p1, :cond_0

    .line 823
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ap;->setVisibility(I)V

    .line 824
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->h()V

    .line 830
    :goto_0
    return-void

    .line 826
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->a(Ljava/lang/String;)V

    .line 827
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ap;->a(I)V

    .line 828
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->setVisibility(I)V

    goto :goto_0
.end method

.method public e()Lcom/amap/api/mapcore/ak;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    return-object v0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 3362
    iput p1, p0, Lcom/amap/api/mapcore/b;->af:I

    .line 3363
    return-void
.end method

.method public declared-synchronized e(Z)V
    .locals 4

    .prologue
    .line 896
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 897
    :try_start_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->as:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    if-nez v0, :cond_0

    .line 898
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    .line 899
    sget-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->av:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 909
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 904
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    .line 905
    sget-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->av:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 906
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->as:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 896
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 669
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Ljava/lang/Boolean;

    .line 674
    :try_start_0
    new-instance v0, Lcom/amap/api/mapcore/b$1;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/b$1;-><init>(Lcom/amap/api/mapcore/b;)V

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->queueEvent(Ljava/lang/Runnable;)V

    .line 680
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    const-wide/16 v0, 0x32

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 681
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->S()V

    .line 682
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 684
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    .line 686
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 687
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 688
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    .line 690
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 691
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 692
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    .line 694
    :cond_2
    sget-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 695
    sget-object v0, Lcom/amap/api/mapcore/b;->au:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->av:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 698
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    if-eqz v0, :cond_4

    .line 699
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/av;->a()V

    .line 702
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    if-eqz v0, :cond_5

    .line 703
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ap;->a()V

    .line 705
    :cond_5
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    if-eqz v0, :cond_6

    .line 706
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/mapcore/au;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/au;->a()V

    .line 708
    :cond_6
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    if-eqz v0, :cond_7

    .line 709
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ad;->a()V

    .line 711
    :cond_7
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    if-eqz v0, :cond_8

    .line 712
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/mapcore/k;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/k;->a()V

    .line 714
    :cond_8
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_9

    .line 715
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->b()V

    .line 717
    :cond_9
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    if-eqz v0, :cond_a

    .line 718
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->b()V

    .line 720
    :cond_a
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    if-eqz v0, :cond_b

    .line 721
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ah;->e()V

    .line 723
    :cond_b
    sget-object v0, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_c

    .line 724
    sget-object v0, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 726
    :cond_c
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    if-eqz v0, :cond_d

    .line 727
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 729
    :cond_d
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_e

    .line 730
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/a;->OnMapDestory(Lcom/autonavi/amap/mapcore/MapCore;)V

    .line 734
    :cond_e
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    if-eqz v0, :cond_f

    .line 735
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ai;->removeAllViews()V

    .line 737
    :cond_f
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V

    .line 739
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->a(Landroid/graphics/drawable/Drawable;)V

    .line 741
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_10

    .line 742
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 743
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->n:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 746
    :cond_10
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_11

    .line 747
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 748
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 772
    :cond_11
    const-string v0, "amap"

    const-string v1, "\u5b8c\u5168\u91ca\u653e"

    const/16 v2, 0x71

    invoke-static {v0, v1, v2}, Lcom/amap/api/mapcore/b/n;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 778
    :goto_0
    return-void

    .line 773
    :catch_0
    move-exception v0

    .line 774
    const-string v1, "amap"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u6ca1\u6709\u5b8c\u5168\u91ca\u653e"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x6f

    invoke-static {v1, v2, v3}, Lcom/amap/api/mapcore/b/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 776
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public f(I)V
    .locals 0

    .prologue
    .line 3387
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/b;->setVisibility(I)V

    .line 3388
    return-void
.end method

.method public f(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1857
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ax:Z

    .line 1858
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    new-instance v1, Lcom/amap/api/mapcore/af;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/amap/api/mapcore/af;-><init>(I)V

    invoke-virtual {v1, p1}, Lcom/amap/api/mapcore/af;->a(Z)Lcom/amap/api/mapcore/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/af;)V

    .line 1860
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    .line 806
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 807
    return-void
.end method

.method public g(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1879
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    if-eqz v0, :cond_4

    .line 1880
    if-eqz p1, :cond_2

    .line 1881
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->W:Lcom/amap/api/mapcore/f;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/LocationSource;->activate(Lcom/amap/api/maps/LocationSource$OnLocationChangedListener;)V

    .line 1882
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ad;->a(Z)V

    .line 1883
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    if-nez v0, :cond_0

    .line 1884
    new-instance v0, Lcom/amap/api/mapcore/ak;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ak;-><init>(Lcom/amap/api/mapcore/r;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    .line 1897
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    .line 1898
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Q:Lcom/amap/api/mapcore/ac;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/ac;->d(Z)V

    .line 1900
    :cond_1
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->T:Z

    .line 1901
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1902
    return-void

    .line 1887
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    if-eqz v0, :cond_3

    .line 1888
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ak;->a()V

    .line 1889
    iput-object v1, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/mapcore/ak;

    .line 1891
    :cond_3
    iput-object v1, p0, Lcom/amap/api/mapcore/b;->aj:Landroid/location/Location;

    .line 1892
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    invoke-interface {v0}, Lcom/amap/api/maps/LocationSource;->deactivate()V

    goto :goto_0

    .line 1895
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/mapcore/ad;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ad;->a(Z)V

    goto :goto_0
.end method

.method public h(Z)Lcom/amap/api/maps/model/CameraPosition;
    .locals 6

    .prologue
    .line 2098
    .line 2099
    if-eqz p1, :cond_0

    .line 2100
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2101
    iget v0, p0, Lcom/amap/api/mapcore/b;->aN:I

    iget v2, p0, Lcom/amap/api/mapcore/b;->aO:I

    invoke-virtual {p0, v0, v2, v1}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2102
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 2106
    :goto_0
    invoke-static {}, Lcom/amap/api/maps/model/CameraPosition;->builder()Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->target(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->bearing(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->tilt(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->build()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    return-object v0

    .line 2104
    :cond_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->U()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    goto :goto_0
.end method

.method h()V
    .locals 2

    .prologue
    .line 833
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 834
    return-void
.end method

.method i()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 837
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    if-nez v0, :cond_0

    .line 854
    :goto_0
    return-void

    .line 840
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    .line 841
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 842
    new-instance v2, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 843
    invoke-virtual {p0, v3, v3, v1}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 844
    invoke-virtual {p0, v0, v3, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 845
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v2, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v2, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/amap/api/mapcore/b/r;->a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)D

    move-result-wide v2

    .line 848
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v1

    float-to-int v1, v1

    .line 849
    iget-object v4, p0, Lcom/amap/api/mapcore/b;->al:[I

    aget v4, v4, v1

    mul-int/2addr v0, v4

    int-to-double v4, v0

    div-double v2, v4, v2

    double-to-int v0, v2

    .line 850
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->al:[I

    aget v1, v2, v1

    invoke-static {v1}, Lcom/amap/api/mapcore/b/r;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 851
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v2, v0}, Lcom/amap/api/mapcore/ap;->a(I)V

    .line 852
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ap;->a(Ljava/lang/String;)V

    .line 853
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/mapcore/ap;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ap;->invalidate()V

    goto :goto_0
.end method

.method i(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3264
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v3, 0x14

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 3266
    return-void

    :cond_0
    move v0, v1

    .line 3264
    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->S:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->S:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 1253
    iget v0, p0, Lcom/amap/api/mapcore/b;->j:I

    return v0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    if-eqz v0, :cond_1

    .line 1263
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ai$a;

    .line 1265
    if-eqz v0, :cond_0

    .line 1266
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->O:Lcom/amap/api/mapcore/v;

    invoke-interface {v1}, Lcom/amap/api/mapcore/v;->e()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    iput-object v1, v0, Lcom/amap/api/mapcore/ai$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 1268
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ai;->a()V

    .line 1269
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1271
    :cond_1
    return-void
.end method

.method public n()Lcom/amap/api/maps/model/CameraPosition;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1280
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aM:Z

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->h(Z)Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public o()F
    .locals 1

    .prologue
    .line 1285
    const/high16 v0, 0x41a00000    # 20.0f

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 951
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1010
    :cond_0
    :goto_0
    return-void

    .line 957
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    if-eqz v0, :cond_2

    .line 958
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v1

    invoke-static {v3, v3, v0, v1, p1}, Lcom/amap/api/mapcore/b;->a(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 959
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 960
    iput-boolean v3, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 962
    :cond_2
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->as:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 975
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p1, v5}, Lcom/amap/api/mapcore/ah;->a(Ljavax/microedition/khronos/opengles/GL10;Z)V

    goto :goto_0

    .line 988
    :cond_3
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-interface {p1, v4, v4, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 990
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 991
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->drawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 992
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 993
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    iget v1, p0, Lcom/amap/api/mapcore/b;->af:I

    invoke-virtual {v0, p1, v3, v1}, Lcom/amap/api/mapcore/p;->a(Ljavax/microedition/khronos/opengles/GL10;ZI)V

    .line 994
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ar;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 995
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0, p1, v3}, Lcom/amap/api/mapcore/ah;->a(Ljavax/microedition/khronos/opengles/GL10;Z)V

    .line 996
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Lcom/amap/api/mapcore/m;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/m;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 999
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1000
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1002
    :cond_4
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->U:Z

    if-nez v0, :cond_0

    .line 1003
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1004
    iput-boolean v5, p0, Lcom/amap/api/mapcore/b;->U:Z

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 575
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->S()V

    .line 580
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/a;->onPause()V

    .line 583
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_1

    .line 584
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->d()V

    .line 586
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 555
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->R()V

    .line 556
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/a;->onResume()V

    .line 558
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_1

    .line 561
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->e()V

    .line 571
    :cond_1
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 12

    .prologue
    const/4 v10, 0x2

    const/16 v2, 0x64

    const/4 v4, 0x1

    const/16 v1, 0x803

    const/4 v5, 0x0

    .line 1034
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v5, v5, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->S:Landroid/graphics/Rect;

    .line 1035
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1036
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1, p2, p3}, Lcom/autonavi/amap/mapcore/MapCore;->surfaceChange(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1037
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1038
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->T()V

    .line 1040
    const/16 v3, 0x78

    if-gt v0, v3, :cond_2

    .line 1041
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x32

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1081
    :goto_0
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x3fd

    move v6, v5

    move v7, v5

    invoke-virtual/range {v2 .. v7}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1082
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v7, 0x3fe

    move v8, v5

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1083
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x3ff

    move v6, v5

    move v7, v5

    invoke-virtual/range {v2 .. v7}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1090
    invoke-virtual {p0, v5}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1092
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1093
    new-instance v0, Lcom/amap/api/mapcore/b$2;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/b$2;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    .line 1101
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1102
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    if-eqz v0, :cond_1

    .line 1103
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    invoke-interface {v0, p1, p2, p3}, Lcom/amap/api/maps/CustomRenderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1105
    :cond_1
    return-void

    .line 1042
    :cond_2
    const/16 v3, 0xa0

    if-gt v0, v3, :cond_5

    .line 1043
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_3

    .line 1044
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x50

    move v7, v1

    move v9, v2

    move v10, v4

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1045
    :cond_3
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v3, 0x1e0

    if-gt v0, v3, :cond_4

    .line 1046
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v2, 0x78

    const/16 v3, 0x3c

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1049
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x50

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1053
    :cond_5
    const/16 v3, 0xd7

    if-gt v0, v3, :cond_6

    .line 1054
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x50

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1055
    :cond_6
    const/16 v3, 0xf0

    if-gt v0, v3, :cond_8

    .line 1056
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_7

    .line 1057
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x3c

    move v7, v1

    move v9, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1059
    :cond_7
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v9, 0x5a

    move v7, v1

    move v8, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1062
    :cond_8
    const/16 v3, 0x140

    if-gt v0, v3, :cond_b

    .line 1063
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v3, 0x500

    if-gt v0, v3, :cond_9

    .line 1064
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x5a

    move v7, v1

    move v9, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1065
    :cond_9
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v2, 0x870

    if-lt v0, v2, :cond_a

    .line 1066
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x32

    const/16 v9, 0xaa

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1068
    :cond_a
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x46

    const/16 v9, 0x96

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1070
    :cond_b
    const/16 v2, 0x1e0

    if-gt v0, v2, :cond_c

    .line 1071
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x46

    const/16 v9, 0x96

    const/4 v10, 0x3

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1073
    :cond_c
    const/16 v2, 0x280

    if-ne v0, v2, :cond_d

    .line 1074
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x32

    const/16 v9, 0xb4

    const/4 v10, 0x3

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 1076
    :cond_d
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x3c

    const/16 v9, 0xb4

    const/4 v10, 0x3

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 7

    .prologue
    const/16 v1, 0x3e9

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1192
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->setRenderMode(I)V

    .line 1193
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->R()V

    .line 1194
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1196
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v4, "style_v3.data"

    invoke-static {v3, v4}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setStyleData([BI)V

    .line 1198
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v4, "style_sv3.data"

    invoke-static {v3, v4}, Lcom/amap/api/mapcore/b/r;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Lcom/autonavi/amap/mapcore/MapCore;->setStyleData([BI)V

    .line 1201
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->surfaceCreate(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1202
    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    .line 1203
    if-eqz v0, :cond_0

    .line 1204
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1205
    const-string v3, "adreno"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-le v0, v3, :cond_7

    .line 1206
    iput-boolean v6, p0, Lcom/amap/api/mapcore/b;->V:Z

    .line 1207
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1214
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1215
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v1, "lineTexture.png"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/r;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    .line 1217
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1218
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->r:Landroid/content/Context;

    const-string v1, "lineDashTexture.png"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/r;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    .line 1224
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/amap/api/mapcore/b/r;->a(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/b;->i:I

    .line 1225
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Landroid/graphics/Bitmap;

    invoke-static {p1, v0, v6}, Lcom/amap/api/mapcore/b/r;->a(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;Z)I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/b;->j:I

    .line 1227
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->k:Landroid/graphics/Bitmap;

    .line 1236
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->T()V

    .line 1237
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1238
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->d:Z

    if-nez v0, :cond_5

    .line 1240
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    const-string v1, "AuthThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1241
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aI:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->d:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1247
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    if-eqz v0, :cond_6

    .line 1248
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Lcom/amap/api/maps/CustomRenderer;

    invoke-interface {v0, p1, p2}, Lcom/amap/api/maps/CustomRenderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1250
    :cond_6
    return-void

    .line 1209
    :cond_7
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->V:Z

    .line 1210
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->q:Lcom/autonavi/amap/mapcore/MapCore;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1243
    :catch_0
    move-exception v0

    .line 1244
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2192
    iget-boolean v2, p0, Lcom/amap/api/mapcore/b;->aK:Z

    if-nez v2, :cond_0

    .line 2209
    :goto_0
    return v0

    .line 2196
    :cond_0
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 2197
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->u:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2198
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->v:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2199
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/a/c;

    invoke-virtual {v2, p1}, Lcom/amap/api/mapcore/a/c;->a(Landroid/view/MotionEvent;)Z

    .line 2200
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->X:Lcom/amap/api/mapcore/a/b;

    invoke-virtual {v2, p1}, Lcom/amap/api/mapcore/a/b;->a(Landroid/view/MotionEvent;)Z

    .line 2201
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2202
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2203
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b;->a(Landroid/view/MotionEvent;)V

    .line 2205
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_2

    .line 2206
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->V()V

    .line 2208
    :cond_2
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    move v0, v1

    .line 2209
    goto :goto_0
.end method

.method public p()F
    .locals 1

    .prologue
    .line 1290
    const/high16 v0, 0x40800000    # 4.0f

    return v0
.end method

.method public q()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1641
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1642
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aa:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/h;->a(Z)V

    .line 1643
    invoke-virtual {p0, v1, v2}, Lcom/amap/api/mapcore/b;->a(ZLcom/amap/api/maps/model/CameraPosition;)V

    .line 1644
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    if-eqz v0, :cond_0

    .line 1645
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-interface {v0}, Lcom/amap/api/maps/AMap$CancelableCallback;->onCancel()V

    .line 1647
    :cond_0
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->ad:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1649
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1650
    return-void
.end method

.method public r()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1790
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V

    .line 1791
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->f:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->a()V

    .line 1792
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->b()V

    .line 1793
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ah;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ah;->c()V

    .line 1794
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1800
    :goto_0
    return-void

    .line 1795
    :catch_0
    move-exception v0

    .line 1796
    const-string v1, "amapApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AMapDelegateImpGLSurfaceView clear erro"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1798
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public s()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1819
    iget v0, p0, Lcom/amap/api/mapcore/b;->p:I

    return v0
.end method

.method public setZOrderOnTop(Z)V
    .locals 0

    .prologue
    .line 1275
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->setZOrderOnTop(Z)V

    .line 1276
    return-void
.end method

.method public t()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1847
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ax:Z

    return v0
.end method

.method public u()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1874
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->T:Z

    return v0
.end method

.method public v()Landroid/location/Location;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/maps/LocationSource;

    if-eqz v0, :cond_0

    .line 1907
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->W:Lcom/amap/api/mapcore/f;

    iget-object v0, v0, Lcom/amap/api/mapcore/f;->a:Landroid/location/Location;

    .line 1909
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Lcom/amap/api/mapcore/ac;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1925
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Q:Lcom/amap/api/mapcore/ac;

    return-object v0
.end method

.method public x()Lcom/amap/api/mapcore/z;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1930
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->P:Lcom/amap/api/mapcore/z;

    return-object v0
.end method

.method public y()Lcom/amap/api/maps/AMap$OnCameraChangeListener;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1950
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    return-object v0
.end method

.method public z()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1997
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/ai;

    return-object v0
.end method
