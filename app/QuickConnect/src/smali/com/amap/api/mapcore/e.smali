.class Lcom/amap/api/mapcore/e;
.super Landroid/os/Handler;
.source "AMapDelegateImpGLSurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/b;


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 0

    .prologue
    .line 2927
    iput-object p1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2929
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->D(Lcom/amap/api/mapcore/b;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3120
    :cond_0
    :goto_0
    return-void

    .line 2932
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 2933
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 3119
    :cond_2
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0

    .line 2935
    :pswitch_1
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->E(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2938
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->C(Lcom/amap/api/mapcore/b;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/amap/api/mapcore/g;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 2943
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/amap/api/maps/model/CameraPosition;

    .line 2944
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->F(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2945
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->F(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/amap/api/maps/AMap$OnCameraChangeListener;->onCameraChange(Lcom/amap/api/maps/model/CameraPosition;)V

    goto :goto_1

    .line 2950
    :pswitch_3
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->G(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLoadedListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2951
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->G(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLoadedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/maps/AMap$OnMapLoadedListener;->onMapLoaded()V

    goto :goto_1

    .line 2956
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/amap/api/mapcore/i;

    .line 2957
    if-eqz v0, :cond_2

    .line 2958
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    goto :goto_1

    .line 2963
    :pswitch_5
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    if-eqz v0, :cond_3

    .line 2964
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/av;

    iget-object v3, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v3}, Lcom/amap/api/mapcore/b;->B()F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/amap/api/mapcore/av;->a(F)V

    .line 2969
    :cond_3
    :pswitch_6
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2971
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 2988
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->b()I

    move-result v0

    iget-object v3, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)I

    move-result v3

    sub-int/2addr v0, v3

    .line 2989
    iget-object v3, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amap/api/mapcore/h;->c()I

    move-result v3

    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->p(Lcom/amap/api/mapcore/b;)I

    move-result v4

    sub-int/2addr v3, v4

    .line 2990
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v5, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v5}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/amap/api/mapcore/h;->b()I

    move-result v5

    invoke-static {v4, v5}, Lcom/amap/api/mapcore/b;->b(Lcom/amap/api/mapcore/b;I)I

    .line 2991
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v5, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v5}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/amap/api/mapcore/h;->c()I

    move-result v5

    invoke-static {v4, v5}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;I)I

    .line 2992
    new-instance v4, Lcom/autonavi/amap/mapcore/FPoint;

    iget-object v5, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v5}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    int-to-float v0, v0

    iget-object v5, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v5}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    int-to-float v3, v3

    invoke-direct {v4, v0, v3}, Lcom/autonavi/amap/mapcore/FPoint;-><init>(FF)V

    .line 2994
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2995
    iget-object v3, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v3

    iget v5, v4, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    float-to-int v5, v5

    iget v4, v4, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    float-to-int v4, v4

    invoke-virtual {v3, v5, v4, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2996
    new-instance v3, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2997
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v4

    iget v5, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v4, v5, v0, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2998
    invoke-static {v3}, Lcom/amap/api/mapcore/i;->a(Lcom/autonavi/amap/mapcore/IPoint;)Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 3000
    iget-object v3, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 3001
    iput-boolean v1, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 3003
    :cond_4
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    goto/16 :goto_1

    .line 2973
    :pswitch_7
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    iget-object v3, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amap/api/mapcore/h;->b()I

    move-result v3

    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v4

    invoke-virtual {v4}, Lcom/amap/api/mapcore/h;->c()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    iget-object v3, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amap/api/mapcore/h;->d()F

    move-result v3

    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v4

    invoke-virtual {v4}, Lcom/amap/api/mapcore/h;->e()F

    move-result v4

    iget-object v5, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v5}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/amap/api/mapcore/h;->f()F

    move-result v5

    invoke-static {v0, v3, v4, v5}, Lcom/amap/api/mapcore/i;->a(Lcom/autonavi/amap/mapcore/IPoint;FFF)Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2980
    iget-object v3, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2981
    iput-boolean v1, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 2983
    :cond_5
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/mapcore/h;->k()Z

    move-result v1

    iput-boolean v1, v0, Lcom/amap/api/mapcore/i;->n:Z

    .line 2984
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    goto/16 :goto_1

    .line 3010
    :pswitch_8
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->H(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/k;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3019
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->H(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/k;->b()V

    goto/16 :goto_1

    .line 3026
    :pswitch_9
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->i()V

    goto/16 :goto_1

    .line 3030
    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 3031
    if-eqz v0, :cond_a

    .line 3032
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3033
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->I(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/au;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 3034
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->I(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/au;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/amap/api/mapcore/au;->onDraw(Landroid/graphics/Canvas;)V

    .line 3036
    :cond_6
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->x(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->J(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/v;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 3037
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->x(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 3038
    if-eqz v1, :cond_7

    .line 3039
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->x(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 3040
    iget-object v5, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v5}, Lcom/amap/api/mapcore/b;->x(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    .line 3041
    int-to-float v4, v4

    int-to-float v5, v5

    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v3, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 3044
    :cond_7
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->K(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 3045
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->K(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    move-result-object v1

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v4}, Lcom/amap/api/mapcore/b;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-interface {v1, v3}, Lcom/amap/api/maps/AMap$onMapPrintScreenListener;->onMapPrint(Landroid/graphics/drawable/Drawable;)V

    .line 3048
    :cond_8
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->L(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 3049
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->L(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/amap/api/maps/AMap$OnMapScreenShotListener;->onMapScreenShot(Landroid/graphics/Bitmap;)V

    .line 3059
    :cond_9
    :goto_2
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v7}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    .line 3060
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v7}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    goto/16 :goto_1

    .line 3052
    :cond_a
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->K(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 3053
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->K(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/amap/api/maps/AMap$onMapPrintScreenListener;->onMapPrint(Landroid/graphics/drawable/Drawable;)V

    .line 3055
    :cond_b
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->L(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 3056
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->L(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/amap/api/maps/AMap$OnMapScreenShotListener;->onMapScreenShot(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 3064
    :pswitch_b
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->m(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$CancelableCallback;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->M(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 3065
    :cond_c
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0, v1, v7}, Lcom/amap/api/mapcore/b;->a(ZLcom/amap/api/maps/model/CameraPosition;)V

    .line 3066
    :cond_d
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->m(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$CancelableCallback;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 3067
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->j(Lcom/amap/api/mapcore/b;Z)Z

    .line 3068
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->m(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$CancelableCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/maps/AMap$CancelableCallback;->onFinish()V

    .line 3069
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v2}, Lcom/amap/api/mapcore/b;->j(Lcom/amap/api/mapcore/b;Z)Z

    .line 3071
    :cond_e
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->N(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 3072
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v7}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$CancelableCallback;)Lcom/amap/api/maps/AMap$CancelableCallback;

    goto/16 :goto_1

    .line 3074
    :cond_f
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v2}, Lcom/amap/api/mapcore/b;->k(Lcom/amap/api/mapcore/b;Z)Z

    goto/16 :goto_1

    .line 3079
    :pswitch_c
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    .line 3080
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v1

    .line 3081
    if-lez v0, :cond_10

    if-gtz v1, :cond_11

    .line 3082
    :cond_10
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v7}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;)Lcom/amap/api/maps/model/LatLngBounds;

    goto/16 :goto_1

    .line 3085
    :cond_11
    new-instance v3, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 3086
    iget-object v4, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v4, v2, v2, v3}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 3087
    new-instance v4, Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v3, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v8, v3, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 3088
    iget-object v5, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v5, v0, v2, v3}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 3089
    new-instance v5, Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v3, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v8, v3, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 3090
    iget-object v6, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v6, v2, v1, v3}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 3091
    new-instance v6, Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v3, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v10, v3, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v6, v8, v9, v10, v11}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 3092
    iget-object v7, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v7, v0, v1, v3}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 3093
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v3, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v10, v3, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v0, v8, v9, v10, v11}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 3094
    iget-object v1, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {}, Lcom/amap/api/maps/model/LatLngBounds;->builder()Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;)Lcom/amap/api/maps/model/LatLngBounds;

    goto/16 :goto_1

    .line 3100
    :pswitch_d
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->E()V

    .line 3101
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/b;->i(Z)V

    goto/16 :goto_1

    .line 3105
    :pswitch_e
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->j()I

    move-result v0

    if-eq v0, v1, :cond_13

    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_13

    .line 3108
    :cond_12
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ar;->b(Z)V

    .line 3110
    :cond_13
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_2

    .line 3111
    iget-object v0, p0, Lcom/amap/api/mapcore/e;->a:Lcom/amap/api/mapcore/b;

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->c:Lcom/amap/api/mapcore/ar;

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_14

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcom/amap/api/mapcore/ar;->a(Z)V

    goto/16 :goto_1

    :cond_14
    move v0, v2

    goto :goto_3

    .line 2933
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_5
    .end packed-switch

    .line 2971
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_7
    .end packed-switch
.end method
