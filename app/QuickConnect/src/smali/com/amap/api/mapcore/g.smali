.class public Lcom/amap/api/mapcore/g;
.super Ljava/lang/Object;
.source "AuthManager.java"


# static fields
.field public static a:I

.field static b:Ljava/lang/String;

.field static c:Ljava/net/HttpURLConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, -0x1

    sput v0, Lcom/amap/api/mapcore/g;->a:I

    .line 33
    const-string v0, ""

    sput-object v0, Lcom/amap/api/mapcore/g;->b:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a(Ljava/net/HttpURLConnection;)Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps/AMapException;
        }
    .end annotation

    .prologue
    .line 77
    .line 79
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 81
    new-instance v1, Ljava/io/PushbackInputStream;

    const/4 v2, 0x2

    invoke-direct {v1, v0, v2}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 82
    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 83
    invoke-virtual {v1, v0}, Ljava/io/PushbackInputStream;->read([B)I

    .line 84
    invoke-virtual {v1, v0}, Ljava/io/PushbackInputStream;->unread([B)V

    .line 85
    const/4 v2, 0x0

    aget-byte v2, v0, v2

    const/16 v3, 0x1f

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    aget-byte v0, v0, v2

    const/16 v2, -0x75

    if-ne v0, v2, :cond_0

    .line 86
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownServiceException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 88
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v0, Lcom/amap/api/maps/AMapException;

    const-string v1, "\u534f\u8bae\u89e3\u6790\u9519\u8bef - ProtocolException"

    invoke-direct {v0, v1}, Lcom/amap/api/maps/AMapException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :catch_1
    move-exception v0

    .line 93
    new-instance v0, Lcom/amap/api/maps/AMapException;

    const-string v1, "\u672a\u77e5\u4e3b\u673a - UnKnowHostException"

    invoke-direct {v0, v1}, Lcom/amap/api/maps/AMapException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :catch_2
    move-exception v0

    .line 95
    new-instance v0, Lcom/amap/api/maps/AMapException;

    const-string v1, "\u670d\u52a1\u5668\u8fde\u63a5\u5931\u8d25 - UnknownServiceException"

    invoke-direct {v0, v1}, Lcom/amap/api/maps/AMapException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :catch_3
    move-exception v0

    .line 97
    new-instance v0, Lcom/amap/api/maps/AMapException;

    const-string v1, "IO \u64cd\u4f5c\u5f02\u5e38 - IOException"

    invoke-direct {v0, v1}, Lcom/amap/api/maps/AMapException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "http://restapi.amap.com/log/init"

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps/AMapException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 38
    const-class v2, Lcom/amap/api/mapcore/g;

    monitor-enter v2

    .line 41
    :try_start_0
    invoke-static {}, Lcom/amap/api/mapcore/g;->b()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 44
    :try_start_1
    invoke-static {}, Lcom/amap/api/mapcore/g;->a()Ljava/lang/String;

    move-result-object v3

    .line 45
    invoke-static {p0}, Lcom/amap/api/mapcore/b/e;->c(Landroid/content/Context;)Ljava/net/Proxy;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/amap/api/mapcore/b/i;->a(Ljava/lang/String;[BLjava/net/Proxy;)Ljava/net/HttpURLConnection;

    move-result-object v0

    sput-object v0, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    .line 47
    sget-object v0, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    invoke-static {v0}, Lcom/amap/api/mapcore/g;->a(Ljava/net/HttpURLConnection;)Ljava/io/InputStream;

    move-result-object v1

    .line 48
    invoke-static {v1}, Lcom/amap/api/mapcore/g;->a(Ljava/io/InputStream;)Z
    :try_end_1
    .catch Lcom/amap/api/maps/AMapException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 54
    if-eqz v1, :cond_0

    .line 56
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 62
    :cond_0
    :try_start_3
    sget-object v1, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_1

    .line 63
    sget-object v1, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 64
    const/4 v1, 0x0

    sput-object v1, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :cond_1
    monitor-exit v2

    return v0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    :try_start_4
    new-instance v0, Lcom/amap/api/maps/AMapException;

    const-string v1, "IO \u64cd\u4f5c\u5f02\u5e38 - IOException"

    invoke-direct {v0, v1}, Lcom/amap/api/maps/AMapException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 49
    :catch_1
    move-exception v0

    .line 50
    :try_start_5
    const-string v3, "AuthFailure"

    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->getErrorMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->printStackTrace()V

    .line 52
    new-instance v3, Lcom/amap/api/maps/AMapException;

    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/amap/api/maps/AMapException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 54
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_2

    .line 56
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 62
    :cond_2
    :try_start_7
    sget-object v1, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_3

    .line 63
    sget-object v1, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 64
    const/4 v1, 0x0

    sput-object v1, Lcom/amap/api/mapcore/g;->c:Ljava/net/HttpURLConnection;

    :cond_3
    throw v0

    .line 57
    :catch_2
    move-exception v0

    .line 58
    new-instance v0, Lcom/amap/api/maps/AMapException;

    const-string v1, "IO \u64cd\u4f5c\u5f02\u5e38 - IOException"

    invoke-direct {v0, v1}, Lcom/amap/api/maps/AMapException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method private static a(Ljava/io/InputStream;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps/AMapException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 103
    .line 105
    :try_start_0
    new-instance v2, Ljava/lang/String;

    invoke-static {p0}, Lcom/amap/api/mapcore/b/e;->a(Ljava/io/InputStream;)[B

    move-result-object v3

    const-string v4, "utf-8"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 106
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 107
    const-string v2, "status"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    const-string v2, "status"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 109
    if-ne v2, v0, :cond_3

    .line 110
    const/4 v2, 0x1

    sput v2, Lcom/amap/api/mapcore/g;->a:I

    .line 115
    :cond_0
    :goto_0
    const-string v2, "info"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 116
    const-string v2, "info"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/amap/api/mapcore/g;->b:Ljava/lang/String;

    .line 118
    :cond_1
    sget v2, Lcom/amap/api/mapcore/g;->a:I

    if-nez v2, :cond_4

    .line 119
    const-string v2, "AuthFailure"

    sget-object v3, Lcom/amap/api/mapcore/g;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    new-instance v2, Lcom/amap/api/maps/AMapException;

    const-string v3, "key\u9274\u6743\u5931\u8d25"

    invoke-direct {v2, v3}, Lcom/amap/api/maps/AMapException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :catchall_0
    move-exception v2

    sget v2, Lcom/amap/api/mapcore/g;->a:I

    if-ne v2, v0, :cond_5

    .line 127
    :cond_2
    :goto_1
    return v0

    .line 111
    :cond_3
    if-nez v2, :cond_0

    .line 112
    const/4 v2, 0x0

    :try_start_1
    sput v2, Lcom/amap/api/mapcore/g;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124
    :cond_4
    sget v2, Lcom/amap/api/mapcore/g;->a:I

    if-eq v2, v0, :cond_2

    move v0, v1

    .line 127
    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method private static b()[B
    .locals 2

    .prologue
    .line 133
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 134
    const-string v1, "resType=json&encode=UTF-8&ec=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "utf-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 140
    const/4 v0, 0x0

    new-array v0, v0, [B

    goto :goto_0
.end method
