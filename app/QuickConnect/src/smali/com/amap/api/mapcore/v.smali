.class public interface abstract Lcom/amap/api/mapcore/v;
.super Ljava/lang/Object;
.source "IMarkerDelegate.java"


# virtual methods
.method public abstract a(F)V
.end method

.method public abstract a(FF)V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(Lcom/amap/api/maps/model/BitmapDescriptor;)V
.end method

.method public abstract a(Lcom/amap/api/maps/model/LatLng;)V
.end method

.method public abstract a(Ljava/lang/Object;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps/model/BitmapDescriptor;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljavax/microedition/khronos/opengles/GL10;Lcom/amap/api/mapcore/r;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract a(Lcom/amap/api/mapcore/v;)Z
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()Landroid/graphics/Rect;
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()Lcom/amap/api/maps/model/LatLng;
.end method

.method public abstract e()Lcom/amap/api/maps/model/LatLng;
.end method

.method public abstract f()Lcom/autonavi/amap/mapcore/IPoint;
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()I
.end method

.method public abstract i()Ljava/lang/String;
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public abstract k()Z
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract n()Z
.end method

.method public abstract o()Z
.end method

.method public abstract p()V
.end method

.method public abstract q()I
.end method

.method public abstract r()Ljava/lang/Object;
.end method

.method public abstract s()I
.end method

.method public abstract t()Z
.end method

.method public abstract u()I
.end method

.method public abstract v()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/maps/model/BitmapDescriptor;",
            ">;"
        }
    .end annotation
.end method

.method public abstract w()Z
.end method

.method public abstract x()V
.end method
