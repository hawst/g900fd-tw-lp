.class Lcom/amap/api/mapcore/b$e;
.super Ljava/lang/Object;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field a:Lcom/amap/api/mapcore/i;

.field final synthetic b:Lcom/amap/api/mapcore/b;

.field private c:F

.field private d:Lcom/autonavi/amap/mapcore/IPoint;


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 1

    .prologue
    .line 2212
    iput-object p1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2217
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b$e;->c:F

    .line 2218
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b$e;->d:Lcom/autonavi/amap/mapcore/IPoint;

    .line 2219
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/i;

    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V
    .locals 0

    .prologue
    .line 2212
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b$e;-><init>(Lcom/amap/api/mapcore/b;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2227
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2237
    :cond_0
    :goto_0
    return v6

    .line 2230
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    .line 2231
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_2

    float-to-double v2, v0

    const-wide v4, 0x3ff147ae147ae148L    # 1.08

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_2

    float-to-double v2, v0

    const-wide v4, 0x3fed70a3d70a3d71L    # 0.92

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 2232
    :cond_2
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;Z)Z

    .line 2233
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {}, Lcom/amap/api/mapcore/b;->P()D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 2234
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/i;

    iget v2, p0, Lcom/amap/api/mapcore/b$e;->c:F

    add-float/2addr v0, v2

    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->b(F)F

    move-result v0

    iput v0, v1, Lcom/amap/api/mapcore/i;->d:F

    .line 2235
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/i;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2246
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ac;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/ac;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 2270
    :cond_0
    :goto_0
    return v0

    .line 2249
    :catch_0
    move-exception v1

    .line 2250
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2255
    :cond_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;I)I

    .line 2256
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2259
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2260
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/i;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;)Z

    move-result v2

    iput-boolean v2, v1, Lcom/amap/api/mapcore/i;->n:Z

    .line 2261
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/i;

    sget-object v2, Lcom/amap/api/mapcore/i$a;->g:Lcom/amap/api/mapcore/i$a;

    iput-object v2, v1, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 2262
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->g(Lcom/amap/api/mapcore/b;)I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;)I

    move-result v3

    iget-object v4, p0, Lcom/amap/api/mapcore/b$e;->d:Lcom/autonavi/amap/mapcore/IPoint;

    invoke-virtual {v1, v2, v3, v4}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 2263
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/i;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$e;->d:Lcom/autonavi/amap/mapcore/IPoint;

    iput-object v2, v1, Lcom/amap/api/mapcore/i;->o:Lcom/autonavi/amap/mapcore/IPoint;

    .line 2267
    :goto_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;Z)Z

    .line 2268
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/b$e;->c:F

    .line 2270
    const/4 v0, 0x1

    goto :goto_0

    .line 2265
    :cond_2
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/i;

    sget-object v2, Lcom/amap/api/mapcore/i$a;->i:Lcom/amap/api/mapcore/i$a;

    iput-object v2, v1, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    goto :goto_1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    .line 2275
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b$e;->c:F

    .line 2276
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2277
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;Z)Z

    .line 2278
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2280
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 2281
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 2292
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->b:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->j(Lcom/amap/api/mapcore/b;)V

    .line 2293
    return-void
.end method
