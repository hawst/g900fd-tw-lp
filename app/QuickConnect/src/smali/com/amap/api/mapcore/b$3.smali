.class Lcom/amap/api/mapcore/b$3;
.super Ljava/lang/Object;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Lcom/amap/api/maps/AMap$CancelableCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/maps/model/LatLngBounds;

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:J

.field final synthetic f:Lcom/amap/api/maps/AMap$CancelableCallback;

.field final synthetic g:Lcom/amap/api/mapcore/b;


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;IIIJLcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 0

    .prologue
    .line 1606
    iput-object p1, p0, Lcom/amap/api/mapcore/b$3;->g:Lcom/amap/api/mapcore/b;

    iput-object p2, p0, Lcom/amap/api/mapcore/b$3;->a:Lcom/amap/api/maps/model/LatLngBounds;

    iput p3, p0, Lcom/amap/api/mapcore/b$3;->b:I

    iput p4, p0, Lcom/amap/api/mapcore/b$3;->c:I

    iput p5, p0, Lcom/amap/api/mapcore/b$3;->d:I

    iput-wide p6, p0, Lcom/amap/api/mapcore/b$3;->e:J

    iput-object p8, p0, Lcom/amap/api/mapcore/b$3;->f:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    .prologue
    .line 1622
    iget-object v0, p0, Lcom/amap/api/mapcore/b$3;->f:Lcom/amap/api/maps/AMap$CancelableCallback;

    if-eqz v0, :cond_0

    .line 1623
    iget-object v0, p0, Lcom/amap/api/mapcore/b$3;->f:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-interface {v0}, Lcom/amap/api/maps/AMap$CancelableCallback;->onCancel()V

    .line 1624
    :cond_0
    return-void
.end method

.method public onFinish()V
    .locals 5

    .prologue
    .line 1611
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$3;->g:Lcom/amap/api/mapcore/b;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$3;->a:Lcom/amap/api/maps/model/LatLngBounds;

    iget v2, p0, Lcom/amap/api/mapcore/b$3;->b:I

    iget v3, p0, Lcom/amap/api/mapcore/b$3;->c:I

    iget v4, p0, Lcom/amap/api/mapcore/b$3;->d:I

    invoke-static {v1, v2, v3, v4}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLngBounds;III)Lcom/amap/api/mapcore/i;

    move-result-object v1

    iget-wide v2, p0, Lcom/amap/api/mapcore/b$3;->e:J

    iget-object v4, p0, Lcom/amap/api/mapcore/b$3;->f:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1618
    :goto_0
    return-void

    .line 1615
    :catch_0
    move-exception v0

    .line 1616
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
