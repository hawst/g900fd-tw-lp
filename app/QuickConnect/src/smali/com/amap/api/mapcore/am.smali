.class Lcom/amap/api/mapcore/am;
.super Ljava/lang/Object;
.source "PolygonDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/x;


# static fields
.field private static o:F


# instance fields
.field private a:Lcom/amap/api/mapcore/r;

.field private b:F

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:F

.field private f:I

.field private g:I

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore/IPoint;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/nio/FloatBuffer;

.field private j:Ljava/nio/FloatBuffer;

.field private k:I

.field private l:I

.field private m:Lcom/amap/api/maps/model/LatLngBounds;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 329
    const v0, 0x501502f9    # 1.0E10f

    sput v0, Lcom/amap/api/mapcore/am;->o:F

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/mapcore/r;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/am;->b:F

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/am;->c:Z

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    .line 35
    iput v1, p0, Lcom/amap/api/mapcore/am;->k:I

    iput v1, p0, Lcom/amap/api/mapcore/am;->l:I

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->m:Lcom/amap/api/maps/model/LatLngBounds;

    .line 37
    iput-boolean v1, p0, Lcom/amap/api/mapcore/am;->n:Z

    .line 40
    iput-object p1, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    .line 42
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->d:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static a([Lcom/autonavi/amap/mapcore/FPoint;)[Lcom/autonavi/amap/mapcore/FPoint;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 332
    array-length v2, p0

    .line 333
    new-array v3, v2, [Lcom/autonavi/amap/mapcore/FPoint;

    move v1, v0

    .line 334
    :goto_0
    if-ge v1, v2, :cond_0

    .line 335
    new-instance v4, Lcom/autonavi/amap/mapcore/FPoint;

    aget-object v5, p0, v1

    iget v5, v5, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    sget v6, Lcom/amap/api/mapcore/am;->o:F

    mul-float/2addr v5, v6

    aget-object v6, p0, v1

    iget v6, v6, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    sget v7, Lcom/amap/api/mapcore/am;->o:F

    mul-float/2addr v6, v7

    invoke-direct {v4, v5, v6}, Lcom/autonavi/amap/mapcore/FPoint;-><init>(FF)V

    aput-object v4, v3, v1

    .line 334
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 338
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 339
    invoke-static {v3, v2}, Lcom/amap/api/mapcore/b/q;->a([Lcom/autonavi/amap/mapcore/FPoint;Ljava/util/List;)Z

    .line 340
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 341
    new-array v4, v3, [Lcom/autonavi/amap/mapcore/FPoint;

    move v1, v0

    .line 342
    :goto_1
    if-ge v1, v3, :cond_1

    .line 343
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    aput-object v0, v4, v1

    .line 344
    aget-object v5, v4, v1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/FPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    sget v6, Lcom/amap/api/mapcore/am;->o:F

    div-float/2addr v0, v6

    iput v0, v5, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    .line 345
    aget-object v5, v4, v1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/FPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    sget v6, Lcom/amap/api/mapcore/am;->o:F

    div-float/2addr v0, v6

    iput v0, v5, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    .line 342
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 347
    :cond_1
    return-object v4
.end method


# virtual methods
.method public a(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 75
    iput p1, p0, Lcom/amap/api/mapcore/am;->b:F

    .line 76
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->J()V

    .line 77
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 78
    return-void
.end method

.method public a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 287
    iput p1, p0, Lcom/amap/api/mapcore/am;->f:I

    .line 288
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 289
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/am;->b(Ljava/util/List;)V

    .line 65
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 66
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/am;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/am;->k:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/am;->l:I

    if-nez v0, :cond_3

    .line 262
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->g()V

    .line 264
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/amap/api/mapcore/am;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/amap/api/mapcore/am;->k:I

    if-lez v0, :cond_4

    iget v0, p0, Lcom/amap/api/mapcore/am;->l:I

    if-lez v0, :cond_4

    .line 266
    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->i()I

    move-result v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->k()I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->h()F

    move-result v4

    iget-object v5, p0, Lcom/amap/api/mapcore/am;->j:Ljava/nio/FloatBuffer;

    iget v6, p0, Lcom/amap/api/mapcore/am;->k:I

    iget v7, p0, Lcom/amap/api/mapcore/am;->l:I

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/amap/api/mapcore/n;->a(Ljavax/microedition/khronos/opengles/GL10;IILjava/nio/FloatBuffer;FLjava/nio/FloatBuffer;II)V

    .line 271
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/am;->n:Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/amap/api/mapcore/am;->c:Z

    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 89
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 240
    iget-object v2, p0, Lcom/amap/api/mapcore/am;->m:Lcom/amap/api/maps/model/LatLngBounds;

    if-nez v2, :cond_1

    .line 247
    :cond_0
    :goto_0
    return v0

    .line 243
    :cond_1
    iget-object v2, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->D()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v2

    .line 244
    if-nez v2, :cond_2

    move v0, v1

    .line 245
    goto :goto_0

    .line 247
    :cond_2
    iget-object v3, p0, Lcom/amap/api/mapcore/am;->m:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps/model/LatLngBounds;->contains(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/mapcore/am;->m:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps/model/LatLngBounds;->intersects(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/w;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/w;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    :cond_0
    const/4 v0, 0x1

    .line 103
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/LatLng;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->j()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/amap/api/mapcore/b/r;->a(Lcom/amap/api/maps/model/LatLng;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Ljava/lang/String;)Z

    .line 51
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 52
    return-void
.end method

.method public b(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 276
    iput p1, p0, Lcom/amap/api/mapcore/am;->e:F

    .line 277
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 278
    return-void
.end method

.method public b(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 298
    iput p1, p0, Lcom/amap/api/mapcore/am;->g:I

    .line 299
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 300
    return-void
.end method

.method b(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 107
    .line 108
    invoke-static {}, Lcom/amap/api/maps/model/LatLngBounds;->builder()Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v7

    .line 109
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 110
    if-eqz p1, :cond_2

    .line 111
    const/4 v0, 0x0

    .line 112
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/model/LatLng;

    .line 113
    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 115
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 116
    iget-object v1, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v1 .. v6}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 117
    iget-object v1, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-virtual {v7, v0}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-object v1, v0

    .line 120
    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 122
    const/4 v0, 0x1

    if-le v2, v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 124
    iget-object v1, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    add-int/lit8 v3, v2, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/autonavi/amap/mapcore/IPoint;

    .line 125
    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-ne v3, v4, :cond_2

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-ne v0, v1, :cond_2

    .line 126
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    add-int/lit8 v1, v2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 130
    :cond_2
    invoke-virtual {v7}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->m:Lcom/amap/api/maps/model/LatLngBounds;

    .line 131
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_3

    .line 132
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 134
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_4

    .line 135
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->j:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 137
    :cond_4
    iput v9, p0, Lcom/amap/api/mapcore/am;->k:I

    .line 138
    iput v9, p0, Lcom/amap/api/mapcore/am;->l:I

    .line 139
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, v9}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 140
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 57
    const-string v0, "Polygon"

    invoke-static {v0}, Lcom/amap/api/mapcore/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->d:Ljava/lang/String;

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 82
    iget v0, p0, Lcom/amap/api/mapcore/am;->b:F

    return v0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/amap/api/mapcore/am;->c:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 205
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const v9, 0x501502f9    # 1.0E10f

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 164
    iput-boolean v2, p0, Lcom/amap/api/mapcore/am;->n:Z

    .line 165
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Lcom/autonavi/amap/mapcore/FPoint;

    .line 166
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    new-array v4, v0, [F

    .line 168
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 169
    new-instance v6, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    aput-object v6, v3, v1

    .line 170
    iget-object v6, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    iget v7, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    aget-object v8, v3, v1

    invoke-interface {v6, v7, v0, v8}, Lcom/amap/api/mapcore/r;->b(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 171
    mul-int/lit8 v0, v1, 0x3

    aget-object v6, v3, v1

    iget v6, v6, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v6, v4, v0

    .line 172
    mul-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, 0x1

    aget-object v6, v3, v1

    iget v6, v6, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v6, v4, v0

    .line 173
    mul-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, 0x2

    aput v10, v4, v0

    .line 174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 175
    goto :goto_0

    .line 177
    :cond_0
    invoke-static {v3}, Lcom/amap/api/mapcore/am;->a([Lcom/autonavi/amap/mapcore/FPoint;)[Lcom/autonavi/amap/mapcore/FPoint;

    move-result-object v0

    .line 178
    array-length v1, v0

    if-nez v1, :cond_1

    .line 179
    sget v0, Lcom/amap/api/mapcore/am;->o:F

    cmpl-float v0, v0, v9

    if-nez v0, :cond_2

    .line 180
    const v0, 0x4cbebc20    # 1.0E8f

    sput v0, Lcom/amap/api/mapcore/am;->o:F

    .line 184
    :goto_1
    invoke-static {v3}, Lcom/amap/api/mapcore/am;->a([Lcom/autonavi/amap/mapcore/FPoint;)[Lcom/autonavi/amap/mapcore/FPoint;

    move-result-object v0

    .line 186
    :cond_1
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x3

    new-array v5, v1, [F

    .line 188
    array-length v6, v0

    move v1, v2

    :goto_2
    if-ge v2, v6, :cond_3

    aget-object v7, v0, v2

    .line 189
    mul-int/lit8 v8, v1, 0x3

    iget v9, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v9, v5, v8

    .line 190
    mul-int/lit8 v8, v1, 0x3

    add-int/lit8 v8, v8, 0x1

    iget v7, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v7, v5, v8

    .line 191
    mul-int/lit8 v7, v1, 0x3

    add-int/lit8 v7, v7, 0x2

    aput v10, v5, v7

    .line 192
    add-int/lit8 v1, v1, 0x1

    .line 188
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 182
    :cond_2
    sput v9, Lcom/amap/api/mapcore/am;->o:F

    goto :goto_1

    .line 195
    :cond_3
    array-length v1, v3

    iput v1, p0, Lcom/amap/api/mapcore/am;->k:I

    .line 196
    array-length v0, v0

    iput v0, p0, Lcom/amap/api/mapcore/am;->l:I

    .line 198
    invoke-static {v4}, Lcom/amap/api/mapcore/b/r;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    .line 199
    invoke-static {v5}, Lcom/amap/api/mapcore/b/r;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->j:Ljava/nio/FloatBuffer;

    .line 201
    return-void
.end method

.method public h()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 282
    iget v0, p0, Lcom/amap/api/mapcore/am;->e:F

    return v0
.end method

.method public i()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 293
    iget v0, p0, Lcom/amap/api/mapcore/am;->f:I

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/amap/api/mapcore/am;->l()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public k()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 304
    iget v0, p0, Lcom/amap/api/mapcore/am;->g:I

    return v0
.end method

.method l()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 144
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 145
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 146
    if-eqz v0, :cond_0

    .line 147
    new-instance v3, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 148
    iget-object v4, p0, Lcom/amap/api/mapcore/am;->a:Lcom/amap/api/mapcore/r;

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-interface {v4, v5, v0, v3}, Lcom/amap/api/mapcore/r;->b(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 149
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v3, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v3, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 154
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public n()V
    .locals 2

    .prologue
    .line 361
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 363
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->i:Ljava/nio/FloatBuffer;

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/am;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 366
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/am;->j:Ljava/nio/FloatBuffer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :cond_1
    :goto_0
    return-void

    .line 368
    :catch_0
    move-exception v0

    .line 369
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 370
    const-string v0, "destroy erro"

    const-string v1, "PolygonDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 381
    iget-boolean v0, p0, Lcom/amap/api/mapcore/am;->n:Z

    return v0
.end method
