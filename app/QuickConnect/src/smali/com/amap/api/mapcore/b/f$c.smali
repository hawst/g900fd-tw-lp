.class public final Lcom/amap/api/mapcore/b/f$c;
.super Ljava/lang/Object;
.source "DiskLruCache.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/b/f;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:[Ljava/io/InputStream;


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/b/f;Ljava/lang/String;J[Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 752
    iput-object p1, p0, Lcom/amap/api/mapcore/b/f$c;->a:Lcom/amap/api/mapcore/b/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 753
    iput-object p2, p0, Lcom/amap/api/mapcore/b/f$c;->b:Ljava/lang/String;

    .line 754
    iput-wide p3, p0, Lcom/amap/api/mapcore/b/f$c;->c:J

    .line 755
    iput-object p5, p0, Lcom/amap/api/mapcore/b/f$c;->d:[Ljava/io/InputStream;

    .line 756
    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/b/f;Ljava/lang/String;J[Ljava/io/InputStream;Lcom/amap/api/mapcore/b/g;)V
    .locals 1

    .prologue
    .line 747
    invoke-direct/range {p0 .. p5}, Lcom/amap/api/mapcore/b/f$c;-><init>(Lcom/amap/api/mapcore/b/f;Ljava/lang/String;J[Ljava/io/InputStream;)V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f$c;->d:[Ljava/io/InputStream;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public close()V
    .locals 4

    .prologue
    .line 783
    iget-object v1, p0, Lcom/amap/api/mapcore/b/f$c;->d:[Ljava/io/InputStream;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 784
    invoke-static {v3}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/Closeable;)V

    .line 783
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 786
    :cond_0
    return-void
.end method
