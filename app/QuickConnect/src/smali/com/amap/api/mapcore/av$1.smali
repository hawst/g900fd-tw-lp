.class Lcom/amap/api/mapcore/av$1;
.super Ljava/lang/Object;
.source "ZoomControllerView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amap/api/mapcore/av;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/av;


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/av;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v0, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-static {v0}, Lcom/amap/api/mapcore/av;->a(Lcom/amap/api/mapcore/av;)Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->B()F

    move-result v0

    iget-object v1, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-static {v1}, Lcom/amap/api/mapcore/av;->a(Lcom/amap/api/mapcore/av;)Lcom/amap/api/mapcore/r;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/r;->o()F

    move-result v1

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-static {v0}, Lcom/amap/api/mapcore/av;->a(Lcom/amap/api/mapcore/av;)Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->O()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v2

    .line 126
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 127
    iget-object v0, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-static {v0}, Lcom/amap/api/mapcore/av;->c(Lcom/amap/api/mapcore/av;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-static {v1}, Lcom/amap/api/mapcore/av;->b(Lcom/amap/api/mapcore/av;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 128
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-static {v0}, Lcom/amap/api/mapcore/av;->c(Lcom/amap/api/mapcore/av;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-static {v1}, Lcom/amap/api/mapcore/av;->d(Lcom/amap/api/mapcore/av;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/av$1;->a:Lcom/amap/api/mapcore/av;

    invoke-static {v0}, Lcom/amap/api/mapcore/av;->a(Lcom/amap/api/mapcore/av;)Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-static {}, Lcom/amap/api/mapcore/i;->b()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->b(Lcom/amap/api/mapcore/i;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
