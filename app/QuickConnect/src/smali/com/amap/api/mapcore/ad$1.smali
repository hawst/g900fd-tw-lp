.class Lcom/amap/api/mapcore/ad$1;
.super Ljava/lang/Object;
.source "LocationView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amap/api/mapcore/ad;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ag;Lcom/amap/api/mapcore/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/ad;


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/ad;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 108
    iget-object v0, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-boolean v0, v0, Lcom/amap/api/mapcore/ad;->i:Z

    if-nez v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v6

    .line 111
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v0, v0, Lcom/amap/api/mapcore/ad;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v1, v1, Lcom/amap/api/mapcore/ad;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 113
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v0, v0, Lcom/amap/api/mapcore/ad;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v1, v1, Lcom/amap/api/mapcore/ad;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 116
    iget-object v0, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v0, v0, Lcom/amap/api/mapcore/ad;->h:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->g(Z)V

    .line 117
    iget-object v0, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v0, v0, Lcom/amap/api/mapcore/ad;->h:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->v()Landroid/location/Location;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    .line 121
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 123
    iget-object v2, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v2, v2, Lcom/amap/api/mapcore/ad;->h:Lcom/amap/api/mapcore/r;

    invoke-interface {v2, v0}, Lcom/amap/api/mapcore/r;->a(Landroid/location/Location;)V

    .line 124
    iget-object v0, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v0, v0, Lcom/amap/api/mapcore/ad;->h:Lcom/amap/api/mapcore/r;

    iget-object v2, p0, Lcom/amap/api/mapcore/ad$1;->a:Lcom/amap/api/mapcore/ad;

    iget-object v2, v2, Lcom/amap/api/mapcore/ad;->h:Lcom/amap/api/mapcore/r;

    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->B()F

    move-result v2

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;F)Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/mapcore/i;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
