.class public final Lcom/amap/api/mapcore/b/f;
.super Ljava/lang/Object;
.source "DiskLruCache.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/mapcore/b/f$b;,
        Lcom/amap/api/mapcore/b/f$a;,
        Lcom/amap/api/mapcore/b/f$c;
    }
.end annotation


# static fields
.field private static final a:Ljava/nio/charset/Charset;


# instance fields
.field private final b:Ljava/io/File;

.field private final c:Ljava/io/File;

.field private final d:Ljava/io/File;

.field private final e:I

.field private final f:J

.field private final g:I

.field private h:J

.field private i:Ljava/io/Writer;

.field private final j:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/amap/api/mapcore/b/f$b;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:J

.field private final m:Ljava/util/concurrent/ExecutorService;

.field private final n:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/amap/api/mapcore/b/f;->a:Ljava/nio/charset/Charset;

    return-void
.end method

.method private constructor <init>(Ljava/io/File;IIJ)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput-wide v4, p0, Lcom/amap/api/mapcore/b/f;->h:J

    .line 158
    new-instance v0, Ljava/util/LinkedHashMap;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {v0, v2, v1, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    .line 167
    iput-wide v4, p0, Lcom/amap/api/mapcore/b/f;->l:J

    .line 269
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/amap/api/mapcore/b/f;->m:Ljava/util/concurrent/ExecutorService;

    .line 271
    new-instance v0, Lcom/amap/api/mapcore/b/g;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/b/g;-><init>(Lcom/amap/api/mapcore/b/f;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b/f;->n:Ljava/util/concurrent/Callable;

    .line 290
    iput-object p1, p0, Lcom/amap/api/mapcore/b/f;->b:Ljava/io/File;

    .line 291
    iput p2, p0, Lcom/amap/api/mapcore/b/f;->e:I

    .line 292
    new-instance v0, Ljava/io/File;

    const-string v1, "journal"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b/f;->c:Ljava/io/File;

    .line 293
    new-instance v0, Ljava/io/File;

    const-string v1, "journal.tmp"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b/f;->d:Ljava/io/File;

    .line 294
    iput p3, p0, Lcom/amap/api/mapcore/b/f;->g:I

    .line 295
    iput-wide p4, p0, Lcom/amap/api/mapcore/b/f;->f:J

    .line 296
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b/f;I)I
    .locals 0

    .prologue
    .line 104
    iput p1, p0, Lcom/amap/api/mapcore/b/f;->k:I

    return p1
.end method

.method private declared-synchronized a(Ljava/lang/String;J)Lcom/amap/api/mapcore/b/f$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 529
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->h()V

    .line 530
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b/f;->e(Ljava/lang/String;)V

    .line 531
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/b/f$b;

    .line 532
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->e(Lcom/amap/api/mapcore/b/f$b;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v2, v2, p2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 549
    :goto_0
    monitor-exit p0

    return-object v0

    .line 536
    :cond_1
    if-nez v0, :cond_2

    .line 537
    :try_start_1
    new-instance v0, Lcom/amap/api/mapcore/b/f$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/amap/api/mapcore/b/f$b;-><init>(Lcom/amap/api/mapcore/b/f;Ljava/lang/String;Lcom/amap/api/mapcore/b/g;)V

    .line 538
    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 543
    :goto_1
    new-instance v0, Lcom/amap/api/mapcore/b/f$a;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/amap/api/mapcore/b/f$a;-><init>(Lcom/amap/api/mapcore/b/f;Lcom/amap/api/mapcore/b/f$b;Lcom/amap/api/mapcore/b/g;)V

    .line 544
    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;Lcom/amap/api/mapcore/b/f$a;)Lcom/amap/api/mapcore/b/f$a;

    .line 547
    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DIRTY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 548
    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    invoke-virtual {v1}, Ljava/io/Writer;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 529
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 539
    :cond_2
    :try_start_2
    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;)Lcom/amap/api/mapcore/b/f$a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 540
    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public static a(Ljava/io/File;IIJ)Lcom/amap/api/mapcore/b/f;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 314
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gtz v0, :cond_0

    .line 315
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxSize <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_0
    if-gtz p2, :cond_1

    .line 318
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "valueCount <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_1
    new-instance v0, Lcom/amap/api/mapcore/b/f;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/mapcore/b/f;-><init>(Ljava/io/File;IIJ)V

    .line 324
    iget-object v1, v0, Lcom/amap/api/mapcore/b/f;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 326
    :try_start_0
    invoke-direct {v0}, Lcom/amap/api/mapcore/b/f;->d()V

    .line 327
    invoke-direct {v0}, Lcom/amap/api/mapcore/b/f;->e()V

    .line 328
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/FileWriter;

    iget-object v3, v0, Lcom/amap/api/mapcore/b/f;->c:Ljava/io/File;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    const/16 v3, 0x2000

    invoke-direct {v1, v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    iput-object v1, v0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :goto_0
    return-object v0

    .line 331
    :catch_0
    move-exception v1

    .line 334
    invoke-virtual {v0}, Lcom/amap/api/mapcore/b/f;->c()V

    .line 339
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    .line 340
    new-instance v0, Lcom/amap/api/mapcore/b/f;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/mapcore/b/f;-><init>(Ljava/io/File;IIJ)V

    .line 341
    invoke-direct {v0}, Lcom/amap/api/mapcore/b/f;->f()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b/f;)Ljava/io/Writer;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x50

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 217
    :goto_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 218
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 219
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 220
    :cond_0
    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    .line 226
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 227
    if-lez v1, :cond_1

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_1

    .line 228
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 230
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 224
    :cond_2
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private declared-synchronized a(Lcom/amap/api/mapcore/b/f$a;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 578
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/amap/api/mapcore/b/f$a;->a(Lcom/amap/api/mapcore/b/f$a;)Lcom/amap/api/mapcore/b/f$b;

    move-result-object v2

    .line 579
    invoke-static {v2}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;)Lcom/amap/api/mapcore/b/f$a;

    move-result-object v1

    if-eq v1, p1, :cond_0

    .line 580
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 585
    :cond_0
    if-eqz p2, :cond_2

    :try_start_1
    invoke-static {v2}, Lcom/amap/api/mapcore/b/f$b;->d(Lcom/amap/api/mapcore/b/f$b;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v0

    .line 586
    :goto_0
    iget v3, p0, Lcom/amap/api/mapcore/b/f;->g:I

    if-ge v1, v3, :cond_2

    .line 587
    invoke-virtual {v2, v1}, Lcom/amap/api/mapcore/b/f$b;->b(I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 588
    invoke-virtual {p1}, Lcom/amap/api/mapcore/b/f$a;->b()V

    .line 589
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "edit didn\'t create file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 586
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 595
    :cond_2
    :goto_1
    iget v1, p0, Lcom/amap/api/mapcore/b/f;->g:I

    if-ge v0, v1, :cond_5

    .line 596
    invoke-virtual {v2, v0}, Lcom/amap/api/mapcore/b/f$b;->b(I)Ljava/io/File;

    move-result-object v1

    .line 597
    if-eqz p2, :cond_4

    .line 598
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 599
    invoke-virtual {v2, v0}, Lcom/amap/api/mapcore/b/f$b;->a(I)Ljava/io/File;

    move-result-object v3

    .line 600
    invoke-virtual {v1, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 601
    invoke-static {v2}, Lcom/amap/api/mapcore/b/f$b;->b(Lcom/amap/api/mapcore/b/f$b;)[J

    move-result-object v1

    aget-wide v4, v1, v0

    .line 602
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 603
    invoke-static {v2}, Lcom/amap/api/mapcore/b/f$b;->b(Lcom/amap/api/mapcore/b/f$b;)[J

    move-result-object v1

    aput-wide v6, v1, v0

    .line 604
    iget-wide v8, p0, Lcom/amap/api/mapcore/b/f;->h:J

    sub-long v4, v8, v4

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/amap/api/mapcore/b/f;->h:J

    .line 595
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 607
    :cond_4
    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->b(Ljava/io/File;)V

    goto :goto_2

    .line 611
    :cond_5
    iget v0, p0, Lcom/amap/api/mapcore/b/f;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/b/f;->k:I

    .line 612
    const/4 v0, 0x0

    invoke-static {v2, v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;Lcom/amap/api/mapcore/b/f$a;)Lcom/amap/api/mapcore/b/f$a;

    .line 613
    invoke-static {v2}, Lcom/amap/api/mapcore/b/f$b;->d(Lcom/amap/api/mapcore/b/f$b;)Z

    move-result v0

    or-int/2addr v0, p2

    if-eqz v0, :cond_9

    .line 614
    const/4 v0, 0x1

    invoke-static {v2, v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;Z)Z

    .line 615
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CLEAN "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2}, Lcom/amap/api/mapcore/b/f$b;->c(Lcom/amap/api/mapcore/b/f$b;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/amap/api/mapcore/b/f$b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 617
    if-eqz p2, :cond_6

    .line 618
    iget-wide v0, p0, Lcom/amap/api/mapcore/b/f;->l:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/amap/api/mapcore/b/f;->l:J

    invoke-static {v2, v0, v1}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;J)J

    .line 625
    :cond_6
    :goto_3
    iget-wide v0, p0, Lcom/amap/api/mapcore/b/f;->h:J

    iget-wide v2, p0, Lcom/amap/api/mapcore/b/f;->f:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_7

    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 626
    :cond_7
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->m:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->n:Ljava/util/concurrent/Callable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 628
    :cond_8
    monitor-exit p0

    return-void

    .line 621
    :cond_9
    :try_start_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-static {v2}, Lcom/amap/api/mapcore/b/f$b;->c(Lcom/amap/api/mapcore/b/f$b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REMOVE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2}, Lcom/amap/api/mapcore/b/f$b;->c(Lcom/amap/api/mapcore/b/f$b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b/f;Lcom/amap/api/mapcore/b/f$a;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lcom/amap/api/mapcore/b/f;->a(Lcom/amap/api/mapcore/b/f$a;Z)V

    return-void
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 238
    if-eqz p0, :cond_0

    .line 240
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 241
    :catch_0
    move-exception v0

    .line 242
    throw v0

    .line 243
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/io/File;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 254
    if-nez v1, :cond_0

    .line 255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not a directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 258
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 259
    invoke-static {v3}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/File;)V

    .line 261
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_2

    .line 262
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to delete file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    :cond_3
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 266
    return-void
.end method

.method private static a([Ljava/lang/Object;II)[Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II)[TT;"
        }
    .end annotation

    .prologue
    .line 172
    array-length v0, p0

    .line 174
    if-le p1, p2, :cond_0

    .line 175
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 177
    :cond_0
    if-ltz p1, :cond_1

    if-le p1, v0, :cond_2

    .line 178
    :cond_1
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 180
    :cond_2
    sub-int v1, p2, p1

    .line 181
    sub-int/2addr v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 182
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 184
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 185
    return-object v0
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->i()V

    return-void
.end method

.method private static b(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 473
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 474
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 476
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b/f;)Z
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->g()Z

    move-result v0

    return v0
.end method

.method private d()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v0, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/amap/api/mapcore/b/f;->c:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v2, 0x2000

    invoke-direct {v1, v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 349
    :try_start_0
    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 350
    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 351
    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 352
    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    .line 353
    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    .line 354
    const-string v6, "libcore.io.DiskLruCache"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/amap/api/mapcore/b/f;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/amap/api/mapcore/b/f;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 358
    :cond_0
    new-instance v3, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unexpected journal header: ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/Closeable;)V

    throw v0

    .line 365
    :cond_1
    :goto_0
    :try_start_1
    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/amap/api/mapcore/b/f;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 366
    :catch_0
    move-exception v0

    .line 371
    invoke-static {v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/Closeable;)V

    .line 373
    return-void
.end method

.method static synthetic d(Lcom/amap/api/mapcore/b/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->f()V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 376
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 377
    array-length v0, v2

    if-ge v0, v4, :cond_0

    .line 378
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected journal line: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_0
    aget-object v1, v2, v7

    .line 382
    aget-object v0, v2, v5

    const-string v3, "REMOVE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    array-length v0, v2

    if-ne v0, v4, :cond_2

    .line 383
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    :cond_1
    :goto_0
    return-void

    .line 387
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/b/f$b;

    .line 388
    if-nez v0, :cond_6

    .line 389
    new-instance v0, Lcom/amap/api/mapcore/b/f$b;

    invoke-direct {v0, p0, v1, v6}, Lcom/amap/api/mapcore/b/f$b;-><init>(Lcom/amap/api/mapcore/b/f;Ljava/lang/String;Lcom/amap/api/mapcore/b/g;)V

    .line 390
    iget-object v3, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 393
    :goto_1
    aget-object v0, v2, v5

    const-string v3, "CLEAN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    array-length v0, v2

    iget v3, p0, Lcom/amap/api/mapcore/b/f;->g:I

    add-int/lit8 v3, v3, 0x2

    if-ne v0, v3, :cond_3

    .line 394
    invoke-static {v1, v7}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;Z)Z

    .line 395
    invoke-static {v1, v6}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;Lcom/amap/api/mapcore/b/f$a;)Lcom/amap/api/mapcore/b/f$a;

    .line 396
    array-length v0, v2

    invoke-static {v2, v4, v0}, Lcom/amap/api/mapcore/b/f;->a([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;[Ljava/lang/String;)V

    goto :goto_0

    .line 397
    :cond_3
    aget-object v0, v2, v5

    const-string v3, "DIRTY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    array-length v0, v2

    if-ne v0, v4, :cond_4

    .line 398
    new-instance v0, Lcom/amap/api/mapcore/b/f$a;

    invoke-direct {v0, p0, v1, v6}, Lcom/amap/api/mapcore/b/f$a;-><init>(Lcom/amap/api/mapcore/b/f;Lcom/amap/api/mapcore/b/f$b;Lcom/amap/api/mapcore/b/g;)V

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;Lcom/amap/api/mapcore/b/f$a;)Lcom/amap/api/mapcore/b/f$a;

    goto :goto_0

    .line 399
    :cond_4
    aget-object v0, v2, v5

    const-string v1, "READ"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    array-length v0, v2

    if-eq v0, v4, :cond_1

    .line 402
    :cond_5
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected journal line: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move-object v1, v0

    goto :goto_1
.end method

.method static synthetic e(Lcom/amap/api/mapcore/b/f;)I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/amap/api/mapcore/b/f;->g:I

    return v0
.end method

.method private e()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 411
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->d:Ljava/io/File;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f;->b(Ljava/io/File;)V

    .line 412
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 413
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/b/f$b;

    .line 414
    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;)Lcom/amap/api/mapcore/b/f$a;

    move-result-object v1

    if-nez v1, :cond_1

    move v1, v2

    .line 415
    :goto_1
    iget v4, p0, Lcom/amap/api/mapcore/b/f;->g:I

    if-ge v1, v4, :cond_0

    .line 416
    iget-wide v4, p0, Lcom/amap/api/mapcore/b/f;->h:J

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->b(Lcom/amap/api/mapcore/b/f$b;)[J

    move-result-object v6

    aget-wide v6, v6, v1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/amap/api/mapcore/b/f;->h:J

    .line 415
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 419
    :cond_1
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;Lcom/amap/api/mapcore/b/f$a;)Lcom/amap/api/mapcore/b/f$a;

    move v1, v2

    .line 420
    :goto_2
    iget v4, p0, Lcom/amap/api/mapcore/b/f;->g:I

    if-ge v1, v4, :cond_2

    .line 421
    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/b/f$b;->a(I)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/mapcore/b/f;->b(Ljava/io/File;)V

    .line 422
    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/b/f$b;->b(I)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/mapcore/b/f;->b(Ljava/io/File;)V

    .line 420
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 424
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 427
    :cond_3
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 733
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\r"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 734
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "keys must not contain spaces or newlines: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 737
    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/amap/api/mapcore/b/f;)Ljava/io/File;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->b:Ljava/io/File;

    return-object v0
.end method

.method private declared-synchronized f()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 438
    :cond_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/FileWriter;

    iget-object v2, p0, Lcom/amap/api/mapcore/b/f;->d:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    const/16 v2, 0x2000

    invoke-direct {v1, v0, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 440
    const-string v0, "libcore.io.DiskLruCache"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 441
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 442
    const-string v0, "1"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 443
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 444
    iget v0, p0, Lcom/amap/api/mapcore/b/f;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 445
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 446
    iget v0, p0, Lcom/amap/api/mapcore/b/f;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 447
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 448
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/b/f$b;

    .line 451
    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;)Lcom/amap/api/mapcore/b/f$a;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 452
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DIRTY "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->c(Lcom/amap/api/mapcore/b/f$b;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 434
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 454
    :cond_1
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CLEAN "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->c(Lcom/amap/api/mapcore/b/f$b;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b/f$b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_2
    invoke-virtual {v1}, Ljava/io/Writer;->close()V

    .line 460
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->d:Ljava/io/File;

    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->c:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 461
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/FileWriter;

    iget-object v2, p0, Lcom/amap/api/mapcore/b/f;->c:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    const/16 v2, 0x2000

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463
    monitor-exit p0

    return-void
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 635
    .line 636
    iget v0, p0, Lcom/amap/api/mapcore/b/f;->k:I

    const/16 v1, 0x7d0

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/b/f;->k:I

    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    if-nez v0, :cond_0

    .line 683
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cache is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 685
    :cond_0
    return-void
.end method

.method private i()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 714
    :goto_0
    iget-wide v0, p0, Lcom/amap/api/mapcore/b/f;->h:J

    iget-wide v2, p0, Lcom/amap/api/mapcore/b/f;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 718
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b/f;->c(Ljava/lang/String;)Z

    goto :goto_0

    .line 720
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lcom/amap/api/mapcore/b/f$c;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 484
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->h()V

    .line 485
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b/f;->e(Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/b/f$b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    if-nez v0, :cond_1

    .line 516
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 491
    :cond_1
    :try_start_1
    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->d(Lcom/amap/api/mapcore/b/f$b;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 500
    iget v2, p0, Lcom/amap/api/mapcore/b/f;->g:I

    new-array v6, v2, [Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 502
    const/4 v2, 0x0

    :goto_1
    :try_start_2
    iget v3, p0, Lcom/amap/api/mapcore/b/f;->g:I

    if-ge v2, v3, :cond_2

    .line 503
    new-instance v3, Ljava/io/FileInputStream;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/b/f$b;->a(I)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    aput-object v3, v6, v2
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 502
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 510
    :cond_2
    :try_start_3
    iget v1, p0, Lcom/amap/api/mapcore/b/f;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/amap/api/mapcore/b/f;->k:I

    .line 511
    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "READ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 512
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 513
    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->m:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lcom/amap/api/mapcore/b/f;->n:Ljava/util/concurrent/Callable;

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 516
    :cond_3
    new-instance v1, Lcom/amap/api/mapcore/b/f$c;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->e(Lcom/amap/api/mapcore/b/f$b;)J

    move-result-wide v4

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/amap/api/mapcore/b/f$c;-><init>(Lcom/amap/api/mapcore/b/f;Ljava/lang/String;J[Ljava/io/InputStream;Lcom/amap/api/mapcore/b/g;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 505
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/amap/api/mapcore/b/f$a;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 524
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/amap/api/mapcore/b/f;->a(Ljava/lang/String;J)Lcom/amap/api/mapcore/b/f$a;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 691
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->h()V

    .line 692
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->i()V

    .line 693
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 694
    monitor-exit p0

    return-void

    .line 691
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 728
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b/f;->close()V

    .line 729
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->b:Ljava/io/File;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f;->a(Ljava/io/File;)V

    .line 730
    return-void
.end method

.method public declared-synchronized c(Ljava/lang/String;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 647
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->h()V

    .line 648
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b/f;->e(Ljava/lang/String;)V

    .line 649
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/b/f$b;

    .line 650
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;)Lcom/amap/api/mapcore/b/f$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 671
    :goto_0
    monitor-exit p0

    return v0

    .line 659
    :cond_1
    :try_start_1
    iget-wide v2, p0, Lcom/amap/api/mapcore/b/f;->h:J

    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->b(Lcom/amap/api/mapcore/b/f$b;)[J

    move-result-object v4

    aget-wide v4, v4, v1

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/amap/api/mapcore/b/f;->h:J

    .line 660
    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->b(Lcom/amap/api/mapcore/b/f$b;)[J

    move-result-object v2

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    .line 654
    add-int/lit8 v1, v1, 0x1

    :cond_2
    iget v2, p0, Lcom/amap/api/mapcore/b/f;->g:I

    if-ge v1, v2, :cond_3

    .line 655
    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/b/f$b;->a(I)Ljava/io/File;

    move-result-object v2

    .line 656
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 657
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to delete "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 647
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 663
    :cond_3
    :try_start_2
    iget v0, p0, Lcom/amap/api/mapcore/b/f;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/b/f;->k:I

    .line 664
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "REMOVE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 665
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 667
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 668
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->m:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->n:Ljava/util/concurrent/Callable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 671
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 700
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 711
    :goto_0
    monitor-exit p0

    return-void

    .line 703
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/amap/api/mapcore/b/f;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/b/f$b;

    .line 704
    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;)Lcom/amap/api/mapcore/b/f$a;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 705
    invoke-static {v0}, Lcom/amap/api/mapcore/b/f$b;->a(Lcom/amap/api/mapcore/b/f$b;)Lcom/amap/api/mapcore/b/f$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b/f$a;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 700
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 708
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/f;->i()V

    .line 709
    iget-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 710
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b/f;->i:Ljava/io/Writer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
