.class public Lcom/amap/api/mapcore/q;
.super Ljava/lang/Object;
.source "GroundOverlayDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/t;


# instance fields
.field final a:D

.field final b:D

.field private c:Lcom/amap/api/mapcore/r;

.field private d:Lcom/amap/api/maps/model/BitmapDescriptor;

.field private e:Lcom/amap/api/maps/model/LatLng;

.field private f:F

.field private g:F

.field private h:Lcom/amap/api/maps/model/LatLngBounds;

.field private i:F

.field private j:F

.field private k:Z

.field private l:F

.field private m:F

.field private n:F

.field private o:Ljava/lang/String;

.field private p:Ljava/nio/FloatBuffer;

.field private q:Ljava/nio/FloatBuffer;

.field private r:I

.field private s:Z

.field private t:Z


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-wide v0, 0x3f91df46a2529d37L    # 0.01745329251994329

    iput-wide v0, p0, Lcom/amap/api/mapcore/q;->a:D

    .line 25
    const-wide v0, 0x41584dae328f5c29L    # 6371000.79

    iput-wide v0, p0, Lcom/amap/api/mapcore/q;->b:D

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/q;->k:Z

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/q;->l:F

    .line 36
    iput v2, p0, Lcom/amap/api/mapcore/q;->m:F

    .line 37
    iput v2, p0, Lcom/amap/api/mapcore/q;->n:F

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    .line 42
    iput-boolean v3, p0, Lcom/amap/api/mapcore/q;->s:Z

    .line 43
    iput-boolean v3, p0, Lcom/amap/api/mapcore/q;->t:Z

    .line 46
    iput-object p1, p0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    .line 48
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/q;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->o:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Lcom/autonavi/amap/mapcore/DPoint;DDDDLcom/autonavi/amap/mapcore/FPoint;)V
    .locals 14

    .prologue
    .line 209
    iget v2, p0, Lcom/amap/api/mapcore/q;->m:F

    float-to-double v2, v2

    mul-double v2, v2, p6

    sub-double v2, p2, v2

    .line 210
    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, p0, Lcom/amap/api/mapcore/q;->n:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double v4, v4, p8

    sub-double v4, v4, p4

    .line 211
    iget v6, p0, Lcom/amap/api/mapcore/q;->i:F

    neg-float v6, v6

    float-to-double v6, v6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide v8, 0x3f91df46a2529d37L    # 0.01745329251994329

    mul-double/2addr v6, v8

    .line 213
    iget-wide v8, p1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v10, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v12, v4

    add-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-float v8, v8

    move-object/from16 v0, p10

    iput v8, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    .line 215
    iget-wide v8, p1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v4, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v2, v6

    sub-double v2, v4, v2

    add-double/2addr v2, v8

    double-to-float v2, v2

    move-object/from16 v0, p10

    iput v2, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    .line 217
    return-void
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;ILjava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V
    .locals 8

    .prologue
    const/16 v7, 0x1406

    const/16 v6, 0xbe2

    const/16 v5, 0xde1

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 278
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    invoke-interface {p1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 282
    const/16 v0, 0x2300

    const/16 v1, 0x2200

    const/high16 v2, 0x46040000    # 8448.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    .line 284
    const/16 v0, 0x302

    const/16 v1, 0x303

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 285
    iget v0, p0, Lcom/amap/api/mapcore/q;->l:F

    sub-float v0, v3, v0

    invoke-interface {p1, v3, v3, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 288
    invoke-interface {p1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 289
    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 290
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 291
    invoke-interface {p1, v5, p2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 293
    const/4 v0, 0x3

    invoke-interface {p1, v0, v7, v4, p3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 294
    const/4 v0, 0x2

    invoke-interface {p1, v0, v7, v4, p4}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 295
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-interface {p1, v0, v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 297
    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 298
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 299
    invoke-interface {p1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 300
    invoke-interface {p1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_0
.end method

.method private p()V
    .locals 13

    .prologue
    const/high16 v12, 0x3f800000    # 1.0f

    const-wide v6, 0x3f91df46a2529d37L    # 0.01745329251994329

    .line 119
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    if-nez v0, :cond_0

    .line 131
    :goto_0
    return-void

    .line 121
    :cond_0
    iget v0, p0, Lcom/amap/api/mapcore/q;->f:F

    float-to-double v0, v0

    const-wide v2, 0x41584dae328f5c29L    # 6371000.79

    iget-object v4, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    mul-double/2addr v2, v6

    div-double/2addr v0, v2

    .line 123
    iget v2, p0, Lcom/amap/api/mapcore/q;->g:F

    float-to-double v2, v2

    const-wide v4, 0x40fb25af0c031ddeL    # 111194.94043265979

    div-double/2addr v2, v4

    .line 125
    new-instance v4, Lcom/amap/api/maps/model/LatLngBounds;

    new-instance v5, Lcom/amap/api/maps/model/LatLng;

    iget-object v6, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget v8, p0, Lcom/amap/api/mapcore/q;->n:F

    sub-float v8, v12, v8

    float-to-double v8, v8

    mul-double/2addr v8, v2

    sub-double/2addr v6, v8

    iget-object v8, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v8, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget v10, p0, Lcom/amap/api/mapcore/q;->m:F

    float-to-double v10, v10

    mul-double/2addr v10, v0

    sub-double/2addr v8, v10

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    new-instance v6, Lcom/amap/api/maps/model/LatLng;

    iget-object v7, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v7, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget v7, p0, Lcom/amap/api/mapcore/q;->n:F

    float-to-double v10, v7

    mul-double/2addr v2, v10

    add-double/2addr v2, v8

    iget-object v7, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v7, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget v7, p0, Lcom/amap/api/mapcore/q;->m:F

    sub-float v7, v12, v7

    float-to-double v10, v7

    mul-double/2addr v0, v10

    add-double/2addr v0, v8

    invoke-direct {v6, v2, v3, v0, v1}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v4, v5, v6}, Lcom/amap/api/maps/model/LatLngBounds;-><init>(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)V

    iput-object v4, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    .line 130
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->r()V

    goto :goto_0
.end method

.method private q()V
    .locals 14

    .prologue
    .line 134
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v0, v0, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    .line 137
    iget-object v1, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v1, v1, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    .line 139
    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const/high16 v3, 0x3f800000    # 1.0f

    iget v6, p0, Lcom/amap/api/mapcore/q;->n:F

    sub-float/2addr v3, v6

    float-to-double v6, v3

    iget-wide v8, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v10, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    sub-double/2addr v8, v10

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget v3, p0, Lcom/amap/api/mapcore/q;->m:F

    float-to-double v8, v3

    iget-wide v10, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-wide v12, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    iput-object v2, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    .line 144
    const-wide v2, 0x41584dae328f5c29L    # 6371000.79

    iget-object v4, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide v6, 0x3f91df46a2529d37L    # 0.01745329251994329

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-wide v6, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x3f91df46a2529d37L    # 0.01745329251994329

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Lcom/amap/api/mapcore/q;->f:F

    .line 146
    const-wide v2, 0x41584dae328f5c29L    # 6371000.79

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v0, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    sub-double v0, v4, v0

    mul-double/2addr v0, v2

    const-wide v2, 0x3f91df46a2529d37L    # 0.01745329251994329

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/amap/api/mapcore/q;->g:F

    .line 148
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->r()V

    goto :goto_0
.end method

.method private r()V
    .locals 39

    .prologue
    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    if-nez v2, :cond_0

    .line 205
    :goto_0
    return-void

    .line 154
    :cond_0
    const/16 v2, 0xc

    new-array v2, v2, [F

    .line 156
    new-instance v8, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 158
    new-instance v14, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v14}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 160
    new-instance v20, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct/range {v20 .. v20}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 162
    new-instance v26, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct/range {v26 .. v26}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 164
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v4, v4, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v6, v6, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v3 .. v8}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/FPoint;)V

    .line 166
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v3, v3, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v10, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v3, v3, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v12, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v9 .. v14}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/FPoint;)V

    .line 168
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v3, v3, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v0, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v3, v3, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v0, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-wide/from16 v18, v0

    invoke-interface/range {v15 .. v20}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/FPoint;)V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v3, v3, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v0, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    iget-object v3, v3, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v0, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-wide/from16 v24, v0

    invoke-interface/range {v21 .. v26}, Lcom/amap/api/mapcore/r;->a(DDLcom/autonavi/amap/mapcore/FPoint;)V

    .line 172
    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/q;->i:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_1

    .line 173
    iget v3, v14, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v4, v8, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    sub-float/2addr v3, v4

    float-to-double v0, v3

    move-wide/from16 v34, v0

    .line 174
    iget v3, v14, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    move-object/from16 v0, v20

    iget v4, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    sub-float/2addr v3, v4

    float-to-double v0, v3

    move-wide/from16 v36, v0

    .line 175
    new-instance v29, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct/range {v29 .. v29}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 176
    iget v3, v8, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    float-to-double v4, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/amap/api/mapcore/q;->m:F

    float-to-double v6, v3

    mul-double v6, v6, v34

    add-double/2addr v4, v6

    move-object/from16 v0, v29

    iput-wide v4, v0, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    .line 177
    iget v3, v8, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    float-to-double v4, v3

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/amap/api/mapcore/q;->n:F

    sub-float/2addr v3, v6

    float-to-double v6, v3

    mul-double v6, v6, v36

    sub-double/2addr v4, v6

    move-object/from16 v0, v29

    iput-wide v4, v0, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    .line 178
    const-wide/16 v30, 0x0

    const-wide/16 v32, 0x0

    move-object/from16 v28, p0

    move-object/from16 v38, v8

    invoke-direct/range {v28 .. v38}, Lcom/amap/api/mapcore/q;->a(Lcom/autonavi/amap/mapcore/DPoint;DDDDLcom/autonavi/amap/mapcore/FPoint;)V

    .line 179
    const-wide/16 v32, 0x0

    move-object/from16 v28, p0

    move-wide/from16 v30, v34

    move-object/from16 v38, v14

    invoke-direct/range {v28 .. v38}, Lcom/amap/api/mapcore/q;->a(Lcom/autonavi/amap/mapcore/DPoint;DDDDLcom/autonavi/amap/mapcore/FPoint;)V

    move-object/from16 v28, p0

    move-wide/from16 v30, v34

    move-wide/from16 v32, v36

    move-object/from16 v38, v20

    .line 180
    invoke-direct/range {v28 .. v38}, Lcom/amap/api/mapcore/q;->a(Lcom/autonavi/amap/mapcore/DPoint;DDDDLcom/autonavi/amap/mapcore/FPoint;)V

    .line 181
    const-wide/16 v30, 0x0

    move-object/from16 v28, p0

    move-wide/from16 v32, v36

    move-object/from16 v38, v26

    invoke-direct/range {v28 .. v38}, Lcom/amap/api/mapcore/q;->a(Lcom/autonavi/amap/mapcore/DPoint;DDDDLcom/autonavi/amap/mapcore/FPoint;)V

    .line 184
    :cond_1
    const/4 v3, 0x0

    iget v4, v8, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v4, v2, v3

    .line 185
    const/4 v3, 0x1

    iget v4, v8, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v4, v2, v3

    .line 186
    const/4 v3, 0x2

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 188
    const/4 v3, 0x3

    iget v4, v14, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v4, v2, v3

    .line 189
    const/4 v3, 0x4

    iget v4, v14, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v4, v2, v3

    .line 190
    const/4 v3, 0x5

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 192
    const/4 v3, 0x6

    move-object/from16 v0, v20

    iget v4, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v4, v2, v3

    .line 193
    const/4 v3, 0x7

    move-object/from16 v0, v20

    iget v4, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v4, v2, v3

    .line 194
    const/16 v3, 0x8

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 196
    const/16 v3, 0x9

    move-object/from16 v0, v26

    iget v4, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v4, v2, v3

    .line 197
    const/16 v3, 0xa

    move-object/from16 v0, v26

    iget v4, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v4, v2, v3

    .line 198
    const/16 v3, 0xb

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 199
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    if-nez v3, :cond_2

    .line 200
    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    goto/16 :goto_0

    .line 202
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    invoke-static {v2, v3}, Lcom/amap/api/mapcore/b/r;->a([FLjava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    goto/16 :goto_0
.end method

.method private s()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 221
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getWidth()I

    move-result v0

    .line 224
    iget-object v1, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    invoke-virtual {v1}, Lcom/amap/api/maps/model/BitmapDescriptor;->getHeight()I

    move-result v1

    .line 225
    iget-object v2, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 226
    iget-object v3, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    invoke-virtual {v3}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 227
    int-to-float v0, v0

    int-to-float v3, v3

    div-float/2addr v0, v3

    .line 228
    int-to-float v1, v1

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 230
    const/16 v2, 0x8

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v4, v2, v3

    const/4 v3, 0x1

    aput v1, v2, v3

    const/4 v3, 0x2

    aput v0, v2, v3

    const/4 v3, 0x3

    aput v1, v2, v3

    const/4 v1, 0x4

    aput v0, v2, v1

    const/4 v0, 0x5

    aput v4, v2, v0

    const/4 v0, 0x6

    aput v4, v2, v0

    const/4 v0, 0x7

    aput v4, v2, v0

    invoke-static {v2}, Lcom/amap/api/mapcore/b/r;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->q:Ljava/nio/FloatBuffer;

    goto :goto_0
.end method


# virtual methods
.method public a(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 71
    iput p1, p0, Lcom/amap/api/mapcore/q;->j:F

    .line 72
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->J()V

    .line 73
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 74
    return-void
.end method

.method public a(FF)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 377
    cmpl-float v0, p1, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Width must be non-negative"

    invoke-static {v0, v3}, Lcom/amap/api/mapcore/b/a;->b(ZLjava/lang/Object;)V

    .line 379
    cmpl-float v0, p2, v4

    if-ltz v0, :cond_1

    :goto_1
    const-string v0, "Height must be non-negative"

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b/a;->b(ZLjava/lang/Object;)V

    .line 381
    iget-boolean v0, p0, Lcom/amap/api/mapcore/q;->s:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/q;->f:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/q;->g:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_2

    .line 382
    iput p1, p0, Lcom/amap/api/mapcore/q;->f:F

    .line 383
    iput p2, p0, Lcom/amap/api/mapcore/q;->g:F

    .line 384
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->p()V

    .line 389
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 377
    goto :goto_0

    :cond_1
    move v1, v2

    .line 379
    goto :goto_1

    .line 386
    :cond_2
    iput p1, p0, Lcom/amap/api/mapcore/q;->f:F

    .line 387
    iput p2, p0, Lcom/amap/api/mapcore/q;->g:F

    goto :goto_2
.end method

.method public a(Lcom/amap/api/maps/model/BitmapDescriptor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 449
    iput-object p1, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 450
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->s()V

    .line 451
    iget-boolean v0, p0, Lcom/amap/api/mapcore/q;->s:Z

    if-eqz v0, :cond_0

    .line 452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/q;->s:Z

    .line 454
    :cond_0
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/LatLng;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 348
    iput-object p1, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    .line 349
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->p()V

    .line 354
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/LatLngBounds;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 405
    iput-object p1, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    .line 406
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->q()V

    .line 410
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 238
    iget-boolean v0, p0, Lcom/amap/api/mapcore/q;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    if-nez v0, :cond_2

    .line 267
    :cond_1
    :goto_0
    return-void

    .line 244
    :cond_2
    iget-boolean v0, p0, Lcom/amap/api/mapcore/q;->s:Z

    if-nez v0, :cond_5

    .line 245
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 246
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 247
    iget v1, p0, Lcom/amap/api/mapcore/q;->r:I

    if-nez v1, :cond_7

    .line 248
    iget-object v1, p0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v1}, Lcom/amap/api/mapcore/r;->H()I

    move-result v1

    iput v1, p0, Lcom/amap/api/mapcore/q;->r:I

    .line 249
    iget v1, p0, Lcom/amap/api/mapcore/q;->r:I

    if-nez v1, :cond_3

    .line 250
    new-array v1, v4, [I

    aput v3, v1, v3

    .line 251
    invoke-interface {p1, v4, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 252
    aget v1, v1, v3

    iput v1, p0, Lcom/amap/api/mapcore/q;->r:I

    .line 257
    :cond_3
    :goto_1
    iget v1, p0, Lcom/amap/api/mapcore/q;->r:I

    invoke-static {p1, v1, v0}, Lcom/amap/api/mapcore/b/r;->a(Ljavax/microedition/khronos/opengles/GL10;ILandroid/graphics/Bitmap;)I

    .line 260
    :cond_4
    iput-boolean v4, p0, Lcom/amap/api/mapcore/q;->s:Z

    .line 262
    :cond_5
    iget v0, p0, Lcom/amap/api/mapcore/q;->f:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_6

    iget v0, p0, Lcom/amap/api/mapcore/q;->g:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_1

    .line 264
    :cond_6
    iget v0, p0, Lcom/amap/api/mapcore/q;->r:I

    iget-object v1, p0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/amap/api/mapcore/q;->q:Ljava/nio/FloatBuffer;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/amap/api/mapcore/q;->a(Ljavax/microedition/khronos/opengles/GL10;ILjava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V

    .line 266
    iput-boolean v4, p0, Lcom/amap/api/mapcore/q;->t:Z

    goto :goto_0

    .line 255
    :cond_7
    new-array v1, v4, [I

    iget v2, p0, Lcom/amap/api/mapcore/q;->r:I

    aput v2, v1, v3

    invoke-interface {p1, v4, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/amap/api/mapcore/q;->k:Z

    .line 84
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 334
    iget-object v2, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    if-nez v2, :cond_1

    .line 341
    :cond_0
    :goto_0
    return v0

    .line 337
    :cond_1
    iget-object v2, p0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->D()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v2

    .line 338
    if-nez v2, :cond_2

    move v0, v1

    .line 339
    goto :goto_0

    .line 341
    :cond_2
    iget-object v3, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps/model/LatLngBounds;->contains(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps/model/LatLngBounds;->intersects(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/w;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/w;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/q;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    :cond_0
    const/4 v0, 0x1

    .line 98
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    iget v1, p0, Lcom/amap/api/mapcore/q;->r:I

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->d(I)V

    .line 57
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/q;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Ljava/lang/String;)Z

    .line 58
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->c:Lcom/amap/api/mapcore/r;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V

    .line 59
    return-void
.end method

.method public b(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 363
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Width must be non-negative"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/a;->b(ZLjava/lang/Object;)V

    .line 365
    iget-boolean v0, p0, Lcom/amap/api/mapcore/q;->s:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/amap/api/mapcore/q;->f:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_1

    .line 366
    iput p1, p0, Lcom/amap/api/mapcore/q;->f:F

    .line 367
    iput p1, p0, Lcom/amap/api/mapcore/q;->g:F

    .line 368
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->p()V

    .line 373
    :goto_1
    return-void

    .line 363
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 370
    :cond_1
    iput p1, p0, Lcom/amap/api/mapcore/q;->f:F

    .line 371
    iput p1, p0, Lcom/amap/api/mapcore/q;->g:F

    goto :goto_1
.end method

.method public b(FF)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 458
    iput p1, p0, Lcom/amap/api/mapcore/q;->m:F

    .line 459
    iput p2, p0, Lcom/amap/api/mapcore/q;->n:F

    .line 460
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 64
    const-string v0, "GroundOverlay"

    invoke-static {v0}, Lcom/amap/api/mapcore/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->o:Ljava/lang/String;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->o:Ljava/lang/String;

    return-object v0
.end method

.method public c(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/high16 v1, 0x43b40000    # 360.0f

    .line 419
    rem-float v0, p1, v1

    add-float/2addr v0, v1

    rem-float/2addr v0, v1

    .line 420
    iget-boolean v1, p0, Lcom/amap/api/mapcore/q;->s:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/amap/api/mapcore/q;->i:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 421
    iput v0, p0, Lcom/amap/api/mapcore/q;->i:F

    .line 422
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->r()V

    .line 426
    :goto_0
    return-void

    .line 424
    :cond_0
    iput v0, p0, Lcom/amap/api/mapcore/q;->i:F

    goto :goto_0
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 78
    iget v0, p0, Lcom/amap/api/mapcore/q;->j:F

    return v0
.end method

.method public d(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 435
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Transparency must be in the range [0..1]"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/a;->b(ZLjava/lang/Object;)V

    .line 438
    iput p1, p0, Lcom/amap/api/mapcore/q;->l:F

    .line 439
    return-void

    .line 435
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/q;->k:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/q;->t:Z

    .line 109
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    if-nez v0, :cond_0

    .line 110
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->q()V

    .line 116
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    if-nez v0, :cond_1

    .line 112
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->p()V

    goto :goto_0

    .line 114
    :cond_1
    invoke-direct {p0}, Lcom/amap/api/mapcore/q;->r()V

    goto :goto_0
.end method

.method public h()Lcom/amap/api/maps/model/LatLng;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 358
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    return-object v0
.end method

.method public i()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 393
    iget v0, p0, Lcom/amap/api/mapcore/q;->f:F

    return v0
.end method

.method public j()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 398
    iget v0, p0, Lcom/amap/api/mapcore/q;->g:F

    return v0
.end method

.method public k()Lcom/amap/api/maps/model/LatLngBounds;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 414
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;

    return-object v0
.end method

.method public l()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 430
    iget v0, p0, Lcom/amap/api/mapcore/q;->i:F

    return v0
.end method

.method public m()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 443
    iget v0, p0, Lcom/amap/api/mapcore/q;->l:F

    return v0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 307
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/q;->b()V

    .line 308
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_0

    .line 311
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 313
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->d:Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->q:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->q:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 318
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->q:Ljava/nio/FloatBuffer;

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 322
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->p:Ljava/nio/FloatBuffer;

    .line 324
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->e:Lcom/amap/api/maps/model/LatLng;

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/q;->h:Lcom/amap/api/maps/model/LatLngBounds;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 328
    const-string v0, "destroy erro"

    const-string v1, "GroundOverlayDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 464
    iget-boolean v0, p0, Lcom/amap/api/mapcore/q;->t:Z

    return v0
.end method
