.class Lcom/amap/api/mapcore/aq$b;
.super Lcom/amap/api/mapcore/b/b;
.source "TileOverlayDelegateImp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/aq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/amap/api/mapcore/b/b",
        "<",
        "Lcom/amap/api/mapcore/r;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/amap/api/mapcore/aq$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/aq;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/aq;Z)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/amap/api/mapcore/aq$b;->a:Lcom/amap/api/mapcore/aq;

    .line 518
    invoke-direct {p0}, Lcom/amap/api/mapcore/b/b;-><init>()V

    .line 519
    iput-boolean p2, p0, Lcom/amap/api/mapcore/aq$b;->f:Z

    .line 520
    return-void
.end method


# virtual methods
.method protected bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 512
    check-cast p1, [Lcom/amap/api/mapcore/r;

    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/aq$b;->a([Lcom/amap/api/mapcore/r;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs a([Lcom/amap/api/mapcore/r;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/amap/api/mapcore/r;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/mapcore/aq$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 526
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->j()I

    move-result v2

    .line 527
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->k()I

    move-result v0

    .line 528
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-interface {v3}, Lcom/amap/api/mapcore/r;->B()F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/amap/api/mapcore/aq$b;->e:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 533
    :goto_0
    if-lez v1, :cond_0

    if-gtz v0, :cond_1

    .line 534
    :cond_0
    const/4 v0, 0x0

    .line 535
    :goto_1
    return-object v0

    .line 529
    :catch_0
    move-exception v0

    move v0, v1

    .line 531
    goto :goto_0

    .line 535
    :cond_1
    iget-object v2, p0, Lcom/amap/api/mapcore/aq$b;->a:Lcom/amap/api/mapcore/aq;

    iget v3, p0, Lcom/amap/api/mapcore/aq$b;->e:I

    invoke-static {v2, v3, v1, v0}, Lcom/amap/api/mapcore/aq;->a(Lcom/amap/api/mapcore/aq;III)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 512
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/aq$b;->a(Ljava/util/List;)V

    return-void
.end method

.method protected a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/mapcore/aq$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 540
    if-nez p1, :cond_1

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 544
    if-lez v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$b;->a:Lcom/amap/api/mapcore/aq;

    iget v1, p0, Lcom/amap/api/mapcore/aq$b;->e:I

    iget-boolean v2, p0, Lcom/amap/api/mapcore/aq$b;->f:Z

    invoke-static {v0, p1, v1, v2}, Lcom/amap/api/mapcore/aq;->a(Lcom/amap/api/mapcore/aq;Ljava/util/List;IZ)Z

    .line 548
    invoke-interface {p1}, Ljava/util/List;->clear()V

    goto :goto_0
.end method
