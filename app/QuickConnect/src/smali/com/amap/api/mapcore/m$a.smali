.class Lcom/amap/api/mapcore/m$a;
.super Ljava/lang/Object;
.source "CustomGLOverlayLayer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/m;


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/m;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/amap/api/mapcore/m$a;->a:Lcom/amap/api/mapcore/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 35
    check-cast p1, Lcom/amap/api/mapcore/o;

    .line 36
    check-cast p2, Lcom/amap/api/mapcore/o;

    .line 38
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/amap/api/mapcore/o;->getZIndex()I

    move-result v0

    invoke-virtual {p2}, Lcom/amap/api/mapcore/o;->getZIndex()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 40
    const/4 v0, 0x1

    .line 48
    :goto_0
    return v0

    .line 41
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/mapcore/o;->getZIndex()I

    move-result v0

    invoke-virtual {p2}, Lcom/amap/api/mapcore/o;->getZIndex()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ge v0, v1, :cond_1

    .line 42
    const/4 v0, -0x1

    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
