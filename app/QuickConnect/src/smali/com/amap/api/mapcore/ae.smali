.class public Lcom/amap/api/mapcore/ae;
.super Ljava/lang/Object;
.source "MapFragmentDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/u;


# static fields
.field public static a:Landroid/content/Context;


# instance fields
.field public b:I

.field private c:Lcom/amap/api/mapcore/r;

.field private d:Lcom/amap/api/maps/AMapOptions;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/ae;->b:I

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    if-nez v0, :cond_2

    .line 97
    sget-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 98
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    .line 101
    :cond_0
    sget-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 102
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context \u4e3a null \u8bf7\u5728\u5730\u56fe\u8c03\u7528\u4e4b\u524d \u4f7f\u7528 MapsInitializer.initialize(Context paramContext) \u6765\u8bbe\u7f6eContext"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_1
    sget-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 107
    const/16 v1, 0x78

    if-gt v0, v1, :cond_4

    .line 108
    const/high16 v0, 0x3f000000    # 0.5f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    .line 123
    :goto_0
    new-instance v0, Lcom/amap/api/mapcore/b;

    sget-object v1, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/amap/api/mapcore/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    .line 125
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    iget v1, p0, Lcom/amap/api/mapcore/ae;->b:I

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->f(I)V

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    if-nez v0, :cond_3

    if-eqz p3, :cond_3

    .line 130
    const-string v0, "MapOptions"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/AMapOptions;

    iput-object v0, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    .line 134
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/ae;->b(Lcom/amap/api/maps/AMapOptions;)V

    .line 135
    const-string v0, "MapFragmentDelegateImp"

    const-string v1, "onCreateView"

    const/16 v2, 0x71

    invoke-static {v0, v1, v2}, Lcom/amap/api/mapcore/b/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 137
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->z()Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 109
    :cond_4
    const/16 v1, 0xa0

    if-gt v0, v1, :cond_5

    .line 110
    const v0, 0x3f19999a    # 0.6f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 111
    :cond_5
    const/16 v1, 0xf0

    if-gt v0, v1, :cond_6

    .line 112
    const v0, 0x3f5eb852    # 0.87f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 113
    :cond_6
    const/16 v1, 0x140

    if-gt v0, v1, :cond_7

    .line 114
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 115
    :cond_7
    const/16 v1, 0x1e0

    if-gt v0, v1, :cond_8

    .line 116
    const/high16 v0, 0x3fc00000    # 1.5f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 117
    :cond_8
    const/16 v1, 0x280

    if-gt v0, v1, :cond_9

    .line 118
    const v0, 0x3fe66666    # 1.8f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 120
    :cond_9
    const v0, 0x3f666666    # 0.9f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0
.end method

.method public a()Lcom/amap/api/mapcore/r;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    if-nez v0, :cond_1

    .line 41
    sget-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context \u4e3a null \u8bf7\u5728\u5730\u56fe\u8c03\u7528\u4e4b\u524d \u4f7f\u7528 MapsInitializer.initialize(Context paramContext) \u6765\u8bbe\u7f6eContext"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    sget-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 47
    const/16 v1, 0x78

    if-gt v0, v1, :cond_2

    .line 48
    const/high16 v0, 0x3f000000    # 0.5f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    .line 63
    :goto_0
    new-instance v0, Lcom/amap/api/mapcore/b;

    sget-object v1, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/amap/api/mapcore/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    return-object v0

    .line 49
    :cond_2
    const/16 v1, 0xa0

    if-gt v0, v1, :cond_3

    .line 50
    const v0, 0x3f4ccccd    # 0.8f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 51
    :cond_3
    const/16 v1, 0xf0

    if-gt v0, v1, :cond_4

    .line 52
    const v0, 0x3f5eb852    # 0.87f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 53
    :cond_4
    const/16 v1, 0x140

    if-gt v0, v1, :cond_5

    .line 54
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 55
    :cond_5
    const/16 v1, 0x1e0

    if-gt v0, v1, :cond_6

    .line 56
    const/high16 v0, 0x3fc00000    # 1.5f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 57
    :cond_6
    const/16 v1, 0x280

    if-gt v0, v1, :cond_7

    .line 58
    const v0, 0x3fe66666    # 1.8f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 60
    :cond_7
    const v0, 0x3f666666    # 0.9f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 252
    iput p1, p0, Lcom/amap/api/mapcore/ae;->b:I

    .line 253
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->f(I)V

    .line 256
    :cond_0
    return-void
.end method

.method public a(Landroid/app/Activity;Lcom/amap/api/maps/AMapOptions;Landroid/os/Bundle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    .line 76
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    if-eqz p1, :cond_0

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/amap/api/mapcore/ae;->a:Landroid/content/Context;

    .line 25
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 88
    const-string v0, "MapFragmentDelegateImp"

    const-string v1, "onCreate"

    const/16 v2, 0x71

    invoke-static {v0, v1, v2}, Lcom/amap/api/mapcore/b/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMapOptions;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    .line 29
    return-void
.end method

.method public b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->onResume()V

    .line 167
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 234
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    if-nez v0, :cond_0

    .line 236
    new-instance v0, Lcom/amap/api/maps/AMapOptions;

    invoke-direct {v0}, Lcom/amap/api/maps/AMapOptions;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ae;->a()Lcom/amap/api/mapcore/r;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/amap/api/mapcore/r;->h(Z)Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMapOptions;->camera(Lcom/amap/api/maps/model/CameraPosition;)Lcom/amap/api/maps/AMapOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    .line 239
    const-string v0, "MapOptions"

    iget-object v1, p0, Lcom/amap/api/mapcore/ae;->d:Lcom/amap/api/maps/AMapOptions;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 242
    :cond_1
    return-void
.end method

.method b(Lcom/amap/api/maps/AMapOptions;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 141
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    if-eqz v0, :cond_1

    .line 143
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getCamera()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_0

    .line 145
    iget-object v1, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    iget-object v2, v0, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget v3, v0, Lcom/amap/api/maps/model/CameraPosition;->zoom:F

    iget v4, v0, Lcom/amap/api/maps/model/CameraPosition;->bearing:F

    iget v0, v0, Lcom/amap/api/maps/model/CameraPosition;->tilt:F

    invoke-static {v2, v3, v4, v0}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;FFF)Lcom/amap/api/mapcore/i;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/mapcore/i;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->w()Lcom/amap/api/mapcore/ac;

    move-result-object v0

    .line 149
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getRotateGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ac;->h(Z)V

    .line 150
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getScrollGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ac;->e(Z)V

    .line 151
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getTiltGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ac;->g(Z)V

    .line 152
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getZoomControlsEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ac;->b(Z)V

    .line 153
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getZoomGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ac;->f(Z)V

    .line 154
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getCompassEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ac;->c(Z)V

    .line 155
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getScaleControlsEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ac;->a(Z)V

    .line 156
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getLogoPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/ac;->a(I)V

    .line 157
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getMapType()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(I)V

    .line 158
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getZOrderOnTop()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->setZOrderOnTop(Z)V

    .line 160
    :cond_1
    return-void
.end method

.method public c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/amap/api/mapcore/ae;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->onPause()V

    .line 191
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 207
    return-void
.end method

.method public e()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ae;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ae;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->r()V

    .line 213
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ae;->a()Lcom/amap/api/mapcore/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->f()V

    .line 224
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 228
    const-string v0, "onLowMemory"

    const-string v1, "onLowMemory run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    return-void
.end method
