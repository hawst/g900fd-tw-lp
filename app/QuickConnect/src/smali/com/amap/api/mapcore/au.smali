.class Lcom/amap/api/mapcore/au;
.super Landroid/view/View;
.source "WaterMarkerView.java"


# instance fields
.field a:Landroid/graphics/Rect;

.field b:I

.field private c:Landroid/graphics/Bitmap;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/graphics/Bitmap;

.field private g:Landroid/graphics/Paint;

.field private h:Z

.field private i:I

.field private j:Lcom/amap/api/mapcore/b;

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    .line 31
    iput-boolean v1, p0, Lcom/amap/api/mapcore/au;->h:Z

    .line 32
    iput v1, p0, Lcom/amap/api/mapcore/au;->i:I

    .line 34
    iput v1, p0, Lcom/amap/api/mapcore/au;->k:I

    .line 35
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->a:Landroid/graphics/Rect;

    .line 36
    const/16 v0, 0xa

    iput v0, p0, Lcom/amap/api/mapcore/au;->b:I

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/mapcore/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    .line 31
    iput-boolean v1, p0, Lcom/amap/api/mapcore/au;->h:Z

    .line 32
    iput v1, p0, Lcom/amap/api/mapcore/au;->i:I

    .line 34
    iput v1, p0, Lcom/amap/api/mapcore/au;->k:I

    .line 35
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->a:Landroid/graphics/Rect;

    .line 36
    const/16 v0, 0xa

    iput v0, p0, Lcom/amap/api/mapcore/au;->b:I

    .line 70
    iput-object p2, p0, Lcom/amap/api/mapcore/au;->j:Lcom/amap/api/mapcore/b;

    .line 71
    invoke-static {}, Lcom/amap/api/mapcore/b/r;->a()Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-static {v0}, Lcom/amap/api/mapcore/b/p;->a(Ljava/lang/String;)Landroid/content/res/AssetManager;

    move-result-object v1

    .line 75
    :try_start_0
    sget-object v0, Lcom/amap/api/mapcore/l;->d:Lcom/amap/api/mapcore/l$a;

    sget-object v2, Lcom/amap/api/mapcore/l$a;->b:Lcom/amap/api/mapcore/l$a;

    if-ne v0, v2, :cond_0

    .line 76
    const-string v0, "apl.data"

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 80
    :goto_0
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/amap/api/mapcore/au;->e:Landroid/graphics/Bitmap;

    .line 81
    iget-object v2, p0, Lcom/amap/api/mapcore/au;->e:Landroid/graphics/Bitmap;

    sget v3, Lcom/amap/api/mapcore/l;->a:F

    invoke-static {v2, v3}, Lcom/amap/api/mapcore/b/r;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/amap/api/mapcore/au;->c:Landroid/graphics/Bitmap;

    .line 83
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 85
    sget-object v0, Lcom/amap/api/mapcore/l;->d:Lcom/amap/api/mapcore/l$a;

    sget-object v2, Lcom/amap/api/mapcore/l$a;->b:Lcom/amap/api/mapcore/l$a;

    if-ne v0, v2, :cond_1

    .line 86
    const-string v0, "apl1.data"

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 91
    :goto_1
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/mapcore/au;->f:Landroid/graphics/Bitmap;

    .line 92
    iget-object v1, p0, Lcom/amap/api/mapcore/au;->f:Landroid/graphics/Bitmap;

    sget v2, Lcom/amap/api/mapcore/l;->a:F

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/r;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    .line 94
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 95
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/au;->i:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_2
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 102
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 103
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 105
    return-void

    .line 78
    :cond_0
    :try_start_1
    const-string v0, "ap.data"

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_1
    const-string v0, "ap1.data"

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 44
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 46
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->c:Landroid/graphics/Bitmap;

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    .line 49
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 50
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->e:Landroid/graphics/Bitmap;

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 54
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->f:Landroid/graphics/Bitmap;

    .line 57
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 124
    iput p1, p0, Lcom/amap/api/mapcore/au;->k:I

    .line 125
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/amap/api/mapcore/au;->h:Z

    .line 116
    invoke-virtual {p0}, Lcom/amap/api/mapcore/au;->invalidate()V

    .line 117
    return-void
.end method

.method public b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/amap/api/mapcore/au;->h:Z

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    .line 111
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->c:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public c()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 120
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/amap/api/mapcore/au;->b:I

    invoke-virtual {p0}, Lcom/amap/api/mapcore/au;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/amap/api/mapcore/au;->i:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0xa

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 129
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    const-string v1, "V2.2.0.1"

    const/4 v2, 0x0

    const-string v3, "V2.2.0.1"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/amap/api/mapcore/au;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 131
    iget-object v0, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    iget-object v1, p0, Lcom/amap/api/mapcore/au;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    iget v1, p0, Lcom/amap/api/mapcore/au;->k:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 133
    iget-object v1, p0, Lcom/amap/api/mapcore/au;->j:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v1

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/amap/api/mapcore/au;->b:I

    .line 140
    :goto_0
    sget-object v0, Lcom/amap/api/mapcore/l;->d:Lcom/amap/api/mapcore/l$a;

    sget-object v1, Lcom/amap/api/mapcore/l$a;->b:Lcom/amap/api/mapcore/l$a;

    if-ne v0, v1, :cond_2

    .line 141
    invoke-virtual {p0}, Lcom/amap/api/mapcore/au;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lcom/amap/api/mapcore/au;->b:I

    add-int/lit8 v1, v1, 0xf

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/au;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/amap/api/mapcore/au;->i:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x8

    int-to-float v2, v2

    iget-object v3, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 143
    const-string v0, "V2.2.0.1"

    iget-object v1, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/amap/api/mapcore/au;->b:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x4

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/au;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x10

    int-to-float v2, v2

    iget-object v3, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 153
    :goto_1
    return-void

    .line 134
    :cond_0
    iget v1, p0, Lcom/amap/api/mapcore/au;->k:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 135
    iget-object v1, p0, Lcom/amap/api/mapcore/au;->j:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v1

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/amap/api/mapcore/au;->b:I

    goto :goto_0

    .line 137
    :cond_1
    const/16 v0, 0xa

    iput v0, p0, Lcom/amap/api/mapcore/au;->b:I

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/mapcore/au;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lcom/amap/api/mapcore/au;->b:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/au;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/amap/api/mapcore/au;->i:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x8

    int-to-float v2, v2

    iget-object v3, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 149
    const-string v0, "V2.2.0.1"

    iget-object v1, p0, Lcom/amap/api/mapcore/au;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/amap/api/mapcore/au;->b:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/au;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0xc

    int-to-float v2, v2

    iget-object v3, p0, Lcom/amap/api/mapcore/au;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method
