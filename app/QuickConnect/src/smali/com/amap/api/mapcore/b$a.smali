.class Lcom/amap/api/mapcore/b$a;
.super Ljava/lang/Object;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Lcom/amap/api/mapcore/a/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field a:Ljava/lang/Float;

.field b:Ljava/lang/Float;

.field c:Lcom/autonavi/amap/mapcore/IPoint;

.field d:F

.field e:Lcom/amap/api/mapcore/i;

.field final synthetic f:Lcom/amap/api/mapcore/b;

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2791
    iput-object p1, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2798
    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    .line 2799
    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    .line 2801
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 2827
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b$a;->d:F

    .line 2828
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V
    .locals 0

    .prologue
    .line 2791
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b$a;-><init>(Lcom/amap/api/mapcore/b;)V

    return-void
.end method


# virtual methods
.method public a(FFFFF)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2806
    iput p2, p0, Lcom/amap/api/mapcore/b$a;->g:F

    .line 2807
    iput p3, p0, Lcom/amap/api/mapcore/b$a;->i:F

    .line 2808
    iput p4, p0, Lcom/amap/api/mapcore/b$a;->h:F

    .line 2809
    iput p5, p0, Lcom/amap/api/mapcore/b$a;->j:F

    .line 2810
    iget v0, p0, Lcom/amap/api/mapcore/b$a;->j:F

    iget v1, p0, Lcom/amap/api/mapcore/b$a;->i:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/amap/api/mapcore/b$a;->h:F

    iget v2, p0, Lcom/amap/api/mapcore/b$a;->g:F

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/b$a;->k:F

    .line 2812
    iput-object v3, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    .line 2813
    iput-object v3, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    .line 2815
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2816
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    sget-object v1, Lcom/amap/api/mapcore/i$a;->o:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 2817
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->g(Lcom/amap/api/mapcore/b;)I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;)I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/b$a;->c:Lcom/autonavi/amap/mapcore/IPoint;

    invoke-virtual {v0, v1, v2, v3}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 2818
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->c:Lcom/autonavi/amap/mapcore/IPoint;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->o:Lcom/autonavi/amap/mapcore/IPoint;

    .line 2819
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/amap/api/mapcore/i;->n:Z

    .line 2823
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v1

    iput v1, v0, Lcom/amap/api/mapcore/i;->d:F

    .line 2824
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v1

    iput v1, v0, Lcom/amap/api/mapcore/i;->g:F

    .line 2825
    return-void

    .line 2821
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    sget-object v1, Lcom/amap/api/mapcore/i$a;->d:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    goto :goto_0
.end method

.method public a(Landroid/view/MotionEvent;FFFF)Z
    .locals 8

    .prologue
    .line 2835
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ac;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/ac;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    .line 2836
    const/4 v0, 0x1

    .line 2879
    :goto_0
    return v0

    .line 2838
    :catch_0
    move-exception v0

    .line 2839
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2840
    const/4 v0, 0x1

    goto :goto_0

    .line 2843
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->B(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2844
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2847
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    if-nez v0, :cond_3

    .line 2848
    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    .line 2850
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    if-nez v0, :cond_4

    .line 2851
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    .line 2854
    :cond_4
    iget v0, p0, Lcom/amap/api/mapcore/b$a;->i:F

    sub-float/2addr v0, p3

    .line 2855
    iget v1, p0, Lcom/amap/api/mapcore/b$a;->j:F

    sub-float/2addr v1, p5

    .line 2856
    iget v2, p0, Lcom/amap/api/mapcore/b$a;->g:F

    sub-float/2addr v2, p2

    .line 2857
    iget v3, p0, Lcom/amap/api/mapcore/b$a;->h:F

    sub-float/2addr v3, p4

    .line 2858
    sub-float v4, p5, p3

    sub-float v5, p4, p2

    div-float/2addr v4, v5

    .line 2859
    iget v5, p0, Lcom/amap/api/mapcore/b$a;->k:F

    sub-float v4, v5, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    const-wide v6, 0x3fc999999999999aL    # 0.2

    cmpg-double v4, v4, v6

    if-gez v4, :cond_9

    const/4 v4, 0x0

    cmpl-float v4, v0, v4

    if-lez v4, :cond_5

    const/4 v4, 0x0

    cmpl-float v4, v1, v4

    if-gtz v4, :cond_6

    :cond_5
    const/4 v4, 0x0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_9

    const/4 v0, 0x0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_9

    :cond_6
    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-ltz v0, :cond_7

    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-gez v0, :cond_8

    :cond_7
    const/4 v0, 0x0

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_9

    const/4 v0, 0x0

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_9

    .line 2862
    :cond_8
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float/2addr v0, p3

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    .line 2864
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;Z)Z

    .line 2867
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v1

    sub-float v0, v1, v0

    iput v0, p0, Lcom/amap/api/mapcore/b$a;->d:F

    .line 2871
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    iget v1, p0, Lcom/amap/api/mapcore/b$a;->d:F

    iput v1, v0, Lcom/amap/api/mapcore/i;->f:F

    .line 2872
    iget-object v0, p0, Lcom/amap/api/mapcore/b$a;->f:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/i;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 2874
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    .line 2875
    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    .line 2877
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2879
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
