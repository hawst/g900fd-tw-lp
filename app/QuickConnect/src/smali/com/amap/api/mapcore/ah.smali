.class Lcom/amap/api/mapcore/ah;
.super Landroid/view/View;
.source "MapOverlayImageView.java"


# instance fields
.field a:Lcom/amap/api/mapcore/r;

.field private b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/amap/api/mapcore/v;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/autonavi/amap/mapcore/IPoint;

.field private d:Lcom/amap/api/mapcore/v;

.field private final e:Landroid/os/Handler;

.field private f:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 127
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->e:Landroid/os/Handler;

    .line 141
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/amap/api/mapcore/r;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 127
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->e:Landroid/os/Handler;

    .line 141
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 48
    iput-object p3, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    .line 49
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/ah;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->h()V

    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 228
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    .line 229
    iget-object v2, p0, Lcom/amap/api/mapcore/ah;->d:Lcom/amap/api/mapcore/v;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/amap/api/mapcore/ah;->d:Lcom/amap/api/mapcore/v;

    invoke-interface {v2}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->c()Landroid/graphics/Rect;

    move-result-object v0

    .line 231
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {v2, v3, v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    iput-object v2, p0, Lcom/amap/api/mapcore/ah;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 233
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->m()V

    goto :goto_0

    .line 236
    :cond_1
    return-void
.end method


# virtual methods
.method public a()Lcom/amap/api/mapcore/r;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    return-object v0
.end method

.method public a(Landroid/view/MotionEvent;)Lcom/amap/api/mapcore/v;
    .locals 6

    .prologue
    .line 247
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 248
    const/4 v1, 0x0

    move v2, v0

    .line 249
    :goto_0
    if-ltz v2, :cond_1

    .line 250
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    .line 251
    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->c()Landroid/graphics/Rect;

    move-result-object v3

    .line 252
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/amap/api/mapcore/ah;->a(Landroid/graphics/Rect;II)Z

    move-result v3

    .line 253
    if-eqz v3, :cond_0

    .line 258
    :goto_1
    return-object v0

    .line 249
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Lcom/amap/api/mapcore/v;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    .line 53
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/v;)V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 78
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method a(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    :cond_0
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;Z)V
    .locals 4

    .prologue
    .line 174
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 175
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lcom/amap/api/mapcore/b/r;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    goto :goto_0

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 179
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->e:Landroid/os/Handler;

    new-instance v1, Lcom/amap/api/mapcore/ah$1;

    invoke-direct {v1, p0}, Lcom/amap/api/mapcore/ah$1;-><init>(Lcom/amap/api/mapcore/ah;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 184
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    .line 185
    if-eqz p2, :cond_2

    .line 186
    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 187
    iget-object v2, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, p1, v2}, Lcom/amap/api/mapcore/v;->a(Ljavax/microedition/khronos/opengles/GL10;Lcom/amap/api/mapcore/r;)V

    goto :goto_1

    .line 190
    :cond_2
    iget-object v2, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, p1, v2}, Lcom/amap/api/mapcore/v;->a(Ljavax/microedition/khronos/opengles/GL10;Lcom/amap/api/mapcore/r;)V

    goto :goto_1

    .line 193
    :cond_3
    return-void
.end method

.method public a(Landroid/graphics/Rect;II)Z
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    return v0
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 283
    const/4 v2, 0x0

    .line 284
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    .line 285
    :goto_0
    if-ltz v3, :cond_1

    .line 286
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    .line 287
    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->c()Landroid/graphics/Rect;

    move-result-object v4

    .line 288
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v4, v1, v5}, Lcom/amap/api/mapcore/ah;->a(Landroid/graphics/Rect;II)Z

    move-result v1

    .line 289
    if-eqz v1, :cond_0

    .line 291
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v4, Landroid/graphics/Rect;->left:I

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->s()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-direct {v2, v3, v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    iput-object v2, p0, Lcom/amap/api/mapcore/ah;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 293
    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->d:Lcom/amap/api/mapcore/v;

    move v0, v1

    .line 315
    :goto_1
    return v0

    .line 285
    :cond_0
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public b(Lcom/amap/api/mapcore/v;)Z
    .locals 1

    .prologue
    .line 83
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->h()I

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/ah;->a(Ljava/lang/Integer;)V

    .line 86
    :cond_0
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/ah;->e(Lcom/amap/api/mapcore/v;)V

    .line 87
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    .line 70
    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/ah;->a(Ljava/lang/Integer;)V

    .line 71
    iget-object v2, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->h()I

    move-result v0

    invoke-interface {v2, v0}, Lcom/amap/api/mapcore/r;->d(I)V

    goto :goto_0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 74
    return-void
.end method

.method public c(Lcom/amap/api/mapcore/v;)V
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 92
    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 94
    iget-object v2, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v3, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 99
    return-void
.end method

.method public d()Lcom/amap/api/mapcore/v;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->d:Lcom/amap/api/mapcore/v;

    return-object v0
.end method

.method public d(Lcom/amap/api/mapcore/v;)V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->c:Lcom/autonavi/amap/mapcore/IPoint;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 106
    :cond_0
    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->c()Landroid/graphics/Rect;

    move-result-object v0

    .line 107
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    invoke-interface {p1}, Lcom/amap/api/mapcore/v;->s()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {v1, v2, v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    iput-object v1, p0, Lcom/amap/api/mapcore/ah;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 108
    iput-object p1, p0, Lcom/amap/api/mapcore/ah;->d:Lcom/amap/api/mapcore/v;

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->d()Lcom/amap/api/mapcore/v;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/mapcore/v;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 112
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 267
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    .line 268
    if-eqz v0, :cond_0

    .line 269
    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->p()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 276
    const-string v1, "amapApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MapOverlayImageView clear erro"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :goto_1
    return-void

    .line 273
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->c()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public e(Lcom/amap/api/mapcore/v;)V
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/ah;->f(Lcom/amap/api/mapcore/v;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0}, Lcom/amap/api/mapcore/r;->A()V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->d:Lcom/amap/api/mapcore/v;

    .line 121
    :cond_0
    return-void
.end method

.method public f()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 323
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 324
    new-instance v9, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v1}, Lcom/amap/api/mapcore/r;->j()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v2}, Lcom/amap/api/mapcore/r;->k()I

    move-result v2

    invoke-direct {v9, v0, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 327
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    move v7, v0

    .line 328
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->e()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    .line 330
    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v1 .. v6}, Lcom/amap/api/mapcore/r;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 331
    iget v0, v6, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v1, v6, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {p0, v9, v0, v1}, Lcom/amap/api/mapcore/ah;->a(Landroid/graphics/Rect;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    new-instance v1, Lcom/amap/api/maps/model/Marker;

    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v7}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/v;)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 335
    :cond_1
    return-object v8
.end method

.method public f(Lcom/amap/api/mapcore/v;)Z
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->a:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->b(Lcom/amap/api/mapcore/v;)Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/v;

    .line 341
    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 342
    invoke-interface {v0}, Lcom/amap/api/mapcore/v;->x()V

    goto :goto_0

    .line 345
    :cond_1
    return-void
.end method
