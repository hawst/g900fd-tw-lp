.class Lcom/amap/api/mapcore/b$d;
.super Ljava/lang/Object;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Lcom/amap/api/mapcore/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field a:F

.field b:F

.field c:Lcom/autonavi/amap/mapcore/IPoint;

.field d:Lcom/amap/api/mapcore/i;

.field final synthetic e:Lcom/amap/api/mapcore/b;


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2711
    iput-object p1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2713
    iput v0, p0, Lcom/amap/api/mapcore/b$d;->a:F

    .line 2714
    iput v0, p0, Lcom/amap/api/mapcore/b$d;->b:F

    .line 2715
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b$d;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 2716
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/i;

    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b$1;)V
    .locals 0

    .prologue
    .line 2711
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b$d;-><init>(Lcom/amap/api/mapcore/b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/amap/api/mapcore/a/c;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2721
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2722
    const/4 v0, 0x0

    .line 2737
    :cond_0
    :goto_0
    return v0

    .line 2724
    :cond_1
    invoke-virtual {p1}, Lcom/amap/api/mapcore/a/c;->b()F

    move-result v1

    .line 2725
    iget v2, p0, Lcom/amap/api/mapcore/b$d;->a:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/amap/api/mapcore/b$d;->a:F

    .line 2726
    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->B(Lcom/amap/api/mapcore/b;)Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Lcom/amap/api/mapcore/b$d;->a:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41f00000    # 30.0f

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_2

    iget v2, p0, Lcom/amap/api/mapcore/b$d;->a:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x43af0000    # 350.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 2727
    :cond_2
    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v2, v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;Z)Z

    .line 2728
    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/amap/api/mapcore/b$d;->b:F

    .line 2732
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/i;

    iget v2, p0, Lcom/amap/api/mapcore/b$d;->b:F

    iput v2, v1, Lcom/amap/api/mapcore/i;->g:F

    .line 2733
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/i;

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 2734
    const/4 v1, 0x0

    iput v1, p0, Lcom/amap/api/mapcore/b$d;->a:F

    goto :goto_0
.end method

.method public b(Lcom/amap/api/mapcore/a/c;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2744
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ac;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/ac;->h()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 2767
    :cond_0
    :goto_0
    return v0

    .line 2747
    :catch_0
    move-exception v1

    .line 2748
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2750
    :cond_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2751
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/i;

    sget-object v2, Lcom/amap/api/mapcore/i$a;->e:Lcom/amap/api/mapcore/i$a;

    iput-object v2, v1, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 2758
    :goto_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;Z)Z

    .line 2759
    const/4 v1, 0x0

    iput v1, p0, Lcom/amap/api/mapcore/b$d;->a:F

    .line 2760
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;I)I

    .line 2761
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2764
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/amap/api/mapcore/a/c;->c()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 2765
    const/4 v0, 0x1

    goto :goto_0

    .line 2753
    :cond_2
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/i;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;)Z

    move-result v2

    iput-boolean v2, v1, Lcom/amap/api/mapcore/i;->n:Z

    .line 2754
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/i;

    sget-object v2, Lcom/amap/api/mapcore/i$a;->f:Lcom/amap/api/mapcore/i$a;

    iput-object v2, v1, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 2755
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->g(Lcom/amap/api/mapcore/b;)I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;)I

    move-result v3

    iget-object v4, p0, Lcom/amap/api/mapcore/b$d;->c:Lcom/autonavi/amap/mapcore/IPoint;

    invoke-virtual {v1, v2, v3, v4}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 2756
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/i;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->c:Lcom/autonavi/amap/mapcore/IPoint;

    iput-object v2, v1, Lcom/amap/api/mapcore/i;->o:Lcom/autonavi/amap/mapcore/IPoint;

    goto :goto_1
.end method

.method public c(Lcom/amap/api/mapcore/a/c;)V
    .locals 2

    .prologue
    .line 2773
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b$d;->a:F

    .line 2774
    iget-object v0, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->B(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2775
    iget-object v0, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;Z)Z

    .line 2776
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2778
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/amap/api/mapcore/i;->p:Z

    .line 2779
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ag;->a(Lcom/amap/api/mapcore/i;)V

    .line 2781
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$d;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->j(Lcom/amap/api/mapcore/b;)V

    .line 2782
    return-void
.end method
