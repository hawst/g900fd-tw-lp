.class Lcom/amap/api/mapcore/as;
.super Ljava/lang/Object;
.source "UiSettingsDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/ac;


# instance fields
.field final a:Landroid/os/Handler;

.field private b:Lcom/amap/api/mapcore/r;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/r;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-boolean v0, p0, Lcom/amap/api/mapcore/as;->c:Z

    .line 10
    iput-boolean v0, p0, Lcom/amap/api/mapcore/as;->d:Z

    .line 11
    iput-boolean v0, p0, Lcom/amap/api/mapcore/as;->e:Z

    .line 12
    iput-boolean v1, p0, Lcom/amap/api/mapcore/as;->f:Z

    .line 13
    iput-boolean v0, p0, Lcom/amap/api/mapcore/as;->g:Z

    .line 14
    iput-boolean v0, p0, Lcom/amap/api/mapcore/as;->h:Z

    .line 15
    iput-boolean v0, p0, Lcom/amap/api/mapcore/as;->i:Z

    .line 16
    iput-boolean v1, p0, Lcom/amap/api/mapcore/as;->j:Z

    .line 17
    iput v1, p0, Lcom/amap/api/mapcore/as;->k:I

    .line 24
    new-instance v0, Lcom/amap/api/mapcore/at;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/at;-><init>(Lcom/amap/api/mapcore/as;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/os/Handler;

    .line 51
    iput-object p1, p0, Lcom/amap/api/mapcore/as;->b:Lcom/amap/api/mapcore/r;

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/as;)Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->h:Z

    return v0
.end method

.method static synthetic b(Lcom/amap/api/mapcore/as;)Lcom/amap/api/mapcore/r;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->b:Lcom/amap/api/mapcore/r;

    return-object v0
.end method

.method static synthetic c(Lcom/amap/api/mapcore/as;)Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->j:Z

    return v0
.end method

.method static synthetic d(Lcom/amap/api/mapcore/as;)Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->i:Z

    return v0
.end method

.method static synthetic e(Lcom/amap/api/mapcore/as;)Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->f:Z

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 115
    iput p1, p0, Lcom/amap/api/mapcore/as;->k:I

    .line 116
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->b:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->c(I)V

    .line 117
    return-void
.end method

.method public a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/amap/api/mapcore/as;->j:Z

    .line 57
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 59
    return-void
.end method

.method public a()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->j:Z

    return v0
.end method

.method public b(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/amap/api/mapcore/as;->h:Z

    .line 64
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 66
    return-void
.end method

.method public b()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->h:Z

    return v0
.end method

.method public c(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/amap/api/mapcore/as;->i:Z

    .line 71
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 73
    return-void
.end method

.method public c()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->i:Z

    return v0
.end method

.method public d(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/amap/api/mapcore/as;->f:Z

    .line 79
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 81
    return-void
.end method

.method public d()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->f:Z

    return v0
.end method

.method public e(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/amap/api/mapcore/as;->d:Z

    .line 87
    return-void
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->d:Z

    return v0
.end method

.method public f(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/amap/api/mapcore/as;->g:Z

    .line 92
    return-void
.end method

.method public f()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->g:Z

    return v0
.end method

.method public g(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/amap/api/mapcore/as;->e:Z

    .line 97
    return-void
.end method

.method public g()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->e:Z

    return v0
.end method

.method public h(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/amap/api/mapcore/as;->c:Z

    .line 103
    return-void
.end method

.method public h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/amap/api/mapcore/as;->c:Z

    return v0
.end method

.method public i()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 161
    iget v0, p0, Lcom/amap/api/mapcore/as;->k:I

    return v0
.end method

.method public i(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/as;->h(Z)V

    .line 108
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/as;->g(Z)V

    .line 109
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/as;->f(Z)V

    .line 110
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/as;->e(Z)V

    .line 111
    return-void
.end method
