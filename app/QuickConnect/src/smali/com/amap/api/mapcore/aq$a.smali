.class public Lcom/amap/api/mapcore/aq$a;
.super Ljava/lang/Object;
.source "TileOverlayDelegateImp.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/aq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public e:Lcom/autonavi/amap/mapcore/IPoint;

.field public f:I

.field public g:Z

.field public h:Ljava/nio/FloatBuffer;

.field public i:Landroid/graphics/Bitmap;

.field public j:Lcom/amap/api/mapcore/b/m$a;

.field public k:I

.field final synthetic l:Lcom/amap/api/mapcore/aq;


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/aq;IIII)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 555
    iput-object p1, p0, Lcom/amap/api/mapcore/aq$a;->l:Lcom/amap/api/mapcore/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 664
    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->f:I

    .line 665
    iput-boolean v0, p0, Lcom/amap/api/mapcore/aq$a;->g:Z

    .line 666
    iput-object v1, p0, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    .line 667
    iput-object v1, p0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    .line 668
    iput-object v1, p0, Lcom/amap/api/mapcore/aq$a;->j:Lcom/amap/api/mapcore/b/m$a;

    .line 669
    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    .line 556
    iput p2, p0, Lcom/amap/api/mapcore/aq$a;->a:I

    .line 557
    iput p3, p0, Lcom/amap/api/mapcore/aq$a;->b:I

    .line 558
    iput p4, p0, Lcom/amap/api/mapcore/aq$a;->c:I

    .line 559
    iput p5, p0, Lcom/amap/api/mapcore/aq$a;->d:I

    .line 560
    return-void
.end method

.method public constructor <init>(Lcom/amap/api/mapcore/aq;Lcom/amap/api/mapcore/aq$a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 562
    iput-object p1, p0, Lcom/amap/api/mapcore/aq$a;->l:Lcom/amap/api/mapcore/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 664
    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->f:I

    .line 665
    iput-boolean v0, p0, Lcom/amap/api/mapcore/aq$a;->g:Z

    .line 666
    iput-object v1, p0, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    .line 667
    iput-object v1, p0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    .line 668
    iput-object v1, p0, Lcom/amap/api/mapcore/aq$a;->j:Lcom/amap/api/mapcore/b/m$a;

    .line 669
    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    .line 563
    iget v0, p2, Lcom/amap/api/mapcore/aq$a;->a:I

    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->a:I

    .line 564
    iget v0, p2, Lcom/amap/api/mapcore/aq$a;->b:I

    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->b:I

    .line 565
    iget v0, p2, Lcom/amap/api/mapcore/aq$a;->c:I

    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->c:I

    .line 566
    iget v0, p2, Lcom/amap/api/mapcore/aq$a;->d:I

    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->d:I

    .line 567
    iget-object v0, p2, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    iput-object v0, p0, Lcom/amap/api/mapcore/aq$a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    .line 568
    iget-object v0, p2, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    iput-object v0, p0, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    .line 569
    return-void
.end method


# virtual methods
.method public a()Lcom/amap/api/mapcore/aq$a;
    .locals 2

    .prologue
    .line 573
    new-instance v0, Lcom/amap/api/mapcore/aq$a;

    iget-object v1, p0, Lcom/amap/api/mapcore/aq$a;->l:Lcom/amap/api/mapcore/aq;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/aq$a;-><init>(Lcom/amap/api/mapcore/aq;Lcom/amap/api/mapcore/aq$a;)V

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/16 v4, 0x6f

    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 610
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 612
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/amap/api/mapcore/aq$a;->j:Lcom/amap/api/mapcore/b/m$a;

    .line 613
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 614
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 615
    invoke-static {v0}, Lcom/amap/api/mapcore/b/r;->a(I)I

    move-result v0

    invoke-static {v1}, Lcom/amap/api/mapcore/b/r;->a(I)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/amap/api/mapcore/b/r;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    .line 618
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->l:Lcom/amap/api/mapcore/aq;

    invoke-static {v0}, Lcom/amap/api/mapcore/aq;->a(Lcom/amap/api/mapcore/aq;)Lcom/amap/api/mapcore/r;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->e(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    :cond_0
    :goto_0
    return-void

    .line 619
    :catch_0
    move-exception v0

    .line 620
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 621
    iget v0, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    if-ge v0, v3, :cond_0

    .line 622
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->l:Lcom/amap/api/mapcore/aq;

    invoke-static {v0}, Lcom/amap/api/mapcore/aq;->b(Lcom/amap/api/mapcore/aq;)Lcom/amap/api/mapcore/b/k;

    move-result-object v0

    invoke-virtual {v0, v2, p0}, Lcom/amap/api/mapcore/b/k;->a(ZLcom/amap/api/mapcore/aq$a;)V

    .line 623
    iget v0, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    .line 624
    const-string v0, "TileOverlayDelegateImp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBitmap Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "retry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/amap/api/mapcore/b/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 630
    :cond_1
    iget v0, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    if-ge v0, v3, :cond_0

    .line 631
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->l:Lcom/amap/api/mapcore/aq;

    invoke-static {v0}, Lcom/amap/api/mapcore/aq;->b(Lcom/amap/api/mapcore/aq;)Lcom/amap/api/mapcore/b/k;

    move-result-object v0

    invoke-virtual {v0, v2, p0}, Lcom/amap/api/mapcore/b/k;->a(ZLcom/amap/api/mapcore/aq$a;)V

    .line 632
    iget v0, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    .line 633
    const-string v0, "TileOverlayDelegateImp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBitmap failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "retry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/amap/api/mapcore/aq$a;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/amap/api/mapcore/b/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 642
    invoke-static {p0}, Lcom/amap/api/mapcore/b/m;->a(Lcom/amap/api/mapcore/aq$a;)V

    .line 643
    iget-boolean v0, p0, Lcom/amap/api/mapcore/aq$a;->g:Z

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->l:Lcom/amap/api/mapcore/aq;

    invoke-static {v0}, Lcom/amap/api/mapcore/aq;->c(Lcom/amap/api/mapcore/aq;)Lcom/amap/api/mapcore/ar;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/mapcore/ar;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget v1, p0, Lcom/amap/api/mapcore/aq$a;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 646
    :cond_0
    iput-boolean v3, p0, Lcom/amap/api/mapcore/aq$a;->g:Z

    .line 647
    iput v3, p0, Lcom/amap/api/mapcore/aq$a;->f:I

    .line 648
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 651
    :cond_1
    iput-object v2, p0, Lcom/amap/api/mapcore/aq$a;->i:Landroid/graphics/Bitmap;

    .line 652
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    .line 653
    iget-object v0, p0, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 655
    :cond_2
    iput-object v2, p0, Lcom/amap/api/mapcore/aq$a;->h:Ljava/nio/FloatBuffer;

    .line 656
    iput-object v2, p0, Lcom/amap/api/mapcore/aq$a;->j:Lcom/amap/api/mapcore/b/m$a;

    .line 657
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/amap/api/mapcore/aq$a;->a()Lcom/amap/api/mapcore/aq$a;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 578
    if-ne p0, p1, :cond_1

    .line 586
    :cond_0
    :goto_0
    return v0

    .line 581
    :cond_1
    instance-of v2, p1, Lcom/amap/api/mapcore/aq$a;

    if-nez v2, :cond_2

    move v0, v1

    .line 582
    goto :goto_0

    .line 585
    :cond_2
    check-cast p1, Lcom/amap/api/mapcore/aq$a;

    .line 586
    iget v2, p0, Lcom/amap/api/mapcore/aq$a;->a:I

    iget v3, p1, Lcom/amap/api/mapcore/aq$a;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/mapcore/aq$a;->b:I

    iget v3, p1, Lcom/amap/api/mapcore/aq$a;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/mapcore/aq$a;->c:I

    iget v3, p1, Lcom/amap/api/mapcore/aq$a;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/mapcore/aq$a;->d:I

    iget v3, p1, Lcom/amap/api/mapcore/aq$a;->d:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 593
    iget v0, p0, Lcom/amap/api/mapcore/aq$a;->a:I

    mul-int/lit8 v0, v0, 0x7

    iget v1, p0, Lcom/amap/api/mapcore/aq$a;->b:I

    mul-int/lit8 v1, v1, 0xb

    add-int/2addr v0, v1

    iget v1, p0, Lcom/amap/api/mapcore/aq$a;->c:I

    mul-int/lit8 v1, v1, 0xd

    add-int/2addr v0, v1

    iget v1, p0, Lcom/amap/api/mapcore/aq$a;->d:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 598
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 599
    iget v1, p0, Lcom/amap/api/mapcore/aq$a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 600
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 601
    iget v1, p0, Lcom/amap/api/mapcore/aq$a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 602
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 603
    iget v1, p0, Lcom/amap/api/mapcore/aq$a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 604
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    iget v1, p0, Lcom/amap/api/mapcore/aq$a;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 606
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
