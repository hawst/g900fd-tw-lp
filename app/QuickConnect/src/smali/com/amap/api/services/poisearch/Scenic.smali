.class public final Lcom/amap/api/services/poisearch/Scenic;
.super Ljava/lang/Object;
.source "Scenic.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/amap/api/services/poisearch/Scenic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179
    new-instance v0, Lcom/amap/api/services/poisearch/m;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/m;-><init>()V

    sput-object v0, Lcom/amap/api/services/poisearch/Scenic;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    .line 170
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    .line 172
    sget-object v0, Lcom/amap/api/services/poisearch/Photo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    .line 173
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    .line 38
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    .line 134
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    .line 46
    return-void
.end method

.method c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    .line 54
    return-void
.end method

.method d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 220
    if-ne p0, p1, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v0

    .line 222
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 223
    goto :goto_0

    .line 224
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 225
    goto :goto_0

    .line 226
    :cond_3
    check-cast p1, Lcom/amap/api/services/poisearch/Scenic;

    .line 227
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 228
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 229
    goto :goto_0

    .line 230
    :cond_4
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 231
    goto :goto_0

    .line 232
    :cond_5
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 233
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 234
    goto :goto_0

    .line 235
    :cond_6
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 236
    goto :goto_0

    .line 237
    :cond_7
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 238
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 239
    goto :goto_0

    .line 240
    :cond_8
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 241
    goto :goto_0

    .line 242
    :cond_9
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 243
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 244
    goto :goto_0

    .line 245
    :cond_a
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 246
    goto :goto_0

    .line 247
    :cond_b
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 248
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 249
    goto :goto_0

    .line 250
    :cond_c
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 251
    goto :goto_0

    .line 252
    :cond_d
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 253
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    .line 254
    goto/16 :goto_0

    .line 255
    :cond_e
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 256
    goto/16 :goto_0

    .line 257
    :cond_f
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 258
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    if-eqz v2, :cond_11

    move v0, v1

    .line 259
    goto/16 :goto_0

    .line 260
    :cond_10
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 261
    goto/16 :goto_0

    .line 262
    :cond_11
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    if-nez v2, :cond_12

    .line 263
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    if-eqz v2, :cond_13

    move v0, v1

    .line 264
    goto/16 :goto_0

    .line 265
    :cond_12
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 266
    goto/16 :goto_0

    .line 267
    :cond_13
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    if-nez v2, :cond_14

    .line 268
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    if-eqz v2, :cond_15

    move v0, v1

    .line 269
    goto/16 :goto_0

    .line 270
    :cond_14
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 271
    goto/16 :goto_0

    .line 272
    :cond_15
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    if-nez v2, :cond_16

    .line 273
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    if-eqz v2, :cond_17

    move v0, v1

    .line 274
    goto/16 :goto_0

    .line 275
    :cond_16
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    move v0, v1

    .line 276
    goto/16 :goto_0

    .line 277
    :cond_17
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    if-nez v2, :cond_18

    .line 278
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    if-eqz v2, :cond_19

    move v0, v1

    .line 279
    goto/16 :goto_0

    .line 280
    :cond_18
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    move v0, v1

    .line 281
    goto/16 :goto_0

    .line 282
    :cond_19
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    if-nez v2, :cond_1a

    .line 283
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    if-eqz v2, :cond_1b

    move v0, v1

    .line 284
    goto/16 :goto_0

    .line 285
    :cond_1a
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    move v0, v1

    .line 286
    goto/16 :goto_0

    .line 287
    :cond_1b
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    if-nez v2, :cond_1c

    .line 288
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 289
    goto/16 :goto_0

    .line 290
    :cond_1c
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 291
    goto/16 :goto_0
.end method

.method f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    .line 78
    return-void
.end method

.method g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public getDeepsrc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getIntro()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLevel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getOpentime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getOpentimeGDF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderWapUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderWebUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    return-object v0
.end method

.method public getPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommend()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getSeason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getTheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    return-object v0
.end method

.method h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 194
    .line 196
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 198
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 199
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 200
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 202
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 204
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 206
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 208
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 209
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 210
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 211
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 213
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 214
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    if-nez v2, :cond_c

    :goto_c
    add-int/2addr v0, v1

    .line 215
    return v0

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 199
    :cond_2
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 200
    :cond_3
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 202
    :cond_4
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 204
    :cond_5
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 206
    :cond_6
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 208
    :cond_7
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_7

    .line 209
    :cond_8
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 210
    :cond_9
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 211
    :cond_a
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_a

    .line 213
    :cond_b
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_b

    .line 214
    :cond_c
    iget-object v1, p0, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c
.end method

.method i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    .line 102
    return-void
.end method

.method j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    .line 110
    return-void
.end method

.method k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    .line 118
    return-void
.end method

.method l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Scenic;->m:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 157
    return-void
.end method
