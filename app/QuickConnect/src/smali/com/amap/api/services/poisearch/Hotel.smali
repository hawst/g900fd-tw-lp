.class public final Lcom/amap/api/services/poisearch/Hotel;
.super Ljava/lang/Object;
.source "Hotel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/amap/api/services/poisearch/Hotel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lcom/amap/api/services/poisearch/e;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/e;-><init>()V

    sput-object v0, Lcom/amap/api/services/poisearch/Hotel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    .line 152
    sget-object v0, Lcom/amap/api/services/poisearch/Photo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    .line 153
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    .line 28
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    .line 116
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    .line 36
    return-void
.end method

.method c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    .line 44
    return-void
.end method

.method d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 203
    if-ne p0, p1, :cond_1

    .line 270
    :cond_0
    :goto_0
    return v0

    .line 205
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 206
    goto :goto_0

    .line 207
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 208
    goto :goto_0

    .line 209
    :cond_3
    check-cast p1, Lcom/amap/api/services/poisearch/Hotel;

    .line 210
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 211
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 212
    goto :goto_0

    .line 213
    :cond_4
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 214
    goto :goto_0

    .line 215
    :cond_5
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 216
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 217
    goto :goto_0

    .line 218
    :cond_6
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 219
    goto :goto_0

    .line 220
    :cond_7
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 221
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 222
    goto :goto_0

    .line 223
    :cond_8
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 224
    goto :goto_0

    .line 225
    :cond_9
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 226
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 227
    goto :goto_0

    .line 228
    :cond_a
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 229
    goto :goto_0

    .line 230
    :cond_b
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 231
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 232
    goto :goto_0

    .line 233
    :cond_c
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 234
    goto :goto_0

    .line 235
    :cond_d
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 236
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    .line 237
    goto/16 :goto_0

    .line 238
    :cond_e
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 239
    goto/16 :goto_0

    .line 240
    :cond_f
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 241
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    if-eqz v2, :cond_11

    move v0, v1

    .line 242
    goto/16 :goto_0

    .line 243
    :cond_10
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 244
    goto/16 :goto_0

    .line 245
    :cond_11
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    if-nez v2, :cond_12

    .line 246
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    if-eqz v2, :cond_13

    move v0, v1

    .line 247
    goto/16 :goto_0

    .line 248
    :cond_12
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 249
    goto/16 :goto_0

    .line 250
    :cond_13
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    if-nez v2, :cond_14

    .line 251
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    if-eqz v2, :cond_15

    move v0, v1

    .line 252
    goto/16 :goto_0

    .line 253
    :cond_14
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 254
    goto/16 :goto_0

    .line 255
    :cond_15
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    if-nez v2, :cond_16

    .line 256
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    if-eqz v2, :cond_17

    move v0, v1

    .line 257
    goto/16 :goto_0

    .line 258
    :cond_16
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    move v0, v1

    .line 259
    goto/16 :goto_0

    .line 260
    :cond_17
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    if-nez v2, :cond_18

    .line 261
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    if-eqz v2, :cond_19

    move v0, v1

    .line 262
    goto/16 :goto_0

    .line 263
    :cond_18
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    move v0, v1

    .line 264
    goto/16 :goto_0

    .line 265
    :cond_19
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    if-nez v2, :cond_1a

    .line 266
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 267
    goto/16 :goto_0

    .line 268
    :cond_1a
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 269
    goto/16 :goto_0
.end method

.method f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    .line 68
    return-void
.end method

.method g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public getAddition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getDeepsrc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getEnvironmentRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getFaciRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getHealthRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getIntro()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getLowestPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    return-object v0
.end method

.method public getRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getStar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getTraffic()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    return-object v0
.end method

.method h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 174
    .line 176
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 178
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 180
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 184
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 186
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 188
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 189
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 191
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 192
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 193
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 195
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 196
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    if-nez v2, :cond_b

    :goto_b
    add-int/2addr v0, v1

    .line 198
    return v0

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 184
    :cond_3
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 186
    :cond_4
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 188
    :cond_5
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 189
    :cond_6
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 191
    :cond_7
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_7

    .line 192
    :cond_8
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 193
    :cond_9
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 195
    :cond_a
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_a

    .line 196
    :cond_b
    iget-object v1, p0, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b
.end method

.method i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    .line 92
    return-void
.end method

.method j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    .line 100
    return-void
.end method

.method k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Hotel;->l:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 138
    return-void
.end method
