.class public final Lcom/amap/api/services/poisearch/Dining;
.super Ljava/lang/Object;
.source "Dining.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/amap/api/services/poisearch/Dining;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 249
    new-instance v0, Lcom/amap/api/services/poisearch/b;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/b;-><init>()V

    sput-object v0, Lcom/amap/api/services/poisearch/Dining;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 226
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 227
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/amap/api/services/poisearch/Dining;->a:Z

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    .line 240
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    .line 241
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    .line 242
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    .line 243
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    .line 244
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    .line 245
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    .line 246
    sget-object v0, Lcom/amap/api/services/poisearch/Photo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    .line 247
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    .line 44
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 187
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    .line 188
    return-void
.end method

.method a(Z)V
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/amap/api/services/poisearch/Dining;->a:Z

    .line 36
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    .line 52
    return-void
.end method

.method c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    .line 60
    return-void
.end method

.method d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    return v0
.end method

.method e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 307
    if-ne p0, p1, :cond_1

    .line 411
    :cond_0
    :goto_0
    return v0

    .line 309
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 310
    goto :goto_0

    .line 311
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 312
    goto :goto_0

    .line 313
    :cond_3
    check-cast p1, Lcom/amap/api/services/poisearch/Dining;

    .line 314
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 315
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 316
    goto :goto_0

    .line 317
    :cond_4
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 318
    goto :goto_0

    .line 319
    :cond_5
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 320
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 321
    goto :goto_0

    .line 322
    :cond_6
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 323
    goto :goto_0

    .line 324
    :cond_7
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 325
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 326
    goto :goto_0

    .line 327
    :cond_8
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 328
    goto :goto_0

    .line 329
    :cond_9
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 330
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 331
    goto :goto_0

    .line 332
    :cond_a
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 333
    goto :goto_0

    .line 334
    :cond_b
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 335
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 336
    goto :goto_0

    .line 337
    :cond_c
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 338
    goto :goto_0

    .line 339
    :cond_d
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 340
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    .line 341
    goto/16 :goto_0

    .line 342
    :cond_e
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 343
    goto/16 :goto_0

    .line 344
    :cond_f
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 345
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    if-eqz v2, :cond_11

    move v0, v1

    .line 346
    goto/16 :goto_0

    .line 347
    :cond_10
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 348
    goto/16 :goto_0

    .line 349
    :cond_11
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 350
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    if-eqz v2, :cond_13

    move v0, v1

    .line 351
    goto/16 :goto_0

    .line 352
    :cond_12
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 353
    goto/16 :goto_0

    .line 354
    :cond_13
    iget-boolean v2, p0, Lcom/amap/api/services/poisearch/Dining;->a:Z

    iget-boolean v3, p1, Lcom/amap/api/services/poisearch/Dining;->a:Z

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 355
    goto/16 :goto_0

    .line 356
    :cond_14
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 357
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    if-eqz v2, :cond_16

    move v0, v1

    .line 358
    goto/16 :goto_0

    .line 359
    :cond_15
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 360
    goto/16 :goto_0

    .line 361
    :cond_16
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 362
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    if-eqz v2, :cond_18

    move v0, v1

    .line 363
    goto/16 :goto_0

    .line 364
    :cond_17
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 365
    goto/16 :goto_0

    .line 366
    :cond_18
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    if-nez v2, :cond_19

    .line 367
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    if-eqz v2, :cond_1a

    move v0, v1

    .line 368
    goto/16 :goto_0

    .line 369
    :cond_19
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    .line 370
    goto/16 :goto_0

    .line 371
    :cond_1a
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    if-nez v2, :cond_1b

    .line 372
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    if-eqz v2, :cond_1c

    move v0, v1

    .line 373
    goto/16 :goto_0

    .line 374
    :cond_1b
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    move v0, v1

    .line 375
    goto/16 :goto_0

    .line 376
    :cond_1c
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    if-nez v2, :cond_1d

    .line 377
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    if-eqz v2, :cond_1e

    move v0, v1

    .line 378
    goto/16 :goto_0

    .line 379
    :cond_1d
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    move v0, v1

    .line 380
    goto/16 :goto_0

    .line 381
    :cond_1e
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    if-nez v2, :cond_1f

    .line 382
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    if-eqz v2, :cond_20

    move v0, v1

    .line 383
    goto/16 :goto_0

    .line 384
    :cond_1f
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    move v0, v1

    .line 385
    goto/16 :goto_0

    .line 386
    :cond_20
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    if-nez v2, :cond_21

    .line 387
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    if-eqz v2, :cond_22

    move v0, v1

    .line 388
    goto/16 :goto_0

    .line 389
    :cond_21
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    move v0, v1

    .line 390
    goto/16 :goto_0

    .line 391
    :cond_22
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    if-nez v2, :cond_23

    .line 392
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    if-eqz v2, :cond_24

    move v0, v1

    .line 393
    goto/16 :goto_0

    .line 394
    :cond_23
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    move v0, v1

    .line 395
    goto/16 :goto_0

    .line 396
    :cond_24
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    if-nez v2, :cond_25

    .line 397
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    if-eqz v2, :cond_26

    move v0, v1

    .line 398
    goto/16 :goto_0

    .line 399
    :cond_25
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    move v0, v1

    .line 400
    goto/16 :goto_0

    .line 401
    :cond_26
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    if-nez v2, :cond_27

    .line 402
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    if-eqz v2, :cond_28

    move v0, v1

    .line 403
    goto/16 :goto_0

    .line 404
    :cond_27
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    move v0, v1

    .line 405
    goto/16 :goto_0

    .line 406
    :cond_28
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    if-nez v2, :cond_29

    .line 407
    iget-object v2, p1, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 408
    goto/16 :goto_0

    .line 409
    :cond_29
    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 410
    goto/16 :goto_0
.end method

.method f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    .line 84
    return-void
.end method

.method g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public getAddition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    return-object v0
.end method

.method public getAtmosphere()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getCost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getCpRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getCuisines()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getDeepsrc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getEnvironmentRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getIntro()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getOpentime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    return-object v0
.end method

.method public getOpentimeGDF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderinAppUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderingWapUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderingWebUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    return-object v0
.end method

.method public getRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommend()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getTasteRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    return-object v0
.end method

.method h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 264
    .line 266
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 268
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 270
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 271
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 273
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 275
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 277
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 281
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 282
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/amap/api/services/poisearch/Dining;->a:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x4cf

    :goto_8
    add-int/2addr v0, v2

    .line 283
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 285
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 287
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 289
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 291
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 293
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    .line 294
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    .line 295
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v1

    :goto_10
    add-int/2addr v0, v2

    .line 297
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    if-nez v0, :cond_11

    move v0, v1

    :goto_11
    add-int/2addr v0, v2

    .line 299
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    if-nez v0, :cond_12

    move v0, v1

    :goto_12
    add-int/2addr v0, v2

    .line 300
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    if-nez v2, :cond_13

    :goto_13
    add-int/2addr v0, v1

    .line 302
    return v0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 270
    :cond_2
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 271
    :cond_3
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 273
    :cond_4
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 275
    :cond_5
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 277
    :cond_6
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 281
    :cond_7
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 282
    :cond_8
    const/16 v0, 0x4d5

    goto/16 :goto_8

    .line 283
    :cond_9
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 285
    :cond_a
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 287
    :cond_b
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 289
    :cond_c
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 291
    :cond_d
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 293
    :cond_e
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 294
    :cond_f
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 295
    :cond_10
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 297
    :cond_11
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_11

    .line 299
    :cond_12
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_12

    .line 300
    :cond_13
    iget-object v1, p0, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_13
.end method

.method i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public isMealOrdering()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/amap/api/services/poisearch/Dining;->a:Z

    return v0
.end method

.method j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    .line 116
    return-void
.end method

.method k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    .line 124
    return-void
.end method

.method l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    .line 132
    return-void
.end method

.method m(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    .line 140
    return-void
.end method

.method n(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    .line 148
    return-void
.end method

.method o(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    .line 156
    return-void
.end method

.method p(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    .line 164
    return-void
.end method

.method q(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    .line 172
    return-void
.end method

.method r(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 198
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/amap/api/services/poisearch/Dining;->a:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 199
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Dining;->t:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 218
    return-void
.end method
