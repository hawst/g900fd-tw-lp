.class public Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private currentRoom:Landroid/widget/TextView;

.field private mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

.field private final mServiceConnection:Landroid/content/ServiceConnection;

.field private map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ltv/peel/samsung/widget/service/Device;",
            ">;"
        }
    .end annotation
.end field

.field private paths:[Ljava/lang/String;

.field private roomList:Landroid/widget/ListView;

.field private rooms:[Ltv/peel/samsung/widget/service/Room;

.field private setupStatus:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->map:Ljava/util/HashMap;

    .line 162
    new-instance v0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$1;-><init>(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)V

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 28
    return-void
.end method

.method static synthetic access$0(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;Ltv/peel/samsung/widget/service/IQuickConnectService;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    return-void
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->renderViews()V

    return-void
.end method

.method static synthetic access$3(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)[Ltv/peel/samsung/widget/service/Room;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    return-object v0
.end method

.method static synthetic access$4(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->map:Ljava/util/HashMap;

    return-object v0
.end method

.method private renderViews()V
    .locals 8

    .prologue
    .line 76
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    if-eqz v5, :cond_0

    .line 79
    :try_start_0
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    invoke-interface {v5}, Ltv/peel/samsung/widget/service/IQuickConnectService;->getCurrentRoom()Ltv/peel/samsung/widget/service/Room;

    move-result-object v4

    .line 80
    .local v4, "room":Ltv/peel/samsung/widget/service/Room;
    if-eqz v4, :cond_1

    .line 81
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->currentRoom:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ltv/peel/samsung/widget/service/Room;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .end local v4    # "room":Ltv/peel/samsung/widget/service/Room;
    :goto_0
    :try_start_1
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    invoke-interface {v5}, Ltv/peel/samsung/widget/service/IQuickConnectService;->getRoomList()[Ltv/peel/samsung/widget/service/Room;

    move-result-object v5

    iput-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    .line 92
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    array-length v5, v5

    if-lez v5, :cond_0

    .line 94
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    array-length v5, v5

    new-array v0, v5, [Ljava/lang/String;

    .line 95
    .local v0, "data":[Ljava/lang/String;
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    array-length v5, v5

    new-array v5, v5, [Ljava/lang/String;

    iput-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->paths:[Ljava/lang/String;

    .line 97
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    array-length v5, v5

    if-lt v3, v5, :cond_2

    .line 119
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->roomList:Landroid/widget/ListView;

    new-instance v6, Landroid/widget/ArrayAdapter;

    .line 120
    const v7, 0x1090003

    invoke-direct {v6, p0, v7, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 119
    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 127
    .end local v0    # "data":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_0
    :goto_2
    return-void

    .line 83
    .restart local v4    # "room":Ltv/peel/samsung/widget/service/Room;
    :cond_1
    :try_start_2
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->currentRoom:Landroid/widget/TextView;

    const-string v6, "No current room"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 84
    .end local v4    # "room":Ltv/peel/samsung/widget/service/Room;
    :catch_0
    move-exception v2

    .line 85
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 98
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "data":[Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_2
    :try_start_3
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    .line 99
    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ltv/peel/samsung/widget/service/IQuickConnectService;->getRemoteControlList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 100
    .local v1, "devices":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Device;>;"
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 101
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Room ID: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\nRoom Name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 102
    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Ltv/peel/samsung/widget/service/Room;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\nIs Remocon setup? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 103
    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    iget-object v7, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v7, v7, v3

    invoke-virtual {v7}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ltv/peel/samsung/widget/service/IQuickConnectService;->isRemoteControlSetuped(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 104
    const-string v6, "\nTV ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v5}, Ltv/peel/samsung/widget/service/Device;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\nTV brand: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 105
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v5}, Ltv/peel/samsung/widget/service/Device;->getBrandName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 101
    aput-object v5, v0, v3

    .line 106
    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->paths:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v7, v7, v3

    invoke-virtual {v7}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v5}, Ltv/peel/samsung/widget/service/Device;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v3

    .line 108
    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->map:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v6, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 110
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Room ID: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\nRoom Name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 111
    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Ltv/peel/samsung/widget/service/Room;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\nIs Remocon setup? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 112
    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    iget-object v7, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v7, v7, v3

    invoke-virtual {v7}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ltv/peel/samsung/widget/service/IQuickConnectService;->isRemoteControlSetuped(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 113
    const-string v6, "\nNo TV Found"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 110
    aput-object v5, v0, v3

    .line 114
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->paths:[Ljava/lang/String;

    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    .line 115
    iget-object v5, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->map:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 123
    .end local v0    # "data":[Ljava/lang/String;
    .end local v1    # "devices":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Device;>;"
    .end local v3    # "i":I
    :catch_1
    move-exception v2

    .line 124
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->setContentView(I)V

    .line 43
    const v0, 0x7f080002

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->setupStatus:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f080003

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->currentRoom:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f080004

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->roomList:Landroid/widget/ListView;

    .line 47
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->roomList:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;

    invoke-direct {v1, p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;-><init>(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f070001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 142
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 143
    .local v0, "id":I
    const v1, 0x7f080005

    if-ne v0, v1, :cond_0

    .line 144
    const/4 v1, 0x1

    .line 146
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 151
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 152
    new-instance v0, Landroid/content/Intent;

    const-string v1, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 153
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 154
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 159
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 160
    return-void
.end method
