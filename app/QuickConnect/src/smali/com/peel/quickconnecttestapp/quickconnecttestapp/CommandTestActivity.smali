.class public Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;
.super Landroid/app/Activity;
.source "CommandTestActivity.java"


# instance fields
.field private commandList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/peel/samsung/widget/service/Command;",
            ">;"
        }
    .end annotation
.end field

.field private device:Ltv/peel/samsung/widget/service/Device;

.field private list:Landroid/widget/ListView;

.field private mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

.field private final mServiceConnection:Landroid/content/ServiceConnection;

.field private roomid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 106
    new-instance v0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$1;

    invoke-direct {v0, p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$1;-><init>(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)V

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 27
    return-void
.end method

.method static synthetic access$0(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;Ltv/peel/samsung/widget/service/IQuickConnectService;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    return-void
.end method

.method static synthetic access$1(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->renderViews()V

    return-void
.end method

.method static synthetic access$2(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)Ltv/peel/samsung/widget/service/Device;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;

    return-object v0
.end method

.method static synthetic access$3(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)Ltv/peel/samsung/widget/service/IQuickConnectService;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    return-object v0
.end method

.method static synthetic access$4(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->roomid:Ljava/lang/String;

    return-object v0
.end method

.method private renderViews()V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->list:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandArrayAdapter;

    .line 69
    const v2, 0x1090003

    iget-object v3, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v3}, Ltv/peel/samsung/widget/service/Device;->getCommands()Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 68
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->setContentView(I)V

    .line 42
    invoke-virtual {p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "room_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->roomid:Ljava/lang/String;

    .line 43
    invoke-virtual {p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "device"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ltv/peel/samsung/widget/service/Device;

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;

    .line 45
    const/high16 v0, 0x7f080000

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v2}, Ltv/peel/samsung/widget/service/Device;->getBrandName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " -- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 46
    iget-object v2, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v2}, Ltv/peel/samsung/widget/service/Device;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    const v0, 0x7f080001

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->list:Landroid/widget/ListView;

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "device.getCommands(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v2}, Ltv/peel/samsung/widget/service/Device;->getCommands()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->list:Landroid/widget/ListView;

    new-instance v1, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;

    invoke-direct {v1, p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;-><init>(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 63
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 86
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 87
    .local v0, "id":I
    const v1, 0x7f080005

    if-ne v0, v1, :cond_0

    .line 88
    const/4 v1, 0x1

    .line 90
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 96
    new-instance v0, Landroid/content/Intent;

    const-string v1, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 98
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 103
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 104
    return-void
.end method
