.class public Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CommandArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ltv/peel/samsung/widget/service/Command;",
        ">;"
    }
.end annotation


# instance fields
.field private commands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/peel/samsung/widget/service/Command;",
            ">;"
        }
    .end annotation
.end field

.field private inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ltv/peel/samsung/widget/service/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p3, "commands":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Command;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 24
    iput-object p3, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandArrayAdapter;->commands:Ljava/util/List;

    .line 25
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandArrayAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 26
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 30
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandArrayAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x1090003

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 32
    .local v0, "view":Landroid/view/View;
    :goto_0
    const v1, 0x1020014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandArrayAdapter;->commands:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ltv/peel/samsung/widget/service/Command;

    invoke-virtual {v2}, Ltv/peel/samsung/widget/service/Command;->getCommandName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    return-object v0

    .end local v0    # "view":Landroid/view/View;
    :cond_0
    move-object v0, p2

    .line 30
    goto :goto_0
.end method
