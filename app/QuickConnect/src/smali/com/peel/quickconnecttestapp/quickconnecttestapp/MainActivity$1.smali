.class Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;


# direct methods
.method constructor <init>(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$1;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$1;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    invoke-static {p2}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;->asInterface(Landroid/os/IBinder;)Ltv/peel/samsung/widget/service/IQuickConnectService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->access$0(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;Ltv/peel/samsung/widget/service/IQuickConnectService;)V

    .line 166
    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->access$1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceConnected!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$1;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    # invokes: Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->renderViews()V
    invoke-static {v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->access$2(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)V

    .line 168
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 172
    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->access$1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v0, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$1;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->access$0(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;Ltv/peel/samsung/widget/service/IQuickConnectService;)V

    .line 174
    return-void
.end method
