.class Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;
.super Ljava/lang/Object;
.source "CommandTestActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;


# direct methods
.method constructor <init>(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\n\n##### sending command: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 56
    iget-object v3, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;

    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;
    invoke-static {v3}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->access$2(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)Ltv/peel/samsung/widget/service/Device;

    move-result-object v3

    invoke-virtual {v3}, Ltv/peel/samsung/widget/service/Device;->getCommands()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;

    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->mService:Ltv/peel/samsung/widget/service/IQuickConnectService;
    invoke-static {v1}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->access$3(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)Ltv/peel/samsung/widget/service/IQuickConnectService;

    move-result-object v2

    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;

    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->roomid:Ljava/lang/String;
    invoke-static {v1}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->access$4(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;

    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;
    invoke-static {v1}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->access$2(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)Ltv/peel/samsung/widget/service/Device;

    move-result-object v4

    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;

    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->device:Ltv/peel/samsung/widget/service/Device;
    invoke-static {v1}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;->access$2(Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;)Ltv/peel/samsung/widget/service/Device;

    move-result-object v1

    invoke-virtual {v1}, Ltv/peel/samsung/widget/service/Device;->getCommands()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/peel/samsung/widget/service/Command;

    invoke-interface {v2, v3, v4, v1}, Ltv/peel/samsung/widget/service/IQuickConnectService;->sendRemocon(Ljava/lang/String;Ltv/peel/samsung/widget/service/Device;Ltv/peel/samsung/widget/service/Command;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
