.class Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;


# direct methods
.method constructor <init>(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    const-class v2, Lcom/peel/quickconnecttestapp/quickconnecttestapp/CommandTestActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "room_id"

    iget-object v2, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;
    invoke-static {v2}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->access$3(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)[Ltv/peel/samsung/widget/service/Room;

    move-result-object v2

    aget-object v2, v2, p3

    invoke-virtual {v2}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string v2, "device"

    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->map:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->access$4(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    # getter for: Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->rooms:[Ltv/peel/samsung/widget/service/Room;
    invoke-static {v3}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->access$3(Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;)[Ltv/peel/samsung/widget/service/Room;

    move-result-object v3

    aget-object v3, v3, p3

    invoke-virtual {v3}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 69
    iget-object v1, p0, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity$2;->this$0:Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;

    invoke-virtual {v1, v0}, Lcom/peel/quickconnecttestapp/quickconnecttestapp/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method
