.class Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;
.super Landroid/os/Handler;
.source "CCAVPlayerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sdial/CCAVPlayerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MessageHandler"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ServiceMessageHandler"


# instance fields
.field final synthetic this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;


# direct methods
.method private constructor <init>(Lcom/sec/android/sdial/CCAVPlayerImpl;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/sdial/CCAVPlayerImpl;Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;)V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;-><init>(Lcom/sec/android/sdial/CCAVPlayerImpl;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x0

    .line 221
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 223
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    if-nez v8, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget v7, p1, Landroid/os/Message;->what:I

    .line 228
    .local v7, "what":I
    const-string v8, "ServiceMessageHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Message received in ServiceMessageHandler"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 231
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v8, "function"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 233
    .local v3, "function":Ljava/lang/String;
    const-string v8, "onRemoteMediaPlayerStatusUpdated"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 234
    const-string v8, "State"

    invoke-virtual {v0, v8, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 235
    .local v6, "state":I
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    invoke-interface {v8, v6}, Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;->onRemoteMediaPlayerStatusUpdated(I)V

    goto :goto_0

    .line 236
    .end local v6    # "state":I
    :cond_2
    const-string v8, "onRemoteMediaPlayerMetadataUpdated"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 237
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;->onRemoteMediaPlayerMetadataUpdated()V

    goto :goto_0

    .line 238
    :cond_3
    const-string v8, "onDisconnected"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 239
    const-string v8, "ServiceMessageHandler"

    const-string v9, "onDisconnected in framework"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;->onDisconnected()V

    goto :goto_0

    .line 241
    :cond_4
    const-string v8, "onConnectivityRecovered"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 242
    const-string v8, "ServiceMessageHandler"

    const-string v9, "onConnectivityRecovered in framework"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;->onConnectivityRecovered()V

    goto :goto_0

    .line 244
    :cond_5
    const-string v8, "onConnectionSuspended"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 245
    const-string v8, "ServiceMessageHandler"

    const-string v9, "onConnectionSuspended in framework"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const-string v8, "cause"

    invoke-virtual {v0, v8, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 247
    .local v1, "cause":I
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    invoke-interface {v8, v1}, Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;->onConnectionSuspended(I)V

    goto/16 :goto_0

    .line 248
    .end local v1    # "cause":I
    :cond_6
    const-string v8, "onApplicationDisconnected"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 249
    const-string v8, "errorCode"

    invoke-virtual {v0, v8, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 250
    .local v2, "errorCode":I
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    invoke-interface {v8, v2}, Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;->onApplicationDisconnected(I)V

    goto/16 :goto_0

    .line 251
    .end local v2    # "errorCode":I
    :cond_7
    const-string v8, "onAppConnected"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 252
    const-string v8, "ServiceMessageHandler"

    const-string v9, "onAppConnected..."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;->onAppConnected()V

    goto/16 :goto_0

    .line 254
    :cond_8
    const-string v8, "onProgressUpdate"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 255
    const-string v8, "ServiceMessageHandler"

    const-string v9, "Update the seekbar Intent received"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const-string v8, "CurrentPosition"

    const-wide/16 v10, 0x0

    invoke-virtual {v0, v8, v10, v11}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v4

    .line 257
    .local v4, "progress":D
    iget-object v8, p0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/CCAVPlayerImpl;

    # getter for: Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    invoke-static {v8}, Lcom/sec/android/sdial/CCAVPlayerImpl;->access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    move-result-object v8

    invoke-interface {v8, v4, v5}, Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;->onProgressUpdate(D)V

    goto/16 :goto_0
.end method
