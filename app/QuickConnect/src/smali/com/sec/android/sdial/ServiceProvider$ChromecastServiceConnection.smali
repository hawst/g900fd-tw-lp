.class Lcom/sec/android/sdial/ServiceProvider$ChromecastServiceConnection;
.super Ljava/lang/Object;
.source "ServiceProvider.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sdial/ServiceProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ChromecastServiceConnection"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/sdial/ServiceProvider$ChromecastServiceConnection;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/sec/android/sdial/ServiceProvider$ChromecastServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "component"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 89
    const-string v0, "ServiceProvider"

    const-string v1, "ChromecastServiceConnection Service Connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    invoke-static {v0}, Lcom/sec/android/sdial/ServiceProvider;->access$0(Landroid/os/Messenger;)V

    .line 91
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/sdial/ServiceProvider;->access$1(Z)V

    .line 92
    # getter for: Lcom/sec/android/sdial/ServiceProvider;->mIServiceConnectionEventlistener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;
    invoke-static {}, Lcom/sec/android/sdial/ServiceProvider;->access$2()Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    move-result-object v0

    # getter for: Lcom/sec/android/sdial/ServiceProvider;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;
    invoke-static {}, Lcom/sec/android/sdial/ServiceProvider;->access$3()Lcom/sec/android/sdial/DeviceFinder;

    move-result-object v1

    # getter for: Lcom/sec/android/sdial/ServiceProvider;->mCCAVPlayer:Lcom/sec/android/sdial/CAVPlayer;
    invoke-static {}, Lcom/sec/android/sdial/ServiceProvider;->access$4()Lcom/sec/android/sdial/CAVPlayer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;->onServiceConnected(Lcom/sec/android/sdial/DeviceFinder;Lcom/sec/android/sdial/CAVPlayer;)V

    .line 93
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "component"    # Landroid/content/ComponentName;

    .prologue
    .line 97
    const-string v0, "ServiceProvider"

    const-string v1, "ChromecastServiceConnectionService Disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/sdial/ServiceProvider;->access$0(Landroid/os/Messenger;)V

    .line 99
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/sdial/ServiceProvider;->access$1(Z)V

    .line 100
    # getter for: Lcom/sec/android/sdial/ServiceProvider;->mIServiceConnectionEventlistener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;
    invoke-static {}, Lcom/sec/android/sdial/ServiceProvider;->access$2()Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;->onServiceDisconnected()V

    .line 101
    return-void
.end method
