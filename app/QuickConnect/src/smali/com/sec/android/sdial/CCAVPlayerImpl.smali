.class Lcom/sec/android/sdial/CCAVPlayerImpl;
.super Lcom/sec/android/sdial/CAVPlayer;
.source "CCAVPlayerImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CCAVPlayerImpl"

.field public static final VOLUME_INCREMENT:D = 0.05


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

.field private mLocalFileServer:Lcom/sec/android/sdial/httpserver/IHttpService;

.field private mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/sdial/CAVPlayer;-><init>(Landroid/content/Context;)V

    .line 36
    iput-object p1, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mContext:Landroid/content/Context;

    .line 38
    iput-object v1, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    .line 40
    new-instance v0, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;-><init>(Lcom/sec/android/sdial/CCAVPlayerImpl;Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;)V

    iput-object v0, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    .line 42
    new-instance v0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;

    invoke-direct {v0}, Lcom/sec/android/sdial/httpserver/NanoHttpServer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mLocalFileServer:Lcom/sec/android/sdial/httpserver/IHttpService;

    .line 43
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/sdial/CCAVPlayerImpl;)Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    return-object v0
.end method


# virtual methods
.method public connectRoute(Ljava/lang/String;)V
    .locals 4
    .param p1, "device_id"    # Ljava/lang/String;

    .prologue
    .line 87
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mLocalFileServer:Lcom/sec/android/sdial/httpserver/IHttpService;

    const/16 v3, 0x1f96

    invoke-interface {v2, v3}, Lcom/sec/android/sdial/httpserver/IHttpService;->start(I)V

    .line 89
    const/4 v2, 0x0

    .line 90
    const/4 v3, 0x1

    .line 89
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 92
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 93
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "setRouteInfo"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v2, "Route_Id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 97
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 98
    return-void
.end method

.method public disconnectRoute()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;,
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation

    .prologue
    .line 192
    const/4 v2, 0x0

    .line 193
    const/4 v3, 0x1

    .line 192
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 195
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 196
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "stopApplication"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 199
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 201
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mLocalFileServer:Lcom/sec/android/sdial/httpserver/IHttpService;

    invoke-interface {v2}, Lcom/sec/android/sdial/httpserver/IHttpService;->stop()V

    .line 202
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mLocalFileServer:Lcom/sec/android/sdial/httpserver/IHttpService;

    invoke-interface {v2}, Lcom/sec/android/sdial/httpserver/IHttpService;->destroy()V

    .line 203
    return-void
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    const-string v0, "Second Screen"

    return-object v0
.end method

.method public getIdleReason()I
    .locals 1

    .prologue
    .line 212
    const/4 v0, -0x1

    return v0
.end method

.method public getPlaybackStatus()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, -0x1

    return v0
.end method

.method public initialize(Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;)V
    .locals 4
    .param p1, "iPlaybackListener"    # Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mIPlaybackListener:Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;

    .line 73
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mLocalFileServer:Lcom/sec/android/sdial/httpserver/IHttpService;

    iget-object v3, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mContext:Landroid/content/Context;

    invoke-interface {v2, v3}, Lcom/sec/android/sdial/httpserver/IHttpService;->create(Landroid/content/Context;)V

    .line 75
    const/4 v2, 0x0

    .line 76
    const/4 v3, 0x1

    .line 75
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 78
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 79
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "initialize"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 82
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 83
    return-void
.end method

.method public loadMedia(Landroid/os/Bundle;ZI)V
    .locals 8
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "autoplay"    # Z
    .param p3, "pos"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation

    .prologue
    .line 144
    const-string v5, "url"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 145
    .local v4, "url":Ljava/lang/String;
    const-string v5, "imageUrl"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 146
    .local v2, "imageUrl":Ljava/lang/String;
    const-string v5, "bigImageUrl"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "bigImageUrl":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 148
    const-string v5, "http"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 150
    iget-object v5, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mLocalFileServer:Lcom/sec/android/sdial/httpserver/IHttpService;

    invoke-interface {v5, v4}, Lcom/sec/android/sdial/httpserver/IHttpService;->getUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 151
    const-string v5, "url"

    invoke-virtual {p1, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v5, "CCAVPlayerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "hosted url "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_0
    if-eqz v2, :cond_1

    .line 156
    const-string v5, "http"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 158
    iget-object v5, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mLocalFileServer:Lcom/sec/android/sdial/httpserver/IHttpService;

    invoke-interface {v5, v2}, Lcom/sec/android/sdial/httpserver/IHttpService;->getUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 159
    const-string v5, "imageUrl"

    invoke-virtual {p1, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v5, "CCAVPlayerImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "image hosted url thumbnail"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_1
    if-eqz v1, :cond_2

    .line 164
    const-string v5, "http"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 168
    move-object v1, v2

    .line 171
    :cond_2
    const/4 v5, 0x0

    .line 172
    const/4 v6, 0x1

    .line 171
    invoke-static {v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    .line 174
    .local v3, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 175
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "function"

    const-string v6, "loadMedia"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v5, "SelectedMedia"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 177
    const-string v5, "AutoPlay"

    invoke-virtual {v0, v5, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 178
    const-string v5, "StartTime"

    invoke-virtual {v0, v5, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 180
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 181
    iget-object v5, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v5, v3}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 182
    return-void
.end method

.method public pause()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sdial/exceptions/CastException;,
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation

    .prologue
    .line 130
    const/4 v2, 0x0

    .line 131
    const/4 v3, 0x1

    .line 130
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 133
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 134
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "pause"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 137
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 138
    return-void
.end method

.method public play()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sdial/exceptions/CastException;,
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation

    .prologue
    .line 117
    const/4 v2, 0x0

    .line 118
    const/4 v3, 0x1

    .line 117
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 120
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 121
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "play"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 124
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 125
    return-void
.end method

.method public play(I)V
    .locals 4
    .param p1, "time"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation

    .prologue
    .line 103
    const/4 v2, 0x0

    .line 104
    const/4 v3, 0x1

    .line 103
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 106
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 107
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "play_time"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v2, "StartTime"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 110
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 111
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 112
    return-void
.end method

.method public volumeDown()V
    .locals 4

    .prologue
    .line 47
    const/4 v2, 0x0

    .line 48
    const/4 v3, 0x1

    .line 47
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 50
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "volumeDown"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 54
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 55
    return-void
.end method

.method public volumeUp()V
    .locals 4

    .prologue
    .line 59
    const/4 v2, 0x0

    .line 60
    const/4 v3, 0x1

    .line 59
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 62
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "volumeUp"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 66
    iget-object v2, p0, Lcom/sec/android/sdial/CCAVPlayerImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/CCAVPlayerImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 67
    return-void
.end method
