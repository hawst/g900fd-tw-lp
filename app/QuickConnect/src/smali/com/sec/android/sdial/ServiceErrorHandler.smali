.class public Lcom/sec/android/sdial/ServiceErrorHandler;
.super Ljava/lang/Object;
.source "ServiceErrorHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ServiceErrorHandler"


# instance fields
.field private mIErrorListener:Lcom/sec/android/sdial/IErrorListener;

.field private mServiceMessageHandler:Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/sdial/ServiceErrorHandler;)Lcom/sec/android/sdial/IErrorListener;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/sdial/ServiceErrorHandler;->mIErrorListener:Lcom/sec/android/sdial/IErrorListener;

    return-object v0
.end method


# virtual methods
.method public setIErrorListener(Lcom/sec/android/sdial/IErrorListener;)V
    .locals 4
    .param p1, "mIErrorListener"    # Lcom/sec/android/sdial/IErrorListener;

    .prologue
    const/4 v3, 0x0

    .line 15
    iput-object p1, p0, Lcom/sec/android/sdial/ServiceErrorHandler;->mIErrorListener:Lcom/sec/android/sdial/IErrorListener;

    .line 17
    new-instance v2, Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;-><init>(Lcom/sec/android/sdial/ServiceErrorHandler;Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;)V

    iput-object v2, p0, Lcom/sec/android/sdial/ServiceErrorHandler;->mServiceMessageHandler:Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;

    .line 20
    const/4 v2, 0x2

    .line 19
    invoke-static {v3, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 22
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 23
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "error_callback_registration"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 26
    iget-object v2, p0, Lcom/sec/android/sdial/ServiceErrorHandler;->mServiceMessageHandler:Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 27
    return-void
.end method
