.class public Lcom/sec/android/sdial/httpserver/SSHttpServer;
.super Ljava/lang/Object;
.source "SSHttpServer.java"

# interfaces
.implements Lcom/sec/android/sdial/httpserver/IHttpService;


# instance fields
.field private isServerOn:Z

.field private isServiceBound:Z

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mPort:I

.field private mService:Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v0, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServerOn:Z

    .line 18
    iput-boolean v0, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServiceBound:Z

    .line 22
    new-instance v0, Lcom/sec/android/sdial/httpserver/SSHttpServer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sdial/httpserver/SSHttpServer$1;-><init>(Lcom/sec/android/sdial/httpserver/SSHttpServer;)V

    iput-object v0, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mConnection:Landroid/content/ServiceConnection;

    .line 15
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/sdial/httpserver/SSHttpServer;Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mService:Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 38
    iput-object p1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mContext:Landroid/content/Context;

    .line 39
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 40
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.embeddedserverservice"

    const-string v2, "com.sec.embeddedserverservice.service.EmbeddedServerService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 42
    iput-boolean v3, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServiceBound:Z

    .line 43
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 46
    iget-boolean v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServiceBound:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mService:Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;

    if-eqz v1, :cond_0

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mService:Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;

    invoke-interface {v1}, Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;->stopServer()V

    .line 49
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 50
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServiceBound:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v3, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mContext:Landroid/content/Context;

    .line 91
    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 90
    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 92
    .local v2, "wim":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    .line 93
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v3

    .line 92
    invoke-static {v3}, Landroid/text/format/Formatter;->formatIpAddress(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "address":Ljava/lang/String;
    const-string v3, "/storage/emulated/legacy"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 96
    const-string v3, "/storage/emulated/legacy"

    const-string v4, "/mnt/sdcard"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 99
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "http://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 100
    iget v4, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mPort:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 99
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "Url":Ljava/lang/String;
    return-object v0
.end method

.method public isStarted()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServerOn:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public start(I)V
    .locals 3
    .param p1, "port"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mPort:I

    .line 61
    iget-boolean v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServiceBound:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mService:Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;

    if-eqz v1, :cond_0

    .line 63
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mService:Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;

    iget v2, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mPort:I

    invoke-interface {v1, v2}, Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;->startServer(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServerOn:Z

    .line 69
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 72
    iget-boolean v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServiceBound:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mService:Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;

    if-eqz v1, :cond_0

    .line 74
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->mService:Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;

    invoke-interface {v1}, Lcom/sec/embeddedserverservice/service/IEmbeddedServerService;->stopServer()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/sdial/httpserver/SSHttpServer;->isServerOn:Z

    .line 80
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
