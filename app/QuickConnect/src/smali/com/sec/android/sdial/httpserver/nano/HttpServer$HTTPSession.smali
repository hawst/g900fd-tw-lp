.class Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;
.super Ljava/lang/Object;
.source "HttpServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sdial/httpserver/nano/HttpServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HTTPSession"
.end annotation


# instance fields
.field private mySocket:Ljava/net/Socket;

.field final synthetic this$0:Lcom/sec/android/sdial/httpserver/nano/HttpServer;


# direct methods
.method public constructor <init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/net/Socket;)V
    .locals 2
    .param p2, "s"    # Ljava/net/Socket;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->this$0:Lcom/sec/android/sdial/httpserver/nano/HttpServer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    iput-object p2, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->mySocket:Ljava/net/Socket;

    .line 217
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 218
    .local v0, "t":Ljava/lang/Thread;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 219
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 220
    return-void
.end method

.method private decodeHeader(Ljava/io/BufferedReader;Ljava/util/Properties;Ljava/util/Properties;Ljava/util/Properties;)V
    .locals 11
    .param p1, "in"    # Ljava/io/BufferedReader;
    .param p2, "pre"    # Ljava/util/Properties;
    .param p3, "parms"    # Ljava/util/Properties;
    .param p4, "header"    # Ljava/util/Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 386
    :try_start_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 387
    .local v0, "inLine":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 434
    .end local v0    # "inLine":Ljava/lang/String;
    :goto_0
    return-void

    .line 389
    .restart local v0    # "inLine":Ljava/lang/String;
    :cond_0
    new-instance v6, Ljava/util/StringTokenizer;

    invoke-direct {v6, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 390
    .local v6, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-nez v8, :cond_1

    .line 391
    const-string v8, "400 Bad Request"

    .line 392
    const-string v9, "BAD REQUEST: Syntax error. Usage: GET /example/file.html"

    .line 391
    invoke-direct {p0, v8, v9}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_1
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 395
    .local v3, "method":Ljava/lang/String;
    const-string v8, "method"

    invoke-virtual {p2, v8, v3}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-nez v8, :cond_2

    .line 398
    const-string v8, "400 Bad Request"

    .line 399
    const-string v9, "BAD REQUEST: Missing URI. Usage: GET /example/file.html"

    .line 398
    invoke-direct {p0, v8, v9}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_2
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    .line 404
    .local v7, "uri":Ljava/lang/String;
    const/16 v8, 0x3f

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 405
    .local v5, "qmi":I
    if-ltz v5, :cond_4

    .line 406
    add-int/lit8 v8, v5, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8, p3}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->decodeParms(Ljava/lang/String;Ljava/util/Properties;)V

    .line 407
    const/4 v8, 0x0

    invoke-virtual {v7, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->decodePercent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 415
    :goto_1
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 416
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 417
    .local v2, "line":Ljava/lang/String;
    :goto_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-gtz v8, :cond_5

    .line 427
    .end local v2    # "line":Ljava/lang/String;
    :cond_3
    const-string v8, "uri"

    invoke-virtual {p2, v8, v7}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 428
    .end local v0    # "inLine":Ljava/lang/String;
    .end local v3    # "method":Ljava/lang/String;
    .end local v5    # "qmi":I
    .end local v6    # "st":Ljava/util/StringTokenizer;
    .end local v7    # "uri":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 430
    .local v1, "ioe":Ljava/io/IOException;
    const-string v8, "500 Internal Server Error"

    .line 431
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "SERVER INTERNAL ERROR: IOException: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 432
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 431
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 429
    invoke-direct {p0, v8, v9}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 409
    .end local v1    # "ioe":Ljava/io/IOException;
    .restart local v0    # "inLine":Ljava/lang/String;
    .restart local v3    # "method":Ljava/lang/String;
    .restart local v5    # "qmi":I
    .restart local v6    # "st":Ljava/util/StringTokenizer;
    .restart local v7    # "uri":Ljava/lang/String;
    :cond_4
    :try_start_1
    invoke-direct {p0, v7}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->decodePercent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 418
    .restart local v2    # "line":Ljava/lang/String;
    :cond_5
    const/16 v8, 0x3a

    invoke-virtual {v2, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 419
    .local v4, "p":I
    if-ltz v4, :cond_6

    .line 420
    const/4 v8, 0x0

    invoke-virtual {v2, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 421
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v2, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 422
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 420
    invoke-virtual {p4, v8, v9}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    :cond_6
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_2
.end method

.method private decodeMultipartData(Ljava/lang/String;[BLjava/io/BufferedReader;Ljava/util/Properties;Ljava/util/Properties;)V
    .locals 21
    .param p1, "boundary"    # Ljava/lang/String;
    .param p2, "fbuf"    # [B
    .param p3, "in"    # Ljava/io/BufferedReader;
    .param p4, "parms"    # Ljava/util/Properties;
    .param p5, "files"    # Ljava/util/Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 444
    .line 445
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    .line 444
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->getBoundaryPositions([B[B)[I

    move-result-object v4

    .line 446
    .local v4, "bpositions":[I
    const/4 v3, 0x1

    .line 447
    .local v3, "boundarycount":I
    invoke-virtual/range {p3 .. p3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    .line 448
    .local v10, "mpline":Ljava/lang/String;
    :cond_0
    :goto_0
    if-nez v10, :cond_1

    .line 524
    .end local v3    # "boundarycount":I
    .end local v4    # "bpositions":[I
    .end local v10    # "mpline":Ljava/lang/String;
    :goto_1
    return-void

    .line 449
    .restart local v3    # "boundarycount":I
    .restart local v4    # "bpositions":[I
    .restart local v10    # "mpline":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 451
    const-string v18, "400 Bad Request"

    .line 452
    const-string v19, "BAD REQUEST: Content type is multipart/form-data but next chunk does not start with boundary. Usage: GET /example/file.html"

    .line 450
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 454
    new-instance v9, Ljava/util/Properties;

    invoke-direct {v9}, Ljava/util/Properties;-><init>()V

    .line 455
    .local v9, "item":Ljava/util/Properties;
    invoke-virtual/range {p3 .. p3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    .line 456
    :goto_2
    if-eqz v10, :cond_3

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    if-gtz v18, :cond_8

    .line 464
    :cond_3
    if-eqz v10, :cond_0

    .line 466
    const-string v18, "content-disposition"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 467
    .local v5, "contentDisposition":Ljava/lang/String;
    if-nez v5, :cond_4

    .line 469
    const-string v18, "400 Bad Request"

    .line 470
    const-string v19, "BAD REQUEST: Content type is multipart/form-data but no content-disposition info found. Usage: GET /example/file.html"

    .line 468
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    :cond_4
    new-instance v15, Ljava/util/StringTokenizer;

    .line 473
    const-string v18, "; "

    .line 472
    move-object/from16 v0, v18

    invoke-direct {v15, v5, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    .local v15, "st":Ljava/util/StringTokenizer;
    new-instance v7, Ljava/util/Properties;

    invoke-direct {v7}, Ljava/util/Properties;-><init>()V

    .line 475
    .local v7, "disposition":Ljava/util/Properties;
    :cond_5
    :goto_3
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v18

    if-nez v18, :cond_a

    .line 483
    const-string v18, "name"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 484
    .local v14, "pname":Ljava/lang/String;
    const/16 v18, 0x1

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 486
    const-string v17, ""

    .line 487
    .local v17, "value":Ljava/lang/String;
    const-string v18, "content-type"

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    if-nez v18, :cond_d

    .line 488
    :cond_6
    :goto_4
    if-eqz v10, :cond_7

    .line 489
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_b

    .line 515
    :cond_7
    :goto_5
    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v14, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 518
    .end local v3    # "boundarycount":I
    .end local v4    # "bpositions":[I
    .end local v5    # "contentDisposition":Ljava/lang/String;
    .end local v7    # "disposition":Ljava/util/Properties;
    .end local v9    # "item":Ljava/util/Properties;
    .end local v10    # "mpline":Ljava/lang/String;
    .end local v14    # "pname":Ljava/lang/String;
    .end local v15    # "st":Ljava/util/StringTokenizer;
    .end local v17    # "value":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 520
    .local v8, "ioe":Ljava/io/IOException;
    const-string v18, "500 Internal Server Error"

    .line 521
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "SERVER INTERNAL ERROR: IOException: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 522
    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 521
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 519
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 457
    .end local v8    # "ioe":Ljava/io/IOException;
    .restart local v3    # "boundarycount":I
    .restart local v4    # "bpositions":[I
    .restart local v9    # "item":Ljava/util/Properties;
    .restart local v10    # "mpline":Ljava/lang/String;
    :cond_8
    const/16 v18, 0x3a

    :try_start_1
    move/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    .line 458
    .local v12, "p":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_9

    .line 459
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v0, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 460
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    add-int/lit8 v19, v12, 0x1

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    .line 461
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    .line 459
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    :cond_9
    invoke-virtual/range {p3 .. p3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_2

    .line 476
    .end local v12    # "p":I
    .restart local v5    # "contentDisposition":Ljava/lang/String;
    .restart local v7    # "disposition":Ljava/util/Properties;
    .restart local v15    # "st":Ljava/util/StringTokenizer;
    :cond_a
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v16

    .line 477
    .local v16, "token":Ljava/lang/String;
    const/16 v18, 0x3d

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    .line 478
    .restart local v12    # "p":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_5

    .line 479
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 480
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    add-int/lit8 v19, v12, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    .line 481
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    .line 479
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 490
    .end local v12    # "p":I
    .end local v16    # "token":Ljava/lang/String;
    .restart local v14    # "pname":Ljava/lang/String;
    .restart local v17    # "value":Ljava/lang/String;
    :cond_b
    invoke-virtual/range {p3 .. p3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    .line 491
    if-eqz v10, :cond_6

    .line 492
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 493
    .local v6, "d":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v6, v0, :cond_c

    .line 494
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_4

    .line 496
    :cond_c
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v19, 0x0

    add-int/lit8 v20, v6, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_4

    .line 500
    .end local v6    # "d":I
    :cond_d
    array-length v0, v4

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v3, v0, :cond_e

    .line 501
    const-string v18, "500 Internal Server Error"

    .line 502
    const-string v19, "Error processing request"

    .line 501
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    :cond_e
    add-int/lit8 v18, v3, -0x2

    aget v18, v4, v18

    .line 503
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->stripMultipartHeaders([BI)I

    move-result v11

    .line 506
    .local v11, "offset":I
    add-int/lit8 v18, v3, -0x1

    aget v18, v4, v18

    sub-int v18, v18, v11

    add-int/lit8 v18, v18, -0x4

    .line 505
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v18

    invoke-direct {v0, v1, v11, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->saveTmpFile([BII)Ljava/lang/String;

    move-result-object v13

    .line 507
    .local v13, "path":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-virtual {v0, v14, v13}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    const-string v18, "filename"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 509
    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 511
    :cond_f
    invoke-virtual/range {p3 .. p3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    .line 512
    if-eqz v10, :cond_7

    .line 513
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v18

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_f

    goto/16 :goto_5
.end method

.method private decodeParms(Ljava/lang/String;Ljava/util/Properties;)V
    .locals 5
    .param p1, "parms"    # Ljava/lang/String;
    .param p2, "p"    # Ljava/util/Properties;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 631
    if-nez p1, :cond_1

    .line 643
    :cond_0
    return-void

    .line 634
    :cond_1
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, "&"

    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    .local v2, "st":Ljava/util/StringTokenizer;
    :cond_2
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 636
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 637
    .local v0, "e":Ljava/lang/String;
    const/16 v3, 0x3d

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 638
    .local v1, "sep":I
    if-ltz v1, :cond_2

    .line 639
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->decodePercent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 640
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->decodePercent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 639
    invoke-virtual {p2, v3, v4}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private decodePercent(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 598
    :try_start_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 599
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 615
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 618
    .end local v2    # "i":I
    .end local v3    # "sb":Ljava/lang/StringBuffer;
    :goto_1
    return-object v4

    .line 600
    .restart local v2    # "i":I
    .restart local v3    # "sb":Ljava/lang/StringBuffer;
    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 601
    .local v0, "c":C
    sparse-switch v0, :sswitch_data_0

    .line 611
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 599
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 603
    :sswitch_0
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 616
    .end local v0    # "c":C
    .end local v2    # "i":I
    .end local v3    # "sb":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v1

    .line 617
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "400 Bad Request"

    const-string v5, "BAD REQUEST: Bad percent-encoding."

    invoke-direct {p0, v4, v5}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const/4 v4, 0x0

    goto :goto_1

    .line 607
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":C
    .restart local v2    # "i":I
    .restart local v3    # "sb":Ljava/lang/StringBuffer;
    :sswitch_1
    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v2, 0x3

    :try_start_1
    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    .line 606
    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 608
    add-int/lit8 v2, v2, 0x2

    .line 609
    goto :goto_2

    .line 601
    nop

    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_1
        0x2b -> :sswitch_0
    .end sparse-switch
.end method

.method private saveTmpFile([BII)Ljava/lang/String;
    .locals 8
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 561
    const-string v2, ""

    .line 562
    .local v2, "path":Ljava/lang/String;
    if-lez p3, :cond_0

    .line 563
    const-string v5, "java.io.tmpdir"

    invoke-static {v5}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 565
    .local v4, "tmpdir":Ljava/lang/String;
    :try_start_0
    const-string v5, "NanoHTTPD"

    const-string v6, ""

    new-instance v7, Ljava/io/File;

    .line 566
    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 565
    invoke-static {v5, v6, v7}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v3

    .line 567
    .local v3, "temp":Ljava/io/File;
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 568
    .local v1, "fstream":Ljava/io/OutputStream;
    invoke-virtual {v1, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 569
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 570
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 575
    .end local v1    # "fstream":Ljava/io/OutputStream;
    .end local v3    # "temp":Ljava/io/File;
    .end local v4    # "tmpdir":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 571
    .restart local v4    # "tmpdir":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "status"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 651
    const-string v0, "text/plain"

    const/4 v1, 0x0

    .line 652
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 651
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendResponse(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;Ljava/io/InputStream;)V

    .line 653
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
.end method

.method private sendResponse(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;Ljava/io/InputStream;)V
    .locals 13
    .param p1, "status"    # Ljava/lang/String;
    .param p2, "mime"    # Ljava/lang/String;
    .param p3, "header"    # Ljava/util/Properties;
    .param p4, "data"    # Ljava/io/InputStream;

    .prologue
    .line 662
    if-nez p1, :cond_1

    .line 663
    :try_start_0
    new-instance v10, Ljava/lang/Error;

    const-string v11, "sendResponse(): Status can\'t be null."

    invoke-direct {v10, v11}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 705
    :catch_0
    move-exception v3

    .line 708
    .local v3, "ioe":Ljava/io/IOException;
    :try_start_1
    iget-object v10, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->mySocket:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 712
    .end local v3    # "ioe":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void

    .line 665
    :cond_1
    :try_start_2
    iget-object v10, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->mySocket:Ljava/net/Socket;

    invoke-virtual {v10}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    .line 666
    .local v5, "out":Ljava/io/OutputStream;
    new-instance v7, Ljava/io/PrintWriter;

    invoke-direct {v7, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 667
    .local v7, "pw":Ljava/io/PrintWriter;
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "HTTP/1.0 "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " \r\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 669
    if-eqz p2, :cond_2

    .line 670
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Content-Type: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\r\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 672
    :cond_2
    if-eqz p3, :cond_3

    const-string v10, "Date"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_4

    .line 673
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Date: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/sec/android/sdial/httpserver/nano/HttpServer;->gmtFrmt:Ljava/text/SimpleDateFormat;
    invoke-static {}, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->access$0()Ljava/text/SimpleDateFormat;

    move-result-object v11

    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    invoke-virtual {v11, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\r\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 675
    :cond_4
    if-eqz p3, :cond_5

    .line 676
    invoke-virtual/range {p3 .. p3}, Ljava/util/Properties;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 677
    .local v2, "e":Ljava/util/Enumeration;
    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-nez v10, :cond_7

    .line 684
    .end local v2    # "e":Ljava/util/Enumeration;
    :cond_5
    const-string v10, "\r\n"

    invoke-virtual {v7, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 685
    invoke-virtual {v7}, Ljava/io/PrintWriter;->flush()V

    .line 687
    if-eqz p4, :cond_6

    .line 688
    invoke-virtual/range {p4 .. p4}, Ljava/io/InputStream;->available()I

    move-result v6

    .line 691
    .local v6, "pending":I
    const/16 v10, 0x800

    new-array v1, v10, [B

    .line 692
    .local v1, "buff":[B
    :goto_2
    if-gtz v6, :cond_8

    .line 701
    .end local v1    # "buff":[B
    .end local v6    # "pending":I
    :cond_6
    invoke-virtual {v5}, Ljava/io/OutputStream;->flush()V

    .line 702
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 703
    if-eqz p4, :cond_0

    .line 704
    invoke-virtual/range {p4 .. p4}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 678
    .restart local v2    # "e":Ljava/util/Enumeration;
    :cond_7
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 679
    .local v4, "key":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 680
    .local v9, "value":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, ": "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\r\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_1

    .line 693
    .end local v2    # "e":Ljava/util/Enumeration;
    .end local v4    # "key":Ljava/lang/String;
    .end local v9    # "value":Ljava/lang/String;
    .restart local v1    # "buff":[B
    .restart local v6    # "pending":I
    :cond_8
    const/4 v11, 0x0

    const/16 v10, 0x800

    if-le v6, v10, :cond_9

    const/16 v10, 0x800

    :goto_3
    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v11, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    .line 695
    .local v8, "read":I
    if-lez v8, :cond_6

    .line 697
    const/4 v10, 0x0

    invoke-virtual {v5, v1, v10, v8}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 698
    sub-int/2addr v6, v8

    goto :goto_2

    .end local v8    # "read":I
    :cond_9
    move v10, v6

    .line 694
    goto :goto_3

    .line 709
    .end local v1    # "buff":[B
    .end local v5    # "out":Ljava/io/OutputStream;
    .end local v6    # "pending":I
    .end local v7    # "pw":Ljava/io/PrintWriter;
    .restart local v3    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v10

    goto/16 :goto_0
.end method

.method private stripMultipartHeaders([BI)I
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I

    .prologue
    const/16 v3, 0xd

    const/16 v2, 0xa

    .line 583
    const/4 v0, 0x0

    .line 584
    .local v0, "i":I
    move v0, p2

    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_1

    .line 589
    :cond_0
    add-int/lit8 v1, v0, 0x1

    return v1

    .line 585
    :cond_1
    aget-byte v1, p1, v0

    if-ne v1, v3, :cond_2

    add-int/lit8 v0, v0, 0x1

    aget-byte v1, p1, v0

    if-ne v1, v2, :cond_2

    add-int/lit8 v0, v0, 0x1

    aget-byte v1, p1, v0

    if-ne v1, v3, :cond_2

    .line 586
    add-int/lit8 v0, v0, 0x1

    aget-byte v1, p1, v0

    if-eq v1, v2, :cond_0

    .line 584
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getBoundaryPositions([B[B)[I
    .locals 7
    .param p1, "b"    # [B
    .param p2, "boundary"    # [B

    .prologue
    .line 530
    const/4 v3, 0x0

    .line 531
    .local v3, "matchcount":I
    const/4 v1, -0x1

    .line 532
    .local v1, "matchbyte":I
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 533
    .local v2, "matchbytes":Ljava/util/Vector;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, p1

    if-lt v0, v5, :cond_0

    .line 549
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v4, v5, [I

    .line 550
    .local v4, "ret":[I
    const/4 v0, 0x0

    :goto_1
    array-length v5, v4

    if-lt v0, v5, :cond_4

    .line 553
    return-object v4

    .line 534
    .end local v4    # "ret":[I
    :cond_0
    aget-byte v5, p1, v0

    aget-byte v6, p2, v3

    if-ne v5, v6, :cond_3

    .line 535
    if-nez v3, :cond_1

    .line 536
    move v1, v0

    .line 537
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 538
    array-length v5, p2

    if-ne v3, v5, :cond_2

    .line 539
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 540
    const/4 v3, 0x0

    .line 541
    const/4 v1, -0x1

    .line 533
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 544
    :cond_3
    sub-int/2addr v0, v3

    .line 545
    const/4 v3, 0x0

    .line 546
    const/4 v1, -0x1

    goto :goto_2

    .line 551
    .restart local v4    # "ret":[I
    :cond_4
    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v4, v0

    .line 550
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public run()V
    .locals 39

    .prologue
    .line 224
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->mySocket:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v27

    .line 225
    .local v27, "is":Ljava/io/InputStream;
    if-nez v27, :cond_1

    .line 375
    .end local v27    # "is":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-void

    .line 231
    .restart local v27    # "is":Ljava/io/InputStream;
    :cond_1
    const/16 v19, 0x2000

    .line 232
    .local v19, "bufsize":I
    move/from16 v0, v19

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 233
    .local v18, "buf":[B
    const/4 v4, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v4, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v33

    .line 234
    .local v33, "rlen":I
    if-lez v33, :cond_0

    .line 238
    new-instance v24, Ljava/io/ByteArrayInputStream;

    const/4 v4, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    move/from16 v2, v33

    invoke-direct {v0, v1, v4, v2}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 240
    .local v24, "hbis":Ljava/io/ByteArrayInputStream;
    new-instance v25, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    .line 241
    move-object/from16 v0, v24

    invoke-direct {v4, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 240
    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 242
    .local v25, "hin":Ljava/io/BufferedReader;
    new-instance v30, Ljava/util/Properties;

    invoke-direct/range {v30 .. v30}, Ljava/util/Properties;-><init>()V

    .line 243
    .local v30, "pre":Ljava/util/Properties;
    new-instance v8, Ljava/util/Properties;

    invoke-direct {v8}, Ljava/util/Properties;-><init>()V

    .line 244
    .local v8, "parms":Ljava/util/Properties;
    new-instance v13, Ljava/util/Properties;

    invoke-direct {v13}, Ljava/util/Properties;-><init>()V

    .line 245
    .local v13, "header":Ljava/util/Properties;
    new-instance v9, Ljava/util/Properties;

    invoke-direct {v9}, Ljava/util/Properties;-><init>()V

    .line 248
    .local v9, "files":Ljava/util/Properties;
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2, v8, v13}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->decodeHeader(Ljava/io/BufferedReader;Ljava/util/Properties;Ljava/util/Properties;Ljava/util/Properties;)V

    .line 249
    const-string v4, "method"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 250
    .local v12, "method":Ljava/lang/String;
    const-string v4, "uri"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 252
    .local v11, "uri":Ljava/lang/String;
    const-wide v36, 0x7fffffffffffffffL

    .line 253
    .local v36, "size":J
    const-string v4, "content-length"

    invoke-virtual {v13, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v20

    .line 254
    .local v20, "contentLength":Ljava/lang/String;
    if-eqz v20, :cond_2

    .line 256
    :try_start_1
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v36, v0

    .line 264
    :cond_2
    :goto_1
    const/16 v35, 0x0

    .line 265
    .local v35, "splitbyte":I
    const/16 v34, 0x0

    .line 266
    .local v34, "sbfound":Z
    :goto_2
    move/from16 v0, v35

    move/from16 v1, v33

    if-lt v0, v1, :cond_b

    .line 275
    :goto_3
    add-int/lit8 v35, v35, 0x1

    .line 279
    :try_start_2
    new-instance v23, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v23 .. v23}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 280
    .local v23, "f":Ljava/io/ByteArrayOutputStream;
    move/from16 v0, v35

    move/from16 v1, v33

    if-ge v0, v1, :cond_3

    .line 281
    sub-int v4, v33, v35

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 289
    :cond_3
    move/from16 v0, v35

    move/from16 v1, v33

    if-ge v0, v1, :cond_d

    .line 290
    sub-int v4, v33, v35

    add-int/lit8 v4, v4, 0x1

    int-to-long v14, v4

    sub-long v36, v36, v14

    .line 295
    :cond_4
    :goto_4
    const/16 v4, 0x200

    new-array v0, v4, [B

    move-object/from16 v18, v0

    .line 296
    :cond_5
    :goto_5
    if-ltz v33, :cond_6

    const-wide/16 v14, 0x0

    cmp-long v4, v36, v14

    if-gtz v4, :cond_f

    .line 304
    :cond_6
    invoke-virtual/range {v23 .. v23}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 307
    .local v6, "fbuf":[B
    new-instance v16, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 308
    .local v16, "bin":Ljava/io/ByteArrayInputStream;
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    .line 309
    move-object/from16 v0, v16

    invoke-direct {v4, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 308
    invoke-direct {v7, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 313
    .local v7, "in":Ljava/io/BufferedReader;
    const-string v4, "POST"

    invoke-virtual {v12, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 314
    const-string v21, ""

    .line 316
    .local v21, "contentType":Ljava/lang/String;
    const-string v4, "content-type"

    invoke-virtual {v13, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 317
    .local v22, "contentTypeHeader":Ljava/lang/String;
    new-instance v38, Ljava/util/StringTokenizer;

    .line 318
    const-string v4, "; "

    .line 317
    move-object/from16 v0, v38

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .local v38, "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v38 .. v38}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 320
    invoke-virtual/range {v38 .. v38}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v21

    .line 323
    :cond_7
    const-string v4, "multipart/form-data"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 325
    invoke-virtual/range {v38 .. v38}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-nez v4, :cond_8

    .line 327
    const-string v4, "400 Bad Request"

    .line 328
    const-string v10, "BAD REQUEST: Content type is multipart/form-data but boundary missing. Usage: GET /example/file.html"

    .line 326
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_8
    invoke-virtual/range {v38 .. v38}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v17

    .line 330
    .local v17, "boundaryExp":Ljava/lang/String;
    new-instance v38, Ljava/util/StringTokenizer;

    .end local v38    # "st":Ljava/util/StringTokenizer;
    const-string v4, "="

    move-object/from16 v0, v38

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    .restart local v38    # "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v38 .. v38}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v4

    const/4 v10, 0x2

    if-eq v4, v10, :cond_9

    .line 333
    const-string v4, "400 Bad Request"

    .line 334
    const-string v10, "BAD REQUEST: Content type is multipart/form-data but boundary syntax error. Usage: GET /example/file.html"

    .line 332
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_9
    invoke-virtual/range {v38 .. v38}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 336
    invoke-virtual/range {v38 .. v38}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .local v5, "boundary":Ljava/lang/String;
    move-object/from16 v4, p0

    .line 338
    invoke-direct/range {v4 .. v9}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->decodeMultipartData(Ljava/lang/String;[BLjava/io/BufferedReader;Ljava/util/Properties;Ljava/util/Properties;)V

    .line 354
    .end local v5    # "boundary":Ljava/lang/String;
    .end local v17    # "boundaryExp":Ljava/lang/String;
    .end local v21    # "contentType":Ljava/lang/String;
    .end local v22    # "contentTypeHeader":Ljava/lang/String;
    .end local v38    # "st":Ljava/util/StringTokenizer;
    :cond_a
    :goto_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->this$0:Lcom/sec/android/sdial/httpserver/nano/HttpServer;

    move-object v14, v8

    move-object v15, v9

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->serve(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;Ljava/util/Properties;Ljava/util/Properties;)Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    move-result-object v31

    .line 355
    .local v31, "r":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    if-nez v31, :cond_13

    .line 356
    const-string v4, "500 Internal Server Error"

    .line 357
    const-string v10, "SERVER INTERNAL ERROR: Serve() returned a null response."

    .line 356
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :goto_7
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V

    .line 362
    invoke-virtual/range {v27 .. v27}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_0

    .line 363
    .end local v6    # "fbuf":[B
    .end local v7    # "in":Ljava/io/BufferedReader;
    .end local v8    # "parms":Ljava/util/Properties;
    .end local v9    # "files":Ljava/util/Properties;
    .end local v11    # "uri":Ljava/lang/String;
    .end local v12    # "method":Ljava/lang/String;
    .end local v13    # "header":Ljava/util/Properties;
    .end local v16    # "bin":Ljava/io/ByteArrayInputStream;
    .end local v18    # "buf":[B
    .end local v19    # "bufsize":I
    .end local v20    # "contentLength":Ljava/lang/String;
    .end local v23    # "f":Ljava/io/ByteArrayOutputStream;
    .end local v24    # "hbis":Ljava/io/ByteArrayInputStream;
    .end local v25    # "hin":Ljava/io/BufferedReader;
    .end local v27    # "is":Ljava/io/InputStream;
    .end local v30    # "pre":Ljava/util/Properties;
    .end local v31    # "r":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .end local v33    # "rlen":I
    .end local v34    # "sbfound":Z
    .end local v35    # "splitbyte":I
    .end local v36    # "size":J
    :catch_0
    move-exception v26

    .line 366
    .local v26, "ioe":Ljava/io/IOException;
    :try_start_3
    const-string v4, "500 Internal Server Error"

    .line 367
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v14, "SERVER INTERNAL ERROR: IOException: "

    invoke-direct {v10, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 368
    invoke-virtual/range {v26 .. v26}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 367
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 365
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendError(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 369
    :catch_1
    move-exception v4

    goto/16 :goto_0

    .line 267
    .end local v26    # "ioe":Ljava/io/IOException;
    .restart local v8    # "parms":Ljava/util/Properties;
    .restart local v9    # "files":Ljava/util/Properties;
    .restart local v11    # "uri":Ljava/lang/String;
    .restart local v12    # "method":Ljava/lang/String;
    .restart local v13    # "header":Ljava/util/Properties;
    .restart local v18    # "buf":[B
    .restart local v19    # "bufsize":I
    .restart local v20    # "contentLength":Ljava/lang/String;
    .restart local v24    # "hbis":Ljava/io/ByteArrayInputStream;
    .restart local v25    # "hin":Ljava/io/BufferedReader;
    .restart local v27    # "is":Ljava/io/InputStream;
    .restart local v30    # "pre":Ljava/util/Properties;
    .restart local v33    # "rlen":I
    .restart local v34    # "sbfound":Z
    .restart local v35    # "splitbyte":I
    .restart local v36    # "size":J
    :cond_b
    :try_start_4
    aget-byte v4, v18, v35

    const/16 v10, 0xd

    if-ne v4, v10, :cond_c

    add-int/lit8 v35, v35, 0x1

    aget-byte v4, v18, v35

    const/16 v10, 0xa

    if-ne v4, v10, :cond_c

    .line 268
    add-int/lit8 v35, v35, 0x1

    aget-byte v4, v18, v35

    const/16 v10, 0xd

    if-ne v4, v10, :cond_c

    .line 269
    add-int/lit8 v35, v35, 0x1

    aget-byte v4, v18, v35

    const/16 v10, 0xa

    if-ne v4, v10, :cond_c

    .line 270
    const/16 v34, 0x1

    .line 271
    goto/16 :goto_3

    .line 273
    :cond_c
    add-int/lit8 v35, v35, 0x1

    goto/16 :goto_2

    .line 291
    .restart local v23    # "f":Ljava/io/ByteArrayOutputStream;
    :cond_d
    if-eqz v34, :cond_e

    const-wide v14, 0x7fffffffffffffffL

    cmp-long v4, v36, v14

    if-nez v4, :cond_4

    .line 292
    :cond_e
    const-wide/16 v36, 0x0

    goto/16 :goto_4

    .line 297
    :cond_f
    const/4 v4, 0x0

    const/16 v10, 0x200

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v4, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v33

    .line 298
    move/from16 v0, v33

    int-to-long v14, v0

    sub-long v36, v36, v14

    .line 299
    if-lez v33, :cond_5

    .line 300
    const/4 v4, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    move/from16 v2, v33

    invoke-virtual {v0, v1, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto/16 :goto_5

    .line 371
    .end local v8    # "parms":Ljava/util/Properties;
    .end local v9    # "files":Ljava/util/Properties;
    .end local v11    # "uri":Ljava/lang/String;
    .end local v12    # "method":Ljava/lang/String;
    .end local v13    # "header":Ljava/util/Properties;
    .end local v18    # "buf":[B
    .end local v19    # "bufsize":I
    .end local v20    # "contentLength":Ljava/lang/String;
    .end local v23    # "f":Ljava/io/ByteArrayOutputStream;
    .end local v24    # "hbis":Ljava/io/ByteArrayInputStream;
    .end local v25    # "hin":Ljava/io/BufferedReader;
    .end local v27    # "is":Ljava/io/InputStream;
    .end local v30    # "pre":Ljava/util/Properties;
    .end local v33    # "rlen":I
    .end local v34    # "sbfound":Z
    .end local v35    # "splitbyte":I
    .end local v36    # "size":J
    :catch_2
    move-exception v4

    goto/16 :goto_0

    .line 341
    .restart local v6    # "fbuf":[B
    .restart local v7    # "in":Ljava/io/BufferedReader;
    .restart local v8    # "parms":Ljava/util/Properties;
    .restart local v9    # "files":Ljava/util/Properties;
    .restart local v11    # "uri":Ljava/lang/String;
    .restart local v12    # "method":Ljava/lang/String;
    .restart local v13    # "header":Ljava/util/Properties;
    .restart local v16    # "bin":Ljava/io/ByteArrayInputStream;
    .restart local v18    # "buf":[B
    .restart local v19    # "bufsize":I
    .restart local v20    # "contentLength":Ljava/lang/String;
    .restart local v21    # "contentType":Ljava/lang/String;
    .restart local v22    # "contentTypeHeader":Ljava/lang/String;
    .restart local v23    # "f":Ljava/io/ByteArrayOutputStream;
    .restart local v24    # "hbis":Ljava/io/ByteArrayInputStream;
    .restart local v25    # "hin":Ljava/io/BufferedReader;
    .restart local v27    # "is":Ljava/io/InputStream;
    .restart local v30    # "pre":Ljava/util/Properties;
    .restart local v33    # "rlen":I
    .restart local v34    # "sbfound":Z
    .restart local v35    # "splitbyte":I
    .restart local v36    # "size":J
    .restart local v38    # "st":Ljava/util/StringTokenizer;
    :cond_10
    const-string v29, ""

    .line 342
    .local v29, "postLine":Ljava/lang/String;
    const/16 v4, 0x200

    new-array v0, v4, [C

    move-object/from16 v28, v0

    .line 343
    .local v28, "pbuf":[C
    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/io/BufferedReader;->read([C)I

    move-result v32

    .line 344
    .local v32, "read":I
    :goto_8
    if-ltz v32, :cond_11

    const-string v4, "\r\n"

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 348
    :cond_11
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v29

    .line 349
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v8}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->decodeParms(Ljava/lang/String;Ljava/util/Properties;)V

    goto/16 :goto_6

    .line 373
    .end local v6    # "fbuf":[B
    .end local v7    # "in":Ljava/io/BufferedReader;
    .end local v8    # "parms":Ljava/util/Properties;
    .end local v9    # "files":Ljava/util/Properties;
    .end local v11    # "uri":Ljava/lang/String;
    .end local v12    # "method":Ljava/lang/String;
    .end local v13    # "header":Ljava/util/Properties;
    .end local v16    # "bin":Ljava/io/ByteArrayInputStream;
    .end local v18    # "buf":[B
    .end local v19    # "bufsize":I
    .end local v20    # "contentLength":Ljava/lang/String;
    .end local v21    # "contentType":Ljava/lang/String;
    .end local v22    # "contentTypeHeader":Ljava/lang/String;
    .end local v23    # "f":Ljava/io/ByteArrayOutputStream;
    .end local v24    # "hbis":Ljava/io/ByteArrayInputStream;
    .end local v25    # "hin":Ljava/io/BufferedReader;
    .end local v27    # "is":Ljava/io/InputStream;
    .end local v28    # "pbuf":[C
    .end local v29    # "postLine":Ljava/lang/String;
    .end local v30    # "pre":Ljava/util/Properties;
    .end local v32    # "read":I
    .end local v33    # "rlen":I
    .end local v34    # "sbfound":Z
    .end local v35    # "splitbyte":I
    .end local v36    # "size":J
    .end local v38    # "st":Ljava/util/StringTokenizer;
    :catch_3
    move-exception v4

    goto/16 :goto_0

    .line 345
    .restart local v6    # "fbuf":[B
    .restart local v7    # "in":Ljava/io/BufferedReader;
    .restart local v8    # "parms":Ljava/util/Properties;
    .restart local v9    # "files":Ljava/util/Properties;
    .restart local v11    # "uri":Ljava/lang/String;
    .restart local v12    # "method":Ljava/lang/String;
    .restart local v13    # "header":Ljava/util/Properties;
    .restart local v16    # "bin":Ljava/io/ByteArrayInputStream;
    .restart local v18    # "buf":[B
    .restart local v19    # "bufsize":I
    .restart local v20    # "contentLength":Ljava/lang/String;
    .restart local v21    # "contentType":Ljava/lang/String;
    .restart local v22    # "contentTypeHeader":Ljava/lang/String;
    .restart local v23    # "f":Ljava/io/ByteArrayOutputStream;
    .restart local v24    # "hbis":Ljava/io/ByteArrayInputStream;
    .restart local v25    # "hin":Ljava/io/BufferedReader;
    .restart local v27    # "is":Ljava/io/InputStream;
    .restart local v28    # "pbuf":[C
    .restart local v29    # "postLine":Ljava/lang/String;
    .restart local v30    # "pre":Ljava/util/Properties;
    .restart local v32    # "read":I
    .restart local v33    # "rlen":I
    .restart local v34    # "sbfound":Z
    .restart local v35    # "splitbyte":I
    .restart local v36    # "size":J
    .restart local v38    # "st":Ljava/util/StringTokenizer;
    :cond_12
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v32

    invoke-static {v0, v10, v1}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 346
    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/io/BufferedReader;->read([C)I

    move-result v32

    goto :goto_8

    .line 359
    .end local v21    # "contentType":Ljava/lang/String;
    .end local v22    # "contentTypeHeader":Ljava/lang/String;
    .end local v28    # "pbuf":[C
    .end local v29    # "postLine":Ljava/lang/String;
    .end local v32    # "read":I
    .end local v38    # "st":Ljava/util/StringTokenizer;
    .restart local v31    # "r":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :cond_13
    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->status:Ljava/lang/String;

    move-object/from16 v0, v31

    iget-object v10, v0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->mimeType:Ljava/lang/String;

    move-object/from16 v0, v31

    iget-object v14, v0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->header:Ljava/util/Properties;

    move-object/from16 v0, v31

    iget-object v15, v0, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->data:Ljava/io/InputStream;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10, v14, v15}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;->sendResponse(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_7

    .line 257
    .end local v6    # "fbuf":[B
    .end local v7    # "in":Ljava/io/BufferedReader;
    .end local v16    # "bin":Ljava/io/ByteArrayInputStream;
    .end local v23    # "f":Ljava/io/ByteArrayOutputStream;
    .end local v31    # "r":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .end local v34    # "sbfound":Z
    .end local v35    # "splitbyte":I
    :catch_4
    move-exception v4

    goto/16 :goto_1
.end method
