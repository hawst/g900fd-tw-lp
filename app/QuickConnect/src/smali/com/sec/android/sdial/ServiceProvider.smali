.class public Lcom/sec/android/sdial/ServiceProvider;
.super Ljava/lang/Object;
.source "ServiceProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sdial/ServiceProvider$ChromecastServiceConnection;,
        Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;
    }
.end annotation


# static fields
.field public static final MESSAGE_DISCOVERY:I = 0x0

.field public static final MESSAGE_ERROR:I = 0x2

.field public static final MESSAGE_PLAYBACK:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ServiceProvider"

.field private static mCCAVPlayer:Lcom/sec/android/sdial/CAVPlayer;

.field private static mChromecastServiceConnection:Landroid/content/ServiceConnection;

.field private static mContext:Landroid/content/Context;

.field private static mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

.field private static mIErrorListener:Lcom/sec/android/sdial/IErrorListener;

.field private static mIServiceConnectionEventlistener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

.field private static mIsBound:Z

.field private static mMessenger:Landroid/os/Messenger;

.field private static mServiceErrorHandler:Lcom/sec/android/sdial/ServiceErrorHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/sdial/ServiceProvider;->mIsBound:Z

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Landroid/os/Messenger;)V
    .locals 0

    .prologue
    .line 18
    sput-object p0, Lcom/sec/android/sdial/ServiceProvider;->mMessenger:Landroid/os/Messenger;

    return-void
.end method

.method static synthetic access$1(Z)V
    .locals 0

    .prologue
    .line 19
    sput-boolean p0, Lcom/sec/android/sdial/ServiceProvider;->mIsBound:Z

    return-void
.end method

.method static synthetic access$2()Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/sdial/ServiceProvider;->mIServiceConnectionEventlistener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    return-object v0
.end method

.method static synthetic access$3()Lcom/sec/android/sdial/DeviceFinder;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/sdial/ServiceProvider;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    return-object v0
.end method

.method static synthetic access$4()Lcom/sec/android/sdial/CAVPlayer;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/sdial/ServiceProvider;->mCCAVPlayer:Lcom/sec/android/sdial/CAVPlayer;

    return-object v0
.end method

.method public static sendMessage(Landroid/os/Handler;Landroid/os/Message;)V
    .locals 5
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 73
    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 74
    const-string v1, "ServiceProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendMessage request msg = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "function"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    sget-boolean v1, Lcom/sec/android/sdial/ServiceProvider;->mIsBound:Z

    if-eqz v1, :cond_0

    .line 77
    :try_start_0
    sget-object v1, Lcom/sec/android/sdial/ServiceProvider;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 78
    const-string v1, "ServiceProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sending message msg = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "function"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setIErrorListener(Lcom/sec/android/sdial/IErrorListener;)V
    .locals 2
    .param p0, "l"    # Lcom/sec/android/sdial/IErrorListener;

    .prologue
    .line 60
    sput-object p0, Lcom/sec/android/sdial/ServiceProvider;->mIErrorListener:Lcom/sec/android/sdial/IErrorListener;

    .line 61
    sget-object v0, Lcom/sec/android/sdial/ServiceProvider;->mServiceErrorHandler:Lcom/sec/android/sdial/ServiceErrorHandler;

    sget-object v1, Lcom/sec/android/sdial/ServiceProvider;->mIErrorListener:Lcom/sec/android/sdial/IErrorListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/sdial/ServiceErrorHandler;->setIErrorListener(Lcom/sec/android/sdial/IErrorListener;)V

    .line 62
    return-void
.end method

.method private static startChromecastService()V
    .locals 4

    .prologue
    .line 49
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 50
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.chromecastservice"

    const-string v2, "com.sec.android.chromecastservice.ServiceManager"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    sget-object v1, Lcom/sec/android/sdial/ServiceProvider;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/sdial/ServiceProvider;->mChromecastServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 53
    return-void
.end method

.method public static startService(Landroid/content/Context;Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;)V
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "l"    # Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    .prologue
    .line 33
    sput-object p0, Lcom/sec/android/sdial/ServiceProvider;->mContext:Landroid/content/Context;

    .line 34
    sput-object p1, Lcom/sec/android/sdial/ServiceProvider;->mIServiceConnectionEventlistener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    .line 35
    new-instance v0, Lcom/sec/android/sdial/ServiceProvider$ChromecastServiceConnection;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/sdial/ServiceProvider$ChromecastServiceConnection;-><init>(Lcom/sec/android/sdial/ServiceProvider$ChromecastServiceConnection;)V

    sput-object v0, Lcom/sec/android/sdial/ServiceProvider;->mChromecastServiceConnection:Landroid/content/ServiceConnection;

    .line 37
    new-instance v0, Lcom/sec/android/sdial/DeviceFinderImpl;

    sget-object v1, Lcom/sec/android/sdial/ServiceProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/sdial/DeviceFinderImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/sdial/ServiceProvider;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    .line 38
    new-instance v0, Lcom/sec/android/sdial/CCAVPlayerImpl;

    sget-object v1, Lcom/sec/android/sdial/ServiceProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/sdial/CCAVPlayerImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/sdial/ServiceProvider;->mCCAVPlayer:Lcom/sec/android/sdial/CAVPlayer;

    .line 39
    new-instance v0, Lcom/sec/android/sdial/ServiceErrorHandler;

    invoke-direct {v0}, Lcom/sec/android/sdial/ServiceErrorHandler;-><init>()V

    sput-object v0, Lcom/sec/android/sdial/ServiceProvider;->mServiceErrorHandler:Lcom/sec/android/sdial/ServiceErrorHandler;

    .line 41
    invoke-static {}, Lcom/sec/android/sdial/ServiceProvider;->startChromecastService()V

    .line 42
    return-void
.end method

.method private static stopChromecastService()V
    .locals 2

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/sdial/ServiceProvider;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/sdial/ServiceProvider;->mChromecastServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 57
    return-void
.end method

.method public static stopService()V
    .locals 0

    .prologue
    .line 45
    invoke-static {}, Lcom/sec/android/sdial/ServiceProvider;->stopChromecastService()V

    .line 46
    return-void
.end method


# virtual methods
.method public setIServiceConnectionEventlistener(Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    .prologue
    .line 106
    sput-object p1, Lcom/sec/android/sdial/ServiceProvider;->mIServiceConnectionEventlistener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    .line 107
    return-void
.end method
