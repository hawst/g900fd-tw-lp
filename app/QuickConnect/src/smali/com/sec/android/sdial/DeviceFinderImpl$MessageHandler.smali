.class Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;
.super Landroid/os/Handler;
.source "DeviceFinderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sdial/DeviceFinderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MessageHandler"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ServiceMessageHandler"


# instance fields
.field final synthetic this$0:Lcom/sec/android/sdial/DeviceFinderImpl;


# direct methods
.method private constructor <init>(Lcom/sec/android/sdial/DeviceFinderImpl;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/DeviceFinderImpl;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/sdial/DeviceFinderImpl;Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;-><init>(Lcom/sec/android/sdial/DeviceFinderImpl;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 75
    iget v5, p1, Landroid/os/Message;->what:I

    .line 77
    .local v5, "what":I
    const-string v6, "ServiceMessageHandler"

    const-string v7, "Message received in ServiceMessageHandler"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 81
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "function"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "function":Ljava/lang/String;
    const-string v6, "onRouteAdded"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 84
    const-string v6, "device_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 85
    .local v3, "id":Ljava/lang/String;
    const-string v6, "device_name"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "name":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/sdial/DeviceImpl;

    invoke-direct {v1}, Lcom/sec/android/sdial/DeviceImpl;-><init>()V

    .line 88
    .local v1, "device":Lcom/sec/android/sdial/Device;
    invoke-virtual {v1, v3}, Lcom/sec/android/sdial/Device;->setId(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v1, v4}, Lcom/sec/android/sdial/Device;->setName(Ljava/lang/String;)V

    .line 90
    iget-object v6, p0, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/DeviceFinderImpl;

    # getter for: Lcom/sec/android/sdial/DeviceFinderImpl;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/sdial/DeviceFinderImpl;->access$0(Lcom/sec/android/sdial/DeviceFinderImpl;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v6, p0, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/DeviceFinderImpl;

    # getter for: Lcom/sec/android/sdial/DeviceFinderImpl;->mListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v6}, Lcom/sec/android/sdial/DeviceFinderImpl;->access$1(Lcom/sec/android/sdial/DeviceFinderImpl;)Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 93
    iget-object v6, p0, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/DeviceFinderImpl;

    # getter for: Lcom/sec/android/sdial/DeviceFinderImpl;->mListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v6}, Lcom/sec/android/sdial/DeviceFinderImpl;->access$1(Lcom/sec/android/sdial/DeviceFinderImpl;)Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v6

    invoke-interface {v6, v1}, Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;->onDeviceAdded(Lcom/sec/android/sdial/Device;)V

    .line 99
    :goto_0
    const-string v6, "ServiceMessageHandler"

    const-string v7, "Response: Route Added "

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    .end local v1    # "device":Lcom/sec/android/sdial/Device;
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "name":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 96
    .restart local v1    # "device":Lcom/sec/android/sdial/Device;
    .restart local v3    # "id":Ljava/lang/String;
    .restart local v4    # "name":Ljava/lang/String;
    :cond_1
    const-string v6, "ServiceMessageHandler"

    const-string v7, "IDeviceFinderEventListener is null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 100
    .end local v1    # "device":Lcom/sec/android/sdial/Device;
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "name":Ljava/lang/String;
    :cond_2
    const-string v6, "onRouteRemoved"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 101
    const-string v6, "device_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 102
    .restart local v3    # "id":Ljava/lang/String;
    const-string v6, "device_name"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 103
    .restart local v4    # "name":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/sdial/DeviceImpl;

    invoke-direct {v1}, Lcom/sec/android/sdial/DeviceImpl;-><init>()V

    .line 104
    .restart local v1    # "device":Lcom/sec/android/sdial/Device;
    invoke-virtual {v1, v3}, Lcom/sec/android/sdial/Device;->setId(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v1, v4}, Lcom/sec/android/sdial/Device;->setName(Ljava/lang/String;)V

    .line 107
    iget-object v6, p0, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/DeviceFinderImpl;

    # getter for: Lcom/sec/android/sdial/DeviceFinderImpl;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/sdial/DeviceFinderImpl;->access$0(Lcom/sec/android/sdial/DeviceFinderImpl;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 109
    iget-object v6, p0, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/DeviceFinderImpl;

    # getter for: Lcom/sec/android/sdial/DeviceFinderImpl;->mListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v6}, Lcom/sec/android/sdial/DeviceFinderImpl;->access$1(Lcom/sec/android/sdial/DeviceFinderImpl;)Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 110
    iget-object v6, p0, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;->this$0:Lcom/sec/android/sdial/DeviceFinderImpl;

    # getter for: Lcom/sec/android/sdial/DeviceFinderImpl;->mListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v6}, Lcom/sec/android/sdial/DeviceFinderImpl;->access$1(Lcom/sec/android/sdial/DeviceFinderImpl;)Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v6

    invoke-interface {v6, v1}, Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;->onDeviceRemoved(Lcom/sec/android/sdial/Device;)V

    .line 116
    :goto_2
    const-string v6, "ServiceMessageHandler"

    const-string v7, "Request: Route Removed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 113
    :cond_3
    const-string v6, "ServiceMessageHandler"

    const-string v7, "IDeviceFinderEventListener is null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
