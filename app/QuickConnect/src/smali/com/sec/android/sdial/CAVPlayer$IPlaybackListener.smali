.class public interface abstract Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
.super Ljava/lang/Object;
.source "CAVPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sdial/CAVPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IPlaybackListener"
.end annotation


# virtual methods
.method public abstract onAppConnected()V
.end method

.method public abstract onApplicationDisconnected(I)V
.end method

.method public abstract onConnectionSuspended(I)V
.end method

.method public abstract onConnectivityRecovered()V
.end method

.method public abstract onDisconnected()V
.end method

.method public abstract onProgressUpdate(D)V
.end method

.method public abstract onRemoteMediaPlayerMetadataUpdated()V
.end method

.method public abstract onRemoteMediaPlayerStatusUpdated(I)V
.end method
