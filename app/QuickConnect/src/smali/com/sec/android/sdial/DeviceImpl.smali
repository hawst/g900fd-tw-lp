.class Lcom/sec/android/sdial/DeviceImpl;
.super Lcom/sec/android/sdial/Device;
.source "DeviceImpl.java"


# instance fields
.field private mDeviceId:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/sdial/Device;-><init>()V

    .line 12
    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/sdial/DeviceImpl;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/sdial/DeviceImpl;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/sdial/DeviceImpl;->mDeviceId:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/sdial/DeviceImpl;->mDeviceName:Ljava/lang/String;

    .line 38
    return-void
.end method
