.class public Lcom/sec/android/sdial/httpserver/nano/HttpServer;
.super Ljava/lang/Object;
.source "HttpServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sdial/httpserver/nano/HttpServer$HTTPSession;,
        Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    }
.end annotation


# static fields
.field public static final HTTP_BADREQUEST:Ljava/lang/String; = "400 Bad Request"

.field public static final HTTP_FORBIDDEN:Ljava/lang/String; = "403 Forbidden"

.field public static final HTTP_INTERNALERROR:Ljava/lang/String; = "500 Internal Server Error"

.field public static final HTTP_NOTFOUND:Ljava/lang/String; = "404 Not Found"

.field public static final HTTP_NOTIMPLEMENTED:Ljava/lang/String; = "501 Not Implemented"

.field public static final HTTP_OK:Ljava/lang/String; = "200 OK"

.field public static final HTTP_PARTIALCONTENT:Ljava/lang/String; = "206 Partial Content"

.field public static final HTTP_RANGE_NOT_SATISFIABLE:Ljava/lang/String; = "416 Requested Range Not Satisfiable"

.field public static final HTTP_REDIRECT:Ljava/lang/String; = "301 Moved Permanently"

.field private static final LICENCE:Ljava/lang/String; = "Copyright (C) 2001,2005-2011 by Jarno Elonen <elonen@iki.fi>\nand Copyright (C) 2010 by Konstantinos Togias <info@ktogias.gr>\n\nRedistribution and use in source and binary forms, with or without\nmodification, are permitted provided that the following conditions\nare met:\n\nRedistributions of source code must retain the above copyright notice,\nthis list of conditions and the following disclaimer. Redistributions in\nbinary form must reproduce the above copyright notice, this list of\nconditions and the following disclaimer in the documentation and/or other\nmaterials provided with the distribution. The name of the author may not\nbe used to endorse or promote products derived from this software without\nspecific prior written permission. \n \nTHIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS\'\' AND ANY EXPRESS OR\nIMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES\nOF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.\nIN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,\nINCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT\nNOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,\nDATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY\nTHEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE\nOF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."

.field public static final MIME_DEFAULT_BINARY:Ljava/lang/String; = "application/octet-stream"

.field public static final MIME_HTML:Ljava/lang/String; = "text/html"

.field public static final MIME_PLAINTEXT:Ljava/lang/String; = "text/plain"

.field public static final MIME_XML:Ljava/lang/String; = "text/xml"

.field private static gmtFrmt:Ljava/text/SimpleDateFormat;

.field protected static theMimeTypes:Ljava/util/Hashtable;


# instance fields
.field private myRootDir:Ljava/io/File;

.field private final myServerSocket:Ljava/net/ServerSocket;

.field private myTcpPort:I

.field private myThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 932
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    sput-object v1, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->theMimeTypes:Ljava/util/Hashtable;

    .line 934
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, "css        text/css js         text/javascript jet         text/javascript htm        text/html html       text/html txt        text/plain asc        text/plain gif        image/gif jpg        image/jpeg jpeg       image/jpeg png        image/png mp3        audio/mpeg m3u        audio/mpeg-url avi        video/x-msvideo m4u\t\tvideo/vnd.mpegurl m4v\t\tvideo/x-m4v mov\t\tvideo/quicktime mp4\t\tvideo/mp4 mpe\t\tvideo/mpeg mpeg\t\tvideo/mpeg mpg\t\tvideo/mpeg mxu\t\tvideo/vnd.mpegurl qt\t\t\tvideo/quicktime flv\t\tvideo/x-flv m3u8\t\tapplication/x-mpegURL ts\t\t\tvideo/MP2T 3gp\t\tvideo/3gpp wmv\t\tvideo/x-ms-wmv pdf        application/pdf doc        application/msword ogg        application/x-ogg zip        application/octet-stream exe        application/octet-stream class      application/octet-stream "

    invoke-direct {v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 957
    .local v0, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-nez v1, :cond_0

    .line 966
    new-instance v1, Ljava/text/SimpleDateFormat;

    .line 967
    const-string v2, "E, d MMM yyyy HH:mm:ss \'GMT\'"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 966
    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v1, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->gmtFrmt:Ljava/text/SimpleDateFormat;

    .line 968
    sget-object v1, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->gmtFrmt:Ljava/text/SimpleDateFormat;

    const-string v2, "GMT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 998
    return-void

    .line 958
    :cond_0
    sget-object v1, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->theMimeTypes:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "port"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    iput p1, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myTcpPort:I

    .line 184
    new-instance v0, Ljava/io/File;

    const-string v1, "/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myRootDir:Ljava/io/File;

    .line 185
    new-instance v0, Ljava/net/ServerSocket;

    iget v1, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myTcpPort:I

    invoke-direct {v0, v1}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myServerSocket:Ljava/net/ServerSocket;

    .line 186
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/sdial/httpserver/nano/HttpServer$1;

    invoke-direct {v1, p0}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$1;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myThread:Ljava/lang/Thread;

    .line 195
    iget-object v0, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myThread:Ljava/lang/Thread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 197
    return-void
.end method

.method static synthetic access$0()Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 964
    sget-object v0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->gmtFrmt:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/sdial/httpserver/nano/HttpServer;)Ljava/net/ServerSocket;
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myServerSocket:Ljava/net/ServerSocket;

    return-object v0
.end method


# virtual methods
.method protected encodeUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 722
    const-string v0, ""

    .line 723
    .local v0, "newUri":Ljava/lang/String;
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v3, "/ "

    const/4 v4, 0x1

    invoke-direct {v1, p1, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 724
    .local v1, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-nez v3, :cond_0

    .line 737
    return-object v0

    .line 725
    :cond_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 726
    .local v2, "tok":Ljava/lang/String;
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 727
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 728
    :cond_1
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 729
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "%20"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 731
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public serve(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;Ljava/util/Properties;Ljava/util/Properties;)Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .locals 5
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "header"    # Ljava/util/Properties;
    .param p4, "parms"    # Ljava/util/Properties;
    .param p5, "files"    # Ljava/util/Properties;

    .prologue
    .line 69
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p3}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v0

    .line 72
    .local v0, "e":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    invoke-virtual {p4}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v0

    .line 78
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-nez v2, :cond_1

    .line 83
    invoke-virtual {p5}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v0

    .line 84
    :goto_2
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-nez v2, :cond_2

    .line 89
    new-instance v2, Ljava/io/File;

    const-string v3, "."

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {p0, p1, p3, v2, v3}, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->serveFile(Ljava/lang/String;Ljava/util/Properties;Ljava/io/File;Z)Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    move-result-object v2

    return-object v2

    .line 73
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 74
    .local v1, "value":Ljava/lang/String;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  HDR: \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 75
    invoke-virtual {p3, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 74
    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 79
    .end local v1    # "value":Ljava/lang/String;
    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 80
    .restart local v1    # "value":Ljava/lang/String;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  PRM: \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 81
    invoke-virtual {p4, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 80
    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 85
    .end local v1    # "value":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 86
    .restart local v1    # "value":Ljava/lang/String;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  UPLOADED: \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 87
    invoke-virtual {p5, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public serveFile(Ljava/lang/String;Ljava/util/Properties;Ljava/io/File;Z)Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .locals 40
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "header"    # Ljava/util/Properties;
    .param p3, "homeDir"    # Ljava/io/File;
    .param p4, "allowDirectoryListing"    # Z

    .prologue
    .line 755
    const/16 v28, 0x0

    .line 758
    .local v28, "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->isDirectory()Z

    move-result v34

    if-nez v34, :cond_0

    .line 759
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    const-string v34, "500 Internal Server Error"

    const-string v35, "text/plain"

    .line 760
    const-string v36, "INTERNAL ERRROR: serveFile(): given homeDir is not a directory."

    .line 759
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move-object/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :cond_0
    if-nez v28, :cond_3

    .line 764
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v34

    sget-char v35, Ljava/io/File;->separatorChar:C

    const/16 v36, 0x2f

    invoke-virtual/range {v34 .. v36}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 765
    const/16 v34, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v34

    if-ltz v34, :cond_1

    .line 766
    const/16 v34, 0x0

    const/16 v35, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v35

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 769
    :cond_1
    const-string v34, ".."

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v34

    if-nez v34, :cond_2

    const-string v34, ".."

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v34

    if-nez v34, :cond_2

    .line 770
    const-string v34, "../"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v34

    if-ltz v34, :cond_3

    .line 771
    :cond_2
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    const-string v34, "403 Forbidden"

    const-string v35, "text/plain"

    .line 772
    const-string v36, "FORBIDDEN: Won\'t serve ../ for security reasons."

    .line 771
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move-object/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :cond_3
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-direct {v11, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 776
    .local v11, "f":Ljava/io/File;
    if-nez v28, :cond_4

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v34

    if-nez v34, :cond_4

    .line 777
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    const-string v34, "404 Not Found"

    const-string v35, "text/plain"

    .line 778
    const-string v36, "Error 404, file not found."

    .line 777
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move-object/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :cond_4
    if-nez v28, :cond_19

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v34

    if-eqz v34, :cond_19

    .line 784
    const-string v34, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v34

    if-nez v34, :cond_5

    .line 785
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "/"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 786
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    const-string v34, "301 Moved Permanently"

    const-string v35, "text/html"

    .line 787
    new-instance v36, Ljava/lang/StringBuilder;

    const-string v37, "<html><body>Redirected: <a href=\""

    invoke-direct/range {v36 .. v37}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "\">"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    .line 788
    move-object/from16 v0, v36

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "</a></body></html>"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    .line 787
    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 786
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move-object/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    const-string v34, "Location"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    :cond_5
    if-nez v28, :cond_19

    .line 794
    new-instance v34, Ljava/io/File;

    const-string v35, "index.html"

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-direct {v0, v11, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->exists()Z

    move-result v34

    if-eqz v34, :cond_9

    .line 795
    new-instance v11, Ljava/io/File;

    .end local v11    # "f":Ljava/io/File;
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "/index.html"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p3

    move-object/from16 v1, v34

    invoke-direct {v11, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v11    # "f":Ljava/io/File;
    move-object/from16 v29, v28

    .line 855
    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .local v29, "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :goto_0
    if-nez v29, :cond_18

    .line 857
    const/16 v22, 0x0

    .line 858
    .local v22, "mime":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v11}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v34

    const/16 v35, 0x2e

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v10

    .line 859
    .local v10, "dot":I
    if-ltz v10, :cond_6

    .line 860
    sget-object v34, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->theMimeTypes:Ljava/util/Hashtable;

    invoke-virtual {v11}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v35

    .line 861
    add-int/lit8 v36, v10, 0x1

    invoke-virtual/range {v35 .. v36}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v35

    .line 860
    invoke-virtual/range {v34 .. v35}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    move-object/from16 v0, v34

    check-cast v0, Ljava/lang/String;

    move-object/from16 v22, v0

    .line 862
    :cond_6
    if-nez v22, :cond_7

    .line 863
    const-string v22, "application/octet-stream"

    .line 866
    :cond_7
    const-wide/16 v32, 0x0

    .line 867
    .local v32, "startFrom":J
    const-wide/16 v12, -0x1

    .line 868
    .local v12, "endAt":J
    const-string v34, "range"

    move-object/from16 v0, p2

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 869
    .local v25, "range":Ljava/lang/String;
    if-eqz v25, :cond_8

    .line 870
    const-string v34, "bytes="

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_8

    .line 871
    const-string v34, "bytes="

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->length()I

    move-result v34

    move-object/from16 v0, v25

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 872
    const/16 v34, 0x2d

    move-object/from16 v0, v25

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v23

    .line 874
    .local v23, "minus":I
    if-lez v23, :cond_8

    .line 875
    const/16 v34, 0x0

    :try_start_1
    move-object/from16 v0, v25

    move/from16 v1, v34

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v32

    .line 878
    add-int/lit8 v34, v23, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v34

    .line 877
    invoke-static/range {v34 .. v34}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v12

    .line 887
    .end local v23    # "minus":I
    :cond_8
    :goto_1
    :try_start_2
    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v14

    .line 888
    .local v14, "fileLen":J
    if-eqz v25, :cond_17

    const-wide/16 v34, 0x0

    cmp-long v34, v32, v34

    if-ltz v34, :cond_17

    .line 889
    cmp-long v34, v32, v14

    if-ltz v34, :cond_14

    .line 890
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    const-string v34, "416 Requested Range Not Satisfiable"

    .line 891
    const-string v35, "text/plain"

    const-string v36, ""

    .line 890
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move-object/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 892
    .end local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :try_start_3
    const-string v34, "Content-Range"

    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "bytes 0-0/"

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 923
    .end local v10    # "dot":I
    .end local v12    # "endAt":J
    .end local v14    # "fileLen":J
    .end local v22    # "mime":Ljava/lang/String;
    .end local v25    # "range":Ljava/lang/String;
    .end local v32    # "startFrom":J
    :goto_2
    const-string v34, "Accept-Ranges"

    const-string v35, "bytes"

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    return-object v28

    .line 796
    :cond_9
    new-instance v34, Ljava/io/File;

    const-string v35, "index.htm"

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-direct {v0, v11, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->exists()Z

    move-result v34

    if-eqz v34, :cond_a

    .line 797
    new-instance v11, Ljava/io/File;

    .end local v11    # "f":Ljava/io/File;
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "/index.htm"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p3

    move-object/from16 v1, v34

    invoke-direct {v11, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v11    # "f":Ljava/io/File;
    move-object/from16 v29, v28

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    goto/16 :goto_0

    .line 799
    .end local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :cond_a
    if-eqz p4, :cond_13

    invoke-virtual {v11}, Ljava/io/File;->canRead()Z

    move-result v34

    if-eqz v34, :cond_13

    .line 800
    invoke-virtual {v11}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v16

    .line 801
    .local v16, "files":[Ljava/lang/String;
    new-instance v34, Ljava/lang/StringBuilder;

    const-string v35, "<html><body><h1>Directory "

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 802
    const-string v35, "</h1><br/>"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 801
    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 804
    .local v24, "msg":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-le v0, v1, :cond_b

    .line 805
    const/16 v34, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v35

    add-int/lit8 v35, v35, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v31

    .line 806
    .local v31, "u":Ljava/lang/String;
    const/16 v34, 0x2f

    move-object/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v30

    .line 807
    .local v30, "slash":I
    if-ltz v30, :cond_b

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v34

    move/from16 v0, v30

    move/from16 v1, v34

    if-ge v0, v1, :cond_b

    .line 808
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "<b><a href=\""

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 809
    const/16 v35, 0x0

    add-int/lit8 v36, v30, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 810
    const-string v35, "\">..</a></b><br/>"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 808
    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 813
    .end local v30    # "slash":I
    .end local v31    # "u":Ljava/lang/String;
    :cond_b
    if-eqz v16, :cond_c

    .line 814
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_3
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v34, v0

    move/from16 v0, v18

    move/from16 v1, v34

    if-lt v0, v1, :cond_d

    .line 845
    .end local v18    # "i":I
    :cond_c
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "</body></html>"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 846
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    const-string v34, "200 OK"

    const-string v35, "text/html"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move-object/from16 v4, v24

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    move-object/from16 v29, v28

    .line 847
    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    goto/16 :goto_0

    .line 815
    .end local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v18    # "i":I
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :cond_d
    new-instance v6, Ljava/io/File;

    aget-object v34, v16, v18

    move-object/from16 v0, v34

    invoke-direct {v6, v11, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 816
    .local v6, "curFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v7

    .line 817
    .local v7, "dir":Z
    if-eqz v7, :cond_e

    .line 818
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "<b>"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 819
    aget-object v34, v16, v18

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-static/range {v34 .. v34}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v34, "/"

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    aput-object v34, v16, v18

    .line 822
    :cond_e
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "<a href=\""

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v36, v16, v18

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->encodeUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 823
    const-string v35, "\">"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    aget-object v35, v16, v18

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, "</a>"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 822
    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 826
    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v34

    if-eqz v34, :cond_f

    .line 827
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v20

    .line 828
    .local v20, "len":J
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, " &nbsp;<font size=2>("

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 829
    const-wide/16 v34, 0x400

    cmp-long v34, v20, v34

    if-gez v34, :cond_11

    .line 830
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " bytes"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 838
    :goto_4
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, ")</font>"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 840
    .end local v20    # "len":J
    :cond_f
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "<br/>"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 841
    if-eqz v7, :cond_10

    .line 842
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, "</b>"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 814
    :cond_10
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_3

    .line 831
    .restart local v20    # "len":J
    :cond_11
    const-wide/32 v34, 0x100000

    cmp-long v34, v20, v34

    if-gez v34, :cond_12

    .line 832
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v36, 0x400

    div-long v36, v20, v36

    move-object/from16 v0, v34

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, "."

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 833
    const-wide/16 v36, 0x400

    rem-long v36, v20, v36

    const-wide/16 v38, 0xa

    div-long v36, v36, v38

    const-wide/16 v38, 0x64

    rem-long v36, v36, v38

    move-object/from16 v0, v34

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " KB"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 832
    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 833
    goto :goto_4

    .line 835
    :cond_12
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/32 v36, 0x100000

    div-long v36, v20, v36

    move-object/from16 v0, v34

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, "."

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 836
    const-wide/32 v36, 0x100000

    rem-long v36, v20, v36

    const-wide/16 v38, 0xa

    div-long v36, v36, v38

    const-wide/16 v38, 0x64

    rem-long v36, v36, v38

    move-object/from16 v0, v34

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " MB"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 835
    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_4

    .line 848
    .end local v6    # "curFile":Ljava/io/File;
    .end local v7    # "dir":Z
    .end local v16    # "files":[Ljava/lang/String;
    .end local v18    # "i":I
    .end local v20    # "len":J
    .end local v24    # "msg":Ljava/lang/String;
    :cond_13
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    const-string v34, "403 Forbidden"

    const-string v35, "text/plain"

    .line 849
    const-string v36, "FORBIDDEN: No directory listing."

    .line 848
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move-object/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    move-object/from16 v29, v28

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    goto/16 :goto_0

    .line 894
    .restart local v10    # "dot":I
    .restart local v12    # "endAt":J
    .restart local v14    # "fileLen":J
    .restart local v22    # "mime":Ljava/lang/String;
    .restart local v25    # "range":Ljava/lang/String;
    .restart local v32    # "startFrom":J
    :cond_14
    const-wide/16 v34, 0x0

    cmp-long v34, v12, v34

    if-gez v34, :cond_15

    .line 895
    const-wide/16 v34, 0x1

    sub-long v12, v14, v34

    .line 896
    :cond_15
    sub-long v34, v12, v32

    const-wide/16 v36, 0x1

    add-long v26, v34, v36

    .line 897
    .local v26, "newLen":J
    const-wide/16 v34, 0x0

    cmp-long v34, v26, v34

    if-gez v34, :cond_16

    .line 898
    const-wide/16 v26, 0x0

    .line 900
    :cond_16
    move-wide/from16 v8, v26

    .line 901
    .local v8, "dataLen":J
    :try_start_4
    new-instance v17, Lcom/sec/android/sdial/httpserver/nano/HttpServer$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11, v8, v9}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$2;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/io/File;J)V

    .line 906
    .local v17, "fis":Ljava/io/FileInputStream;
    move-object/from16 v0, v17

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/io/FileInputStream;->skip(J)J

    .line 908
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    const-string v34, "206 Partial Content"

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v22

    move-object/from16 v4, v17

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 909
    .end local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :try_start_5
    const-string v34, "Content-Length"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    const-string v34, "Content-Range"

    new-instance v35, Ljava/lang/StringBuilder;

    const-string v36, "bytes "

    invoke-direct/range {v35 .. v36}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v35

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    .line 911
    const-string v36, "-"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "/"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 910
    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_2

    .line 918
    .end local v8    # "dataLen":J
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .end local v26    # "newLen":J
    :catch_0
    move-exception v19

    .line 919
    .end local v10    # "dot":I
    .end local v12    # "endAt":J
    .end local v14    # "fileLen":J
    .end local v25    # "range":Ljava/lang/String;
    .end local v32    # "startFrom":J
    .local v19, "ioe":Ljava/io/IOException;
    :goto_5
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    const-string v34, "403 Forbidden"

    const-string v35, "text/plain"

    .line 920
    const-string v36, "FORBIDDEN: Reading file failed."

    .line 919
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move-object/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    goto/16 :goto_2

    .line 914
    .end local v19    # "ioe":Ljava/io/IOException;
    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v10    # "dot":I
    .restart local v12    # "endAt":J
    .restart local v14    # "fileLen":J
    .restart local v25    # "range":Ljava/lang/String;
    .restart local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v32    # "startFrom":J
    :cond_17
    :try_start_6
    new-instance v28, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;

    const-string v34, "200 OK"

    new-instance v35, Ljava/io/FileInputStream;

    move-object/from16 v0, v35

    invoke-direct {v0, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    move-object/from16 v3, v22

    move-object/from16 v4, v35

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;-><init>(Lcom/sec/android/sdial/httpserver/nano/HttpServer;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 915
    .end local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :try_start_7
    const-string v34, "Content-Length"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_2

    .line 918
    .end local v10    # "dot":I
    .end local v12    # "endAt":J
    .end local v14    # "fileLen":J
    .end local v25    # "range":Ljava/lang/String;
    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .end local v32    # "startFrom":J
    .restart local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    :catch_1
    move-exception v19

    move-object/from16 v28, v29

    .end local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    goto :goto_5

    .line 880
    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v10    # "dot":I
    .restart local v12    # "endAt":J
    .restart local v23    # "minus":I
    .restart local v25    # "range":Ljava/lang/String;
    .restart local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v32    # "startFrom":J
    :catch_2
    move-exception v34

    goto/16 :goto_1

    .end local v10    # "dot":I
    .end local v12    # "endAt":J
    .end local v22    # "mime":Ljava/lang/String;
    .end local v23    # "minus":I
    .end local v25    # "range":Ljava/lang/String;
    .end local v32    # "startFrom":J
    :cond_18
    move-object/from16 v28, v29

    .end local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    goto/16 :goto_2

    :cond_19
    move-object/from16 v29, v28

    .end local v28    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    .restart local v29    # "res":Lcom/sec/android/sdial/httpserver/nano/HttpServer$Response;
    goto/16 :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->close()V

    .line 205
    iget-object v0, p0, Lcom/sec/android/sdial/httpserver/nano/HttpServer;->myThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :goto_0
    return-void

    .line 206
    :catch_0
    move-exception v0

    goto :goto_0
.end method
