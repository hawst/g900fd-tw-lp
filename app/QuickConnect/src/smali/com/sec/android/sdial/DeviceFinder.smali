.class public abstract Lcom/sec/android/sdial/DeviceFinder;
.super Ljava/lang/Object;
.source "DeviceFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method


# virtual methods
.method public abstract getDevices()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/sdial/Device;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setDeviceFinderEventListener(Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;)V
.end method

.method public abstract startDiscovery()V
.end method

.method public abstract stopDiscovery()V
.end method
