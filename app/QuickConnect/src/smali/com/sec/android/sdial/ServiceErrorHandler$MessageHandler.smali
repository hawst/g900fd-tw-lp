.class Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;
.super Landroid/os/Handler;
.source "ServiceErrorHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sdial/ServiceErrorHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MessageHandler"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ServiceMessageHandler"


# instance fields
.field final synthetic this$0:Lcom/sec/android/sdial/ServiceErrorHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/sdial/ServiceErrorHandler;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;->this$0:Lcom/sec/android/sdial/ServiceErrorHandler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/sdial/ServiceErrorHandler;Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;-><init>(Lcom/sec/android/sdial/ServiceErrorHandler;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 37
    iget v4, p1, Landroid/os/Message;->what:I

    .line 39
    .local v4, "what":I
    const-string v5, "ServiceMessageHandler"

    const-string v6, "Message received in ServiceMessageHandler"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 43
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "function"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 44
    .local v2, "function":Ljava/lang/String;
    const-string v5, "onFailed"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 46
    const-string v5, "errorMessage"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 47
    .local v1, "errorMessage":Ljava/lang/String;
    const-string v5, "statusCode"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 49
    .local v3, "statusCode":I
    const-string v5, "ServiceMessageHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error message received in ServiceErrorHandler_Test is "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v5, p0, Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;->this$0:Lcom/sec/android/sdial/ServiceErrorHandler;

    # getter for: Lcom/sec/android/sdial/ServiceErrorHandler;->mIErrorListener:Lcom/sec/android/sdial/IErrorListener;
    invoke-static {v5}, Lcom/sec/android/sdial/ServiceErrorHandler;->access$0(Lcom/sec/android/sdial/ServiceErrorHandler;)Lcom/sec/android/sdial/IErrorListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 51
    iget-object v5, p0, Lcom/sec/android/sdial/ServiceErrorHandler$MessageHandler;->this$0:Lcom/sec/android/sdial/ServiceErrorHandler;

    # getter for: Lcom/sec/android/sdial/ServiceErrorHandler;->mIErrorListener:Lcom/sec/android/sdial/IErrorListener;
    invoke-static {v5}, Lcom/sec/android/sdial/ServiceErrorHandler;->access$0(Lcom/sec/android/sdial/ServiceErrorHandler;)Lcom/sec/android/sdial/IErrorListener;

    move-result-object v5

    invoke-interface {v5, v1, v3}, Lcom/sec/android/sdial/IErrorListener;->onFailed(Ljava/lang/String;I)V

    .line 54
    .end local v1    # "errorMessage":Ljava/lang/String;
    .end local v3    # "statusCode":I
    :cond_0
    return-void
.end method
