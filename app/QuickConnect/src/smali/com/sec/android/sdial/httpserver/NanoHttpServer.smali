.class public Lcom/sec/android/sdial/httpserver/NanoHttpServer;
.super Ljava/lang/Object;
.source "NanoHttpServer.java"

# interfaces
.implements Lcom/sec/android/sdial/httpserver/IHttpService;


# static fields
.field private static final TAG:Ljava/lang/String; = "NanoHttpServer"


# instance fields
.field address:Ljava/lang/String;

.field private isServerOn:Z

.field private mContext:Landroid/content/Context;

.field private mEmbeddedServerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;",
            ">;"
        }
    .end annotation
.end field

.field private mPort:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->isServerOn:Z

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mEmbeddedServerList:Ljava/util/ArrayList;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->address:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public getUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 60
    const-string v2, ""

    .line 62
    .local v2, "hostedUrl":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 63
    const-string v8, "."

    invoke-virtual {p1, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 64
    .local v3, "index":I
    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "extension":Ljava/lang/String;
    move-object v4, p1

    .line 67
    .local v4, "mFileToHost":Ljava/lang/String;
    const/4 v7, 0x0

    .line 68
    .local v7, "started":Z
    const/4 v5, 0x0

    .local v5, "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    move-object v6, v5

    .line 69
    .end local v5    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    .local v6, "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    :cond_0
    :goto_0
    if-eqz v7, :cond_3

    .line 83
    if-eqz v6, :cond_1

    .line 84
    iget-object v8, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mEmbeddedServerList:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_1
    const-string v8, "NanoHttpServer"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Extension is "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "http://"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->address:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mPort:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/video"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 90
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 89
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    iget v8, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mPort:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mPort:I

    .line 92
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->isServerOn:Z

    .line 94
    .end local v1    # "extension":Ljava/lang/String;
    .end local v3    # "index":I
    .end local v4    # "mFileToHost":Ljava/lang/String;
    .end local v6    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    .end local v7    # "started":Z
    :cond_2
    return-object v2

    .line 70
    .restart local v1    # "extension":Ljava/lang/String;
    .restart local v3    # "index":I
    .restart local v4    # "mFileToHost":Ljava/lang/String;
    .restart local v6    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    .restart local v7    # "started":Z
    :cond_3
    if-eqz v4, :cond_0

    .line 72
    :try_start_0
    new-instance v5, Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;

    iget v8, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mPort:I

    invoke-direct {v5, v8}, Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;-><init>(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .end local v6    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    .restart local v5    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    :try_start_1
    iput-object v4, v5, Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;->CURRENT_FILE:Ljava/lang/String;

    .line 74
    const-string v8, "NanoHttpServer"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Started web server on port for video "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mPort:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 79
    :goto_1
    const/4 v7, 0x1

    move-object v6, v5

    .end local v5    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    .restart local v6    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    move-object v5, v6

    .line 77
    .end local v6    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    .local v0, "e":Ljava/io/IOException;
    .restart local v5    # "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 75
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public isStarted()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->isServerOn:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public start(I)V
    .locals 3
    .param p1, "port"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mPort:I

    .line 37
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mContext:Landroid/content/Context;

    .line 38
    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 37
    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 40
    .local v0, "wim":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v1

    .line 40
    invoke-static {v1}, Landroid/text/format/Formatter;->formatIpAddress(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->address:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    :goto_0
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mEmbeddedServerList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->isServerOn:Z

    .line 52
    const/16 v1, 0x1f96

    iput v1, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mPort:I

    .line 53
    return-void

    .line 47
    :cond_0
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mEmbeddedServerList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;

    .line 48
    .local v0, "server":Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;
    invoke-virtual {v0}, Lcom/sec/android/sdial/httpserver/nano/EmbeddedServer;->stop()V

    .line 49
    iget-object v1, p0, Lcom/sec/android/sdial/httpserver/NanoHttpServer;->mEmbeddedServerList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method
