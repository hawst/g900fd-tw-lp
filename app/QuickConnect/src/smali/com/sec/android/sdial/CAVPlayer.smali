.class public abstract Lcom/sec/android/sdial/CAVPlayer;
.super Ljava/lang/Object;
.source "CAVPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sdial/CAVPlayer$IErrorListener;,
        Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public abstract connectRoute(Ljava/lang/String;)V
.end method

.method public abstract disconnectRoute()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;,
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation
.end method

.method public abstract getDeviceName()Ljava/lang/String;
.end method

.method public abstract getIdleReason()I
.end method

.method public abstract getPlaybackStatus()I
.end method

.method public abstract initialize(Lcom/sec/android/sdial/CAVPlayer$IPlaybackListener;)V
.end method

.method public abstract loadMedia(Landroid/os/Bundle;ZI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation
.end method

.method public abstract pause()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sdial/exceptions/CastException;,
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation
.end method

.method public abstract play()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sdial/exceptions/CastException;,
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation
.end method

.method public abstract play(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/sdial/exceptions/TransientNetworkDisconnectionException;,
            Lcom/sec/android/sdial/exceptions/NoConnectionException;
        }
    .end annotation
.end method

.method public abstract volumeDown()V
.end method

.method public abstract volumeUp()V
.end method
