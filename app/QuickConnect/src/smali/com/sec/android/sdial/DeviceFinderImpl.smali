.class Lcom/sec/android/sdial/DeviceFinderImpl;
.super Lcom/sec/android/sdial/DeviceFinder;
.source "DeviceFinderImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DeviceFinderImpl"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/sdial/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

.field private mServiceMessageHandler:Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/sdial/DeviceFinder;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mContext:Landroid/content/Context;

    .line 24
    new-instance v0, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;-><init>(Lcom/sec/android/sdial/DeviceFinderImpl;Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;)V

    iput-object v0, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;

    .line 25
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/sdial/DeviceFinderImpl;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/sdial/DeviceFinderImpl;)Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method


# virtual methods
.method public getDevices()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/sdial/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setDeviceFinderEventListener(Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    .prologue
    .line 63
    const-string v0, "DeviceFinderImpl"

    const-string v1, "In setDeviceFinderEventListener()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iput-object p1, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    .line 65
    return-void
.end method

.method public startDiscovery()V
    .locals 4

    .prologue
    .line 29
    const-string v2, "DeviceFinderImpl"

    const-string v3, "In startDiscovery()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mDeviceList:Ljava/util/ArrayList;

    .line 32
    const/4 v2, 0x0

    .line 33
    const/4 v3, 0x0

    .line 32
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 35
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 36
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "refresh"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 39
    iget-object v2, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 40
    return-void
.end method

.method public stopDiscovery()V
    .locals 4

    .prologue
    .line 44
    const-string v2, "DeviceFinderImpl"

    const-string v3, "In stopDiscovery()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    const/4 v2, 0x0

    .line 47
    const/4 v3, 0x0

    .line 46
    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 49
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 50
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "function"

    const-string v3, "terminate"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 53
    iget-object v2, p0, Lcom/sec/android/sdial/DeviceFinderImpl;->mServiceMessageHandler:Lcom/sec/android/sdial/DeviceFinderImpl$MessageHandler;

    invoke-static {v2, v1}, Lcom/sec/android/sdial/ServiceProvider;->sendMessage(Landroid/os/Handler;Landroid/os/Message;)V

    .line 54
    return-void
.end method
