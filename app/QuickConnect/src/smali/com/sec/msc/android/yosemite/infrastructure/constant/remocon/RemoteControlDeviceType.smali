.class public final enum Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;
.super Ljava/lang/Enum;
.source "RemoteControlDeviceType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$msc$android$yosemite$infrastructure$constant$remocon$RemoteControlDeviceType:[I

.field public static final enum AVRECEIVER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum BD:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum MEDIAPLAYER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum STB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum TV:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum UNKNOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum WIFI_BD:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum WIFI_HTS:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum WIFI_STB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

.field public static final enum WIFI_TV:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$msc$android$yosemite$infrastructure$constant$remocon$RemoteControlDeviceType()[I
    .locals 3

    .prologue
    .line 4
    sget-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->$SWITCH_TABLE$com$sec$msc$android$yosemite$infrastructure$constant$remocon$RemoteControlDeviceType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->values()[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->AVRECEIVER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->BD:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->MEDIAPLAYER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->STB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->TV:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->UNKNOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_BD:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_HTS:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_STB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_TV:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v1}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_a
    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->$SWITCH_TABLE$com$sec$msc$android$yosemite$infrastructure$constant$remocon$RemoteControlDeviceType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_a

    :catch_1
    move-exception v1

    goto :goto_9

    :catch_2
    move-exception v1

    goto :goto_8

    :catch_3
    move-exception v1

    goto :goto_7

    :catch_4
    move-exception v1

    goto :goto_6

    :catch_5
    move-exception v1

    goto :goto_5

    :catch_6
    move-exception v1

    goto :goto_4

    :catch_7
    move-exception v1

    goto :goto_3

    :catch_8
    move-exception v1

    goto :goto_2

    :catch_9
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "TV"

    invoke-direct {v0, v1, v3}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->TV:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 7
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "STB"

    invoke-direct {v0, v1, v4}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->STB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 8
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "BD"

    invoke-direct {v0, v1, v5}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->BD:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 9
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "MEDIAPLAYER"

    invoke-direct {v0, v1, v6}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->MEDIAPLAYER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 10
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "AVRECEIVER"

    invoke-direct {v0, v1, v7}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->AVRECEIVER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 12
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "WIFI_TV"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_TV:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 13
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "WIFI_STB"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_STB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 14
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "WIFI_BD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_BD:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 15
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "WIFI_HTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_HTS:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 17
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->UNKNOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 4
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->TV:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->STB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->BD:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->MEDIAPLAYER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->AVRECEIVER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_TV:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_STB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_BD:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->WIFI_HTS:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->UNKNOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ENUM$VALUES:[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isWifiType(Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;)Z
    .locals 2
    .param p0, "type"    # Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .prologue
    .line 21
    invoke-static {}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->$SWITCH_TABLE$com$sec$msc$android$yosemite$infrastructure$constant$remocon$RemoteControlDeviceType()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 30
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 27
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 21
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    return-object v0
.end method

.method public static values()[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->ENUM$VALUES:[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
