.class public Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
.super Ljava/lang/Object;
.source "Room.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private headend:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

.field private region:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

.field private roomId:Ljava/lang/String;

.field private roomName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room$1;

    invoke-direct {v0}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room$1;-><init>()V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomId:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->region:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    .line 12
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->headend:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomId:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomName:Ljava/lang/String;

    .line 24
    const-class v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->region:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    .line 25
    const-class v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->headend:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "roomId"    # Ljava/lang/String;
    .param p2, "roomName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomId:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->region:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    .line 12
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->headend:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    .line 16
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomId:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomName:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public getHeadend()Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->headend:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    return-object v0
.end method

.method public getRegion()Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->region:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    return-object v0
.end method

.method public getRoomId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomId:Ljava/lang/String;

    return-object v0
.end method

.method public getRoomName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomName:Ljava/lang/String;

    return-object v0
.end method

.method public setHeadend(Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;)V
    .locals 0
    .param p1, "headend"    # Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->headend:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    .line 67
    return-void
.end method

.method public setRegion(Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;)V
    .locals 0
    .param p1, "region"    # Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->region:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    .line 57
    return-void
.end method

.method public setRoomId(Ljava/lang/String;)V
    .locals 0
    .param p1, "roomId"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomId:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setRoomName(Ljava/lang/String;)V
    .locals 0
    .param p1, "roomName"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomName:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Room : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->region:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->headend:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->roomName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->region:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 81
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->headend:Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 82
    return-void
.end method
