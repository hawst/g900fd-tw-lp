.class public final enum Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;
.super Ljava/lang/Enum;
.source "RemoconKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BACK:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum BLUE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum CHANNEL_DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum CHANNEL_UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum CLEAR:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum DASH:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum DISK_MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum ENTER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum EXIT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum FF:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum FOWARD_SKIP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum GREEN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum GUIDE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum HISTORY:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum HOME:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum INFO:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum LEFT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_0:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_1:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_2:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_3:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_4:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_5:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_6:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_7:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_8:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum NUMBER_9:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum OK:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum PAUSE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum PLAY:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum PLAY_PAUSE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum POPUP_MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum POWER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum PRECH:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum REC:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum RED:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum REW:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum REWIND_SKIP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum RIGHT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum SMARTHUB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum SOURCE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum STOP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum TOOLS:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum VOLUME_DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum VOLUME_MUTE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum VOLUME_UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

.field public static final enum YELLOW:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "POWER"

    invoke-direct {v0, v1, v3}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->POWER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 6
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "SOURCE"

    invoke-direct {v0, v1, v4}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->SOURCE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 7
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "VOLUME_UP"

    invoke-direct {v0, v1, v5}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 8
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "VOLUME_DOWN"

    invoke-direct {v0, v1, v6}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 9
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "VOLUME_MUTE"

    invoke-direct {v0, v1, v7}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_MUTE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 10
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "CHANNEL_UP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->CHANNEL_UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 11
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "CHANNEL_DOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->CHANNEL_DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 12
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_0"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_0:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 13
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_1"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_1:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 14
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_2"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_2:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 15
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_3"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_3:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 16
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_4"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_4:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 17
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_5"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_5:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 18
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_6"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_6:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 19
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_7"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_7:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 20
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_8"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_8:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 21
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "NUMBER_9"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_9:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 22
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "DASH"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->DASH:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 23
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "ENTER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->ENTER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 25
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "MENU"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 26
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "BACK"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->BACK:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 27
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "INFO"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->INFO:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 28
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "EXIT"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->EXIT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 29
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "CLEAR"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->CLEAR:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 30
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "HOME"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->HOME:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 31
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "POPUP_MENU"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->POPUP_MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 32
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "DISK_MENU"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->DISK_MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 34
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "UP"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 35
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "DOWN"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 36
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "LEFT"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->LEFT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 37
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "RIGHT"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->RIGHT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 38
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "OK"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->OK:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 40
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "PLAY"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->PLAY:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 41
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "PAUSE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->PAUSE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 42
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "PLAY_PAUSE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->PLAY_PAUSE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 43
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "REC"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->REC:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 44
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "STOP"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->STOP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 45
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "REW"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->REW:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 46
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "FF"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->FF:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 47
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "REWIND_SKIP"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->REWIND_SKIP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 48
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "FOWARD_SKIP"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->FOWARD_SKIP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 50
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "GUIDE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->GUIDE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 51
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "SMARTHUB"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->SMARTHUB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 52
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "TOOLS"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->TOOLS:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 54
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "RED"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->RED:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 55
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "GREEN"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->GREEN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 56
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "YELLOW"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->YELLOW:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 57
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "BLUE"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->BLUE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 59
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "HISTORY"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->HISTORY:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 60
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    const-string v1, "PRECH"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->PRECH:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 3
    const/16 v0, 0x32

    new-array v0, v0, [Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->POWER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->SOURCE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_MUTE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->CHANNEL_UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->CHANNEL_DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_0:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_1:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_2:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_3:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_4:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_5:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_6:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_7:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_8:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_9:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->DASH:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->ENTER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->BACK:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->INFO:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->EXIT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->CLEAR:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->HOME:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->POPUP_MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->DISK_MENU:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->LEFT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->RIGHT:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->OK:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->PLAY:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->PAUSE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->PLAY_PAUSE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->REC:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->STOP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->REW:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->FF:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->REWIND_SKIP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->FOWARD_SKIP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->GUIDE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->SMARTHUB:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->TOOLS:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->RED:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->GREEN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->YELLOW:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->BLUE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->HISTORY:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->PRECH:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->ENUM$VALUES:[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    return-object v0
.end method

.method public static values()[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->ENUM$VALUES:[Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
