.class public abstract Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub;
.super Landroid/os/Binder;
.source "IBasicRemoconService.java"

# interfaces
.implements Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

.field static final TRANSACTION_getCurrentRoom:I = 0x5

.field static final TRANSACTION_getRemoteControlList:I = 0x3

.field static final TRANSACTION_getRoomList:I = 0x4

.field static final TRANSACTION_isRemoteControlSetuped:I = 0x2

.field static final TRANSACTION_sendRemocon:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 103
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 42
    :sswitch_0
    const-string v8, "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v8, "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_0

    move v3, v7

    .line 56
    .local v3, "_arg3":Z
    :cond_0
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub;->sendRemocon(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 57
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 62
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":Ljava/lang/String;
    .end local v3    # "_arg3":Z
    :sswitch_2
    const-string v8, "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 65
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub;->isRemoteControlSetuped(Ljava/lang/String;)Z

    move-result v4

    .line 66
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 67
    if-eqz v4, :cond_1

    move v3, v7

    :cond_1
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 72
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v4    # "_result":Z
    :sswitch_3
    const-string v8, "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 75
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub;->getRemoteControlList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 76
    .local v5, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 77
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 82
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;>;"
    :sswitch_4
    const-string v8, "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub;->getRoomList()Ljava/util/List;

    move-result-object v6

    .line 84
    .local v6, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 90
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;>;"
    :sswitch_5
    const-string v8, "com.sec.msc.android.yosemite.service.remocon.IBasicRemoconService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub;->getCurrentRoom()Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;

    move-result-object v4

    .line 92
    .local v4, "_result":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 93
    if-eqz v4, :cond_2

    .line 94
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    invoke-virtual {v4, p3, v7}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
