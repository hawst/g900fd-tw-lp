.class public Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
.super Ljava/lang/Object;
.source "RemoteControlSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private brandName:Ljava/lang/String;

.field private codesetName:Ljava/lang/String;

.field private deviceId:I

.field private deviceType:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting$1;

    invoke-direct {v0}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting$1;-><init>()V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 175
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "deviceTypeString":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->valueOf(Ljava/lang/String;)Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceType:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->brandName:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->codesetName:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceId:I

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;Ljava/lang/String;I)V
    .locals 0
    .param p1, "deviceType"    # Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;
    .param p2, "brandName"    # Ljava/lang/String;
    .param p3, "deviceId"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceType:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 51
    iput-object p2, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->brandName:Ljava/lang/String;

    .line 52
    iput p3, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceId:I

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceType"    # Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;
    .param p2, "brandName"    # Ljava/lang/String;
    .param p3, "codesetName"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceType:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 58
    iput-object p2, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->brandName:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->codesetName:Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public getBrandName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->brandName:Ljava/lang/String;

    return-object v0
.end method

.method public getCodesetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->codesetName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceId:I

    return v0
.end method

.method public getDeviceType()Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceType:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    return-object v0
.end method

.method public setBrandName(Ljava/lang/String;)V
    .locals 0
    .param p1, "brandName"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->brandName:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setCodesetName(Ljava/lang/String;)V
    .locals 0
    .param p1, "codesetName"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->codesetName:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setDeviceId(I)V
    .locals 0
    .param p1, "deviceId"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceId:I

    .line 144
    return-void
.end method

.method public setDeviceType(Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;)V
    .locals 0
    .param p1, "deviceType"    # Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceType:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    .line 84
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceType:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    invoke-virtual {v0}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->brandName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->codesetName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 158
    iget v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->deviceId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 159
    return-void
.end method
