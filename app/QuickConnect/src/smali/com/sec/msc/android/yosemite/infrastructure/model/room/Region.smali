.class public Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;
.super Ljava/lang/Object;
.source "Region.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cityName:Ljava/lang/String;

.field private stateName:Ljava/lang/String;

.field private zipCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region$1;

    invoke-direct {v0}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region$1;-><init>()V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->zipCode:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->stateName:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->cityName:Ljava/lang/String;

    .line 21
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->zipCode:Ljava/lang/String;

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->stateName:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->cityName:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "zipCode"    # Ljava/lang/String;
    .param p2, "stateName"    # Ljava/lang/String;
    .param p3, "cityName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->zipCode:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->stateName:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->cityName:Ljava/lang/String;

    .line 14
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->zipCode:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->stateName:Ljava/lang/String;

    .line 16
    iput-object p3, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->cityName:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public getCityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->cityName:Ljava/lang/String;

    return-object v0
.end method

.method public getStateName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->stateName:Ljava/lang/String;

    return-object v0
.end method

.method public getZipCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->zipCode:Ljava/lang/String;

    return-object v0
.end method

.method public setCityName(Ljava/lang/String;)V
    .locals 0
    .param p1, "cityName"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->cityName:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setStateName(Ljava/lang/String;)V
    .locals 0
    .param p1, "stateName"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->stateName:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setZipCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "zipCode"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->zipCode:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->zipCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->stateName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Region;->cityName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32
    return-void
.end method
