.class public Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;
.super Ljava/lang/Object;
.source "Headend.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;",
            ">;"
        }
    .end annotation
.end field

.field public static final TYPE_CATV:Ljava/lang/String; = "CATV"

.field public static final TYPE_DBS:Ljava/lang/String; = "DBS"

.field public static final TYPE_MA:Ljava/lang/String; = "MA"

.field public static final TYPE_MD:Ljava/lang/String; = "MD"

.field public static final TYPE_MH:Ljava/lang/String; = "MH"

.field public static final TYPE_MO:Ljava/lang/String; = "MO"

.field public static final TYPE_NC:Ljava/lang/String; = "NC"

.field public static final TYPE_OTA:Ljava/lang/String; = "OTA"

.field public static final TYPE_RF:Ljava/lang/String; = "RF"

.field public static final TYPE_RX:Ljava/lang/String; = "RX"

.field public static final TYPE_TL:Ljava/lang/String; = "TL"


# instance fields
.field private channelDeviceType:Ljava/lang/String;

.field private headendId:Ljava/lang/String;

.field private headendName:Ljava/lang/String;

.field private serviceType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend$1;

    invoke-direct {v0}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend$1;-><init>()V

    sput-object v0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 116
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendId:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendName:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->channelDeviceType:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->serviceType:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendId:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendName:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->channelDeviceType:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->serviceType:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendId:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendName:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->channelDeviceType:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->serviceType:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "headendId"    # Ljava/lang/String;
    .param p2, "headendName"    # Ljava/lang/String;
    .param p3, "channelDeviceType"    # Ljava/lang/String;
    .param p4, "serviceType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendId:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendName:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->channelDeviceType:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->serviceType:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendId:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendName:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->channelDeviceType:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->serviceType:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public getChannelDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->channelDeviceType:Ljava/lang/String;

    return-object v0
.end method

.method public getHeadendId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendId:Ljava/lang/String;

    return-object v0
.end method

.method public getHeadendName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendName:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->serviceType:Ljava/lang/String;

    return-object v0
.end method

.method public setChannelDeviceType(Ljava/lang/String;)V
    .locals 0
    .param p1, "channelDeviceType"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->channelDeviceType:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setHeadendId(Ljava/lang/String;)V
    .locals 0
    .param p1, "headendId"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendId:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setHeadendName(Ljava/lang/String;)V
    .locals 0
    .param p1, "headendName"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendName:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setServiceType(Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->serviceType:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->headendName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->channelDeviceType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Headend;->serviceType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 54
    return-void
.end method
