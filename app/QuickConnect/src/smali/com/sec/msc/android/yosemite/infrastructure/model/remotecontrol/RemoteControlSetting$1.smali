.class Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting$1;
.super Ljava/lang/Object;
.source "RemoteControlSetting.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 167
    new-instance v0, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    invoke-direct {v0, p1}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 173
    new-array v0, p1, [Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting$1;->newArray(I)[Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    move-result-object v0

    return-object v0
.end method
