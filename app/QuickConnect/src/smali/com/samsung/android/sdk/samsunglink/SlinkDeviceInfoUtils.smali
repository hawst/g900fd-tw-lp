.class public Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;
.super Ljava/lang/Object;
.source "SlinkDeviceInfoUtils.java"


# static fields
.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;->context:Landroid/content/Context;

    .line 45
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 29
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 31
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;

    if-nez v0, :cond_1

    .line 32
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;

    .line 34
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public doesLocalMultimediaFrameworkSupportSCS()Z
    .locals 2

    .prologue
    .line 83
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/lib/libSLinkNTSMngr_jni.so"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public getDeviceInfo(I)Ljava/lang/String;
    .locals 1
    .param p1, "deviceId"    # I

    .prologue
    .line 54
    const-string v0, "info"

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;->getDeviceInfo(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceInfo(ILjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "deviceId"    # I
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 65
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 66
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "INTENT_ARG_DEVICE_ID"

    int-to-long v4, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 67
    const-string v2, "INTENT_ARG_DEVICE_CMD"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceInfoUtils;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 70
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 71
    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetDeviceInfo.NAME"

    .line 72
    const/4 v5, 0x0

    .line 69
    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 75
    .local v1, "result":Landroid/os/Bundle;
    const-string v2, "method_result_str"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
