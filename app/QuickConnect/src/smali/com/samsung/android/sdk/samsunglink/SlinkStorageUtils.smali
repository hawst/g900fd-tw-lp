.class public Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;
.super Ljava/lang/Object;
.source "SlinkStorageUtils.java"


# static fields
.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;->context:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 28
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 30
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;

    if-nez v0, :cond_1

    .line 31
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;

    .line 33
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public signOutOfStorageService(I)V
    .locals 5
    .param p1, "storageDeviceId"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkStorageUtils;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 48
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 49
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SignOutOfStorageService.NAME"

    .line 50
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 51
    const/4 v4, 0x0

    .line 47
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 52
    return-void
.end method
