.class final Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ThumbnailUtility"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;
    }
.end annotation


# static fields
.field private static final THUMB_PROJECTION:[Ljava/lang/String;

.field private static final sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 893
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 895
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 896
    const-string v2, "width"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 897
    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 898
    const-string v2, "thumb_width"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 899
    const-string v2, "thumb_height"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 900
    const-string v2, "full_width"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 901
    const-string v2, "full_height"

    aput-object v2, v0, v1

    .line 895
    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->THUMB_PROJECTION:[Ljava/lang/String;

    .line 901
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelThumbnailRequest(Landroid/content/ContentResolver;Ljava/lang/String;JJ)V
    .locals 10
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "origId"    # J
    .param p4, "groupId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 1169
    new-instance v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;

    move-wide v2, p2

    move-wide v4, p4

    move v7, v6

    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;-><init>(JJII)V

    .line 1171
    .local v1, "task":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "task":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;
    check-cast v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;

    .line 1172
    .restart local v1    # "task":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;
    if-eqz v1, :cond_0

    .line 1173
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1174
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$2(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;Z)V

    .line 1176
    invoke-static {p2, p3, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1178
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v2, "cancel"

    .line 1179
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1177
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1181
    const-string v2, "group_id"

    .line 1182
    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 1180
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1183
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    move-object v2, p0

    move-object v4, v9

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1184
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 1185
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1188
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_0
    return-void
.end method

.method public static getThumbnailImage(Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 28
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "origId"    # J
    .param p4, "groupId"    # J
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "skipCacheGet"    # Z
    .param p9, "skipCachePut"    # Z
    .param p10, "bitmapOptions"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 1015
    const/16 v24, 0x0

    .line 1017
    .local v24, "result":Landroid/graphics/Bitmap;
    new-instance v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move/from16 v10, p6

    move/from16 v11, p7

    invoke-direct/range {v5 .. v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;-><init>(JJII)V

    .line 1018
    .local v5, "task":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    move/from16 v12, p6

    move/from16 v13, p7

    move/from16 v14, p8

    move/from16 v15, p9

    .line 1020
    :try_start_0
    invoke-static/range {v5 .. v15}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->getThumbnailUri(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v27

    .line 1031
    .local v27, "uri":Landroid/net/Uri;
    const/16 v17, 0x0

    .line 1033
    .local v17, "is":Ljava/io/InputStream;
    :try_start_1
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$1(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Z

    move-result v6

    if-nez v6, :cond_7

    move-object/from16 v0, p10

    iget-boolean v6, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-nez v6, :cond_7

    .line 1034
    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    if-lez v6, :cond_0

    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    if-gtz v6, :cond_4

    .line 1035
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v17

    .line 1036
    if-nez v17, :cond_3

    .line 1037
    sget-boolean v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v6, :cond_1

    .line 1038
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$0()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unable to open inputStream for bitmap. origId:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1039
    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1038
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148
    :cond_1
    if-eqz v17, :cond_2

    .line 1150
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1158
    :cond_2
    :goto_0
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1041
    const/4 v6, 0x0

    .line 1161
    :goto_1
    return-object v6

    .line 1043
    :cond_3
    const/4 v6, 0x1

    :try_start_3
    move-object/from16 v0, p10

    iput-boolean v6, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1044
    const/4 v6, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p10

    invoke-static {v0, v6, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1045
    move-object/from16 v0, p10

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    .line 1046
    move-object/from16 v0, p10

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    .line 1048
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V

    .line 1051
    move-wide/from16 v0, p2

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1052
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 1054
    .local v4, "builder":Landroid/net/Uri$Builder;
    const-string v6, "width"

    .line 1055
    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    .line 1053
    invoke-virtual {v4, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1057
    const-string v6, "height"

    .line 1058
    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    .line 1056
    invoke-virtual {v4, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1060
    const-string v6, "skip_cache_get"

    .line 1061
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    .line 1059
    invoke-virtual {v4, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1063
    const-string v6, "skip_cache_put"

    .line 1064
    invoke-static/range {p9 .. p9}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    .line 1062
    invoke-virtual {v4, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1065
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v27

    .line 1069
    .end local v4    # "builder":Landroid/net/Uri$Builder;
    :cond_4
    const/16 v25, 0x1

    .line 1070
    .local v25, "scaleFactor":I
    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    if-gt v6, v7, :cond_5

    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    if-le v6, v7, :cond_6

    .line 1071
    :cond_5
    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    if-le v6, v7, :cond_a

    .line 1072
    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    int-to-float v6, v6

    .line 1073
    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    int-to-float v7, v7

    .line 1072
    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v25

    .line 1079
    :cond_6
    :goto_2
    const/4 v6, 0x1

    move/from16 v0, v25

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v25

    .line 1081
    const/4 v6, 0x0

    move-object/from16 v0, p10

    iput-boolean v6, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1082
    move/from16 v0, v25

    move-object/from16 v1, p10

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1084
    sget-boolean v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v6, :cond_7

    .line 1085
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$0()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "::getThumbnailImage scaleFactor:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1086
    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1087
    const-string v8, " requested:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1088
    iget v8, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1089
    const/16 v8, 0x78

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1090
    iget v8, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1091
    const-string v8, " bitmap:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1092
    iget v8, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1093
    const/16 v8, 0x78

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1094
    iget v8, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1085
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    .end local v25    # "scaleFactor":I
    :cond_7
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$1(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 1099
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v17

    .line 1101
    const/4 v6, 0x0

    :try_start_4
    move-object/from16 v0, v17

    move-object/from16 v1, p10

    invoke-static {v0, v6, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 1102
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V

    .line 1103
    if-eqz v24, :cond_8

    .line 1104
    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v19

    .line 1106
    .local v19, "largestRequestDim":I
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 1107
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 1105
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 1108
    .local v18, "largestActualDim":I
    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_8

    .line 1109
    move/from16 v0, v19

    int-to-float v6, v0

    .line 1110
    move/from16 v0, v18

    int-to-float v7, v0

    .line 1109
    div-float v23, v6, v7

    .line 1114
    .local v23, "ratio":F
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    move/from16 v0, v18

    if-ne v0, v6, :cond_b

    .line 1115
    move/from16 v21, v19

    .line 1116
    .local v21, "newWidth":I
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v23

    float-to-int v0, v6

    move/from16 v20, v0

    .line 1122
    .local v20, "newHeight":I
    :goto_3
    if-lez v21, :cond_8

    if-lez v20, :cond_8

    .line 1127
    const/4 v6, 0x0

    .line 1123
    move-object/from16 v0, v24

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-static {v0, v1, v2, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v26

    .line 1128
    .local v26, "scaledBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1129
    move-object/from16 v24, v26

    .line 1148
    .end local v18    # "largestActualDim":I
    .end local v19    # "largestRequestDim":I
    .end local v20    # "newHeight":I
    .end local v21    # "newWidth":I
    .end local v23    # "ratio":F
    .end local v26    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_8
    :goto_4
    if-eqz v17, :cond_9

    .line 1150
    :try_start_5
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1158
    :cond_9
    :goto_5
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-object/from16 v6, v24

    .line 1161
    goto/16 :goto_1

    .line 1075
    .restart local v25    # "scaleFactor":I
    :cond_a
    :try_start_6
    iget v6, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    int-to-float v6, v6

    .line 1076
    iget v7, v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    int-to-float v7, v7

    .line 1075
    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v25

    goto/16 :goto_2

    .line 1118
    .end local v25    # "scaleFactor":I
    .restart local v18    # "largestActualDim":I
    .restart local v19    # "largestRequestDim":I
    .restart local v23    # "ratio":F
    :cond_b
    :try_start_7
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v23

    float-to-int v0, v6

    move/from16 v21, v0

    .line 1119
    .restart local v21    # "newWidth":I
    move/from16 v20, v19

    .restart local v20    # "newHeight":I
    goto :goto_3

    .line 1133
    .end local v18    # "largestActualDim":I
    .end local v19    # "largestRequestDim":I
    .end local v20    # "newHeight":I
    .end local v21    # "newWidth":I
    .end local v23    # "ratio":F
    :catch_0
    move-exception v22

    .line 1134
    .local v22, "oome":Ljava/lang/OutOfMemoryError;
    :try_start_8
    sget-boolean v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v6, :cond_8

    .line 1135
    if-eqz v24, :cond_d

    .line 1136
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$0()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unable to rescale bitmap. origId:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v22

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4

    .line 1143
    .end local v22    # "oome":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v16

    .line 1144
    .local v16, "ioe":Ljava/io/IOException;
    :try_start_9
    sget-boolean v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v6, :cond_c

    .line 1145
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$0()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unable to decode bitmap. origId:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1148
    :cond_c
    if-eqz v17, :cond_9

    .line 1150
    :try_start_a
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_5

    .line 1151
    :catch_2
    move-exception v6

    goto :goto_5

    .line 1138
    .end local v16    # "ioe":Ljava/io/IOException;
    .restart local v22    # "oome":Ljava/lang/OutOfMemoryError;
    :cond_d
    :try_start_b
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$0()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unable to decode bitmap. origId:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v22

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_4

    .line 1147
    .end local v22    # "oome":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v6

    .line 1148
    if-eqz v17, :cond_e

    .line 1150
    :try_start_c
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 1155
    :cond_e
    :goto_6
    :try_start_d
    throw v6
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 1157
    .end local v17    # "is":Ljava/io/InputStream;
    .end local v27    # "uri":Landroid/net/Uri;
    :catchall_1
    move-exception v6

    .line 1158
    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v5}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1159
    throw v6

    .line 1151
    .restart local v17    # "is":Ljava/io/InputStream;
    .restart local v27    # "uri":Landroid/net/Uri;
    :catch_3
    move-exception v6

    goto/16 :goto_0

    :catch_4
    move-exception v7

    goto :goto_6

    :catch_5
    move-exception v6

    goto/16 :goto_5
.end method

.method public static getThumbnailInputStream(Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Ljava/io/InputStream;
    .locals 14
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "origId"    # J
    .param p4, "groupId"    # J
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "skipCacheGet"    # Z
    .param p9, "skipCachePut"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 912
    new-instance v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;-><init>(JJII)V

    .line 913
    .local v1, "task":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p0

    move-object v3, p1

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    .line 915
    :try_start_0
    invoke-static/range {v1 .. v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->getThumbnailUri(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Landroid/net/Uri;

    move-result-object v13

    .line 925
    .local v13, "uri":Landroid/net/Uri;
    const/4 v12, 0x0

    .line 926
    .local v12, "is":Ljava/io/InputStream;
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$1(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 927
    invoke-virtual {p0, v13}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    .line 938
    :cond_0
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 934
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v12

    .line 930
    :catch_0
    move-exception v0

    .line 931
    .local v0, "exc":Ljava/lang/Exception;
    :try_start_1
    sget-boolean v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v2, :cond_1

    .line 932
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$0()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ThumbnailUtility.getThumbnailUri failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 938
    :cond_1
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 934
    const/4 v12, 0x0

    goto :goto_0

    .line 937
    .end local v0    # "exc":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 938
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->sTaskMap:Ljava/util/concurrent/ConcurrentHashMap;

    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHashKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$0(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 939
    throw v2
.end method

.method private static getThumbnailUri(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;Landroid/content/ContentResolver;Ljava/lang/String;JJIIZZ)Landroid/net/Uri;
    .locals 19
    .param p0, "task"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "origId"    # J
    .param p5, "groupId"    # J
    .param p7, "width"    # I
    .param p8, "height"    # I
    .param p9, "skipCacheGet"    # Z
    .param p10, "skipCachePut"    # Z

    .prologue
    .line 953
    move-wide/from16 v0, p3

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 955
    .local v5, "uri":Landroid/net/Uri;
    const/16 v17, -0x1

    .line 956
    .local v17, "requestWidth":I
    const/16 v16, -0x1

    .line 958
    .local v16, "requestHeight":I
    if-nez p9, :cond_1

    .line 959
    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility;->THUMB_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 960
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_1

    .line 961
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 962
    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 963
    const/4 v4, 0x1

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 964
    const/4 v4, 0x2

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualWidth:I

    .line 965
    const/4 v4, 0x3

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mActualHeight:I

    .line 966
    const/4 v4, 0x4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullWidth:I

    .line 967
    const/4 v4, 0x5

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullHeight:I

    .line 969
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullWidth:I

    if-lez v4, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullHeight:I

    if-lez v4, :cond_0

    .line 970
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    int-to-long v6, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    int-to-long v8, v4

    mul-long v14, v6, v8

    .line 971
    .local v14, "requestArea":J
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullWidth:I

    int-to-long v6, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullHeight:I

    int-to-long v8, v4

    mul-long v12, v6, v8

    .line 972
    .local v12, "fullArea":J
    cmp-long v4, v14, v12

    if-lez v4, :cond_0

    .line 973
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullWidth:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    .line 974
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mFullHeight:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    .line 979
    .end local v12    # "fullArea":J
    .end local v14    # "requestArea":J
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 983
    .end local v11    # "cursor":Landroid/database/Cursor;
    :cond_1
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mCancelled:Z
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->access$1(Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 984
    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v10

    .line 986
    .local v10, "builder":Landroid/net/Uri$Builder;
    const-string v4, "width"

    .line 987
    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 985
    invoke-virtual {v10, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 989
    const-string v4, "height"

    .line 990
    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 988
    invoke-virtual {v10, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 992
    const-string v6, "skip_cache_get"

    .line 993
    if-nez p9, :cond_3

    .line 994
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mWidth:I

    move/from16 v0, v17

    if-gt v4, v0, :cond_3

    .line 995
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ThumbnailUtility$ThumbnailTask;->mHeight:I

    move/from16 v0, v16

    if-gt v4, v0, :cond_3

    const/4 v4, 0x0

    .line 993
    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    .line 991
    invoke-virtual {v10, v6, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 997
    const-string v4, "skip_cache_put"

    .line 998
    invoke-static/range {p10 .. p10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    .line 996
    invoke-virtual {v10, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 999
    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 1002
    .end local v10    # "builder":Landroid/net/Uri$Builder;
    :cond_2
    return-object v5

    .line 995
    .restart local v10    # "builder":Landroid/net/Uri$Builder;
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method
