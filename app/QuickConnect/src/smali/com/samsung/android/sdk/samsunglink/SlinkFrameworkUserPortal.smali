.class public Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;
.super Ljava/lang/Object;
.source "SlinkFrameworkUserPortal.java"


# static fields
.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    .line 45
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 29
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 31
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    if-nez v0, :cond_1

    .line 32
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    .line 34
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public deleteUserMobile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 85
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 86
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.DeleteUserMobile.NAME"

    .line 88
    const/4 v4, 0x0

    .line 84
    invoke-virtual {v1, v2, v3, p1, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 89
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public deleteUserPC(Ljava/lang/String;)Z
    .locals 5
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 76
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 77
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.DeleteUserPC.NAME"

    .line 79
    const/4 v4, 0x0

    .line 75
    invoke-virtual {v1, v2, v3, p1, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 80
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public sendWakeupPushInBackground()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 93
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 94
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 95
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SendWakeupPushInBackground.NAME"

    .line 93
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 98
    return-void
.end method

.method public setDeviceInfo()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 102
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 103
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetDeviceInfo.NAME"

    .line 101
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 106
    return-void
.end method

.method public turnOnDevice(Ljava/lang/String;)Z
    .locals 5
    .param p1, "deviceImei"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 67
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 68
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.TurnOnDevice.NAME"

    .line 70
    const/4 v4, 0x0

    .line 66
    invoke-virtual {v1, v2, v3, p1, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 71
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public updateuserDeviceName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "newDeviceName"    # Ljava/lang/String;
    .param p2, "deviceImei"    # Ljava/lang/String;

    .prologue
    .line 49
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_NAME"

    .line 50
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v2, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_IMEI"

    .line 53
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 58
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 59
    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.UpdateUserDeviceName.NAME"

    .line 60
    const/4 v5, 0x0

    .line 57
    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 62
    .local v1, "result":Landroid/os/Bundle;
    const-string v2, "method_result"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    return v2
.end method
