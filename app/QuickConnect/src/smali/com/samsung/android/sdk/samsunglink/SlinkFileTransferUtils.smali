.class public Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;
.super Ljava/lang/Object;
.source "SlinkFileTransferUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    }
.end annotation


# static fields
.field public static final ACTION_CHOOSE_DEVICE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.ChooseDevice"

.field public static final ACTION_DOWNLOAD:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.Download"

.field public static final ACTION_DOWNLOAD_MODAL:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.DownloadModal"

.field public static final ACTION_SEND_TO:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.SendTo"

.field public static final ACTION_TRANSFER:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.Transfer"

.field public static final ACTION_UPLOAD:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.filetransfer.Upload"

.field public static final DEVICE_CHOOSER_RESULT_BROADCAST_ACTION:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.DEVICE_CHOOSER_RESULT_BROADCAST_ACTION"

.field public static final DOWNLOAD_RESULT_BROADCAST_ACTION:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.DOWNLOAD_RESULT_BROADCAST_ACTION"

.field public static final EXTRA_BROADCAST_RESULTS:Ljava/lang/String; = "broadcastResults"

.field public static final EXTRA_CHOOSE_STORAGE:Ljava/lang/String; = "choose_storage"

.field public static final EXTRA_DEVICE_CHOOSER_MODE:Ljava/lang/String; = "deviceChooserMode"

.field public static final EXTRA_DEVICE_ID:Ljava/lang/String; = "deviceId"

.field public static final EXTRA_DOWNLOADING_TEXT:Ljava/lang/String; = "downloadingText"

.field public static final EXTRA_MEDIA_SET:Ljava/lang/String; = "mediaSet"

.field public static final EXTRA_PATHS:Ljava/lang/String; = "paths"

.field public static final EXTRA_ROW_IDS:Ljava/lang/String; = "rowIds"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "title"

.field public static final EXTRA_TRANSFER_OPTIONS:Ljava/lang/String; = "transferOptions"

.field public static final EXTRA_URIS:Ljava/lang/String; = "uris"

.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    .line 152
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 136
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 138
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    if-nez v0, :cond_1

    .line 139
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    .line 141
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public createDeviceChooserActivityIntent(JLcom/samsung/android/sdk/samsunglink/SlinkMediaSet;ZZ)Landroid/content/Intent;
    .locals 3
    .param p1, "sourceDeviceId"    # J
    .param p3, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p4, "broadcastResults"    # Z
    .param p5, "chooseStorage"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 543
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.ChooseDevice"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 544
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "deviceId"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 545
    if-eqz p3, :cond_0

    .line 546
    const-string v1, "mediaSet"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 548
    :cond_0
    if-eqz p4, :cond_1

    .line 549
    const-string v1, "broadcastResults"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 551
    :cond_1
    if-eqz p5, :cond_2

    .line 552
    const-string v1, "choose_storage"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 554
    :cond_2
    return-object v0
.end method

.method public createDeviceChooserActivityIntent(JZ)Landroid/content/Intent;
    .locals 7
    .param p1, "sourceDeviceId"    # J
    .param p3, "broadcastResults"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 467
    .line 469
    const/4 v4, 0x0

    .line 471
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v5, p3

    .line 467
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createDeviceChooserActivityIntent(JLcom/samsung/android/sdk/samsunglink/SlinkMediaSet;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createDeviceChooserActivityIntent(J[JZ)Landroid/content/Intent;
    .locals 7
    .param p1, "sourceDeviceId"    # J
    .param p3, "samsungLinkMediaStoreRowIds"    # [J
    .param p4, "broadcastResults"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 504
    .line 506
    invoke-static {p3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v4

    .line 508
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v5, p4

    .line 504
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createDeviceChooserActivityIntent(JLcom/samsung/android/sdk/samsunglink/SlinkMediaSet;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createModalDownloadActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;
    .locals 3
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "downloadingText"    # Ljava/lang/String;
    .param p4, "broadcastResults"    # Z
    .param p5, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 387
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.DownloadModal"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 388
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "mediaSet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 389
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 393
    const-string v1, "downloadingText"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    :cond_1
    if-eqz p4, :cond_2

    .line 396
    const-string v1, "broadcastResults"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 398
    :cond_2
    if-eqz p5, :cond_3

    .line 399
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 402
    :cond_3
    return-object v0
.end method

.method public createModalDownloadActivityIntent([JLjava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;
    .locals 6
    .param p1, "samsungLinkMediaStoreRowIds"    # [J
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "downloadingText"    # Ljava/lang/String;
    .param p4, "broadcastResults"    # Z
    .param p5, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 348
    .line 349
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    .line 348
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createModalDownloadActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createSendToActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;
    .locals 3
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 587
    if-nez p1, :cond_0

    .line 588
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "mediaSet is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 591
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.SendTo"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 592
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_1

    .line 593
    const-string v1, "mediaSet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 595
    :cond_1
    if-eqz p2, :cond_2

    .line 596
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 598
    :cond_2
    return-object v0
.end method

.method public downloadFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 181
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.Download"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 182
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "mediaSet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 183
    if-eqz p2, :cond_0

    .line 184
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 188
    return-void
.end method

.method public downloadFiles([JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 1
    .param p1, "samsungLinkMediaStoreRowIds"    # [J
    .param p2, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 166
    .line 167
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 166
    invoke-virtual {p0, v0, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->downloadFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 169
    return-void
.end method

.method public getLocalPathsFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;
    .locals 2
    .param p1, "resultData"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 437
    const-string v1, "paths"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 438
    .local v0, "localPaths":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 439
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 441
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;
    .locals 5
    .param p1, "resultData"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415
    const-string v3, "uris"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 416
    .local v0, "mediaStoreUris":[Landroid/os/Parcelable;
    if-nez v0, :cond_1

    .line 417
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 424
    :cond_0
    return-object v2

    .line 419
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    array-length v3, v0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 420
    .local v2, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 421
    .local v1, "parcelable":Landroid/os/Parcelable;
    check-cast v1, Landroid/net/Uri;

    .end local v1    # "parcelable":Landroid/os/Parcelable;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getTargetDeviceIdFromSuccessfulDeviceChooserResult(Landroid/content/Intent;)J
    .locals 4
    .param p1, "resultData"    # Landroid/content/Intent;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 568
    const-string v0, "deviceId"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public is3boxSupported(JJ)Z
    .locals 7
    .param p1, "sourceDeviceId"    # J
    .param p3, "targetDeviceId"    # J

    .prologue
    .line 297
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 299
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_SOURCE_DEVICE_ID"

    .line 298
    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 302
    const-string v2, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_TARGET_DEVICE_ID"

    .line 301
    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 305
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 306
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 307
    const-string v4, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.Is3boxSupported.NAME"

    .line 308
    const/4 v5, 0x0

    .line 305
    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 311
    .local v1, "result":Landroid/os/Bundle;
    const-string v2, "method_result"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    return v2
.end method

.method public transferFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "remoteDeviceId"    # J
    .param p4, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 225
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.Transfer"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 226
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "mediaSet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 227
    const-string v1, "deviceId"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 228
    if-eqz p4, :cond_0

    .line 229
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 233
    return-void
.end method

.method public transferFiles([JJLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2
    .param p1, "samsungLinkMediaStoreRowIds"    # [J
    .param p2, "remoteDeviceId"    # J
    .param p4, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 207
    .line 208
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 207
    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->transferFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 211
    return-void
.end method

.method public uploadMediaStoreFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .param p2, "remoteDeviceId"    # J
    .param p4, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .prologue
    .line 275
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.filetransfer.Upload"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 277
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "mediaSet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 278
    const-string v1, "deviceId"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 279
    if-eqz p4, :cond_0

    .line 280
    const-string v1, "transferOptions"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 284
    return-void
.end method

.method public uploadMediaStoreFiles([JJLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2
    .param p1, "mediaStoreRowIds"    # [J
    .param p2, "remoteDeviceId"    # J
    .param p4, "options"    # Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 253
    .line 254
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 253
    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->uploadMediaStoreFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;JLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 257
    return-void
.end method
