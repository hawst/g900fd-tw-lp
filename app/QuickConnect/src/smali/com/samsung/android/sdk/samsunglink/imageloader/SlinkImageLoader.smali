.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;
.super Ljava/lang/Object;
.source "SlinkImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$FIFOPriofityRunnable;,
        Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;,
        Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$LoadFromFileCacheTask;,
        Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$LoadFromRemoteTask;,
        Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$NullImageCache;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final sDecodeBuffer:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[B>;"
        }
    .end annotation
.end field


# instance fields
.field private final mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;

.field private final mContext:Landroid/content/Context;

.field private final mFileCacheExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mMainHandler:Landroid/os/Handler;

.field private final mRemoteExecutor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->sDecodeBuffer:Ljava/lang/ThreadLocal;

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageCache"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x4

    const/4 v2, 0x2

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 62
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 63
    new-instance v7, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    move v3, v2

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mFileCacheExecutor:Ljava/util/concurrent/ExecutorService;

    .line 65
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 69
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 70
    new-instance v7, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    move v2, v0

    move v3, v0

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mRemoteExecutor:Ljava/util/concurrent/ExecutorService;

    .line 73
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mMainHandler:Landroid/os/Handler;

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mContext:Landroid/content/Context;

    .line 94
    if-nez p2, :cond_0

    .line 95
    new-instance p2, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$NullImageCache;

    .end local p2    # "imageCache":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;
    const/4 v0, 0x0

    invoke-direct {p2, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$NullImageCache;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$NullImageCache;)V

    .line 97
    .restart local p2    # "imageCache":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;

    .line 98
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;II)I
    .locals 1

    .prologue
    .line 1057
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->calcScaleFactor(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;II)I

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 932
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->loadBitmap(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)Z
    .locals 1

    .prologue
    .line 478
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->isBitmapTooSmall(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mRemoteExecutor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$6()Ljava/lang/ThreadLocal;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->sDecodeBuffer:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 896
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->createFineScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 1069
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->closeInputStreamQuietly(Ljava/io/InputStream;)V

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1011
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->dispatchBitmapResult(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private calcScaleFactor(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;II)I
    .locals 3
    .param p1, "request"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .param p2, "fileWidth"    # I
    .param p3, "fileHeight"    # I

    .prologue
    .line 1058
    const/4 v0, 0x1

    .line 1059
    .local v0, "scaleFactor":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMaxHeight()I

    move-result v1

    if-gt p3, v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMaxWidth()I

    move-result v1

    if-le p2, v1, :cond_1

    .line 1060
    :cond_0
    if-le p2, p3, :cond_2

    .line 1061
    int-to-float v1, p3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMaxHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1066
    :cond_1
    :goto_0
    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1

    .line 1063
    :cond_2
    int-to-float v1, p2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMaxWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method

.method private closeInputStreamQuietly(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 1070
    if-eqz p1, :cond_0

    .line 1072
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077
    :cond_0
    :goto_0
    return-void

    .line 1073
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private createFineScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "requestedWidth"    # I
    .param p3, "requestedHeight"    # I

    .prologue
    .line 897
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 898
    .local v1, "largestRequestDim":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 899
    .local v0, "largestActualDim":I
    if-le v0, v1, :cond_0

    .line 900
    int-to-float v7, v1

    int-to-float v8, v0

    div-float v5, v7, v8

    .line 904
    .local v5, "ratio":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-ne v0, v7, :cond_1

    .line 905
    move v3, v1

    .line 906
    .local v3, "newWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v5

    float-to-int v2, v7

    .line 912
    .local v2, "newHeight":I
    :goto_0
    if-lez v3, :cond_0

    if-lez v2, :cond_0

    .line 918
    const/4 v7, 0x0

    .line 914
    :try_start_0
    invoke-static {p1, v3, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 919
    .local v6, "scaledBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 920
    move-object p1, v6

    .line 929
    .end local v2    # "newHeight":I
    .end local v3    # "newWidth":I
    .end local v5    # "ratio":F
    .end local v6    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_1
    return-object p1

    .line 908
    .restart local v5    # "ratio":F
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v5

    float-to-int v3, v7

    .line 909
    .restart local v3    # "newWidth":I
    move v2, v1

    .restart local v2    # "newHeight":I
    goto :goto_0

    .line 921
    :catch_0
    move-exception v4

    .line 922
    .local v4, "oome":Ljava/lang/OutOfMemoryError;
    sget-boolean v7, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v7, :cond_0

    .line 923
    sget-object v7, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    const-string v8, "Out of memory fine-scaling Bitmap."

    invoke-static {v7, v8, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private dispatchBitmapResult(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "imageContainer"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$3;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1055
    return-void
.end method

.method private getCacheKey(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)Ljava/lang/String;
    .locals 4
    .param p1, "request"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    .prologue
    .line 457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getRowId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMediaType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isBitmapTooSmall(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)Z
    .locals 2
    .param p1, "bitmapInfo"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;
    .param p2, "request"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    .prologue
    .line 479
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->getRequestedMaxWidth()I

    move-result v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMaxWidth()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 480
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->getRequestedMaxHeight()I

    move-result v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMaxHeight()I

    move-result v1

    .line 479
    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private loadBitmap(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "imageContainer"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 935
    .line 936
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    .line 937
    const-string v7, "cancel"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    .line 938
    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 935
    invoke-static {p1, v5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$8(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/net/Uri;)V

    .line 940
    const/4 v2, 0x0

    .line 942
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    sget-boolean v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v5, :cond_0

    .line 943
    sget-object v5, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "openInputStream for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 947
    :cond_0
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 949
    const/4 v5, 0x0

    :try_start_2
    invoke-static {p1, v5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$8(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/net/Uri;)V

    .line 952
    if-nez v2, :cond_5

    .line 953
    sget-boolean v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v5, :cond_1

    .line 954
    sget-object v5, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "openInputStream returned null for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1002
    :cond_1
    if-eqz v2, :cond_2

    .line 1004
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    :goto_0
    move-object v4, v6

    .line 1000
    :cond_3
    :goto_1
    return-object v4

    .line 948
    :catchall_0
    move-exception v5

    .line 949
    const/4 v6, 0x0

    :try_start_4
    invoke-static {p1, v6}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$8(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/net/Uri;)V

    .line 950
    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1001
    :catchall_1
    move-exception v5

    .line 1002
    if-eqz v2, :cond_4

    .line 1004
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1008
    :cond_4
    :goto_2
    throw v5

    .line 959
    :cond_5
    :try_start_6
    # invokes: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->isCanceled()Z
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$0(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 963
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getRequest()Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    move-result-object v3

    .line 964
    .local v3, "request":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->bitmapFactoryOptions:Landroid/graphics/BitmapFactory$Options;
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$1(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    .line 965
    .local v0, "bitmapOptions":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x0

    iput-boolean v5, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 966
    const/4 v1, 0x0

    .line 968
    .local v1, "clearTempStorage":Z
    iget-object v5, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    if-nez v5, :cond_6

    .line 969
    sget-object v5, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->sDecodeBuffer:Ljava/lang/ThreadLocal;

    invoke-virtual {v5}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    iput-object v5, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 970
    const/4 v1, 0x1

    .line 973
    :cond_6
    const/4 v4, 0x0

    .line 976
    .local v4, "tempBitmap":Landroid/graphics/Bitmap;
    const/4 v5, 0x0

    :try_start_7
    invoke-static {v2, v5, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v4

    .line 980
    :goto_3
    :try_start_8
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->closeInputStreamQuietly(Ljava/io/InputStream;)V

    .line 981
    const/4 v2, 0x0

    .line 983
    if-eqz v1, :cond_7

    .line 984
    const/4 v5, 0x0

    iput-object v5, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 987
    :cond_7
    # invokes: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->isCanceled()Z
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$0(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v5

    if-eqz v5, :cond_9

    .line 1002
    if-eqz v2, :cond_8

    .line 1004
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    :cond_8
    :goto_4
    move-object v4, v6

    .line 988
    goto :goto_1

    .line 991
    :cond_9
    if-eqz v4, :cond_a

    .line 994
    :try_start_a
    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMaxWidth()I

    move-result v5

    .line 995
    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getMaxHeight()I

    move-result v6

    .line 992
    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->createFineScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 998
    :cond_a
    invoke-direct {p0, p1, v4}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->dispatchBitmapResult(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/Bitmap;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1002
    if-eqz v2, :cond_3

    .line 1004
    :try_start_b
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    goto :goto_1

    .line 1005
    :catch_0
    move-exception v5

    goto :goto_1

    .end local v0    # "bitmapOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v1    # "clearTempStorage":Z
    .end local v3    # "request":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .end local v4    # "tempBitmap":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v5

    goto :goto_0

    .line 977
    .restart local v0    # "bitmapOptions":Landroid/graphics/BitmapFactory$Options;
    .restart local v1    # "clearTempStorage":Z
    .restart local v3    # "request":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .restart local v4    # "tempBitmap":Landroid/graphics/Bitmap;
    :catch_2
    move-exception v5

    goto :goto_3

    .line 1005
    :catch_3
    move-exception v5

    goto :goto_4

    .end local v0    # "bitmapOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v1    # "clearTempStorage":Z
    .end local v3    # "request":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .end local v4    # "tempBitmap":Landroid/graphics/Bitmap;
    :catch_4
    move-exception v6

    goto :goto_2
.end method

.method private throwIfNotOnMainThread()V
    .locals 2

    .prologue
    .line 461
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 462
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ImageLoader must be invoked from the main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 464
    :cond_0
    return-void
.end method


# virtual methods
.method public loadDeviceIcon(JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[ILandroid/widget/ImageView;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    .locals 9
    .param p1, "deviceId"    # J
    .param p3, "size"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;
    .param p4, "theme"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;
    .param p5, "states"    # [I
    .param p6, "imageView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v0, 0x0

    .line 223
    new-instance v7, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;

    invoke-direct {v7, p6, v0, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;-><init>(Landroid/widget/ImageView;II)V

    .local v7, "imageListener":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 224
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->loadDeviceIcon(JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[ILcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    move-result-object v0

    return-object v0
.end method

.method public loadDeviceIcon(JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[ILcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    .locals 21
    .param p1, "deviceId"    # J
    .param p3, "size"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;
    .param p4, "theme"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;
    .param p5, "states"    # [I
    .param p6, "imageListener"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;

    .prologue
    .line 250
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 251
    .local v19, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, v19

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 252
    const-string v5, ":"

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    move-object/from16 v0, v19

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 254
    const-string v5, ":"

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    move-object/from16 v0, v19

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 256
    const-string v5, ":"

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-static/range {p5 .. p5}, Ljava/util/Arrays;->sort([I)V

    .line 259
    invoke-static/range {p5 .. p5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 262
    .local v8, "cacheKey":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;

    invoke-interface {v5, v8}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;->getBitmapInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;

    move-result-object v18

    .line 264
    .local v18, "cachedBitmapInfo":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;
    new-instance v4, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    .line 265
    const/4 v6, 0x0

    .line 266
    const/4 v7, 0x0

    move-object/from16 v5, p0

    move-object/from16 v9, p6

    .line 264
    invoke-direct/range {v4 .. v9}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;)V

    .line 270
    .local v4, "imageContainer":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    if-eqz v18, :cond_1

    .line 272
    sget-boolean v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v5, :cond_0

    .line 273
    sget-object v5, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "cache hit for device icon "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_0
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$4(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/Bitmap;)V

    .line 276
    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$5(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V

    .line 277
    const/4 v5, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;->onResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V

    .line 331
    :goto_0
    return-object v4

    .line 282
    :cond_1
    const/4 v5, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;->onResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V

    .line 283
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mFileCacheExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$FIFOPriofityRunnable;

    new-instance v10, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;

    move-object/from16 v11, p0

    move-wide/from16 v12, p1

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v17, v4

    invoke-direct/range {v10 .. v17}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[ILcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V

    .line 330
    const/4 v7, 0x0

    invoke-direct {v6, v10, v7}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$FIFOPriofityRunnable;-><init>(Ljava/lang/Runnable;I)V

    .line 283
    invoke-interface {v5, v6}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public loadImage(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;Landroid/graphics/BitmapFactory$Options;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    .locals 12
    .param p1, "request"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .param p2, "imageListener"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;
    .param p3, "bitmapFactoryOptions"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 395
    sget-boolean v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v1, :cond_0

    .line 396
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loadImage "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->throwIfNotOnMainThread()V

    .line 402
    if-nez p3, :cond_1

    .line 403
    new-instance p3, Landroid/graphics/BitmapFactory$Options;

    .end local p3    # "bitmapFactoryOptions":Landroid/graphics/BitmapFactory$Options;
    invoke-direct {p3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 404
    .restart local p3    # "bitmapFactoryOptions":Landroid/graphics/BitmapFactory$Options;
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, p3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 407
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->getCacheKey(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)Ljava/lang/String;

    move-result-object v4

    .line 410
    .local v4, "cacheKey":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;

    invoke-interface {v1, v4}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;->getBitmapInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;

    move-result-object v11

    .line 411
    .local v11, "cachedBitmapInfo":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;
    if-eqz v11, :cond_5

    .line 413
    sget-boolean v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v1, :cond_2

    .line 414
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cache hit for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    .line 417
    invoke-virtual {v11}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    .line 416
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;)V

    .line 422
    .local v0, "container":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    invoke-static {v0, p3}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$6(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/BitmapFactory$Options;)V

    .line 424
    invoke-direct {p0, v11, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->isBitmapTooSmall(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 425
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$5(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V

    .line 432
    :cond_3
    :goto_0
    const/4 v1, 0x1

    invoke-interface {p2, v0, v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;->onResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V

    .line 433
    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->complete:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$7(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 453
    .end local v0    # "container":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    :goto_1
    return-object v0

    .line 427
    .restart local v0    # "container":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    :cond_4
    sget-boolean v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v1, :cond_3

    .line 428
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bitmap in cache too small for request "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 437
    .end local v0    # "container":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    :cond_5
    sget-boolean v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v1, :cond_6

    .line 438
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cache miss for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_6
    new-instance v5, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    const/4 v7, 0x0

    move-object v6, p0

    move-object v8, p1

    move-object v9, v4

    move-object v10, p2

    invoke-direct/range {v5 .. v10}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;)V

    .line 444
    .local v5, "imageContainer":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    invoke-static {v5, p3}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->access$6(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Landroid/graphics/BitmapFactory$Options;)V

    .line 446
    if-nez v11, :cond_7

    .line 448
    const/4 v1, 0x1

    invoke-interface {p2, v5, v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;->onResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V

    .line 451
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mFileCacheExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$FIFOPriofityRunnable;

    new-instance v3, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$LoadFromFileCacheTask;

    .line 452
    invoke-direct {v3, p0, v5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$LoadFromFileCacheTask;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getPriority()I

    move-result v6

    invoke-direct {v2, v3, v6}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$FIFOPriofityRunnable;-><init>(Ljava/lang/Runnable;I)V

    .line 451
    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    move-object v0, v5

    .line 453
    goto :goto_1
.end method

.method public loadImageFromCursor(Landroid/database/Cursor;Landroid/widget/ImageView;IIIII)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "maxWidth"    # I
    .param p4, "maxHeight"    # I
    .param p5, "priority"    # I
    .param p6, "loadingResource"    # I
    .param p7, "errorResource"    # I

    .prologue
    .line 364
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;

    invoke-direct {v0, p2, p6, p7}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;-><init>(Landroid/widget/ImageView;II)V

    .line 369
    .local v0, "imageListener":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;
    invoke-static {p1, p3, p4, p5}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->createFromCursor(Landroid/database/Cursor;III)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    move-result-object v1

    .line 375
    .local v1, "imageRequest":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->loadImage(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;Landroid/graphics/BitmapFactory$Options;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    move-result-object v2

    return-object v2
.end method
