.class public Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;
.super Ljava/lang/Object;
.source "SlinkFrameworkStorageGateway.java"


# static fields
.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;->context:Landroid/content/Context;

    .line 47
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 31
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 33
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;

    if-nez v0, :cond_1

    .line 34
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;

    .line 36
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public accountInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 59
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 60
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 61
    const-string v3, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.GetSignInStatus.NAME"

    .line 59
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 64
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public logout(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 50
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkStorageGateway;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 51
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 52
    const-string v3, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.GetSignInStatus.NAME"

    .line 50
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 55
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
