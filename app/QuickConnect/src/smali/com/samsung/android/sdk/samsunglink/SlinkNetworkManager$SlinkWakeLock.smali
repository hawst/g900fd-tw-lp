.class public final Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
.super Ljava/lang/Object;
.source "SlinkNetworkManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "SlinkWakeLock"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SlinkWakeLock"


# instance fields
.field private final context:Landroid/content/Context;

.field private held:Z

.field private final serviceConnection:Landroid/content/ServiceConnection;

.field private final tag:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->this$0:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    .line 163
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock$1;-><init>(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->serviceConnection:Landroid/content/ServiceConnection;

    .line 175
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "tag must be non-empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_0
    iput-object p3, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->tag:Ljava/lang/String;

    .line 179
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    .line 180
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;-><init>(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public acquire()V
    .locals 6

    .prologue
    .line 192
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v1

    .line 193
    .local v1, "siUtils":Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isUpgradeAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getPlatformUpgradeIntent()Landroid/content/Intent;

    move-result-object v0

    .line 195
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 198
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    if-nez v2, :cond_1

    .line 199
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    .line 200
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.NETWORK_LOCK_SERVICE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 201
    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->serviceConnection:Landroid/content/ServiceConnection;

    .line 202
    const/4 v5, 0x1

    .line 199
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    .line 204
    :cond_1
    sget-boolean v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v2, :cond_2

    .line 205
    const-string v2, "SlinkWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::acquire success = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " tag = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->tag:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_2
    return-void
.end method

.method public isHeld()Z
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    return v0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    if-eqz v0, :cond_0

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->held:Z

    .line 219
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 220
    sget-boolean v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v0, :cond_0

    .line 221
    const-string v0, "SlinkWakeLock"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::release tag = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :cond_0
    return-void
.end method
