.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;
.super Ljava/lang/Object;
.source "SlinkLruImageCache.java"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;,
        Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;

.field private final mComponentCallbacksListener:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;

.field private final mContext:Landroid/content/Context;

.field private final mMinTrimMemoryLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxSizeInBytes"    # I
    .param p3, "minTrimMemoryLevel"    # I

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mContext:Landroid/content/Context;

    .line 79
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;

    invoke-direct {v0, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;

    .line 80
    iput p3, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mMinTrimMemoryLevel:I

    .line 82
    if-ltz p3, :cond_0

    .line 83
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mComponentCallbacksListener:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;

    .line 84
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mComponentCallbacksListener:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mComponentCallbacksListener:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mMinTrimMemoryLevel:I

    return v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static create(Landroid/content/Context;FI)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "maxPercentageOfMemory"    # F
    .param p2, "minTrimMemoryLevel"    # I

    .prologue
    .line 46
    const/4 v4, 0x0

    cmpg-float v4, p1, v4

    if-lez v4, :cond_0

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_1

    .line 47
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "maxPercentageOfMemory must be between 0 and 1"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 50
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v5, 0x100000

    and-int/2addr v4, v5

    if-eqz v4, :cond_2

    const/4 v1, 0x1

    .line 52
    .local v1, "largeHeap":Z
    :goto_0
    const-string v4, "activity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 51
    check-cast v0, Landroid/app/ActivityManager;

    .line 55
    .local v0, "activityManager":Landroid/app/ActivityManager;
    if-eqz v1, :cond_3

    .line 56
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLargeMemoryClass()I

    move-result v3

    .line 61
    .local v3, "memoryClass":I
    :goto_1
    mul-int/lit16 v4, v3, 0x400

    mul-int/lit16 v4, v4, 0x400

    int-to-float v4, v4

    mul-float/2addr v4, p1

    float-to-int v2, v4

    .line 63
    .local v2, "maxSizeInBytes":I
    new-instance v4, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;

    invoke-direct {v4, p0, v2, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;-><init>(Landroid/content/Context;II)V

    return-object v4

    .line 50
    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    .end local v1    # "largeHeap":Z
    .end local v2    # "maxSizeInBytes":I
    .end local v3    # "memoryClass":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 58
    .restart local v0    # "activityManager":Landroid/app/ActivityManager;
    .restart local v1    # "largeHeap":Z
    :cond_3
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v3

    .restart local v3    # "memoryClass":I
    goto :goto_1
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;->evictAll()V

    .line 112
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 113
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->clearCache()V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mComponentCallbacksListener:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mComponentCallbacksListener:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 108
    :cond_0
    return-void
.end method

.method public getBitmapInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;

    return-object v0
.end method

.method public putBitmapInfo(Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmapInfo"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache;->mCache:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkLruImageCache$MyLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-void
.end method
