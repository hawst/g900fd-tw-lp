.class public final Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
.super Ljava/lang/Object;
.source "SlinkMediaSet.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private idColumnName:Ljava/lang/String;

.field private ids:[Ljava/lang/String;

.field private include:Z

.field private selection:Ljava/lang/String;

.field private selectionArgs:[Ljava/lang/String;

.field private uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 161
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->uri:Landroid/net/Uri;

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selection:Ljava/lang/String;

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selectionArgs:[Ljava/lang/String;

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->ids:[Ljava/lang/String;

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->idColumnName:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->include:Z

    .line 138
    return-void

    .line 137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private static convertToStringArray([J)[Ljava/lang/String;
    .locals 4
    .param p0, "vals"    # [J

    .prologue
    .line 164
    if-nez p0, :cond_1

    .line 165
    const/4 v2, 0x0

    new-array v1, v2, [Ljava/lang/String;

    .line 172
    :cond_0
    return-object v1

    .line 168
    :cond_1
    array-length v2, p0

    new-array v1, v2, [Ljava/lang/String;

    .line 169
    .local v1, "result":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 170
    aget-wide v2, p0, v0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static createExcludeSet(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;[J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;
    .param p3, "idsToExclude"    # [J

    .prologue
    .line 85
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;-><init>()V

    .line 86
    .local v0, "mediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    iput-object p0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->uri:Landroid/net/Uri;

    .line 87
    iput-object p1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selection:Ljava/lang/String;

    .line 88
    iput-object p2, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selectionArgs:[Ljava/lang/String;

    .line 89
    invoke-static {p3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->convertToStringArray([J)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->ids:[Ljava/lang/String;

    .line 90
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->include:Z

    .line 92
    return-object v0
.end method

.method public static createFromMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 5
    .param p0, "mediaStoreIds"    # [J

    .prologue
    const/4 v4, 0x1

    .line 35
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;-><init>()V

    .line 36
    .local v0, "mediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->uri:Landroid/net/Uri;

    .line 37
    const-string v1, "transport_type=?"

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selection:Ljava/lang/String;

    .line 38
    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selectionArgs:[Ljava/lang/String;

    .line 39
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->convertToStringArray([J)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->ids:[Ljava/lang/String;

    .line 40
    iput-boolean v4, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->include:Z

    .line 41
    const-string v1, "local_source_media_id"

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->idColumnName:Ljava/lang/String;

    .line 43
    return-object v0
.end method

.method public static createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 3
    .param p0, "slinkMediaStoreIds"    # [J

    .prologue
    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;-><init>()V

    .line 56
    .local v0, "mediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->uri:Landroid/net/Uri;

    .line 57
    iput-object v2, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selection:Ljava/lang/String;

    .line 58
    iput-object v2, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selectionArgs:[Ljava/lang/String;

    .line 59
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->convertToStringArray([J)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->ids:[Ljava/lang/String;

    .line 60
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->include:Z

    .line 62
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public getIdColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->idColumnName:Ljava/lang/String;

    return-object v0
.end method

.method public getIds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->ids:[Ljava/lang/String;

    return-object v0
.end method

.method public getSelection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selection:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectionArgs()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selectionArgs:[Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public isInclude()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->include:Z

    return v0
.end method

.method public isSlinkUri()Z
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x0

    .line 142
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->uri:Landroid/net/Uri;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 143
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selection:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->selectionArgs:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->ids:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->idColumnName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 147
    iget-boolean v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->include:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 148
    return-void
.end method
