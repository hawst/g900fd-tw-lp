.class public Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
.super Ljava/lang/Object;
.source "SlinkSignInUtils.java"


# static fields
.field public static final ACTION_CONFIRM_SAMSUNG_LINK_USAGE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_CONFIRM_SAMSUNG_LINK_USAGE"

.field public static final ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE"

.field public static final ACTION_SDK_SIGNIN:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SDK_SIGNIN"

.field public static final BROADCAST_AUTH_CHANGED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_AUTH_CHANGED"

.field public static final BROADCAST_SIGNIN_STATE_CHANGED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_SIGNIN_STATE_CHANGED"

.field public static final BROADCAST_TIME_DIFF_ERROR:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_TIME_DIFF_ERROR"

.field public static final EXTRA_SDK_SIGNIN_APP_LABEL:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.EXTRA_SDK_SIGNIN_APP_LABEL"

.field public static final EXTRA_SIGNIN_STATE_SIGNED_IN:Ljava/lang/String; = "SlinkSignInUtils.EXTRA_SIGNIN_STATE_SIGNED_IN"

.field private static final SAMSUNG_ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"

.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    .line 70
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 54
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 56
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    if-nez v0, :cond_1

    .line 57
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    .line 59
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public createSamsungLinkUsageConfirmationIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_CONFIRM_SAMSUNG_LINK_USAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public createSignInActivityIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 93
    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 94
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 95
    .local v3, "pm":Landroid/content/pm/PackageManager;
    if-eqz v3, :cond_0

    if-nez v0, :cond_1

    :cond_0
    const/4 v1, 0x0

    .line 97
    .local v1, "appLabel":Ljava/lang/CharSequence;
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SDK_SIGNIN"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 98
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.EXTRA_SDK_SIGNIN_APP_LABEL"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 100
    return-object v2

    .line 95
    .end local v1    # "appLabel":Ljava/lang/CharSequence;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method

.method public getAuthInfo()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 148
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 149
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 150
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAuthInfo.NAME"

    .line 148
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 153
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getPlatformUpgradeIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.ACTION_SAMSUNG_LINK_PLATFORM_UPGARDE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public getSamsungAccountSignInIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 133
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 134
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 135
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSamsungAccountSignInIntent.NAME"

    .line 133
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 139
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSamsungAccountSignInIntent.RESULT_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    return-object v1
.end method

.method public isSignedIn()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 78
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 79
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 80
    const-string v3, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.GetSignInStatus.NAME"

    .line 78
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 83
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public isUpgradeAvailable()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 162
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 163
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 164
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsUpgradeAvailable.NAME"

    .line 162
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 167
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public samsungAccountExists()Z
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public signIn()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 126
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 127
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SignIn.NAME"

    .line 125
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 130
    return-void
.end method
