.class public final Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;
.super Ljava/lang/Object;
.source "SlinkNetworkManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    }
.end annotation


# static fields
.field public static final BROADCAST_INITIALIZING_STATE_CHANGED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

.field public static final EXTRA_INITIALIZING_STATE_CONNECTED:Ljava/lang/String; = "SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

.field private static final NETWORK_LOCK_SERVICE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.NETWORK_LOCK_SERVICE"

.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->mContext:Landroid/content/Context;

    .line 37
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 50
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 52
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    if-nez v0, :cond_1

    .line 53
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    .line 55
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public createWakeLock(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 101
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;-><init>(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;Landroid/content/Context;Ljava/lang/String;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;)V

    return-object v0
.end method

.method public getScsCoreConfig()Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 111
    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 112
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 113
    const-string v4, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetScsCoreConfig.NAME"

    .line 111
    invoke-virtual {v2, v3, v4, v5, v5}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 117
    .local v0, "callResult":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 118
    .local v1, "config":Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;
    if-eqz v0, :cond_0

    .line 119
    const-class v2, Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 120
    const-string v2, "method_result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .end local v1    # "config":Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;
    check-cast v1, Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;

    .line 122
    .restart local v1    # "config":Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;
    :cond_0
    return-object v1
.end method

.method public isInitialized()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 131
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 132
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 133
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsInitialized.NAME"

    .line 131
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 136
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public requestRefresh()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 63
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 64
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.RequestNetworkRefresh.NAME"

    .line 62
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 67
    return-void
.end method

.method public requestRefresh(J)V
    .locals 5
    .param p1, "deviceId"    # J

    .prologue
    .line 76
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 77
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.RequestNetworkRefresh.INTENT_ARG_DEVICE_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 79
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 80
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    .line 81
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.RequestNetworkRefresh.NAME"

    .line 82
    const/4 v4, 0x0

    .line 79
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 84
    return-void
.end method
