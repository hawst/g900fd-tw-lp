.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
.super Ljava/lang/Object;
.source "SlinkImageRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private albumId:J

.field private localData:Ljava/lang/String;

.field private localSourceAlbumId:J

.field private localSourceMediaId:J

.field private maxHeight:I

.field private maxWidth:I

.field private mediaType:I

.field private orientation:I

.field private priority:I

.field private rowId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 220
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 231
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 209
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 210
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 211
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 212
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 215
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 216
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 218
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)V
    .locals 2
    .param p1, "original"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 104
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 105
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 106
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 107
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 108
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 109
    iget-object v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 110
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 111
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 112
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 113
    return-void
.end method

.method public static createFromCursor(Landroid/database/Cursor;III)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .locals 12
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I
    .param p3, "priority"    # I

    .prologue
    const-wide/16 v10, 0x0

    .line 35
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>()V

    .line 36
    .local v0, "imageRequest":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    iput p1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 37
    iput p2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 38
    iput p3, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 41
    const-string v8, "_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 40
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 44
    const-string v8, "media_type"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 43
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    iput v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 46
    iget v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 48
    const-string v8, "orientation"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 47
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    iput v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 52
    :cond_0
    const-string v8, "local_source_media_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 53
    .local v5, "localSourceMediaIdColumnIndex":I
    if-ltz v5, :cond_1

    .line 54
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 55
    .local v6, "localSourceMediaId":J
    cmp-long v8, v6, v10

    if-lez v8, :cond_1

    .line 56
    iput-wide v6, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 60
    .end local v6    # "localSourceMediaId":J
    :cond_1
    const-string v8, "local_data"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 61
    .local v1, "localDataColumnIndex":I
    if-ltz v1, :cond_2

    .line 62
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 65
    :cond_2
    iget v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/4 v9, 0x2

    if-eq v8, v9, :cond_3

    .line 66
    iget v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/16 v9, 0xd

    if-eq v8, v9, :cond_3

    .line 67
    iget v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/16 v9, 0xe

    if-ne v8, v9, :cond_4

    .line 70
    :cond_3
    const-string v8, "album_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 69
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 73
    const-string v8, "local_source_album_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 74
    .local v4, "localSourceAlbumIdColumnIndex":I
    if-ltz v4, :cond_4

    .line 75
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 76
    .local v2, "localSourceAlbumId":J
    cmp-long v8, v2, v10

    if-lez v8, :cond_4

    .line 77
    iput-wide v2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 82
    .end local v2    # "localSourceAlbumId":J
    .end local v4    # "localSourceAlbumIdColumnIndex":I
    :cond_4
    return-object v0
.end method

.method public static createFromRequest(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;III)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .locals 1
    .param p0, "request"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I
    .param p3, "priority"    # I

    .prologue
    .line 91
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)V

    .line 92
    .local v0, "imageRequest":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    iput p1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 93
    iput p2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 94
    iput p3, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 96
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 152
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    return-wide v0
.end method

.method public getLocalData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalSourceAlbumId()J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    return-wide v0
.end method

.method public getLocalSourceMediaId()J
    .locals 2

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    return-wide v0
.end method

.method public getMaxHeight()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    return v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    return v0
.end method

.method public getRowId()J
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SlinkImageRequest [rowId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 178
    iget-wide v2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 179
    const-string v1, ", maxWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 180
    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 181
    const-string v1, ", maxHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 182
    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 183
    const-string v1, ", priority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 184
    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 185
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 195
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 196
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 197
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 198
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 199
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 200
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 201
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 202
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 203
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 204
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 205
    return-void
.end method
