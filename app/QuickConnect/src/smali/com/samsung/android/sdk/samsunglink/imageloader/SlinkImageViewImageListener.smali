.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;
.super Ljava/lang/Object;
.source "SlinkImageViewImageListener.java"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;


# instance fields
.field private final mErrorResource:I

.field private mImageLoaded:Z

.field private final mImageView:Landroid/widget/ImageView;

.field private final mLoadingResource:I


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;II)V
    .locals 0
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "loadingResource"    # I
    .param p3, "errorResource"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    .line 35
    iput p2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mLoadingResource:I

    .line 36
    iput p3, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mErrorResource:I

    .line 37
    return-void
.end method

.method private hasImageLoaded()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageLoaded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onImageFailedToLoad()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->hasImageLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageLoaded:Z

    .line 82
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mErrorResource:I

    if-eqz v0, :cond_0

    .line 83
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mErrorResource:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->setImageResource(I)V

    .line 86
    :cond_0
    return-void
.end method

.method private setImageAndOrientation(Landroid/graphics/Bitmap;I)V
    .locals 3
    .param p1, "image"    # Landroid/graphics/Bitmap;
    .param p2, "orientation"    # I

    .prologue
    .line 93
    if-nez p2, :cond_0

    .line 94
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 99
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageLoaded:Z

    .line 100
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    .line 97
    invoke-virtual {v2}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkRotateBitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)V

    .line 96
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageLoaded:Z

    .line 76
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    return-void
.end method

.method private updateImageContainer(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V
    .locals 2
    .param p1, "response"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    .prologue
    .line 62
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    .line 63
    .local v0, "oldImageContainer":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->cancelRequest()V

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 72
    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V
    .locals 0
    .param p1, "response"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->updateImageContainer(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V

    .line 58
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->onImageFailedToLoad()V

    .line 59
    return-void
.end method

.method public onResponse(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;Z)V
    .locals 2
    .param p1, "response"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
    .param p2, "isImmediate"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->updateImageContainer(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V

    .line 43
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    .line 44
    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->mLoadingResource:I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->setImageResource(I)V

    .line 52
    :goto_0
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    .line 47
    .local v0, "orientation":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getRequest()Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 48
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getRequest()Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->getOrientation()I

    move-result v0

    .line 50
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageViewImageListener;->setImageAndOrientation(Landroid/graphics/Bitmap;I)V

    goto :goto_0
.end method
