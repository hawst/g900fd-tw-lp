.class public Lcom/samsung/android/sdk/samsunglink/SlinkLaunchUtils;
.super Ljava/lang/Object;
.source "SlinkLaunchUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/samsunglink/SlinkLaunchUtils$LaunchToContentTypes;
    }
.end annotation


# static fields
.field public static final BROADCAST_SAMSUNG_LINK_STARTED:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.BROADCAST_SAMSUNG_LINK_STARTED"

.field public static final EXTRA_LAUNCH_TO_CONTENT_TYPE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_CONTENT_TYPE"

.field public static final EXTRA_LAUNCH_TO_DEVICE_ID:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_DEVICE_ID"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static createLaunchToDeviceIntent(JJ)Landroid/content/Intent;
    .locals 2
    .param p0, "deviceId"    # J
    .param p2, "initialContentType"    # J

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.ui.MobileChargesNotificationActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 60
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_DEVICE_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 61
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.EXTRA_LAUNCH_TO_CONTENT_TYPE"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 63
    return-object v0
.end method

.method public static createLaunchToSearchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "searchQuery"    # Ljava/lang/String;

    .prologue
    .line 67
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    const-string v1, "query"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    :cond_0
    return-object v0
.end method
