.class public Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Settings;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# static fields
.field public static final VIDEO_OPTIMIZATION:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.VIDEO_OPTIMIZATION"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 5
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "settingName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    .prologue
    .line 3686
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSetting.NAME"

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, p1, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 3688
    .local v0, "result":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 3689
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3690
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3691
    const-string v4, " not found in Samsung Link."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3689
    invoke-direct {v2, v3}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3694
    :cond_0
    invoke-static {p1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Settings;->getSettingTypeFromBundle(Ljava/lang/String;Landroid/os/Bundle;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    move-result-object v1

    .line 3696
    .local v1, "type":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->BOOLEAN:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;

    if-ne v1, v2, :cond_1

    .line 3697
    const-string v2, "method_result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    return v2

    .line 3698
    :cond_1
    if-nez v1, :cond_2

    .line 3699
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    .line 3700
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Internal error: Missing or unparsable type for setting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3701
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3702
    const-string v4, " in Samsung Link."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3700
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3699
    invoke-direct {v2, v3}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3704
    :cond_2
    new-instance v2, Landroid/provider/Settings$SettingNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Samsung Link setting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3705
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3706
    const-string v4, " is type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3707
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3708
    const-string v4, ", not a boolean."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3704
    invoke-direct {v2, v3}, Landroid/provider/Settings$SettingNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static getSettingTypeFromBundle(Ljava/lang/String;Landroid/os/Bundle;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;
    .locals 6
    .param p0, "settingName"    # Ljava/lang/String;
    .param p1, "result"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3715
    const-string v3, "method_result_str"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3716
    .local v1, "typeStr":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 3730
    :cond_0
    :goto_0
    return-object v2

    .line 3721
    :cond_1
    :try_start_0
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;->valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetSetting$SettingType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 3722
    :catch_0
    move-exception v0

    .line 3723
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-boolean v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v3, :cond_0

    .line 3724
    # getter for: Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t parse type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3725
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 3726
    const-string v5, " for setting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 3727
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 3728
    const-string v5, " in Samsung Link."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3724
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
