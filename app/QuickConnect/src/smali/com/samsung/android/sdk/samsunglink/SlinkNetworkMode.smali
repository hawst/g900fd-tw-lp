.class public final enum Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
.super Ljava/lang/Enum;
.source "SlinkNetworkMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

.field public static final enum MOBILE_2G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

.field public static final enum MOBILE_3G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

.field public static final enum MOBILE_LTE:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

.field public static final enum OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

.field private static final TAG:Ljava/lang/String;

.field public static final enum UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

.field public static final enum WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;-><init>(Ljava/lang/String;I)V

    .line 18
    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;-><init>(Ljava/lang/String;I)V

    .line 23
    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    const-string v1, "MOBILE_2G"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;-><init>(Ljava/lang/String;I)V

    .line 28
    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_2G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 30
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    const-string v1, "MOBILE_3G"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;-><init>(Ljava/lang/String;I)V

    .line 33
    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_3G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 35
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    const-string v1, "MOBILE_LTE"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;-><init>(Ljava/lang/String;I)V

    .line 38
    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_LTE:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 40
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;-><init>(Ljava/lang/String;I)V

    .line 43
    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_2G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_3G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_LTE:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->ENUM$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 45
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getNetworkMode(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    .locals 6
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 58
    const-string v3, "network_mode"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 59
    .local v0, "columnIndex":I
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "value":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 61
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 68
    :goto_0
    return-object v3

    .line 65
    :cond_0
    :try_start_0
    invoke-static {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 66
    :catch_0
    move-exception v1

    .line 67
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unrecognized value for network mode: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 68
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->ENUM$VALUES:[Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public toContentValues(Landroid/content/ContentValues;)V
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 79
    const-string v0, "network_mode"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void
.end method
