.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;
.super Ljava/lang/Object;
.source "SlinkBitmapInfo.java"


# instance fields
.field private final bitmap:Landroid/graphics/Bitmap;

.field private final requestedMaxHeight:I

.field private final requestedMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;II)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "requestedMaxWidth"    # I
    .param p3, "requestedMaxHeight"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 20
    iput p2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->requestedMaxWidth:I

    .line 21
    iput p3, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->requestedMaxHeight:I

    .line 22
    return-void
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getRequestedMaxHeight()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->requestedMaxHeight:I

    return v0
.end method

.method public getRequestedMaxWidth()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkBitmapInfo;->requestedMaxWidth:I

    return v0
.end method
