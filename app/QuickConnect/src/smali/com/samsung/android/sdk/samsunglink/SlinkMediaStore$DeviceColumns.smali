.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$DeviceColumns;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceColumns"
.end annotation


# static fields
.field public static final ALIAS_NAME:Ljava/lang/String; = "alias_name"

.field public static final DEVICE_MODEL_ID:Ljava/lang/String; = "model_id"

.field public static final DEVICE_MODEL_NAME:Ljava/lang/String; = "model_name"

.field public static final DEVICE_PRIORITY:Ljava/lang/String; = "device_priority"

.field public static final IS_ON_LOCAL_NETWORK:Ljava/lang/String; = "is_on_local_network"

.field public static final IS_SYNCING:Ljava/lang/String; = "is_syncing"

.field public static final LOCAL_IP_ADDRESS:Ljava/lang/String; = "local_ip_address"

.field public static final NETWORK_MODE:Ljava/lang/String; = "network_mode"

.field public static final PHYSICAL_TYPE:Ljava/lang/String; = "physical_type"

.field public static final TRANSPORT_TYPE:Ljava/lang/String; = "transport_type"
