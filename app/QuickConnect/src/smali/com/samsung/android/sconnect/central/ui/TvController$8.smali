.class Lcom/samsung/android/sconnect/central/ui/TvController$8;
.super Landroid/content/BroadcastReceiver;
.source "TvController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/TvController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/TvController;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V
    .locals 0

    .prologue
    .line 1039
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1042
    if-nez p2, :cond_1

    .line 1171
    :cond_0
    :goto_0
    return-void

    .line 1045
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 1046
    .local v2, "action":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/TvController$8;->isInitialStickyBroadcast()Z

    move-result v18

    if-nez v18, :cond_0

    .line 1049
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    if-eqz v2, :cond_0

    .line 1051
    const-string v18, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.ROOM_CREATED"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 1054
    const-string v18, "Room"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;

    .line 1055
    .local v4, "added_room":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    invoke-virtual {v4}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1056
    .end local v4    # "added_room":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    :cond_2
    const-string v18, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.ROOM_UPDATED"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1059
    const-string v18, "Room"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;

    .line 1060
    .local v16, "updated_room":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->getRoomId()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1063
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    const-string v20, "selected room is updated !!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1066
    .end local v16    # "updated_room":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    :cond_3
    const-string v18, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.ROOM_REMOVED"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 1069
    const-string v18, "RoomId"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1070
    .local v5, "deleted_room":Ljava/lang/String;
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1073
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    const-string v20, "selected room is deleted !!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1702(Lcom/samsung/android/sconnect/central/ui/TvController;Z)Z

    .line 1076
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v19

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->deleteTvOnDB(Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1800(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V

    .line 1077
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->showTvController(Z)V
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1400(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V

    goto/16 :goto_0

    .line 1080
    .end local v5    # "deleted_room":Ljava/lang/String;
    :cond_4
    const-string v18, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.DEVICE_CREATED"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 1083
    const-string v18, "RemoteControlSetting"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    .line 1085
    .local v3, "added_device":Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    const-string v18, "TV"

    invoke-virtual {v3}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getDeviceType()Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->name()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSetttingTv:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1900(Lcom/samsung/android/sconnect/central/ui/TvController;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1090
    const/16 v17, 0x0

    .line 1093
    .local v17, "watchonRoomList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$700(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-result-object v18

    if-eqz v18, :cond_5

    .line 1094
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$700(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;->getRoomList()Ljava/util/List;

    move-result-object v17

    .line 1095
    const-string v18, "TvController"

    const-string v19, "DEVICE_CREATED:check new DeviceList"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "mRoomList count : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1103
    :cond_5
    :goto_1
    if-eqz v17, :cond_6

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    if-nez v18, :cond_7

    .line 1104
    :cond_6
    const-string v18, "TvController"

    const-string v19, "DEVICE_CREATED"

    const-string v20, "mRoomList is null"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1099
    :catch_0
    move-exception v7

    .line 1100
    .local v7, "e":Ljava/lang/Exception;
    const-string v18, "TvController"

    const-string v19, "DEVICE_CREATED"

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1108
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_7
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_8
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;

    .line 1110
    .local v11, "room":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    const/4 v10, 0x0

    .line 1111
    .local v10, "remoteControlSettingList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;>;"
    :try_start_1
    invoke-virtual {v11}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->getRoomId()Ljava/lang/String;

    move-result-object v12

    .line 1112
    .local v12, "roomId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$700(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-result-object v18

    if-eqz v18, :cond_8

    .line 1113
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$700(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v0, v12}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;->getRemoteControlList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 1116
    if-eqz v10, :cond_a

    .line 1117
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_9
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    .line 1118
    .local v14, "settings":Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    invoke-virtual {v14}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getDeviceType()Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    move-result-object v18

    invoke-virtual {v3}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getDeviceType()Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-virtual {v14}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getDeviceId()I

    move-result v18

    invoke-virtual {v3}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getDeviceId()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 1122
    const-string v18, "TvController"

    const-string v19, "DEVICE_CREATED"

    const-string v20, "mRoomList found"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    new-instance v6, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    invoke-direct {v6, v11, v14}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;-><init>(Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;)V

    .line 1124
    .local v6, "device":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # setter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static {v0, v6}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1202(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .line 1125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1702(Lcom/samsung/android/sconnect/central/ui/TvController;Z)Z

    .line 1126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$400(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v20

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->registerTvOnDB(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1600(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 1134
    .end local v6    # "device":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v12    # "roomId":Ljava/lang/String;
    .end local v14    # "settings":Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    :catch_1
    move-exception v7

    .line 1135
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v18, "TvController"

    const-string v19, "DEVICE_CREATED"

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1130
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v12    # "roomId":Ljava/lang/String;
    :cond_a
    :try_start_2
    const-string v18, "TvController"

    const-string v19, "DEVICE_CREATED"

    const-string v20, "mRoomList No found"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 1142
    .end local v3    # "added_device":Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    .end local v10    # "remoteControlSettingList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;>;"
    .end local v11    # "room":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    .end local v12    # "roomId":Ljava/lang/String;
    .end local v17    # "watchonRoomList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;>;"
    :cond_b
    const-string v18, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.DEVICE_UPDATED"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 1144
    const-string v18, "RemoteControlSetting"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    .line 1146
    .local v15, "updated_device":Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual {v15}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getDeviceType()Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->name()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1151
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    const-string v20, "selected device is updated !!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1154
    .end local v15    # "updated_device":Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    :cond_c
    const-string v18, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.DEVICE_REMOVED"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1156
    const-string v18, "DeviceType"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1157
    .local v6, "device":Ljava/lang/String;
    const-string v18, "RoomId"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1158
    .local v13, "roomid":Ljava/lang/String;
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v13}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1163
    const-string v18, "TvController"

    const-string v19, "mWatchonReceiver"

    const-string v20, "selected device is deleted !!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1702(Lcom/samsung/android/sconnect/central/ui/TvController;Z)Z

    .line 1166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v19

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->deleteTvOnDB(Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1800(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V

    .line 1167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->showTvController(Z)V
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1400(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V

    goto/16 :goto_0
.end method
