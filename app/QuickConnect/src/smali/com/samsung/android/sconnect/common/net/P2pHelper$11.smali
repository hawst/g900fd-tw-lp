.class Lcom/samsung/android/sconnect/common/net/P2pHelper$11;
.super Landroid/content/BroadcastReceiver;
.source "P2pHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/P2pHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0

    .prologue
    .line 1004
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1007
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 1009
    .local v8, "action":Ljava/lang/String;
    const-string v2, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1010
    const-string v2, "wifi_p2p_state"

    const/4 v3, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v20

    .line 1011
    .local v20, "state":I
    const/4 v2, 0x2

    move/from16 v0, v20

    if-ne v0, v2, :cond_3

    .line 1012
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onEnabled(B)V

    .line 1013
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1014
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const-wide/32 v6, 0x9c40

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v6, v7, v3, v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startFindP2p(JZZ)V

    .line 1020
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-boolean v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoverP2p:Z

    if-eqz v2, :cond_1

    .line 1021
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1500(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z

    move-result v3

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->discoverP2p(Z)V
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)V

    .line 1195
    .end local v20    # "state":I
    :cond_1
    :goto_1
    return-void

    .line 1015
    .restart local v20    # "state":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-boolean v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNeedListening:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1016
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNeedListening:Z

    .line 1017
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const-wide/32 v6, 0x19a28

    const/4 v3, 0x0

    invoke-virtual {v2, v6, v7, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startListenP2p(JZ)V

    .line 1018
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x1

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeP2pConfirm(Z)V
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1400(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)V

    goto :goto_0

    .line 1023
    :cond_3
    const/4 v2, 0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_1

    .line 1024
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisabled(B)V

    goto :goto_1

    .line 1026
    .end local v20    # "state":I
    :cond_4
    const-string v2, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1027
    const-string v2, "wifiP2pDeviceList"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v17

    check-cast v17, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .line 1029
    .local v17, "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    const-string v2, "connectedDevAddress"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1032
    .local v10, "connectedDevAddr":Ljava/lang/String;
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI_P2P_PEERS_CHANGED_ACTION : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    if-eqz v10, :cond_8

    .line 1035
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->get(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v9

    .line 1036
    .local v9, "changedDevice":Landroid/net/wifi/p2p/WifiP2pDevice;
    if-eqz v9, :cond_b

    .line 1037
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI_P2P_PEERS_CHANGED_ACTION : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " , "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v6, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1040
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI_P2P_PEERS_CHANGED_ACTION mRequestList.remove : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1044
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1045
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    const v4, 0x9c40

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v6, v6, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    mul-int/2addr v4, v6

    int-to-long v6, v4

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1048
    :cond_5
    iget v2, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    .line 1049
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    if-nez v2, :cond_6

    .line 1053
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    const-string v4, "WIFI_P2P_PEERS_CHANGED_ACTION : WifiP2pDevice.FAILED, iGroupOwner -no client : removeGroup"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1700(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 1057
    :cond_6
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3, v10}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisconnected(BLjava/lang/String;)V

    .line 1059
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceRemove(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    invoke-static {v2, v9}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1900(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)V

    .line 1062
    :cond_7
    iget v2, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-nez v2, :cond_a

    .line 1063
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3, v10}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onConnected(BLjava/lang/String;)V

    .line 1064
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x1

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceAdd(Landroid/net/wifi/p2p/WifiP2pDevice;Z)V
    invoke-static {v2, v9, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2000(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;Z)V

    .line 1082
    .end local v9    # "changedDevice":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2100(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1083
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestPeerList()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$400(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    goto/16 :goto_1

    .line 1065
    .restart local v9    # "changedDevice":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_a
    iget v2, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_8

    .line 1066
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3, v10}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisconnected(BLjava/lang/String;)V

    .line 1068
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x0

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceAdd(Landroid/net/wifi/p2p/WifiP2pDevice;Z)V
    invoke-static {v2, v9, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2000(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;Z)V

    goto :goto_2

    .line 1072
    :cond_b
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI_P2P_PEERS_CHANGED_ACTION : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " ,no device"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3, v10}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisconnected(BLjava/lang/String;)V

    .line 1077
    new-instance v16, Landroid/net/wifi/p2p/WifiP2pDevice;

    invoke-direct/range {v16 .. v16}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>()V

    .line 1078
    .local v16, "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    move-object/from16 v0, v16

    iput-object v10, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    .line 1079
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-object/from16 v0, v16

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceRemove(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    invoke-static {v2, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1900(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)V

    goto :goto_2

    .line 1085
    .end local v9    # "changedDevice":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v10    # "connectedDevAddr":Ljava/lang/String;
    .end local v16    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v17    # "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    :cond_c
    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 1088
    const-string v2, "connectedDevAddress"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1091
    .restart local v10    # "connectedDevAddr":Ljava/lang/String;
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI_P2P_CONNECTION_CHANGED_ACTION : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    if-eqz v10, :cond_d

    .line 1094
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI_P2P_CONNECTION_CHANGED_ACTION : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1096
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1097
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI_P2P_CONNECTION_CHANGED_ACTION mRequestList.remove : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    const v4, 0x9c40

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v6, v6, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    mul-int/2addr v4, v6

    int-to-long v6, v4

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1107
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestGroupInfo()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2200(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    .line 1109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pEnabled()Z

    move-result v2

    if-nez v2, :cond_e

    .line 1110
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    const-string v4, "no p2pInfoWifi...remove all wifi device."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1115
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pConnected()Z

    move-result v20

    .line 1116
    .local v20, "state":Z
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI_P2P_CONNECTION_CHANGED_ACTION :"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    if-eqz v20, :cond_f

    .line 1118
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3, v10}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onConnected(BLjava/lang/String;)V

    .line 1150
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$102(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)Z

    .line 1152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 1120
    :cond_f
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3, v10}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisconnected(BLjava/lang/String;)V

    .line 1121
    if-eqz v10, :cond_10

    .line 1122
    new-instance v16, Landroid/net/wifi/p2p/WifiP2pDevice;

    invoke-direct/range {v16 .. v16}, Landroid/net/wifi/p2p/WifiP2pDevice;-><init>()V

    .line 1123
    .restart local v16    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    move-object/from16 v0, v16

    iput-object v10, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    .line 1124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-object/from16 v0, v16

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceRemove(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    invoke-static {v2, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1900(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)V

    goto :goto_3

    .line 1126
    .end local v16    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 1127
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2100(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 1128
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/ArrayList;

    .line 1130
    .local v15, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_11
    :goto_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    .line 1131
    .local v12, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    iget-boolean v2, v12, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    const/4 v4, 0x1

    if-ne v2, v4, :cond_11

    .line 1132
    const-string v2, "P2pHelper"

    const-string v4, "onReceive"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WIFI_P2P_CONNECTION_CHANGED_ACTION REMOVE: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v12, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/TYPE("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v12, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")/P2p("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v12, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")/WFD("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v12, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")/MyGroup("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v12, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v4, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    new-instance v19, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$000(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 1141
    .local v19, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;)V

    .line 1142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2100(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 1143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 1147
    .end local v12    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;>;"
    .end local v19    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_12
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    .line 1154
    .end local v10    # "connectedDevAddr":Ljava/lang/String;
    .end local v20    # "state":Z
    :cond_13
    const-string v2, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1155
    const-string v2, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v21

    check-cast v21, Landroid/hardware/display/WifiDisplayStatus;

    .line 1157
    .local v21, "status":Landroid/hardware/display/WifiDisplayStatus;
    if-eqz v21, :cond_1

    .line 1158
    invoke-virtual/range {v21 .. v21}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v22

    .line 1159
    .local v22, "wfdStatus":I
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_WIFI_DISPLAY_STATUS_CHANGED :"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const/4 v2, 0x4

    move/from16 v0, v22

    if-eq v0, v2, :cond_14

    const/4 v2, 0x2

    move/from16 v0, v22

    if-ne v0, v2, :cond_1

    .line 1162
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$700(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$700(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$702(Lcom/samsung/android/sconnect/common/net/P2pHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 1165
    const/4 v2, 0x4

    move/from16 v0, v22

    if-ne v0, v2, :cond_1

    .line 1166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->turnOffWfdSetting()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2400(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    goto/16 :goto_1

    .line 1171
    .end local v21    # "status":Landroid/hardware/display/WifiDisplayStatus;
    .end local v22    # "wfdStatus":I
    :cond_15
    const-string v2, "android.net.wifi.p2p.SCONNECT_PROBE_REQ"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1172
    const-string v2, "probeReq"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1173
    .local v18, "reqData":Ljava/lang/String;
    if-eqz v18, :cond_1

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x99

    if-ne v2, v3, :cond_1

    .line 1174
    const/4 v5, 0x0

    .line 1175
    .local v5, "addr":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1177
    .local v11, "data":Ljava/lang/String;
    const/16 v2, 0x17

    const/16 v3, 0x28

    :try_start_2
    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1178
    const/16 v2, 0x2b

    const/16 v3, 0x99

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v11

    .line 1182
    :goto_5
    if-eqz v5, :cond_1

    if-eqz v11, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x11

    if-ne v2, v3, :cond_1

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x6e

    if-ne v2, v3, :cond_1

    .line 1183
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    const-string v4, "receive SCONNECT_PROBE_REQ"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2500(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2500(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v11}, Lcom/samsung/android/sconnect/common/util/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;->onReceived(ILjava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V

    goto/16 :goto_1

    .line 1179
    :catch_0
    move-exception v13

    .line 1180
    .local v13, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    const-string v4, "IndexOutOfBoundsException"

    invoke-static {v2, v3, v4, v13}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 1190
    .end local v5    # "addr":Ljava/lang/String;
    .end local v11    # "data":Ljava/lang/String;
    .end local v13    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v18    # "reqData":Ljava/lang/String;
    :cond_16
    const-string v2, "com.android.settings.wifi.p2p.SETTINGS_STRATED"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const-string v3, "started"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    # setter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2602(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)Z

    .line 1192
    const-string v2, "P2pHelper"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isRunningWifiDirectSetting : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2600(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
