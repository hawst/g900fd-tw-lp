.class Lcom/samsung/android/sconnect/SconnectManager$4;
.super Ljava/lang/Object;
.source "SconnectManager.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/SconnectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/SconnectManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/SconnectManager;)V
    .locals 0

    .prologue
    .line 847
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    .line 851
    if-eqz p1, :cond_0

    .line 852
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getUpnpDeviceType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 855
    const-string v0, "SconnectManager"

    const-string v1, "mDiscoveryListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ignore onDeviceAdded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_0
    :goto_0
    return-void

    .line 858
    :cond_1
    const-string v0, "SconnectManager"

    const-string v1, "mDiscoveryListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeviceAdded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/sconnect/SconnectManager;->updateScanList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I
    invoke-static {v0, p1, v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$900(Lcom/samsung/android/sconnect/SconnectManager;Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I

    goto :goto_0
.end method

.method public onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    .line 865
    if-eqz p1, :cond_0

    .line 866
    const-string v0, "SconnectManager"

    const-string v1, "mDiscoveryListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeviceRemoved: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    const/4 v1, 0x2

    # invokes: Lcom/samsung/android/sconnect/SconnectManager;->updateScanList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I
    invoke-static {v0, p1, v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$900(Lcom/samsung/android/sconnect/SconnectManager;Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I

    .line 869
    :cond_0
    return-void
.end method

.method public onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    .line 873
    if-eqz p1, :cond_0

    .line 874
    const-string v0, "SconnectManager"

    const-string v1, "mDiscoveryListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeviceUpdated: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    const/4 v1, 0x3

    # invokes: Lcom/samsung/android/sconnect/SconnectManager;->updateScanList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I
    invoke-static {v0, p1, v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$900(Lcom/samsung/android/sconnect/SconnectManager;Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I

    .line 878
    :cond_0
    return-void
.end method

.method public onDiscoveryFinished()V
    .locals 4

    .prologue
    .line 890
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1100(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 891
    const-string v0, "SconnectManager"

    const-string v1, "mDiscoveryListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDiscoveryFinished: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/SconnectManager;->access$1100(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 894
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDiscoveryFinished()V

    .line 896
    :cond_1
    return-void
.end method

.method public onDiscoveryStarted()V
    .locals 3

    .prologue
    .line 882
    const-string v0, "SconnectManager"

    const-string v1, "mDiscoveryListener"

    const-string v2, "onDiscoveryStarted"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 884
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$4;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDiscoveryStarted()V

    .line 886
    :cond_0
    return-void
.end method
