.class Lcom/samsung/android/sconnect/common/net/P2pHelper$7;
.super Ljava/lang/Object;
.source "P2pHelper.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/common/net/P2pHelper;->cancelP2pConnectRequest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0

    .prologue
    .line 653
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$7;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4
    .param p1, "arg0"    # I

    .prologue
    .line 656
    const-string v0, "P2pHelper"

    const-string v1, "cancelConnect"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFailure "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    return-void
.end method

.method public onSuccess()V
    .locals 3

    .prologue
    .line 661
    const-string v0, "P2pHelper"

    const-string v1, "cancelConnect"

    const-string v2, "onSuccess "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    return-void
.end method
