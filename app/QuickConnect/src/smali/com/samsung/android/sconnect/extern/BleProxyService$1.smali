.class Lcom/samsung/android/sconnect/extern/BleProxyService$1;
.super Lcom/samsung/android/sconnect/extern/IBleProxyService$Stub;
.source "BleProxyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/extern/BleProxyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/extern/BleProxyService;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/extern/BleProxyService;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/sconnect/extern/BleProxyService$1;->this$0:Lcom/samsung/android/sconnect/extern/BleProxyService;

    invoke-direct {p0}, Lcom/samsung/android/sconnect/extern/IBleProxyService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public registerCallback(Lcom/samsung/android/sconnect/extern/IBleProxyCallback;Ljava/lang/String;)Z
    .locals 5
    .param p1, "callback"    # Lcom/samsung/android/sconnect/extern/IBleProxyCallback;
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 50
    const-string v1, "BleProxyService"

    const-string v2, "registerCallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " [IBleProxyCallback]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "retVal":Z
    iget-object v1, p0, Lcom/samsung/android/sconnect/extern/BleProxyService$1;->this$0:Lcom/samsung/android/sconnect/extern/BleProxyService;

    # getter for: Lcom/samsung/android/sconnect/extern/BleProxyService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/extern/BleProxyService;->access$000(Lcom/samsung/android/sconnect/extern/BleProxyService;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->startPeriphService(Landroid/content/Context;Z)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/samsung/android/sconnect/extern/BleProxyService$1;->this$0:Lcom/samsung/android/sconnect/extern/BleProxyService;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/android/sconnect/extern/BleProxyService;->addBleProxyCallback(Lcom/samsung/android/sconnect/extern/IBleProxyCallback;Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x1

    .line 57
    :cond_0
    return v0
.end method

.method public unregisterCallback(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 62
    const-string v1, "BleProxyService"

    const-string v2, "unregisterCallback"

    invoke-static {v1, v2, p1}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "retVal":Z
    invoke-static {p1}, Lcom/samsung/android/sconnect/extern/BleProxyService;->removeBleProxyCallback(Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x1

    .line 67
    return v0
.end method
