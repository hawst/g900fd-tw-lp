.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 1489
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1493
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1640
    :cond_0
    :goto_0
    return-void

    .line 1495
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1496
    .local v5, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    const-string v1, "SconnectDisplay"

    const-string v2, "expandButton onClick"

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1499
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2302(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1500
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->expandActionList()V

    goto :goto_0

    .line 1501
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1502
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    goto :goto_0

    .line 1504
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 1505
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2302(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1506
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->expandActionList()V

    goto :goto_0

    .line 1511
    .end local v5    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->refreshDiscoveryDevice()V

    goto :goto_0

    .line 1514
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showSettingMenu()V

    goto :goto_0

    .line 1517
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 1518
    new-instance v16, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1519
    .local v16, "tmpView":Landroid/view/View;
    const v1, 0x7f0c0015

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 1520
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1521
    const/4 v1, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->setLabelFor(I)V

    .line 1522
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, v16

    invoke-interface {v1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1525
    .end local v16    # "tmpView":Landroid/view/View;
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1526
    .local v3, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContainsDRMContents(Ljava/util/ArrayList;)Z
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1528
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToSendDRMDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/util/ArrayList;III)V

    goto/16 :goto_0

    .line 1531
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->sendMultiple(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    invoke-static {v1, v3, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 1535
    .end local v3    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v1, v2, :cond_0

    .line 1536
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->endSelecteMode()V

    goto/16 :goto_0

    .line 1540
    :sswitch_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1542
    .restart local v5    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    if-nez v5, :cond_5

    .line 1543
    const-string v1, "SconnectDisplay"

    const-string v2, "device_layout onClick"

    const-string v4, "item is null"

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1546
    :cond_5
    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1547
    const-string v1, "SconnectDisplay"

    const-string v2, "device_layout onClick"

    const-string v4, "not searched device!"

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1548
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1549
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 1550
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2302(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    goto/16 :goto_0

    .line 1554
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-eq v1, v2, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLabelFor()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_d

    .line 1555
    :cond_7
    const-string v1, "SconnectDisplay"

    const-string v2, "device_layout onClick"

    const-string v4, "invoke primary action"

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1557
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1102(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1559
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_8

    .line 1560
    const-string v1, "SconnectDisplay"

    const-string v2, "*****TV device_layout onClick"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1565
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 1566
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2302(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    goto/16 :goto_0

    .line 1568
    :cond_9
    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v1

    if-nez v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContainsDRMContents(Ljava/util/ArrayList;)Z
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1570
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v7

    const/4 v8, 0x2

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v9

    invoke-virtual/range {v4 .. v9}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToSendDRMDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/util/ArrayList;III)V

    goto/16 :goto_0

    .line 1572
    :cond_a
    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_b

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v1

    if-nez v1, :cond_c

    .line 1574
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v4

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->sendWithCheckConnectionAndMax(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V
    invoke-static {v1, v5, v2, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V

    goto/16 :goto_0

    .line 1577
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v2

    invoke-virtual {v1, v5, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)V

    goto/16 :goto_0

    .line 1580
    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v1, v2, :cond_0

    .line 1581
    const-string v1, "SconnectDisplay"

    const-string v2, "device_layout onClick"

    const-string v4, "choice mode is multiple"

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1582
    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x7f0c001e

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    .line 1584
    .local v10, "checkbox":Landroid/widget/CheckBox;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v10, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    :cond_e
    const/4 v1, 0x0

    goto :goto_1

    .line 1588
    .end local v5    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v10    # "checkbox":Landroid/widget/CheckBox;
    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1589
    .local v14, "iconItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v14}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v11

    .line 1590
    .local v11, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getState()I

    move-result v1

    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_SENDING:I

    if-ne v1, v2, :cond_f

    .line 1591
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1, v14}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2902(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1592
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showStopDialog()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    goto/16 :goto_0

    .line 1594
    :cond_f
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_10

    .line 1595
    const-string v1, "SconnectDisplay"

    const-string v2, "device_icon onClick"

    const-string v4, "Together Room cannnot added to favorite"

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0901a7

    const/4 v4, 0x1

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1599
    :cond_10
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->hasMac()Z

    move-result v1

    if-nez v1, :cond_11

    .line 1600
    const-string v1, "SconnectDisplay"

    const-string v2, "device_icon onClick"

    const-string v4, "No mac address in device, cannot add to favorite"

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1602
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0901a7

    const/4 v4, 0x1

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1605
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    move-result-object v1

    invoke-virtual {v1, v14}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showFavoriteDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto/16 :goto_0

    .line 1611
    .end local v11    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v14    # "iconItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showTroubleShootingDialog()V

    goto/16 :goto_0

    .line 1614
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onBackPressed()V

    goto/16 :goto_0

    .line 1617
    :sswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1618
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 1619
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2302(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    goto/16 :goto_0

    .line 1623
    :sswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v1

    if-nez v1, :cond_12

    const/4 v1, 0x1

    :goto_2
    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z
    invoke-static {v2, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3102(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    .line 1624
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshDeviceListingMode()V

    .line 1625
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 1626
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHidedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1627
    .local v12, "hidedItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    const-string v1, "SconnectDisplay"

    const-string v2, "mode changed"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add hided item: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1629
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/16 v2, 0xbb8

    invoke-virtual {v12}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    goto :goto_3

    .line 1623
    .end local v12    # "hidedItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_12
    const/4 v1, 0x0

    goto :goto_2

    .line 1632
    :cond_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHidedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1633
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .restart local v13    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1634
    .local v15, "refreshItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/16 v2, 0xbba

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    goto :goto_4

    .line 1493
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c0015 -> :sswitch_5
        0x7f0c0018 -> :sswitch_6
        0x7f0c001d -> :sswitch_0
        0x7f0c0020 -> :sswitch_8
        0x7f0c0024 -> :sswitch_1
        0x7f0c0026 -> :sswitch_4
        0x7f0c0027 -> :sswitch_2
        0x7f0c0028 -> :sswitch_3
        0x7f0c002b -> :sswitch_9
        0x7f0c0030 -> :sswitch_a
        0x7f0c006d -> :sswitch_7
        0x7f0c006e -> :sswitch_7
    .end sparse-switch
.end method
