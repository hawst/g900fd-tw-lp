.class Lcom/samsung/android/sconnect/central/ui/ViewManager$4;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/ViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$4;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 324
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$4;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$300(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 326
    .local v1, "toast":Landroid/widget/Toast;
    const/4 v4, 0x2

    new-array v0, v4, [I

    .line 327
    .local v0, "pos":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 328
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$4;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mWidth:I
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$400(Lcom/samsung/android/sconnect/central/ui/ViewManager;)I

    move-result v4

    aget v5, v0, v7

    sub-int/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$4;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$500(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070040

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v2, v4

    .line 330
    .local v2, "x":I
    aget v4, v0, v8

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v3, v4, v5

    .line 332
    .local v3, "y":I
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/Util;->isLocaleRTL()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 333
    aget v2, v0, v7

    .line 336
    :cond_0
    const-string v4, "ViewManager"

    const-string v5, "onLongClick"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const v4, 0x800035

    invoke-virtual {v1, v4, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 338
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 339
    return v8
.end method
