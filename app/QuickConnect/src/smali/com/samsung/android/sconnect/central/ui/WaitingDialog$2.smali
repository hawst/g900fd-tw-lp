.class Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;
.super Landroid/content/BroadcastReceiver;
.source "WaitingDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/WaitingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V
    .locals 0

    .prologue
    .line 595
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 598
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 599
    .local v1, "action":Ljava/lang/String;
    const-string v9, "FINISH"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 601
    .local v8, "isFinish":Z
    if-eqz v8, :cond_0

    .line 602
    const-string v9, "WaitingDialog"

    const-string v10, "mLocalReceiver"

    const-string v11, "Waiting popup should be close without any action"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-virtual {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    .line 606
    :cond_0
    const-string v9, "com.samsung.android.sconnect.central.REQUEST_RESULT"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 607
    const-string v9, "KEY"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 608
    .local v6, "id":Ljava/lang/String;
    const-string v9, "ADDRESS"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 609
    .local v3, "deviceAddress":Ljava/lang/String;
    const-string v9, "RES"

    const/4 v10, 0x1

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 610
    .local v0, "accept":Z
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mId:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$200(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 611
    const-string v9, "WaitingDialog"

    const-string v10, "mLocalReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ACTION_C_REQUEST_RESULT - "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mAction:I
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$000(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)I

    move-result v9

    const/16 v10, 0x11

    if-eq v9, v10, :cond_1

    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mAction:I
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$000(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)I

    move-result v9

    const/16 v10, 0x10

    if-ne v9, v10, :cond_7

    .line 615
    :cond_1
    const/4 v4, 0x1

    .line 616
    .local v4, "done":Z
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 617
    .local v7, "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget-object v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-eqz v9, :cond_3

    iget-object v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 618
    const-string v9, "WaitingDialog"

    const-string v10, "mLocalReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Response from "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " -"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    if-eqz v0, :cond_4

    .line 621
    const/4 v9, 0x3

    iput v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 627
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateState(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    invoke-static {v9, v7}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$500(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    .line 629
    :cond_3
    iget v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-nez v9, :cond_2

    .line 630
    const/4 v4, 0x0

    goto :goto_0

    .line 623
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$400(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0900b5

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 625
    const/4 v9, 0x2

    iput v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto :goto_1

    .line 633
    .end local v7    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_5
    if-eqz v4, :cond_6

    .line 634
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->waitingAnswerTimeout()V
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$100(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V

    .line 691
    .end local v0    # "accept":Z
    .end local v3    # "deviceAddress":Ljava/lang/String;
    .end local v4    # "done":Z
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "id":Ljava/lang/String;
    :cond_6
    :goto_2
    return-void

    .line 637
    .restart local v0    # "accept":Z
    .restart local v3    # "deviceAddress":Ljava/lang/String;
    .restart local v6    # "id":Ljava/lang/String;
    :cond_7
    const/4 v4, 0x1

    .line 638
    .restart local v4    # "done":Z
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_8
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 639
    .restart local v7    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget-object v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-eqz v9, :cond_9

    iget-object v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 640
    const-string v9, "WaitingDialog"

    const-string v10, "mLocalReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Response from "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " -"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    if-eqz v0, :cond_a

    .line 643
    const/4 v9, 0x1

    iput v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 644
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateConnectedDevice(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    invoke-static {v9, v7}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$600(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    .line 650
    :goto_4
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateState(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    invoke-static {v9, v7}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$500(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    .line 652
    :cond_9
    iget v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-nez v9, :cond_8

    .line 653
    const/4 v4, 0x0

    goto :goto_3

    .line 646
    :cond_a
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$400(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0900b5

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 648
    const/4 v9, 0x2

    iput v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto :goto_4

    .line 656
    .end local v7    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_b
    if-eqz v4, :cond_6

    .line 657
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->waitingAnswerTimeout()V
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$100(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V

    goto :goto_2

    .line 660
    .end local v4    # "done":Z
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_c
    const-string v9, "CANCEL"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 661
    if-nez v0, :cond_6

    .line 662
    if-eqz v3, :cond_e

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_e

    .line 663
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_12

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 664
    .restart local v7    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget-object v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-eqz v9, :cond_d

    iget-object v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 665
    const/4 v9, 0x2

    iput v9, v7, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto/16 :goto_2

    .line 670
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_e
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_f
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_12

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 671
    .local v2, "device":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget v9, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-nez v9, :cond_10

    iget v9, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v9, v9, 0x8

    if-eqz v9, :cond_11

    :cond_10
    iget v9, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_f

    iget v9, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v9, v9, 0x8

    if-lez v9, :cond_f

    .line 673
    :cond_11
    const/4 v9, 0x2

    iput v9, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto :goto_5

    .line 677
    .end local v2    # "device":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_12
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v9

    const/16 v10, 0x7d0

    invoke-virtual {v9, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v9

    if-nez v9, :cond_6

    .line 679
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v9

    const/16 v10, 0x7d0

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    .line 684
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_13
    const-string v9, "WaitingDialog"

    const-string v10, "mLocalReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ACTION_C_REQUEST_RESULT ignore "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", current: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mId:Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$200(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 687
    .end local v0    # "accept":Z
    .end local v3    # "deviceAddress":Ljava/lang/String;
    .end local v6    # "id":Ljava/lang/String;
    :cond_14
    const-string v9, "com.samsung.android.sconnect.central.PLUGIN_PRINTER_CONNECTED"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 688
    const-string v9, "WaitingDialog"

    const-string v10, "mLocalReceiver"

    const-string v11, "PLUGIN PRINTER connected : auto cancel"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-virtual {v9}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    goto/16 :goto_2
.end method
