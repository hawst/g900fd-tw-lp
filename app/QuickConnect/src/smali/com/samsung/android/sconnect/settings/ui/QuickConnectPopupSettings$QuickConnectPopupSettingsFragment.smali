.class public Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "QuickConnectPopupSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QuickConnectPopupSettingsFragment"
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static TAG_CLASS:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mIsPopup:Z

.field private mPopupPrefScreen:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-string v0, "[SConnect] Popup Settings"

    sput-object v0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->TAG:Ljava/lang/String;

    .line 90
    const-string v0, "QuickConnectPopupSettings : "

    sput-object v0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->TAG_CLASS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mActivity:Landroid/app/Activity;

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mIsPopup:Z

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 129
    const-string v0, "quick_connect_popup_settings"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mPopupPrefScreen:Landroid/preference/CheckBoxPreference;

    .line 131
    # invokes: Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->getPopupPref()Z
    invoke-static {}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->access$000()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mIsPopup:Z

    .line 132
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    const/high16 v2, 0x7f050000

    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->addPreferencesFromResource(I)V

    .line 103
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mActivity:Landroid/app/Activity;

    .line 106
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mActivity:Landroid/app/Activity;

    instance-of v2, v2, Landroid/preference/PreferenceActivity;

    if-eqz v2, :cond_1

    .line 107
    iget-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mActivity:Landroid/app/Activity;

    check-cast v1, Landroid/preference/PreferenceActivity;

    .line 108
    .local v1, "preferenceActivity":Landroid/preference/PreferenceActivity;
    invoke-virtual {v1}, Landroid/preference/PreferenceActivity;->onIsHidingHeaders()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v2

    if-nez v2, :cond_1

    .line 110
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const v3, 0x7f09017e

    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 114
    iget-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/16 v3, 0x10

    const/16 v4, 0x10

    invoke-virtual {v2, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .end local v1    # "preferenceActivity":Landroid/preference/PreferenceActivity;
    :cond_1
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->TAG_CLASS:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "onCreate - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Exception Title : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 145
    sget-object v0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->TAG_CLASS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onPause - called"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 148
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mPopupPrefScreen:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mPopupPrefScreen:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 152
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 159
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 160
    .local v0, "key":Ljava/lang/String;
    sget-object v3, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->TAG_CLASS:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "onPreferenceChange - key ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    if-nez v0, :cond_0

    .line 175
    .end local p2    # "newValue":Ljava/lang/Object;
    :goto_0
    return v1

    .line 166
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_0
    const-string v3, "quick_connect_popup_settings"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 167
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mIsPopup:Z

    .line 168
    iget-object v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mPopupPrefScreen:Landroid/preference/CheckBoxPreference;

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mIsPopup:Z

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 169
    iget-boolean v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mIsPopup:Z

    if-eqz v3, :cond_2

    .line 170
    # invokes: Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->putPopupPref(Z)V
    invoke-static {v1}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->access$100(Z)V

    :cond_1
    :goto_1
    move v1, v2

    .line 175
    goto :goto_0

    .line 172
    :cond_2
    # invokes: Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->putPopupPref(Z)V
    invoke-static {v2}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->access$100(Z)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 137
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mPopupPrefScreen:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mPopupPrefScreen:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 139
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mPopupPrefScreen:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;->mIsPopup:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 141
    :cond_0
    return-void
.end method
