.class public Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
.super Ljava/lang/Object;
.source "BluetoothActionHelper.java"


# static fields
.field public static final A2DP:Ljava/lang/String; = "A2DP"

.field private static final BT_WORK_TIMEOUT:I = 0x7530

.field public static final HID:Ljava/lang/String; = "HID"

.field public static final HSP:Ljava/lang/String; = "HSP"

.field public static final STATE_BT_IDLE:I = 0x0

.field public static final STATE_BT_WORKING:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BluetoothActionHelper"

.field public static mState:I


# instance fields
.field private helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

.field private mA2dp:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBtBondReceiver:Landroid/content/BroadcastReceiver;

.field private mBtProfileReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field private mHeadset:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

.field private mHid:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

.field private mIsBtBondReceiver:Z

.field private mIsBtProfileReceiver:Z

.field private mIsReadyToConnect:Z

.field private mLocalUuids:[Landroid/os/ParcelUuid;

.field private mRemoteUuids:[Landroid/os/ParcelUuid;

.field private mTargetBtMacAddr:Ljava/lang/String;

.field private mTryingProfiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mState:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    .line 51
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mLocalUuids:[Landroid/os/ParcelUuid;

    .line 61
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mRemoteUuids:[Landroid/os/ParcelUuid;

    .line 62
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    .line 63
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mA2dp:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    .line 64
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHeadset:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHid:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    .line 328
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtBondReceiver:Z

    .line 329
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;-><init>(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBtBondReceiver:Landroid/content/BroadcastReceiver;

    .line 422
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtProfileReceiver:Z

    .line 423
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;-><init>(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBtProfileReceiver:Landroid/content/BroadcastReceiver;

    .line 516
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$3;

    const-wide/16 v2, 0x7530

    const-wide/16 v4, 0x2710

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$3;-><init>(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;JJ)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mCountDownTimer:Landroid/os/CountDownTimer;

    .line 68
    const-string v0, "BluetoothActionHelper"

    const-string v1, "BluetoothActionHelper"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    .line 71
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->initProfiles()V

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->unregisterBondReceiver()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->registerProfileReceiver(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->connectAllProfile(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mCountDownTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;[Landroid/os/ParcelUuid;)[Landroid/os/ParcelUuid;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    .param p1, "x1"    # [Landroid/os/ParcelUuid;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mRemoteUuids:[Landroid/os/ParcelUuid;

    return-object p1
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->unregisterProfileReceiver()V

    return-void
.end method

.method private connectAllProfile(Landroid/bluetooth/BluetoothDevice;)V
    .locals 10
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 105
    if-nez p1, :cond_0

    .line 106
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    const-string v8, "dev is null"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :goto_0
    return-void

    .line 110
    :cond_0
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mLocalUuids:[Landroid/os/ParcelUuid;

    if-nez v6, :cond_1

    .line 113
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    const-string v8, "mUuids is null. but keep going..."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mRemoteUuids:[Landroid/os/ParcelUuid;

    if-nez v6, :cond_2

    .line 118
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    const-string v8, "mRemoteUuids is null. but keep going..."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_2
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v3

    .line 122
    .local v3, "remoteUuids":[Landroid/os/ParcelUuid;
    if-nez v3, :cond_3

    .line 124
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    const-string v8, "remoteUuids is null. STOP!!!"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_3
    move-object v0, v3

    .local v0, "arr$":[Landroid/os/ParcelUuid;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_4

    aget-object v5, v0, v1

    .line 128
    .local v5, "uuid":Landroid/os/ParcelUuid;
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "remoteUuids has "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 131
    .end local v5    # "uuid":Landroid/os/ParcelUuid;
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 132
    sget-object v6, Landroid/bluetooth/BluetoothUuid;->HSP:Landroid/os/ParcelUuid;

    invoke-static {v3, v6}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v6

    if-nez v6, :cond_5

    sget-object v6, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    invoke-static {v3, v6}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 134
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHeadset:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    invoke-virtual {v6, p1, v7}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->connect(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v4

    .line 135
    .local v4, "ret":Z
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Headset connected in remoteUuids "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;

    const-string v7, "HSP"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    .end local v4    # "ret":Z
    :goto_2
    sget-object v6, Landroid/bluetooth/BluetoothUuid;->AudioSink:Landroid/os/ParcelUuid;

    invoke-static {v3, v6}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 142
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mA2dp:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    invoke-virtual {v6, p1, v7}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->connect(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v4

    .line 143
    .restart local v4    # "ret":Z
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "A2DP connected in remoteUuids "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;

    const-string v7, "A2DP"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    .end local v4    # "ret":Z
    :goto_3
    sget-object v6, Landroid/bluetooth/BluetoothUuid;->Hid:Landroid/os/ParcelUuid;

    invoke-static {v3, v6}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 150
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHid:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    invoke-virtual {v6, p1, v7}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->connect(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v4

    .line 151
    .restart local v4    # "ret":Z
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HID connected in remoteUuids "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;

    const-string v7, "HID"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 138
    .end local v4    # "ret":Z
    :cond_6
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    const-string v8, "no Headset in remoteUuids"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 146
    :cond_7
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    const-string v8, "no A2DP in remoteUuids"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 154
    :cond_8
    const-string v6, "BluetoothActionHelper"

    const-string v7, "connectAllProfile"

    const-string v8, "no HID in remoteUuids"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private disconnectAllProfile(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 159
    if-nez p1, :cond_1

    .line 160
    const-string v1, "BluetoothActionHelper"

    const-string v2, "disconnectAllProfile"

    const-string v3, "dev is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    const-string v1, "BluetoothActionHelper"

    const-string v2, "disconnectAllProfile"

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mA2dp:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->isConnected(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 167
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mA2dp:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->disconnect(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v0

    .line 168
    .local v0, "ret":Z
    const-string v1, "BluetoothActionHelper"

    const-string v2, "disconnectAllProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "A2DP disconnected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    .end local v0    # "ret":Z
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHeadset:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->isConnected(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 172
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHeadset:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->disconnect(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v0

    .line 173
    .restart local v0    # "ret":Z
    const-string v1, "BluetoothActionHelper"

    const-string v2, "disconnectAllProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Headset disconnected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .end local v0    # "ret":Z
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHid:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->isConnected(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHid:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->disconnect(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v0

    .line 178
    .restart local v0    # "ret":Z
    const-string v1, "BluetoothActionHelper"

    const-string v2, "disconnectAllProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HID disconnected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 6
    .param p0, "btMacAddr"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 221
    if-nez p0, :cond_0

    .line 222
    const-string v2, "BluetoothActionHelper"

    const-string v3, "getDevice"

    const-string v4, "btMacAddr is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :goto_0
    return-object v0

    .line 226
    :cond_0
    const-string v2, ""

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 227
    const-string v2, "BluetoothActionHelper"

    const-string v3, "getDevice"

    const-string v4, "btMacAddr is empty"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :cond_1
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "upperedBtMacAddr":Ljava/lang/String;
    invoke-static {v1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 233
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 239
    .local v0, "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    goto :goto_0

    .line 242
    .end local v0    # "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    const-string v2, "BluetoothActionHelper"

    const-string v3, "getDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " address NOT EXIST"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initProfiles()V
    .locals 3

    .prologue
    .line 80
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 81
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_4

    .line 82
    const-string v0, "BluetoothActionHelper"

    const-string v1, "initProfiles"

    const-string v2, "Device does not support Bluetooth"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mA2dp:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    if-nez v0, :cond_1

    .line 92
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mA2dp:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHeadset:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    if-nez v0, :cond_2

    .line 96
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHeadset:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHid:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    if-nez v0, :cond_3

    .line 100
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHid:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    .line 102
    :cond_3
    return-void

    .line 84
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mLocalUuids:[Landroid/os/ParcelUuid;

    goto :goto_0
.end method

.method private registerBondReceiver(Ljava/lang/String;)V
    .locals 6
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 394
    const-string v2, "BluetoothActionHelper"

    const-string v3, "registerBondReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIsBtBondReceiver = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtBondReceiver:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;

    .line 397
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtBondReceiver:Z

    if-nez v2, :cond_0

    .line 398
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 399
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 400
    const-string v2, "android.bluetooth.device.action.UUID"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 402
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBtBondReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 403
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtBondReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 404
    .restart local v1    # "intentFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "BluetoothActionHelper"

    const-string v3, "registerBondReceiver"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private registerProfileReceiver(Ljava/lang/String;)V
    .locals 6
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 487
    const-string v2, "BluetoothActionHelper"

    const-string v3, "registerProfileReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIsBtProfileReceiver = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtProfileReceiver:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;

    .line 490
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtProfileReceiver:Z

    if-nez v2, :cond_0

    .line 491
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 492
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 493
    const-string v2, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 494
    const-string v2, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 496
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBtProfileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 497
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtProfileReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 502
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 498
    .restart local v1    # "intentFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    .line 499
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "BluetoothActionHelper"

    const-string v3, "registerProfileReceiver"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setState(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 531
    const-string v0, "BluetoothActionHelper"

    const-string v1, "setState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    sput p1, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mState:I

    .line 534
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 535
    return-void
.end method

.method private unbondDevice(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 285
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->unbondDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 286
    .local v0, "ret":Z
    const-string v1, "BluetoothActionHelper"

    const-string v2, "unbondDeviceDev"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unbonding to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    return-void
.end method

.method private unbondDevice(Ljava/lang/String;)V
    .locals 5
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 290
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->unbondDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 291
    .local v0, "ret":Z
    const-string v1, "BluetoothActionHelper"

    const-string v2, "unbondDeviceAddr"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unbonding to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    return-void
.end method

.method private unregisterBondReceiver()V
    .locals 5

    .prologue
    .line 411
    const-string v1, "BluetoothActionHelper"

    const-string v2, "unregisterBondReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIsBtBondReceiver = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtBondReceiver:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtBondReceiver:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 414
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBtBondReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 415
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtBondReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 416
    :catch_0
    move-exception v0

    .line 417
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BluetoothActionHelper"

    const-string v2, "unregisterBondReceiver"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private unregisterProfileReceiver()V
    .locals 5

    .prologue
    .line 505
    const-string v1, "BluetoothActionHelper"

    const-string v2, "unregisterProfileReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIsBtProfileReceiver = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtProfileReceiver:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtProfileReceiver:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 508
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBtProfileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 509
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsBtProfileReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 510
    :catch_0
    move-exception v0

    .line 511
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BluetoothActionHelper"

    const-string v2, "unregisterProfileReceiver"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public bondAndConnectDevice(Ljava/lang/String;)V
    .locals 7
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 249
    const-string v2, "BluetoothActionHelper"

    const-string v3, "bondConnectDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getHowToControlBluetooth == "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v2

    if-nez v2, :cond_0

    .line 252
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/SconnectManager;->setHowToControlBluetooth(I)V

    .line 256
    :cond_0
    invoke-direct {p0, v6}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->setState(I)V

    .line 257
    const-string v2, "BluetoothActionHelper"

    const-string v3, "bondAndConnectDevice"

    const-string v4, "stopLeScan&cancelDiscovery + wait 1 second for better Bluetooth performance"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopScan(Z)V

    .line 260
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothHelper()Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->stopDiscovery()V

    .line 262
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->isBonded(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 268
    const-string v2, "BluetoothActionHelper"

    const-string v3, "bondConnectDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "already paired...so just connect to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->registerProfileReceiver(Ljava/lang/String;)V

    .line 270
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->connectAllProfile(Landroid/bluetooth/BluetoothDevice;)V

    .line 276
    :goto_1
    return-void

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "BluetoothActionHelper"

    const-string v3, "bondAndConnectDevice"

    const-string v4, "InterruptedException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 272
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->registerBondReceiver(Ljava/lang/String;)V

    .line 273
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->bondDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    .line 274
    .local v1, "ret":Z
    const-string v2, "BluetoothActionHelper"

    const-string v3, "bondConnectDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bonding to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public disconnectAndUnbondDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 279
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 280
    .local v0, "btDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->disconnectAllProfile(Landroid/bluetooth/BluetoothDevice;)V

    .line 281
    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->unbondDevice(Landroid/bluetooth/BluetoothDevice;)V

    .line 282
    return-void
.end method

.method public isBonded(Ljava/lang/String;)Z
    .locals 6
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 305
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    .line 306
    .local v1, "bondedList":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v1, :cond_1

    .line 307
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 308
    .local v0, "bondedDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 309
    const/4 v3, 0x1

    .line 315
    .end local v0    # "bondedDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_0
    return v3

    .line 313
    :cond_1
    const-string v3, "BluetoothActionHelper"

    const-string v4, "isBonded"

    const-string v5, "getBondedDevices() is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 8
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 183
    if-nez p1, :cond_0

    move v3, v4

    .line 217
    :goto_0
    return v3

    .line 188
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mA2dp:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    invoke-virtual {v3, p1, v6}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->isConnected(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 189
    const-string v3, "BluetoothActionHelper"

    const-string v4, "isConnectedDev"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mA2dp connected"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v5

    .line 190
    goto :goto_0

    .line 193
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHeadset:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    invoke-virtual {v3, p1, v6}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->isConnected(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 194
    const-string v3, "BluetoothActionHelper"

    const-string v4, "isConnectedDev"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mHeadset connected"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v5

    .line 195
    goto :goto_0

    .line 198
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->helper:Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mHid:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    invoke-virtual {v3, p1, v6}, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->isConnected(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 199
    const-string v3, "BluetoothActionHelper"

    const-string v4, "isConnectedDev"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mHid connected"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v5

    .line 200
    goto :goto_0

    .line 203
    :cond_3
    const/4 v2, 0x0

    .line 205
    .local v2, "methods":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "getGearIsConnected"

    const/4 v3, 0x0

    check-cast v3, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 206
    const/4 v3, 0x0

    check-cast v3, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 207
    .local v0, "connected":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 208
    const-string v3, "BluetoothActionHelper"

    const-string v6, "isConnectedDev"

    const-string v7, "Gear is connected"

    invoke-static {v3, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v5

    .line 209
    goto/16 :goto_0

    .line 211
    .end local v0    # "connected":Ljava/lang/Boolean;
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    move v3, v4

    .line 217
    goto/16 :goto_0
.end method

.method public isConnected(Ljava/lang/String;)Z
    .locals 1
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 319
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->isConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    const/4 v0, 0x1

    .line 324
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public terminte()V
    .locals 3

    .prologue
    .line 75
    const-string v0, "BluetoothActionHelper"

    const-string v1, "terminte"

    const-string v2, "nothing to do"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method
