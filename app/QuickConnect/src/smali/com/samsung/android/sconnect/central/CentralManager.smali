.class public Lcom/samsung/android/sconnect/central/CentralManager;
.super Ljava/lang/Object;
.source "CentralManager.java"


# static fields
.field static final TAG:Ljava/lang/String; = "CentralManager"


# instance fields
.field public mDeviceListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mGUIHandler:Landroid/os/Handler;

.field private mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Landroid/app/LoaderManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "guiHandler"    # Landroid/os/Handler;
    .param p3, "loaderManager"    # Landroid/app/LoaderManager;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    .line 74
    new-instance v0, Lcom/samsung/android/sconnect/central/CentralManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/CentralManager$1;-><init>(Lcom/samsung/android/sconnect/central/CentralManager;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mDeviceListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 37
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mGUIHandler:Landroid/os/Handler;

    .line 38
    invoke-static {p1}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    .line 39
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->createPreDiscoveryHelper()V

    .line 41
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p3}, Lcom/samsung/android/sconnect/SconnectManager;->setLoaderManager(Landroid/app/LoaderManager;)V

    .line 42
    return-void
.end method


# virtual methods
.method public cancelTransfer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mac"    # Ljava/lang/String;
    .param p2, "btMac"    # Ljava/lang/String;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sconnect/SconnectManager;->cancelTransfer(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method public checkConnectedDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->checkConnectedDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkEnabledBt()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->checkEnabledBt()Z

    move-result v0

    return v0
.end method

.method public checkEnabledWifi()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->checkEnabledWifi()Z

    move-result v0

    return v0
.end method

.method public checkMaxDirectDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->checkMaxDirectDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public doAction(Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/lang/String;I)V
    .locals 6
    .param p2, "action"    # I
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "contentsType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    .local p3, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/SconnectManager;->doActionForMultipleDevice(Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/lang/String;I)V

    .line 149
    return-void
.end method

.method public doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;)Z
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I
    .param p4, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 143
    .local p3, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    const/4 v5, 0x0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/SconnectManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "contentsType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "I)Z"
        }
    .end annotation

    .prologue
    .line 139
    .local p3, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/SconnectManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;ILandroid/content/Intent;)Z
    .locals 7
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "contentsType"    # I
    .param p6, "passingIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Landroid/content/Intent;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 133
    .local p3, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sconnect/SconnectManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;ILandroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public getGUIStateListener()Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v0, v0, Lcom/samsung/android/sconnect/SconnectManager;->mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

    return-object v0
.end method

.method public getP2pConnectedDeviceCount()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pClientCount()I

    move-result v0

    return v0
.end method

.method public isBonded(Ljava/lang/String;)Z
    .locals 1
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->isBonded(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isConnectingState()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isConnectingState()Z

    move-result v0

    return v0
.end method

.method public isP2pConnectingDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->isConnectingDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public needNewConnection(Ljava/lang/String;)Z
    .locals 1
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->needToDisconnectOnCentral(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public notifyGuiMessage(I)V
    .locals 3
    .param p1, "message"    # I

    .prologue
    const/4 v2, 0x0

    .line 115
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mGUIHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 120
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 119
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public notifyGuiMessage(ILjava/lang/Object;)V
    .locals 3
    .param p1, "message"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mGUIHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 127
    .local v0, "msg":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 128
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public pauseMirroring()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->pauseMirroring()V

    .line 161
    return-void
.end method

.method public resumeMirroring()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->resumeMirroring()V

    .line 165
    return-void
.end method

.method public startCentral(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V
    .locals 2
    .param p1, "fileShareActionListener"    # Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->startCentral(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->clearDeviceList()V

    .line 47
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mDeviceListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->startDiscoveryDevice(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    .line 48
    return-void
.end method

.method public startDiscoveryDevice()V
    .locals 3

    .prologue
    .line 64
    const-string v0, "CentralManager"

    const-string v1, "startDiscoveryDevice"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->clearDeviceList()V

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mDeviceListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->startDiscoveryDevice(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    .line 67
    return-void
.end method

.method public stopDiscoveryDevice()V
    .locals 3

    .prologue
    .line 70
    const-string v0, "CentralManager"

    const-string v1, "stopDiscoveryDevice"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->stopDiscoveryDevice()V

    .line 72
    return-void
.end method

.method public stopMirroring()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->stopMirroring()V

    .line 157
    return-void
.end method

.method public terminate()V
    .locals 3

    .prologue
    .line 59
    const-string v0, "CentralManager"

    const-string v1, "terminate"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->stopCentral()V

    .line 61
    return-void
.end method

.method public unableToMirroring(Ljava/lang/String;)Z
    .locals 1
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->needToDisconnectOnPeriph(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
