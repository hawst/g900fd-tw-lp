.class public Lcom/samsung/android/sconnect/common/util/LocalIntent;
.super Ljava/lang/Object;
.source "LocalIntent.java"


# static fields
.field public static final ACTION_C_DISCONNECT_P2P:Ljava/lang/String; = "com.samsung.android.sconnect.central.DISCONNECT_P2P"

.field public static final ACTION_C_PLUGIN_PRINTER_CONNECTED:Ljava/lang/String; = "com.samsung.android.sconnect.central.PLUGIN_PRINTER_CONNECTED"

.field public static final ACTION_C_PRINTER_FINISHED:Ljava/lang/String; = "com.samsung.android.sconnect.central.PRINTER_FINISHED"

.field public static final ACTION_C_PRINTER_WAITING_CANCEL:Ljava/lang/String; = "com.samsung.android.sconnect.central.PRINTER_WAITING_CANCEL"

.field public static final ACTION_C_REQUEST_RESULT:Ljava/lang/String; = "com.samsung.android.sconnect.central.REQUEST_RESULT"

.field public static final ACTION_C_WAITING_CANCEL:Ljava/lang/String; = "com.samsung.android.sconnect.central.WAITING_CANCEL"

.field public static final ACTION_P_ACCEPT_RESULT:Ljava/lang/String; = "com.samsung.android.sconnect.periph.ACCEPT_RESULT"

.field public static final ACTION_P_REQUEST_CANCEL:Ljava/lang/String; = "com.samsung.android.sconnect.periph.REQUEST_CANCEL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
