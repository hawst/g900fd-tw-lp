.class public Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;
.super Ljava/lang/Object;
.source "TogetherActionHelper.java"


# static fields
.field private static final KEY_BSSID:Ljava/lang/String; = "BSSID"

.field private static final KEY_IS_RESTART:Ljava/lang/String; = "isrestart"

.field private static final TAG:Ljava/lang/String; = "TogetherActionHelper"

.field private static final TOGETHER_ACTION_START:Ljava/lang/String; = "com.sec.android.app.alltogether.sconnect.start"

.field private static final TOGETHER_ACTION_STARTED:Ljava/lang/String; = "com.sec.android.app.alltogether.sconnect.started"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mP2pMac:Ljava/lang/String;

.field private mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mP2pMac:Ljava/lang/String;

    .line 137
    new-instance v0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper$1;-><init>(Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 44
    const-string v0, "TogetherActionHelper"

    const-string v1, "TogetherActionHelper"

    const-string v2, " --"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mContext:Landroid/content/Context;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mP2pMac:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    return-object v0
.end method

.method private registerBroadcastReceiver()V
    .locals 3

    .prologue
    .line 55
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 59
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.alltogether.sconnect.started"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 61
    return-void
.end method

.method public static startTogetherWithRoomId(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "roomId"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 100
    const-string v3, "TogetherActionHelper"

    const-string v4, "startTogetherWithRoomId"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[roomId]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [uri]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz p2, :cond_0

    .line 103
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.alltogether.sconnect.start"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    .local v1, "startIntent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 108
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "isrestart"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    const-string v3, "BSSID"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 111
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 112
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 115
    return-void
.end method


# virtual methods
.method public initiate()V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getPreDiscoveryHelper()Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->registerBroadcastReceiver()V

    .line 52
    return-void
.end method

.method public joinTogether(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4
    .param p1, "roomId"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 89
    const-string v0, "TogetherActionHelper"

    const-string v1, "joinTogether"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[roomId]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [uri]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->startTogetherWithRoomId(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;)V

    .line 97
    return-void
.end method

.method public startTogether(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 7
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "p2pMac"    # Ljava/lang/String;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "targetBtMac"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string v3, "TogetherActionHelper"

    const-string v4, "startTogether"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[deviceName]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [p2pMac]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [uri]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [targetBtMac]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mP2pMac:Ljava/lang/String;

    .line 71
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 72
    .local v2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz p3, :cond_0

    .line 73
    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.alltogether.sconnect.start"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 78
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "isrestart"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 79
    const-string v3, "BSSID"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 81
    const-string v3, "TARGET_BT_MAC"

    invoke-virtual {v0, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 83
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 85
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 86
    return-void
.end method

.method public unregisterBroadcastReceiver()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 65
    return-void
.end method
