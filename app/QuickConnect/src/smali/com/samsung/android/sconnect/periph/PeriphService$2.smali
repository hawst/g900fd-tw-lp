.class Lcom/samsung/android/sconnect/periph/PeriphService$2;
.super Landroid/content/BroadcastReceiver;
.source "PeriphService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/periph/PeriphService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/periph/PeriphService;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/periph/PeriphService;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/samsung/android/sconnect/periph/PeriphService$2;->this$0:Lcom/samsung/android/sconnect/periph/PeriphService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 141
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService$2;->this$0:Lcom/samsung/android/sconnect/periph/PeriphService;

    # getter for: Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sconnect/periph/PeriphService;->access$100(Lcom/samsung/android/sconnect/periph/PeriphService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mUserReceiver"

    const-string v3, "ACTION_USER_BACKGROUND"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService$2;->this$0:Lcom/samsung/android/sconnect/periph/PeriphService;

    # getter for: Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/periph/PeriphService;->access$200(Lcom/samsung/android/sconnect/periph/PeriphService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 145
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService$2;->this$0:Lcom/samsung/android/sconnect/periph/PeriphService;

    # getter for: Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/periph/PeriphService;->access$200(Lcom/samsung/android/sconnect/periph/PeriphService;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    const-string v1, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 147
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService$2;->this$0:Lcom/samsung/android/sconnect/periph/PeriphService;

    # getter for: Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sconnect/periph/PeriphService;->access$100(Lcom/samsung/android/sconnect/periph/PeriphService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mUserReceiver"

    const-string v3, "ACTION_USER_FOREGROUND"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService$2;->this$0:Lcom/samsung/android/sconnect/periph/PeriphService;

    # getter for: Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/periph/PeriphService;->access$200(Lcom/samsung/android/sconnect/periph/PeriphService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 149
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService$2;->this$0:Lcom/samsung/android/sconnect/periph/PeriphService;

    # getter for: Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/periph/PeriphService;->access$200(Lcom/samsung/android/sconnect/periph/PeriphService;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0xfa0

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 150
    :cond_2
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphService;->mFoundDevice:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 152
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphService;->mFoundDevice:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 153
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService$2;->this$0:Lcom/samsung/android/sconnect/periph/PeriphService;

    # getter for: Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sconnect/periph/PeriphService;->access$100(Lcom/samsung/android/sconnect/periph/PeriphService;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mUserReceiver"

    const-string v3, "ACTION_SCREEN_OFF - mFoundDevice is clear"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
