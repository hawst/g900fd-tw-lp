.class Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SelectMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MapGestureListener"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;)V
    .locals 0

    .prologue
    .line 1076
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1080
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v1}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/maps/Projection;->fromPixels(II)Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    .line 1082
    .local v0, "point":Lcom/google/android/maps/GeoPoint;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->access$2100(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1083
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v3

    const-string v4, "here"

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->addMarker(IILjava/lang/String;)Z
    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$1700(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;IILjava/lang/String;)Z

    .line 1084
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->moveToMarker(Lcom/google/android/maps/GeoPoint;)Z
    invoke-static {v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$2000(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Z

    .line 1086
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$800(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1087
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 1088
    return-void
.end method
