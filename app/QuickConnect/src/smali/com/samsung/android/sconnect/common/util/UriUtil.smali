.class public Lcom/samsung/android/sconnect/common/util/UriUtil;
.super Ljava/lang/Object;
.source "UriUtil.java"


# static fields
.field private static MEDIA_STORE_URIS_ALL:[Landroid/net/Uri; = null

.field private static MEDIA_STORE_URIS_AUDIO:[Landroid/net/Uri; = null

.field private static MEDIA_STORE_URIS_IMAGES:[Landroid/net/Uri; = null

.field private static MEDIA_STORE_URIS_VIDEO:[Landroid/net/Uri; = null

.field private static final OMA_PLUGIN_MIME:Ljava/lang/String; = "application/vnd.oma.drm.content"

.field private static final TAG:Ljava/lang/String; = "UriUtil"

.field private static final mimeTypeToRepresentativeMimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-array v0, v4, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_VIDEO:[Landroid/net/Uri;

    .line 37
    new-array v0, v4, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_AUDIO:[Landroid/net/Uri;

    .line 42
    new-array v0, v4, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_IMAGES:[Landroid/net/Uri;

    .line 47
    const/4 v0, 0x6

    new-array v0, v0, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    const/4 v1, 0x3

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_ALL:[Landroid/net/Uri;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/UriUtil;->mimeTypeToRepresentativeMimeMap:Ljava/util/Map;

    .line 63
    sget-object v0, Lcom/samsung/android/sconnect/common/util/UriUtil;->mimeTypeToRepresentativeMimeMap:Ljava/util/Map;

    const-string v1, "application/ogg"

    const-string v2, "audio/ogg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "drmFilename"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 346
    if-eqz p0, :cond_0

    .line 347
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x5

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 348
    .local v0, "mimeType":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 349
    const-string v2, ".dcf"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 350
    const-string v1, "application/vnd.oma.drm.content"

    .line 373
    .end local v0    # "mimeType":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 351
    .restart local v0    # "mimeType":Ljava/lang/String;
    :cond_1
    const-string v2, ".avi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 352
    const-string v1, "video/mux/AVI"

    goto :goto_0

    .line 353
    :cond_2
    const-string v2, ".mkv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 354
    const-string v1, "video/mux/MKV"

    goto :goto_0

    .line 355
    :cond_3
    const-string v2, ".divx"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 356
    const-string v1, "video/mux/DivX"

    goto :goto_0

    .line 357
    :cond_4
    const-string v2, ".pyv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 358
    const-string v1, "video/vnd.ms-playready.media.pyv"

    goto :goto_0

    .line 359
    :cond_5
    const-string v2, ".pya"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 360
    const-string v1, "audio/vnd.ms-playready.media.pya"

    goto :goto_0

    .line 361
    :cond_6
    const-string v2, ".wmv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 362
    const-string v1, "video/x-ms-wmv"

    goto :goto_0

    .line 363
    :cond_7
    const-string v2, ".wma"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 364
    const-string v1, "audio/x-ms-wma"

    goto :goto_0

    .line 365
    :cond_8
    const-string v2, ".isma"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 366
    const-string v1, "audio/isma"

    goto :goto_0

    .line 367
    :cond_9
    const-string v2, ".ismv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 368
    const-string v1, "video/ismv"

    goto :goto_0
.end method

.method private static getRepresentativeMime(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "mime"    # Ljava/lang/String;

    .prologue
    .line 67
    sget-object v0, Lcom/samsung/android/sconnect/common/util/UriUtil;->mimeTypeToRepresentativeMimeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    sget-object v0, Lcom/samsung/android/sconnect/common/util/UriUtil;->mimeTypeToRepresentativeMimeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 70
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static isAvailableForward(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 296
    const/4 v10, 0x0

    invoke-static {p0, p1, v10}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaFilepath(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 297
    .local v3, "filepathUri":Landroid/net/Uri;
    if-nez v3, :cond_1

    .line 340
    :cond_0
    :goto_0
    return v8

    .line 300
    :cond_1
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 301
    .local v2, "filePath":Ljava/lang/String;
    const-string v10, "application/vnd.oma.drm.content"

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/UriUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 306
    new-instance v5, Landroid/drm/DrmManagerClient;

    invoke-direct {v5, p0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 307
    .local v5, "mDrmManager":Landroid/drm/DrmManagerClient;
    new-instance v4, Landroid/drm/DrmInfoRequest;

    const/16 v10, 0xe

    const-string v11, "application/vnd.oma.drm.content"

    invoke-direct {v4, v10, v11}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 309
    .local v4, "infoRequest":Landroid/drm/DrmInfoRequest;
    const-string v10, "drm_path"

    invoke-virtual {v4, v10, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 310
    invoke-virtual {v5, v4}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 312
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    if-eqz v0, :cond_3

    .line 313
    const-string v10, "status"

    invoke-virtual {v0, v10}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 314
    .local v7, "status":Ljava/lang/String;
    const-string v10, "success"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 325
    new-instance v10, Ljava/lang/String;

    const-string v11, "type"

    invoke-direct {v10, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 326
    .local v6, "objType":Ljava/lang/Object;
    if-nez v6, :cond_4

    .line 327
    const-string v8, "UriUtil"

    const-string v10, "isAvailableForward"

    const-string v11, "objType is null"

    invoke-static {v8, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 328
    goto :goto_0

    .line 317
    .end local v6    # "objType":Ljava/lang/Object;
    :cond_2
    const-string v8, "UriUtil"

    const-string v10, "isAvailableForward"

    const-string v11, "processdrmRequest Fail"

    invoke-static {v8, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 318
    goto :goto_0

    .line 321
    .end local v7    # "status":Ljava/lang/String;
    :cond_3
    const-string v8, "UriUtil"

    const-string v10, "isAvailableForward"

    const-string v11, "processdrmRequest Fail"

    invoke-static {v8, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 322
    goto :goto_0

    .line 330
    .restart local v6    # "objType":Ljava/lang/Object;
    .restart local v7    # "status":Ljava/lang/String;
    :cond_4
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 332
    .local v1, "drmType":I
    const-string v10, "UriUtil"

    const-string v11, "isAvailableForward"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, " drmType = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    if-eqz v1, :cond_5

    if-eq v1, v8, :cond_5

    const/4 v10, 0x2

    if-ne v1, v10, :cond_0

    .line 337
    :cond_5
    const-string v8, "UriUtil"

    const-string v10, "isAvailableForward"

    const-string v11, "this is FL, CD, SSD"

    invoke-static {v8, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 338
    goto/16 :goto_0
.end method

.method public static queryMediaFilepath(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mediaUri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 145
    const-string v6, "queryMediaFilepath"

    .line 146
    .local v6, "FUNCTION_NAME":Ljava/lang/String;
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 147
    :cond_0
    const-string v0, "UriUtil"

    const-string v1, "Context and Uri should not be null"

    invoke-static {v0, v6, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v11

    .line 210
    .end local p1    # "mediaUri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-object p1

    .line 151
    .restart local p1    # "mediaUri":Landroid/net/Uri;
    :cond_2
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 160
    const-string v0, "com.android.contacts"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 161
    const-string v0, "UriUtil"

    const-string v1, "com.android.contacts AUTHORITY found null MediaFilepath"

    invoke-static {v0, v6, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v11

    .line 163
    goto :goto_0

    .line 166
    :cond_3
    const/4 v8, 0x0

    .line 168
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 179
    if-nez v8, :cond_4

    .line 180
    const-string v0, "UriUtil"

    const-string v1, "NO cursor! return null"

    invoke-static {v0, v6, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v11

    .line 181
    goto :goto_0

    .line 171
    :catch_0
    move-exception v9

    .line 172
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "UriUtil"

    const-string v1, "SQLiteException"

    invoke-static {v0, v6, v1, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p1, v11

    .line 173
    goto :goto_0

    .line 174
    .end local v9    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v9

    .line 175
    .local v9, "e":Ljava/lang/Exception;
    const-string v0, "UriUtil"

    const-string v1, "Exception"

    invoke-static {v0, v6, v1, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p1, v11

    .line 176
    goto :goto_0

    .line 184
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-ne v0, v12, :cond_5

    .line 185
    const/4 v10, 0x0

    .line 187
    .local v10, "filepath":Ljava/lang/String;
    :try_start_1
    const-string v0, "_data"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v10

    .line 193
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 194
    new-instance v7, Landroid/net/Uri$Builder;

    invoke-direct {v7}, Landroid/net/Uri$Builder;-><init>()V

    .line 195
    .local v7, "builder":Landroid/net/Uri$Builder;
    const-string v0, "file"

    invoke-virtual {v7, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 196
    invoke-virtual {v7, v10}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 197
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v11

    .local v11, "filepathUri":Landroid/net/Uri;
    move-object p1, v11

    .line 201
    goto/16 :goto_0

    .line 188
    .end local v7    # "builder":Landroid/net/Uri$Builder;
    .end local v11    # "filepathUri":Landroid/net/Uri;
    :catch_2
    move-exception v9

    .line 189
    .local v9, "e":Ljava/lang/IllegalStateException;
    const-string v0, "UriUtil"

    const-string v1, "IllegalStateException"

    invoke-static {v0, v6, v1, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 190
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object p1, v11

    .line 191
    goto/16 :goto_0

    .line 203
    .end local v9    # "e":Ljava/lang/IllegalStateException;
    .end local v10    # "filepath":Ljava/lang/String;
    :cond_5
    const-string v0, "UriUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NO record in ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v6, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 206
    const-string v0, "UriUtil"

    const-string v1, "NO item! return null"

    invoke-static {v0, v6, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v11

    .line 207
    goto/16 :goto_0

    .line 209
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_6
    const-string v0, "UriUtil"

    const-string v1, "UNKNOWN scheme! return null"

    invoke-static {v0, v6, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v11

    .line 210
    goto/16 :goto_0
.end method

.method public static queryMediaMime(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mediaUri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 216
    const-string v7, "queryMediaMime"

    .line 217
    .local v7, "FUNCTION_NAME":Ljava/lang/String;
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 218
    :cond_0
    const-string v1, "UriUtil"

    const-string v3, "Context and Uri should not be null"

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const/16 v17, 0x0

    .line 291
    :goto_0
    return-object v17

    .line 222
    :cond_1
    const-string v1, "content"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 223
    const-string v1, "UriUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SCHEME_CONTENT... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v15

    .line 225
    .local v15, "mime":Ljava/lang/String;
    invoke-static {v15}, Lcom/samsung/android/sconnect/common/util/UriUtil;->getRepresentativeMime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 226
    .local v17, "representativeMime":Ljava/lang/String;
    const-string v1, "UriUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "return Representative "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    .end local v15    # "mime":Ljava/lang/String;
    .end local v17    # "representativeMime":Ljava/lang/String;
    :cond_2
    const-string v1, "file"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 230
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 231
    .local v11, "filePath":Ljava/lang/String;
    invoke-static {v11}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 232
    const-string v1, "file://"

    const-string v3, ""

    invoke-virtual {v11, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 233
    invoke-static {v11}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 234
    .local v18, "sqlfilePath":Ljava/lang/String;
    const-string v1, "UriUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SCHEME_FILE escaped... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    sget-object v14, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_ALL:[Landroid/net/Uri;

    .line 237
    .local v14, "mediaStoreUris":[Landroid/net/Uri;
    if-eqz p2, :cond_6

    .line 238
    const-string v1, "video"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 239
    sget-object v14, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_VIDEO:[Landroid/net/Uri;

    .line 251
    :goto_1
    move-object v8, v14

    .local v8, "arr$":[Landroid/net/Uri;
    array-length v13, v8

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_2
    if-ge v12, v13, :cond_9

    aget-object v2, v8, v12

    .line 254
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 261
    .local v9, "c":Landroid/database/Cursor;
    if-nez v9, :cond_7

    .line 262
    const-string v1, "UriUtil"

    const-string v3, "NO cursor! query next store..."

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 240
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v8    # "arr$":[Landroid/net/Uri;
    .end local v9    # "c":Landroid/database/Cursor;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    :cond_3
    const-string v1, "audio"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 241
    sget-object v14, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_AUDIO:[Landroid/net/Uri;

    goto :goto_1

    .line 242
    :cond_4
    const-string v1, "image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 243
    sget-object v14, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_IMAGES:[Landroid/net/Uri;

    goto :goto_1

    .line 245
    :cond_5
    const-string v1, "UriUtil"

    const-string v3, "mimeType is UNKNOWN! query all type..."

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 248
    :cond_6
    const-string v1, "UriUtil"

    const-string v3, "mimeType is NULL! query all type..."

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 256
    .restart local v2    # "uri":Landroid/net/Uri;
    .restart local v8    # "arr$":[Landroid/net/Uri;
    .restart local v12    # "i$":I
    .restart local v13    # "len$":I
    :catch_0
    move-exception v10

    .line 257
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "UriUtil"

    const-string v3, "SQLiteException"

    invoke-static {v1, v7, v3, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 258
    const-string v17, "*/*"

    goto/16 :goto_0

    .line 266
    .end local v10    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v9    # "c":Landroid/database/Cursor;
    :cond_7
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_8

    .line 267
    const-string v1, "mime_type"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 268
    .restart local v15    # "mime":Ljava/lang/String;
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 269
    invoke-static {v15}, Lcom/samsung/android/sconnect/common/util/UriUtil;->getRepresentativeMime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 270
    .restart local v17    # "representativeMime":Ljava/lang/String;
    const-string v1, "UriUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "return Representative "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 274
    .end local v15    # "mime":Ljava/lang/String;
    .end local v17    # "representativeMime":Ljava/lang/String;
    :cond_8
    const-string v1, "UriUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NO record in ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]! query next store..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 278
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_9
    invoke-static {v11}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getMimeTypeFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 279
    .local v16, "mimeFromPath":Ljava/lang/String;
    if-eqz v16, :cond_a

    .line 280
    const-string v1, "UriUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMimeTypeFromPath "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v17, v16

    .line 281
    goto/16 :goto_0

    .line 283
    :cond_a
    const-string v1, "UriUtil"

    const-string v3, "NO item! return */*"

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v17, "*/*"

    goto/16 :goto_0

    .line 285
    .end local v8    # "arr$":[Landroid/net/Uri;
    .end local v11    # "filePath":Ljava/lang/String;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v14    # "mediaStoreUris":[Landroid/net/Uri;
    .end local v16    # "mimeFromPath":Ljava/lang/String;
    .end local v18    # "sqlfilePath":Ljava/lang/String;
    :cond_b
    const-string v1, "http"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "text"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "https"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 287
    :cond_c
    const-string v1, "UriUtil"

    const-string v3, "return text/sconnect"

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v17, "text/sconnect"

    goto/16 :goto_0

    .line 290
    :cond_d
    const-string v1, "UriUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UNKNOWN scheme! return */*"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const-string v17, "*/*"

    goto/16 :goto_0
.end method

.method public static queryMediaUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mediaUri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 75
    const-string v8, "queryMediaUri"

    .line 76
    .local v8, "FUNCTION_NAME":Ljava/lang/String;
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 77
    :cond_0
    const-string v2, "UriUtil"

    const-string v4, "Context and Uri should not be null"

    invoke-static {v2, v8, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const/16 p1, 0x0

    .line 140
    .end local p1    # "mediaUri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-object p1

    .line 81
    .restart local p1    # "mediaUri":Landroid/net/Uri;
    :cond_2
    const-string v2, "content"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 86
    const-string v2, "file"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 87
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    .line 88
    .local v12, "filePath":Ljava/lang/String;
    invoke-static {v12}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 89
    const-string v2, "file://"

    const-string v4, ""

    invoke-virtual {v12, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 90
    invoke-static {v12}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 94
    .local v18, "sqlfilePath":Ljava/lang/String;
    sget-object v17, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_ALL:[Landroid/net/Uri;

    .line 95
    .local v17, "mediaStoreUris":[Landroid/net/Uri;
    if-eqz p2, :cond_6

    .line 96
    const-string v2, "video"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 97
    sget-object v17, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_VIDEO:[Landroid/net/Uri;

    .line 109
    :goto_1
    move-object/from16 v9, v17

    .local v9, "arr$":[Landroid/net/Uri;
    array-length v0, v9

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_2
    move/from16 v0, v16

    if-ge v13, v0, :cond_9

    aget-object v3, v9, v13

    .line 112
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_data = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 119
    .local v10, "c":Landroid/database/Cursor;
    if-nez v10, :cond_7

    .line 120
    const-string v2, "UriUtil"

    const-string v4, "NO cursor! query next store..."

    invoke-static {v2, v8, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 98
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v9    # "arr$":[Landroid/net/Uri;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v13    # "i$":I
    .end local v16    # "len$":I
    :cond_3
    const-string v2, "audio"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 99
    sget-object v17, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_AUDIO:[Landroid/net/Uri;

    goto :goto_1

    .line 100
    :cond_4
    const-string v2, "image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 101
    sget-object v17, Lcom/samsung/android/sconnect/common/util/UriUtil;->MEDIA_STORE_URIS_IMAGES:[Landroid/net/Uri;

    goto :goto_1

    .line 103
    :cond_5
    const-string v2, "UriUtil"

    const-string v4, "mimeType is UNKNOWN! query all type..."

    invoke-static {v2, v8, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 106
    :cond_6
    const-string v2, "UriUtil"

    const-string v4, "mimeType is NULL! query all type..."

    invoke-static {v2, v8, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 114
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v9    # "arr$":[Landroid/net/Uri;
    .restart local v13    # "i$":I
    .restart local v16    # "len$":I
    :catch_0
    move-exception v11

    .line 115
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    const-string v2, "UriUtil"

    const-string v4, "SQLiteException"

    invoke-static {v2, v8, v4, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 116
    const/16 p1, 0x0

    goto/16 :goto_0

    .line 124
    .end local v11    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v10    # "c":Landroid/database/Cursor;
    :cond_7
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_8

    .line 125
    const-string v2, "_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 126
    .local v14, "id":J
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 130
    invoke-static {v3, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object p1

    goto/16 :goto_0

    .line 132
    .end local v14    # "id":J
    :cond_8
    const-string v2, "UriUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NO record in ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]! query next store..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v8, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 136
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v10    # "c":Landroid/database/Cursor;
    :cond_9
    const-string v2, "UriUtil"

    const-string v4, "NO item! return null"

    invoke-static {v2, v8, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const/16 p1, 0x0

    goto/16 :goto_0

    .line 139
    .end local v9    # "arr$":[Landroid/net/Uri;
    .end local v12    # "filePath":Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v16    # "len$":I
    .end local v17    # "mediaStoreUris":[Landroid/net/Uri;
    .end local v18    # "sqlfilePath":Ljava/lang/String;
    :cond_a
    const-string v2, "UriUtil"

    const-string v4, "UNKNOWN scheme! return null"

    invoke-static {v2, v8, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const/16 p1, 0x0

    goto/16 :goto_0
.end method
