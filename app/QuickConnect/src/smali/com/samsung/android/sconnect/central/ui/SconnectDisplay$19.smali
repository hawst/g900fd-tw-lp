.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onMediaPickerResult(Landroid/content/Intent;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$actionType:I

.field final synthetic val$data:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 2419
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->val$data:Landroid/content/Intent;

    iput p3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->val$actionType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 2422
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4802(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2423
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mUriProcessor:Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->val$data:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->process(Landroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v11

    .line 2424
    .local v11, "uris":[Landroid/net/Uri;
    if-eqz v11, :cond_0

    .line 2426
    move-object v7, v11

    .local v7, "arr$":[Landroid/net/Uri;
    array-length v9, v7

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v10, v7, v8

    .line 2427
    .local v10, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2426
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 2431
    .end local v7    # "arr$":[Landroid/net/Uri;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    .end local v10    # "uri":Landroid/net/Uri;
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2432
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19$1;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2453
    :cond_2
    :goto_1
    return-void

    .line 2441
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2442
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->val$actionType:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_5

    .line 2443
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->val$actionType:I

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    const-string v4, ""

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 2449
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2450
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto :goto_1

    .line 2445
    :cond_5
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->val$actionType:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 2446
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->val$actionType:I

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    const-string v4, ""

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;ILandroid/content/Intent;)Z

    goto :goto_2
.end method
