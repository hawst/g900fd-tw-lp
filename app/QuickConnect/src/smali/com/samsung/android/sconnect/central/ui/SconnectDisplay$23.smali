.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$23;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onCropIconPickerResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$data:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2661
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$23;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$23;->val$data:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 2664
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$23;->val$data:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 2665
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 2666
    const-string v1, "SconnectDisplay"

    const-string v2, "onCropIconPickerResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2667
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$23;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->changeIcon(Landroid/net/Uri;)V

    .line 2671
    :goto_0
    return-void

    .line 2669
    :cond_0
    const-string v1, "SconnectDisplay"

    const-string v2, "onCropIconPickerResult"

    const-string v3, "uri is null!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
