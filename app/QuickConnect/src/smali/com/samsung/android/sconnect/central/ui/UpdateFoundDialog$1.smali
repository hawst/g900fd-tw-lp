.class Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$1;
.super Ljava/lang/Object;
.source "UpdateFoundDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->showUpdatePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$1;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 81
    const-string v0, "UpdateFoundDialog"

    const-string v1, "showUpdatePopup"

    const-string v2, "onClick: negative"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$1;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mSconnectDisplayActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->access$000(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$1;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mSconnectDisplayActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->access$000(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$1;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mTutorialActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->access$100(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$1;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mTutorialActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->access$100(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->finish()V

    .line 88
    :cond_1
    return-void
.end method
