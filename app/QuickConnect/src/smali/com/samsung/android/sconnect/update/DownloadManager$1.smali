.class Lcom/samsung/android/sconnect/update/DownloadManager$1;
.super Ljava/lang/Object;
.source "DownloadManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/update/DownloadManager;->updateCheck(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/update/DownloadManager;

.field final synthetic val$showupDialog:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/update/DownloadManager;Z)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/sconnect/update/DownloadManager$1;->this$0:Lcom/samsung/android/sconnect/update/DownloadManager;

    iput-boolean p2, p0, Lcom/samsung/android/sconnect/update/DownloadManager$1;->val$showupDialog:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 40
    iget-object v2, p0, Lcom/samsung/android/sconnect/update/DownloadManager$1;->this$0:Lcom/samsung/android/sconnect/update/DownloadManager;

    # getter for: Lcom/samsung/android/sconnect/update/DownloadManager;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/update/DownloadManager;->access$000(Lcom/samsung/android/sconnect/update/DownloadManager;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mUpdateThread"

    const-string v4, "run()"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v2, p0, Lcom/samsung/android/sconnect/update/DownloadManager$1;->this$0:Lcom/samsung/android/sconnect/update/DownloadManager;

    # getter for: Lcom/samsung/android/sconnect/update/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/update/DownloadManager;->access$100(Lcom/samsung/android/sconnect/update/DownloadManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sconnect/update/UpdateSamsungApps;->checkUpdate(Ljava/lang/String;)Z

    move-result v1

    .line 44
    .local v1, "isNeedToUpdate":Z
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/sconnect/update/DownloadManager$1;->val$showupDialog:Z

    if-eqz v2, :cond_0

    .line 45
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/sconnect/update/DownloadManager$1;->this$0:Lcom/samsung/android/sconnect/update/DownloadManager;

    # getter for: Lcom/samsung/android/sconnect/update/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/update/DownloadManager;->access$100(Lcom/samsung/android/sconnect/update/DownloadManager;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 46
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 48
    iget-object v2, p0, Lcom/samsung/android/sconnect/update/DownloadManager$1;->this$0:Lcom/samsung/android/sconnect/update/DownloadManager;

    # getter for: Lcom/samsung/android/sconnect/update/DownloadManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/update/DownloadManager;->access$100(Lcom/samsung/android/sconnect/update/DownloadManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 50
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
