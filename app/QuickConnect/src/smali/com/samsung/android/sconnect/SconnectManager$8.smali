.class Lcom/samsung/android/sconnect/SconnectManager$8;
.super Ljava/lang/Object;
.source "SconnectManager.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/SconnectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/SconnectManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/SconnectManager;)V
    .locals 0

    .prologue
    .line 2109
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager$8;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2113
    const-string v0, "SconnectManager"

    const-string v1, "onCreateLoader"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2115
    const/4 v4, 0x0

    .line 2116
    .local v4, "where":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2118
    .local v5, "whereArgs":[Ljava/lang/String;
    const-string v4, "(physical_type == ? AND is_on_local_network == ? AND network_mode != ?) OR (physical_type == ? AND is_on_local_network == ? AND network_mode != ?)"

    .line 2125
    const/4 v0, 0x6

    new-array v5, v0, [Ljava/lang/String;

    .end local v5    # "whereArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x1

    const-string v1, "1"

    aput-object v1, v5, v0

    const/4 v0, 0x2

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x4

    const-string v1, "1"

    aput-object v1, v5, v0

    const/4 v0, 0x5

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    .line 2130
    .restart local v5    # "whereArgs":[Ljava/lang/String;
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$8;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 12
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2137
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    if-nez p2, :cond_0

    .line 2138
    const-string v8, "SconnectManager"

    const-string v9, "onLoadFinished"

    const-string v10, "cursor == null"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2178
    :goto_0
    return-void

    .line 2141
    :cond_0
    const-string v8, "SconnectManager"

    const-string v9, "onLoadFinished"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "cursor size - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2144
    const-string v8, "SconnectManager"

    const-string v9, "onLoadFinished"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "My Ip"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/sconnect/SconnectManager$8;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mMyIpAddr:Ljava/lang/String;
    invoke-static {v11}, Lcom/samsung/android/sconnect/SconnectManager;->access$1500(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2146
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2148
    :cond_1
    new-instance v8, Ljava/util/Random;

    invoke-direct {v8}, Ljava/util/Random;-><init>()V

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    add-int/lit8 v4, v8, 0x1a

    .line 2149
    .local v4, "type":I
    invoke-static {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 2150
    .local v1, "name":Ljava/lang/String;
    const-string v8, "_id"

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2151
    .local v2, "id":J
    const-string v8, "physical_type"

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2153
    .local v6, "deviceType":Ljava/lang/String;
    const-string v8, "local_ip_address"

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2156
    .local v5, "ipAddr":Ljava/lang/String;
    const-string v8, "SconnectManager"

    const-string v9, "onLoadFinished"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Device["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] - find : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", id - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", IP - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2159
    iget-object v8, p0, Lcom/samsung/android/sconnect/SconnectManager$8;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mMyIpAddr:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/sconnect/SconnectManager;->access$1500(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2160
    const-string v8, "SconnectManager"

    const-string v9, "onLoadFinished"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Device["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] - It\'s me!"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2175
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 2177
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "id":J
    .end local v4    # "type":I
    .end local v5    # "ipAddr":Ljava/lang/String;
    .end local v6    # "deviceType":Ljava/lang/String;
    :cond_2
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 2165
    .restart local v1    # "name":Ljava/lang/String;
    .restart local v2    # "id":J
    .restart local v4    # "type":I
    .restart local v5    # "ipAddr":Ljava/lang/String;
    .restart local v6    # "deviceType":Ljava/lang/String;
    :cond_3
    const-string v8, "PC"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2166
    const/4 v4, 0x4

    .line 2170
    :cond_4
    :goto_2
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;-><init>(Ljava/lang/String;JILjava/lang/String;)V

    .line 2171
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;
    new-instance v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v8, p0, Lcom/samsung/android/sconnect/SconnectManager$8;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 2172
    .local v7, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v7, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;)V

    .line 2174
    iget-object v8, p0, Lcom/samsung/android/sconnect/SconnectManager$8;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    const/4 v9, 0x1

    # invokes: Lcom/samsung/android/sconnect/SconnectManager;->updateScanList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I
    invoke-static {v8, v7, v9}, Lcom/samsung/android/sconnect/SconnectManager;->access$900(Lcom/samsung/android/sconnect/SconnectManager;Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I

    goto :goto_1

    .line 2167
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;
    .end local v7    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_5
    const-string v8, "DEVICE_TAB"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2168
    const/4 v4, 0x2

    goto :goto_2
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 2109
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/SconnectManager$8;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2182
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const-string v0, "SconnectManager"

    const-string v1, "onLoaderReset"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2183
    return-void
.end method
