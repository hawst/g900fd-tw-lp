.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onProfilePickerResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$data:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2458
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->val$data:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 2461
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2462
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->val$data:Landroid/content/Intent;

    const-string v4, "com.android.settings.wifi.WifiShareProfileSelected"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2464
    .local v0, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->val$data:Landroid/content/Intent;

    const-string v4, "com.android.settings.wifi.WifiShareProfileSelectedSSID"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2466
    .local v1, "ssid":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4802(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2467
    if-eqz v0, :cond_0

    .line 2468
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "ssid"

    invoke-virtual {v3, v4, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 2470
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2473
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2474
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v3, v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    new-instance v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20$1;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2487
    .end local v0    # "path":Ljava/lang/String;
    .end local v1    # "ssid":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 2482
    .restart local v0    # "path":Ljava/lang/String;
    .restart local v1    # "ssid":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    const/16 v5, 0x12

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;)Z

    .line 2484
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto :goto_0
.end method
