.class public Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
.super Ljava/lang/Object;
.source "PreDiscoveryHelper.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/IDiscoveryAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;,
        Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;,
        Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    }
.end annotation


# static fields
.field private static final ACK_TARGET_NONE:[B

.field public static final ADV_DATA_LEN:I = 0x1a

.field public static final ADV_IDX_APP_ID:I = 0x3

.field public static final ADV_IDX_CRC:I = 0x18

.field public static final ADV_IDX_DEVICE_HASH:I = 0x6

.field public static final ADV_IDX_DEVICE_TYPE:I = 0x9

.field public static final ADV_IDX_P2P_MAC:I = 0xc

.field public static final ADV_IDX_PACKETINFO:I = 0x5

.field public static final ADV_IDX_SERVICE_TYPE:I = 0xa

.field public static final ADV_IDX_TARGET:I = 0x12

.field public static final ADV_LEN_APP_ID:I = 0x2

.field public static final ADV_LEN_CRC:I = 0x2

.field public static final ADV_LEN_DEVICE_HASH:I = 0x3

.field public static final ADV_LEN_DEVICE_TYPE:I = 0x1

.field public static final ADV_LEN_P2P_MAC:I = 0x6

.field public static final ADV_LEN_PACKETINFO:I = 0x1

.field public static final ADV_LEN_RESP_TARGET:I = 0x6

.field public static final ADV_LEN_SAMSUNG_INFO:I = 0x3

.field public static final ADV_LEN_SERVICE_TYPE:I = 0x2

.field private static final ASCII_NEWLINE:Ljava/lang/String; = "10"

.field private static final ASCII_NULL:Ljava/lang/String; = "0"

.field private static final BLE_AUTOSCAN_TIME:I = 0x3a98

.field public static final BLE_PACKET_FORMAT_VERSION:B = 0x1t

.field private static final BLE_RESP_PERIOD:I = 0xbb8

.field private static final CMD_PERIOD:J = 0x1b58L

.field private static final CMD_REQ_TIMEOUT:I = 0x7530

.field private static final EXIST_RESP_DEVICE:I = 0x0

.field private static final EXIST_RESP_DEVICE_NEED_UPDATE:I = 0x2

.field public static final FLAG_DEVICETYPE_BTBLE_SEPERATED_ADDR:B = 0x40t

.field public static final FLAG_DEVICETYPE_P2P_SUPPORT:B = -0x80t

.field public static final FLAG_PACKETINFO_CMD:B = 0x4t

.field public static final FLAG_PACKETINFO_CONTACT:B = 0x1t

.field public static final FLAG_PACKETINFO_RESPMODE:B = 0x2t

.field public static final FLAG_PACKETINFO_SCANMODE:B = 0x8t

.field public static final MANUFACTURER_FLAG:B = -0x1t

.field public static final MANUFACTURER_ID_SAMSUNG:B = 0x75t

.field private static final MSG_RECEIVE_PREDISCOVERY_PACKET:I = 0x1

.field private static final MSG_SEND_CMD:I = 0xa

.field private static final MSG_START_CMD_ADV:I = 0x5

.field private static final MSG_START_DISCOVERY_REQ_ADV:I = 0x1

.field private static final MSG_START_DISCOVERY_RESP_ADV:I = 0x3

.field private static final MSG_STOP_CMD_ADV:I = 0x6

.field private static final MSG_STOP_DISCOVERY_REQ_ADV:I = 0x2

.field private static final MSG_STOP_DISCOVERY_RESP_ADV:I = 0x4

.field private static final NEW_RESP_DEVICE:I = 0x1

.field public static final RES_DATA_LEN:I = 0x1d

.field public static final SCAN_IDX_ACK_TARGET:I = 0xd

.field public static final SCAN_IDX_CMD_DATA:I = 0x1b

.field public static final SCAN_IDX_CMD_ID:I = 0xd

.field public static final SCAN_IDX_CMD_TARGET:I = 0xf

.field public static final SCAN_IDX_CMD_TYPE:I = 0xe

.field public static final SCAN_IDX_CONFIRM_CMD_TYPE:I = 0xe

.field public static final SCAN_IDX_CONFIRM_DATA:I = 0x15

.field public static final SCAN_IDX_CONFIRM_EMPTY:I = 0x1c

.field public static final SCAN_IDX_CONFIRM_MAX:I = 0x1d

.field public static final SCAN_IDX_CONFIRM_NAME:I = 0x0

.field public static final SCAN_IDX_CONFIRM_REQ_ID:I = 0xd

.field public static final SCAN_IDX_CONFIRM_TARGET:I = 0xf

.field public static final SCAN_IDX_CONFIRM_VALUE:I = 0x1b

.field public static final SCAN_IDX_MANUFACTURER:I = 0x0

.field public static final SCAN_IDX_NAME:I = 0x2

.field public static final SCAN_LEN_ACKTARGET:I = 0x10

.field public static final SCAN_LEN_CMDBSSID:I = 0x6

.field public static final SCAN_LEN_CMDBTMAC:I = 0x6

.field public static final SCAN_LEN_CMDDATA:I = 0x2

.field public static final SCAN_LEN_CMDTARGET:I = 0xc

.field public static final SCAN_LEN_CMD_ID:I = 0x1

.field public static final SCAN_LEN_CMD_TYPE:I = 0x1

.field public static final SCAN_LEN_CONFIRM_CMD_TYPE:I = 0x1

.field public static final SCAN_LEN_CONFIRM_DATA:I = 0x6

.field public static final SCAN_LEN_CONFIRM_EMPTY:I = 0x1

.field public static final SCAN_LEN_CONFIRM_MAX:I = 0x1d

.field public static final SCAN_LEN_CONFIRM_NAME:I = 0xd

.field public static final SCAN_LEN_CONFIRM_REQ_ID:I = 0x1

.field public static final SCAN_LEN_CONFIRM_TARGET:I = 0x6

.field public static final SCAN_LEN_CONFIRM_VALUE:I = 0x1

.field public static final SCAN_LEN_MANUFACTURER:I = 0x2

.field public static final SCAN_LEN_NAME:I = 0xb

.field public static final SERVICE_ID_SCONNECT:B = 0x1t

.field public static final SERVICE_ID_WEARABLE:B = 0x2t

.field private static final STRING_ELLIPSIS:Ljava/lang/String; = "..."

.field private static final TAG:Ljava/lang/String; = "PreDiscoveryHelper"

.field private static hash2_byte:[B

.field private static hash_byte:[B


# instance fields
.field mAdvData:[B

.field private mAutoScanTimer:Ljava/util/Timer;

.field private mBleHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

.field private mBleMac:[B

.field mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

.field private mContactCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mDiscoveryDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscoveryDeviceNetTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscoveryRespAckTarget:[B

.field private mDiscoveryRespDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscoveryRespTarget:[B

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsCommandRunning:Z

.field private mIsDiscoveryReqRunning:Z

.field private mIsDiscoveryRespRunning:Z

.field private mLastCmd:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocalBleReceiver:Landroid/content/BroadcastReceiver;

.field private mMyBleMac:[B

.field private mNeedP2pFind:Z

.field private mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPreDiscoveryDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

.field private mPreDiscoveryReceiveHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;

.field private mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field mRespData:[B

.field private mServiceType:I

.field private mSupportP2pResponse:Z

.field private mVisibilitySetting:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->ACK_TARGET_NONE:[B

    .line 151
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    .line 154
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    return-void

    .line 145
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 151
    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 154
    :array_2
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .line 56
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    .line 57
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    .line 58
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 60
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    .line 61
    new-array v1, v5, [B

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleMac:[B

    .line 62
    iput v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mServiceType:I

    .line 158
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryRespRunning:Z

    .line 159
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z

    .line 160
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsCommandRunning:Z

    .line 176
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespTarget:[B

    .line 177
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckTarget:[B

    .line 178
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    .line 180
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    .line 181
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceNetTypeList:Ljava/util/ArrayList;

    .line 186
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    .line 187
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLastCmd:Ljava/util/ArrayList;

    .line 188
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    .line 190
    const/16 v1, 0x1a

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    .line 191
    const/16 v1, 0x1d

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    .line 193
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAutoScanTimer:Ljava/util/Timer;

    .line 197
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;

    .line 198
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;

    .line 199
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mNeedP2pFind:Z

    .line 200
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mSupportP2pResponse:Z

    .line 202
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 204
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mHandlerThread:Landroid/os/HandlerThread;

    .line 205
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    .line 206
    iput-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryReceiveHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;

    .line 208
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;

    .line 505
    new-instance v1, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLocalBleReceiver:Landroid/content/BroadcastReceiver;

    .line 570
    new-instance v1, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 588
    new-instance v1, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$4;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$4;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContentObserver:Landroid/database/ContentObserver;

    .line 598
    new-instance v1, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    .line 215
    const-string v1, "PreDiscoveryHelper"

    const-string v2, "PreDiscoveryHelper"

    const-string v3, " -- "

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    .line 217
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 219
    new-array v1, v5, [B

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespTarget:[B

    .line 220
    const/16 v1, 0x10

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckTarget:[B

    .line 222
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setVisibilityFromSystemDb()V

    .line 224
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;

    .line 225
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;

    .line 227
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryResp()V

    .line 228
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryReq()V

    .line 230
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setIntentFilter()V

    .line 233
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleAddress()Z

    .line 235
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    aput-byte v4, v1, v0

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initializeAdvData()V

    .line 240
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->setPreDiscoveryPacketListener(Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;)V

    .line 241
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setPreDiscoveryPacketListener(Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;)V

    .line 243
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "PreDiscoveryWorkThread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mHandlerThread:Landroid/os/HandlerThread;

    .line 244
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 245
    new-instance v1, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    .line 246
    new-instance v1, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryReceiveHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;

    .line 247
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryReq()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateMyDeviceInfo()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryReceiveHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/BleHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleMac:[B

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsCommandRunning:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsCommandRunning:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryRespRunning:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryRespRunning:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mNeedP2pFind:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mSupportP2pResponse:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mSupportP2pResponse:Z

    return p1
.end method

.method static synthetic access$202(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mNeedP2pFind:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/os/PowerManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateAdvData()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->startAutoScanTimer()V

    return-void
.end method

.method static synthetic access$2500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryResp()V

    return-void
.end method

.method static synthetic access$2600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->clearCmdData()V

    return-void
.end method

.method static synthetic access$2700()[B
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->ACK_TARGET_NONE:[B

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # [B
    .param p6, "x6"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->handleCmdData(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # [B

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->checkForAckTarget([B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # [B

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->addDiscoveryRespAckDevice([B)V

    return-void
.end method

.method static synthetic access$3200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->isReceiveAllAck()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # [B

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->isNewDiscoveryDevice([B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # [B

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->checkForRespTarget([B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[BILjava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # [B
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->isNewDiscoveryRespDevice([BILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$3600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateDiscAdv()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/lang/String;Ljava/lang/String;Z[BZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # [B
    .param p5, "x5"    # Z

    .prologue
    .line 51
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendConfirmCommand(Ljava/lang/String;Ljava/lang/String;Z[BZ)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopAutoScanTimer()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setVisibilityFromSystemDb()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initializeAdvData()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;

    return-object v0
.end method

.method private addDiscoveryRespAckDevice([B)V
    .locals 7
    .param p1, "bleMac"    # [B

    .prologue
    const/4 v6, 0x5

    .line 857
    new-instance v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;

    const/4 v2, 0x4

    aget-byte v2, p1, v2

    aget-byte v3, p1, v6

    invoke-direct {v0, p0, v2, v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;BB)V

    .line 859
    .local v0, "lBleLastMacAddr":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 860
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 862
    .local v1, "lPos":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 863
    const-string v2, "PreDiscoveryHelper"

    const-string v4, "isNewDiscoveryRespAckDevice"

    const-string v5, "indexOf : not exist"

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, v6, :cond_0

    .line 865
    const-string v2, "PreDiscoveryHelper"

    const-string v4, "isNewDiscoveryRespAckDevice"

    const-string v5, "indexOf : not exist && 5"

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 868
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 878
    :goto_0
    monitor-exit v3

    .line 879
    return-void

    .line 870
    :cond_1
    const-string v2, "PreDiscoveryHelper"

    const-string v4, "isNewDiscoveryRespAckDevice"

    const-string v5, "indexOf : exist"

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v6, :cond_2

    .line 872
    const-string v2, "PreDiscoveryHelper"

    const-string v4, "isNewDiscoveryRespAckDevice"

    const-string v5, "indexOf : exist && 5"

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 876
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 878
    .end local v1    # "lPos":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private checkForAckTarget([B)Z
    .locals 5
    .param p1, "target"    # [B

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 842
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    if-nez v3, :cond_1

    .line 843
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleAddress()Z

    move-result v3

    if-nez v3, :cond_1

    .line 853
    :cond_0
    :goto_0
    return v1

    .line 847
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/16 v3, 0x10

    if-ge v0, v3, :cond_0

    .line 848
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    aget-byte v3, v3, v1

    aget-byte v4, p1, v0

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    aget-byte v3, v3, v2

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, p1, v4

    if-ne v3, v4, :cond_2

    .line 849
    const-string v1, "PreDiscoveryHelper"

    const-string v3, "checkForAckTarget"

    const-string v4, "my addr!!!"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 850
    goto :goto_0

    .line 847
    :cond_2
    add-int/lit8 v0, v0, 0x2

    goto :goto_1
.end method

.method private checkForRespTarget([B)Z
    .locals 5
    .param p1, "target"    # [B

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 756
    if-nez p1, :cond_1

    .line 776
    :cond_0
    :goto_0
    return v1

    .line 759
    :cond_1
    invoke-static {p1}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ffffffffffff"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 760
    const-string v1, "PreDiscoveryHelper"

    const-string v3, "checkForRespTarget"

    const-string v4, "not contact only"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 761
    goto :goto_0

    .line 764
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    if-nez v3, :cond_3

    .line 765
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleAddress()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 769
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v3, 0x6

    if-ge v0, v3, :cond_0

    .line 770
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    aget-byte v3, v3, v1

    aget-byte v4, p1, v0

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    aget-byte v3, v3, v2

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, p1, v4

    if-ne v3, v4, :cond_4

    .line 771
    const-string v1, "PreDiscoveryHelper"

    const-string v3, "checkForRespTarget"

    const-string v4, "my addr!!!"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 772
    goto :goto_0

    .line 769
    :cond_4
    add-int/lit8 v0, v0, 0x2

    goto :goto_1
.end method

.method private clearCmdData()V
    .locals 4

    .prologue
    .line 1173
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsCommandRunning:Z

    if-eqz v1, :cond_0

    .line 1181
    :goto_0
    return-void

    .line 1176
    :cond_0
    const-string v1, "PreDiscoveryHelper"

    const-string v2, "clearCmdData"

    const-string v3, "reset"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1178
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    add-int/lit8 v2, v0, 0xd

    const/4 v3, 0x0

    aput-byte v3, v1, v2

    .line 1177
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1180
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v2, 0x5

    aget-byte v3, v1, v2

    and-int/lit8 v3, v3, -0x5

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    goto :goto_0
.end method

.method private getBleAddress()Z
    .locals 6

    .prologue
    const/4 v4, 0x6

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 346
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getBleMacAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToByte(Ljava/lang/String;)[B

    move-result-object v0

    .line 347
    .local v0, "macAddress":[B
    if-nez v0, :cond_0

    .line 359
    :goto_0
    return v1

    .line 350
    :cond_0
    array-length v3, v0

    if-ge v3, v4, :cond_1

    .line 351
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleMac:[B

    array-length v4, v0

    invoke-static {v0, v1, v3, v1, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 355
    :goto_1
    const/4 v3, 0x2

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    .line 356
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleMac:[B

    const/4 v5, 0x4

    aget-byte v4, v4, v5

    aput-byte v4, v3, v1

    .line 357
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mMyBleMac:[B

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleMac:[B

    const/4 v4, 0x5

    aget-byte v3, v3, v4

    aput-byte v3, v1, v2

    move v1, v2

    .line 359
    goto :goto_0

    .line 353
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleMac:[B

    invoke-static {v0, v1, v3, v1, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_1
.end method

.method private getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    if-nez v0, :cond_0

    .line 329
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    return-object v0
.end method

.method private getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    if-nez v0, :cond_0

    .line 336
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    return-object v0
.end method

.method private handleCmdData(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLjava/lang/String;)V
    .locals 28
    .param p1, "bdname"    # Ljava/lang/String;
    .param p2, "bleMacAddr"    # Ljava/lang/String;
    .param p3, "deviceType"    # I
    .param p4, "p2pMac"    # Ljava/lang/String;
    .param p5, "sConnectData"    # [B
    .param p6, "number"    # Ljava/lang/String;

    .prologue
    .line 1284
    const/16 v24, 0x27

    aget-byte v16, p5, v24

    .line 1285
    .local v16, "id":B
    const/16 v24, 0x28

    aget-byte v11, p5, v24

    .line 1286
    .local v11, "cmd":B
    const/4 v12, 0x0

    .line 1287
    .local v12, "cmdTarget":[B
    const/4 v13, 0x0

    .line 1288
    .local v13, "cmdTargetAddr":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1289
    .local v5, "bssid":[B
    const/4 v6, 0x0

    .line 1290
    .local v6, "bssidAddr":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1291
    .local v7, "btMac":[B
    const/4 v8, 0x0

    .line 1294
    .local v8, "btMacAddr":Ljava/lang/String;
    const/16 v24, 0x8

    move/from16 v0, v24

    if-eq v11, v0, :cond_0

    const/16 v24, 0xe

    move/from16 v0, v24

    if-ne v11, v0, :cond_3

    .line 1295
    :cond_0
    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v12, v0, [B

    .line 1296
    const/16 v24, 0x29

    const/16 v25, 0x0

    const/16 v26, 0x6

    move-object/from16 v0, p5

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v12, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1297
    invoke-static {v12}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v13

    .line 1300
    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v5, v0, [B

    .line 1301
    array-length v0, v12

    move/from16 v24, v0

    add-int/lit8 v24, v24, 0x29

    const/16 v25, 0x0

    const/16 v26, 0x6

    move-object/from16 v0, p5

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v5, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1303
    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v6

    .line 1331
    :goto_0
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "PACKET INFO - CMD: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/CMD ID :"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/ACTION Target :"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getP2pMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v22

    .line 1336
    .local v22, "tmpP2pMac":Ljava/lang/String;
    if-eqz v13, :cond_1

    if-eqz v22, :cond_6

    const/16 v24, 0x9

    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_6

    .line 1338
    :cond_1
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Ignore CMD- not for me "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    :cond_2
    :goto_1
    return-void

    .line 1304
    .end local v22    # "tmpP2pMac":Ljava/lang/String;
    :cond_3
    const/16 v24, 0x11

    move/from16 v0, v24

    if-ne v11, v0, :cond_5

    .line 1305
    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v12, v0, [B

    .line 1306
    const/16 v24, 0x29

    const/16 v25, 0x0

    const/16 v26, 0x6

    move-object/from16 v0, p5

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v12, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1307
    invoke-static {v12}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v13

    .line 1310
    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v7, v0, [B

    .line 1311
    array-length v0, v12

    move/from16 v24, v0

    add-int/lit8 v24, v24, 0x29

    const/16 v25, 0x0

    const/16 v26, 0x6

    move-object/from16 v0, p5

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1313
    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v0, v0, [B

    move-object/from16 v24, v0

    fill-array-data v24, :array_0

    move-object/from16 v0, v24

    invoke-static {v0, v7}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v24

    if-nez v24, :cond_4

    .line 1316
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    const-string v26, "ACTION_ASK_TO_EMEET get HOST_BT_MAC from data"

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseBtMacAddressToString([B)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 1319
    :cond_4
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    const-string v26, "ACTION_ASK_TO_EMEET get HOST_BT_MAC from header"

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1320
    move-object/from16 v8, p2

    goto/16 :goto_0

    .line 1323
    :cond_5
    const/16 v24, 0xc

    move/from16 v0, v24

    new-array v12, v0, [B

    .line 1324
    const/16 v24, 0x29

    const/16 v25, 0x0

    const/16 v26, 0xc

    move-object/from16 v0, p5

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v0, v1, v12, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1327
    invoke-static {v12}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_0

    .line 1341
    .restart local v22    # "tmpP2pMac":Ljava/lang/String;
    :cond_6
    const/16 v24, 0x63

    move/from16 v0, v24

    if-ne v11, v0, :cond_8

    .line 1342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    move-object/from16 v24, v0

    const/16 v25, 0xd

    aget-byte v24, v24, v25

    move/from16 v0, v16

    move/from16 v1, v24

    if-ne v0, v1, :cond_7

    .line 1343
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->clearCmdData()V

    .line 1346
    :cond_7
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "ACTION_CANCEL : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static/range {p5 .. p5}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    new-instance v10, Landroid/content/Intent;

    const-string v24, "com.samsung.android.sconnect.periph.REQUEST_CANCEL"

    move-object/from16 v0, v24

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1348
    .local v10, "cancel":Landroid/content/Intent;
    const-string v24, "KEY"

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_1

    .line 1351
    .end local v10    # "cancel":Landroid/content/Intent;
    :cond_8
    const/16 v24, 0x62

    move/from16 v0, v24

    if-ne v11, v0, :cond_b

    .line 1352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    move-object/from16 v24, v0

    if-eqz v24, :cond_a

    .line 1353
    const/16 v24, 0x35

    aget-byte v24, p5, v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_9

    .line 1354
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "ACTION_CONFIRM - OK :"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v0, v0, [B

    move-object/from16 v20, v0

    .line 1356
    .local v20, "respDataByte":[B
    const/16 v24, 0x2f

    const/16 v25, 0x0

    const/16 v26, 0x6

    move-object/from16 v0, p5

    move/from16 v1, v24

    move-object/from16 v2, v20

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    move-object/from16 v24, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, p4

    move-object/from16 v3, v20

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;->onConfirmed(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 1367
    .end local v20    # "respDataByte":[B
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    move-object/from16 v24, v0

    const/16 v25, 0xd

    aget-byte v24, v24, v25

    move/from16 v0, v16

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    .line 1368
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->clearCmdData()V

    goto/16 :goto_1

    .line 1360
    :cond_9
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "ACTION_CONFIRM - REJECT : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    move-object/from16 v24, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, p4

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;->onRejected(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1365
    :cond_a
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    const-string v26, "ACTION_CONFIRM - No listener : "

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1375
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLastCmd:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    if-eqz v24, :cond_e

    .line 1376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLastCmd:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 1377
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Ignore duplicated CMD : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1380
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLastCmd:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v24

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    .line 1381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLastCmd:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1382
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    const-string v26, "mLastCmd size 5, remove(0)"

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLastCmd:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1385
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "add mLastCmd("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1388
    :cond_e
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    const-string v26, "CMD packet "

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    new-instance v18, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const-class v25, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1393
    .local v18, "intent":Landroid/content/Intent;
    const v24, 0x30808000

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1395
    const-string v24, "KEY"

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1396
    const-string v24, "CMD"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1397
    const-string v24, "ADDRESS"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1399
    new-instance v17, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    move-object/from16 v3, p6

    move/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1400
    .local v17, "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 1401
    .local v19, "requestList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;>;"
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1402
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 1403
    .local v9, "bundle":Landroid/os/Bundle;
    const-string v24, "DEVICE_INFO"

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1404
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1406
    sparse-switch v11, :sswitch_data_0

    .line 1457
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "UNKNOWN CMD "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1409
    :sswitch_0
    const/16 v24, 0x35

    aget-byte v15, p5, v24

    .line 1410
    .local v15, "contentType":I
    const/16 v24, 0x36

    aget-byte v21, p5, v24

    .line 1411
    .local v21, "signedByte":B
    move/from16 v0, v21

    and-int/lit16 v14, v0, 0xff

    .line 1412
    .local v14, "contentCount":I
    const-string v24, "CONTENTTYPE"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1413
    const-string v24, "COUNT"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1414
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ACTION_SEND_CURRENT_CONTENTS "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1460
    .end local v14    # "contentCount":I
    .end local v15    # "contentType":I
    .end local v21    # "signedByte":B
    :goto_3
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "onLeScan - cmd: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    move-object/from16 v24, v0

    if-eqz v24, :cond_2

    .line 1462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;->onRequestReceived(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 1432
    :sswitch_1
    const-string v24, "BSSID"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1433
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ACTION_TOGETHER_CREATE_ROOM "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1437
    :sswitch_2
    const/16 v24, 0x35

    aget-byte v23, p5, v24

    .line 1438
    .local v23, "type":I
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ACTION_JOIN_TO_GROUP_PLAY "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ") [type]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    const-string v24, "BSSID"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1441
    const-string v24, "CONTENT_TYPE"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_3

    .line 1444
    .end local v23    # "type":I
    :sswitch_3
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ACTION_ASK_TO_EMEET from [BT]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    const-string v24, "BT_MAC_ADDRESS"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3

    .line 1449
    :sswitch_4
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ACTION_SIDESYNC_CONTROL from [BLE]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1453
    :sswitch_5
    const-string v24, "PreDiscoveryHelper"

    const-string v25, "handleCmdData"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " ACTION_PROFILE_SHARE from [BLE]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1313
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1406
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xe -> :sswitch_2
        0x10 -> :sswitch_4
        0x11 -> :sswitch_3
        0x12 -> :sswitch_5
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method private initDiscoveryReq()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 265
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckTarget:[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 266
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckTarget:[B

    aput-byte v5, v1, v0

    .line 265
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 269
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v2, 0x5

    aget-byte v1, v1, v2

    and-int/lit8 v1, v1, 0x4

    if-nez v1, :cond_1

    .line 270
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckTarget:[B

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/16 v3, 0xd

    const/16 v4, 0x10

    invoke-static {v1, v5, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 274
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 275
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 276
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceNetTypeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 277
    monitor-exit v2

    .line 278
    return-void

    .line 277
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private initDiscoveryResp()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 250
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespTarget:[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespTarget:[B

    aput-byte v5, v1, v0

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespTarget:[B

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v3, 0x12

    const/4 v4, 0x6

    invoke-static {v1, v5, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 256
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 257
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 258
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 260
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 261
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 262
    return-void

    .line 258
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 261
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method private initializeAdvData()V
    .locals 18

    .prologue
    .line 1029
    const-string v13, "PreDiscoveryHelper"

    const-string v14, "initializeAdvData"

    const-string v15, ""

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    const/4 v4, 0x0

    .line 1033
    .local v4, "i":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    const/4 v14, 0x0

    aput-byte v14, v13, v4

    .line 1034
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    const/16 v14, 0x75

    aput-byte v14, v13, v5

    .line 1035
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .restart local v5    # "i":I
    const/4 v14, 0x1

    aput-byte v14, v13, v4

    .line 1038
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v14, 0x3

    const/4 v15, 0x0

    aput-byte v15, v13, v14

    .line 1039
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v14, 0x4

    const/4 v15, 0x1

    aput-byte v15, v13, v14

    .line 1041
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z

    if-eqz v13, :cond_2

    .line 1043
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v14, 0x5

    const/16 v15, 0x8

    aput-byte v15, v13, v14

    .line 1049
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/samsung/android/sconnect/common/util/Util;->getMyMobileNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 1051
    .local v11, "phoneNumber":Ljava/lang/String;
    if-nez v11, :cond_3

    .line 1052
    const-string v11, "00000000"

    .line 1064
    :goto_1
    sget-object v13, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    if-eqz v13, :cond_4

    .line 1065
    sget-object v13, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v16, 0x6

    const/16 v17, 0x3

    invoke-static/range {v13 .. v17}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1070
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getServiceType()I

    move-result v12

    .line 1072
    .local v12, "serviceType":I
    const-string v13, "ro.build.characteristics"

    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "tablet"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1073
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v14, 0x9

    const/4 v15, 0x2

    aput-byte v15, v13, v14

    .line 1078
    :goto_3
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/NetUtil;->isBtBleSeperated()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1079
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v14, 0x9

    aget-byte v15, v13, v14

    or-int/lit8 v15, v15, 0x40

    int-to-byte v15, v15

    aput-byte v15, v13, v14

    .line 1084
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v14, 0xa

    shr-int/lit8 v15, v12, 0x8

    int-to-byte v15, v15

    aput-byte v15, v13, v14

    .line 1085
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v14, 0xb

    int-to-byte v15, v12

    aput-byte v15, v13, v14

    .line 1087
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getP2pMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToByte(Ljava/lang/String;)[B

    move-result-object v1

    .line 1089
    .local v1, "address":[B
    if-eqz v1, :cond_7

    .line 1090
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v15, 0xc

    const/16 v16, 0x6

    move/from16 v0, v16

    invoke-static {v1, v13, v14, v15, v0}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1096
    :goto_5
    sget-object v13, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    if-eqz v13, :cond_8

    .line 1097
    sget-object v13, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v16, 0x18

    const/16 v17, 0x2

    invoke-static/range {v13 .. v17}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1102
    :goto_6
    const/4 v2, 0x0

    .line 1105
    .local v2, "compensationLen":I
    const/4 v4, 0x0

    .line 1106
    .end local v5    # "i":I
    .restart local v4    # "i":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .restart local v5    # "i":I
    const/16 v14, 0x75

    aput-byte v14, v13, v4

    .line 1107
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    const/4 v14, 0x0

    aput-byte v14, v13, v5

    .line 1109
    add-int/lit8 v10, v2, 0xb

    .line 1111
    .local v10, "nameLen":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/samsung/android/sconnect/common/util/Util;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1112
    .local v3, "deviceName":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    .line 1114
    .local v9, "name":[B
    array-length v13, v9

    if-lt v13, v10, :cond_1

    .line 1115
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v7, v13, -0x1

    .line 1117
    .local v7, "index":I
    :cond_0
    const/4 v13, 0x0

    invoke-virtual {v3, v13, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    .line 1119
    const-string v13, "PreDiscoveryHelper"

    const-string v14, "initializeAdvData"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "name: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v9}, Ljava/lang/String;-><init>([B)V

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    array-length v0, v9

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    add-int/lit8 v7, v7, -0x1

    .line 1122
    array-length v13, v9

    if-ge v13, v10, :cond_0

    .line 1123
    const-string v13, "PreDiscoveryHelper"

    const-string v14, "initializeAdvData"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "name selected: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v9}, Ljava/lang/String;-><init>([B)V

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    array-length v0, v9

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1125
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v9}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    .line 1128
    .end local v7    # "index":I
    :cond_1
    const/4 v6, 0x0

    .local v6, "idx":I
    :goto_7
    if-ge v6, v10, :cond_9

    .line 1129
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    add-int/lit8 v14, v6, 0x2

    const/4 v15, 0x0

    aput-byte v15, v13, v14

    .line 1128
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 1046
    .end local v1    # "address":[B
    .end local v2    # "compensationLen":I
    .end local v3    # "deviceName":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v6    # "idx":I
    .end local v9    # "name":[B
    .end local v10    # "nameLen":I
    .end local v11    # "phoneNumber":Ljava/lang/String;
    .end local v12    # "serviceType":I
    .restart local v5    # "i":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v14, 0x5

    const/4 v15, 0x0

    aput-byte v15, v13, v14

    goto/16 :goto_0

    .line 1054
    .restart local v11    # "phoneNumber":Ljava/lang/String;
    :cond_3
    invoke-static {v11}, Lcom/samsung/android/sconnect/common/util/Util;->cutNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1055
    .local v8, "myNumber":Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/sconnect/common/util/Hash;->getSipHashByte(Ljava/lang/String;)[B

    move-result-object v13

    sput-object v13, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    .line 1056
    invoke-static {v8}, Lcom/samsung/android/sconnect/common/util/Hash;->getDataCheckByte(Ljava/lang/String;)[B

    move-result-object v13

    sput-object v13, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    .line 1058
    const-string v13, "PreDiscoveryHelper"

    const-string v14, "initializeAdvData Hash ***"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ":"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1067
    .end local v8    # "myNumber":Ljava/lang/String;
    :cond_4
    const-string v13, "PreDiscoveryHelper"

    const-string v14, "initializeAdvData Hash ***"

    const-string v15, "hash is null"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1075
    .restart local v12    # "serviceType":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v14, 0x9

    const/4 v15, 0x1

    aput-byte v15, v13, v14

    goto/16 :goto_3

    .line 1081
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v14, 0x9

    aget-byte v15, v13, v14

    and-int/lit8 v15, v15, -0x41

    int-to-byte v15, v15

    aput-byte v15, v13, v14

    goto/16 :goto_4

    .line 1092
    .restart local v1    # "address":[B
    :cond_7
    const-string v13, "PreDiscoveryHelper"

    const-string v14, "initializeAdvData"

    const-string v15, "address is null"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1099
    :cond_8
    const-string v13, "PreDiscoveryHelper"

    const-string v14, "initializeAdvData"

    const-string v15, "hash2 is null"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1131
    .end local v5    # "i":I
    .restart local v2    # "compensationLen":I
    .restart local v3    # "deviceName":Ljava/lang/String;
    .restart local v4    # "i":I
    .restart local v6    # "idx":I
    .restart local v9    # "name":[B
    .restart local v10    # "nameLen":I
    :cond_9
    array-length v13, v9

    if-le v13, v10, :cond_a

    .line 1132
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/4 v15, 0x2

    invoke-static {v9, v13, v14, v15, v10}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1137
    :goto_8
    return-void

    .line 1134
    :cond_a
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/4 v15, 0x2

    array-length v0, v9

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v9, v13, v14, v15, v0}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_8
.end method

.method private isNewDiscoveryDevice([B)Z
    .locals 15
    .param p1, "bleMac"    # [B

    .prologue
    .line 715
    const/4 v10, 0x0

    .line 717
    .local v10, "toBeUpdate":Z
    new-instance v6, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;

    const/4 v11, 0x4

    aget-byte v11, p1, v11

    const/4 v12, 0x5

    aget-byte v12, p1, v12

    invoke-direct {v6, p0, v11, v12}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;BB)V

    .line 719
    .local v6, "lBleLastMacAddr":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    iget-object v12, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    monitor-enter v12

    .line 720
    :try_start_0
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v7

    .line 722
    .local v7, "lPos":I
    const/4 v11, -0x1

    if-ne v7, v11, :cond_1

    .line 723
    const-string v11, "PreDiscoveryHelper"

    const-string v13, "isNewDiscoveryDevice"

    const-string v14, "indexOf : not exist"

    invoke-static {v11, v13, v14}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v13, 0x3

    if-lt v11, v13, :cond_0

    .line 725
    const-string v11, "PreDiscoveryHelper"

    const-string v13, "isNewDiscoveryDevice"

    const-string v14, "indexOf : not exist && 3"

    invoke-static {v11, v13, v14}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 728
    :cond_0
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 729
    const/4 v10, 0x1

    .line 741
    :goto_0
    if-eqz v10, :cond_5

    .line 742
    const/4 v2, 0x0

    .line 743
    .local v2, "i":I
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;

    .line 744
    .local v9, "target":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->get2Byte()[B

    move-result-object v1

    .local v1, "arr$":[B
    array-length v8, v1

    .local v8, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_2
    if-ge v5, v8, :cond_3

    aget-byte v11, v1, v5

    invoke-static {v11}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 745
    .local v0, "addr":Ljava/lang/Byte;
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespTarget:[B

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v13

    aput-byte v13, v11, v3

    .line 744
    add-int/lit8 v5, v5, 0x1

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_2

    .line 731
    .end local v0    # "addr":Ljava/lang/Byte;
    .end local v1    # "arr$":[B
    .end local v3    # "i":I
    .end local v5    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "target":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    :cond_1
    const-string v11, "PreDiscoveryHelper"

    const-string v13, "isNewDiscoveryDevice"

    const-string v14, "indexOf : exist"

    invoke-static {v11, v13, v14}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v13, 0x3

    if-le v11, v13, :cond_2

    .line 733
    const-string v11, "PreDiscoveryHelper"

    const-string v13, "isNewDiscoveryDevice"

    const-string v14, "indexOf : exist && 3"

    invoke-static {v11, v13, v14}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :cond_2
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 737
    iget-object v11, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 738
    const/4 v10, 0x0

    goto :goto_0

    .restart local v1    # "arr$":[B
    .restart local v3    # "i":I
    .restart local v5    # "i$":I
    .restart local v8    # "len$":I
    .restart local v9    # "target":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    :cond_3
    move v2, v3

    .line 747
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_1

    .line 748
    .end local v1    # "arr$":[B
    .end local v5    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "target":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateRespTarget()V

    .line 749
    const/4 v11, 0x1

    monitor-exit v12

    .line 752
    .end local v2    # "i":I
    :goto_3
    return v11

    .line 751
    :cond_5
    monitor-exit v12

    .line 752
    const/4 v11, 0x0

    goto :goto_3

    .line 751
    .end local v7    # "lPos":I
    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11
.end method

.method private isNewDiscoveryRespDevice([BILjava/lang/String;)I
    .locals 16
    .param p1, "bleMac"    # [B
    .param p2, "netType"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 780
    new-instance v7, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;

    const/4 v12, 0x4

    aget-byte v12, p1, v12

    const/4 v13, 0x5

    aget-byte v13, p1, v13

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v12, v13}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;BB)V

    .line 781
    .local v7, "lBleLastMacAddr":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    const/4 v10, 0x0

    .line 783
    .local v10, "result":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    monitor-enter v13

    .line 784
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v8

    .line 786
    .local v8, "lPos":I
    const/4 v12, -0x1

    if-ne v8, v12, :cond_3

    .line 787
    const-string v12, "PreDiscoveryHelper"

    const-string v14, "isNewDiscoveryRespDevice"

    const-string v15, "indexOf : not exist"

    invoke-static {v12, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/16 v14, 0x8

    if-lt v12, v14, :cond_0

    .line 789
    const-string v12, "PreDiscoveryHelper"

    const-string v14, "isNewDiscoveryRespDevice"

    const-string v15, "indexOf : not exist && 8"

    invoke-static {v12, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 791
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceNetTypeList:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 793
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 794
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceNetTypeList:Ljava/util/ArrayList;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 795
    const/4 v3, 0x0

    .line 796
    .local v3, "i":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;

    .line 797
    .local v11, "target":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->get2Byte()[B

    move-result-object v2

    .local v2, "arr$":[B
    array-length v9, v2

    .local v9, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v4, v3

    .end local v3    # "i":I
    .local v4, "i":I
    :goto_1
    if-ge v6, v9, :cond_1

    aget-byte v12, v2, v6

    invoke-static {v12}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    .line 798
    .local v1, "addr":Ljava/lang/Byte;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckTarget:[B

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v14

    aput-byte v14, v12, v4

    .line 797
    add-int/lit8 v6, v6, 0x1

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_1

    .end local v1    # "addr":Ljava/lang/Byte;
    :cond_1
    move v3, v4

    .line 800
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 801
    .end local v2    # "arr$":[B
    .end local v6    # "i$":I
    .end local v9    # "len$":I
    .end local v11    # "target":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateAckTarget()V

    .line 802
    const/4 v10, 0x1

    .line 820
    .end local v3    # "i":I
    :goto_2
    monitor-exit v13

    .line 822
    return v10

    .line 804
    :cond_3
    const-string v12, "PreDiscoveryHelper"

    const-string v14, "isNewDiscoveryRespDevice"

    const-string v15, "indexOf : exist"

    invoke-static {v12, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/16 v14, 0x8

    if-le v12, v14, :cond_4

    .line 806
    const-string v12, "PreDiscoveryHelper"

    const-string v14, "isNewDiscoveryRespDevice"

    const-string v15, "indexOf : exist && 8"

    invoke-static {v12, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceNetTypeList:Ljava/util/ArrayList;

    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/16 v14, 0x8

    if-ne v12, v14, :cond_5

    const/4 v12, 0x2

    move/from16 v0, p2

    if-ne v0, v12, :cond_5

    if-eqz p3, :cond_5

    .line 810
    const/4 v10, 0x2

    .line 811
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceNetTypeList:Ljava/util/ArrayList;

    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 812
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceNetTypeList:Ljava/util/ArrayList;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 818
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 820
    .end local v8    # "lPos":I
    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v12

    .line 814
    .restart local v8    # "lPos":I
    :cond_5
    const/4 v10, 0x0

    goto :goto_3
.end method

.method private isReceiveAllAck()Z
    .locals 7

    .prologue
    .line 826
    const/4 v2, 0x0

    .line 827
    .local v2, "notReceive":Z
    const-string v3, "PreDiscoveryHelper"

    const-string v4, "isReceiveAllAck"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mDiscoveryDeviceList : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    const-string v3, "PreDiscoveryHelper"

    const-string v4, "isReceiveAllAck"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mDiscoveryRespAckDeviceList : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 832
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;

    .line 833
    .local v0, "addr":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_0

    .line 834
    const/4 v2, 0x1

    goto :goto_0

    .line 837
    .end local v0    # "addr":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    :cond_1
    monitor-exit v4

    .line 838
    if-nez v2, :cond_2

    const/4 v3, 0x1

    :goto_1
    return v3

    .line 837
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 838
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private sendCommand(Ljava/lang/String;Ljava/lang/String;I[B)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "cmd"    # I
    .param p4, "cmdData"    # [B

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 1020
    invoke-direct {p0, p1, p3, p4, p2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateCmdData(Ljava/lang/String;I[BLjava/lang/String;)V

    .line 1021
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessage(I)Z

    .line 1022
    return-void
.end method

.method private sendCommandOnlyBLE(Ljava/lang/String;Ljava/lang/String;I[B)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "cmd"    # I
    .param p4, "cmdData"    # [B

    .prologue
    .line 1010
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 1011
    invoke-direct {p0, p1, p3, p4, p2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateCmdData(Ljava/lang/String;I[BLjava/lang/String;)V

    .line 1012
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1013
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1014
    const-string v1, "ONLY_BLE"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1015
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1016
    return-void
.end method

.method private sendConfirmCommand(Ljava/lang/String;Ljava/lang/String;Z[BZ)V
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "respValue"    # Z
    .param p4, "respData"    # [B
    .param p5, "onlyBLE"    # Z

    .prologue
    const/16 v5, 0x62

    const/4 v4, 0x6

    const/4 v3, 0x0

    .line 987
    const/4 v2, 0x7

    new-array v1, v2, [B

    .line 988
    .local v1, "resData":[B
    if-nez p4, :cond_0

    .line 989
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 990
    aput-byte v3, v1, v0

    .line 989
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 993
    .end local v0    # "i":I
    :cond_0
    invoke-static {p4, v3, v1, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 996
    :cond_1
    if-eqz p3, :cond_2

    .line 997
    const/4 v2, 0x1

    aput-byte v2, v1, v4

    .line 1002
    :goto_1
    if-eqz p5, :cond_3

    .line 1003
    invoke-direct {p0, p1, p2, v5, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommandOnlyBLE(Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 1007
    :goto_2
    return-void

    .line 999
    :cond_2
    aput-byte v3, v1, v4

    goto :goto_1

    .line 1005
    :cond_3
    invoke-direct {p0, p1, p2, v5, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommand(Ljava/lang/String;Ljava/lang/String;I[B)V

    goto :goto_2
.end method

.method private setIntentFilter()V
    .locals 6

    .prologue
    .line 482
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 483
    .local v1, "localIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "com.samsung.android.sconnect.central.WAITING_CANCEL"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 484
    const-string v2, "com.samsung.android.sconnect.periph.ACCEPT_RESULT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 485
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLocalBleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 488
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 489
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 490
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 491
    const-string v2, "com.android.settings.DEVICE_NAME_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 492
    const-string v2, "com.android.settings.QUICK_CONNECT_SETTINGS_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 493
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 495
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 497
    return-void
.end method

.method private setVisibilityFromSystemDb()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1538
    const-string v2, "PreDiscoveryHelper"

    const-string v3, "setVisibilityFromSystemDb"

    const-string v4, "--"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    const/4 v0, 0x0

    .line 1541
    .local v0, "isAllowToConnect":Z
    const/4 v1, 0x0

    .line 1543
    .local v1, "isContactOnly":Z
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 1544
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_CONTACT_ONLY:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    .line 1546
    if-nez v0, :cond_1

    .line 1548
    const/4 v2, 0x2

    iput v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    .line 1557
    :goto_0
    const-string v2, "PreDiscoveryHelper"

    const-string v3, "setVisibilityFromSystemDb"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isAllowToConnect : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", isContactOnly : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", visibilitySetting : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/Util;->getVisibilityString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1560
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->isCentralRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1562
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    const-string v3, "ARTC"

    iget v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    invoke-static {v2, v3, v6, v4}, Lcom/samsung/android/sconnect/common/util/Util;->loggingSurvey(Landroid/content/Context;Ljava/lang/String;II)V

    .line 1565
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->changeResponseSetting()V

    .line 1566
    return-void

    .line 1549
    :cond_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 1551
    const/4 v2, 0x1

    iput v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    goto :goto_0

    .line 1554
    :cond_2
    iput v6, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    goto :goto_0
.end method

.method private startAutoScanTimer()V
    .locals 4

    .prologue
    .line 413
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isCentralRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopAutoScanTimer()V

    .line 417
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "startAutoScanTimer"

    const-string v2, "15 seconds"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAutoScanTimer:Ljava/util/Timer;

    .line 420
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAutoScanTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;-><init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 439
    :cond_0
    return-void
.end method

.method private startDiscAdv()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 449
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopAutoScanTimer()V

    .line 450
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->clearCmdData()V

    .line 451
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryReq()V

    .line 453
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 454
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 455
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 456
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mNeedP2pFind:Z

    .line 460
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 461
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessage(I)Z

    .line 464
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 465
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const-wide/16 v2, 0x1b58

    invoke-virtual {v0, v4, v2, v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 467
    return-void

    .line 456
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private stopAutoScanTimer()V
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAutoScanTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAutoScanTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 444
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAutoScanTimer:Ljava/util/Timer;

    .line 446
    :cond_0
    return-void
.end method

.method private stopDiscAdv()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 475
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 477
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessage(I)Z

    .line 479
    :cond_0
    return-void
.end method

.method private unregisterIntentFilter()V
    .locals 2

    .prologue
    .line 500
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLocalBleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 501
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 502
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 503
    return-void
.end method

.method private updateAckTarget()V
    .locals 5

    .prologue
    .line 1271
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "updateAckTarget"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_0

    .line 1275
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespAckTarget:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/16 v3, 0xd

    const/16 v4, 0x10

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1279
    :cond_0
    return-void
.end method

.method private updateAdvData()V
    .locals 5

    .prologue
    const/16 v4, 0x9

    const/4 v3, 0x5

    .line 1239
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "updateAdvData"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z

    if-eqz v0, :cond_0

    .line 1242
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    aget-byte v1, v0, v3

    or-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    .line 1247
    :goto_0
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    if-nez v0, :cond_1

    .line 1248
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    aget-byte v1, v0, v3

    or-int/lit8 v1, v1, 0x2

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    .line 1253
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isPossibleP2pProbe()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1254
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    aget-byte v1, v0, v4

    or-int/lit8 v1, v1, -0x80

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    .line 1258
    :goto_2
    return-void

    .line 1244
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    aget-byte v1, v0, v3

    and-int/lit8 v1, v1, -0x9

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    goto :goto_0

    .line 1250
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    aget-byte v1, v0, v3

    and-int/lit8 v1, v1, -0x3

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    goto :goto_1

    .line 1256
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    aget-byte v1, v0, v4

    and-int/lit8 v1, v1, 0x7f

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    goto :goto_2
.end method

.method private updateCmdData(Ljava/lang/String;I[BLjava/lang/String;)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "cmd"    # I
    .param p3, "cmdData"    # [B
    .param p4, "target"    # Ljava/lang/String;

    .prologue
    .line 1184
    const-string v4, "PreDiscoveryHelper"

    const-string v5, "updateCmdData"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    const/4 v2, 0x0

    .line 1186
    .local v2, "cmdDataLen":I
    const/4 v0, 0x0

    .line 1188
    .local v0, "addrLen":I
    if-eqz p3, :cond_0

    .line 1189
    array-length v2, p3

    .line 1191
    :cond_0
    invoke-static {p4}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToByte(Ljava/lang/String;)[B

    move-result-object v1

    .line 1192
    .local v1, "address":[B
    if-eqz v1, :cond_1

    .line 1193
    array-length v0, v1

    .line 1196
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v5, 0x5

    aget-byte v6, v4, v5

    or-int/lit8 v6, v6, 0x4

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 1198
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isPossibleP2pProbe()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1199
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v5, 0x9

    aget-byte v6, v4, v5

    or-int/lit8 v6, v6, -0x80

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 1205
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/16 v5, 0xd

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(Ljava/lang/String;)Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    aput-byte v6, v4, v5

    .line 1208
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/16 v5, 0xe

    int-to-byte v6, p2

    aput-byte v6, v4, v5

    .line 1212
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/16 v6, 0xf

    invoke-static {v1, v4, v5, v6, v0}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1214
    const/16 v4, 0x62

    if-ne p2, v4, :cond_5

    .line 1216
    move v3, v0

    .local v3, "i":I
    :goto_1
    const/16 v4, 0xc

    if-ge v3, v4, :cond_3

    .line 1217
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    add-int/lit8 v5, v3, 0xf

    const/4 v6, 0x0

    aput-byte v6, v4, v5

    .line 1216
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1201
    .end local v3    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v5, 0x9

    aget-byte v6, v4, v5

    and-int/lit8 v6, v6, 0x7f

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    goto :goto_0

    .line 1220
    .restart local v3    # "i":I
    :cond_3
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/16 v6, 0x15

    invoke-static {p3, v4, v5, v6, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1236
    .end local v3    # "i":I
    :cond_4
    :goto_2
    return-void

    .line 1221
    :cond_5
    const/16 v4, 0x8

    if-eq p2, v4, :cond_6

    const/16 v4, 0xe

    if-eq p2, v4, :cond_6

    const/16 v4, 0x11

    if-ne p2, v4, :cond_7

    .line 1224
    :cond_6
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    add-int/lit8 v6, v0, 0xf

    invoke-static {p3, v4, v5, v6, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_2

    .line 1227
    :cond_7
    move v3, v0

    .restart local v3    # "i":I
    :goto_3
    const/16 v4, 0xc

    if-ge v3, v4, :cond_8

    .line 1228
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    add-int/lit8 v5, v3, 0xf

    const/4 v6, 0x0

    aput-byte v6, v4, v5

    .line 1227
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1232
    :cond_8
    if-lez v2, :cond_4

    .line 1233
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    const/16 v6, 0x1b

    invoke-static {p3, v4, v5, v6, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_2
.end method

.method private updateDiscAdv()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 470
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 471
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessage(I)Z

    .line 472
    return-void
.end method

.method private updateMyDeviceInfo()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1141
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/Util;->getMyMobileNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1143
    .local v1, "phoneNumber":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1144
    const-string v1, "00000000"

    .line 1157
    :goto_0
    sget-object v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    if-eqz v2, :cond_1

    .line 1158
    sget-object v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/4 v4, 0x6

    const/4 v5, 0x3

    invoke-static {v2, v6, v3, v4, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1164
    :goto_1
    sget-object v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    if-eqz v2, :cond_2

    .line 1165
    sget-object v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v4, 0x18

    const/4 v5, 0x2

    invoke-static {v2, v6, v3, v4, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1170
    :goto_2
    return-void

    .line 1146
    :cond_0
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/util/Util;->cutNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1147
    .local v0, "myNumber":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/Hash;->getSipHashByte(Ljava/lang/String;)[B

    move-result-object v2

    sput-object v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    .line 1148
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/Hash;->getDataCheckByte(Ljava/lang/String;)[B

    move-result-object v2

    sput-object v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    .line 1150
    const-string v2, "PreDiscoveryHelper"

    const-string v3, "updateMyDeviceInfo Hash ***"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1160
    .end local v0    # "myNumber":Ljava/lang/String;
    :cond_1
    const-string v2, "PreDiscoveryHelper"

    const-string v3, "updateMyDeviceInfo Hash ***"

    const-string v4, "hash is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1167
    :cond_2
    const-string v2, "PreDiscoveryHelper"

    const-string v3, "initializeAdvData"

    const-string v4, "hash2 is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private updateRespTarget()V
    .locals 5

    .prologue
    .line 1262
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "updateRespTarget"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1265
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespTarget:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v3, 0x12

    const/4 v4, 0x6

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1267
    return-void
.end method


# virtual methods
.method public changeResponseSetting()V
    .locals 5

    .prologue
    .line 1469
    const-string v1, "PreDiscoveryHelper"

    const-string v2, "changeResponseSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    invoke-static {v4}, Lcom/samsung/android/sconnect/common/util/Util;->getVisibilityString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1471
    iget v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1473
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 1474
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryRespTarget:[B

    const/4 v2, 0x0

    aput-byte v2, v1, v0

    .line 1473
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1477
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1478
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1479
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1480
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateAdvData()V

    .line 1481
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateRespTarget()V

    .line 1484
    .end local v0    # "i":I
    :cond_2
    return-void

    .line 1479
    .restart local v0    # "i":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public checkEnabledNetwork()Z
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x1

    return v0
.end method

.method public getDeviceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 404
    const/4 v0, 0x0

    return-object v0
.end method

.method public getServiceType()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mServiceType:I

    return v0
.end method

.method public sendCancelCommand(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 960
    .local p2, "addressList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 961
    .local v2, "target":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 962
    .local v0, "device":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x11

    if-eq v3, v4, :cond_2

    .line 963
    :cond_0
    const-string v3, "PreDiscoveryHelper"

    const-string v4, "sendFileShareCommand"

    const-string v5, "Invalid target"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    .end local v0    # "device":Ljava/lang/String;
    :cond_1
    const/16 v3, 0x63

    const/4 v4, 0x0

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommand(Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 974
    return-void

    .line 966
    .restart local v0    # "device":Ljava/lang/String;
    :cond_2
    if-nez v2, :cond_3

    .line 967
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 969
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public sendEmeetCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "hostBtMacAddr"    # Ljava/lang/String;

    .prologue
    .line 901
    invoke-static {p3}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToByte(Ljava/lang/String;)[B

    move-result-object v0

    .line 902
    .local v0, "hostBtMac":[B
    if-eqz v0, :cond_0

    .line 903
    const/16 v1, 0x11

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommandOnlyBLE(Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 907
    :goto_0
    return-void

    .line 905
    :cond_0
    const-string v1, "PreDiscoveryHelper"

    const-string v2, "sendEmeetCommand"

    const-string v3, "hostBtMac data is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendFileShareCommand(Ljava/lang/String;Ljava/util/ArrayList;IIZ)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "count"    # I
    .param p5, "isGroupOwner"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;IIZ)V"
        }
    .end annotation

    .prologue
    .local p2, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 926
    const/4 v4, 0x2

    new-array v0, v4, [B

    .line 927
    .local v0, "data":[B
    const/16 v4, 0xff

    if-le p4, v4, :cond_0

    .line 928
    const/16 p4, 0xff

    .line 930
    :cond_0
    int-to-byte v4, p3

    aput-byte v4, v0, v7

    .line 931
    const/4 v4, 0x1

    int-to-byte v5, p4

    aput-byte v5, v0, v4

    .line 933
    const/4 v3, 0x0

    .line 934
    .local v3, "target":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 935
    .local v1, "device":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x11

    if-eq v4, v5, :cond_3

    .line 936
    :cond_1
    const-string v4, "PreDiscoveryHelper"

    const-string v5, "sendFileShareCommand"

    const-string v6, "Invalid target"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    .end local v1    # "device":Ljava/lang/String;
    :cond_2
    if-eqz p5, :cond_5

    .line 948
    const/16 v4, 0x50

    invoke-direct {p0, p1, v3, v4, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommand(Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 952
    :goto_1
    return-void

    .line 939
    .restart local v1    # "device":Ljava/lang/String;
    :cond_3
    if-nez v3, :cond_4

    .line 940
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 942
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 950
    .end local v1    # "device":Ljava/lang/String;
    :cond_5
    invoke-direct {p0, p1, v3, v7, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommand(Ljava/lang/String;Ljava/lang/String;I[B)V

    goto :goto_1
.end method

.method public sendGroupShareCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "bssidAddr"    # Ljava/lang/String;
    .param p4, "contentType"    # I

    .prologue
    const/4 v3, 0x0

    .line 888
    invoke-static {p3}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToByte(Ljava/lang/String;)[B

    move-result-object v0

    .line 889
    .local v0, "bssid":[B
    if-eqz v0, :cond_0

    .line 890
    array-length v2, v0

    add-int/lit8 v2, v2, 0x1

    new-array v1, v2, [B

    .line 891
    .local v1, "cmdData":[B
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 892
    array-length v2, v0

    int-to-byte v3, p4

    aput-byte v3, v1, v2

    .line 894
    const/16 v2, 0xe

    invoke-direct {p0, p1, p2, v2, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommandOnlyBLE(Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 898
    .end local v1    # "cmdData":[B
    :goto_0
    return-void

    .line 896
    :cond_0
    const-string v2, "PreDiscoveryHelper"

    const-string v3, "sendGroupShareCommand"

    const-string v4, "bssid data is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendProfileShareCommand(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 955
    const/16 v0, 0x12

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommand(Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 956
    return-void
.end method

.method public sendSideSyncReqCommand(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 910
    const/16 v0, 0x10

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommandOnlyBLE(Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 911
    return-void
.end method

.method public sendTogetherCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "roomId"    # Ljava/lang/String;

    .prologue
    .line 882
    invoke-static {p3}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToByte(Ljava/lang/String;)[B

    move-result-object v0

    .line 884
    .local v0, "bssid":[B
    const/16 v1, 0x8

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCommandOnlyBLE(Ljava/lang/String;Ljava/lang/String;I[B)V

    .line 885
    return-void
.end method

.method public setCommandListener(Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    .prologue
    .line 342
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    .line 343
    return-void
.end method

.method public setServiceType(IZ)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "isAdd"    # Z

    .prologue
    .line 363
    if-eqz p2, :cond_0

    .line 364
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mServiceType:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mServiceType:I

    .line 368
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v1, 0xa

    iget v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mServiceType:I

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 369
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    const/16 v1, 0xb

    iget v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mServiceType:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 370
    return-void

    .line 366
    :cond_0
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mServiceType:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mServiceType:I

    goto :goto_0
.end method

.method public startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .prologue
    .line 378
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "startDiscovery"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 382
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    .line 383
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->setCentralRunningMode(Z)V

    .line 384
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setDiscoveryListener(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    .line 385
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setDeviceSelectMode(Z)V

    .line 386
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->startDiscAdv()V

    .line 388
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "startDiscovery Hash ***"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash_byte:[B

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->hash2_byte:[B

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    return-void
.end method

.method public stopCentral()V
    .locals 3

    .prologue
    .line 281
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "stopCentral"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->isBleOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopScan(Z)V

    .line 284
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    .line 286
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopAutoScanTimer()V

    .line 287
    return-void
.end method

.method public stopCmdRequest()V
    .locals 1

    .prologue
    .line 1025
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->clearProbeData()V

    .line 1026
    return-void
.end method

.method public stopDiscovery()V
    .locals 3

    .prologue
    .line 394
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "stopDiscovery"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopDiscAdv()V

    .line 397
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->setCentralRunningMode(Z)V

    .line 398
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->clearDiscoveryListener()V

    .line 399
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 400
    return-void
.end method

.method public stopScanForPopup()V
    .locals 3

    .prologue
    .line 290
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "stopScanForPopup"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopScan(Z)V

    .line 292
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    .line 293
    return-void
.end method

.method public terminate()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 296
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "terminate"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mLastCmd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 298
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 300
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 301
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 302
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 303
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 304
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 305
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 306
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryReceiveHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->removeMessages(I)V

    .line 308
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopAutoScanTimer()V

    .line 309
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 310
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    .line 312
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->unregisterIntentFilter()V

    .line 314
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 315
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 316
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 317
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 320
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mHandlerThread:Landroid/os/HandlerThread;

    .line 325
    return-void

    .line 317
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
