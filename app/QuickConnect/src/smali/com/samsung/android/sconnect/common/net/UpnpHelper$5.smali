.class Lcom/samsung/android/sconnect/common/net/UpnpHelper$5;
.super Ljava/lang/Object;
.source "UpnpHelper.java"

# interfaces
.implements Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/UpnpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 329
    const-string v0, "UpnpHelper"

    const-string v1, "mImageViewerFinderListener"

    const-string v2, "onDeviceAdded"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    const/4 v1, 0x2

    # invokes: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DeviceAdd(Lcom/samsung/android/allshare/Device;I)Z
    invoke-static {v0, p2, v1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$800(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/Device;I)Z

    .line 331
    return-void
.end method

.method public onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 323
    const-string v0, "UpnpHelper"

    const-string v1, "mImageViewerFinderListener"

    const-string v2, "onDeviceRemoved"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    const/4 v1, 0x2

    # invokes: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DeviceRemove(Lcom/samsung/android/allshare/Device;I)Z
    invoke-static {v0, p2, v1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$700(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/Device;I)Z

    .line 325
    return-void
.end method
