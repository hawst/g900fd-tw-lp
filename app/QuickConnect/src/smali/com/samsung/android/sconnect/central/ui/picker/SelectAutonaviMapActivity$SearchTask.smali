.class Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;
.super Landroid/os/AsyncTask;
.source "SelectAutonaviMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V
    .locals 0

    .prologue
    .line 823
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;

    .prologue
    .line 823
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 823
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->doInBackground([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "locationName"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 826
    const-string v3, "SelectAutonaviMapActivity"

    const-string v4, ""

    const-string v5, "SearchTask - doInBackground"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    new-instance v2, Landroid/location/Geocoder;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 829
    .local v2, "geocoder":Landroid/location/Geocoder;
    const/4 v0, 0x0

    .line 833
    .local v0, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    const/4 v3, 0x0

    :try_start_0
    aget-object v3, p1, v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 837
    :goto_0
    return-object v0

    .line 834
    :catch_0
    move-exception v1

    .line 835
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 823
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 842
    .local p1, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v2, "SearchTask - onPostExecute"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->markAddress(Ljava/util/List;)V

    .line 845
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 846
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 850
    :cond_0
    return-void
.end method
