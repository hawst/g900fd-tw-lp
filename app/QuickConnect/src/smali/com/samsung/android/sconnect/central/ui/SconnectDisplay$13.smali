.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$13;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 1454
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$13;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 1458
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 1485
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v4, 0x0

    :goto_1
    return v4

    .line 1460
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1461
    .local v3, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    const-string v5, "SconnectDisplay"

    const-string v6, "DeviceImage onLongClick"

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v1

    .line 1463
    .local v1, "deviceType":I
    if-eq v1, v4, :cond_1

    const/4 v5, 0x2

    if-ne v1, v5, :cond_2

    :cond_1
    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v5

    const/4 v6, 0x4

    if-eq v5, v6, :cond_2

    .line 1465
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$13;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v5, v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v6, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v5, v6, :cond_0

    .line 1466
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$13;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f090117

    invoke-static {v5, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 1469
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$13;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->startMultiSelecteMode()V

    .line 1470
    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v5

    const v6, 0x7f0c001e

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1472
    .local v0, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 1476
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    :cond_2
    const-string v4, "SconnectDisplay"

    const-string v5, "onLongClick"

    const-string v6, "multi selection mode is activate only for MOBILE & TABLET that are not BT discovered"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1481
    .end local v1    # "deviceType":I
    .end local v3    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1482
    .local v2, "iconItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$13;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showFavoriteDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto :goto_0

    .line 1458
    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0015
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
