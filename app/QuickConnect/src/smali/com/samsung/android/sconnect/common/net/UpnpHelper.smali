.class public Lcom/samsung/android/sconnect/common/net/UpnpHelper;
.super Ljava/lang/Object;
.source "UpnpHelper.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/IDiscoveryAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/common/net/UpnpHelper$7;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UpnpHelper"


# instance fields
.field private final mAVPlayerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private mContext:Landroid/content/Context;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

.field private final mFileReceiverFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private mFileServiceListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

.field private mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

.field private final mImageViewerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

.field private mMediaServiceListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

.field private mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRegisterIntent:Z

.field private mUnbindFileService:Z

.field private mUpnpDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 52
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .line 54
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 55
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    .line 59
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUnbindFileService:Z

    .line 224
    new-instance v0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;-><init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    .line 254
    new-instance v0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;-><init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    .line 286
    new-instance v0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper$3;-><init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileReceiverFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 300
    new-instance v0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper$4;-><init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mAVPlayerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 320
    new-instance v0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper$5;-><init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mImageViewerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 483
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mRegisterIntent:Z

    .line 484
    new-instance v0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;-><init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 62
    const-string v0, "UpnpHelper"

    const-string v1, "UpnpHelper"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    .line 64
    return-void
.end method

.method private ConnectASF()V
    .locals 4

    .prologue
    .line 174
    const-string v1, "UpnpHelper"

    const-string v2, "ConnectASF"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-nez v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    const-string v3, "com.samsung.android.allshare.media"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    if-nez v1, :cond_1

    .line 181
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    const-string v3, "com.samsung.android.allshare.file"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    .line 185
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mRegisterIntent:Z

    if-nez v1, :cond_2

    .line 186
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mRegisterIntent:Z

    .line 187
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 188
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 189
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 191
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_2
    return-void
.end method

.method private DeviceAdd(Lcom/samsung/android/allshare/Device;I)Z
    .locals 15
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;
    .param p2, "type"    # I

    .prologue
    .line 382
    const/4 v14, 0x0

    .line 383
    .local v14, "update":Z
    const/4 v12, 0x0

    .line 384
    .local v12, "oldDevice":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    const/4 v11, 0x0

    .line 386
    .local v11, "newDevice":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    const/4 v7, 0x0

    .line 387
    .local v7, "p2pMac":Ljava/lang/String;
    const/4 v2, 0x4

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 388
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getIPAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getMacAddrFromArpTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 389
    .local v10, "mac":Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getP2pMacFromConnectedMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 390
    if-nez v7, :cond_0

    .line 391
    const-string v2, "UpnpHelper"

    const-string v3, "DeviceAdd"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ignore - p2pMac(arp) disconnected :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const/4 v2, 0x0

    .line 439
    .end local v10    # "mac":Ljava/lang/String;
    :goto_0
    return v2

    .line 394
    .restart local v10    # "mac":Ljava/lang/String;
    :cond_0
    const-string v2, "UpnpHelper"

    const-string v3, "DeviceAdd"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "p2pMac(arp): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    .end local v10    # "mac":Ljava/lang/String;
    :cond_1
    :goto_1
    new-instance v1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getIPAddress()Ljava/lang/String;

    move-result-object v5

    move/from16 v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 403
    .local v1, "deviceUpnp":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    const/4 v2, 0x4

    move/from16 v0, p2

    if-eq v0, v2, :cond_4

    .line 404
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    .line 405
    .local v8, "dev":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    invoke-virtual {v1, v8}, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 406
    move-object v12, v8

    .line 407
    invoke-virtual {v8, v1}, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->updateUpnp(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V

    .line 408
    move-object v11, v8

    .line 409
    const/4 v14, 0x1

    goto :goto_2

    .line 395
    .end local v1    # "deviceUpnp":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    .end local v8    # "dev":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    .end local v9    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getP2pMacAddress()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getP2pMacAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x8

    if-le v2, v3, :cond_1

    .line 396
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getP2pMacAddress()Ljava/lang/String;

    move-result-object v7

    .line 397
    const-string v2, "UpnpHelper"

    const-string v3, "DeviceAdd"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "p2pMac: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 414
    .restart local v1    # "deviceUpnp":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    :cond_4
    const-string v2, "UpnpHelper"

    const-string v3, "DeviceAdd"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " should be update : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    if-eqz v14, :cond_6

    .line 416
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 417
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    new-instance v13, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    invoke-direct {v13, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 420
    .local v13, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v13, v11}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V

    .line 422
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v2, :cond_5

    .line 423
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v2, v13}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 425
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 427
    .end local v13    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    new-instance v13, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    invoke-direct {v13, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 430
    .restart local v13    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v13, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V

    .line 432
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v2, :cond_7

    .line 433
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v2, v13}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 436
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v2, :cond_8

    .line 437
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    :cond_8
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private DeviceRemove(Lcom/samsung/android/allshare/Device;I)Z
    .locals 12
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;
    .param p2, "type"    # I

    .prologue
    .line 335
    const/4 v7, 0x0

    .line 336
    .local v7, "deleted":Z
    const/4 v10, 0x0

    .line 337
    .local v10, "newDevice":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getIPAddress()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 339
    .local v0, "deviceUpnp":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 340
    const/4 v7, 0x1

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    .line 343
    .local v8, "dev":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    if-eqz v7, :cond_5

    .line 352
    .end local v8    # "dev":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    :cond_2
    if-eqz v7, :cond_7

    .line 353
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 355
    new-instance v11, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    invoke-direct {v11, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 356
    .local v11, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    if-eqz v10, :cond_6

    .line 357
    invoke-virtual {v11, v10}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V

    .line 361
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v1, :cond_3

    .line 362
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v1, v11}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 364
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 365
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 367
    :cond_4
    const/4 v1, 0x1

    .line 377
    :goto_2
    return v1

    .line 346
    .end local v11    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .restart local v8    # "dev":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    :cond_5
    invoke-virtual {v0, v8}, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 347
    invoke-virtual {v8, v0}, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->DeleteUpnp(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)Z

    move-result v7

    .line 348
    move-object v10, v8

    goto :goto_0

    .line 359
    .end local v8    # "dev":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    .restart local v11    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_6
    invoke-virtual {v11, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V

    goto :goto_1

    .line 369
    .end local v11    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 370
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    new-instance v11, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    invoke-direct {v11, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 373
    .restart local v11    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v11, v10}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V

    .line 374
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v1, :cond_8

    .line 375
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v1, v11}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 377
    :cond_8
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private DisconnectASF()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 194
    const-string v0, "UpnpHelper"

    const-string v1, "DisconnectASF"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 198
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 199
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 203
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    if-eqz v0, :cond_2

    .line 207
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->checkEnabledP2pNetwork()Z

    move-result v0

    if-eq v0, v4, :cond_2

    .line 208
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/allshare/file/FileDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 210
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    .line 213
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    if-eqz v0, :cond_3

    .line 214
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->checkEnabledP2pNetwork()Z

    move-result v0

    if-eq v0, v4, :cond_4

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 216
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 217
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->unregisterIntent()V

    .line 222
    :cond_3
    :goto_0
    return-void

    .line 219
    :cond_4
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUnbindFileService:Z

    goto :goto_0
.end method

.method private declared-synchronized RemoveFileServerDevice()V
    .locals 8

    .prologue
    .line 523
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 524
    .local v0, "copyUpnpDeviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 525
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 526
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    .line 527
    .local v1, "deviceUpnp":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    if-eqz v1, :cond_0

    iget v4, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    .line 528
    const-string v4, "UpnpHelper"

    const-string v5, "RemoveFileServerDevice"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "REMOVE : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 532
    new-instance v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 534
    .local v3, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v3, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V

    .line 536
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v4, :cond_1

    .line 537
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v4, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 539
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 540
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 523
    .end local v0    # "copyUpnpDeviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;>;"
    .end local v1    # "deviceUpnp":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;>;"
    .end local v3    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 544
    .restart local v0    # "copyUpnpDeviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;>;"
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;>;"
    :cond_2
    monitor-exit p0

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileServiceProvider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/file/FileServiceProvider;)Lcom/samsung/android/allshare/file/FileServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    .param p1, "x1"    # Lcom/samsung/android/allshare/file/FileServiceProvider;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUnbindFileService:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUnbindFileService:Z

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/file/FileDeviceFinder;)Lcom/samsung/android/allshare/file/FileDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    .param p1, "x1"    # Lcom/samsung/android/allshare/file/FileDeviceFinder;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->unregisterIntent()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileReceiverFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mAVPlayerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mImageViewerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/Device;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    .param p1, "x1"    # Lcom/samsung/android/allshare/Device;
    .param p2, "x2"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DeviceRemove(Lcom/samsung/android/allshare/Device;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/Device;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    .param p1, "x1"    # Lcom/samsung/android/allshare/Device;
    .param p2, "x2"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DeviceAdd(Lcom/samsung/android/allshare/Device;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->RemoveFileServerDevice()V

    return-void
.end method

.method private getDeviceListFromASF()V
    .locals 8

    .prologue
    .line 444
    const-string v5, "UpnpHelper"

    const-string v6, "getDeviceListFromASF"

    const-string v7, ""

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 447
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v5, :cond_2

    .line 448
    const-string v5, "UpnpHelper"

    const-string v6, "getDeviceListFromASF"

    const-string v7, "really try to get MediaDevices"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v0

    .line 450
    .local v0, "AVPlayers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device;

    .line 451
    .local v3, "device":Lcom/samsung/android/allshare/Device;
    instance-of v5, v3, Lcom/samsung/android/allshare/media/AVPlayer;

    if-eqz v5, :cond_0

    move-object v5, v3

    check-cast v5, Lcom/samsung/android/allshare/media/AVPlayer;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/media/AVPlayer;->isSupportVideo()Z

    move-result v5

    if-nez v5, :cond_0

    .line 452
    const/16 v5, 0x9

    invoke-direct {p0, v3, v5}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DeviceAdd(Lcom/samsung/android/allshare/Device;I)Z

    .line 454
    :cond_0
    const/4 v5, 0x1

    invoke-direct {p0, v3, v5}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DeviceAdd(Lcom/samsung/android/allshare/Device;I)Z

    goto :goto_0

    .line 457
    .end local v3    # "device":Lcom/samsung/android/allshare/Device;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v2

    .line 459
    .local v2, "ImageViewers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device;

    .line 460
    .restart local v3    # "device":Lcom/samsung/android/allshare/Device;
    const/4 v5, 0x2

    invoke-direct {p0, v3, v5}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DeviceAdd(Lcom/samsung/android/allshare/Device;I)Z

    goto :goto_1

    .line 464
    .end local v0    # "AVPlayers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    .end local v2    # "ImageViewers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    .end local v3    # "device":Lcom/samsung/android/allshare/Device;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    if-eqz v5, :cond_3

    .line 465
    const-string v5, "UpnpHelper"

    const-string v6, "getDeviceListFromASF"

    const-string v7, "really try to get FileDevices"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    sget-object v6, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/file/FileDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v1

    .line 468
    .local v1, "FileDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device;

    .line 469
    .restart local v3    # "device":Lcom/samsung/android/allshare/Device;
    const/4 v5, 0x4

    invoke-direct {p0, v3, v5}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DeviceAdd(Lcom/samsung/android/allshare/Device;I)Z

    goto :goto_2

    .line 472
    .end local v1    # "FileDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    .end local v3    # "device":Lcom/samsung/android/allshare/Device;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method private unregisterIntent()V
    .locals 2

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mRegisterIntent:Z

    if-eqz v0, :cond_0

    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mRegisterIntent:Z

    .line 477
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 481
    :cond_0
    return-void
.end method


# virtual methods
.method public checkEnabledNetwork()Z
    .locals 6

    .prologue
    .line 121
    const-string v2, "UpnpHelper"

    const-string v3, "checkEnabledNetwork"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->checkEnabledWifiNetwork()Z

    move-result v1

    .line 123
    .local v1, "wifiConnected":Z
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->checkEnabledP2pNetwork()Z

    move-result v0

    .line 125
    .local v0, "p2pConnected":Z
    const-string v2, "UpnpHelper"

    const-string v3, "checkEnabledNetwork"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[wifiConnected]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " [p2pConnected]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    .line 128
    :cond_0
    const/4 v2, 0x1

    .line 130
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public checkEnabledP2pNetwork()Z
    .locals 4

    .prologue
    .line 149
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 151
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 153
    .local v1, "netInfoP2p":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    const/4 v2, 0x1

    .line 156
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public checkEnabledWifiNetwork()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 134
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 136
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 137
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_1

    .line 138
    const-string v3, "UpnpHelper"

    const-string v4, "checkEnabledWifiNetwork"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WIFI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDeviceList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    const-string v0, "UpnpHelper"

    const-string v1, "getDeviceList"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public initiate()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->ConnectASF()V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    .line 69
    return-void
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 160
    const-string v0, "UpnpHelper"

    const-string v1, "refresh"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->refresh()V

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    if-eqz v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/file/FileDeviceFinder;->refresh()V

    .line 171
    :cond_2
    return-void
.end method

.method public startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .prologue
    .line 78
    const-string v0, "UpnpHelper"

    const-string v1, "startDiscovery"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "listener = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 81
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUpnpDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 84
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-nez v0, :cond_0

    .line 85
    const-string v0, "UpnpHelper"

    const-string v1, "startDiscovery"

    const-string v2, "mMediaServiceProvider is null : can\'t start ASF Media discovery"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;

    if-nez v0, :cond_2

    .line 95
    const-string v0, "UpnpHelper"

    const-string v1, "startDiscovery"

    const-string v2, "mFileServiceProvider is null : can\'t start ASF File discovery"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->getDeviceListFromASF()V

    .line 105
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-nez v0, :cond_1

    .line 88
    const-string v0, "UpnpHelper"

    const-string v1, "startDiscovery"

    const-string v2, "mMediaDeviceFinder is null : can\'t start ASF Media discovery"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->refresh()V

    goto :goto_0

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    if-nez v0, :cond_3

    .line 98
    const-string v0, "UpnpHelper"

    const-string v1, "startDiscovery"

    const-string v2, "mFileDeviceFinder is null : can\'t start ASF File discovery"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 101
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/file/FileDeviceFinder;->refresh()V

    goto :goto_1
.end method

.method public stopDiscovery()V
    .locals 4

    .prologue
    .line 109
    const-string v0, "UpnpHelper"

    const-string v1, "stopDiscovery"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDiscoveryListener = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 111
    return-void
.end method

.method public terminate()V
    .locals 0

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->stopDiscovery()V

    .line 73
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->DisconnectASF()V

    .line 74
    return-void
.end method
