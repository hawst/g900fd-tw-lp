.class final Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$3;
.super Ljava/util/HashMap;
.source "SamsungAppsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 86
    const-string v0, "com.sec.android.sidesync30"

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_SIDESYNC_SOURCE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    const-string v0, "com.samsung.groupcast"

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_GROUPPLAY:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    const-string v0, "com.sec.android.emeeting"

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_EMEETING:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    const-string v0, "com.sec.android.emeeting.b2c.hancom"

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_EMEETING:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const-string v0, "com.sec.android.app.alltogether"

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_TOGETHER:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    return-void
.end method
