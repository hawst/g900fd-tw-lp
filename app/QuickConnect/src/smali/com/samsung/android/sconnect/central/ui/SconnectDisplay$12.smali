.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 1372
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/high16 v7, 0x24000000

    const/high16 v6, 0x10000000

    const/4 v5, 0x0

    .line 1376
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1414
    :goto_0
    return v5

    .line 1378
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const-class v3, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1379
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1381
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1382
    const-string v2, "isTutorial"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1383
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1386
    .end local v1    # "intent":Landroid/content/Intent;
    :pswitch_1
    const-string v2, "SconnectDisplay"

    const-string v3, "onClick : help menu!"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1387
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->launchHelpApp()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    goto :goto_0

    .line 1390
    :pswitch_2
    const-string v2, "SconnectDisplay"

    const-string v3, "onClick : set device name menu!"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.DEVICE_NAME_DIALOG"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1394
    .restart local v1    # "intent":Landroid/content/Intent;
    const/high16 v2, 0x800000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1395
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1396
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 1397
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SconnectDisplay"

    const-string v3, "Exception occurs on DEVICE_NAME_DIALOG"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1398
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0900e6

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1403
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_3
    const-string v2, "SconnectDisplay"

    const-string v3, "onClick : china_popup_menu"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const-class v3, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1405
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1407
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1408
    const-string v2, "isTutorial"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1409
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1376
    :pswitch_data_0
    .packed-switch 0x7f0c00ad
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
