.class public Lcom/samsung/android/sconnect/common/util/Const$ContentsType;
.super Ljava/lang/Object;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/util/Const;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContentsType"
.end annotation


# static fields
.field public static final AUDIO:I = 0x3

.field public static final CONTACT:I = 0x8

.field public static final DOCUMENT:I = 0x11

.field public static final EMAIL:I = 0xe

.field public static final FILE:I = 0xd

.field public static final IMAGE:I = 0x1

.field public static final LOCATION:I = 0x7

.field public static final MEMO:I = 0xa

.field public static final NONE:I = 0x0

.field public static final SAMSUNG_LINK:I = 0x10

.field public static final SMEMO:I = 0x4

.field public static final SNOTE:I = 0xb

.field public static final SPLANNER_EVENT:I = 0x5

.field public static final SPLANNER_SCHEDULE:I = 0x6

.field public static final STORY_ALBUM:I = 0xf

.field public static final TEXT:I = 0xc

.field public static final VIDEO:I = 0x2

.field public static final WEBPAGE:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
