.class Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
.super Landroid/os/Handler;
.source "PreDiscoveryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PreDiscoveryWorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1569
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .line 1570
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1571
    return-void
.end method

.method private handleStartStop(ZZ)V
    .locals 6
    .param p1, "isStart"    # Z
    .param p2, "isCmd"    # Z

    .prologue
    const/16 v2, 0x1a

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1574
    new-array v0, v2, [B

    .line 1576
    .local v0, "bAdvData":[B
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopAdv()V

    .line 1578
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1579
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mBleMac:[B
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)[B

    move-result-object v1

    const/16 v2, 0xc

    const/4 v3, 0x6

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1580
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setProbeData([B[B)V

    .line 1582
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1700(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsCommandRunning:Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1800(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1583
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    invoke-virtual {v1, v5, v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startAdv(Z[B[B)V

    .line 1594
    :goto_0
    return-void

    .line 1584
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryRespRunning:Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1585
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isPossibleP2pProbe()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mSupportP2pResponse:Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPowerManager:Landroid/os/PowerManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/os/PowerManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1587
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    const-wide/16 v2, 0x1b58

    invoke-virtual {v1, v2, v3, v5}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startListenP2p(JZ)V

    goto :goto_0

    .line 1589
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mAdvData:[B

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mRespData:[B

    invoke-virtual {v1, v4, v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startAdv(Z[B[B)V

    goto :goto_0

    .line 1592
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopPreDiscovery()V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1599
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1695
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1601
    :pswitch_1
    const-string v0, "PreDiscoveryHelper"

    const-string v4, "AdvHandler"

    const-string v5, "MSG_START_DISCOVER_REQ_ADV"

    invoke-static {v0, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1602
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1702(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    .line 1604
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateAdvData()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 1605
    const/4 v0, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->handleStartStop(ZZ)V

    .line 1607
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mNeedP2pFind:Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1608
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    const-wide/16 v4, 0x1b58

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-virtual {v0, v4, v5, v9, v10}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startFindP2p(JZZ)V

    .line 1609
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mNeedP2pFind:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$202(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    goto :goto_0

    .line 1613
    :pswitch_2
    const-string v0, "PreDiscoveryHelper"

    const-string v4, "AdvHandler"

    const-string v5, "MSG_STOP_DISCOVER_REQ_ADV"

    invoke-static {v0, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1614
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1702(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    .line 1616
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryReq()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 1617
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateAdvData()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 1618
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->handleStartStop(ZZ)V

    .line 1620
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;

    move-result-object v4

    monitor-enter v4

    .line 1621
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 1622
    .local v6, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v5, -0x1

    if-ne v0, v5, :cond_1

    .line 1623
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1624
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto :goto_1

    .line 1628
    .end local v6    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v7    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1630
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;
    invoke-static {v4, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2302(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1633
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->startAutoScanTimer()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    goto/16 :goto_0

    .line 1636
    .end local v7    # "i$":Ljava/util/Iterator;
    :pswitch_3
    const-string v0, "PreDiscoveryHelper"

    const-string v4, "AdvHandler"

    const-string v5, "MSG_START_DISCOVER_RESP_ADV"

    invoke-static {v0, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1637
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/sconnect/common/net/BleHelper;->setLastRespAdvTime(J)V

    .line 1638
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryRespRunning:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1902(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    .line 1640
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateAdvData()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 1641
    const/4 v0, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->handleStartStop(ZZ)V

    .line 1643
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1644
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 1645
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    const/4 v4, 0x4

    const-wide/16 v10, 0xbb8

    invoke-virtual {v0, v4, v10, v11}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1650
    :pswitch_4
    const-string v0, "PreDiscoveryHelper"

    const-string v4, "AdvHandler"

    const-string v5, "MSG_STOP_DISCOVER_RESP_ADV"

    invoke-static {v0, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1651
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryRespRunning:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1902(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    .line 1653
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryResp()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 1654
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateAdvData()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 1655
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->handleStartStop(ZZ)V

    goto/16 :goto_0

    .line 1658
    :pswitch_5
    const-string v0, "PreDiscoveryHelper"

    const-string v4, "AdvHandler"

    const-string v5, "MSG_START_CMD_REQ_ADV"

    invoke-static {v0, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1659
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsCommandRunning:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1802(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    .line 1661
    const/4 v0, 0x1

    const/4 v4, 0x1

    invoke-direct {p0, v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->handleStartStop(ZZ)V

    .line 1663
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_3

    const-string v0, "ONLY_BLE"

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1664
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isPossibleP2pProbe()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1665
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    const-wide/16 v4, 0x1b58

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {v0, v4, v5, v9, v10}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startFindP2p(JZZ)V

    .line 1669
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1670
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 1671
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    const/4 v4, 0x6

    const-wide/16 v10, 0x1b58

    invoke-virtual {v0, v4, v10, v11}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1676
    :pswitch_6
    const-string v0, "PreDiscoveryHelper"

    const-string v4, "AdvHandler"

    const-string v5, "MSG_STOP_CMD_REQ_ADV"

    invoke-static {v0, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1677
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsCommandRunning:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1802(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    .line 1679
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->clearCmdData()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 1680
    const/4 v0, 0x0

    const/4 v4, 0x1

    invoke-direct {p0, v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->handleStartStop(ZZ)V

    goto/16 :goto_0

    .line 1683
    :pswitch_7
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Landroid/content/Intent;

    .line 1684
    .local v8, "intent":Landroid/content/Intent;
    if-eqz v8, :cond_0

    .line 1685
    const-string v0, "PreDiscoveryHelper"

    const-string v4, "AdvHandler"

    const-string v5, "MSG_SEND_CMD: "

    invoke-static {v0, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1686
    const-string v0, "RES"

    const/4 v4, 0x0

    invoke-virtual {v8, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 1687
    .local v3, "result":Z
    const-string v0, "KEY"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1688
    .local v1, "id":Ljava/lang/String;
    const-string v0, "ADDRESS"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1689
    .local v2, "address":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v4, 0x0

    const/4 v5, 0x0

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendConfirmCommand(Ljava/lang/String;Ljava/lang/String;Z[BZ)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/lang/String;Ljava/lang/String;Z[BZ)V

    goto/16 :goto_0

    .line 1599
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method
