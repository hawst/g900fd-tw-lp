.class Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;
.super Ljava/lang/Object;
.source "UpdateFoundDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->showUpdatePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 95
    const-string v0, "UpdateFoundDialog"

    const-string v1, "onDismiss"

    const-string v2, "Dismiss"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mSconnectDisplayActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->access$000(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mSconnectDisplayActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->access$000(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mTutorialActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->access$100(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mTutorialActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->access$100(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->finish()V

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->finish()V

    .line 103
    return-void
.end method
