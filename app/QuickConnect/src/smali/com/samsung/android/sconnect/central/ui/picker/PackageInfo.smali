.class Lcom/samsung/android/sconnect/central/ui/picker/PackageInfo;
.super Ljava/lang/Object;
.source "MediaSourceSelector.java"


# static fields
.field public static final CALENDAR:Ljava/lang/String; = "com.android.calendar"

.field public static final CAMERA:Ljava/lang/String; = "com.sec.android.app.camera"

.field public static final CONTACT:Ljava/lang/String; = "com.android.contacts"

.field public static final GALLERY:Ljava/lang/String; = "com.cooliris.media"

.field public static final GALLERY3D:Ljava/lang/String; = "com.sec.android.gallery3d"

.field public static final MAP:Ljava/lang/String; = "com.google.android.apps.maps"

.field public static final MEMO:Ljava/lang/String; = "com.samsung.android.app.memo"

.field public static final MUSIC:Ljava/lang/String; = "com.sec.android.app.music"

.field public static final MUSICCOMMON:Ljava/lang/String; = "com.sec.android.mmapp"

.field public static final MYFILES:Ljava/lang/String; = "com.sec.android.app.myfiles"

.field public static final MYFILES_ACCESS_CLASS_PHONE:Ljava/lang/String; = "com.sec.android.app.myfiles.fileselector.SingleSelectorActivity"

.field public static final SMEMO:Ljava/lang/String; = "com.sec.android.widgetapp.diotek.smemo"

.field public static final SNOTE:Ljava/lang/String; = "com.samsung.android.snote"

.field public static final TASK:Ljava/lang/String; = "com.android.task"

.field public static final VOICENOTE:Ljava/lang/String; = "com.sec.android.app.voicenote"

.field public static final VOICERECORDER:Ljava/lang/String; = "com.sec.android.app.voicenote"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
