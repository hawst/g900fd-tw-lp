.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showNetworkPermissionDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x0

    .line 547
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 548
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionWifiCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mNetworkPermissionDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 571
    :goto_0
    return-void

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiSetting:I
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v0

    if-lez v0, :cond_1

    .line 552
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "wlan_permission_available"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 556
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 557
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionBtCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mNetworkPermissionDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    goto :goto_0

    .line 560
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtSetting:I
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v0

    if-lez v0, :cond_3

    .line 561
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "bluetooth_security_on_check"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 565
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionUserinfoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_4

    .line 566
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mNetworkPermissionDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    goto :goto_0

    .line 569
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateCheck()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    .line 570
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startSconnectDisplay()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    goto :goto_0
.end method
