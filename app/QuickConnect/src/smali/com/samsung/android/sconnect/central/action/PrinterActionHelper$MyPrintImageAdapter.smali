.class Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;
.super Landroid/print/PrintDocumentAdapter;
.source "PrinterActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyPrintImageAdapter"
.end annotation


# instance fields
.field private mActivityContext:Landroid/content/Context;

.field private mAttributes:Landroid/print/PrintAttributes;

.field public mBitmap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPagesCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 347
    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    .line 341
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mPagesCount:I

    .line 342
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mFileList:Ljava/util/ArrayList;

    .line 344
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mActivityContext:Landroid/content/Context;

    .line 348
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 351
    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    .line 341
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mPagesCount:I

    .line 342
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mFileList:Ljava/util/ArrayList;

    .line 344
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mActivityContext:Landroid/content/Context;

    .line 352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    .line 353
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mFileList:Ljava/util/ArrayList;

    .line 354
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mFileList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mFileList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mPagesCount:I

    .line 357
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mActivityContext:Landroid/content/Context;

    .line 358
    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 5

    .prologue
    .line 482
    const-string v2, "PrinterActionHelper"

    const-string v3, "MyPrintImageAdapter"

    const-string v4, "onFinish"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 484
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 485
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 486
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 491
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onFinish()V

    .line 492
    const-string v2, "PrinterActionHelper"

    const-string v3, "MyPrintImageAdapter"

    const-string v4, "onFinish - 2"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    return-void
.end method

.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 10
    .param p1, "oldPrintAttributes"    # Landroid/print/PrintAttributes;
    .param p2, "newPrintAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "layoutResultCallback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
    .param p5, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 370
    const-string v6, "PrinterActionHelper"

    const-string v7, "MyPrintImageAdapter"

    const-string v8, "onLayout"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mAttributes:Landroid/print/PrintAttributes;

    .line 374
    new-instance v6, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter$1;-><init>(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;)V

    invoke-virtual {p3, v6}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 381
    iget v6, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mPagesCount:I

    if-lez v6, :cond_5

    .line 383
    const/4 v1, 0x0

    .line 385
    .local v1, "count":I
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 386
    const-string v6, "PrinterActionHelper"

    const-string v7, "MyPrintImageAdapter"

    const-string v8, "onLayout - clear mBitmap"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 388
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 389
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 393
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 396
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mFileList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 397
    .local v5, "path":Ljava/lang/String;
    const-string v6, "PrinterActionHelper"

    const-string v7, "MyPrintImageAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onLayout ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "count":I
    .local v2, "count":I
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 399
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_3

    .line 400
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move v1, v2

    .line 402
    .end local v2    # "count":I
    .restart local v1    # "count":I
    goto :goto_1

    .line 404
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "path":Ljava/lang/String;
    :cond_4
    new-instance v7, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mFileList:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v7, v6}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {v7, v6}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mPagesCount:I

    invoke-virtual {v6, v7}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v4

    .line 407
    .local v4, "info":Landroid/print/PrintDocumentInfo;
    const-string v6, "PrinterActionHelper"

    const-string v7, "MyPrintImageAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onLayout - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Landroid/print/PrintDocumentInfo;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const/4 v6, 0x1

    invoke-virtual {p4, v4, v6}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 413
    .end local v1    # "count":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "info":Landroid/print/PrintDocumentInfo;
    :goto_2
    return-void

    .line 411
    :cond_5
    const-string v6, "failed"

    invoke-virtual {p4, v6}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 362
    const-string v0, "PrinterActionHelper"

    const-string v1, "MyPrintImageAdapter"

    const-string v2, "onStart"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onStart()V

    .line 364
    return-void
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 18
    .param p1, "pageRanges"    # [Landroid/print/PageRange;
    .param p2, "fileDescriptor"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "writeResultCallback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    .line 418
    const-string v14, "PrinterActionHelper"

    const-string v15, "MyPrintImageAdapter"

    const-string v16, "onWrite"

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :try_start_0
    const-string v14, "PrinterActionHelper"

    const-string v15, "MyPrintImageAdapter"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "mPagesCount "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mPagesCount:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    new-instance v9, Landroid/print/pdf/PrintedPdfDocument;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mActivityContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mAttributes:Landroid/print/PrintAttributes;

    invoke-direct {v9, v14, v15}, Landroid/print/pdf/PrintedPdfDocument;-><init>(Landroid/content/Context;Landroid/print/PrintAttributes;)V

    .line 424
    .local v9, "pdfDocument":Landroid/print/pdf/PrintedPdfDocument;
    const/4 v8, 0x0

    .line 425
    .local v8, "pageCount":I
    const/4 v11, 0x0

    .local v11, "start":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mPagesCount:I

    add-int/lit8 v4, v14, -0x1

    .line 432
    .local v4, "end":I
    move v5, v11

    .local v5, "i":I
    :goto_0
    if-gt v5, v4, :cond_2

    .line 433
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_0

    .line 434
    invoke-virtual {v9, v8}, Landroid/print/pdf/PrintedPdfDocument;->startPage(I)Landroid/graphics/pdf/PdfDocument$Page;

    move-result-object v7

    .line 436
    .local v7, "page":Landroid/graphics/pdf/PdfDocument$Page;
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/pdf/PdfDocument$Page;->getInfo()Landroid/graphics/pdf/PdfDocument$PageInfo;

    move-result-object v14

    invoke-virtual {v14}, Landroid/graphics/pdf/PdfDocument$PageInfo;->getContentRect()Landroid/graphics/Rect;

    move-result-object v14

    invoke-direct {v2, v14}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 437
    .local v2, "content":Landroid/graphics/RectF;
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 440
    .local v6, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    int-to-float v14, v14

    div-float/2addr v15, v14

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v16

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    int-to-float v14, v14

    div-float v14, v16, v14

    invoke-static {v15, v14}, Ljava/lang/Math;->min(FF)F

    move-result v10

    .line 442
    .local v10, "scale":F
    invoke-virtual {v6, v10, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 445
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v10

    sub-float v14, v15, v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float v12, v14, v15

    .line 447
    .local v12, "translateX":F
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v10

    sub-float v14, v15, v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float v13, v14, v15

    .line 449
    .local v13, "translateY":F
    invoke-virtual {v6, v12, v13}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 452
    invoke-virtual {v7}, Landroid/graphics/pdf/PdfDocument$Page;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;->mBitmap:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/Bitmap;

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v6, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 455
    invoke-virtual {v9, v7}, Landroid/print/pdf/PrintedPdfDocument;->finishPage(Landroid/graphics/pdf/PdfDocument$Page;)V

    .line 456
    add-int/lit8 v8, v8, 0x1

    .line 432
    .end local v2    # "content":Landroid/graphics/RectF;
    .end local v6    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "page":Landroid/graphics/pdf/PdfDocument$Page;
    .end local v10    # "scale":F
    .end local v12    # "translateX":F
    .end local v13    # "translateY":F
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 458
    :cond_0
    const-string v14, "PrinterActionHelper"

    const-string v15, "MyPrintImageAdapter"

    const-string v16, "onWrite - Invalid image received"

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 470
    .end local v4    # "end":I
    .end local v5    # "i":I
    .end local v8    # "pageCount":I
    .end local v9    # "pdfDocument":Landroid/print/pdf/PrintedPdfDocument;
    .end local v11    # "start":I
    :catchall_0
    move-exception v14

    if-eqz p2, :cond_1

    .line 472
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 474
    :cond_1
    :goto_2
    throw v14

    .line 462
    .restart local v4    # "end":I
    .restart local v5    # "i":I
    .restart local v8    # "pageCount":I
    .restart local v9    # "pdfDocument":Landroid/print/pdf/PrintedPdfDocument;
    .restart local v11    # "start":I
    :cond_2
    :try_start_2
    new-instance v14, Ljava/io/FileOutputStream;

    invoke-virtual/range {p2 .. p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-virtual {v9, v14}, Landroid/print/pdf/PrintedPdfDocument;->writeTo(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 466
    :goto_3
    :try_start_3
    invoke-virtual {v9}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 468
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 470
    if-eqz p2, :cond_3

    .line 472
    :try_start_4
    invoke-virtual/range {p2 .. p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 477
    :cond_3
    :goto_4
    const-string v14, "PrinterActionHelper"

    const-string v15, "MyPrintImageAdapter"

    const-string v16, "onWrite - finished "

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    return-void

    .line 463
    :catch_0
    move-exception v3

    .line 464
    .local v3, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 473
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v14

    goto :goto_4

    .end local v4    # "end":I
    .end local v5    # "i":I
    .end local v8    # "pageCount":I
    .end local v9    # "pdfDocument":Landroid/print/pdf/PrintedPdfDocument;
    .end local v11    # "start":I
    :catch_2
    move-exception v15

    goto :goto_2
.end method
