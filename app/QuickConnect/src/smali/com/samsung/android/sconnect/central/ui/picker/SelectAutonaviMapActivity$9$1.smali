.class Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9$1;
.super Ljava/lang/Object;
.source "SelectAutonaviMapActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->onMapLongClick(Lcom/amap/api/maps/model/LatLng;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;)V
    .locals 0

    .prologue
    .line 598
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$500(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$500(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 603
    :cond_0
    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$600()Landroid/location/LocationManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationListener:Landroid/location/LocationListener;

    if-eqz v0, :cond_1

    .line 604
    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$600()Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 606
    :cond_1
    return-void
.end method
