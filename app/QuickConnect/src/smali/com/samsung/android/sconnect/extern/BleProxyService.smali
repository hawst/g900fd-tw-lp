.class public Lcom/samsung/android/sconnect/extern/BleProxyService;
.super Landroid/app/Service;
.source "BleProxyService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BleProxyService"

.field public static mBleProxyCallbackHash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sconnect/extern/IBleProxyCallback;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBleProxyService:Lcom/samsung/android/sconnect/extern/IBleProxyService$Stub;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mContext:Landroid/content/Context;

    .line 46
    new-instance v0, Lcom/samsung/android/sconnect/extern/BleProxyService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/extern/BleProxyService$1;-><init>(Lcom/samsung/android/sconnect/extern/BleProxyService;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyService:Lcom/samsung/android/sconnect/extern/IBleProxyService$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/extern/BleProxyService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/extern/BleProxyService;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static removeBleProxyCallback(Ljava/lang/String;)V
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 88
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    const-string v0, "BleProxyService"

    const-string v1, "removeBleProxyCallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not exist "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :goto_0
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->applyBleScanPolicy()V

    .line 97
    :cond_0
    return-void

    .line 91
    :cond_1
    const-string v0, "BleProxyService"

    const-string v1, "removeBleProxyCallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public addBleProxyCallback(Lcom/samsung/android/sconnect/extern/IBleProxyCallback;Ljava/lang/String;)V
    .locals 4
    .param p1, "bleProxyCallback"    # Lcom/samsung/android/sconnect/extern/IBleProxyCallback;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 74
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0, p2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 75
    const-string v0, "BleProxyService"

    const-string v1, "addBleProxyCallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "update "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [IBleProxyCallback]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0, p2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0, p2, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->applyBleScanPolicy()V

    .line 85
    return-void

    .line 80
    :cond_0
    const-string v0, "BleProxyService"

    const-string v1, "addBleProxyCallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [IBleProxyCallback]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0, p2, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public clearBleProxyCallbacks()V
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 101
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->applyBleScanPolicy()V

    .line 104
    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 36
    const-string v0, "BleProxyService"

    const-string v1, "onBind"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyService:Lcom/samsung/android/sconnect/extern/IBleProxyService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 22
    const-string v0, "BleProxyService"

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 24
    iput-object p0, p0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mContext:Landroid/content/Context;

    .line 25
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 29
    const-string v0, "BleProxyService"

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/extern/BleProxyService;->clearBleProxyCallbacks()V

    .line 31
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 32
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    const-string v0, "BleProxyService"

    const-string v1, "onUnbind"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
