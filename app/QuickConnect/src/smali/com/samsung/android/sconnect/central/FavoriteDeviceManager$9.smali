.class Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$9;
.super Ljava/lang/Object;
.source "FavoriteDeviceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showInputMethod()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0

    .prologue
    .line 594
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$9;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 600
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$9;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$900(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 602
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$9;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$1000(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 607
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 604
    :catch_0
    move-exception v0

    .line 605
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "FavoriteDeviceManager"

    const-string v3, "Caught Exception showInputMethod"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
