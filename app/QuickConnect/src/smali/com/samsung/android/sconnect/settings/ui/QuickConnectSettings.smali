.class public Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;
.super Landroid/app/Activity;
.source "QuickConnectSettings.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "QuickConnectSettings"


# instance fields
.field private mButtonLayout:Landroid/view/View;

.field private mContentsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mDialog:Landroid/app/Dialog;

.field private mIsContentsForwardLock:Z

.field private mIsTutorial:Z

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mMimeType:Ljava/lang/String;

.field private mNextLayout:Landroid/view/View;

.field private mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRootView:Landroid/widget/RelativeLayout;

.field private mTabletSideMargin:I

.field private mVisibility:I

.field private mVisibilityAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mVisibilityText:Landroid/widget/TextView;

.field private mVisibilityView:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 47
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mButtonLayout:Landroid/view/View;

    .line 49
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mNextLayout:Landroid/view/View;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mRootView:Landroid/widget/RelativeLayout;

    .line 51
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityView:Landroid/widget/LinearLayout;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityText:Landroid/widget/TextView;

    .line 53
    iput v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mTabletSideMargin:I

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    .line 56
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mDialog:Landroid/app/Dialog;

    .line 57
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityAdapter:Landroid/widget/ArrayAdapter;

    .line 60
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsContentsForwardLock:Z

    .line 61
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsTutorial:Z

    .line 62
    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mMimeType:Ljava/lang/String;

    .line 343
    new-instance v0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$7;-><init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->showVisibilityDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsContentsForwardLock:Z

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mContentsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->changeVisibility(I)V

    return-void
.end method

.method private changeVisibility(I)V
    .locals 5
    .param p1, "visibility"    # I

    .prologue
    .line 389
    iput p1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    .line 390
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    iget v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    const/4 v4, 0x2

    if-eq v1, v4, :cond_0

    sget v1, Lcom/samsung/android/sconnect/common/util/Util;->ENABLED:I

    :goto_0
    invoke-static {v2, v3, v1}, Lcom/samsung/android/sconnect/common/util/Util;->saveToDb(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 392
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_CONTACT_ONLY:Ljava/lang/String;

    iget v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    sget v1, Lcom/samsung/android/sconnect/common/util/Util;->ENABLED:I

    :goto_1
    invoke-static {v2, v3, v1}, Lcom/samsung/android/sconnect/common/util/Util;->saveToDb(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 395
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.settings.QUICK_CONNECT_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 396
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->sendBroadcast(Landroid/content/Intent;)V

    .line 398
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setVisibilityView()V

    .line 399
    return-void

    .line 390
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    sget v1, Lcom/samsung/android/sconnect/common/util/Util;->DISABLED:I

    goto :goto_0

    .line 392
    :cond_1
    sget v1, Lcom/samsung/android/sconnect/common/util/Util;->DISABLED:I

    goto :goto_1
.end method

.method public static getDeviceInch(Landroid/content/Context;)D
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    const-wide/16 v0, 0x0

    .line 260
    .local v0, "device_inch":D
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 261
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    const-string v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 262
    .local v3, "wm":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 264
    iget v6, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v6, v6

    iget v7, v2, Landroid/util/DisplayMetrics;->xdpi:F

    div-float v4, v6, v7

    .line 265
    .local v4, "xInch":F
    iget v6, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v6, v6

    iget v7, v2, Landroid/util/DisplayMetrics;->ydpi:F

    div-float v5, v6, v7

    .line 267
    .local v5, "yInch":F
    mul-float v6, v4, v4

    mul-float v7, v5, v5

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 269
    return-wide v0
.end method

.method private registerBroadcast()V
    .locals 2

    .prologue
    .line 334
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 335
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 336
    iget-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 337
    return-void
.end method

.method private setPreference(Z)V
    .locals 4
    .param p1, "isTutorialSettingAlive"    # Z

    .prologue
    .line 273
    const-string v2, "SconnectDisplay"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 274
    .local v1, "preference":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 275
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "TUTORIAL_SETTING_ALIVE"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 276
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 277
    return-void
.end method

.method private setVisibilityFromSystemDb()V
    .locals 5

    .prologue
    .line 368
    const-string v2, "QuickConnectSettings"

    const-string v3, "setVisibilityFromSystemDb"

    const-string v4, "--"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const/4 v0, 0x0

    .line 371
    .local v0, "isAllowToConnect":Z
    const/4 v1, 0x0

    .line 373
    .local v1, "isContactOnly":Z
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 374
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_CONTACT_ONLY:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    .line 376
    if-nez v0, :cond_0

    .line 378
    const/4 v2, 0x2

    iput v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    .line 386
    :goto_0
    return-void

    .line 379
    :cond_0
    if-eqz v1, :cond_1

    .line 381
    const/4 v2, 0x1

    iput v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    goto :goto_0

    .line 384
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    goto :goto_0
.end method

.method private setVisibilityView()V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 323
    iget v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 324
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityText:Landroid/widget/TextView;

    const v1, 0x7f090132

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    iget v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 326
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityText:Landroid/widget/TextView;

    const v1, 0x7f090130

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 327
    :cond_2
    iget v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I

    if-nez v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityText:Landroid/widget/TextView;

    const v1, 0x7f09012e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private showVisibilityDialog()V
    .locals 4

    .prologue
    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v0, "visibilityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    new-instance v1, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$4;

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x1090004

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$4;-><init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityAdapter:Landroid/widget/ArrayAdapter;

    .line 213
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09012d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityAdapter:Landroid/widget/ArrayAdapter;

    new-instance v3, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$6;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$6;-><init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090089

    new-instance v3, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$5;-><init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mDialog:Landroid/app/Dialog;

    .line 227
    return-void
.end method

.method private unregisterBroadcast()V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 341
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 281
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsTutorial:Z

    if-eqz v1, :cond_0

    .line 282
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setPreference(Z)V

    .line 283
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 284
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 285
    const-string v1, "FORWARD_LOCK"

    iget-boolean v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsContentsForwardLock:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 286
    const-string v1, "android.intent.extra.STREAM"

    iget-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 287
    const-string v1, "TUTORIAL_LAST_PAGE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 288
    iget-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->startActivity(Landroid/content/Intent;)V

    .line 290
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->finish()V

    .line 292
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 293
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x400

    const/16 v12, 0x8

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 66
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 72
    const v6, 0x7f030018

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setContentView(I)V

    .line 75
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getDeviceInch(Landroid/content/Context;)D

    move-result-wide v2

    .line 76
    .local v2, "inch":D
    const-string v6, "QuickConnectSettings"

    const-string v7, "onCreate"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "device inch: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_0

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    cmpl-double v6, v2, v6

    if-ltz v6, :cond_0

    .line 80
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 81
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 82
    iget v6, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v6, v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v6, v8

    double-to-int v6, v6

    iput v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mTabletSideMargin:I

    .line 83
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 85
    .local v5, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 86
    iget v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mTabletSideMargin:I

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 87
    iget v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mTabletSideMargin:I

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 92
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .end local v0    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v2    # "inch":D
    .end local v5    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    :goto_1
    const v6, 0x7f0c0066

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityView:Landroid/widget/LinearLayout;

    .line 100
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityView:Landroid/widget/LinearLayout;

    const v7, 0x7f0c0067

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityText:Landroid/widget/TextView;

    .line 101
    const v6, 0x7f0c0068

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mButtonLayout:Landroid/view/View;

    .line 103
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibilityView:Landroid/widget/LinearLayout;

    new-instance v7, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$1;

    invoke-direct {v7, p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$1;-><init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 111
    .local v4, "intent":Landroid/content/Intent;
    const-string v6, "isTutorial"

    invoke-virtual {v4, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsTutorial:Z

    .line 113
    iget-boolean v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsTutorial:Z

    if-eqz v6, :cond_2

    .line 114
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    const v7, 0x7f090014

    invoke-virtual {p0, v7}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 115
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 116
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 117
    invoke-direct {p0, v11}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setPreference(Z)V

    .line 118
    const-string v6, "android.intent.extra.STREAM"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mContentsList:Ljava/util/ArrayList;

    .line 119
    const-string v6, "FORWARD_LOCK"

    invoke-virtual {v4, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsContentsForwardLock:Z

    .line 120
    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mMimeType:Ljava/lang/String;

    .line 121
    const-string v6, "QuickConnectSettings"

    const-string v7, "onCreate"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mContentsList : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", FORWARD_LOCK: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsContentsForwardLock:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mButtonLayout:Landroid/view/View;

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 124
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mButtonLayout:Landroid/view/View;

    iput-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mNextLayout:Landroid/view/View;

    .line 126
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mNextLayout:Landroid/view/View;

    new-instance v7, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;

    invoke-direct {v7, p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;-><init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->showVisibilityDialog()V

    .line 152
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setVisibilityFromSystemDb()V

    .line 153
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setVisibilityView()V

    .line 160
    const-string v6, "quickconnect"

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 162
    new-instance v6, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$3;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$3;-><init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V

    iput-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 167
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v7, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v6, v7}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 169
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->registerBroadcast()V

    .line 170
    return-void

    .line 89
    .end local v4    # "intent":Landroid/content/Intent;
    .restart local v0    # "displayMetrics":Landroid/util/DisplayMetrics;
    .restart local v2    # "inch":D
    .restart local v5    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const/4 v6, 0x0

    :try_start_1
    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 90
    const/4 v6, 0x0

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 95
    .end local v0    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v2    # "inch":D
    .end local v5    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :catch_0
    move-exception v1

    .line 96
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "QuickConnectSettings"

    const-string v7, "onCreate"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception Title : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 148
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v4    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    const v7, 0x7f09012a

    invoke-virtual {p0, v7}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 150
    iget-object v6, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mButtonLayout:Landroid/view/View;

    invoke-virtual {v6, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 297
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 298
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->unregisterBroadcast()V

    .line 299
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 301
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/quickconnect/QuickConnectManager;->terminate()V

    .line 302
    iput-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 303
    iput-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 306
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setPreference(Z)V

    .line 308
    invoke-static {p0}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->needToStart(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->startPeriphService(Landroid/content/Context;Z)Landroid/content/ComponentName;

    .line 311
    :cond_1
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 231
    const-string v0, "QuickConnectSettings"

    const-string v1, "onNewIntent"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    if-eqz p1, :cond_0

    .line 233
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsTutorial:Z

    if-eqz v0, :cond_0

    .line 234
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mContentsList:Ljava/util/ArrayList;

    .line 235
    const-string v0, "FORWARD_LOCK"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsContentsForwardLock:Z

    .line 236
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mMimeType:Ljava/lang/String;

    .line 237
    const-string v0, "QuickConnectSettings"

    const-string v1, "onNewIntent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mContentsList : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", FORWARD_LOCK: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsContentsForwardLock:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 242
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 246
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 254
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 248
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->onBackPressed()V

    goto :goto_0

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 315
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 317
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setVisibilityFromSystemDb()V

    .line 318
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->setVisibilityView()V

    .line 319
    return-void
.end method
