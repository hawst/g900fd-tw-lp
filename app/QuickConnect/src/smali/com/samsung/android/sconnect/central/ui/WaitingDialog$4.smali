.class Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;
.super Landroid/os/Handler;
.source "WaitingDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/WaitingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V
    .locals 0

    .prologue
    .line 820
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v10, 0x7d2

    const/16 v9, 0x9

    .line 824
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 879
    const-string v5, "WaitingDialog"

    const-string v6, "mResponseCheckHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "UNKNOWN_MSG: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    :cond_0
    :goto_0
    return-void

    .line 826
    :pswitch_0
    const-string v5, "WaitingDialog"

    const-string v6, "mResponseCheckHandler"

    const-string v7, "MSG_ALLDONE"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    const/4 v0, 0x0

    .line 828
    .local v0, "connecting":I
    const/4 v3, 0x1

    .line 829
    .local v3, "isFinish":Z
    const/4 v4, 0x1

    .line 830
    .local v4, "removeGroup":Z
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 831
    .local v2, "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceType:I

    if-ne v5, v9, :cond_2

    .line 832
    const-string v5, "WaitingDialog"

    const-string v6, "mResponseCheckHandler"

    const-string v7, "Printer Device"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    const/4 v3, 0x0

    .line 834
    const/4 v4, 0x0

    goto :goto_1

    .line 836
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateState(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    invoke-static {v5, v2}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$500(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    .line 837
    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_3

    .line 838
    const/4 v4, 0x0

    .line 840
    :cond_3
    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-eqz v5, :cond_4

    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 842
    :cond_4
    const-string v5, "WaitingDialog"

    const-string v6, "mResponseCheckHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "WAITING-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    const/4 v3, 0x0

    .line 845
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 849
    .end local v2    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_5
    if-eqz v4, :cond_6

    .line 850
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->sendDisconnectForDecline()V
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$900(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V

    .line 852
    :cond_6
    if-eqz v3, :cond_7

    .line 853
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    .line 855
    :cond_7
    if-lez v0, :cond_0

    .line 856
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 857
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v5

    mul-int/lit16 v6, v0, 0x7530

    int-to-long v6, v6

    invoke-virtual {v5, v10, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 862
    .end local v0    # "connecting":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "isFinish":Z
    .end local v4    # "removeGroup":Z
    :pswitch_1
    const-string v5, "WaitingDialog"

    const-string v6, "mResponseCheckHandler"

    const-string v7, "MSG_TIMEOUT_ACCEPT"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_8
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 865
    .restart local v2    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceType:I

    if-ne v5, v9, :cond_8

    .line 866
    const-string v5, "WaitingDialog"

    const-string v6, "mResponseCheckHandler"

    const-string v7, "Printer Device"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    goto :goto_2

    .line 871
    .end local v2    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->waitingAnswerTimeout()V
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$100(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V

    goto/16 :goto_0

    .line 874
    .end local v1    # "i$":Ljava/util/Iterator;
    :pswitch_2
    const-string v5, "WaitingDialog"

    const-string v6, "mResponseCheckHandler"

    const-string v7, "MSG_TIMEOUT_CONNECT"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$400(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0900b5

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 876
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    goto/16 :goto_0

    .line 824
    nop

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
