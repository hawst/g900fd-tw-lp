.class final Lcom/samsung/android/sconnect/common/device/SconnectDevice$1;
.super Ljava/lang/Object;
.source "SconnectDevice.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/device/SconnectDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 924
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>()V

    .line 925
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 926
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    .line 927
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    .line 928
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 929
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    .line 930
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 931
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    .line 932
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    .line 933
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    .line 935
    const-class v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 936
    .local v1, "loaderID":Ljava/lang/ClassLoader;
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    .line 938
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 920
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 943
    new-array v0, p1, [Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 920
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$1;->newArray(I)[Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    return-object v0
.end method
