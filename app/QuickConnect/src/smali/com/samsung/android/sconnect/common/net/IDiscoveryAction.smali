.class public interface abstract Lcom/samsung/android/sconnect/common/net/IDiscoveryAction;
.super Ljava/lang/Object;
.source "IDiscoveryAction.java"


# virtual methods
.method public abstract checkEnabledNetwork()Z
.end method

.method public abstract getDeviceList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end method

.method public abstract startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
.end method

.method public abstract stopDiscovery()V
.end method
