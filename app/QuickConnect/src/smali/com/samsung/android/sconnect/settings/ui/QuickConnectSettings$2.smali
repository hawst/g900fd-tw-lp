.class Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;
.super Ljava/lang/Object;
.source "QuickConnectSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 130
    iget-object v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    const-string v4, "SconnectDisplay"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 132
    .local v2, "preference":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 133
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "isTutorialMode"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 134
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    const-class v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x14010000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 139
    const-string v3, "FORWARD_LOCK"

    iget-object v4, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    # getter for: Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mIsContentsForwardLock:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->access$100(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)Z

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 140
    iget-object v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    # getter for: Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mMimeType:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->access$200(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v3, "android.intent.extra.STREAM"

    iget-object v4, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    # getter for: Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->access$300(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 142
    iget-object v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-virtual {v3, v1}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->startActivity(Landroid/content/Intent;)V

    .line 143
    iget-object v3, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$2;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->finish()V

    .line 144
    return-void
.end method
