.class public Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
.super Ljava/lang/Object;
.source "DeviceWearable.java"


# static fields
.field static final TAG:Ljava/lang/String; = "DeviceWearable"


# instance fields
.field public mData:[B

.field public mRSSI:S

.field public mWearableBleMac:Ljava/lang/String;

.field public mWearableBtMac:Ljava/lang/String;

.field public mWearableDeviceID:I

.field public mWearableInfo:Ljava/lang/String;

.field public mWearableName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ISLjava/lang/String;[B)V
    .locals 2
    .param p1, "displayname"    # Ljava/lang/String;
    .param p2, "btMac"    # Ljava/lang/String;
    .param p3, "bleMac"    # Ljava/lang/String;
    .param p4, "deviceID"    # I
    .param p5, "RSSI"    # S
    .param p6, "info"    # Ljava/lang/String;
    .param p7, "data"    # [B

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableName:Ljava/lang/String;

    .line 9
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableBtMac:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableBleMac:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableDeviceID:I

    .line 12
    const/16 v0, -0x12c

    iput-short v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mRSSI:S

    .line 13
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableInfo:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mData:[B

    .line 18
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableName:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableBtMac:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableBleMac:Ljava/lang/String;

    .line 21
    iput p4, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableDeviceID:I

    .line 22
    iput-short p5, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mRSSI:S

    .line 23
    iput-object p6, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableInfo:Ljava/lang/String;

    .line 24
    iput-object p7, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mData:[B

    .line 25
    return-void
.end method
