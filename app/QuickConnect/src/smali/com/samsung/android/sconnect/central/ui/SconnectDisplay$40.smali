.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToSendDRMDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/util/ArrayList;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$sendAction:I

.field final synthetic val$sendContentsType:I

.field final synthetic val$sendDeviceList:Ljava/util/ArrayList;

.field final synthetic val$sendItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

.field final synthetic val$sendMode:I


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;ILjava/util/ArrayList;Lcom/samsung/android/sconnect/central/ui/DisplayItem;II)V
    .locals 0

    .prologue
    .line 3419
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendMode:I

    iput-object p3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendDeviceList:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    iput p5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendAction:I

    iput p6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendContentsType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 3422
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendMode:I

    packed-switch v0, :pswitch_data_0

    .line 3437
    :goto_0
    return-void

    .line 3424
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendDeviceList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$6000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->sendMultiple(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 3427
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendAction:I

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$6000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->sendWithCheckConnectionAndMax(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V

    goto :goto_0

    .line 3431
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$6000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;->val$sendContentsType:I

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    goto :goto_0

    .line 3422
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
