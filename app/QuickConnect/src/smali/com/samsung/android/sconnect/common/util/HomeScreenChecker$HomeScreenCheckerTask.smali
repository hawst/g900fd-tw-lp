.class Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;
.super Ljava/lang/Thread;
.source "HomeScreenChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HomeScreenCheckerTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$1;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;-><init>(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 65
    const/4 v1, 0x1

    .line 66
    .local v1, "flag":Z
    monitor-enter p0

    .line 68
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :goto_0
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    # getter for: Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->access$100(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->checkHomeScreen(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    # getter for: Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->access$200(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 74
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    # getter for: Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->access$200(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0xbc7

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    .line 75
    .local v2, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    # getter for: Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->access$200(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 78
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    :cond_1
    if-eqz v1, :cond_3

    .line 80
    monitor-enter p0

    .line 82
    const-wide/16 v4, 0xbb8

    :try_start_2
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 86
    :goto_1
    :try_start_3
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    # getter for: Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->access$100(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->checkHomeScreen(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 87
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    # getter for: Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->access$200(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 88
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    # getter for: Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->access$200(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0xbc7

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    .line 89
    .restart local v2    # "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    # getter for: Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->access$200(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 91
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 98
    :cond_3
    :goto_2
    return-void

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v3, "HomeScreenChecker"

    const-string v4, "doInBackground"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3

    .line 83
    :catch_1
    move-exception v0

    .line 84
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_5
    const-string v3, "HomeScreenChecker"

    const-string v4, "doInBackground"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 93
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v3

    :cond_4
    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 94
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->this$0:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    iget-boolean v3, v3, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mCancelled:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    goto :goto_2
.end method
