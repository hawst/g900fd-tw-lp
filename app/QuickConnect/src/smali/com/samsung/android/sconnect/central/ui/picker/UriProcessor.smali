.class public Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;
.super Ljava/lang/Object;
.source "UriProcessor.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "UriProcessor"


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->mContext:Landroid/content/Context;

    .line 31
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->mContentResolver:Landroid/content/ContentResolver;

    .line 32
    return-void
.end method

.method private decodeOnTheBasisOfSemicolonSeparatedLastPathSegment(Landroid/net/Uri;)[Landroid/net/Uri;
    .locals 13
    .param p1, "uriToBeDecoded"    # Landroid/net/Uri;

    .prologue
    .line 158
    if-eqz p1, :cond_0

    .line 159
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    .line 160
    .local v6, "lastSegment":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 161
    const-string v11, ":"

    invoke-virtual {v6, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 162
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, "fullUri":Ljava/lang/String;
    const-string v11, ":"

    invoke-static {v6, v11}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 165
    .local v7, "lastSegmentParts":[Ljava/lang/String;
    const/4 v11, 0x0

    const/16 v12, 0x2f

    invoke-static {v2, v12}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;C)I

    move-result v12

    invoke-virtual {v2, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 168
    .local v1, "basePart":Landroid/net/Uri;
    array-length v11, v7

    new-array v10, v11, [Landroid/net/Uri;

    .line 169
    .local v10, "retUris":[Landroid/net/Uri;
    const/4 v3, 0x0

    .line 170
    .local v3, "i":I
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v4, v3

    .end local v3    # "i":I
    .local v4, "i":I
    :goto_0
    if-ge v5, v8, :cond_1

    aget-object v9, v0, v5

    .line 171
    .local v9, "part":Ljava/lang/String;
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    invoke-static {v1, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->check(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v11

    aput-object v11, v10, v4

    .line 170
    add-int/lit8 v5, v5, 0x1

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_0

    .line 179
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "basePart":Landroid/net/Uri;
    .end local v2    # "fullUri":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "i$":I
    .end local v6    # "lastSegment":Ljava/lang/String;
    .end local v7    # "lastSegmentParts":[Ljava/lang/String;
    .end local v8    # "len$":I
    .end local v9    # "part":Ljava/lang/String;
    .end local v10    # "retUris":[Landroid/net/Uri;
    :cond_0
    const/4 v10, 0x0

    :cond_1
    return-object v10
.end method

.method private getAudioFile(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 183
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v8, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v1

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 187
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 189
    .local v7, "path":Ljava/lang/String;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v8, :cond_0

    .line 190
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 191
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 194
    :cond_0
    if-eqz v6, :cond_1

    .line 195
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 198
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 194
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 195
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method


# virtual methods
.method protected check(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 36
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "scheme":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 38
    new-instance v3, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    .line 40
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 41
    .local v2, "x":Ljava/io/InputStream;
    if-eqz v2, :cond_1

    .line 42
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    move-object v3, p1

    .line 50
    .end local v1    # "scheme":Ljava/lang/String;
    .end local v2    # "x":Ljava/io/InputStream;
    :goto_0
    return-object v3

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 50
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 47
    :catch_1
    move-exception v0

    .line 48
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method protected getUri(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 54
    instance-of v1, p1, Landroid/net/Uri;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 55
    check-cast v0, Landroid/net/Uri;

    .line 56
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->check(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 62
    .end local v0    # "uri":Landroid/net/Uri;
    .end local p1    # "object":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 58
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 59
    check-cast p1, Ljava/lang/String;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 60
    .restart local v0    # "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->check(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    .line 62
    .end local v0    # "uri":Landroid/net/Uri;
    .restart local p1    # "object":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public process(Landroid/content/Intent;)[Landroid/net/Uri;
    .locals 27
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 66
    const-string v22, "UriProcessor"

    const-string v23, "process"

    const-string v24, ""

    invoke-static/range {v22 .. v24}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    if-nez p1, :cond_1

    .line 68
    const-string v22, "UriProcessor"

    const-string v23, "process"

    const-string v24, " data == null"

    invoke-static/range {v22 .. v24}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const/16 v20, 0x0

    .line 146
    :cond_0
    :goto_0
    return-object v20

    .line 71
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v11

    .line 72
    .local v11, "fromData":Landroid/net/Uri;
    if-eqz v11, :cond_2

    .line 73
    const-string v22, "UriProcessor"

    const-string v23, "process"

    const-string v24, "fromData != null"

    invoke-static/range {v22 .. v24}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Landroid/net/Uri;

    move-object/from16 v20, v0

    const/16 v22, 0x0

    aput-object v11, v20, v22

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    .line 79
    .local v10, "extras":Landroid/os/Bundle;
    if-nez v10, :cond_3

    .line 80
    const-string v22, "UriProcessor"

    const-string v23, "process"

    const-string v24, "extras == null"

    invoke-static/range {v22 .. v24}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const/16 v20, 0x0

    goto :goto_0

    .line 83
    :cond_3
    invoke-virtual {v10}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_c

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 84
    .local v8, "extra_key":Ljava/lang/String;
    invoke-virtual {v10, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 86
    .local v9, "extra_value":Ljava/lang/Object;
    const-string v22, "UriProcessor"

    const-string v23, "process"

    const-string v24, "key: %s, value: %s"

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object v8, v25, v26

    const/16 v26, 0x1

    aput-object v9, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v22 .. v24}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->getUri(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v12

    .line 89
    .local v12, "fromExtra":Landroid/net/Uri;
    if-eqz v12, :cond_5

    .line 90
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->decodeOnTheBasisOfSemicolonSeparatedLastPathSegment(Landroid/net/Uri;)[Landroid/net/Uri;

    move-result-object v20

    .line 91
    .local v20, "retUris":[Landroid/net/Uri;
    if-nez v20, :cond_0

    .line 92
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Landroid/net/Uri;

    move-object/from16 v20, v0

    .end local v20    # "retUris":[Landroid/net/Uri;
    const/16 v22, 0x0

    aput-object v12, v20, v22

    .restart local v20    # "retUris":[Landroid/net/Uri;
    goto :goto_0

    .line 98
    .end local v20    # "retUris":[Landroid/net/Uri;
    :cond_5
    instance-of v0, v9, [J

    move/from16 v22, v0

    if-eqz v22, :cond_7

    move-object/from16 v22, v9

    .line 99
    check-cast v22, [J

    move-object/from16 v6, v22

    check-cast v6, [J

    .line 100
    .local v6, "extra_array":[J
    array-length v0, v6

    move/from16 v22, v0

    move/from16 v0, v22

    new-array v0, v0, [Landroid/net/Uri;

    move-object/from16 v20, v0

    .line 102
    .restart local v20    # "retUris":[Landroid/net/Uri;
    const/4 v13, 0x0

    .line 103
    .local v13, "i":I
    move-object v4, v6

    .local v4, "arr$":[J
    array-length v0, v4

    move/from16 v17, v0

    .local v17, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    move v14, v13

    .end local v13    # "i":I
    .local v14, "i":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_6

    aget-wide v18, v4, v16

    .line 104
    .local v18, "id":J
    sget-object v22, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v22

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->getAudioFile(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v21

    .line 106
    .local v21, "uri":Landroid/net/Uri;
    if-eqz v21, :cond_e

    .line 107
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "i":I
    .restart local v13    # "i":I
    aput-object v21, v20, v14

    .line 103
    :goto_2
    add-int/lit8 v16, v16, 0x1

    move v14, v13

    .end local v13    # "i":I
    .restart local v14    # "i":I
    goto :goto_1

    .line 110
    .end local v18    # "id":J
    .end local v21    # "uri":Landroid/net/Uri;
    :cond_6
    if-gtz v14, :cond_0

    .line 114
    .end local v4    # "arr$":[J
    .end local v6    # "extra_array":[J
    .end local v14    # "i":I
    .end local v16    # "i$":I
    .end local v17    # "len$":I
    .end local v20    # "retUris":[Landroid/net/Uri;
    :cond_7
    instance-of v0, v9, [Ljava/lang/Object;

    move/from16 v22, v0

    if-eqz v22, :cond_9

    move-object/from16 v22, v9

    .line 115
    check-cast v22, [Ljava/lang/Object;

    move-object/from16 v6, v22

    check-cast v6, [Ljava/lang/Object;

    .line 116
    .local v6, "extra_array":[Ljava/lang/Object;
    array-length v0, v6

    move/from16 v22, v0

    move/from16 v0, v22

    new-array v0, v0, [Landroid/net/Uri;

    move-object/from16 v20, v0

    .line 117
    .restart local v20    # "retUris":[Landroid/net/Uri;
    const/4 v13, 0x0

    .line 118
    .restart local v13    # "i":I
    move-object v4, v6

    .local v4, "arr$":[Ljava/lang/Object;
    array-length v0, v4

    move/from16 v17, v0

    .restart local v17    # "len$":I
    const/16 v16, 0x0

    .restart local v16    # "i$":I
    move v14, v13

    .end local v13    # "i":I
    .restart local v14    # "i":I
    :goto_3
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_8

    aget-object v5, v4, v16

    .line 119
    .local v5, "entry":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->getUri(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v21

    .line 120
    .restart local v21    # "uri":Landroid/net/Uri;
    if-eqz v21, :cond_d

    .line 121
    add-int/lit8 v13, v14, 0x1

    .end local v14    # "i":I
    .restart local v13    # "i":I
    aput-object v21, v20, v14

    .line 118
    :goto_4
    add-int/lit8 v16, v16, 0x1

    move v14, v13

    .end local v13    # "i":I
    .restart local v14    # "i":I
    goto :goto_3

    .line 124
    .end local v5    # "entry":Ljava/lang/Object;
    .end local v21    # "uri":Landroid/net/Uri;
    :cond_8
    if-gtz v14, :cond_0

    .line 129
    .end local v4    # "arr$":[Ljava/lang/Object;
    .end local v6    # "extra_array":[Ljava/lang/Object;
    .end local v14    # "i":I
    .end local v16    # "i$":I
    .end local v17    # "len$":I
    .end local v20    # "retUris":[Landroid/net/Uri;
    :cond_9
    instance-of v0, v9, Ljava/util/AbstractCollection;

    move/from16 v22, v0

    if-eqz v22, :cond_4

    move-object v7, v9

    .line 130
    check-cast v7, Ljava/util/AbstractCollection;

    .line 131
    .local v7, "extra_array":Ljava/util/AbstractCollection;, "Ljava/util/AbstractCollection<*>;"
    invoke-virtual {v7}, Ljava/util/AbstractCollection;->size()I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [Landroid/net/Uri;

    move-object/from16 v20, v0

    .line 132
    .restart local v20    # "retUris":[Landroid/net/Uri;
    const/4 v13, 0x0

    .line 133
    .restart local v13    # "i":I
    invoke-virtual {v7}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_b

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 134
    .restart local v5    # "entry":Ljava/lang/Object;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->getUri(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v21

    .line 135
    .restart local v21    # "uri":Landroid/net/Uri;
    if-eqz v21, :cond_a

    .line 136
    add-int/lit8 v14, v13, 0x1

    .end local v13    # "i":I
    .restart local v14    # "i":I
    aput-object v21, v20, v13

    move v13, v14

    .end local v14    # "i":I
    .restart local v13    # "i":I
    goto :goto_5

    .line 139
    .end local v5    # "entry":Ljava/lang/Object;
    .end local v21    # "uri":Landroid/net/Uri;
    :cond_b
    if-lez v13, :cond_4

    goto/16 :goto_0

    .line 145
    .end local v7    # "extra_array":Ljava/util/AbstractCollection;, "Ljava/util/AbstractCollection<*>;"
    .end local v8    # "extra_key":Ljava/lang/String;
    .end local v9    # "extra_value":Ljava/lang/Object;
    .end local v12    # "fromExtra":Landroid/net/Uri;
    .end local v13    # "i":I
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v20    # "retUris":[Landroid/net/Uri;
    :cond_c
    const-string v22, "UriProcessor"

    const-string v23, "process"

    const-string v24, "default null"

    invoke-static/range {v22 .. v24}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const/16 v20, 0x0

    goto/16 :goto_0

    .restart local v4    # "arr$":[Ljava/lang/Object;
    .restart local v5    # "entry":Ljava/lang/Object;
    .restart local v6    # "extra_array":[Ljava/lang/Object;
    .restart local v8    # "extra_key":Ljava/lang/String;
    .restart local v9    # "extra_value":Ljava/lang/Object;
    .restart local v12    # "fromExtra":Landroid/net/Uri;
    .restart local v14    # "i":I
    .local v16, "i$":I
    .restart local v17    # "len$":I
    .restart local v20    # "retUris":[Landroid/net/Uri;
    .restart local v21    # "uri":Landroid/net/Uri;
    :cond_d
    move v13, v14

    .end local v14    # "i":I
    .restart local v13    # "i":I
    goto :goto_4

    .end local v5    # "entry":Ljava/lang/Object;
    .end local v13    # "i":I
    .local v4, "arr$":[J
    .local v6, "extra_array":[J
    .restart local v14    # "i":I
    .restart local v18    # "id":J
    :cond_e
    move v13, v14

    .end local v14    # "i":I
    .restart local v13    # "i":I
    goto/16 :goto_2
.end method
