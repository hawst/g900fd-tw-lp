.class Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;
.super Ljava/lang/Object;
.source "FileShareActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TransferItem"
.end annotation


# instance fields
.field private mDeviceName:Ljava/lang/String;

.field private mMac:Ljava/lang/String;

.field private mSession:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "mac"    # Ljava/lang/String;
    .param p3, "sessionID"    # Ljava/lang/String;
    .param p4, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mDeviceName:Ljava/lang/String;

    .line 296
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mMac:Ljava/lang/String;

    .line 297
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    .line 300
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mDeviceName:Ljava/lang/String;

    .line 301
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mMac:Ljava/lang/String;

    .line 302
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public addSessionInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "sessionID"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 323
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 324
    const-string v0, "FileShareHelper"

    const-string v1, "stateChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Add sessionID:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 329
    const-string v0, "FileShareHelper"

    const-string v1, "stateChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Add FilePath:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    :cond_1
    return-void
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mMac:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionSize()I
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    .line 347
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContainedSessionID(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sessionID"    # Ljava/lang/String;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 310
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeSessionID(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sessionID"    # Ljava/lang/String;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    const/4 v0, 0x1

    .line 340
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
