.class public Lcom/samsung/android/sconnect/central/action/EmeetingActionHelper;
.super Ljava/lang/Object;
.source "EmeetingActionHelper.java"


# static fields
.field public static final ACTION_NEW_MEETING_CREATED:Ljava/lang/String; = "com.sec.android.emeeting.NEW_MEETING_CREATED"

.field public static final ACTION_START_EMEETING_GUEST:Ljava/lang/String; = "com.sec.android.emeeting.START_EMEETING_GUEST"

.field public static final ACTION_START_EMEETING_HOST:Ljava/lang/String; = "com.sec.android.emeeting.START_EMEETING_HOST"

.field private static final EXTRA_KEY_TARGET_BT_MAC_ADDRESS:Ljava/lang/String; = "BT_MAC_ADDRESS"

.field private static final TAG:Ljava/lang/String; = "EmeetingActionHelper"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, "EmeetingActionHelper"

    const-string v1, "EmeetingActionHelper"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public static launchEmeeting(Landroid/content/Context;ZLjava/util/ArrayList;Ljava/lang/String;)V
    .locals 6
    .param p0, "appContext"    # Landroid/content/Context;
    .param p1, "isHostMode"    # Z
    .param p3, "btMacAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p0, :cond_0

    .line 38
    const-string v2, "EmeetingActionHelper"

    const-string v3, "launchEmeeting"

    const-string v4, "ERROR - missed parameter"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :goto_0
    return-void

    .line 41
    :cond_0
    if-nez p2, :cond_1

    .line 42
    const-string v2, "EmeetingActionHelper"

    const-string v3, "launchEmeeting"

    const-string v4, "uris is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_1
    :try_start_0
    const-string v2, "EmeetingActionHelper"

    const-string v3, "launchEmeeting"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[isHostMode]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 48
    .local v1, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_2

    .line 49
    const-string v2, "com.sec.android.emeeting.START_EMEETING_HOST"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    :goto_1
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 55
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "EmeetingActionHelper"

    const-string v3, "launchEmeeting"

    const-string v4, "ActivityNotFoundException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 51
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_2
    :try_start_1
    const-string v2, "com.sec.android.emeeting.START_EMEETING_GUEST"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v2, "BT_MAC_ADDRESS"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
