.class Lcom/samsung/android/sconnect/central/ui/TvController$5;
.super Ljava/lang/Object;
.source "TvController.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/TvController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/TvController;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V
    .locals 0

    .prologue
    .line 770
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$5;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 775
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$5;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$900(Lcom/samsung/android/sconnect/central/ui/TvController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 776
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$5;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-static {p2}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$702(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    .line 780
    :goto_0
    const-string v0, "TvController"

    const-string v1, "onServiceConnected !!!!!"

    const-string v2, "onServiceConnected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$5;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->initTvList()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1000(Lcom/samsung/android/sconnect/central/ui/TvController;)V

    .line 783
    return-void

    .line 778
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$5;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-static {p2}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;->asInterface(Landroid/os/IBinder;)Ltv/peel/samsung/widget/service/IQuickConnectService;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$802(Lcom/samsung/android/sconnect/central/ui/TvController;Ltv/peel/samsung/widget/service/IQuickConnectService;)Ltv/peel/samsung/widget/service/IQuickConnectService;

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 787
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$5;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$702(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    .line 789
    const-string v0, "TvController"

    const-string v1, "onServiceDisconnected !!!!!"

    const-string v2, "onServiceDisconnected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    return-void
.end method
