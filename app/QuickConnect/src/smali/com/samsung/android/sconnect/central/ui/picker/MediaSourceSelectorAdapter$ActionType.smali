.class public final enum Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
.super Ljava/lang/Enum;
.source "MediaSourceSelector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_DOC:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_DRAWINGPAD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_LOCATION:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_MEMO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_MYFILES:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_PDF:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_SMEMO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_SNOTE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_VCAL:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_VCARD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_VNOTE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum ADD_VTODO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum CAPTURED_SCREEN:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum RECORD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum RECORD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field public static final enum TAKE_PICTURE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 456
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "CAPTURED_SCREEN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->CAPTURED_SCREEN:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_DOC"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_DOC:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_IMAGE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_VIDEO"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_SOUND"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "TAKE_PICTURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->TAKE_PICTURE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "RECORD_VIDEO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "RECORD_SOUND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_VCARD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCARD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_VCAL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCAL:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_VNOTE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VNOTE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_VTODO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VTODO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_LOCATION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_LOCATION:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_SMEMO"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SMEMO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_DRAWINGPAD"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_DRAWINGPAD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_MYFILES"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_MYFILES:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_SNOTE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SNOTE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_MEMO"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_MEMO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    const-string v1, "ADD_PDF"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_PDF:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    .line 455
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->CAPTURED_SCREEN:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_DOC:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->TAKE_PICTURE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCARD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCAL:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VNOTE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VTODO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_LOCATION:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SMEMO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_DRAWINGPAD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_MYFILES:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SNOTE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_MEMO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_PDF:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->$VALUES:[Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 455
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 455
    const-class v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->$VALUES:[Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    invoke-virtual {v0}, [Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    return-object v0
.end method
