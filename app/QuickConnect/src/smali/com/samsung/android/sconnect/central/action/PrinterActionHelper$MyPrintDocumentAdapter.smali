.class Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;
.super Landroid/print/PrintDocumentAdapter;
.source "PrinterActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyPrintDocumentAdapter"
.end annotation


# instance fields
.field private mPdfPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 263
    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;->mPdfPath:Ljava/lang/String;

    .line 264
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;->mPdfPath:Ljava/lang/String;

    .line 265
    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    .line 334
    const-string v0, "PrinterActionHelper"

    const-string v1, "MyPrintDocumentAdapter"

    const-string v2, "onFinish"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onFinish()V

    .line 336
    return-void
.end method

.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "oldAttributes"    # Landroid/print/PrintAttributes;
    .param p2, "newAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
    .param p5, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 276
    const-string v1, "PrinterActionHelper"

    const-string v2, "MyPrintDocumentAdapter"

    const-string v3, "onLayout"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-virtual {p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278
    invoke-virtual {p4}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    .line 286
    :goto_0
    return-void

    .line 282
    :cond_0
    new-instance v1, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;->mPdfPath:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v0

    .line 284
    .local v0, "info":Landroid/print/PrintDocumentInfo;
    const-string v1, "PrinterActionHelper"

    const-string v2, "MyPrintDocumentAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLayout - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/print/PrintDocumentInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const/4 v1, 0x1

    invoke-virtual {p4, v0, v1}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 269
    const-string v0, "PrinterActionHelper"

    const-string v1, "MyPrintDocumentAdapter"

    const-string v2, "onStart"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onStart()V

    .line 271
    return-void
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 11
    .param p1, "pages"    # [Landroid/print/PageRange;
    .param p2, "destination"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    .line 291
    const-string v7, "PrinterActionHelper"

    const-string v8, "MyPrintDocumentAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onWrite - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;->mPdfPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const/4 v3, 0x0

    .line 294
    .local v3, "input":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 297
    .local v5, "output":Ljava/io/OutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;->mPdfPath:Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    .end local v3    # "input":Ljava/io/InputStream;
    .local v4, "input":Ljava/io/InputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 300
    .end local v5    # "output":Ljava/io/OutputStream;
    .local v6, "output":Ljava/io/OutputStream;
    const/16 v7, 0x400

    :try_start_2
    new-array v0, v7, [B

    .line 303
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    if-lez v1, :cond_2

    .line 304
    const/4 v7, 0x0

    invoke-virtual {v6, v0, v7, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 311
    .end local v0    # "buf":[B
    .end local v1    # "bytesRead":I
    :catch_0
    move-exception v2

    move-object v5, v6

    .end local v6    # "output":Ljava/io/OutputStream;
    .restart local v5    # "output":Ljava/io/OutputStream;
    move-object v3, v4

    .line 312
    .end local v4    # "input":Ljava/io/InputStream;
    .local v2, "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "input":Ljava/io/InputStream;
    :goto_1
    :try_start_3
    const-string v7, "PrinterActionHelper"

    const-string v8, "MyPrintDocumentAdapter"

    const-string v9, "onWrite - FileNotFoundException"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 319
    if-eqz v3, :cond_0

    .line 320
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 322
    :cond_0
    if-eqz v5, :cond_1

    .line 323
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 330
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_2
    return-void

    .line 307
    .end local v3    # "input":Ljava/io/InputStream;
    .end local v5    # "output":Ljava/io/OutputStream;
    .restart local v0    # "buf":[B
    .restart local v1    # "bytesRead":I
    .restart local v4    # "input":Ljava/io/InputStream;
    .restart local v6    # "output":Ljava/io/OutputStream;
    :cond_2
    const/4 v7, 0x1

    :try_start_5
    new-array v7, v7, [Landroid/print/PageRange;

    const/4 v8, 0x0

    sget-object v9, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v9, v7, v8

    invoke-virtual {p4, v7}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 319
    if-eqz v4, :cond_3

    .line 320
    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 322
    :cond_3
    if-eqz v6, :cond_4

    .line 323
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_4
    move-object v5, v6

    .end local v6    # "output":Ljava/io/OutputStream;
    .restart local v5    # "output":Ljava/io/OutputStream;
    move-object v3, v4

    .line 328
    .end local v4    # "input":Ljava/io/InputStream;
    .restart local v3    # "input":Ljava/io/InputStream;
    goto :goto_2

    .line 325
    .end local v3    # "input":Ljava/io/InputStream;
    .end local v5    # "output":Ljava/io/OutputStream;
    .restart local v4    # "input":Ljava/io/InputStream;
    .restart local v6    # "output":Ljava/io/OutputStream;
    :catch_1
    move-exception v2

    .line 326
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "PrinterActionHelper"

    const-string v8, "MyPrintImageAdapter"

    const-string v9, "onWrite - IOException"

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 327
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .end local v6    # "output":Ljava/io/OutputStream;
    .restart local v5    # "output":Ljava/io/OutputStream;
    move-object v3, v4

    .line 329
    .end local v4    # "input":Ljava/io/InputStream;
    .restart local v3    # "input":Ljava/io/InputStream;
    goto :goto_2

    .line 325
    .end local v0    # "buf":[B
    .end local v1    # "bytesRead":I
    .local v2, "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v2

    .line 326
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "PrinterActionHelper"

    const-string v8, "MyPrintImageAdapter"

    const-string v9, "onWrite - IOException"

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 327
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 314
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 315
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_7
    const-string v7, "PrinterActionHelper"

    const-string v8, "MyPrintDocumentAdapter"

    const-string v9, "onWrite - Exception"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 319
    if-eqz v3, :cond_5

    .line 320
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 322
    :cond_5
    if-eqz v5, :cond_1

    .line 323
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_2

    .line 325
    :catch_4
    move-exception v2

    .line 326
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "PrinterActionHelper"

    const-string v8, "MyPrintImageAdapter"

    const-string v9, "onWrite - IOException"

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 327
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 318
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 319
    :goto_4
    if-eqz v3, :cond_6

    .line 320
    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 322
    :cond_6
    if-eqz v5, :cond_7

    .line 323
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 328
    :cond_7
    :goto_5
    throw v7

    .line 325
    :catch_5
    move-exception v2

    .line 326
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "PrinterActionHelper"

    const-string v9, "MyPrintImageAdapter"

    const-string v10, "onWrite - IOException"

    invoke-static {v8, v9, v10, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 327
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 318
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "input":Ljava/io/InputStream;
    .restart local v4    # "input":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "input":Ljava/io/InputStream;
    .restart local v3    # "input":Ljava/io/InputStream;
    goto :goto_4

    .end local v3    # "input":Ljava/io/InputStream;
    .end local v5    # "output":Ljava/io/OutputStream;
    .restart local v4    # "input":Ljava/io/InputStream;
    .restart local v6    # "output":Ljava/io/OutputStream;
    :catchall_2
    move-exception v7

    move-object v5, v6

    .end local v6    # "output":Ljava/io/OutputStream;
    .restart local v5    # "output":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "input":Ljava/io/InputStream;
    .restart local v3    # "input":Ljava/io/InputStream;
    goto :goto_4

    .line 314
    .end local v3    # "input":Ljava/io/InputStream;
    .restart local v4    # "input":Ljava/io/InputStream;
    :catch_6
    move-exception v2

    move-object v3, v4

    .end local v4    # "input":Ljava/io/InputStream;
    .restart local v3    # "input":Ljava/io/InputStream;
    goto :goto_3

    .end local v3    # "input":Ljava/io/InputStream;
    .end local v5    # "output":Ljava/io/OutputStream;
    .restart local v4    # "input":Ljava/io/InputStream;
    .restart local v6    # "output":Ljava/io/OutputStream;
    :catch_7
    move-exception v2

    move-object v5, v6

    .end local v6    # "output":Ljava/io/OutputStream;
    .restart local v5    # "output":Ljava/io/OutputStream;
    move-object v3, v4

    .end local v4    # "input":Ljava/io/InputStream;
    .restart local v3    # "input":Ljava/io/InputStream;
    goto :goto_3

    .line 311
    :catch_8
    move-exception v2

    goto/16 :goto_1

    .end local v3    # "input":Ljava/io/InputStream;
    .restart local v4    # "input":Ljava/io/InputStream;
    :catch_9
    move-exception v2

    move-object v3, v4

    .end local v4    # "input":Ljava/io/InputStream;
    .restart local v3    # "input":Ljava/io/InputStream;
    goto/16 :goto_1
.end method
