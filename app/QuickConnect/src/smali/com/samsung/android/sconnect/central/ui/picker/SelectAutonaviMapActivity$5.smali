.class Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;
.super Ljava/lang/Object;
.source "SelectAutonaviMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->updateActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v4, 0x0

    .line 320
    const-string v0, ""

    .line 322
    .local v0, "address":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-wide v2, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatE6:D

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-wide v2, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLngE6:D

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 323
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-wide v2, v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatE6:D

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-wide v4, v4, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLngE6:D

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getURL(DD)Ljava/lang/String;

    move-result-object v0

    .line 325
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 326
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, "mMenuDone"

    const-string v3, "address isEmpty"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09011f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 333
    :goto_0
    return-void

    .line 331
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->doFinish(Ljava/lang/String;)V

    goto :goto_0
.end method
