.class final Lcom/samsung/android/sconnect/common/util/GUIUtil$1;
.super Ljava/util/ArrayList;
.source "GUIUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/util/GUIUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 45
    const-class v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    .line 46
    const-class v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    .line 47
    const-class v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    .line 48
    const-class v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    .line 49
    const-class v0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    .line 50
    const-class v0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    .line 52
    const-string v0, "CHINA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    const-class v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    .line 57
    :goto_0
    const-class v0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    .line 58
    return-void

    .line 55
    :cond_0
    const-class v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
