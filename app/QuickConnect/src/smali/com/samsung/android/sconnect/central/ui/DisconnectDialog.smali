.class public Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;
.super Landroid/app/Activity;
.source "DisconnectDialog.java"


# static fields
.field private static final ASK_POPUP:I = 0xbb8

.field private static final OK_POPUP:I = 0xbb9

.field private static final STOP_MIRROR_POPUP:I = 0xbba

.field private static final TAG:Ljava/lang/String; = "DisconnectDialog"


# instance fields
.field private mAction:I

.field private mAskPopup:Z

.field mAskPopupDlg:Landroid/app/AlertDialog;

.field private mConfirm:Z

.field private mDeviceName:Ljava/lang/String;

.field private mIsMirroring:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mDeviceName:Ljava/lang/String;

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAction:I

    .line 27
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopup:Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    .line 31
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mConfirm:Z

    .line 33
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mIsMirroring:Z

    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mConfirm:Z

    return p1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->requestWindowFeature(I)Z

    .line 40
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 41
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "DEVICE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mDeviceName:Ljava/lang/String;

    .line 42
    const-string v1, "ACT"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAction:I

    .line 43
    const-string v1, "CONTINUE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopup:Z

    .line 44
    const-string v1, "DisconnectDialog"

    const-string v2, "onCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopup:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/SconnectManager;->getWfdUtil()Lcom/samsung/android/sconnect/common/util/WfdUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWfdConnected()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mIsMirroring:Z

    .line 47
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopup:Z

    if-eqz v1, :cond_1

    .line 48
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mIsMirroring:Z

    if-eqz v1, :cond_0

    .line 49
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/SconnectManager;->getWfdUtil()Lcom/samsung/android/sconnect/common/util/WfdUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getConnectedName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mDeviceName:Ljava/lang/String;

    .line 50
    const/16 v1, 0xbba

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->showDialog(I)V

    .line 57
    :goto_0
    return-void

    .line 52
    :cond_0
    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->showDialog(I)V

    goto :goto_0

    .line 55
    :cond_1
    const/16 v1, 0xbb9

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->showDialog(I)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/AlertDialog;
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 61
    const-string v1, "DisconnectDialog"

    const-string v2, "onCreateDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const/16 v1, 0xbb8

    if-ne p1, v1, :cond_2

    .line 65
    :try_start_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900e0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090088

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$3;-><init>(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090089

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$2;-><init>(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$1;-><init>(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    .line 85
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mDeviceName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v2

    const v3, 0x7f0900e2

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mDeviceName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 92
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    return-object v1

    .line 89
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v2

    const v3, 0x7f0900e4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DisconnectDialog"

    const-string v2, "onCreateDialog"

    const-string v3, "Exception on create popup"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 93
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const/16 v1, 0xbb9

    if-ne p1, v1, :cond_3

    .line 94
    :try_start_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900e5

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v2

    const v3, 0x7f0900e8

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090088

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$5;-><init>(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$4;-><init>(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    goto :goto_1

    .line 113
    :cond_3
    const/16 v1, 0xbba

    if-ne p1, v1, :cond_0

    .line 114
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09011c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09008b

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$8;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$8;-><init>(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090089

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$7;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$7;-><init>(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog$6;-><init>(Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    .line 134
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mDeviceName:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 135
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    const v2, 0x7f09011b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mDeviceName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 141
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_1

    .line 138
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v2

    const v3, 0x7f0900e4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2
.end method

.method protected bridge synthetic onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->onCreateDialog(I)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 152
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 153
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopup:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mConfirm:Z

    if-eqz v1, :cond_0

    .line 154
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.central.DISCONNECT_P2P"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 155
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "ACT"

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAction:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 156
    const-string v1, "FORCE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 157
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 159
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method protected popupDestroy()V
    .locals 4

    .prologue
    .line 164
    :try_start_0
    const-string v1, "DisconnectDialog"

    const-string v2, "popupDestroy"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mAskPopup:Z

    if-eqz v1, :cond_1

    .line 166
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->mIsMirroring:Z

    if-eqz v1, :cond_0

    .line 167
    const/16 v1, 0xbba

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->removeDialog(I)V

    .line 175
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->finish()V

    .line 179
    :goto_1
    return-void

    .line 169
    :cond_0
    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DisconnectDialog"

    const-string v2, "popupDestroy"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 172
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/16 v1, 0xbb9

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;->removeDialog(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
