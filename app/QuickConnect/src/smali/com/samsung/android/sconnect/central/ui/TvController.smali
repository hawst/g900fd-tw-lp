.class public Lcom/samsung/android/sconnect/central/ui/TvController;
.super Ljava/lang/Object;
.source "TvController.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "TvController"


# instance fields
.field private mContentsText:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mIsTvMatched:Z

.field private mIsWatchOn:Z

.field private mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

.field private mKeypadDialog:Landroid/app/Dialog;

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field mOnKeypadClickListener:Landroid/view/View$OnClickListener;

.field private mRegisterIntent:Z

.field private mRemoconMap:Ljava/util/HashMap;

.field private mResources:Landroid/content/res/Resources;

.field private mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field private mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

.field private mSetttingTv:Z

.field private mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

.field private mTooltipListener:Landroid/view/View$OnLongClickListener;

.field private mTvActionView:Landroid/widget/LinearLayout;

.field private mTvActionbarbtnLayout:Landroid/widget/LinearLayout;

.field private mTvAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTvControllerView:Landroid/widget/RelativeLayout;

.field private mTvCreateRoomBtn:Landroid/widget/Button;

.field private mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

.field private mTvInitView:Landroid/widget/LinearLayout;

.field private mTvMainView:Landroid/view/View;

.field private mTvMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mTvSelectBtn:Landroid/widget/Button;

.field private mTvSettingBtn:Landroid/widget/ImageButton;

.field private mTvTextView:Landroid/widget/TextView;

.field private mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

.field private mWatchonReceiver:Landroid/content/BroadcastReceiver;

.field private remoconConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sconnectDisplay"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p3, "contentsText"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    .line 70
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    .line 71
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    .line 72
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    .line 75
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvAdapter:Landroid/widget/ArrayAdapter;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    .line 84
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvCreateRoomBtn:Landroid/widget/Button;

    .line 85
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSelectBtn:Landroid/widget/Button;

    .line 86
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSettingBtn:Landroid/widget/ImageButton;

    .line 90
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    .line 91
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    .line 92
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSetttingTv:Z

    .line 93
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRegisterIntent:Z

    .line 95
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 97
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    .line 360
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/TvController$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/TvController$1;-><init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 498
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/TvController$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/TvController$2;-><init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTooltipListener:Landroid/view/View$OnLongClickListener;

    .line 770
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/TvController$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/TvController$5;-><init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->remoconConnection:Landroid/content/ServiceConnection;

    .line 974
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/TvController$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/TvController$7;-><init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mOnKeypadClickListener:Landroid/view/View$OnClickListener;

    .line 1039
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/TvController$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/TvController$8;-><init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchonReceiver:Landroid/content/BroadcastReceiver;

    .line 145
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    .line 146
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    .line 147
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .line 148
    iput-object p3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContentsText:Ljava/lang/String;

    .line 149
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/TvController;->showTvList(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/ui/TvController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->goSetting()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/central/ui/TvController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->initTvList()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/central/ui/TvController;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/sconnect/central/ui/TvController;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/TvController;->showTvController(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "x2"    # Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/TvController;->updateTvOnDB(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "x2"    # Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/TvController;->registerTvOnDB(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V

    return-void
.end method

.method static synthetic access$1702(Lcom/samsung/android/sconnect/central/ui/TvController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/TvController;->deleteTvOnDB(Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/android/sconnect/central/ui/TvController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSetttingTv:Z

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/ui/TvController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->goMainHome()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/ui/TvController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/central/ui/TvController;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;)Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/central/ui/TvController;)Ltv/peel/samsung/widget/service/IQuickConnectService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/android/sconnect/central/ui/TvController;Ltv/peel/samsung/widget/service/IQuickConnectService;)Ltv/peel/samsung/widget/service/IQuickConnectService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;
    .param p1, "x1"    # Ltv/peel/samsung/widget/service/IQuickConnectService;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    return-object p1
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/central/ui/TvController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TvController;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    return v0
.end method

.method private deleteTvOnDB(Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    .locals 6
    .param p1, "tv"    # Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .prologue
    .line 911
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    if-nez v0, :cond_0

    .line 912
    const-string v0, "TvController"

    const-string v1, "deleteTvOnDB"

    const-string v2, "mTvDbOpenHelper == null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    :goto_0
    return-void

    .line 915
    :cond_0
    if-nez p1, :cond_1

    .line 916
    const-string v0, "TvController"

    const-string v1, "deleteTvOnDB"

    const-string v2, "tv == null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 919
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->open()Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    .line 920
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    iget-object v1, p1, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomName:Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    iget-object v4, p1, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->deleteTV(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 922
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->close()V

    goto :goto_0
.end method

.method private goMainHome()V
    .locals 6

    .prologue
    .line 831
    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v3, :cond_1

    .line 832
    const-string v2, "sym://MainPage"

    .line 834
    .local v2, "url":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 835
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 836
    const v3, 0x10008000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 838
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 860
    .end local v2    # "url":Ljava/lang/String;
    :goto_0
    return-void

    .line 839
    .restart local v2    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 840
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 841
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.yosemite.tab"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 844
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.watchon.phone"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 849
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "url":Ljava/lang/String;
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 850
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v3, "tv.peel.smartremote"

    const-string v4, "com.peel.main.Home"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 851
    const/high16 v3, 0x30000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 854
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 855
    :catch_1
    move-exception v0

    .line 856
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "tv.peel.smartremote.for.note4"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private goSetting()V
    .locals 6

    .prologue
    .line 794
    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v3, :cond_1

    .line 795
    const-string v2, "sym://Settings"

    .line 797
    .local v2, "url":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 798
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 799
    const v3, 0x14000020

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 801
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSetttingTv:Z

    .line 803
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    .end local v2    # "url":Ljava/lang/String;
    :goto_0
    return-void

    .line 804
    .restart local v2    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 805
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "TvController"

    const-string v4, "goWatchOnSetting"

    const-string v5, "Yosemite is not installed"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 807
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.yosemite.tab"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 810
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.watchon.phone"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 815
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "url":Ljava/lang/String;
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 816
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v3, "tv.peel.smartremote"

    const-string v4, "com.peel.main.Home"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 817
    const/high16 v3, 0x30000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 822
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 823
    :catch_1
    move-exception v0

    .line 824
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "tv.peel.smartremote.for.note4"

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initTvList()V
    .locals 21

    .prologue
    .line 584
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move/from16 v17, v0

    if-eqz v17, :cond_7

    .line 585
    const/16 v16, 0x0

    .line 588
    .local v16, "watchonRoomList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 589
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;->getRoomList()Ljava/util/List;

    move-result-object v16

    .line 590
    if-eqz v16, :cond_0

    .line 591
    const-string v17, "TvController"

    const-string v18, "initTvList"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mRoomList count : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 598
    :cond_0
    :goto_0
    new-instance v17, Landroid/widget/ArrayAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v18, v0

    const v19, 0x1090003

    invoke-direct/range {v17 .. v19}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvAdapter:Landroid/widget/ArrayAdapter;

    .line 601
    if-eqz v16, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v17

    if-nez v17, :cond_4

    .line 602
    :cond_1
    const-string v17, "TvController"

    const-string v18, "initTvList"

    const-string v19, "mRoomList is null"

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    .end local v16    # "watchonRoomList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;>;"
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    move/from16 v17, v0

    if-eqz v17, :cond_e

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 684
    const-string v17, "TvController"

    const-string v18, "initTvList"

    const-string v19, "can control this TV"

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move/from16 v17, v0

    if-nez v17, :cond_3

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .line 687
    .local v14, "smartRemoteTv":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    if-eqz v14, :cond_3

    .line 688
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-object/from16 v17, v0

    iget-object v0, v14, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDevice:Ltv/peel/samsung/widget/service/Device;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->updateDevice(Ltv/peel/samsung/widget/service/Device;)V

    .line 700
    .end local v14    # "smartRemoteTv":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->showTvController(Z)V

    .line 701
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mSetttingTv:Z

    .line 702
    return-void

    .line 594
    .restart local v16    # "watchonRoomList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;>;"
    :catch_0
    move-exception v4

    .line 595
    .local v4, "e":Ljava/lang/Exception;
    const-string v17, "TvController"

    const-string v18, "initTvList"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 604
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;

    .line 606
    .local v10, "room":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    const/4 v8, 0x0

    .line 607
    .local v8, "remoteControlSettingList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;>;"
    :try_start_1
    invoke-virtual {v10}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->getRoomId()Ljava/lang/String;

    move-result-object v11

    .line 608
    .local v11, "roomId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    .line 609
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v11}, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;->getRemoteControlList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 610
    if-eqz v8, :cond_5

    .line 611
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    .line 613
    .local v13, "settingSize":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_4
    if-ge v5, v13, :cond_5

    .line 614
    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    .line 615
    .local v12, "setting":Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    const-string v17, "TV"

    invoke-virtual {v12}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getDeviceType()Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->name()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 616
    new-instance v3, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    invoke-direct {v3, v10, v12}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;-><init>(Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;)V

    .line 617
    .local v3, "device":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    if-eqz v3, :cond_6

    .line 618
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v17, v0

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 620
    const-string v17, "TvController"

    const-string v18, "initTvList"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mTvAdapter add "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 613
    .end local v3    # "device":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 627
    .end local v5    # "i":I
    .end local v11    # "roomId":Ljava/lang/String;
    .end local v12    # "setting":Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;
    .end local v13    # "settingSize":I
    :catch_1
    move-exception v4

    .line 628
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v17, "TvController"

    const-string v18, "initTvList"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 633
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "remoteControlSettingList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;>;"
    .end local v10    # "room":Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    .end local v16    # "watchonRoomList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;>;"
    :cond_7
    const/4 v15, 0x0

    .line 635
    .local v15, "srRoomList":[Ltv/peel/samsung/widget/service/Room;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ltv/peel/samsung/widget/service/IQuickConnectService;->getRoomList()[Ltv/peel/samsung/widget/service/Room;

    move-result-object v15

    .line 637
    if-eqz v15, :cond_8

    .line 638
    const-string v17, "TvController"

    const-string v18, "initTvList"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mRoomList count : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    array-length v0, v15

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 645
    :cond_8
    :goto_5
    new-instance v17, Landroid/widget/ArrayAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v18, v0

    const v19, 0x1090003

    invoke-direct/range {v17 .. v19}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvAdapter:Landroid/widget/ArrayAdapter;

    .line 648
    if-eqz v15, :cond_9

    array-length v0, v15

    move/from16 v17, v0

    if-nez v17, :cond_a

    .line 649
    :cond_9
    const-string v17, "TvController"

    const-string v18, "initTvList"

    const-string v19, "mRoomList is null"

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 641
    :catch_2
    move-exception v4

    .line 642
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v17, "TvController"

    const-string v18, "initTvList"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 651
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_a
    move-object v2, v15

    .local v2, "arr$":[Ltv/peel/samsung/widget/service/Room;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_6
    if-ge v6, v7, :cond_2

    aget-object v10, v2, v6

    .line 653
    .local v10, "room":Ltv/peel/samsung/widget/service/Room;
    const/4 v9, 0x0

    .line 654
    .local v9, "remoteControlSettingList":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Device;>;"
    :try_start_3
    invoke-virtual {v10}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v11

    .line 655
    .restart local v11    # "roomId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    move-object/from16 v17, v0

    if-eqz v17, :cond_c

    .line 656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v11}, Ltv/peel/samsung/widget/service/IQuickConnectService;->getRemoteControlList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 658
    if-eqz v9, :cond_c

    .line 659
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v13

    .line 661
    .restart local v13    # "settingSize":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_7
    if-ge v5, v13, :cond_c

    .line 662
    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ltv/peel/samsung/widget/service/Device;

    .line 663
    .local v12, "setting":Ltv/peel/samsung/widget/service/Device;
    const-string v17, "TV"

    invoke-virtual {v12}, Ltv/peel/samsung/widget/service/Device;->getType()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 664
    new-instance v3, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    invoke-direct {v3, v10, v12}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;-><init>(Ltv/peel/samsung/widget/service/Room;Ltv/peel/samsung/widget/service/Device;)V

    .line 665
    .restart local v3    # "device":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    if-eqz v3, :cond_b

    .line 666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v17, v0

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 668
    const-string v17, "TvController"

    const-string v18, "initTvList"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mTvAdapter add "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 661
    .end local v3    # "device":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    :cond_b
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 675
    .end local v5    # "i":I
    .end local v11    # "roomId":Ljava/lang/String;
    .end local v12    # "setting":Ltv/peel/samsung/widget/service/Device;
    .end local v13    # "settingSize":I
    :catch_3
    move-exception v4

    .line 676
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v17, "TvController"

    const-string v18, "initTvList"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_c
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_6

    .line 692
    .end local v2    # "arr$":[Ltv/peel/samsung/widget/service/Room;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v9    # "remoteControlSettingList":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Device;>;"
    .end local v10    # "room":Ltv/peel/samsung/widget/service/Room;
    .end local v15    # "srRoomList":[Ltv/peel/samsung/widget/service/Room;
    :cond_d
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    .line 693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->deleteTvOnDB(Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V

    .line 694
    const-string v17, "TvController"

    const-string v18, "initTvList"

    const-string v19, "can\'t control this TV - deleteTvOnDB"

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 697
    :cond_e
    const-string v17, "TvController"

    const-string v18, "initTvList"

    const-string v19, "can\'t control this TV  - bIsTvMatched false"

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private registerIntent()V
    .locals 3

    .prologue
    .line 982
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRegisterIntent:Z

    if-nez v1, :cond_0

    .line 983
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRegisterIntent:Z

    .line 984
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 985
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.ROOM_CREATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 986
    const-string v1, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.ROOM_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 987
    const-string v1, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.ROOM_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 988
    const-string v1, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.DEVICE_CREATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 989
    const-string v1, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.DEVICE_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 990
    const-string v1, "com.sec.msc.android.yosemite.infrastructure.common.broadcasting.room.DEVICE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 991
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 992
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchonReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 995
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private registerTvOnDB(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    .locals 9
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "tv"    # Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .prologue
    .line 863
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    if-nez v0, :cond_0

    .line 864
    const-string v0, "TvController"

    const-string v1, "registerTvOnDB"

    const-string v2, "mTvDbOpenHelper == null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    :goto_0
    return-void

    .line 867
    :cond_0
    if-nez p2, :cond_1

    .line 868
    const-string v0, "TvController"

    const-string v1, "registerTvOnDB"

    const-string v2, "tv == null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 871
    :cond_1
    const-string v0, "TvController"

    const-string v1, "registerTvOnDB !!!!!"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->open()Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    .line 874
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v4, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    iget-object v5, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomName:Ljava/lang/String;

    iget-object v6, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    iget-object v7, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->insert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 877
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->close()V

    goto :goto_0
.end method

.method private setTvView()V
    .locals 22

    .prologue
    .line 399
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v18, v0

    const v19, 0x7f0c0058

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/view/ViewStub;

    invoke-virtual/range {v18 .. v18}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMainView:Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMainView:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v18, v0

    const v19, 0x7f0c00a2

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvInitView:Landroid/widget/LinearLayout;

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v18, v0

    const v19, 0x7f0c008a

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvControllerView:Landroid/widget/RelativeLayout;

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvInitView:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    const v19, 0x7f0c00a3

    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvTextView:Landroid/widget/TextView;

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMainView:Landroid/view/View;

    move-object/from16 v18, v0

    const v19, 0x7f0c00a1

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvActionView:Landroid/widget/LinearLayout;

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvActionView:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMainView:Landroid/view/View;

    move-object/from16 v18, v0

    const v19, 0x7f0c009c

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvActionbarbtnLayout:Landroid/widget/LinearLayout;

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvInitView:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvControllerView:Landroid/widget/RelativeLayout;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMainView:Landroid/view/View;

    move-object/from16 v18, v0

    const v19, 0x7f0c00a0

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 420
    .local v10, "contentsTextview":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContentsText:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f070024

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v5, v0

    .line 423
    .local v5, "actionIconSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f070025

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v4, v0

    .line 426
    .local v4, "actionIconCircleSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 428
    .local v2, "action":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v18

    const v19, 0x7f030020

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    .line 430
    .local v17, "view":Landroid/view/View;
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout$LayoutParams;

    .line 431
    .local v15, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v15, :cond_0

    .line 432
    new-instance v15, Landroid/widget/LinearLayout$LayoutParams;

    .end local v15    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v18, -0x1

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v15, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 435
    .restart local v15    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    const/high16 v18, 0x3f800000    # 1.0f

    move/from16 v0, v18

    iput v0, v15, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 436
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 438
    const v18, 0x7f0c0089

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 440
    .local v3, "actionIconBgView":Landroid/widget/ImageView;
    const v18, 0x7f0c000a

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 441
    .local v6, "actionIconView":Landroid/widget/ImageView;
    const v18, 0x7f0c000b

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 443
    .local v7, "actionNameView":Landroid/widget/TextView;
    const-string v16, ""

    .line 444
    .local v16, "text":Ljava/lang/String;
    const/4 v13, -0x1

    .line 445
    .local v13, "imageId":I
    const/4 v9, -0x1

    .line 446
    .local v9, "color":I
    sparse-switch v2, :sswitch_data_0

    .line 467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f090022

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 468
    const v13, 0x7f020066

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f060004

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 473
    :goto_2
    invoke-static {v5, v4, v9}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getIconBackground(III)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 475
    .local v8, "background":Landroid/graphics/Bitmap;
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 477
    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    invoke-virtual {v6, v13}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 479
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 480
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 481
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 482
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvActionView:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 401
    .end local v2    # "action":I
    .end local v3    # "actionIconBgView":Landroid/widget/ImageView;
    .end local v4    # "actionIconCircleSize":I
    .end local v5    # "actionIconSize":I
    .end local v6    # "actionIconView":Landroid/widget/ImageView;
    .end local v7    # "actionNameView":Landroid/widget/TextView;
    .end local v8    # "background":Landroid/graphics/Bitmap;
    .end local v9    # "color":I
    .end local v10    # "contentsTextview":Landroid/widget/TextView;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "imageId":I
    .end local v15    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v16    # "text":Ljava/lang/String;
    .end local v17    # "view":Landroid/view/View;
    :catch_0
    move-exception v11

    .line 402
    .local v11, "e":Ljava/lang/NullPointerException;
    const-string v18, "TvController"

    const-string v19, "setTvView"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "ViewStub: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 448
    .end local v11    # "e":Ljava/lang/NullPointerException;
    .restart local v2    # "action":I
    .restart local v3    # "actionIconBgView":Landroid/widget/ImageView;
    .restart local v4    # "actionIconCircleSize":I
    .restart local v5    # "actionIconSize":I
    .restart local v6    # "actionIconView":Landroid/widget/ImageView;
    .restart local v7    # "actionNameView":Landroid/widget/TextView;
    .restart local v9    # "color":I
    .restart local v10    # "contentsTextview":Landroid/widget/TextView;
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v13    # "imageId":I
    .restart local v15    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .restart local v16    # "text":Ljava/lang/String;
    .restart local v17    # "view":Landroid/view/View;
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f090026

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 449
    const v13, 0x7f020067

    .line 450
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f060002

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 451
    goto :goto_2

    .line 453
    :sswitch_1
    const/4 v14, 0x0

    .line 454
    .local v14, "isMirroring":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getState()I

    move-result v18

    sget v19, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_MIRRORING:I

    and-int v18, v18, v19

    if-lez v18, :cond_1

    .line 455
    const/4 v14, 0x1

    .line 457
    :cond_1
    if-eqz v14, :cond_2

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f090028

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 462
    :goto_3
    const v13, 0x7f020067

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f060003

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 464
    goto/16 :goto_2

    .line 460
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f090027

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto :goto_3

    .line 486
    .end local v2    # "action":I
    .end local v3    # "actionIconBgView":Landroid/widget/ImageView;
    .end local v6    # "actionIconView":Landroid/widget/ImageView;
    .end local v7    # "actionNameView":Landroid/widget/TextView;
    .end local v9    # "color":I
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "imageId":I
    .end local v14    # "isMirroring":Z
    .end local v15    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v16    # "text":Ljava/lang/String;
    .end local v17    # "view":Landroid/view/View;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvActionbarbtnLayout:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v18, v0

    const v19, 0x7f0c009f

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageButton;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSettingBtn:Landroid/widget/ImageButton;

    .line 488
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSettingBtn:Landroid/widget/ImageButton;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSettingBtn:Landroid/widget/ImageButton;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTooltipListener:Landroid/view/View$OnLongClickListener;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 491
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    .line 492
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->showTvController(Z)V

    .line 496
    :goto_4
    return-void

    .line 494
    :cond_4
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->showTvController(Z)V

    goto :goto_4

    .line 446
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method

.method private showKeypad()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 949
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    if-nez v4, :cond_0

    .line 950
    new-instance v4, Landroid/app/Dialog;

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {v4, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    .line 951
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    invoke-virtual {v4, v6}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 952
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    invoke-virtual {v4, v6}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 954
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    const v5, 0x7f030011

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setContentView(I)V

    .line 956
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    const v5, 0x7f0c0040

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridLayout;

    .line 957
    .local v1, "keypad":Landroid/widget/GridLayout;
    if-eqz v1, :cond_1

    .line 958
    invoke-virtual {v1}, Landroid/widget/GridLayout;->getChildCount()I

    move-result v3

    .line 959
    .local v3, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 960
    invoke-virtual {v1, v0}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mOnKeypadClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 959
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 964
    .end local v0    # "i":I
    .end local v3    # "size":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 966
    .local v2, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f07001c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 968
    const/16 v4, 0x33

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 969
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f07003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 970
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 971
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    .line 972
    return-void
.end method

.method private showTvController(Z)V
    .locals 7
    .param p1, "mode"    # Z

    .prologue
    .line 531
    if-nez p1, :cond_4

    .line 532
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSettingBtn:Landroid/widget/ImageButton;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 533
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvInitView:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 534
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvControllerView:Landroid/widget/RelativeLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 536
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v4, 0x7f0c00a4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvCreateRoomBtn:Landroid/widget/Button;

    .line 537
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvCreateRoomBtn:Landroid/widget/Button;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 538
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v4, 0x7f0c00a5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSelectBtn:Landroid/widget/Button;

    .line 539
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSelectBtn:Landroid/widget/Button;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 542
    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v3, :cond_1

    .line 543
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvTextView:Landroid/widget/TextView;

    const v4, 0x7f0900a9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 549
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSelectBtn:Landroid/widget/Button;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 558
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v4, 0x7f0c009e

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 560
    .local v2, "tvNameView":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v1

    .line 561
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 562
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvActionbarbtnLayout:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    const v6, 0x7f090142

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 581
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "tvNameView":Landroid/widget/TextView;
    :cond_0
    :goto_2
    return-void

    .line 546
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvTextView:Landroid/widget/TextView;

    const v4, 0x7f0900ab

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 578
    :catch_0
    move-exception v0

    .line 579
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 551
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v3, :cond_3

    .line 552
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvTextView:Landroid/widget/TextView;

    const v4, 0x7f0900a7

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 556
    :goto_3
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSelectBtn:Landroid/widget/Button;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 554
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvTextView:Landroid/widget/TextView;

    const v4, 0x7f0900aa

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 565
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvSettingBtn:Landroid/widget/ImageButton;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 566
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvInitView:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 567
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvControllerView:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 568
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v4, 0x7f0c009e

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 570
    .restart local v2    # "tvNameView":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v1

    .line 571
    .restart local v1    # "name":Ljava/lang/String;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 572
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvActionbarbtnLayout:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    const v6, 0x7f090142

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 574
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mKeypadDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 575
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->showKeypad()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method private showTvList(Z)V
    .locals 3
    .param p1, "bUpdate"    # Z

    .prologue
    .line 926
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900ad

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvAdapter:Landroid/widget/ArrayAdapter;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/TvController$6;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/sconnect/central/ui/TvController$6;-><init>(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 946
    return-void
.end method

.method private updateTvOnDB(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    .locals 9
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "tv"    # Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .prologue
    .line 881
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    if-nez v0, :cond_0

    .line 882
    const-string v0, "TvController"

    const-string v1, "registerTvOnDB"

    const-string v2, "mTvDbOpenHelper == null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    :goto_0
    return-void

    .line 885
    :cond_0
    if-nez p2, :cond_1

    .line 886
    const-string v0, "TvController"

    const-string v1, "registerTvOnDB"

    const-string v2, "tv == null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 889
    :cond_1
    const-string v0, "TvController"

    const-string v1, "registerTvOnDB !!!!!"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->open()Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    .line 893
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 894
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v1, "P2PMAC"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->delete(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 904
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v4, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    iget-object v5, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomName:Ljava/lang/String;

    iget-object v6, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    iget-object v7, p2, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->insert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 907
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->close()V

    goto/16 :goto_0

    .line 896
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 897
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v1, "UUID"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->delete(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 899
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 900
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v1, "BTMAC"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->delete(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method


# virtual methods
.method public actionTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 1
    .param p1, "tv"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sconnect/central/ui/TvController;->actionTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 153
    return-void
.end method

.method public actionTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 29
    .param p1, "tv"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isWatchOn"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    .line 158
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "selected TV: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "(isWatchOn: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ")"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 162
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    .line 163
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v5, v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 165
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->bindController()V

    .line 167
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v4, :cond_10

    .line 168
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    .line 169
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c008d

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c008e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0090

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->CHANNEL_UP:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0091

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->CHANNEL_DOWN:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c008b

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->POWER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0093

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->VOLUME_MUTE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0096

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->SOURCE:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c004b

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_0:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0041

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_1:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0042

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_2:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0043

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_3:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0044

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_4:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0045

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_5:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0046

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_6:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0047

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_7:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0048

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_8:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0049

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->NUMBER_9:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c004a

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->DASH:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c004c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->ENTER:Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    if-nez v4, :cond_0

    .line 212
    new-instance v4, Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    .line 214
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->open()Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    .line 216
    const/16 v21, 0x0

    .line 217
    .local v21, "cur":Landroid/database/Cursor;
    const/16 v18, 0x0

    .line 218
    .local v18, "arg":Ljava/lang/String;
    const/16 v25, 0x0

    .line 219
    .local v25, "roomName":Ljava/lang/String;
    const/16 v24, 0x0

    .line 220
    .local v24, "roomID":Ljava/lang/String;
    const/16 v19, 0x0

    .line 221
    .local v19, "brand":Ljava/lang/String;
    const/16 v27, 0x0

    .line 223
    .local v27, "type":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v8, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    .line 224
    .local v8, "tvUUID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v6, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 225
    .local v6, "tvP2PMac":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v11, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    .line 227
    .local v11, "tvBTMac":Ljava/lang/String;
    const/16 v22, 0x0

    .line 229
    .local v22, "isFound":Z
    if-eqz v8, :cond_3

    .line 230
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    const-string v7, "Find TV on DB by tvUUID..."

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    move-object/from16 v18, v8

    .line 232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v5, "UUID"

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move-object/from16 v0, v18

    invoke-virtual {v4, v5, v0, v7}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->query(Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v21

    .line 233
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 234
    const-string v4, "ROOM_ID"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 235
    const-string v4, "ROOM_NAME"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 236
    const-string v4, "D_TYPE"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 237
    const-string v4, "D_BRAND"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 238
    const-string v4, "P2PMAC"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 239
    .local v23, "p2p":Ljava/lang/String;
    const-string v4, "BTMAC"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 241
    .local v20, "bt":Ljava/lang/String;
    if-nez v23, :cond_11

    const/4 v4, 0x1

    move v5, v4

    :goto_1
    if-eqz v6, :cond_12

    const/4 v4, 0x1

    :goto_2
    and-int/2addr v4, v5

    if-eqz v4, :cond_1

    .line 242
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v5, "P2PMAC"

    const-string v7, "UUID"

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual/range {v4 .. v9}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 245
    :cond_1
    if-nez v20, :cond_13

    const/4 v4, 0x1

    move v5, v4

    :goto_3
    if-eqz v11, :cond_14

    const/4 v4, 0x1

    :goto_4
    and-int/2addr v4, v5

    if-eqz v4, :cond_2

    .line 246
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v10, "BTMAC"

    const-string v12, "UUID"

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move-object v13, v8

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 249
    :cond_2
    const/16 v22, 0x1

    .line 250
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    const-string v7, "TV found by tvUUID!"

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    .end local v20    # "bt":Ljava/lang/String;
    .end local v23    # "p2p":Ljava/lang/String;
    :cond_3
    if-nez v22, :cond_7

    if-eqz v6, :cond_7

    .line 256
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    const-string v7, "Find TV on DB by tvP2PMac..."

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    move-object/from16 v18, v6

    .line 259
    if-eqz v21, :cond_4

    .line 260
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 263
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v5, "P2PMAC"

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move-object/from16 v0, v18

    invoke-virtual {v4, v5, v0, v7}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->query(Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v21

    .line 264
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 265
    const-string v4, "ROOM_ID"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 266
    const-string v4, "ROOM_NAME"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 267
    const-string v4, "D_TYPE"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 268
    const-string v4, "D_BRAND"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 269
    const-string v4, "UUID"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 270
    .local v28, "uuid":Ljava/lang/String;
    const-string v4, "BTMAC"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 272
    .restart local v20    # "bt":Ljava/lang/String;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz v8, :cond_5

    .line 273
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v13, "UUID"

    const-string v15, "P2PMAC"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move/from16 v17, v0

    move-object v14, v8

    move-object/from16 v16, v6

    invoke-virtual/range {v12 .. v17}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 277
    :cond_5
    if-nez v20, :cond_15

    const/4 v4, 0x1

    move v5, v4

    :goto_5
    if-eqz v11, :cond_16

    const/4 v4, 0x1

    :goto_6
    and-int/2addr v4, v5

    if-eqz v4, :cond_6

    .line 278
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v10, "BTMAC"

    const-string v12, "P2PMAC"

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move-object v13, v6

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 281
    :cond_6
    const/16 v22, 0x1

    .line 282
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    const-string v7, "TV found by tvP2PMac!"

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    .end local v20    # "bt":Ljava/lang/String;
    .end local v28    # "uuid":Ljava/lang/String;
    :cond_7
    if-nez v22, :cond_b

    if-eqz v11, :cond_b

    .line 287
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    const-string v7, "Find TV on DB by tvBTMac..."

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    move-object/from16 v18, v11

    .line 290
    if-eqz v21, :cond_8

    .line 291
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 294
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v5, "BTMAC"

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move-object/from16 v0, v18

    invoke-virtual {v4, v5, v0, v7}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->query(Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v21

    .line 295
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 296
    const-string v4, "ROOM_ID"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 297
    const-string v4, "ROOM_NAME"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 298
    const-string v4, "D_TYPE"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 299
    const-string v4, "D_BRAND"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 300
    const-string v4, "UUID"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 301
    .restart local v28    # "uuid":Ljava/lang/String;
    const-string v4, "P2PMAC"

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 303
    .restart local v23    # "p2p":Ljava/lang/String;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_9

    if-eqz v8, :cond_9

    .line 304
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v13, "UUID"

    const-string v15, "BTMAC"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move/from16 v17, v0

    move-object v14, v8

    move-object/from16 v16, v11

    invoke-virtual/range {v12 .. v17}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 308
    :cond_9
    if-nez v23, :cond_17

    const/4 v4, 0x1

    move v5, v4

    :goto_7
    if-eqz v6, :cond_18

    const/4 v4, 0x1

    :goto_8
    and-int/2addr v4, v5

    if-eqz v4, :cond_a

    .line 309
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    const-string v13, "P2PMAC"

    const-string v15, "BTMAC"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    move/from16 v17, v0

    move-object v14, v6

    move-object/from16 v16, v11

    invoke-virtual/range {v12 .. v17}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 312
    :cond_a
    const/16 v22, 0x1

    .line 313
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    const-string v7, "TV found by tvBTMac!"

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    .end local v23    # "p2p":Ljava/lang/String;
    .end local v28    # "uuid":Ljava/lang/String;
    :cond_b
    if-nez v22, :cond_c

    .line 318
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    const-string v7, "TV not found"

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :cond_c
    if-eqz v24, :cond_e

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_e

    if-eqz v25, :cond_e

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_e

    if-eqz v19, :cond_e

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_e

    if-eqz v27, :cond_e

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_e

    .line 323
    new-instance v4, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v27

    move-object/from16 v3, v19

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .line 324
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-nez v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    if-eqz v4, :cond_d

    .line 325
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->getString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .line 326
    .local v26, "smartRemoteTv":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    if-eqz v26, :cond_d

    .line 327
    const-string v4, "TvController"

    const-string v5, "actionTvController"

    const-string v7, "update smartRemote Device"

    invoke-static {v4, v5, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDevice:Ltv/peel/samsung/widget/service/Device;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->updateDevice(Ltv/peel/samsung/widget/service/Device;)V

    .line 331
    .end local v26    # "smartRemoteTv":Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    :cond_d
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    .line 334
    :cond_e
    if-eqz v21, :cond_f

    .line 335
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 337
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvDbOpenHelper:Lcom/samsung/android/sconnect/central/action/TvDBHelper;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/action/TvDBHelper;->close()V

    .line 339
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->setTvView()V

    .line 340
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->registerIntent()V

    .line 341
    return-void

    .line 189
    .end local v6    # "tvP2PMac":Ljava/lang/String;
    .end local v8    # "tvUUID":Ljava/lang/String;
    .end local v11    # "tvBTMac":Ljava/lang/String;
    .end local v18    # "arg":Ljava/lang/String;
    .end local v19    # "brand":Ljava/lang/String;
    .end local v21    # "cur":Landroid/database/Cursor;
    .end local v22    # "isFound":Z
    .end local v24    # "roomID":Ljava/lang/String;
    .end local v25    # "roomName":Ljava/lang/String;
    .end local v27    # "type":Ljava/lang/String;
    :cond_10
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    .line 190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c008d

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "Volume_Up"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c008e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "Volume_Down"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0090

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "Channel_Up"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0091

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "Channel_Down"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c008b

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "Power"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0093

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "Mute"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0096

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "Input"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c004b

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "0"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0041

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "1"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0042

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "2"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0043

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "3"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0044

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "4"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0045

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "5"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0046

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "6"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0047

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "7"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0048

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "8"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c0049

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "9"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c004a

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "."

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    const v5, 0x7f0c004c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v7, "Enter"

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 241
    .restart local v6    # "tvP2PMac":Ljava/lang/String;
    .restart local v8    # "tvUUID":Ljava/lang/String;
    .restart local v11    # "tvBTMac":Ljava/lang/String;
    .restart local v18    # "arg":Ljava/lang/String;
    .restart local v19    # "brand":Ljava/lang/String;
    .restart local v20    # "bt":Ljava/lang/String;
    .restart local v21    # "cur":Landroid/database/Cursor;
    .restart local v22    # "isFound":Z
    .restart local v23    # "p2p":Ljava/lang/String;
    .restart local v24    # "roomID":Ljava/lang/String;
    .restart local v25    # "roomName":Ljava/lang/String;
    .restart local v27    # "type":Ljava/lang/String;
    :cond_11
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_1

    :cond_12
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 245
    :cond_13
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_3

    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 277
    .end local v23    # "p2p":Ljava/lang/String;
    .restart local v28    # "uuid":Ljava/lang/String;
    :cond_15
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_5

    :cond_16
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 308
    .end local v20    # "bt":Ljava/lang/String;
    .restart local v23    # "p2p":Ljava/lang/String;
    :cond_17
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_7

    :cond_18
    const/4 v4, 0x0

    goto/16 :goto_8
.end method

.method public bindController()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1012
    const-string v2, "TvController"

    const-string v3, "bindController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bIsWatchOn: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", service: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    if-nez v1, :cond_2

    .line 1015
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1016
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "com.sec.yosemite.tab"

    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1018
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->remoconConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 1025
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_2
    return-void

    .line 1012
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    goto :goto_0

    .line 1016
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_1
    const-string v1, "com.sec.watchon.phone"

    goto :goto_1

    .line 1019
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    if-nez v1, :cond_3

    .line 1020
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Ltv/peel/samsung/widget/service/IQuickConnectService;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->remoconConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_2

    .line 1023
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->initTvList()V

    goto :goto_2
.end method

.method public buttonClick(Landroid/view/View;)V
    .locals 18
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 706
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v13

    const v14, 0x7f0c0099

    if-ne v13, v14, :cond_1

    .line 707
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->showKeypad()V

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    iget-object v10, v13, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    .line 710
    .local v10, "roomIdString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    iget-object v4, v13, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    .line 711
    .local v4, "deviceTypeString":Ljava/lang/String;
    if-eqz v10, :cond_2

    if-nez v4, :cond_3

    .line 712
    :cond_2
    const-string v13, "TvController"

    const-string v14, "buttonClick"

    const-string v15, "Room ID or Device Type is null!"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 764
    .end local v4    # "deviceTypeString":Ljava/lang/String;
    .end local v10    # "roomIdString":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 765
    .local v5, "e":Ljava/lang/Exception;
    const-string v13, "TvController"

    const-string v14, "buttonClick"

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 716
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v4    # "deviceTypeString":Ljava/lang/String;
    .restart local v10    # "roomIdString":Ljava/lang/String;
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    if-eqz v13, :cond_4

    .line 717
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;

    .line 718
    .local v8, "key":Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;
    const/4 v7, 0x0

    .line 719
    .local v7, "isLongClick":Z
    if-eqz v8, :cond_0

    .line 720
    invoke-virtual {v8}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;->name()Ljava/lang/String;

    move-result-object v9

    .line 721
    .local v9, "keyString":Ljava/lang/String;
    new-instance v13, Lcom/samsung/android/sconnect/central/ui/TvController$3;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v10, v4, v9}, Lcom/samsung/android/sconnect/central/ui/TvController$3;-><init>(Lcom/samsung/android/sconnect/central/ui/TvController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/central/ui/TvController$3;->start()V

    goto :goto_0

    .line 733
    .end local v7    # "isLongClick":Z
    .end local v8    # "key":Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoconKey;
    .end local v9    # "keyString":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    if-eqz v13, :cond_0

    .line 734
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    iget-object v3, v13, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDevice:Ltv/peel/samsung/widget/service/Device;

    .line 735
    .local v3, "device":Ltv/peel/samsung/widget/service/Device;
    if-eqz v3, :cond_0

    .line 736
    const/4 v12, 0x0

    .line 737
    .local v12, "smartRemoteKey":Ltv/peel/samsung/widget/service/Command;
    invoke-virtual {v3}, Ltv/peel/samsung/widget/service/Device;->getCommands()Ljava/util/List;

    move-result-object v2

    .line 738
    .local v2, "cmdList":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Command;>;"
    const-string v13, "TvController"

    const-string v14, "buttonClick"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "command: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    move-object/from16 v16, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "(cmdList: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/peel/samsung/widget/service/Command;

    .line 741
    .local v1, "c":Ltv/peel/samsung/widget/service/Command;
    invoke-virtual {v1}, Ltv/peel/samsung/widget/service/Command;->getCommandName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRemoconMap:Ljava/util/HashMap;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 742
    move-object v12, v1

    .line 743
    const-string v13, "TvController"

    const-string v14, "buttonClick"

    const-string v15, "command found!"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    .end local v1    # "c":Ltv/peel/samsung/widget/service/Command;
    :cond_6
    if-eqz v12, :cond_0

    .line 748
    move-object v11, v12

    .line 749
    .local v11, "smartKey":Ltv/peel/samsung/widget/service/Command;
    new-instance v13, Lcom/samsung/android/sconnect/central/ui/TvController$4;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v10, v3, v11}, Lcom/samsung/android/sconnect/central/ui/TvController$4;-><init>(Lcom/samsung/android/sconnect/central/ui/TvController;Ljava/lang/String;Ltv/peel/samsung/widget/service/Device;Ltv/peel/samsung/widget/service/Command;)V

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/central/ui/TvController$4;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public finishTvControl()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 344
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 345
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .line 346
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    .line 347
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSetttingTv:Z

    .line 348
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 349
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TvController;->unregisterWatchonIntent()V

    .line 350
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    if-eqz v1, :cond_0

    .line 352
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->remoconConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMainView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 358
    return-void

    .line 353
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "TvController"

    const-string v2, "finishTvControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " mBasicRemoconService has exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showTvController()V
    .locals 1

    .prologue
    .line 526
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsTvMatched:Z

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/central/ui/TvController;->showTvController(Z)V

    .line 527
    return-void
.end method

.method public unbindController()V
    .locals 5

    .prologue
    .line 1028
    const-string v2, "TvController"

    const-string v3, "unbindController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bIsWatchOn: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", service: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchOnService:Lcom/sec/msc/android/yosemite/service/remocon/IBasicRemoconService;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    if-eqz v1, :cond_1

    .line 1032
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->remoconConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1037
    :cond_1
    :goto_1
    return-void

    .line 1028
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mSmartRemoteService:Ltv/peel/samsung/widget/service/IQuickConnectService;

    goto :goto_0

    .line 1033
    :catch_0
    move-exception v0

    .line 1034
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "TvController"

    const-string v2, "unbindController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " service has exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public unregisterWatchonIntent()V
    .locals 5

    .prologue
    .line 998
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mIsWatchOn:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRegisterIntent:Z

    if-eqz v1, :cond_0

    .line 999
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mRegisterIntent:Z

    .line 1000
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 1002
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController;->mWatchonReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1009
    :cond_0
    :goto_0
    return-void

    .line 1003
    :catch_0
    move-exception v0

    .line 1004
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "TvController"

    const-string v2, "unregisterWatchonIntent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " mWatchonReceiver has exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
