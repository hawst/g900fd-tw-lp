.class public Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
.super Lcom/google/android/maps/MapActivity;
.source "SelectMapActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;,
        Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;,
        Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SearchLocationTask;
    }
.end annotation


# static fields
.field private static final GET_MYLOCATION:I = 0x0

.field private static final MENU_CANCEL:I = 0x65

.field private static final MENU_OK:I = 0x66

.field private static final OFFSET:I = 0x64

.field public static final RESULT_FAILED:I = 0x2

.field private static final UPDATE_LOCATION:I = 0x1

.field private static final UPDATE_MAP_FROM_LOCATION:I = 0x2


# instance fields
.field private TAG:Ljava/lang/String;

.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarMenuCancel:Landroid/view/MenuItem;

.field private mActionBarMenuDone:Landroid/view/MenuItem;

.field private mCurrentLocThread:Ljava/lang/Thread;

.field private mCurrentLocation:Landroid/widget/ImageButton;

.field private mGeo:Landroid/location/Geocoder;

.field private mLocation:Ljava/lang/String;

.field mLocationListener:Landroid/location/LocationListener;

.field private mLocationManager:Landroid/location/LocationManager;

.field protected mMapView:Lcom/google/android/maps/MapView;

.field private mMenuCancel:Landroid/widget/Button;

.field private mMenuDone:Landroid/widget/Button;

.field private mPickImage:Z

.field private mSearchButton:Landroid/widget/ImageButton;

.field private mSearchPoint:Lcom/google/android/maps/GeoPoint;

.field mSearchRunnable:Ljava/lang/Runnable;

.field private mSearchText:Landroid/widget/EditText;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSelectMapHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lcom/google/android/maps/MapActivity;-><init>()V

    .line 75
    const-string v0, "SelectMapActivity"

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mPickImage:Z

    .line 96
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;

    .line 105
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    .line 110
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMenuCancel:Landroid/widget/Button;

    .line 112
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMenuDone:Landroid/widget/Button;

    .line 114
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocThread:Ljava/lang/Thread;

    .line 124
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBarMenuCancel:Landroid/view/MenuItem;

    .line 125
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBarMenuDone:Landroid/view/MenuItem;

    .line 273
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSelectMapHandler:Landroid/os/Handler;

    .line 494
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$7;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationListener:Landroid/location/LocationListener;

    .line 592
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$8;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchRunnable:Ljava/lang/Runnable;

    .line 988
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->setSearchButtonState(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->searchLocation()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getLocationServiceAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateMapFromLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mPickImage:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getFromLocationName(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/location/Geocoder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mGeo:Landroid/location/Geocoder;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;D)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # D

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToInt(D)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;IILjava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->addMarker(IILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;IIII)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->moveToMarker(IIII)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->moveToMarker(Lcom/google/android/maps/GeoPoint;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocThread:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p1, "x1"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSelectMapHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addMarker(IILjava/lang/String;)Z
    .locals 3
    .param p1, "lat"    # I
    .param p2, "lon"    # I
    .param p3, "snippet"    # Ljava/lang/String;

    .prologue
    .line 667
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v1

    .line 668
    .local v1, "mapOverlays":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    .line 669
    .local v0, "itemizedOverlay":Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2, p3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getOverlay(IILandroid/graphics/drawable/Drawable;Ljava/lang/String;)Lcom/google/android/maps/OverlayItem;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 670
    const/4 v2, 0x1

    return v2
.end method

.method private convToDouble(I)D
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 654
    int-to-double v0, p1

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private convToInt(D)I
    .locals 3
    .param p1, "d"    # D

    .prologue
    .line 650
    const-wide v0, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v0

    return v0
.end method

.method private getFromLocationName(Ljava/lang/String;)Z
    .locals 20
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, ""

    const-string v19, "getFromLocationName"

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_1

    .line 552
    :cond_0
    const/16 v17, 0x0

    .line 589
    :goto_0
    return v17

    .line 555
    :cond_1
    new-instance v6, Landroid/location/Geocoder;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 556
    .local v6, "geo":Landroid/location/Geocoder;
    const v15, 0x55d4a80

    .local v15, "minLat":I
    const v13, -0x55d4a80

    .line 557
    .local v13, "maxLat":I
    const v16, 0xaba9500

    .local v16, "minLon":I
    const v14, -0xaba9500

    .line 559
    .local v14, "maxLon":I
    const/16 v17, 0x5

    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v6, v0, v1}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v10

    .line 560
    .local v10, "loc":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_4

    .line 561
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v12

    .line 562
    .local v12, "mapOverlays":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    .line 563
    .local v8, "itemizedOverlay":Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
    invoke-virtual {v8}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->clearOverlay()V

    .line 564
    const/4 v4, 0x0

    .line 565
    .local v4, "addr":Landroid/location/Address;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v7, v0, :cond_2

    .line 566
    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "addr":Landroid/location/Address;
    check-cast v4, Landroid/location/Address;

    .line 567
    .restart local v4    # "addr":Landroid/location/Address;
    invoke-virtual {v4}, Landroid/location/Address;->getLatitude()D

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToInt(D)I

    move-result v9

    .line 568
    .local v9, "lat":I
    invoke-virtual {v4}, Landroid/location/Address;->getLongitude()D

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToInt(D)I

    move-result v11

    .line 569
    .local v11, "lon":I
    invoke-virtual {v4}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v9, v11, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->addMarker(IILjava/lang/String;)Z

    .line 570
    invoke-static {v15, v9}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 571
    invoke-static {v13, v9}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 572
    move/from16 v0, v16

    invoke-static {v0, v11}, Ljava/lang/Math;->min(II)I

    move-result v16

    .line 573
    invoke-static {v14, v11}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 565
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 576
    .end local v9    # "lat":I
    .end local v11    # "lon":I
    :cond_2
    if-eqz v4, :cond_3

    .line 577
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v15, v13, v1, v14}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->moveToMarker(IIII)Z

    .line 589
    .end local v4    # "addr":Landroid/location/Address;
    .end local v7    # "i":I
    .end local v8    # "itemizedOverlay":Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
    .end local v10    # "loc":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v12    # "mapOverlays":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    :cond_3
    :goto_2
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 580
    .restart local v10    # "loc":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_4
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f09011d

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 584
    .end local v10    # "loc":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :catch_0
    move-exception v5

    .line 585
    .local v5, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "getFromLocationName"

    const-string v19, "IOException"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 586
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 587
    .local v5, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "getFromLocationName"

    const-string v19, "Exception"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private getGeoStringgetFromLocation(DD)Ljava/lang/String;
    .locals 3
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 848
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLocationServiceAvailable()Z
    .locals 2

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMyLocation()Lcom/google/android/maps/GeoPoint;
    .locals 13

    .prologue
    .line 428
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v1, "getMyLocation"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const/4 v12, 0x0

    .line 431
    .local v12, "networkLocationEnabled":Z
    const/4 v9, 0x0

    .line 434
    .local v9, "gpsLocationEnabled":Z
    const/4 v11, 0x0

    .line 435
    .local v11, "myLocation":Lcom/google/android/maps/GeoPoint;
    const/4 v10, 0x0

    .line 448
    .local v10, "location":Landroid/location/Location;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v12

    .line 450
    if-eqz v12, :cond_0

    .line 451
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v1, "getMyLocation"

    const-string v2, "from NETWORK_PROVIDER"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v10

    .line 453
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x447a0000    # 1000.0f

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    :cond_0
    :goto_0
    if-nez v12, :cond_1

    .line 463
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v9

    .line 465
    if-eqz v9, :cond_1

    .line 466
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v1, "getMyLocation"

    const-string v2, "from GPS_PROVIDER"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v10

    .line 468
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x447a0000    # 1000.0f

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 477
    :cond_1
    :goto_1
    if-nez v12, :cond_2

    if-nez v9, :cond_2

    .line 478
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v1, "getMyLocation"

    const-string v2, "Can not use location service!!!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const/4 v0, 0x0

    .line 491
    :goto_2
    return-object v0

    .line 456
    :catch_0
    move-exception v6

    .line 457
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v1, "getMyLocation"

    const-string v2, "Exception from NETWORK_PROVIDER"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 458
    const/4 v12, 0x0

    goto :goto_0

    .line 471
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 472
    .restart local v6    # "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v1, "getMyLocation"

    const-string v2, "Exception from GPS_PROVIDER"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 473
    const/4 v9, 0x0

    goto :goto_1

    .line 482
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v10, :cond_3

    .line 483
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v1, "getMyLocation"

    const-string v2, "got LastKnownLocation"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    .line 485
    .local v7, "geoLat":Ljava/lang/Double;
    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    .line 487
    .local v8, "geoLng":Ljava/lang/Double;
    new-instance v11, Lcom/google/android/maps/GeoPoint;

    .end local v11    # "myLocation":Lcom/google/android/maps/GeoPoint;
    invoke-virtual {v7}, Ljava/lang/Double;->intValue()I

    move-result v0

    invoke-virtual {v8}, Ljava/lang/Double;->intValue()I

    move-result v1

    invoke-direct {v11, v0, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .end local v7    # "geoLat":Ljava/lang/Double;
    .end local v8    # "geoLng":Ljava/lang/Double;
    .restart local v11    # "myLocation":Lcom/google/android/maps/GeoPoint;
    :goto_3
    move-object v0, v11

    .line 491
    goto :goto_2

    .line 489
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v1, "getMyLocation"

    const-string v2, "no LastKnownLocation"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private getOverlay(IILandroid/graphics/drawable/Drawable;Ljava/lang/String;)Lcom/google/android/maps/OverlayItem;
    .locals 3
    .param p1, "latitude"    # I
    .param p2, "longitude"    # I
    .param p3, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p4, "snippet"    # Ljava/lang/String;

    .prologue
    .line 674
    new-instance v1, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v1, p1, p2}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 675
    .local v1, "point":Lcom/google/android/maps/GeoPoint;
    new-instance v0, Lcom/google/android/maps/OverlayItem;

    const-string v2, ""

    invoke-direct {v0, v1, v2, p4}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    .local v0, "overlay":Lcom/google/android/maps/OverlayItem;
    invoke-virtual {v0, p3}, Lcom/google/android/maps/OverlayItem;->setMarker(Landroid/graphics/drawable/Drawable;)V

    .line 677
    return-object v0
.end method

.method private hideSip()V
    .locals 3

    .prologue
    .line 852
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 853
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 854
    return-void
.end method

.method private initMarker()Z
    .locals 5

    .prologue
    .line 658
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v2

    .line 659
    .local v2, "mapOverlays":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200a0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 660
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    invoke-direct {v1, p0, p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 661
    .local v1, "itemizedOverlay":Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662
    const/4 v3, 0x1

    return v3
.end method

.method private moveToMarker(IIII)Z
    .locals 6
    .param p1, "minLat"    # I
    .param p2, "maxLat"    # I
    .param p3, "minLon"    # I
    .param p4, "maxLon"    # I

    .prologue
    .line 691
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "move to marker"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    sub-int v0, p2, p1

    .line 693
    .local v0, "lat_span":I
    sub-int v1, p4, p3

    .line 694
    .local v1, "lon_span":I
    new-instance v2, Lcom/google/android/maps/GeoPoint;

    div-int/lit8 v3, v0, 0x2

    add-int/2addr v3, p1

    div-int/lit8 v4, v1, 0x2

    add-int/2addr v4, p3

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 697
    .local v2, "point":Lcom/google/android/maps/GeoPoint;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 698
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    .line 699
    const/4 v3, 0x1

    return v3
.end method

.method private moveToMarker(Lcom/google/android/maps/GeoPoint;)Z
    .locals 4
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    const/4 v3, 0x1

    .line 683
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 684
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    .line 686
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$1;)V

    new-array v1, v3, [Lcom/google/android/maps/GeoPoint;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 687
    return v3
.end method

.method private searchLocation()V
    .locals 5

    .prologue
    .line 533
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v2, ""

    const-string v3, "searchLocation"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 537
    .local v0, "location":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 538
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v2, ""

    const-string v3, "empay location to search"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    :goto_0
    return-void

    .line 542
    :cond_0
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;

    .line 544
    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SearchLocationTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SearchLocationTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$1;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SearchLocationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 546
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->hideSip()V

    goto :goto_0
.end method

.method private setSearchButtonState(I)V
    .locals 3
    .param p1, "length"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 263
    if-gtz p1, :cond_0

    .line 264
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 265
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 270
    :goto_0
    return-void

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 268
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_0
.end method

.method private updateActionBar()V
    .locals 3

    .prologue
    .line 333
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_0

    .line 334
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 335
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBar:Landroid/app/ActionBar;

    const v2, 0x7f090121

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBar:Landroid/app/ActionBar;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 338
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBar:Landroid/app/ActionBar;

    const v2, 0x7f030005

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 339
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    .line 340
    .local v0, "customView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 341
    const v1, 0x7f0c0013

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMenuCancel:Landroid/widget/Button;

    .line 342
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMenuCancel:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$5;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    const v1, 0x7f0c0014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMenuDone:Landroid/widget/Button;

    .line 351
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMenuDone:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private updateLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    .locals 15
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 752
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v3, ""

    const-string v4, "updateLocation"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    const-string v12, ""

    .line 754
    .local v12, "location":Ljava/lang/String;
    const/4 v9, 0x0

    .line 756
    .local v9, "data":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :try_start_0
    new-instance v1, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 757
    .local v1, "geoCoder":Landroid/location/Geocoder;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v9

    .line 759
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 760
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 761
    .local v0, "address":Landroid/location/Address;
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v8

    .line 762
    .local v8, "addressLines":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    if-gt v11, v8, :cond_2

    .line 763
    invoke-virtual {v0, v11}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v7

    .line 764
    .local v7, "addressLine":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 765
    if-nez v11, :cond_1

    .line 766
    move-object v12, v7

    .line 762
    :cond_0
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 768
    :cond_1
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 769
    .local v14, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v14, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 770
    const-string v2, " "

    invoke-virtual {v14, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 771
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    goto :goto_1

    .line 776
    .end local v0    # "address":Landroid/location/Address;
    .end local v1    # "geoCoder":Landroid/location/Geocoder;
    .end local v7    # "addressLine":Ljava/lang/String;
    .end local v8    # "addressLines":I
    .end local v11    # "i":I
    .end local v14    # "sb":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v10

    .line 777
    .local v10, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v3, "updateLocation"

    const-string v4, "IOException. return raw data"

    invoke-static {v2, v3, v4, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 778
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v4

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getGeoStringgetFromLocation(DD)Ljava/lang/String;

    move-result-object v12

    move-object v13, v12

    .line 783
    .end local v10    # "e":Ljava/io/IOException;
    .end local v12    # "location":Ljava/lang/String;
    .local v13, "location":Ljava/lang/String;
    :goto_2
    return-object v13

    .line 782
    .end local v13    # "location":Ljava/lang/String;
    .restart local v1    # "geoCoder":Landroid/location/Geocoder;
    .restart local v12    # "location":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v3, "updateLocation"

    const-string v4, "complete"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v13, v12

    .line 783
    .end local v12    # "location":Ljava/lang/String;
    .restart local v13    # "location":Ljava/lang/String;
    goto :goto_2
.end method

.method private updateMapFromLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    .locals 25
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 788
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v9, ""

    const-string v10, "updateLocationfromGetMyLocation"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    const-string v17, ""

    .line 790
    .local v17, "location":Ljava/lang/String;
    const/4 v13, 0x0

    .line 792
    .local v13, "data":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :try_start_0
    new-instance v7, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v8}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 793
    .local v7, "geoCoder":Landroid/location/Geocoder;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v10

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v10

    const/4 v12, 0x1

    invoke-virtual/range {v7 .. v12}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v13

    .line 795
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v8}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v20

    .line 796
    .local v20, "mapOverlays":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    .line 797
    .local v15, "itemizedOverlay":Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->clearOverlay()V

    .line 799
    const v23, 0x55d4a80

    .local v23, "minLat":I
    const v21, -0x55d4a80

    .line 800
    .local v21, "maxLat":I
    const v24, 0xaba9500

    .local v24, "minLon":I
    const v22, -0xaba9500

    .line 802
    .local v22, "maxLon":I
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 803
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v9, ""

    const-string v10, "location data found in updateLocationfromGetMyLocation"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    const/4 v8, 0x0

    invoke-interface {v13, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/Address;

    .line 806
    .local v6, "addr":Landroid/location/Address;
    if-eqz v6, :cond_0

    .line 808
    invoke-virtual {v6}, Landroid/location/Address;->getLatitude()D

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToInt(D)I

    move-result v16

    .line 809
    .local v16, "lat":I
    invoke-virtual {v6}, Landroid/location/Address;->getLongitude()D

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToInt(D)I

    move-result v19

    .line 810
    .local v19, "lon":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "lat lon:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    invoke-virtual {v6}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v19

    invoke-direct {v0, v1, v2, v8}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->addMarker(IILjava/lang/String;)Z

    .line 812
    move/from16 v0, v23

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v23

    .line 813
    move/from16 v0, v21

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 814
    move/from16 v0, v24

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v24

    .line 815
    move/from16 v0, v22

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 817
    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v21

    move/from16 v3, v24

    move/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->moveToMarker(IIII)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 842
    .end local v6    # "addr":Landroid/location/Address;
    .end local v16    # "lat":I
    .end local v19    # "lon":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v9, "updateMapFromLocation"

    const-string v10, "complete"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v18, v17

    .line 843
    .end local v7    # "geoCoder":Landroid/location/Geocoder;
    .end local v17    # "location":Ljava/lang/String;
    .local v18, "location":Ljava/lang/String;
    :goto_0
    return-object v18

    .line 820
    .end local v15    # "itemizedOverlay":Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
    .end local v18    # "location":Ljava/lang/String;
    .end local v20    # "mapOverlays":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    .end local v21    # "maxLat":I
    .end local v22    # "maxLon":I
    .end local v23    # "minLat":I
    .end local v24    # "minLon":I
    .restart local v17    # "location":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 821
    .local v14, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v9, "updateMapFromLocation"

    const-string v10, "IOException. return raw data"

    invoke-static {v8, v9, v10, v14}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 822
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v10

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getGeoStringgetFromLocation(DD)Ljava/lang/String;

    move-result-object v17

    .line 824
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v8}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v20

    .line 825
    .restart local v20    # "mapOverlays":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/maps/Overlay;>;"
    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    .line 826
    .restart local v15    # "itemizedOverlay":Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->clearOverlay()V

    .line 828
    const v23, 0x55d4a80

    .restart local v23    # "minLat":I
    const v21, -0x55d4a80

    .line 829
    .restart local v21    # "maxLat":I
    const v24, 0xaba9500

    .restart local v24    # "minLon":I
    const v22, -0xaba9500

    .line 831
    .restart local v22    # "maxLon":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToInt(D)I

    move-result v16

    .line 832
    .restart local v16    # "lat":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToInt(D)I

    move-result v19

    .line 833
    .restart local v19    # "lon":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "lat lon:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    const/4 v8, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v19

    invoke-direct {v0, v1, v2, v8}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->addMarker(IILjava/lang/String;)Z

    .line 835
    move/from16 v0, v23

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v23

    .line 836
    move/from16 v0, v21

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 837
    move/from16 v0, v24

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v24

    .line 838
    move/from16 v0, v22

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v22

    .line 839
    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v21

    move/from16 v3, v24

    move/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->moveToMarker(IIII)Z

    move-object/from16 v18, v17

    .line 840
    .end local v17    # "location":Ljava/lang/String;
    .restart local v18    # "location":Ljava/lang/String;
    goto/16 :goto_0
.end method


# virtual methods
.method protected doFinish(Ljava/lang/String;)V
    .locals 5
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 936
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v2, "doFinish"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "return result..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 938
    .local v0, "resultIntent":Landroid/content/Intent;
    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 939
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->setResult(ILandroid/content/Intent;)V

    .line 944
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->finish()V

    .line 965
    return-void

    .line 941
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 942
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected getURL(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    .locals 7
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 926
    const-string v4, ""

    .line 927
    .local v4, "returnURL":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v0

    .line 928
    .local v0, "lat":D
    invoke-virtual {p1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->convToDouble(I)D

    move-result-wide v2

    .line 929
    .local v2, "lng":D
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "http://maps.google.com/maps?f=q&q=("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    .line 931
    return-object v4
.end method

.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 969
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 132
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 138
    const v4, 0x7f030016

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->setContentView(I)V

    .line 141
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBar:Landroid/app/ActionBar;

    .line 142
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateActionBar()V

    .line 145
    new-instance v4, Landroid/location/Geocoder;

    invoke-direct {v4, p0}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mGeo:Landroid/location/Geocoder;

    .line 146
    const v4, 0x7f0c0064

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/maps/MapView;

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    .line 147
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v4, v6}, Lcom/google/android/maps/MapView;->setBuiltInZoomControls(Z)V

    .line 148
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v4}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v4

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 149
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->initMarker()Z

    .line 154
    const-string v3, "location"

    .line 155
    .local v3, "serviceString":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/location/LocationManager;

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    .line 157
    const v4, 0x7f0c0062

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SearchView;

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchView:Landroid/widget/SearchView;

    .line 158
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4, v6}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 159
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchView:Landroid/widget/SearchView;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/widget/SearchView;->setImeOptions(I)V

    .line 160
    const-string v4, "search"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    .line 162
    .local v1, "searchManager":Landroid/app/SearchManager;
    new-instance v0, Landroid/content/ComponentName;

    const-string v4, "com.samsung.android.sconnect"

    const-string v5, "com.samsung.android.sconnect.central.ui.picker.SelectMapActivity"

    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .local v0, "cn":Landroid/content/ComponentName;
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1, v0}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 165
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 167
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "android:id/search_src_text"

    invoke-virtual {v4, v5, v7, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 169
    .local v2, "searchTextId":I
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4, v2}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;

    .line 185
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 186
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v5, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$1;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 202
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_2

    .line 203
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->length()I

    move-result v4

    if-gtz v4, :cond_1

    .line 204
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 205
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 207
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    new-instance v5, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$2;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    :cond_2
    const v4, 0x7f0c0063

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocation:Landroid/widget/ImageButton;

    .line 218
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocation:Landroid/widget/ImageButton;

    new-instance v5, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 705
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    const/16 v0, 0x65

    const v1, 0x7f090089

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBarMenuCancel:Landroid/view/MenuItem;

    .line 707
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBarMenuCancel:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 710
    const/16 v0, 0x66

    const v1, 0x7f09008c

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBarMenuDone:Landroid/view/MenuItem;

    .line 711
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mActionBarMenuDone:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 714
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 416
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onDestroy()V

    .line 417
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 719
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v3, "onOptionsItemSelected"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 748
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 724
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->finish()V

    goto :goto_0

    .line 727
    :sswitch_1
    const-string v0, ""

    .line 729
    .local v0, "address":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mPickImage:Z

    if-eqz v2, :cond_1

    .line 730
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    if-eqz v2, :cond_0

    .line 731
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateMapFromLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    .line 733
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    if-eqz v2, :cond_1

    .line 734
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getURL(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v0

    .line 737
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 738
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;

    const-string v3, "mMenuDone"

    const-string v4, "address isEmpty"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09011f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 743
    :cond_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->doFinish(Ljava/lang/String;)V

    goto :goto_0

    .line 721
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x66 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 1094
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 1099
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->searchLocation()V

    .line 1100
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 399
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onResume()V

    .line 400
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 392
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onStart()V

    .line 393
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSelectMapHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 394
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 395
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 404
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onStop()V

    .line 405
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationListener:Landroid/location/LocationListener;

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 412
    :cond_0
    return-void
.end method
