.class public Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;
.super Landroid/app/Activity;
.source "AcceptDialog.java"


# static fields
.field private static final MSG_TIMEOUT_ACCEPT:I = 0x7d1

.field static final TAG:Ljava/lang/String; = "AcceptDialog"

.field private static final TIMEOUT_ACCEPT:I = 0x7530

.field private static final TIME_DELAY:I = 0x1388


# instance fields
.field private MAX_COUNT:I

.field private mAction:I

.field private mBssid:Ljava/lang/String;

.field private mBtMacAddr:Ljava/lang/String;

.field private mContentType:I

.field private mContext:Landroid/content/Context;

.field private mFileNumber:I

.field private mFileType:I

.field protected mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

.field private mHandler:Landroid/os/Handler;

.field private mId:Ljava/lang/String;

.field private mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

.field private final mLocalReceiver:Landroid/content/BroadcastReceiver;

.field private mNewConnection:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mReg:Z

.field protected mResources:Landroid/content/res/Resources;

.field private mResult:Z

.field private mSoundNotification:Landroid/app/Notification;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 60
    iput-boolean v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mNewConnection:Z

    .line 63
    iput-boolean v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mResult:Z

    .line 64
    iput-boolean v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mReg:Z

    .line 66
    iput-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 71
    const/16 v0, 0xfa

    iput v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    .line 447
    new-instance v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$3;-><init>(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    .line 464
    new-instance v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$4;-><init>(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 505
    new-instance v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$5;-><init>(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mResult:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mId:Ljava/lang/String;

    return-object v0
.end method

.method private clearSoundNotification()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 497
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 499
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    if-eqz v1, :cond_0

    .line 500
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    iget v1, v1, Landroid/app/Notification;->icon:I

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    .line 501
    iput-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    .line 503
    :cond_0
    return-void
.end method

.method private getFileShareMessage()Ljava/lang/String;
    .locals 7

    .prologue
    const v6, 0x7f09003f

    const v5, 0x7f090042

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 290
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileType:I

    packed-switch v0, :pswitch_data_0

    .line 394
    :pswitch_0
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_13

    .line 395
    const v0, 0x7f090037

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 400
    :goto_0
    return-object v0

    .line 292
    :pswitch_1
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_0

    .line 293
    const v0, 0x7f090039

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 294
    :cond_0
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_1

    .line 295
    const v0, 0x7f090044

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 298
    :cond_1
    const v0, 0x7f090044

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 302
    :pswitch_2
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_2

    .line 303
    const v0, 0x7f09003a

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 304
    :cond_2
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_3

    .line 305
    const v0, 0x7f090045

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 308
    :cond_3
    const v0, 0x7f090045

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 312
    :pswitch_3
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_4

    .line 313
    const v0, 0x7f090038

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 314
    :cond_4
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_5

    .line 315
    const v0, 0x7f090043

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 318
    :cond_5
    const v0, 0x7f090043

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 322
    :pswitch_4
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_6

    .line 323
    const v0, 0x7f090033

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 325
    :cond_6
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v1, v0, v3

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 329
    :pswitch_5
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_7

    .line 330
    const v0, 0x7f090036

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 331
    :cond_7
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_8

    .line 332
    const v0, 0x7f090041

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 335
    :cond_8
    const v0, 0x7f090041

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 339
    :pswitch_6
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_9

    .line 340
    const v0, 0x7f09003e

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 341
    :cond_9
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_a

    .line 342
    const v0, 0x7f090049

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 345
    :cond_a
    const v0, 0x7f090049

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 349
    :pswitch_7
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_b

    .line 350
    const v0, 0x7f09003d

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 351
    :cond_b
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_c

    .line 352
    const v0, 0x7f090048

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 355
    :cond_c
    const v0, 0x7f090048

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 359
    :pswitch_8
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_d

    .line 360
    const v0, 0x7f09003b

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 361
    :cond_d
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_e

    .line 362
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v1, v0, v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 365
    :cond_e
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v1, v0, v3

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 370
    :pswitch_9
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_f

    .line 371
    const v0, 0x7f090035

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 372
    :cond_f
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_10

    .line 373
    const v0, 0x7f090040

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 376
    :cond_10
    const v0, 0x7f090040

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 381
    :pswitch_a
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    if-ne v0, v4, :cond_11

    .line 382
    const v0, 0x7f090034

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 383
    :cond_11
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_12

    .line 384
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v1, v0, v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v6, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 387
    :cond_12
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v1, v0, v3

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v6, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 396
    :cond_13
    iget v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_14

    .line 397
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v1, v0, v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->MAX_COUNT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 400
    :cond_14
    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    aput-object v1, v0, v3

    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method private getIconResource()I
    .locals 2

    .prologue
    const v0, 0x7f020007

    .line 258
    iget v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileType:I

    packed-switch v1, :pswitch_data_0

    .line 285
    :pswitch_0
    const v0, 0x7f02004e

    :goto_0
    :pswitch_1
    return v0

    .line 260
    :pswitch_2
    const v0, 0x7f020005

    goto :goto_0

    .line 262
    :pswitch_3
    const v0, 0x7f02009f

    goto :goto_0

    .line 264
    :pswitch_4
    const v0, 0x7f020052

    goto :goto_0

    .line 266
    :pswitch_5
    const v0, 0x7f020053

    goto :goto_0

    .line 272
    :pswitch_6
    const v0, 0x7f020006

    goto :goto_0

    .line 274
    :pswitch_7
    const v0, 0x7f02000a

    goto :goto_0

    .line 276
    :pswitch_8
    const v0, 0x7f020054

    goto :goto_0

    .line 279
    :pswitch_9
    const v0, 0x7f020055

    goto :goto_0

    .line 281
    :pswitch_a
    const v0, 0x7f020065

    goto :goto_0

    .line 258
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private releaseWakeLock()V
    .locals 3

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    const-string v0, "AcceptDialog"

    const-string v1, "releaseWakeLock"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 423
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 425
    :cond_0
    return-void
.end method

.method private setIntentFilter()V
    .locals 5

    .prologue
    .line 428
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mReg:Z

    if-nez v1, :cond_0

    .line 429
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.sconnect.periph.REQUEST_CANCEL"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 432
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 433
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 434
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 435
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mReg:Z

    .line 437
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private setView()V
    .locals 21

    .prologue
    .line 155
    const/high16 v17, 0x7f030000

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->setContentView(I)V

    .line 156
    const v17, 0x7f0c0001

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 157
    .local v8, "deviceImageView":Landroid/widget/ImageView;
    const/high16 v17, 0x7f0c0000

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 158
    .local v7, "deviceImageBackgroundView":Landroid/widget/ImageView;
    const v17, 0x7f0c0002

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 159
    .local v9, "devicetextView":Landroid/widget/TextView;
    const v17, 0x7f0c0004

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 160
    .local v14, "messageView":Landroid/widget/TextView;
    const v17, 0x7f0c0003

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 162
    .local v15, "requestIconView":Landroid/widget/ImageView;
    const v17, 0x7f0c0008

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 163
    .local v3, "acceptButton":Landroid/widget/Button;
    const v17, 0x7f0c0007

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 164
    .local v5, "declineButton":Landroid/widget/Button;
    const/16 v16, -0x1

    .line 165
    .local v16, "type":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mNumber:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getContactImage(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 167
    .local v12, "icon":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/sconnect/SconnectManager;->getFavoriteList()Ljava/util/ArrayList;

    move-result-object v10

    .line 168
    .local v10, "favoriteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/central/ui/DisplayItem;>;"
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_1

    .line 169
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 170
    .local v13, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v13}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    .line 171
    .local v6, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 172
    const-string v17, "AcceptDialog"

    const-string v18, "setView"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "It\'s favorite device: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getFavoriteImage()Landroid/graphics/Bitmap;

    move-result-object v17

    if-eqz v17, :cond_1

    .line 174
    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getFavoriteImage()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 180
    .end local v6    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_1
    if-gtz v16, :cond_3

    if-eqz v12, :cond_3

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f070023

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v1, v12}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->cropIcon(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 193
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mAction:I

    move/from16 v17, v0

    sparse-switch v17, :sswitch_data_0

    .line 233
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mNewConnection:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    .line 234
    const v17, 0x7f0c0005

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 235
    .local v4, "connectionTextView":Landroid/widget/TextView;
    if-eqz v4, :cond_2

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-object/from16 v17, v0

    const v18, 0x7f0900e2

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    .end local v4    # "connectionTextView":Landroid/widget/TextView;
    :cond_2
    new-instance v17, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$1;-><init>(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    new-instance v17, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$2;-><init>(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    return-void

    .line 184
    :cond_3
    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_4

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceType:I

    move/from16 v16, v0

    .line 187
    :cond_4
    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_5

    const/16 v17, 0x1d

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_6

    .line 188
    :cond_5
    new-instance v17, Ljava/util/Random;

    invoke-direct/range {v17 .. v17}, Ljava/util/Random;-><init>()V

    const/16 v18, 0x3

    invoke-virtual/range {v17 .. v18}, Ljava/util/Random;->nextInt(I)I

    move-result v17

    add-int/lit8 v16, v17, 0x1a

    .line 190
    :cond_6
    sget-object v17, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceImagesLarge1:[I

    add-int/lit8 v18, v16, -0x1

    aget v17, v17, v18

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 191
    sget-object v17, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceBgImages:[I

    add-int/lit8 v18, v16, -0x1

    aget v17, v17, v18

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 198
    :sswitch_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getFileShareMessage()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getIconResource()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/sconnect/common/util/Util;->setSCRunningSetting(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 204
    :sswitch_1
    const v17, 0x7f020080

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 206
    const v17, 0x7f090064

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 210
    :sswitch_2
    const v17, 0x7f090067

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 214
    :sswitch_3
    const v17, 0x7f020076

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 215
    const v17, 0x7f09006c

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 221
    :sswitch_4
    const v17, 0x7f09006d

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 225
    :sswitch_5
    const v17, 0x7f02007d

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-object/from16 v17, v0

    const v18, 0x7f090030

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 194
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_3
        0x9 -> :sswitch_3
        0xe -> :sswitch_1
        0x10 -> :sswitch_4
        0x11 -> :sswitch_2
        0x12 -> :sswitch_5
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method private setWakeLock()V
    .locals 4

    .prologue
    .line 408
    const-string v1, "AcceptDialog"

    const-string v2, "setWakeLock"

    const-string v3, "Screen locked: awake from sleep mode"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 411
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    const-string v2, "SCONNECT_AcceptDialog"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 414
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 415
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v2, 0x1770

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 417
    :cond_0
    return-void
.end method

.method private soundNotification()V
    .locals 5

    .prologue
    .line 481
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 483
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-nez v0, :cond_0

    .line 494
    :goto_0
    return-void

    .line 486
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    if-nez v1, :cond_1

    .line 487
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    .line 488
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    const-wide/16 v2, 0x0

    iput-wide v2, v1, Landroid/app/Notification;->when:J

    .line 489
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    iget v2, v1, Landroid/app/Notification;->defaults:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/app/Notification;->defaults:I

    .line 490
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    iget v2, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v1, Landroid/app/Notification;->flags:I

    .line 492
    :cond_1
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    iget v2, v2, Landroid/app/Notification;->icon:I

    iget-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mSoundNotification:Landroid/app/Notification;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method private unregisterIntentFilter()V
    .locals 2

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mReg:Z

    if-eqz v0, :cond_0

    .line 441
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 442
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 443
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mReg:Z

    .line 445
    :cond_0
    return-void
.end method

.method private waitingConnecting()V
    .locals 2

    .prologue
    .line 476
    const v0, 0x7f0c0006

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 477
    const v0, 0x7f0c0009

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 478
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 148
    const-string v0, "AcceptDialog"

    const-string v1, "onConfigurationChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "newConfing.orientation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->setView()V

    .line 151
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 152
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 77
    const-string v3, "AcceptDialog"

    const-string v4, "onCreate"

    const-string v5, ""

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    .line 81
    iget-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mResources:Landroid/content/res/Resources;

    .line 82
    iget-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 84
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 85
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_2

    .line 94
    const-string v3, "KEY"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mId:Ljava/lang/String;

    .line 95
    const-string v3, "CMD"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mAction:I

    .line 96
    const-string v3, "COUNT"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileNumber:I

    .line 97
    const-string v3, "CONTENTTYPE"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mFileType:I

    .line 98
    const-string v3, "NEWCONNECTION"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mNewConnection:Z

    .line 99
    const-string v3, "BSSID"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mBssid:Ljava/lang/String;

    .line 100
    const-string v3, "CONTENT_TYPE"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContentType:I

    .line 101
    const-string v3, "BT_MAC_ADDRESS"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mBtMacAddr:Ljava/lang/String;

    .line 103
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 104
    .local v0, "b":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 105
    const-string v3, "DEVICE_INFO"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 106
    .local v2, "mList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;>;"
    if-eqz v2, :cond_0

    .line 107
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iput-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 110
    .end local v2    # "mList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;>;"
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->setIntentFilter()V

    .line 111
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->setView()V

    .line 112
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->setWakeLock()V

    .line 113
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->soundNotification()V

    .line 114
    iget-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x7d1

    const-wide/16 v6, 0x7530

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 121
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 118
    :cond_2
    const-string v3, "AcceptDialog"

    const-string v4, "onCreate"

    const-string v5, "ERROR - missed Intent data"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 125
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x7d1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 128
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->clearSoundNotification()V

    .line 129
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->releaseWakeLock()V

    .line 130
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->unregisterIntentFilter()V

    .line 132
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.periph.ACCEPT_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 133
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMD"

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mAction:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 134
    const-string v1, "KEY"

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v1, "ADDRESS"

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mInfo:Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const-string v1, "RES"

    iget-boolean v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mResult:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 137
    const-string v1, "BSSID"

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mBssid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const-string v1, "CONTENT_TYPE"

    iget v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContentType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 139
    const-string v1, "BT_MAC_ADDRESS"

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mBtMacAddr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 142
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/common/util/Util;->setSCRunningSetting(Landroid/content/Context;I)V

    .line 143
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 144
    return-void
.end method
