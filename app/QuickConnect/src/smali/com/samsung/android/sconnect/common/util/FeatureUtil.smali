.class public Lcom/samsung/android/sconnect/common/util/FeatureUtil;
.super Ljava/lang/Object;
.source "FeatureUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FeatureUtil"

.field private static mIsBleAutoEnable:I

.field private static mIsBleScanFilterSupported:I

.field private static mIsCHNDevice:I

.field private static mIsChinaNalSecurity:I

.field private static mIsIRSupport:I

.field private static mIsJPNDevice:I

.field private static mIsLightTheme:I

.field private static mIsServeyModeOn:I

.field private static mIsSlinkSupport:I

.field private static mIsTablet:I

.field private static mIsTmobile:I

.field private static mIsVerizon:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleAutoEnable:I

    .line 19
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleScanFilterSupported:I

    .line 20
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTablet:I

    .line 21
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsIRSupport:I

    .line 22
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsSlinkSupport:I

    .line 23
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsCHNDevice:I

    .line 24
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsJPNDevice:I

    .line 25
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsVerizon:I

    .line 26
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTmobile:I

    .line 27
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsServeyModeOn:I

    .line 28
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsLightTheme:I

    .line 29
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsChinaNalSecurity:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hasPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 222
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 223
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 225
    .local v1, "hasPkg":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :goto_0
    return v1

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .line 229
    const-string v3, "FeatureUtil"

    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Package not found : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isBleAutoEnable()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 32
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleAutoEnable:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 33
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v3, "SEC_FLOATING_FEATURE_BLUETOOTH_ENABLE_AUTO_LE"

    invoke-virtual {v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 35
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleAutoEnable:I

    .line 40
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleAutoEnable:I

    if-ne v2, v0, :cond_2

    .line 43
    :goto_1
    return v0

    .line 37
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleAutoEnable:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 43
    goto :goto_1
.end method

.method public static isBleScanFilterSupported()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    sget v4, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleScanFilterSupported:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 49
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->isOffloadedFilteringSupported()Z

    move-result v1

    .line 51
    .local v1, "isoffloading":Z
    if-eqz v1, :cond_2

    .line 52
    const-string v4, "FeatureUtil"

    const-string v5, "isBleScanFilterSupported"

    const-string v6, "isOffloadedFilteringSupported() : enabled"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const/4 v4, 0x1

    sput v4, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleScanFilterSupported:I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 69
    :cond_0
    :goto_0
    sget v4, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleScanFilterSupported:I

    if-ne v4, v3, :cond_1

    move v2, v3

    .line 72
    :cond_1
    :goto_1
    return v2

    .line 56
    :cond_2
    :try_start_1
    const-string v4, "FeatureUtil"

    const-string v5, "isBleScanFilterSupported"

    const-string v6, "isOffloadedFilteringSupported() : disabled"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const/4 v4, 0x0

    sput v4, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleScanFilterSupported:I
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v4, "FeatureUtil"

    const-string v5, "isBleScanFilterSupported"

    const-string v6, "isOffloadedFilteringSupported() : NoSuchMethodError"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    sput v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsBleScanFilterSupported:I

    goto :goto_0

    .line 64
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_1
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v3, "FeatureUtil"

    const-string v4, "isBleScanFilterSupported"

    const-string v5, "NullPointerException"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static isCHNDevice()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 128
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsCHNDevice:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 129
    const-string v2, "CHINA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 130
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsCHNDevice:I

    .line 135
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsCHNDevice:I

    if-ne v2, v0, :cond_2

    .line 138
    :goto_1
    return v0

    .line 132
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsCHNDevice:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 138
    goto :goto_1
.end method

.method public static isChinaNalSecurity()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 241
    sget v3, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsChinaNalSecurity:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 242
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 244
    .local v0, "ChinaNalSecurityType":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "ChinaNalSecurity"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 245
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsChinaNalSecurity:I

    .line 250
    :cond_0
    :goto_0
    sget v3, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsChinaNalSecurity:I

    if-ne v3, v1, :cond_2

    .line 253
    :goto_1
    return v1

    .line 247
    :cond_1
    sput v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsChinaNalSecurity:I

    goto :goto_0

    :cond_2
    move v1, v2

    .line 253
    goto :goto_1
.end method

.method public static isIRSupport(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 90
    sget v4, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsIRSupport:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 91
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "android.hardware.consumerir"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 93
    .local v1, "mHasConsumerIr":Z
    if-eqz v1, :cond_2

    .line 94
    const-string v4, "consumer_ir"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/ConsumerIrManager;

    .line 96
    .local v0, "mCIR":Landroid/hardware/ConsumerIrManager;
    invoke-virtual {v0}, Landroid/hardware/ConsumerIrManager;->hasIrEmitter()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 97
    sput v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsIRSupport:I

    .line 105
    .end local v0    # "mCIR":Landroid/hardware/ConsumerIrManager;
    .end local v1    # "mHasConsumerIr":Z
    :cond_0
    :goto_0
    sget v4, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsIRSupport:I

    if-ne v4, v2, :cond_3

    .line 108
    :goto_1
    return v2

    .line 99
    .restart local v0    # "mCIR":Landroid/hardware/ConsumerIrManager;
    .restart local v1    # "mHasConsumerIr":Z
    :cond_1
    sput v3, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsIRSupport:I

    goto :goto_0

    .line 102
    .end local v0    # "mCIR":Landroid/hardware/ConsumerIrManager;
    :cond_2
    sput v3, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsIRSupport:I

    goto :goto_0

    .end local v1    # "mHasConsumerIr":Z
    :cond_3
    move v2, v3

    .line 108
    goto :goto_1
.end method

.method public static isJPNDevice()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 142
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsJPNDevice:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 143
    const-string v2, "JP"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 144
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsJPNDevice:I

    .line 149
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsJPNDevice:I

    if-ne v2, v0, :cond_2

    .line 152
    :goto_1
    return v0

    .line 146
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsJPNDevice:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 152
    goto :goto_1
.end method

.method public static isLDUModel()Z
    .locals 2

    .prologue
    .line 236
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "PAP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FOP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLightTheme()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 184
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsLightTheme:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 185
    const-string v2, "ro.build.scafe.cream"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "white"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 186
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsLightTheme:I

    .line 191
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsLightTheme:I

    if-ne v2, v0, :cond_2

    .line 194
    :goto_1
    return v0

    .line 188
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsLightTheme:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 194
    goto :goto_1
.end method

.method public static isRoaming(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 214
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 217
    .local v0, "mTelephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v1

    .line 218
    .local v1, "roamingState":Z
    return v1
.end method

.method public static isServeyModeOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 198
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsServeyModeOn:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 199
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v3, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 201
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsServeyModeOn:I

    .line 206
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsServeyModeOn:I

    if-ne v2, v0, :cond_2

    .line 209
    :goto_1
    return v0

    .line 203
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsServeyModeOn:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 209
    goto :goto_1
.end method

.method public static isSlinkSupport(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 113
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsSlinkSupport:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 114
    const-string v2, "com.samsung.android.sdk.samsunglink"

    invoke-static {p0, v2}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 116
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsSlinkSupport:I

    .line 121
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsSlinkSupport:I

    if-ne v2, v0, :cond_2

    .line 124
    :goto_1
    return v0

    .line 118
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsSlinkSupport:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 124
    goto :goto_1
.end method

.method public static isTablet()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 76
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTablet:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 77
    const-string v2, "ro.build.characteristics"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "tablet"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 78
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTablet:I

    .line 83
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTablet:I

    if-ne v2, v0, :cond_2

    .line 86
    :goto_1
    return v0

    .line 80
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTablet:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 86
    goto :goto_1
.end method

.method public static isTmobile()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 170
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTmobile:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 171
    const-string v2, "TMB"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 172
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTmobile:I

    .line 177
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTmobile:I

    if-ne v2, v0, :cond_2

    .line 180
    :goto_1
    return v0

    .line 174
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsTmobile:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 180
    goto :goto_1
.end method

.method public static isVerizon()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 156
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsVerizon:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 157
    const-string v2, "VZW"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 158
    sput v0, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsVerizon:I

    .line 163
    :cond_0
    :goto_0
    sget v2, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsVerizon:I

    if-ne v2, v0, :cond_2

    .line 166
    :goto_1
    return v0

    .line 160
    :cond_1
    sput v1, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->mIsVerizon:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 166
    goto :goto_1
.end method
