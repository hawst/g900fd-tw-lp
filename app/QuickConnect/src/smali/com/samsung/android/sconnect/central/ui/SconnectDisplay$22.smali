.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onIconPickerResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$data:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2622
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->val$data:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2625
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mUriProcessor:Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->val$data:Landroid/content/Intent;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->process(Landroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v3

    .line 2626
    .local v3, "uris":[Landroid/net/Uri;
    if-eqz v3, :cond_0

    aget-object v4, v3, v7

    if-eqz v4, :cond_0

    .line 2627
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.android.camera.action.CROP"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2628
    .local v2, "intent":Landroid/content/Intent;
    aget-object v4, v3, v7

    const-string v5, "image/*"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2629
    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2631
    const-string v4, "output"

    aget-object v5, v3, v7

    invoke-static {v4, v5}, Landroid/content/ClipData;->newRawUri(Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    .line 2632
    const-string v4, "crop"

    const-string v5, "true"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2633
    const-string v4, "scale"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2634
    const-string v4, "scaleUpIfNeeded"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2635
    const-string v4, "aspectX"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2636
    const-string v4, "aspectY"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2637
    const-string v4, "return-data"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2638
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070023

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 2640
    .local v1, "iconSize":I
    const-string v4, "outputX"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2641
    const-string v4, "outputY"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2642
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    .line 2643
    .local v0, "dir":Ljava/io/File;
    if-eqz v0, :cond_1

    .line 2644
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 2645
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCropIconUri:Landroid/net/Uri;
    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5302(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/net/Uri;)Landroid/net/Uri;

    .line 2648
    const-string v4, "output"

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCropIconUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2649
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/16 v5, 0x3ed

    invoke-virtual {v4, v2, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2656
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "iconSize":I
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 2651
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v1    # "iconSize":I
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_1
    const-string v4, "SconnectDisplay"

    const-string v5, "onIconPickerResult"

    const-string v6, "fail to getExternalCacheDir()"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2652
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0900e6

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
