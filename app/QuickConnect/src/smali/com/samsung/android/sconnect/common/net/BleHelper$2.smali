.class Lcom/samsung/android/sconnect/common/net/BleHelper$2;
.super Landroid/bluetooth/le/ScanCallback;
.source "BleHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/BleHelper;)V
    .locals 0

    .prologue
    .line 568
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-direct {p0}, Landroid/bluetooth/le/ScanCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onScanFailed(I)V
    .locals 3
    .param p1, "arg0"    # I

    .prologue
    .line 571
    const-string v0, "BleHelper"

    const-string v1, "LeScanFilterCallback"

    const-string v2, "onScanFailed"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    return-void
.end method

.method public onScanResult(ILandroid/bluetooth/le/ScanResult;)V
    .locals 4
    .param p1, "callbackType"    # I
    .param p2, "result"    # Landroid/bluetooth/le/ScanResult;

    .prologue
    .line 576
    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-nez v0, :cond_0

    .line 577
    const-string v0, "BleHelper"

    const-string v1, "LeScanFilterCallback"

    const-string v2, "Ignore  device is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    :goto_0
    return-void

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getRssi()I

    move-result v2

    invoke-virtual {p2}, Landroid/bluetooth/le/ScanResult;->getScanRecord()Landroid/bluetooth/le/ScanRecord;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/le/ScanRecord;->getBytes()[B

    move-result-object v3

    # invokes: Lcom/samsung/android/sconnect/common/net/BleHelper;->parseBlePacket(Landroid/bluetooth/BluetoothDevice;I[B)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/bluetooth/BluetoothDevice;I[B)V

    goto :goto_0
.end method
