.class Lcom/samsung/android/sconnect/common/net/P2pHelper$6;
.super Ljava/lang/Object;
.source "P2pHelper.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/common/net/P2pHelper;->createGroup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0

    .prologue
    .line 602
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4
    .param p1, "arg0"    # I

    .prologue
    .line 605
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$102(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)Z

    .line 606
    const-string v0, "P2pHelper"

    const-string v1, "createGroup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFailure "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    return-void
.end method

.method public onSuccess()V
    .locals 3

    .prologue
    .line 611
    const-string v0, "P2pHelper"

    const-string v1, "createGroup"

    const-string v2, "onSuccess "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    return-void
.end method
