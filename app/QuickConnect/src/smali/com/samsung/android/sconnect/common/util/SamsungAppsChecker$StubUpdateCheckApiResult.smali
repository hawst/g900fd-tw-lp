.class public Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;
.super Ljava/lang/Object;
.source "SamsungAppsChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StubUpdateCheckApiResult"
.end annotation


# instance fields
.field appId:Ljava/lang/String;

.field resultCode:Ljava/lang/String;

.field resultMsg:Ljava/lang/String;

.field versionCode:I

.field versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->appId:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->resultCode:Ljava/lang/String;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->resultMsg:Ljava/lang/String;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->versionCode:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->versionName:Ljava/lang/String;

    return-void
.end method
