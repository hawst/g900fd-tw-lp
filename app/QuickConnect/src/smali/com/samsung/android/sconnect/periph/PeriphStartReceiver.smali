.class public Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PeriphStartReceiver.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "PeriphStartReceiver"

    sput-object v0, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "action":Ljava/lang/String;
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 25
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/samsung/android/sconnect/common/util/Util;->setSCRunningSetting(Landroid/content/Context;I)V

    .line 26
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    if-nez v1, :cond_2

    .line 27
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "USER_OWNER care ACTION_BOOT_COMPLETED. run P service"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-static {p1}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->needToStart(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    invoke-static {p1, v4}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->startPeriphService(Landroid/content/Context;Z)Landroid/content/ComponentName;

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "no need to start"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_2
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "not USER_OWNER when ACTION_BOOT_COMPLETED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_3
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 37
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    if-nez v1, :cond_5

    .line 38
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "USER_OWNER care CONNECTIVITY_ACTION. run P service"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-static {p1}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->needToStart(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 40
    invoke-static {p1, v4}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->startPeriphService(Landroid/content/Context;Z)Landroid/content/ComponentName;

    goto :goto_0

    .line 42
    :cond_4
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "no need to start"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_5
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "not USER_OWNER when CONNECTIVITY_ACTION"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_6
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    const-string v1, "package:com.samsung.android.sconnect"

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    if-nez v1, :cond_8

    .line 50
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "USER_OWNER care ACTION_PACKAGE_REPLACED. run P service"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-static {p1}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->needToStart(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 53
    invoke-static {p1, v4}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->startPeriphService(Landroid/content/Context;Z)Landroid/content/ComponentName;

    .line 61
    :goto_1
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "will re-install BeaconManager if exists"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v1, "com.samsung.android.beaconmanager"

    invoke-static {p1, v1}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    const-string v1, "BeaconManager.apk"

    invoke-static {p1, v1}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->startActionInstallApk(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 55
    :cond_7
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "no need to start"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 58
    :cond_8
    sget-object v1, Lcom/samsung/android/sconnect/periph/PeriphStartReceiver;->TAG:Ljava/lang/String;

    const-string v2, "onReceive"

    const-string v3, "not USER_OWNER when ACTION_PACKAGE_REPLACED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
