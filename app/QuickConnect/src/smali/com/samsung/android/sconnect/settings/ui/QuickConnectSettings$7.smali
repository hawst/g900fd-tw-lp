.class Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$7;
.super Landroid/content/BroadcastReceiver;
.source "QuickConnectSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$7;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 346
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 348
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$7;->isInitialStickyBroadcast()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    const-string v2, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 353
    const-string v2, "QuickConnectSettings"

    const-string v3, "mReceiver"

    const-string v4, "HOME_RESUME"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$7;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/Util;->getTopActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "topActivityName":Ljava/lang/String;
    sget-object v2, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mActivityPackageList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    const-string v2, "QuickConnectSettings"

    const-string v3, "mReceiver"

    const-string v4, "HOME_RESUME - Quick Connect has top activity"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_2
    const-string v2, "QuickConnectSettings"

    const-string v3, "mReceiver"

    const-string v4, "HOME_RESUME - Quick Connect does not have top activity. Finish activity"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    iget-object v2, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$7;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->finish()V

    goto :goto_0
.end method
