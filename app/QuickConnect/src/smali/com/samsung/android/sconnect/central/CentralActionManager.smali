.class public Lcom/samsung/android/sconnect/central/CentralActionManager;
.super Ljava/lang/Object;
.source "CentralActionManager.java"


# static fields
.field static final TAG:Ljava/lang/String; = "CentralActionManager"


# instance fields
.field private CAMERA_CLASS:Ljava/lang/String;

.field private CAMERA_PACKAGE:Ljava/lang/String;

.field private mBluetoothOppActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

.field private mBtActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

.field private mContext:Landroid/content/Context;

.field private mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

.field private mIntent:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mIsP2pConnected:Z

.field private mPrinterActionHelper:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

.field private mProfileShareActionHelper:Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;

.field private mScreenMirrorActionHelper:Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;

.field private mSmartHome:Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;

.field private mTogether:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "com.sec.android.app.camera"

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->CAMERA_PACKAGE:Ljava/lang/String;

    .line 34
    const-string v0, "com.sec.android.app.camera.Camera"

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->CAMERA_CLASS:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mTogether:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mSmartHome:Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBtActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBluetoothOppActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    .line 43
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mScreenMirrorActionHelper:Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mProfileShareActionHelper:Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mPrinterActionHelper:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIsP2pConnected:Z

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    .line 52
    const-string v0, "CentralActionManager"

    const-string v1, "CentralActionManager"

    const-string v2, "EXEC"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    .line 55
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBluetoothOppActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    .line 56
    new-instance v0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .line 57
    new-instance v0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mTogether:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    .line 58
    new-instance v0, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mSmartHome:Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;

    .line 59
    new-instance v0, Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mScreenMirrorActionHelper:Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;

    .line 61
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBtActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    .line 62
    new-instance v0, Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mProfileShareActionHelper:Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;

    .line 64
    new-instance v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mPrinterActionHelper:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    .line 65
    return-void
.end method

.method private makeFileShareIntent(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "content"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 253
    if-nez p1, :cond_0

    .line 254
    const-string v2, "CentralActionManager"

    const-string v3, "sendfileShareList"

    const-string v4, "Invalid device."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_0
    return-object v1

    .line 257
    :cond_0
    if-nez p2, :cond_1

    .line 258
    const-string v2, "CentralActionManager"

    const-string v3, "sendfileShareList"

    const-string v4, "Invalid content."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 262
    :cond_1
    const-string v2, "CentralActionManager"

    const-string v3, "makeFileShareIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIsP2pConnected:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SCONNECT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 265
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.app.FileShareClient"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 267
    .local v0, "contentScheme":Ljava/lang/String;
    const-string v2, "http"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "https"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 269
    :cond_2
    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    :goto_1
    const-string v2, "DevMac"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    const-string v2, "DevName"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    const-string v2, "DevP2p"

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIsP2pConnected:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 271
    :cond_3
    const-string v2, "text"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 272
    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 275
    :cond_4
    const-string v2, "audio/mp3"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method private makeFileShareListIntent(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 5
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .local p2, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v0, 0x0

    .line 285
    if-nez p1, :cond_0

    .line 286
    const-string v1, "CentralActionManager"

    const-string v2, "makeFileShareListIntent"

    const-string v3, "Invalid device."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :goto_0
    return-object v0

    .line 289
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_2

    .line 290
    :cond_1
    const-string v1, "CentralActionManager"

    const-string v2, "makeFileShareListIntent"

    const-string v3, "Invalid content list."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 294
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 295
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-direct {p0, p1, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->makeFileShareIntent(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 298
    :cond_3
    const-string v1, "CentralActionManager"

    const-string v2, "makeFileShareListIntent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIsP2pConnected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SCONNECT_MULTIPLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 302
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.app.FileShareClient"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    const-string v1, "audio/mp3"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 305
    const-string v1, "DevMac"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    const-string v1, "DevName"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 307
    const-string v1, "DevP2p"

    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIsP2pConnected:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 308
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_0
.end method


# virtual methods
.method public cancelTransfer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mac"    # Ljava/lang/String;
    .param p2, "btMac"    # Ljava/lang/String;

    .prologue
    .line 176
    if-eqz p1, :cond_0

    .line 177
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->cancelTransfer(Ljava/lang/String;)V

    .line 179
    :cond_0
    if-eqz p2, :cond_1

    .line 180
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->sendStopIntent(Landroid/content/Context;Ljava/lang/String;)V

    .line 182
    :cond_1
    return-void
.end method

.method public connectToBtDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBtActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBtActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->bondAndConnectDevice(Ljava/lang/String;)V

    .line 322
    :cond_0
    return-void
.end method

.method public getBluetoothActionHelper()Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBtActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    return-object v0
.end method

.method public initiate(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V
    .locals 1
    .param p1, "fileShareActionListener"    # Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->setUpdateListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->initiate()V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mTogether:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mTogether:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->initiate()V

    .line 75
    :cond_1
    return-void
.end method

.method public isBonded(Ljava/lang/String;)Z
    .locals 1
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 351
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBtActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->isBonded(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public joinTogether(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3
    .param p1, "roomId"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 96
    const-string v0, "CentralActionManager"

    const-string v1, "joinTogether"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mTogether:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->joinTogether(Ljava/lang/String;Landroid/net/Uri;)V

    .line 98
    return-void
.end method

.method public launchSmartHome()V
    .locals 3

    .prologue
    .line 101
    const-string v0, "CentralActionManager"

    const-string v1, "launchSmartHome"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mSmartHome:Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;->startSmartHome()V

    .line 103
    return-void
.end method

.method public launchTogether(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "p2pMac"    # Ljava/lang/String;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "targetBtMac"    # Ljava/lang/String;

    .prologue
    .line 91
    const-string v0, "CentralActionManager"

    const-string v1, "launchTogether"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mTogether:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->startTogether(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public launchWearableManager(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "macAddr"    # Ljava/lang/String;
    .param p3, "deviceType"    # I
    .param p4, "packageName"    # Ljava/lang/String;

    .prologue
    .line 107
    const-string v0, "CentralActionManager"

    const-string v1, "launchWearableManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deviceType - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/action/WearableManagerActionHelper;->startWearableManager(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    .line 109
    return-void
.end method

.method public playChromecast(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p3, "mime"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 337
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/sconnect/central/action/ChromecastActionHelper;->requestPlay(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 338
    return-void
.end method

.method public playViaDlna(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 1
    .param p1, "dmrUuid"    # Ljava/lang/String;
    .param p3, "mime"    # Ljava/lang/String;
    .param p4, "passingIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 333
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/action/DlnaActionHelper;->requestPlay(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Landroid/content/Intent;)V

    .line 334
    return-void
.end method

.method public removeHoldingIntent(Ljava/lang/String;)Z
    .locals 5
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 229
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    :cond_0
    const-string v1, "CentralActionManager"

    const-string v2, "removeHoldingIntent"

    const-string v3, "all request done."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :cond_1
    :goto_0
    return v0

    .line 233
    :cond_2
    const-string v1, "CentralActionManager"

    const-string v2, "removeHoldingIntent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "holding request :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    const-string v0, "CentralActionManager"

    const-string v1, "removeHoldingIntent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cancel holding request to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeHoldingIntentAll()V
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    :cond_0
    const-string v0, "CentralActionManager"

    const-string v1, "removeHoldingIntentAll"

    const-string v2, "all request done."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :goto_0
    return-void

    .line 248
    :cond_1
    const-string v0, "CentralActionManager"

    const-string v1, "removeHoldingIntentAll"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "holding request :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method

.method public sendFileShare(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Landroid/net/Uri;Z)Z
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "content"    # Landroid/net/Uri;
    .param p3, "isReady"    # Z

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/CentralActionManager;->makeFileShareIntent(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 141
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 142
    const-string v1, "CentralActionManager"

    const-string v2, "sendfileShare"

    const-string v3, "makeFileShareIntent failed"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/4 v1, 0x0

    .line 152
    :goto_0
    return v1

    .line 145
    :cond_0
    if-eqz p3, :cond_1

    .line 146
    const-string v1, "CentralActionManager"

    const-string v2, "sendfileShare"

    const-string v3, "start"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->sendFile(Landroid/content/Intent;)V

    .line 152
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 149
    :cond_1
    const-string v1, "CentralActionManager"

    const-string v2, "sendfileShare"

    const-string v3, "put holding list"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Z)Z
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p3, "isReady"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 158
    .local p2, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/CentralActionManager;->makeFileShareListIntent(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 159
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 160
    const-string v1, "CentralActionManager"

    const-string v2, "sendfileShareList"

    const-string v3, "makeFileShareIntent failed"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/4 v1, 0x0

    .line 172
    :goto_0
    return v1

    .line 164
    :cond_0
    if-eqz p3, :cond_1

    .line 165
    const-string v1, "CentralActionManager"

    const-string v2, "sendfileShareList"

    const-string v3, "start"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->sendFile(Landroid/content/Intent;)V

    .line 172
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 168
    :cond_1
    const-string v1, "CentralActionManager"

    const-string v2, "setFileShare"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public sendFileViaBtOpp(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 2
    .param p1, "btMacAddr"    # Ljava/lang/String;
    .param p3, "mime"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 325
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 326
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->sendMultiFileSendIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 330
    :goto_0
    return-void

    .line 328
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v1, p1, v0, p3}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->sendFileSendIntent(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBtOppListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V
    .locals 1
    .param p1, "fileShareActionListener"    # Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBluetoothOppActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mBluetoothOppActionHelper:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->setBtOppListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 82
    :cond_0
    return-void
.end method

.method public setP2pConnected(Z)V
    .locals 4
    .param p1, "isConnect"    # Z

    .prologue
    .line 314
    const-string v0, "CentralActionManager"

    const-string v1, "setP2pConnected"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIsP2pConnected:Z

    .line 316
    return-void
.end method

.method public setPrinterContext(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 355
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mPrinterActionHelper:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mPrinterActionHelper:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->setPrinterContext(Landroid/content/Context;)V

    .line 358
    :cond_0
    return-void
.end method

.method public startHoldingIntent(Ljava/lang/String;Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V
    .locals 6
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "printPluginHelper"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 186
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 187
    :cond_0
    const-string v2, "CentralActionManager"

    const-string v3, "startHoldingIntent"

    const-string v4, "all request done."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :goto_0
    return-void

    .line 190
    :cond_1
    const-string v2, "CentralActionManager"

    const-string v3, "startHoldingIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "holding request :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 193
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_2

    .line 194
    const-string v2, "CentralActionManager"

    const-string v3, "startHoldingIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "no matching."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_2
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 197
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 223
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 198
    :cond_4
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.SCONNECT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 200
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    if-nez v2, :cond_5

    .line 201
    new-instance v2, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .line 202
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->setUpdateListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 204
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->sendFile(Landroid/content/Intent;)V

    goto :goto_1

    .line 205
    :cond_6
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.PRINT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 206
    const-string v2, "CentralActionManager"

    const-string v3, "startHoldingIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "target:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 209
    .local v1, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/util/Util;->isPdfContents(Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 210
    const-string v2, "CentralActionManager"

    const-string v3, "startHoldingIntent"

    const-string v4, "PdfContents"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p2, v1}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->startP2pDiscovery(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 212
    :cond_7
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/util/Util;->isImageContents(Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 213
    const-string v2, "CentralActionManager"

    const-string v3, "startHoldingIntent"

    const-string v4, "Image: print by plugin"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-virtual {p2, v1}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->startP2pDiscovery(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 216
    :cond_8
    const-string v2, "CentralActionManager"

    const-string v3, "startHoldingIntent"

    const-string v4, "NOT printable uris"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 218
    .end local v1    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_9
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.settings.WIFI_SHARE_PROFILE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 219
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mProfileShareActionHelper:Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;->sendProfile(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method public startPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 361
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mPrinterActionHelper:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->startPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V

    .line 362
    return-void
.end method

.method public startPrintDiscovery(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "p2pMac"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v1, "CentralActionManager"

    const-string v2, "startPrintDiscovery"

    invoke-static {v1, v2, p1}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PRINT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 114
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 116
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    return-void
.end method

.method public startProfileShare(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "ssid"    # Ljava/lang/String;
    .param p4, "isReady"    # Z

    .prologue
    const/4 v3, 0x1

    .line 121
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SHARE_PROFILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "deviceAddress"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const-string v1, "filePath"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    const-string v1, "senderName"

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/Util;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    const-string v1, "ssid"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    if-eqz p4, :cond_0

    .line 128
    const-string v1, "disconnectOnCompletion"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 129
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mProfileShareActionHelper:Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/central/action/ProfileShareActionHelper;->sendProfile(Landroid/content/Intent;)V

    .line 135
    :goto_0
    return v3

    .line 131
    :cond_0
    const-string v1, "disconnectOnCompletion"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 132
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mIntent:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public stopMirroring()V
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mScreenMirrorActionHelper:Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mScreenMirrorActionHelper:Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;->stopMirroring()V

    .line 344
    :cond_0
    return-void
.end method

.method public terminate()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralActionManager;->mFileShare:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->setUpdateListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 88
    :cond_0
    return-void
.end method
