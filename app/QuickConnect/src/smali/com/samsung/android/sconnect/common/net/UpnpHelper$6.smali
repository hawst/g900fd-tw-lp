.class Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;
.super Landroid/content/BroadcastReceiver;
.source "UpnpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/UpnpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 487
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 488
    .local v0, "action":Ljava/lang/String;
    const-string v3, "UpnpHelper"

    const-string v4, "mReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " action: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    const-string v3, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 490
    const-string v3, "networkInfo"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 493
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_2

    .line 494
    const-string v3, "UpnpHelper"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "netInfo.getDetailedState() "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    .line 498
    .local v2, "state":Z
    if-nez v2, :cond_3

    .line 499
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->RemoveFileServerDevice()V
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$900(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V

    .line 500
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUnbindFileService:Z
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$1000(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 501
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mUnbindFileService:Z
    invoke-static {v3, v4}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$1002(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Z)Z

    .line 502
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$100(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileDeviceFinder;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 503
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$100(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileDeviceFinder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v3, v4, v7}, Lcom/samsung/android/allshare/file/FileDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 505
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;
    invoke-static {v3, v7}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$102(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/file/FileDeviceFinder;)Lcom/samsung/android/allshare/file/FileDeviceFinder;

    .line 508
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$000(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 509
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$000(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 510
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v3, v7}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$002(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/file/FileServiceProvider;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 512
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$6;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->unregisterIntent()V
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V

    .line 519
    .end local v1    # "netInfo":Landroid/net/NetworkInfo;
    .end local v2    # "state":Z
    :cond_2
    :goto_0
    return-void

    .line 515
    .restart local v1    # "netInfo":Landroid/net/NetworkInfo;
    .restart local v2    # "state":Z
    :cond_3
    const-string v3, "UpnpHelper"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "netInfo.isConnected() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
