.class Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v4, -0x80000000

    const/16 v5, -0x12c

    .line 250
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    .line 252
    .local v9, "action":Ljava/lang/String;
    const-string v2, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$100(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 255
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Landroid/bluetooth/BluetoothDevice;

    .line 256
    .local v11, "device":Landroid/bluetooth/BluetoothDevice;
    if-nez v11, :cond_1

    .line 257
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "onReceive"

    const-string v4, "getParcelableExtra() is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    .end local v11    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    :goto_0
    return-void

    .line 260
    .restart local v11    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_1
    const-string v2, "android.bluetooth.device.extra.RSSI"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getShortExtra(Ljava/lang/String;S)S

    move-result v8

    .line 261
    .local v8, "RSSI":S
    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 262
    .local v1, "bdname":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 263
    const-string v2, "android.bluetooth.device.extra.NAME"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 265
    :cond_2
    if-nez v1, :cond_3

    .line 266
    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 268
    :cond_3
    const-string v2, "android.bluetooth.device.extra.CLASS"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/bluetooth/BluetoothClass;

    .line 269
    .local v10, "cod":Landroid/bluetooth/BluetoothClass;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_FOUND : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", type: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", bond state: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "["

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "],  Mac - "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", cod: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_4

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_7

    invoke-virtual {v10}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v2

    const/16 v3, 0x700

    if-eq v2, v3, :cond_4

    invoke-virtual {v10}, Landroid/bluetooth/BluetoothClass;->getPeripheralMinorSubClass()I

    move-result v2

    const/16 v3, 0x404

    if-ne v2, v3, :cond_7

    .line 305
    :cond_4
    new-instance v12, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    invoke-direct {v12, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 307
    .local v12, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    invoke-virtual {v2, v11}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->isGearDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 308
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearDeviceID(Landroid/bluetooth/BluetoothDevice;)I
    invoke-static {v4, v11}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$200(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;Landroid/bluetooth/BluetoothDevice;)I

    move-result v4

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearInfo(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    invoke-static {v6, v11}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$300(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearData(Landroid/bluetooth/BluetoothDevice;)[B
    invoke-static {v7, v11}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$400(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;Landroid/bluetooth/BluetoothDevice;)[B

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ISLjava/lang/String;[B)V

    .line 311
    .local v0, "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
    invoke-virtual {v12, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;)V

    .line 318
    .end local v0    # "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$500(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 319
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$500(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$100(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 322
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$100(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    invoke-interface {v2, v12}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_0

    .line 313
    :cond_6
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v3

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;-><init>(Ljava/lang/String;Ljava/lang/String;IS)V

    .line 315
    .local v0, "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceBt;
    invoke-virtual {v12, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceBt;)V

    goto :goto_1

    .line 325
    .end local v0    # "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceBt;
    .end local v12    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "onReceive"

    const-string v4, "Don\'t add. not DEVICE_TYPE_CLASSIC"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 327
    .end local v1    # "bdname":Ljava/lang/String;
    .end local v8    # "RSSI":S
    .end local v10    # "cod":Landroid/bluetooth/BluetoothClass;
    .end local v11    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_8
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$600(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 329
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_9

    .line 330
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "onReceive"

    const-string v4, "BT Turning on"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$600(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_BT:B

    invoke-interface {v2, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onEnabled(B)V

    goto/16 :goto_0

    .line 332
    :cond_9
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_0

    .line 333
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "onReceive"

    const-string v4, "BT Turning off"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$600(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_BT:B

    invoke-interface {v2, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisabled(B)V

    goto/16 :goto_0
.end method
