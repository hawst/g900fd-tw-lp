.class Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;
.super Ljava/lang/Object;
.source "SelectMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 355
    const-string v0, ""

    .line 357
    .local v0, "address":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mPickImage:Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 358
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 359
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateMapFromLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    .line 364
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 365
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getURL(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v0

    .line 374
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 375
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$600(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mMenuDone"

    const-string v3, "address isEmpty"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09011f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 382
    :goto_0
    return-void

    .line 380
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$6;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->doFinish(Ljava/lang/String;)V

    goto :goto_0
.end method
