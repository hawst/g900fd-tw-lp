.class Lcom/samsung/android/sconnect/common/net/WifiHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "WifiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/WifiHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/WifiHelper;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 318
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v7, "mReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onReceive - action: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v6, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 321
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->getScanResult()V

    .line 324
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 325
    const-string v6, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 326
    const-string v6, "wifi_state"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 328
    .local v1, "apState":I
    const/4 v6, 0x3

    if-eq v1, v6, :cond_1

    const/16 v6, 0xd

    if-ne v1, v6, :cond_3

    .line 329
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    invoke-interface {v6, v7}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onEnabled(B)V

    .line 398
    .end local v1    # "apState":I
    :cond_2
    :goto_0
    return-void

    .line 330
    .restart local v1    # "apState":I
    :cond_3
    if-eq v1, v10, :cond_4

    const/16 v6, 0xb

    if-ne v1, v6, :cond_5

    .line 331
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    invoke-interface {v6, v7}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisabled(B)V

    goto :goto_0

    .line 333
    :cond_5
    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v7, "onReceive"

    const-string v8, "WIFI_AP_STATE_CHANGED, unknown wifi_state"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 335
    .end local v1    # "apState":I
    :cond_6
    const-string v6, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 336
    const-string v6, "networkType"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 338
    .local v4, "networkType":I
    if-ne v4, v10, :cond_8

    .line 339
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$200(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Landroid/content/Context;

    move-result-object v6

    const-string v7, "connectivity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 341
    .local v2, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v2, v10}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 344
    .local v3, "netInfoWifi":Landroid/net/NetworkInfo;
    if-nez v3, :cond_7

    .line 345
    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v7, "onReceive"

    const-string v8, "no netInfoWifi...remove all wifi device."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 350
    :cond_7
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    .line 351
    .local v5, "state":Landroid/net/NetworkInfo$DetailedState;
    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v7, "onReceive"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CONNECTIVITY_ACTION : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    sget-object v6, Lcom/samsung/android/sconnect/common/net/WifiHelper$3;->$SwitchMap$android$net$NetworkInfo$DetailedState:[I

    invoke-virtual {v5}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 354
    :pswitch_0
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    invoke-interface {v6, v7, v11}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onConnected(BLjava/lang/String;)V

    goto/16 :goto_0

    .line 358
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    invoke-interface {v6, v7, v11}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisconnected(BLjava/lang/String;)V

    goto/16 :goto_0

    .line 362
    :pswitch_2
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    invoke-interface {v6, v7, v11}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisconnected(BLjava/lang/String;)V

    goto/16 :goto_0

    .line 366
    :pswitch_3
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    invoke-interface {v6, v7}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onPoor(B)V

    goto/16 :goto_0

    .line 373
    .end local v2    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v3    # "netInfoWifi":Landroid/net/NetworkInfo;
    .end local v5    # "state":Landroid/net/NetworkInfo$DetailedState;
    :cond_8
    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v7, "onReceive"

    const-string v8, "CONNECTIVITY_ACTION no intereset in this netType"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 375
    .end local v4    # "networkType":I
    :cond_9
    const-string v6, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 376
    const-string v6, "wifi_state"

    const/4 v7, 0x4

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 379
    .local v5, "state":I
    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v7, "onReceive"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "WIFI_STATE_CHANGED_ACTION : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    packed-switch v5, :pswitch_data_1

    :pswitch_4
    goto/16 :goto_0

    .line 389
    :pswitch_5
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    invoke-interface {v6, v7}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisabled(B)V

    goto/16 :goto_0

    .line 382
    :pswitch_6
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    invoke-interface {v6, v7}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onEnabled(B)V

    goto/16 :goto_0

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 380
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method
