.class Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout$1;
.super Ljava/lang/Object;
.source "ResolverDrawerLayout.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout$1;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouchModeChanged(Z)V
    .locals 3
    .param p1, "isInTouchMode"    # Z

    .prologue
    .line 88
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout$1;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout$1;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout$1;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;->isDescendantClipped(Landroid/view/View;)Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;->access$000(Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout$1;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;

    const/4 v1, 0x0

    const/4 v2, 0x0

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;->smoothScrollTo(IF)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;->access$100(Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;IF)V

    .line 91
    :cond_0
    return-void
.end method
