.class public Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;
.super Ljava/lang/Object;
.source "DeviceChromecast.java"


# instance fields
.field public mDeviceName:Ljava/lang/String;

.field public mID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mDeviceName:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mID:Ljava/lang/String;

    .line 10
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mDeviceName:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mID:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 16
    if-nez p1, :cond_1

    .line 27
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 19
    check-cast v0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;

    .line 21
    .local v0, "dev":Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mDeviceName:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 22
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mID:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 23
    const/4 v1, 0x1

    goto :goto_0
.end method
