.class public Lcom/samsung/android/sconnect/SconnectManager;
.super Ljava/lang/Object;
.source "SconnectManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/SconnectManager$ScanThread;,
        Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    }
.end annotation


# static fields
.field public static final BT_SHOULD_BE_KEEP:I = 0x1

.field public static final BT_SHOULD_BE_OFF:I = 0x0

.field public static final BT_SHOULD_BE_PEND:I = 0x2

.field private static final EVENT_DEVICE_ADDED:I = 0x1

.field private static final EVENT_DEVICE_CLEAR:I = 0x4

.field private static final EVENT_DEVICE_REMOVED:I = 0x2

.field private static final EVENT_DEVICE_UPDATED:I = 0x3

.field private static final MSG_P2P_TIMEOUT:I = 0x16

.field private static final MSG_REQUEST_CLEAR:I = 0x18

.field private static final MSG_REQUEST_START:I = 0x15

.field private static final MSG_SCAN_FINISH:I = 0x13

.field private static final MSG_START_SCAN_BLE_DEVICE:I = 0x5

.field private static final MSG_START_SCAN_BLE_P2P_DEVICE:I = 0x12

.field private static final MSG_START_SCAN_BT_DEVICE:I = 0x7

.field private static final MSG_START_SCAN_CHROMECAST_DEVICE:I = 0x11

.field private static final MSG_START_SCAN_P2P_DEVICE:I = 0x9

.field private static final MSG_START_SCAN_PRINTER_DEVICE:I = 0xd

.field private static final MSG_START_SCAN_SLINK_DEVICE:I = 0xf

.field private static final MSG_START_SCAN_UPNP_DEVICE:I = 0x3

.field private static final MSG_START_SCAN_WIFI_DEVICE:I = 0xb

.field private static final MSG_WAITING_P2P_CONNECT:I = 0x17

.field static final TAG:Ljava/lang/String; = "SconnectManager"

.field private static volatile mInstance:Lcom/samsung/android/sconnect/SconnectManager;

.field private static mPreference:Landroid/content/SharedPreferences;

.field private static mRequestId:B


# instance fields
.field private MAX_DIRECT_CONNECTION:I

.field private ScanHandler:Landroid/os/Handler;

.field private mBLEHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

.field private mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

.field mBluetoothRemain:I

.field private mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

.field private mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mCentralRunning:Z

.field private mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

.field public mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

.field private mContext:Landroid/content/Context;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end field

.field public mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mFavoriteDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

.field public mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

.field private mIsAcceptDialogShowing:Z

.field private mIsDiscovering:Z

.field private mIsStopMirroring:Z

.field private mIsWfdPaused:Z

.field private mLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private mLoaderManager:Landroid/app/LoaderManager;

.field private mLocalReceiver:Landroid/content/BroadcastReceiver;

.field private mMyIpAddr:Ljava/lang/String;

.field private mNetworkState:B

.field private mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

.field private mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

.field private mPackageReceiver:Landroid/content/BroadcastReceiver;

.field private mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

.field private mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

.field private mRequest:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;",
            ">;"
        }
    .end annotation
.end field

.field private mScanDeviceType:I

.field private mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

.field private mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

.field private mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

.field private mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

.field mWifiTurnOn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    sput-object v1, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    .line 136
    const/4 v0, 0x0

    sput-byte v0, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    .line 153
    sput-object v1, Lcom/samsung/android/sconnect/SconnectManager;->mPreference:Landroid/content/SharedPreferences;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mLoaderManager:Landroid/app/LoaderManager;

    .line 95
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    .line 97
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 98
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 100
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .line 101
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBLEHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    .line 102
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    .line 103
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .line 104
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    .line 105
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .line 106
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .line 107
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    .line 108
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    .line 110
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    .line 111
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    .line 113
    iput v8, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 115
    iput-boolean v8, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralRunning:Z

    .line 133
    iput-byte v8, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B

    .line 135
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    .line 137
    iput-boolean v8, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsAcceptDialogShowing:Z

    .line 142
    const/4 v4, 0x1

    iput v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBluetoothRemain:I

    .line 144
    iput-boolean v8, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 146
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    .line 148
    sget v4, Landroid/net/wifi/p2p/WifiP2pManager;->MAX_CLIENT_SUPPORT:I

    iput v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->MAX_DIRECT_CONNECTION:I

    .line 150
    iput-boolean v8, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsDiscovering:Z

    .line 151
    iput-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mMyIpAddr:Ljava/lang/String;

    .line 155
    iput-boolean v8, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsWfdPaused:Z

    .line 156
    iput-boolean v8, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsStopMirroring:Z

    .line 595
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$1;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    .line 695
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$2;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    .line 802
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$3;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$3;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    .line 847
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$4;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$4;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 1045
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$5;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$5;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .line 1881
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$6;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$6;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

    .line 1896
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$7;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$7;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    .line 2109
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$8;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$8;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;

    .line 182
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 183
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/16 v5, 0x2000

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 185
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iget-object v5, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->setTagVer(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 192
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "packageName":Ljava/lang/String;
    :goto_0
    const-string v4, "SconnectManager"

    const-string v5, "SconnectManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initiate from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    .line 195
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isLightTheme()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 196
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v5, 0x103012b

    invoke-virtual {v4, v5}, Landroid/content/Context;->setTheme(I)V

    .line 199
    :cond_0
    const-string v4, "SconnectManager"

    const-string v5, "SconnectManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "set getApplicationContext "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-direct {p0}, Lcom/samsung/android/sconnect/SconnectManager;->registerBroadcast()V

    .line 202
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const-string v5, "SconnectDisplay"

    invoke-virtual {v4, v5, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    sput-object v4, Lcom/samsung/android/sconnect/SconnectManager;->mPreference:Landroid/content/SharedPreferences;

    .line 203
    sget-object v4, Lcom/samsung/android/sconnect/SconnectManager;->mPreference:Landroid/content/SharedPreferences;

    const-string v5, "REQUEST_ID"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    int-to-byte v4, v4

    sput-byte v4, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    .line 204
    sget-byte v4, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    if-nez v4, :cond_1

    .line 205
    new-instance v3, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Random;-><init>(J)V

    .line 206
    .local v3, "random":Ljava/util/Random;
    const/16 v4, 0x78

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    sput-byte v4, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    .line 208
    .end local v3    # "random":Ljava/util/Random;
    :cond_1
    const-string v4, "SconnectManager"

    const-string v5, "SconnectManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "REQUEST_ID :  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-byte v7, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    new-instance v4, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;-><init>(Lcom/samsung/android/sconnect/SconnectManager;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    .line 211
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->start()V

    .line 213
    new-instance v4, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    .line 214
    new-instance v4, Lcom/samsung/android/sconnect/common/net/BleHelper;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/common/net/BleHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBLEHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    .line 215
    new-instance v4, Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    .line 216
    new-instance v4, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .line 217
    new-instance v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sconnect/common/net/P2pHelper;-><init>(Landroid/content/Context;Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .line 218
    new-instance v4, Lcom/samsung/android/sconnect/common/net/WifiHelper;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/common/net/WifiHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    .line 219
    new-instance v4, Lcom/samsung/android/sconnect/common/util/WfdUtil;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/common/util/WfdUtil;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    .line 220
    new-instance v4, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    .line 221
    new-instance v4, Lcom/samsung/android/sconnect/central/CentralActionManager;

    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/sconnect/central/CentralActionManager;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    .line 222
    return-void

    .line 186
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "SconnectManager"

    const-string v5, "SconnectManager"

    const-string v6, "NameNotFoundException"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 188
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SconnectManager"

    const-string v5, "SconnectManager"

    const-string v6, "Exception"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/SconnectManager$ScanThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/SconnectManager;)B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-byte v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B

    return v0
.end method

.method static synthetic access$1272(Lcom/samsung/android/sconnect/SconnectManager;I)B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;
    .param p1, "x1"    # I

    .prologue
    .line 85
    iget-byte v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B

    and-int/2addr v0, p1

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B

    return v0
.end method

.method static synthetic access$1276(Lcom/samsung/android/sconnect/SconnectManager;I)B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;
    .param p1, "x1"    # I

    .prologue
    .line 85
    iget-byte v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B

    or-int/2addr v0, p1

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/sconnect/SconnectManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mMyIpAddr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/sconnect/SconnectManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mMyIpAddr:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/samsung/android/sconnect/SconnectManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsDiscovering:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/WifiHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/ChromecastHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/sconnect/SconnectManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsAcceptDialogShowing:Z

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/util/WfdUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/SconnectManager;Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/SconnectManager;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "x2"    # I

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/SconnectManager;->updateScanList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I

    move-result v0

    return v0
.end method

.method private connectP2pRequest(Ljava/lang/String;)V
    .locals 5
    .param p1, "targetAddress"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x16

    .line 1871
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->connectPeer(Ljava/lang/String;)V

    .line 1872
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1873
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getRequestTargetList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1874
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 1875
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const v2, 0x9c40

    mul-int/2addr v2, v0

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1879
    :goto_0
    return-void

    .line 1877
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x9c40

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/android/sconnect/SconnectManager;
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    if-eqz p0, :cond_1

    sget-object v0, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    if-nez v0, :cond_1

    .line 160
    const-class v1, Lcom/samsung/android/sconnect/SconnectManager;

    monitor-enter v1

    .line 161
    :try_start_0
    sget-object v0, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    if-nez v0, :cond_0

    .line 162
    new-instance v0, Lcom/samsung/android/sconnect/SconnectManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/SconnectManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    .line 164
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/DLog;->initilize()V

    .line 165
    const-string v0, "SconnectManager"

    const-string v2, "getInstance"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "make new instance "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    :cond_1
    const-string v0, "SconnectManager"

    const-string v1, "getInstance"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return existing instance "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    sget-object v0, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    return-object v0

    .line 167
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getRequestId()Ljava/lang/Byte;
    .locals 3

    .prologue
    .line 423
    sget-byte v1, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    sput-byte v1, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    .line 425
    sget-byte v1, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    const/16 v2, 0x7d

    if-lt v1, v2, :cond_0

    .line 427
    const/4 v1, 0x1

    sput-byte v1, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    .line 431
    :cond_0
    sget-object v1, Lcom/samsung/android/sconnect/SconnectManager;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 432
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "REQUEST_ID"

    sget-byte v2, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 433
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 435
    sget-byte v1, Lcom/samsung/android/sconnect/SconnectManager;->mRequestId:B

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    return-object v1
.end method

.method private launchP2pPrinterWaiting(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1828
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v0, "SconnectManager"

    const-string v1, "launchP2pPrinterWaiting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "to : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1829
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/sconnect/central/CentralActionManager;->startPrintDiscovery(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1831
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/SconnectManager;->connectP2pRequest(Ljava/lang/String;)V

    .line 1832
    return-void
.end method

.method private launchWearableManager(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 11
    .param p1, "macAddr"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "data"    # [B

    .prologue
    const/16 v10, 0xc8

    .line 1835
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/sconnect/SconnectManager;->setHowToControlBluetooth(I)V

    .line 1836
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothActionHelper()Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    move-result-object v2

    .line 1839
    .local v2, "mbtHelper":Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.samsung.android.app.watchmanagerstub"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 1841
    .local v3, "pinfo":Landroid/content/pm/PackageInfo;
    iget v4, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 1843
    .local v4, "versionCode":I
    const-string v6, "SconnectManager"

    const-string v7, "launchWearableManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "versionCode : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", packageName : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Data : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p3}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1846
    const/16 v6, 0x65

    if-lt v4, v6, :cond_0

    if-lt v4, v10, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothHelper()Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->isGear1(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1848
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v6, "com.samsung.android.sconnect.action.CONNECT_WEARABLE"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1849
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "MAC"

    invoke-virtual {v1, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1850
    const-string v6, "WM_MANAGER"

    invoke-virtual {v1, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1851
    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1868
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "pinfo":Landroid/content/pm/PackageInfo;
    .end local v4    # "versionCode":I
    :cond_2
    :goto_0
    return-void

    .line 1852
    .restart local v3    # "pinfo":Landroid/content/pm/PackageInfo;
    .restart local v4    # "versionCode":I
    :cond_3
    if-lt v4, v10, :cond_2

    .line 1853
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.samsung.android.wmanger.action.CONNECT_WEARABLE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1855
    .local v5, "wmanagerIntent":Landroid/content/Intent;
    const-string v6, "MAC"

    invoke-virtual {v5, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1856
    const-string v6, "DATA"

    invoke-virtual {v5, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1857
    invoke-static {p3}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    .line 1858
    const-string v6, "WM_MANAGER"

    invoke-virtual {v5, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1859
    const-string v6, "SconnectManager"

    const-string v7, "launchWearableManager"

    const-string v8, "Gear is scanned by BT"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1861
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1863
    .end local v3    # "pinfo":Landroid/content/pm/PackageInfo;
    .end local v4    # "versionCode":I
    .end local v5    # "wmanagerIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 1864
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "SconnectManager"

    const-string v7, "launchWearableManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "WearableInstaller is not preloaded! - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  Try to connect to device"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1866
    invoke-virtual {v2, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->bondAndConnectDevice(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private registerBroadcast()V
    .locals 5

    .prologue
    .line 672
    const-string v2, "SconnectManager"

    const-string v3, "registerBroadcast"

    const-string v4, "--"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 675
    .local v1, "localIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "com.samsung.android.sconnect.periph.ACCEPT_RESULT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 676
    const-string v2, "com.samsung.android.sconnect.central.DISCONNECT_P2P"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 677
    const-string v2, "com.samsung.android.sconnect.central.WAITING_CANCEL"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 678
    const-string v2, "com.samsung.android.sconnect.central.PRINTER_WAITING_CANCEL"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 679
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/SconnectManager;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 682
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 683
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 684
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 685
    const-string v2, "package"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 686
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 687
    return-void
.end method

.method private setServiceCapability()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1015
    sget-object v4, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->DOWNLOADABLE_SVC:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1016
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1017
    .local v3, "packageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v4, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1018
    const-string v4, "com.sec.android.sidesync30"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1019
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v2

    .line 1020
    .local v2, "isSyncMode":Z
    if-eqz v2, :cond_0

    .line 1021
    const-string v4, "SconnectManager"

    const-string v5, "setServiceCapability"

    const-string v6, "SIDESYNC tablet not discoverable"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1023
    :cond_0
    const-string v4, "SconnectManager"

    const-string v5, "setServiceCapability"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " enabled"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4, v8}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setServiceType(IZ)V

    goto :goto_0

    .line 1027
    .end local v2    # "isSyncMode":Z
    :cond_1
    const-string v4, "SconnectManager"

    const-string v5, "setServiceCapability"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " enabled"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4, v8}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setServiceType(IZ)V

    goto/16 :goto_0

    .line 1031
    :cond_2
    const-string v4, "SconnectManager"

    const-string v5, "setServiceCapability"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " disabled"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setServiceType(IZ)V

    goto/16 :goto_0

    .line 1037
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    sget v5, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_FILESHARE:I

    invoke-virtual {v4, v5, v8}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setServiceType(IZ)V

    .line 1039
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isProfileShareAvailable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1040
    const-string v4, "SconnectManager"

    const-string v5, "setServiceCapability"

    const-string v6, "Support Wifi Profile Share"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    sget v5, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_PROFILE_SHARE:I

    invoke-virtual {v4, v5, v8}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setServiceType(IZ)V

    .line 1043
    :cond_4
    return-void
.end method

.method private startDiscovery(I)V
    .locals 8
    .param p1, "type"    # I

    .prologue
    const/16 v7, 0x13

    const/4 v6, 0x0

    .line 511
    const-wide/16 v2, 0x0

    .line 512
    .local v2, "discoveryTime":J
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->cleanMessages()V

    .line 513
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 514
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v1}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDiscoveryStarted()V

    .line 518
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->getScanResult()V

    .line 520
    and-int/lit8 v1, p1, 0x10

    if-gtz v1, :cond_1

    and-int/lit8 v1, p1, 0x20

    if-gtz v1, :cond_1

    and-int/lit16 v1, p1, 0x80

    if-lez v1, :cond_5

    .line 522
    :cond_1
    and-int/lit8 v1, p1, 0x10

    if-lez v1, :cond_2

    .line 523
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 524
    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 526
    :cond_2
    and-int/lit8 v1, p1, 0x20

    if-lez v1, :cond_3

    .line 527
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v4, 0xd

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 528
    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 530
    :cond_3
    and-int/lit16 v1, p1, 0x80

    if-lez v1, :cond_4

    .line 531
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v4, 0x11

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 532
    const-string v1, "SconnectManager"

    const-string v4, "start Discovery"

    const-string v5, "send msg _ MSG_START_SCAN_CHROMECAST_DEVICE"

    invoke-static {v1, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 535
    :cond_4
    const-wide/16 v4, 0x12c

    add-long/2addr v2, v4

    .line 538
    :cond_5
    and-int/lit8 v1, p1, 0x40

    if-lez v1, :cond_a

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isSlinkSupport(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 539
    const-string v1, "SconnectManager"

    const-string v4, "startDiscovery"

    const-string v5, "DISCOVERY_TYPE_SLINK"

    invoke-static {v1, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v4, 0xf

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 541
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 542
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mLoaderManager:Landroid/app/LoaderManager;

    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mLoaderCallbacks:Landroid/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v6, v0, v4}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 543
    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 549
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsStopMirroring:Z

    if-eqz v1, :cond_6

    .line 550
    const-wide/16 v4, 0x9c4

    add-long/2addr v2, v4

    .line 551
    iput-boolean v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsStopMirroring:Z

    .line 554
    :cond_6
    and-int/lit8 v1, p1, 0xa

    const/16 v4, 0xa

    if-ne v1, v4, :cond_7

    .line 555
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v4, 0x12

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 556
    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    and-int/lit8 v1, v1, -0xb

    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 557
    const-wide/16 v4, 0x1bbc

    add-long/2addr v2, v4

    .line 560
    :cond_7
    and-int/lit8 v1, p1, 0x1

    if-lez v1, :cond_8

    .line 561
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v4, 0xb

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 562
    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 563
    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    .line 566
    :cond_8
    and-int/lit8 v1, p1, 0x4

    if-lez v1, :cond_9

    .line 567
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/4 v4, 0x7

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 568
    const-wide/16 v4, 0x32c8

    add-long/2addr v2, v4

    .line 569
    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 572
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 573
    return-void

    .line 545
    :cond_a
    const-string v1, "SconnectManager"

    const-string v4, "startDiscovery"

    const-string v5, "DO NOT DISCOVERY_TYPE_SLINK since isSlinkSupport false"

    invoke-static {v1, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    goto :goto_0
.end method

.method private stopReservedDiscovery()V
    .locals 4

    .prologue
    const/16 v3, 0x13

    .line 576
    const-string v0, "SconnectManager"

    const-string v1, "stopReservedDiscovery"

    const-string v2, " "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 578
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->cleanMessages()V

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 582
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 583
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 584
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 585
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 586
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 587
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 588
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 589
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 590
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 591
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 593
    :cond_1
    return-void
.end method

.method private unregisterBroadcast()V
    .locals 3

    .prologue
    .line 690
    const-string v0, "SconnectManager"

    const-string v1, "unregisterBroadcast"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 692
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 693
    return-void
.end method

.method private declared-synchronized updateScanList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I
    .locals 9
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "event"    # I

    .prologue
    .line 911
    monitor-enter p0

    const/4 v5, 0x4

    if-ne p2, v5, :cond_1

    .line 912
    :try_start_0
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList"

    const-string v7, "EVENT_DEVICE_CLEAR"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 1006
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    monitor-exit p0

    return v5

    .line 914
    :cond_1
    if-eqz p1, :cond_d

    .line 915
    :try_start_1
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "event("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 970
    :pswitch_0
    const/4 v0, 0x0

    .line 971
    .local v0, "findSameDevice":Z
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 972
    .local v3, "item":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 973
    const/4 v0, 0x1

    .line 974
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList - EVENT_DEVICE_ADDED don\'t add :"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    .end local v3    # "item":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_3
    :goto_1
    if-nez v0, :cond_0

    .line 993
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 994
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v5, :cond_4

    .line 995
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v5, p1}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 997
    :cond_4
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList - EVENT_DEVICE_ADDED add :"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 911
    .end local v0    # "findSameDevice":Z
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 919
    :pswitch_1
    :try_start_2
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 920
    .restart local v3    # "item":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 921
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 922
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList - EVENT_DEVICE_REMOVED remove:"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v5, :cond_0

    .line 925
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v5, p1}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_0

    .line 928
    :cond_6
    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->sameNameCheck(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 929
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 930
    .local v2, "index":I
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->removeDeviceInfo(I)I

    move-result v4

    .line 931
    .local v4, "result":I
    const/4 v5, 0x1

    if-ne v4, v5, :cond_8

    .line 932
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 933
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList - EVENT_DEVICE_REMOVED remove:"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v5, :cond_0

    .line 936
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v5, p1}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_0

    .line 938
    :cond_8
    if-nez v4, :cond_9

    .line 939
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 940
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList - EVENT_DEVICE_REMOVED updated:"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v5, :cond_0

    .line 943
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v5, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_0

    .line 946
    :cond_9
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList - EVENT_DEVICE_REMOVED removeDeviceInfo result -1:"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 955
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "index":I
    .end local v3    # "item":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v4    # "result":I
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 956
    .restart local v3    # "item":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 957
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 958
    .restart local v2    # "index":I
    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->updateDeviceInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 959
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 960
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList - EVENT_DEVICE_UPDATED updated:"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v5, :cond_0

    .line 963
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v5, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_0

    .line 977
    .end local v2    # "index":I
    .restart local v0    # "findSameDevice":Z
    :cond_b
    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v5

    if-nez v5, :cond_c

    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->sameNameCheck(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 978
    :cond_c
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 979
    .restart local v2    # "index":I
    invoke-virtual {v3, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->updateDeviceInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 980
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 981
    const/4 v0, 0x1

    .line 982
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList - EVENT_DEVICE_ADDED update :"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v5, :cond_3

    .line 986
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v5, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_1

    .line 1004
    .end local v0    # "findSameDevice":Z
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "index":I
    .end local v3    # "item":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_d
    const-string v5, "SconnectManager"

    const-string v6, "updateScanList"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "device is null! event :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 917
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public cancelTransfer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mac"    # Ljava/lang/String;
    .param p2, "btMac"    # Ljava/lang/String;

    .prologue
    .line 1796
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sconnect/central/CentralActionManager;->cancelTransfer(Ljava/lang/String;Ljava/lang/String;)V

    .line 1797
    return-void
.end method

.method public checkConnectedDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 2048
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2049
    const/4 v0, 0x1

    .line 2051
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkEnabledBt()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 307
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->checkEnabledNetwork()Z

    move-result v2

    if-nez v2, :cond_0

    .line 308
    iput v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBluetoothRemain:I

    .line 312
    :goto_0
    return v0

    .line 311
    :cond_0
    iput v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBluetoothRemain:I

    move v0, v1

    .line 312
    goto :goto_0
.end method

.method public checkEnabledWifi()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->checkEnabledNetwork()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 303
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    return v0

    .line 302
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkMaxDirectDevice(Ljava/lang/String;)Z
    .locals 2
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 2040
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getClientCount()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->MAX_DIRECT_CONNECTION:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2042
    const/4 v0, 0x1

    .line 2044
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized clearDeviceList()V
    .locals 1

    .prologue
    .line 900
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 901
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 903
    :cond_0
    monitor-exit p0

    return-void

    .line 900
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createPreDiscoveryHelper()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    if-nez v0, :cond_0

    .line 226
    new-instance v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .line 227
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCommandListener:Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setCommandListener(Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;)V

    .line 228
    invoke-direct {p0}, Lcom/samsung/android/sconnect/SconnectManager;->setServiceCapability()V

    .line 230
    :cond_0
    return-void
.end method

.method public doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z
    .locals 40
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "contentsType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "I)Z"
        }
    .end annotation

    .prologue
    .line 1167
    .local p3, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    packed-switch p2, :pswitch_data_0

    .line 1604
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0900fd

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1608
    :cond_0
    :goto_0
    const/4 v4, 0x1

    :goto_1
    return v4

    .line 1169
    :pswitch_1
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ACTION_SEND_CURRENT_CONTENTS - send contents :"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    const/4 v8, 0x0

    .line 1174
    .local v8, "count":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const-string v7, "SEND"

    const/4 v9, 0x1

    move/from16 v0, p5

    invoke-static {v4, v7, v0, v9}, Lcom/samsung/android/sconnect/common/util/Util;->loggingSurvey(Landroid/content/Context;Ljava/lang/String;II)V

    .line 1175
    if-eqz p3, :cond_16

    .line 1176
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1177
    const/16 v4, 0xff

    if-le v8, v4, :cond_1

    .line 1178
    const/16 v8, 0xff

    .line 1180
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v4

    const/4 v7, 0x4

    if-eq v4, v7, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v4

    const/4 v7, 0x5

    if-ne v4, v7, :cond_7

    .line 1182
    :cond_2
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_SEND_CURRENT_CONTENTS via BT OPP (PC)"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1183
    if-eqz p4, :cond_3

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1184
    :cond_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-static {v7, v4, v9}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaMime(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 1186
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtMac:Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v4, v7, v0, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileViaBtOpp(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1190
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v4

    if-nez v4, :cond_6

    .line 1191
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/SconnectManager;->setHowToControlBluetooth(I)V

    .line 1193
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0900d8

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1196
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x8

    if-lez v4, :cond_b

    .line 1197
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->needToDisconnect(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1198
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "removeGroup"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeGroup(Z)V

    .line 1210
    :cond_8
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1211
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getRequestId()Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v5

    .line 1212
    .local v5, "id":Ljava/lang/String;
    new-instance v21, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)V

    .line 1213
    .local v21, "cmdFileshare":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1215
    const-string v4, "SconnectManager"

    const-string v7, "sendFileShareCommand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "@@@ "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v11

    iget-object v11, v11, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1218
    .local v6, "target":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1219
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setIgnoreGoDevice(Z)V

    .line 1220
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v9, 0x1

    move/from16 v7, p5

    invoke-virtual/range {v4 .. v9}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendFileShareCommand(Ljava/lang/String;Ljava/util/ArrayList;IIZ)V

    .line 1222
    const/16 v4, 0x8

    move/from16 v0, p5

    if-ne v0, v4, :cond_9

    .line 1223
    invoke-static/range {p3 .. p3}, Lcom/samsung/android/sconnect/common/util/Util;->countContact(Ljava/util/ArrayList;)I

    move-result v8

    :cond_9
    move-object/from16 v9, p0

    move-object v10, v5

    move-object/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p5

    move v14, v8

    .line 1225
    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sconnect/SconnectManager;->requestWaitingPopup(Ljava/lang/String;Lcom/samsung/android/sconnect/common/device/SconnectDevice;III)V

    goto/16 :goto_0

    .line 1200
    .end local v5    # "id":Ljava/lang/String;
    .end local v6    # "target":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v21    # "cmdFileshare":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/SconnectManager;->checkMaxDirectDevice(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1201
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "MAX_DIRECT_CONNECTION : "

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1202
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v9

    const v11, 0x7f0900f2

    invoke-virtual {v9, v11}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v9

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sconnect/SconnectManager;->MAX_DIRECT_CONNECTION:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v11, v13

    invoke-virtual {v7, v9, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1208
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1226
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x2

    if-lez v4, :cond_e

    .line 1227
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->needToDisconnect(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1228
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "removeGroup"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1229
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeGroup(Z)V

    .line 1240
    :cond_c
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1242
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v7

    const v9, 0x7f0900d7

    invoke-virtual {v7, v9}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v7

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1250
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/SconnectManager;->sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 1230
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/SconnectManager;->checkMaxDirectDevice(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1231
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "MAX_DIRECT_CONNECTION : "

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v9

    const v11, 0x7f0900f2

    invoke-virtual {v9, v11}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v9

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sconnect/SconnectManager;->MAX_DIRECT_CONNECTION:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v11, v13

    invoke-virtual {v7, v9, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1238
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1246
    :catch_0
    move-exception v25

    .line 1247
    .local v25, "e":Ljava/lang/Exception;
    const-string v4, "SconnectManager"

    const-string v7, "ACTION_SEND_CURRENT_CONTENTS"

    const-string v9, "Exception on toast"

    move-object/from16 v0, v25

    invoke-static {v4, v7, v9, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1248
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 1251
    .end local v25    # "e":Ljava/lang/Exception;
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x10

    if-lez v4, :cond_10

    .line 1252
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1253
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    const/4 v7, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v4, v0, v1, v7}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Z)Z

    .line 1260
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    goto/16 :goto_0

    .line 1255
    :cond_f
    const-string v4, "SconnectManager"

    const-string v7, "sendFileShareCommand"

    const-string v9, "UPnP device is not connected!"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0900fb

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1258
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1261
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x4

    if-lez v4, :cond_15

    .line 1262
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_SEND_CURRENT_CONTENTS via BT OPP"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    if-eqz p4, :cond_11

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1264
    :cond_11
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-static {v7, v4, v9}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaMime(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 1266
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    move-result-object v4

    if-eqz v4, :cond_13

    .line 1267
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtMac:Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v4, v7, v0, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileViaBtOpp(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1270
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v4

    if-nez v4, :cond_14

    .line 1271
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/SconnectManager;->setHowToControlBluetooth(I)V

    .line 1273
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0900d8

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1276
    :cond_15
    const-string v4, "SconnectManager"

    const-string v7, "sendFileShareCommand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "invalid device : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, "("

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, ")"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0900fb

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1285
    :cond_16
    const-string v4, "SconnectManager"

    const-string v7, "sendFileShareCommand"

    const-string v9, "uri is null"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1290
    .end local v8    # "count":I
    :pswitch_2
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_SHARE_VIA_GROUP_PLAY"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x8

    if-lez v4, :cond_1d

    .line 1292
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getRequestId()Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v5

    .line 1293
    .restart local v5    # "id":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v0, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    move-object/from16 v36, v0

    .line 1294
    .local v36, "targetP2pMac":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getWifiMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    .line 1295
    .local v19, "bssidAddr":Ljava/lang/String;
    const-string v4, "SconnectManager"

    const-string v7, "ACTION_SHARE_VIA_GROUP_PLAY"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sendGroupShareCommand to "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v36

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " using "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298
    if-eqz p3, :cond_17

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v7, 0x1

    if-ge v4, v7, :cond_18

    .line 1299
    :cond_17
    const-string v4, "SconnectManager"

    const-string v7, "ACTION_SHARE_VIA_GROUP_PLAY"

    const-string v9, "with No item"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/central/action/GroupPlayActionHelper;->launchGroupPlayAsMaster(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 1301
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v7, 0x0

    move-object/from16 v0, v36

    move-object/from16 v1, v19

    invoke-virtual {v4, v5, v0, v1, v7}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendGroupShareCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1303
    :cond_18
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Landroid/net/Uri;

    .line 1304
    .local v35, "selectedUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    move-object/from16 v0, v35

    invoke-static {v4, v0, v7}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaMime(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 1305
    .local v33, "mimeToPass":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-static {v4, v0, v1}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    .line 1307
    .local v24, "contentUriToPass":Landroid/net/Uri;
    const/16 v32, 0x0

    .line 1309
    .local v32, "mimeToJoiner":I
    if-eqz v33, :cond_19

    .line 1310
    const-string v4, "video"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1311
    const/16 v32, 0x1

    .line 1321
    :cond_19
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    invoke-static {v4, v0, v1}, Lcom/samsung/android/sconnect/central/action/GroupPlayActionHelper;->launchGroupPlayAsMaster(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 1323
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, v36

    move-object/from16 v1, v19

    move/from16 v2, v32

    invoke-virtual {v4, v5, v0, v1, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendGroupShareCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1312
    :cond_1a
    const-string v4, "audio"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 1313
    const/16 v32, 0x2

    goto :goto_3

    .line 1314
    :cond_1b
    const-string v4, "image"

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 1315
    const/16 v32, 0x3

    goto :goto_3

    .line 1317
    :cond_1c
    const-string v4, "SconnectManager"

    const-string v7, "ACTION_SHARE_VIA_GROUP_PLAY"

    const-string v9, "mimeToJoiner is 0"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1327
    .end local v5    # "id":Ljava/lang/String;
    .end local v19    # "bssidAddr":Ljava/lang/String;
    .end local v24    # "contentUriToPass":Landroid/net/Uri;
    .end local v32    # "mimeToJoiner":I
    .end local v33    # "mimeToPass":Ljava/lang/String;
    .end local v35    # "selectedUri":Landroid/net/Uri;
    .end local v36    # "targetP2pMac":Ljava/lang/String;
    :cond_1d
    const-string v4, "SconnectManager"

    const-string v7, "ACTION_SHARE_VIA_GROUP_PLAY"

    const-string v9, "this is not DISCOVERY_TYPE_BLE deivce"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1333
    :pswitch_3
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_ASK_TO_EMEET"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x8

    if-lez v4, :cond_0

    .line 1335
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getRequestId()Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v5

    .line 1336
    .restart local v5    # "id":Ljava/lang/String;
    new-instance v20, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)V

    .line 1337
    .local v20, "cmdEmeet":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ACTION_ASK_TO_EMEET to "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v11

    iget-object v11, v11, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getBtMacAddress()Ljava/lang/String;

    move-result-object v28

    .line 1341
    .local v28, "hostBtMacAddr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v4, v5, v7, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendEmeetCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1343
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v9, p0

    move-object v10, v5

    move-object/from16 v11, p1

    move/from16 v12, p2

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sconnect/SconnectManager;->requestWaitingPopup(Ljava/lang/String;Lcom/samsung/android/sconnect/common/device/SconnectDevice;III)V

    goto/16 :goto_0

    .line 1348
    .end local v5    # "id":Ljava/lang/String;
    .end local v20    # "cmdEmeet":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    .end local v28    # "hostBtMacAddr":Ljava/lang/String;
    :pswitch_4
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_MORE_FEATURES"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1349
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v4

    const/16 v7, 0x8

    if-ne v4, v7, :cond_0

    .line 1351
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableType()I

    move-result v4

    if-lez v4, :cond_1e

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableInfo()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1e

    .line 1352
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableInfo()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableData()[B

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v7, v9}, Lcom/samsung/android/sconnect/SconnectManager;->launchWearableManager(Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 1356
    :cond_1e
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "this code must not be called"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1357
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v4

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v31

    .line 1358
    .local v31, "lowerName":Ljava/lang/String;
    const-string v4, "galaxy gear"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1f

    const-string v4, "gear 2"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1f

    const-string v4, "gear 3"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1f

    const-string v4, "gear s"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 1360
    :cond_1f
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    const-string v7, "watchmanager"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableData()[B

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v7, v9}, Lcom/samsung/android/sconnect/SconnectManager;->launchWearableManager(Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 1362
    :cond_20
    const-string v4, "gear fit"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1363
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    const-string v7, "wms"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableData()[B

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v7, v9}, Lcom/samsung/android/sconnect/SconnectManager;->launchWearableManager(Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 1371
    .end local v31    # "lowerName":Ljava/lang/String;
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v4

    const/16 v7, 0xe

    if-ne v4, v7, :cond_22

    .line 1372
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.android.homesync"

    invoke-static {v4, v7}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 1374
    new-instance v23, Landroid/content/ComponentName;

    const-string v4, "com.sec.android.homesync"

    const-string v7, "com.sec.android.homesync.RemoteLinkActivity"

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1377
    .local v23, "compName":Landroid/content/ComponentName;
    new-instance v27, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    move-object/from16 v0, v27

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1378
    .local v27, "homesyncIntent":Landroid/content/Intent;
    const-string v4, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1379
    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1380
    const/high16 v4, 0x10000000

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1381
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1383
    .end local v23    # "compName":Landroid/content/ComponentName;
    .end local v27    # "homesyncIntent":Landroid/content/Intent;
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.android.homesync"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->launchAppStore(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 1387
    :cond_22
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1388
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ACTION_BROWSE_CONTENTS - Type : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, ", NetType : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.pcw"

    invoke-static {v4, v7}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 1393
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x40

    if-lez v4, :cond_23

    .line 1394
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_BROWSE_CONTENTS - Registered device"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1395
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getSlinkId()J

    move-result-wide v16

    const-wide/16 v38, 0x1

    move-wide/from16 v0, v16

    move-wide/from16 v2, v38

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkLaunchUtils;->createLaunchToDeviceIntent(JJ)Landroid/content/Intent;

    move-result-object v30

    .line 1398
    .local v30, "intent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1399
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1401
    .end local v30    # "intent":Landroid/content/Intent;
    :cond_23
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_BROWSE_CONTENTS - Not registered device"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->createHowToUseViewIntent()Landroid/content/Intent;

    move-result-object v29

    .line 1405
    .local v29, "howToUseUIActivityIntent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1406
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1410
    .end local v29    # "howToUseUIActivityIntent":Landroid/content/Intent;
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.pcw"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->launchAppStore(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 1417
    :pswitch_6
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_TOGETHER_CREATE_ROOM"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    if-eqz p3, :cond_25

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_25

    .line 1419
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v11, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v13

    iget-object v13, v13, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual {v7, v9, v11, v4, v13}, Lcom/samsung/android/sconnect/central/CentralActionManager;->launchTogether(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1423
    :cond_25
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v13, 0x0

    invoke-virtual {v4, v7, v9, v11, v13}, Lcom/samsung/android/sconnect/central/CentralActionManager;->launchTogether(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1429
    :pswitch_7
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_TOGETHER_JOIN_ROOM"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    if-eqz p3, :cond_26

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_26

    .line 1431
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v9, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    invoke-virtual {v7, v9, v4}, Lcom/samsung/android/sconnect/central/CentralActionManager;->joinTogether(Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1433
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {v4, v7, v9}, Lcom/samsung/android/sconnect/central/CentralActionManager;->joinTogether(Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1438
    :pswitch_8
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_MIRROR_SCREEN_ON_"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v0, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    move-object/from16 v37, v0

    .line 1440
    .local v37, "tvMac":Ljava/lang/String;
    if-eqz v37, :cond_0

    .line 1441
    const-string v4, "SconnectManager"

    const-string v7, "ACTION_MIRROR_SCREEN_ON_"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " addr : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v37

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1442
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1443
    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->connectP2pRequest(Ljava/lang/String;)V

    .line 1444
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setWfdConnecting(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1449
    .end local v37    # "tvMac":Ljava/lang/String;
    :pswitch_9
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_CHROMECAST"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    if-eqz v4, :cond_28

    .line 1452
    if-eqz p3, :cond_27

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v7, 0x1

    if-lt v4, v7, :cond_27

    .line 1454
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-static {v7, v4, v9}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaMime(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 1456
    .local v26, "firstItemMime":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1457
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1458
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-virtual {v4, v7, v0, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->playChromecast(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1462
    .end local v26    # "firstItemMime":Ljava/lang/String;
    :cond_27
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "no content while ACTION_CHROMECAST"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1463
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1466
    :cond_28
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "mCentralActionManager is not initialized while ACTION_CHROMECAST"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1472
    :pswitch_a
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_PLAY_CONTENT should have passingIntent"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1476
    :pswitch_b
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_CONNECT"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    if-eqz v4, :cond_0

    .line 1479
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v34

    .line 1480
    .local v34, "name":Ljava/lang/String;
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "name : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v34

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1481
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v4

    const/16 v7, 0x8

    if-ne v4, v7, :cond_2d

    .line 1482
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableType()I

    move-result v4

    if-ltz v4, :cond_29

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableInfo()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_29

    .line 1483
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_CONNECT -launch wearable manager"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableInfo()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableData()[B

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v7, v9}, Lcom/samsung/android/sconnect/SconnectManager;->launchWearableManager(Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 1488
    :cond_29
    if-eqz v34, :cond_32

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_32

    .line 1490
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "this code must not be called"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1491
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v31

    .line 1492
    .restart local v31    # "lowerName":Ljava/lang/String;
    const-string v4, "galaxy gear"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    const-string v4, "gear 2"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    const-string v4, "gear 3"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    const-string v4, "gear s"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 1494
    :cond_2a
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_CONNECT - Launch GearManager"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1496
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    const-string v7, "watchmanager"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableData()[B

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v7, v9}, Lcom/samsung/android/sconnect/SconnectManager;->launchWearableManager(Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 1499
    :cond_2b
    const-string v4, "gear fit"

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 1500
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_CONNECT - Launch WingtipManager"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1502
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    const-string v7, "wms"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableData()[B

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v7, v9}, Lcom/samsung/android/sconnect/SconnectManager;->launchWearableManager(Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 1506
    :cond_2c
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "It is unknown name device - Name : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1518
    .end local v31    # "lowerName":Ljava/lang/String;
    :cond_2d
    :goto_4
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "This is not samsung\'s wearable. connect via bt"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1519
    if-eqz v34, :cond_2e

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 1520
    :cond_2e
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v0, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    move-object/from16 v34, v0

    .line 1522
    :cond_2f
    if-eqz v34, :cond_30

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_31

    .line 1523
    :cond_30
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0900c8

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 1525
    :cond_31
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v9, 0x7f0900d0

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v34, v11, v13

    invoke-virtual {v7, v9, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1527
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1528
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtMac:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lcom/samsung/android/sconnect/central/CentralActionManager;->connectToBtDevice(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1510
    :cond_32
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "It\'s unknown wearble device. Wearable Type : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableType()I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, ", Wearble Infor : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableInfo()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1535
    .end local v34    # "name":Ljava/lang/String;
    :pswitch_c
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_P2P_PAIRING"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1536
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v34

    .line 1537
    .restart local v34    # "name":Ljava/lang/String;
    if-eqz v34, :cond_33

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_34

    :cond_33
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceP2p()Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    move-result-object v4

    if-eqz v4, :cond_34

    .line 1538
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceP2p()Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    move-result-object v4

    iget-object v0, v4, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    move-object/from16 v34, v0

    .line 1540
    :cond_34
    if-eqz v34, :cond_35

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_36

    .line 1541
    :cond_35
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v7, 0x7f0900c8

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 1543
    :cond_36
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v9, 0x7f0900d0

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v34, v11, v13

    invoke-virtual {v7, v9, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v4, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1545
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1546
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceP2p()Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1547
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceP2p()Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/sconnect/SconnectManager;->connectP2pRequest(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1552
    .end local v34    # "name":Ljava/lang/String;
    :pswitch_d
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ACTION_PRINT via "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1553
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x2

    if-lez v4, :cond_38

    .line 1554
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1555
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_PRINT - P2P device"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/SconnectManager;->launchP2pPrinterWaiting(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V

    .line 1558
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getRequestId()Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v10

    .line 1559
    .local v10, "printId":Ljava/lang/String;
    if-eqz p3, :cond_37

    .line 1560
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v14

    move-object/from16 v9, p0

    move-object/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p5

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sconnect/SconnectManager;->requestWaitingPopup(Ljava/lang/String;Lcom/samsung/android/sconnect/common/device/SconnectDevice;III)V

    goto/16 :goto_0

    .line 1562
    :cond_37
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_PRINT : uri is null"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1564
    .end local v10    # "printId":Ljava/lang/String;
    :cond_38
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v4

    and-int/lit8 v4, v4, 0x4

    if-lez v4, :cond_3a

    .line 1565
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_PRINT via BT OPP"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-static {v7, v4, v9}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaMime(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 1567
    .restart local v26    # "firstItemMime":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    move-result-object v4

    if-eqz v4, :cond_39

    .line 1568
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtMac:Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-virtual {v4, v7, v0, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileViaBtOpp(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1571
    :cond_39
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v4

    if-nez v4, :cond_0

    .line 1572
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/SconnectManager;->setHowToControlBluetooth(I)V

    goto/16 :goto_0

    .line 1575
    .end local v26    # "firstItemMime":Ljava/lang/String;
    :cond_3a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->startPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 1580
    :pswitch_e
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_SIDESYNC_CONTROL"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1582
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/SconnectManager;->setHowToControlBluetooth(I)V

    .line 1585
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getRequestId()Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v12

    .line 1586
    .local v12, "requestId":Ljava/lang/String;
    new-instance v21, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)V

    .line 1587
    .restart local v21    # "cmdFileshare":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1588
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v4, v12, v7}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendSideSyncReqCommand(Ljava/lang/String;Ljava/lang/String;)V

    .line 1591
    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v11, p0

    move-object/from16 v13, p1

    move/from16 v14, p2

    invoke-virtual/range {v11 .. v16}, Lcom/samsung/android/sconnect/SconnectManager;->requestWaitingPopup(Ljava/lang/String;Lcom/samsung/android/sconnect/common/device/SconnectDevice;III)V

    goto/16 :goto_0

    .line 1595
    .end local v12    # "requestId":Ljava/lang/String;
    .end local v21    # "cmdFileshare":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    :pswitch_f
    const-string v4, "SconnectManager"

    const-string v7, "doAction"

    const-string v9, "ACTION_PROFILE_SHARE"

    invoke-static {v4, v7, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1596
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getRequestId()Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v14

    .line 1597
    .local v14, "reqId":Ljava/lang/String;
    new-instance v22, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)V

    .line 1598
    .local v22, "cmdProfileShare":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v22

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1599
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v4, v14, v7}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendProfileShareCommand(Ljava/lang/String;Ljava/lang/String;)V

    .line 1600
    const/16 v18, 0x0

    move-object/from16 v13, p0

    move-object/from16 v15, p1

    move/from16 v16, p2

    move/from16 v17, p5

    invoke-virtual/range {v13 .. v18}, Lcom/samsung/android/sconnect/SconnectManager;->requestWaitingPopup(Ljava/lang/String;Lcom/samsung/android/sconnect/common/device/SconnectDevice;III)V

    goto/16 :goto_0

    .line 1167
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_d
        :pswitch_0
        :pswitch_c
        :pswitch_e
        :pswitch_3
        :pswitch_f
        :pswitch_9
    .end packed-switch
.end method

.method public doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;ILandroid/content/Intent;)Z
    .locals 7
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "contentsType"    # I
    .param p6, "passingIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Landroid/content/Intent;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p3, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1117
    const-string v2, "SconnectManager"

    const-string v5, "doAction"

    const-string v6, "ACTION_PLAY_CONTENT need passingIntent"

    invoke-static {v2, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    packed-switch p2, :pswitch_data_0

    .line 1155
    const-string v2, "SconnectManager"

    const-string v3, "doAction"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mCentralActionManager Default action :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    invoke-virtual/range {p0 .. p5}, Lcom/samsung/android/sconnect/SconnectManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    :cond_0
    :goto_0
    move v2, v4

    .line 1160
    :goto_1
    return v2

    .line 1120
    :pswitch_0
    const-string v2, "SconnectManager"

    const-string v5, "doAction"

    const-string v6, "ACTION_PLAY_CONTENT"

    invoke-static {v2, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    if-eqz v2, :cond_6

    .line 1122
    if-eqz p3, :cond_5

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, v4, :cond_5

    .line 1123
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-static {v5, v2, v6}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaMime(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1124
    .local v1, "firstItemMime":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 1125
    const-string v2, "SconnectManager"

    const-string v4, "doAction"

    const-string v5, "queryMediaMime return null while ACTION_PLAY_CONTENT"

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 1127
    goto :goto_1

    .line 1130
    :cond_1
    const/4 v0, -0x1

    .line 1131
    .local v0, "dmrType":I
    const-string v2, "image"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1132
    const/4 v0, 0x2

    .line 1138
    :cond_2
    :goto_2
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1139
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceUpnp(I)Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1140
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceUpnp(I)Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpID:Ljava/lang/String;

    invoke-virtual {v2, v3, p3, v1, p6}, Lcom/samsung/android/sconnect/central/CentralActionManager;->playViaDlna(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 1133
    :cond_3
    const-string v2, "audio"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1135
    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    .line 1145
    .end local v0    # "dmrType":I
    .end local v1    # "firstItemMime":Ljava/lang/String;
    :cond_5
    const-string v2, "SconnectManager"

    const-string v4, "doAction"

    const-string v5, "no content while ACTION_PLAY_CONTENT"

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 1146
    goto :goto_1

    .line 1149
    :cond_6
    const-string v2, "SconnectManager"

    const-string v4, "doAction"

    const-string v5, "mCentralActionManager is not initialized while ACTION_PLAY_CONTENT"

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 1151
    goto :goto_1

    .line 1118
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public doActionForMultipleDevice(Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/lang/String;I)V
    .locals 18
    .param p2, "action"    # I
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "contentsType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1613
    .local p1, "devicelist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    .local p3, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    packed-switch p2, :pswitch_data_0

    .line 1698
    :cond_0
    :goto_0
    return-void

    .line 1615
    :pswitch_0
    const-string v3, "SconnectManager"

    const-string v6, "doActionForMultipleDevice"

    const-string v8, "ACTION_SEND_CURRENT_CONTENTS - send contents"

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1617
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const-string v6, "SEND"

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    move/from16 v0, p5

    invoke-static {v3, v6, v0, v8}, Lcom/samsung/android/sconnect/common/util/Util;->loggingSurvey(Landroid/content/Context;Ljava/lang/String;II)V

    .line 1619
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getClientCount()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/2addr v3, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sconnect/SconnectManager;->MAX_DIRECT_CONNECTION:I

    if-le v3, v6, :cond_3

    .line 1620
    const/4 v7, 0x0

    .line 1621
    .local v7, "count":I
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 1622
    .local v15, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1624
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 1627
    .end local v15    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getClientCount()I

    move-result v3

    add-int/2addr v3, v7

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sconnect/SconnectManager;->MAX_DIRECT_CONNECTION:I

    if-le v3, v6, :cond_3

    .line 1628
    const-string v3, "SconnectManager"

    const-string v6, "doAction"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MAX_DIRECT_CONNECTION : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getClientCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " + "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1631
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v8

    const v9, 0x7f0900f2

    invoke-virtual {v8, v9}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/sconnect/SconnectManager;->MAX_DIRECT_CONNECTION:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-static {v3, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1640
    .end local v7    # "count":I
    .end local v16    # "i$":Ljava/util/Iterator;
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1641
    .local v5, "targetBle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1642
    .local v17, "p2pDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getRequestId()Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v4

    .line 1643
    .local v4, "id":Ljava/lang/String;
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 1644
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v6, 0x1

    if-le v3, v6, :cond_4

    .line 1645
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->readyToConnection(Z)V

    .line 1647
    :cond_4
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .restart local v16    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 1648
    .restart local v15    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v3

    and-int/lit8 v3, v3, 0x8

    if-lez v3, :cond_5

    .line 1649
    new-instance v14, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;

    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v14, v15, v0, v1}, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)V

    .line 1650
    .local v14, "cmdFileshare":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v8

    iget-object v8, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1651
    const-string v3, "SconnectManager"

    const-string v6, "doActionForMultipleDevice"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BLE- "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1653
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1654
    .end local v14    # "cmdFileshare":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    :cond_5
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v3

    and-int/lit8 v3, v3, 0x2

    if-lez v3, :cond_6

    .line 1655
    const-string v3, "SconnectManager"

    const-string v6, "doActionForMultipleDevice"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "P2P- "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1657
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1659
    :cond_6
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v3

    and-int/lit8 v3, v3, 0x10

    if-lez v3, :cond_8

    .line 1660
    const-string v3, "SconnectManager"

    const-string v6, "doActionForMultipleDevice"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "UPNP- "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v9

    iget-object v9, v9, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1664
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1665
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v3, v15, v0, v6}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Z)Z

    goto/16 :goto_2

    .line 1667
    :cond_7
    const-string v3, "SconnectManager"

    const-string v6, "sendFileShareCommand"

    const-string v8, "UPnP device is not connected!"

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1668
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0900fb

    const/4 v8, 0x0

    invoke-static {v3, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 1672
    :cond_8
    const-string v3, "SconnectManager"

    const-string v6, "doActionForMultipleDevice"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "invalid device : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1678
    .end local v15    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_9
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1679
    .restart local v7    # "count":I
    const/16 v3, 0xff

    if-le v7, v3, :cond_a

    .line 1680
    const/16 v7, 0xff

    .line 1682
    :cond_a
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_b

    .line 1683
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/SconnectManager;->sendFileShareList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1686
    :cond_b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1687
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isGroupOwner()Z

    move-result v8

    move/from16 v6, p5

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendFileShareCommand(Ljava/lang/String;Ljava/util/ArrayList;IIZ)V

    .line 1690
    const/16 v3, 0x8

    move/from16 v0, p5

    if-ne v0, v3, :cond_c

    .line 1691
    invoke-static/range {p3 .. p3}, Lcom/samsung/android/sconnect/common/util/Util;->countContact(Ljava/util/ArrayList;)I

    move-result v7

    :cond_c
    move-object/from16 v8, p0

    move-object v9, v4

    move-object/from16 v10, p1

    move/from16 v11, p2

    move/from16 v12, p5

    move v13, v7

    .line 1693
    invoke-virtual/range {v8 .. v13}, Lcom/samsung/android/sconnect/SconnectManager;->requestWaitingPopup(Ljava/lang/String;Ljava/util/ArrayList;III)V

    goto/16 :goto_0

    .line 1613
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBLEHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    return-object v0
.end method

.method public getBluetoothActionHelper()Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    .locals 1

    .prologue
    .line 2089
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralActionManager;->getBluetoothActionHelper()Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothHelper()Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    return-object v0
.end method

.method public getFavoriteList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 906
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHowToControlBluetooth()I
    .locals 4

    .prologue
    .line 266
    const-string v0, "SconnectManager"

    const-string v1, "getHowToControlBluetooth"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBluetoothRemain == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBluetoothRemain:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBluetoothRemain:I

    return v0
.end method

.method public getP2pClientCount()I
    .locals 1

    .prologue
    .line 2055
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getClientCount()I

    move-result v0

    return v0
.end method

.method public getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    return-object v0
.end method

.method public getPreDiscoveryHelper()Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    return-object v0
.end method

.method public getPrintPluginHelper()Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    return-object v0
.end method

.method public getServiceType()I
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->getServiceType()I

    move-result v0

    return v0
.end method

.method public getWfdUtil()Lcom/samsung/android/sconnect/common/util/WfdUtil;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    return-object v0
.end method

.method public isBonded(Ljava/lang/String;)Z
    .locals 1
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 2093
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->isBonded(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isCentralRunning()Z
    .locals 1

    .prologue
    .line 439
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralRunning:Z

    return v0
.end method

.method public isConnectingDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 2075
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    if-eqz v0, :cond_0

    .line 2076
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectingDevice(Ljava/lang/String;)Z

    move-result v0

    .line 2078
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnectingState()Z
    .locals 1

    .prologue
    .line 2082
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    if-eqz v0, :cond_0

    .line 2083
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectingState()Z

    move-result v0

    .line 2085
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDialogShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2025
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsAcceptDialogShowing:Z

    if-eqz v1, :cond_1

    .line 2030
    :cond_0
    :goto_0
    return v0

    .line 2027
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2030
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDiscovering()Z
    .locals 1

    .prologue
    .line 443
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsDiscovering:Z

    return v0
.end method

.method public needToDisconnectOnCentral(Ljava/lang/String;)Z
    .locals 2
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 2059
    const/4 v0, 0x0

    .line 2060
    .local v0, "disconnect":Z
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    if-eqz v1, :cond_0

    .line 2061
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->needToDisconnect(Ljava/lang/String;)Z

    move-result v0

    .line 2063
    :cond_0
    return v0
.end method

.method public needToDisconnectOnPeriph(Ljava/lang/String;)Z
    .locals 2
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 2067
    const/4 v0, 0x0

    .line 2068
    .local v0, "disconnect":Z
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    if-eqz v1, :cond_0

    .line 2069
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->needToDisconnectOnPeriph(Ljava/lang/String;)Z

    move-result v0

    .line 2071
    :cond_0
    return v0
.end method

.method public pauseMirroring()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1805
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sidesync_source_connect"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1807
    .local v0, "sidesyncConnected":I
    if-ne v0, v4, :cond_1

    .line 1808
    const-string v1, "SconnectManager"

    const-string v2, "pauseMirroring"

    const-string v3, "do not pause Mirroring when SideSync Connected"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1817
    :cond_0
    :goto_0
    return-void

    .line 1811
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWfdConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsWfdPaused:Z

    if-nez v1, :cond_0

    .line 1812
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v2, 0x7f09011a

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1814
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->pauseWifiDisplay()V

    .line 1815
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsWfdPaused:Z

    goto :goto_0
.end method

.method public profileShare(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1730
    const/4 v0, 0x0

    .line 1731
    .local v0, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1732
    .local v1, "ssid":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1733
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1734
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    const-string v3, "ssid"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1736
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1738
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v2, p1, v0, v1, v5}, Lcom/samsung/android/sconnect/central/CentralActionManager;->startProfileShare(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 1743
    :goto_0
    return-void

    .line 1740
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v2, p1, v0, v1, v4}, Lcom/samsung/android/sconnect/central/CentralActionManager;->startProfileShare(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 1741
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/SconnectManager;->connectP2pRequest(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestWaitingPopup(Ljava/lang/String;Lcom/samsung/android/sconnect/common/device/SconnectDevice;III)V
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p3, "action"    # I
    .param p4, "contentType"    # I
    .param p5, "count"    # I

    .prologue
    .line 1994
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1995
    .local v2, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    .line 1996
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/SconnectManager;->requestWaitingPopup(Ljava/lang/String;Ljava/util/ArrayList;III)V

    .line 1997
    return-void
.end method

.method public requestWaitingPopup(Ljava/lang/String;Ljava/util/ArrayList;III)V
    .locals 10
    .param p1, "id"    # Ljava/lang/String;
    .param p3, "action"    # I
    .param p4, "contentType"    # I
    .param p5, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;III)V"
        }
    .end annotation

    .prologue
    .line 2001
    .local p2, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->turnOffWfdSetting()V

    .line 2003
    new-instance v4, Landroid/content/Intent;

    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const-class v7, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-direct {v4, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2004
    .local v4, "intent":Landroid/content/Intent;
    const v6, 0x30808000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2006
    const-string v6, "KEY"

    invoke-virtual {v4, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2007
    const-string v6, "CMD"

    invoke-virtual {v4, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2008
    const-string v6, "CONTENTTYPE"

    invoke-virtual {v4, v6, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2009
    const-string v6, "COUNT"

    invoke-virtual {v4, v6, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2011
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2012
    .local v5, "requestList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 2013
    .local v1, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    new-instance v3, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    invoke-direct {v3, v1, p3}, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)V

    .line 2014
    .local v3, "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2016
    .end local v1    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v3    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2017
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "DEVICE_INFO"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2018
    invoke-virtual {v4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2019
    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v7, 0x18

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 2020
    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2021
    iget-object v6, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const/16 v7, 0x18

    const-wide/32 v8, 0x88b8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2022
    return-void
.end method

.method public resumeMirroring()V
    .locals 1

    .prologue
    .line 1820
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWfdConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsWfdPaused:Z

    if-eqz v0, :cond_0

    .line 1821
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->resumeWifiDisplay()V

    .line 1824
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsWfdPaused:Z

    .line 1825
    return-void
.end method

.method public sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v4, 0x0

    .line 1778
    const-string v0, "SconnectManager"

    const-string v1, "sendfileShareList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "to : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1780
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1782
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Z)Z

    .line 1793
    :goto_0
    return-void

    .line 1785
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v0, p1, p2, v4}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1786
    const-string v0, "SconnectManager"

    const-string v1, "sendfileShareList"

    const-string v2, "FAILED"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0900fb

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1792
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/SconnectManager;->connectP2pRequest(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendFileShareList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    .local p2, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v9, 0x0

    const/16 v8, 0x16

    .line 1746
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1747
    .local v3, "p2pAddressList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "SconnectManager"

    const-string v5, "sendfileShareList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "multiple : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1748
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 1749
    .local v1, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1751
    const-string v4, "SconnectManager"

    const-string v5, "sendfileShareList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "to : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1752
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v1, p2, v5}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Z)Z

    goto :goto_0

    .line 1754
    :cond_0
    const-string v4, "SconnectManager"

    const-string v5, "sendfileShareList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "add : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1755
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v4, v1, p2, v9}, Lcom/samsung/android/sconnect/central/CentralActionManager;->sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1756
    const-string v4, "SconnectManager"

    const-string v5, "sendfileShareList"

    const-string v6, "FAILED"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1757
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0900fb

    invoke-static {v4, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1760
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1764
    .end local v1    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1765
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v4, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->connectPeer(Ljava/util/ArrayList;)V

    .line 1766
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1767
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getRequestTargetList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1768
    .local v0, "count":I
    if-lez v0, :cond_4

    .line 1769
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const v5, 0x9c40

    mul-int/2addr v5, v0

    int-to-long v6, v5

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1775
    .end local v0    # "count":I
    :cond_3
    :goto_1
    return-void

    .line 1771
    .restart local v0    # "count":I
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;

    const-wide/32 v6, 0x9c40

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method public setDeviceSelectMode(Z)V
    .locals 4
    .param p1, "mode"    # Z

    .prologue
    .line 2035
    const-string v0, "SconnectManager"

    const-string v1, "setDeviceSelectMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2036
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setDeviceSelectMode(Z)V

    .line 2037
    return-void
.end method

.method public setEnabledBt(Z)V
    .locals 4
    .param p1, "btTurnOn"    # Z

    .prologue
    .line 322
    const-string v0, "SconnectManager"

    const-string v1, "setEnabledBt"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    if-eqz p1, :cond_0

    .line 324
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBluetoothRemain:I

    .line 328
    :goto_0
    return-void

    .line 326
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBluetoothRemain:I

    goto :goto_0
.end method

.method public setEnabledWifi(Z)V
    .locals 4
    .param p1, "wifiTurnOn"    # Z

    .prologue
    .line 317
    const-string v0, "SconnectManager"

    const-string v1, "setEnabledWifi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    .line 319
    return-void
.end method

.method public setHowToControlBluetooth(I)V
    .locals 4
    .param p1, "bluetoothRemain"    # I

    .prologue
    .line 271
    const-string v0, "SconnectManager"

    const-string v1, "setHowToControlBluetooth"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBluetoothRemain = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iput p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBluetoothRemain:I

    .line 273
    return-void
.end method

.method public setLoaderManager(Landroid/app/LoaderManager;)V
    .locals 0
    .param p1, "loaderManager"    # Landroid/app/LoaderManager;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mLoaderManager:Landroid/app/LoaderManager;

    .line 234
    return-void
.end method

.method public setP2pConnected(Z)V
    .locals 4
    .param p1, "isConnect"    # Z

    .prologue
    .line 2103
    const-string v0, "SconnectManager"

    const-string v1, "setP2pConnected"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2104
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    if-eqz v0, :cond_0

    .line 2105
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->setP2pConnected(Z)V

    .line 2107
    :cond_0
    return-void
.end method

.method public setPrinterContext(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2097
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    if-eqz v0, :cond_0

    .line 2098
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->setPrinterContext(Landroid/content/Context;)V

    .line 2100
    :cond_0
    return-void
.end method

.method public startAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;I)V
    .locals 5
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p3, "cmdAction"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1702
    .local p2, "uri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    sparse-switch p3, :sswitch_data_0

    .line 1727
    :cond_0
    :goto_0
    return-void

    .line 1705
    :sswitch_0
    const-string v0, "SconnectManager"

    const-string v1, "startAction"

    const-string v2, "ACTION_SEND_CURRENT_CONTENTS - send contents"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1706
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v0

    and-int/lit8 v0, v0, 0x18

    if-lez v0, :cond_0

    .line 1708
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/SconnectManager;->sendFileShareList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1714
    :sswitch_1
    const-string v0, "SconnectManager"

    const-string v1, "startAction"

    const-string v2, "ACTION_ASK_TO_EMEET accepted"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1715
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, p2, v2}, Lcom/samsung/android/sconnect/central/action/EmeetingActionHelper;->launchEmeeting(Landroid/content/Context;ZLjava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0

    .line 1718
    :sswitch_2
    const-string v0, "SconnectManager"

    const-string v1, "startAction"

    const-string v2, "ACTION_SIDESYNC_CONTROL accepted"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1719
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/sconnect/central/action/SideSyncActionHelper;->startSideSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1723
    :sswitch_3
    const-string v0, "SconnectManager"

    const-string v1, "startAction"

    const-string v2, "ACTION_PROFILE_SHARE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1724
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/SconnectManager;->profileShare(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1702
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_1
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method public startCentral(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V
    .locals 3
    .param p1, "fileShareActionListener"    # Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .prologue
    .line 280
    const-string v0, "SconnectManager"

    const-string v1, "startCentral"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->startPeriphService(Landroid/content/Context;Z)Landroid/content/ComponentName;

    .line 283
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralRunning:Z

    .line 285
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->checkEnabledBt()Z

    .line 286
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->checkEnabledWifi()Z

    .line 288
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->initiate(Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;)V

    .line 289
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->initiate()V

    .line 290
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->initiate()V

    .line 291
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->initiate(Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;)V

    .line 292
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->initiate()V

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->initiate()V

    .line 295
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 296
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->initiate(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 298
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->pauseMirroring()V

    .line 299
    return-void
.end method

.method public startConnector()V
    .locals 3

    .prologue
    .line 389
    const-string v0, "SconnectManager"

    const-string v1, "startConnector"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->checkEnabledBt()Z

    .line 392
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->checkEnabledWifi()Z

    .line 394
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->initiate(Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;)V

    .line 395
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->initiate()V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->initiate(Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;)V

    .line 397
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->initiate()V

    .line 398
    return-void
.end method

.method public startDiscoveryDevice(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
    .locals 3
    .param p1, "discoveryListener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .prologue
    .line 474
    const-string v0, "SconnectManager"

    const-string v1, "startDiscoveryDevice"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 476
    const/16 v0, 0xff

    iput v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 477
    iget v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/SconnectManager;->startDiscovery(I)V

    .line 478
    return-void
.end method

.method public startDiscoveryDevice(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;I)V
    .locals 4
    .param p1, "discoveryListener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    .param p2, "type"    # I

    .prologue
    .line 467
    const-string v0, "SconnectManager"

    const-string v1, "startDiscoveryDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 469
    iput p2, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 470
    invoke-direct {p0, p2}, Lcom/samsung/android/sconnect/SconnectManager;->startDiscovery(I)V

    .line 471
    return-void
.end method

.method public stopCentral()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 331
    const-string v0, "SconnectManager"

    const-string v1, "stopCentral"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCentralRunning : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralRunning:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralRunning:Z

    if-eqz v0, :cond_a

    .line 333
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralRunning:Z

    .line 335
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->stopDiscoveryDevice()V

    .line 336
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v0

    if-nez v0, :cond_0

    .line 337
    const-string v0, "SconnectManager"

    const-string v1, "stopCentral"

    const-string v2, "No use bt : will stop bt"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->disableBluetoothAdapter()Z

    .line 339
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/SconnectManager;->setHowToControlBluetooth(I)V

    .line 341
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    if-eqz v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->checkConnectedAP(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 343
    const-string v0, "SconnectManager"

    const-string v1, "stopCentral"

    const-string v2, "wifi running"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralActionManager;->terminate()V

    .line 358
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    if-eqz v0, :cond_3

    .line 359
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->terminate()V

    .line 361
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    if-eqz v0, :cond_4

    .line 362
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopCentral()V

    .line 364
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    if-eqz v0, :cond_5

    .line 365
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->terminate()V

    .line 367
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    if-eqz v0, :cond_6

    .line 368
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->terminate()V

    .line 370
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    if-eqz v0, :cond_7

    .line 371
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->terminate()V

    .line 373
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    if-eqz v0, :cond_8

    .line 374
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopCentral()V

    .line 376
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    if-eqz v0, :cond_9

    .line 377
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->terminate()V

    .line 380
    :cond_9
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsStopMirroring:Z

    .line 383
    :cond_a
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->updateScanList(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)I

    .line 385
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->resumeMirroring()V

    .line 386
    return-void

    .line 344
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->isEnableMobileAP(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 345
    const-string v0, "SconnectManager"

    const-string v1, "stopCentral"

    const-string v2, "Mobile AP running"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 346
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 347
    const-string v0, "SconnectManager"

    const-string v1, "stopCentral"

    const-string v2, "p2p connected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 349
    :cond_d
    const-string v0, "SconnectManager"

    const-string v1, "stopCentral"

    const-string v2, "No use wifi : will stop wifi"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->disableWifi(Landroid/content/Context;)V

    .line 351
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiTurnOn:Z

    goto/16 :goto_0
.end method

.method public stopConnector()V
    .locals 3

    .prologue
    .line 401
    const-string v0, "SconnectManager"

    const-string v1, "stopConnector"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->stopDiscoveryDevice()V

    .line 405
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->terminate()V

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    if-eqz v0, :cond_1

    .line 409
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopCentral()V

    .line 411
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    if-eqz v0, :cond_2

    .line 412
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->terminate()V

    .line 414
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    if-eqz v0, :cond_3

    .line 415
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->terminate()V

    .line 417
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    if-eqz v0, :cond_4

    .line 420
    :cond_4
    return-void
.end method

.method public stopDiscoveryDevice()V
    .locals 3

    .prologue
    .line 481
    const-string v0, "SconnectManager"

    const-string v1, "stopDiscoveryDevice"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanDeviceType:I

    .line 484
    invoke-direct {p0}, Lcom/samsung/android/sconnect/SconnectManager;->stopReservedDiscovery()V

    .line 486
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopDiscovery()V

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    if-eqz v0, :cond_1

    .line 490
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->stopDiscovery()V

    .line 492
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    if-eqz v0, :cond_2

    .line 493
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->stopDiscovery()V

    .line 495
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    if-eqz v0, :cond_3

    .line 496
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->stopDiscovery()V

    .line 498
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    if-eqz v0, :cond_4

    .line 499
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->stopDiscovery()V

    .line 501
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    if-eqz v0, :cond_5

    .line 502
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->stopDiscovery()V

    .line 504
    :cond_5
    return-void
.end method

.method public stopMirroring()V
    .locals 1

    .prologue
    .line 1800
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralActionManager;->stopMirroring()V

    .line 1801
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/SconnectManager;->mIsStopMirroring:Z

    .line 1802
    return-void
.end method

.method public terminate()V
    .locals 4

    .prologue
    .line 237
    const-string v1, "SconnectManager"

    const-string v2, "terminate"

    const-string v3, "--"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-direct {p0}, Lcom/samsung/android/sconnect/SconnectManager;->unregisterBroadcast()V

    .line 240
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/SconnectManager;->stopCentral()V

    .line 242
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    if-eqz v1, :cond_0

    .line 243
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->terminate()V

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBLEHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    if-eqz v1, :cond_1

    .line 246
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mBLEHelper:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->terminate()V

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    if-eqz v1, :cond_2

    .line 249
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->terminate()V

    .line 252
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    if-eqz v1, :cond_3

    .line 253
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->terminate()V

    .line 257
    :cond_3
    const-wide/16 v2, 0x12c

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/android/sconnect/SconnectManager;->mInstance:Lcom/samsung/android/sconnect/SconnectManager;

    .line 263
    return-void

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
