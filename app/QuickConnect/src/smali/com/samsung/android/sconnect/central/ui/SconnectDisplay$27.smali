.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$27;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 2951
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$27;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "mac"    # Ljava/lang/String;
    .param p3, "state"    # Ljava/lang/String;

    .prologue
    .line 2954
    const-string v3, "SconnectDisplay"

    const-string v4, "IFileShareActionListener"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2955
    const/4 v1, 0x0

    .line 2956
    .local v1, "isFound":Z
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$27;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2957
    .local v2, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$27;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v4, 0x1

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findFileShareItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)Z
    invoke-static {v3, v2, p2, p3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    .line 2958
    if-eqz v1, :cond_0

    .line 2962
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_1
    if-nez v1, :cond_3

    .line 2963
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$27;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2964
    .restart local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$27;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v4, 0x0

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findFileShareItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)Z
    invoke-static {v3, v2, p2, p3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    .line 2965
    if-eqz v1, :cond_2

    .line 2970
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_3
    return-void
.end method
