.class Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$4;
.super Landroid/database/ContentObserver;
.source "PreDiscoveryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 588
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$4;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 591
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 592
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "mContentObserver"

    const-string v2, "Contact Data changed"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$4;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 594
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$4;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateMyDeviceInfo()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 595
    return-void
.end method
