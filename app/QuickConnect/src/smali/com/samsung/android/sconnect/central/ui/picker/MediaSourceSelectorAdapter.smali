.class Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MediaSourceSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;,
        Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final mResource:I = 0x7f03000e


# instance fields
.field protected mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;I)V
    .locals 2
    .param p1, "activity"    # Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;
    .param p2, "requestCode"    # I

    .prologue
    .line 487
    const v0, 0x7f03000e

    invoke-static {p1, p2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->getData(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 488
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 489
    return-void
.end method

.method private static addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p3, "menuStringId"    # I
    .param p4, "icondId"    # I
    .param p5, "item"    # Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;",
            ">;II",
            "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 647
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;>;"
    invoke-static {p0, p1}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    .line 648
    .local v5, "isEnabled":Z
    const-string v8, "MediaSourceSelector"

    const-string v9, "addEnabledPackageItem"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " isEnabledpkg? "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    if-eqz v5, :cond_5

    .line 650
    const/4 v1, 0x0

    .line 651
    .local v1, "appIcon":Landroid/graphics/drawable/Drawable;
    const/4 v6, 0x0

    .line 653
    .local v6, "menuString":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 655
    .local v7, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v7, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v3

    .line 656
    .local v3, "appState":I
    const/4 v8, 0x2

    if-eq v3, v8, :cond_0

    const/4 v8, 0x4

    if-eq v3, v8, :cond_0

    const/4 v8, 0x3

    if-ne v3, v8, :cond_1

    .line 660
    :cond_0
    const-string v8, "MediaSourceSelector"

    const-string v9, "getApplicationEnabledSetting :"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " packageName? "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const/4 v8, 0x0

    .line 691
    .end local v1    # "appIcon":Landroid/graphics/drawable/Drawable;
    .end local v3    # "appState":I
    .end local v6    # "menuString":Ljava/lang/String;
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v8

    .line 664
    .restart local v1    # "appIcon":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "appState":I
    .restart local v6    # "menuString":Ljava/lang/String;
    .restart local v7    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    if-ltz p4, :cond_2

    .line 665
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    move/from16 v0, p4

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 670
    :goto_1
    if-ltz p3, :cond_3

    .line 671
    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 682
    :goto_2
    if-eqz v1, :cond_4

    .line 683
    move-object/from16 v0, p5

    invoke-static {p2, v6, v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addItem(Ljava/util/List;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)V

    .line 684
    const/4 v8, 0x1

    goto :goto_0

    .line 667
    :cond_2
    invoke-static {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->getApplicationIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 674
    :cond_3
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v7, p1, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 675
    .local v2, "appInfo":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v7, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object v6, v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 677
    .end local v2    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v4

    .line 678
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "MediaSourceSelector"

    const-string v9, "NameNotFoundException(2)"

    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 686
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_4
    const-string v8, "MediaSourceSelector"

    const-string v9, "addEnabledPackageItem"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " appIcon is null!: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p4

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    .end local v1    # "appIcon":Landroid/graphics/drawable/Drawable;
    .end local v3    # "appState":I
    .end local v6    # "menuString":Ljava/lang/String;
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    :cond_5
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private static addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p3, "menuStringId"    # I
    .param p4, "item"    # Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;",
            ">;I",
            "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 696
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;>;"
    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    move-result v0

    return v0
.end method

.method protected static addItem(Ljava/util/List;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "appIcon"    # Landroid/graphics/drawable/Drawable;
    .param p3, "command"    # Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 701
    .local p0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;>;"
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;

    invoke-direct {v0, p1, p2, p3}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)V

    .line 702
    .local v0, "temp":Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 703
    return-void
.end method

.method private static getApplicationIcon(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "package_name"    # Ljava/lang/String;

    .prologue
    .line 636
    const/4 v0, 0x0

    .line 638
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 642
    :goto_0
    return-object v2

    .line 639
    :catch_0
    move-exception v1

    .line 640
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected static getData(Landroid/content/Context;I)Ljava/util/List;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const v13, 0x7f090093

    const v12, 0x7f02009f

    const v4, 0x7f020052

    const v3, 0x7f090090

    .line 529
    const/4 v11, 0x0

    .line 530
    .local v11, "bResult":Z
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 532
    .local v2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;>;"
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_6

    .line 533
    const-string v1, "com.sec.android.gallery3d"

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    move-result v11

    .line 536
    if-nez v11, :cond_0

    .line 537
    const-string v1, "com.cooliris.media"

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 541
    :cond_0
    const-string v1, "com.sec.android.app.camera"

    const v3, 0x7f090092

    const v4, 0x7f02008f

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->TAKE_PICTURE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 545
    const-string v1, "com.sec.android.gallery3d"

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    move v3, v13

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    move-result v11

    .line 548
    if-nez v11, :cond_1

    .line 549
    const-string v1, "com.cooliris.media"

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    move v3, v13

    move v4, v12

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 553
    :cond_1
    const-string v1, "com.sec.android.app.camera"

    const v3, 0x7f090094

    const v4, 0x7f020008

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 556
    const-string v1, "com.sec.android.app.music"

    const v3, 0x7f090096

    const v4, 0x7f020005

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 559
    const-string v1, "com.sec.android.app.voicenote"

    const v3, 0x7f090097

    const v4, 0x7f02005a

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    move-result v11

    .line 561
    if-nez v11, :cond_2

    .line 562
    const-string v1, "com.sec.android.app.voicenote"

    const v3, 0x7f090097

    const v4, 0x7f02005a

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 566
    :cond_2
    const-string v1, "com.samsung.android.app.memo"

    const v4, 0x7f020054

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_MEMO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    move v3, v6

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 569
    const-string v1, "com.samsung.android.snote"

    const v3, 0x7f090098

    const v4, 0x7f020065

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SNOTE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 572
    const-string v1, "com.android.calendar"

    const v3, 0x7f09009f

    const v4, 0x7f020007

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCAL:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 575
    const-string v1, "com.google.android.apps.maps"

    const v3, 0x7f090091

    const v4, 0x7f020053

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_LOCATION:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    move-result v11

    .line 577
    if-nez v11, :cond_3

    .line 578
    const-string v0, "CHINA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 579
    const v0, 0x7f090091

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020056

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_LOCATION:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    invoke-static {v2, v0, v3, v4}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addItem(Ljava/util/List;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)V

    .line 585
    :cond_3
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v3, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 588
    .local v1, "contactPackage":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 589
    const-string v1, "com.android.contacts"

    .end local v1    # "contactPackage":Ljava/lang/String;
    const v4, 0x7f02000a

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCARD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    move v3, v6

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 596
    :goto_0
    const-string v4, "com.sec.android.app.myfiles"

    const v6, 0x7f09009e

    const v7, 0x7f020055

    sget-object v8, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_DOC:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v3, p0

    move-object v5, v2

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 599
    const-string v4, "com.sec.android.app.myfiles"

    const v6, 0x7f0900a0

    const v7, 0x7f02004e

    sget-object v8, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_MYFILES:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v3, p0

    move-object v5, v2

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 632
    :cond_4
    :goto_1
    return-object v2

    .line 592
    .restart local v1    # "contactPackage":Ljava/lang/String;
    :cond_5
    const v4, 0x7f02000a

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCARD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v0, p0

    move v3, v6

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    goto :goto_0

    .line 601
    .end local v1    # "contactPackage":Ljava/lang/String;
    :cond_6
    const/16 v0, 0x3ea

    if-eq p1, v0, :cond_7

    const/16 v0, 0x3ef

    if-ne p1, v0, :cond_a

    .line 603
    :cond_7
    const-string v6, "com.sec.android.gallery3d"

    sget-object v10, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v5, p0

    move-object v7, v2

    move v8, v3

    move v9, v4

    invoke-static/range {v5 .. v10}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    move-result v11

    .line 606
    if-nez v11, :cond_8

    .line 607
    const-string v6, "com.cooliris.media"

    sget-object v10, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v5, p0

    move-object v7, v2

    move v8, v3

    move v9, v4

    invoke-static/range {v5 .. v10}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 610
    :cond_8
    const-string v4, "com.sec.android.gallery3d"

    sget-object v8, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v3, p0

    move-object v5, v2

    move v6, v13

    move v7, v12

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    move-result v11

    .line 613
    if-nez v11, :cond_9

    .line 614
    const-string v4, "com.cooliris.media"

    sget-object v8, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v3, p0

    move-object v5, v2

    move v6, v13

    move v7, v12

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 617
    :cond_9
    const-string v4, "com.sec.android.app.music"

    const v6, 0x7f090096

    const v7, 0x7f020005

    sget-object v8, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v3, p0

    move-object v5, v2

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    goto :goto_1

    .line 619
    :cond_a
    const/16 v0, 0x3eb

    if-ne p1, v0, :cond_4

    .line 621
    const-string v0, "com.sec.android.gallery3d"

    sget-object v4, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    invoke-static {p0, v0, v2, v3, v4}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    move-result v11

    .line 624
    if-nez v11, :cond_b

    .line 625
    const-string v0, "com.cooliris.media"

    sget-object v4, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    invoke-static {p0, v0, v2, v3, v4}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    .line 629
    :cond_b
    const-string v4, "com.sec.android.app.myfiles"

    const v6, 0x7f09009e

    const v7, 0x7f020055

    sget-object v8, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_PDF:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-object v3, p0

    move-object v5, v2

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->addEnabledPackageItem(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;IILcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)Z

    goto :goto_1
.end method


# virtual methods
.method public buttonToCommand(I)Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
    .locals 2
    .param p1, "whichButton"    # I

    .prologue
    .line 524
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;

    .line 525
    .local v0, "item":Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->getCommand()Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    move-result-object v1

    return-object v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 498
    if-nez p2, :cond_0

    .line 499
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f03000e

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 505
    .local v3, "view":Landroid/view/View;
    :goto_0
    const v4, 0x1020014

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 506
    .local v2, "text":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 509
    const v4, 0x7f0c003c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 511
    .local v1, "image":Landroid/widget/ImageView;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->getResource()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 513
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1

    .line 514
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->getResource()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 515
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 520
    :goto_1
    return-object v3

    .line 501
    .end local v0    # "appIcon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "image":Landroid/widget/ImageView;
    .end local v2    # "text":Landroid/widget/TextView;
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    move-object v3, p2

    .restart local v3    # "view":Landroid/view/View;
    goto :goto_0

    .line 517
    .restart local v0    # "appIcon":Landroid/graphics/drawable/Drawable;
    .restart local v1    # "image":Landroid/widget/ImageView;
    .restart local v2    # "text":Landroid/widget/TextView;
    :cond_1
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
