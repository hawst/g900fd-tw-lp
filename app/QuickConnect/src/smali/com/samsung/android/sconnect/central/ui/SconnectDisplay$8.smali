.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$action:I

.field final synthetic val$contentsList:Ljava/util/ArrayList;

.field final synthetic val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 986
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iput p3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->val$action:I

    iput-object p4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->val$contentsList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 989
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->val$action:I

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->val$contentsList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;ILandroid/content/Intent;)Z

    .line 991
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 992
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 993
    return-void
.end method
