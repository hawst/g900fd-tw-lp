.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;

.field final synthetic val$service:I


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;I)V
    .locals 0

    .prologue
    .line 3503
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;

    iput p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;->val$service:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 3507
    const-wide/16 v2, 0x7d0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 3508
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3509
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;->val$service:I

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;

    iget-object v4, v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$6000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3515
    :cond_0
    :goto_0
    return-void

    .line 3511
    :catch_0
    move-exception v0

    .line 3512
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_SEND_CONTENTS Runnable Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
