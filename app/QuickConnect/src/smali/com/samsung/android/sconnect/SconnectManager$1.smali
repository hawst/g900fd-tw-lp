.class Lcom/samsung/android/sconnect/SconnectManager$1;
.super Landroid/os/Handler;
.source "SconnectManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/SconnectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/SconnectManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/SconnectManager;)V
    .locals 0

    .prologue
    .line 595
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x0

    .line 598
    const-string v7, "SconnectManager"

    const-string v8, "ScanHandler: "

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 668
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 610
    :pswitch_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 611
    .local v0, "argMsg":Landroid/os/Message;
    iget v7, p1, Landroid/os/Message;->what:I

    iput v7, v0, Landroid/os/Message;->what:I

    .line 612
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mScanThread:Lcom/samsung/android/sconnect/SconnectManager$ScanThread;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/SconnectManager$ScanThread;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->sendMessage(Landroid/os/Message;)I

    goto :goto_0

    .line 615
    .end local v0    # "argMsg":Landroid/os/Message;
    :pswitch_2
    const-string v7, "SconnectManager"

    const-string v8, "ScanThread"

    const-string v9, "MSG_P2P_TIMEOUT"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 617
    .local v6, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 618
    .local v1, "cancel":Z
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 619
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getRequestPeerList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 620
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getRequestTargetList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 621
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->cancelConnectPeer()V

    .line 623
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v7

    if-eqz v7, :cond_3

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 625
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 626
    .local v5, "target":Ljava/lang/String;
    if-eqz v5, :cond_2

    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/samsung/android/sconnect/central/CentralActionManager;->removeHoldingIntent(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 627
    const/4 v1, 0x1

    goto :goto_1

    .line 631
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "target":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 632
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/central/CentralActionManager;->removeHoldingIntentAll()V

    .line 633
    :cond_4
    if-eqz v1, :cond_0

    .line 634
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.samsung.android.sconnect.central.REQUEST_RESULT"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 635
    .local v3, "i":Landroid/content/Intent;
    const-string v7, "KEY"

    const-string v8, "CANCEL"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 636
    const-string v7, "ADDRESS"

    const-string v8, ""

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 637
    const-string v7, "RES"

    invoke-virtual {v3, v7, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 638
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 640
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0900fb

    invoke-static {v7, v8, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 645
    .end local v1    # "cancel":Z
    .end local v3    # "i":Landroid/content/Intent;
    .end local v6    # "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :pswitch_3
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;

    .line 646
    .local v2, "cmd":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    if-eqz v2, :cond_0

    .line 647
    iget v7, v2, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;->mAction:I

    const/16 v8, 0x10

    if-ne v7, v8, :cond_5

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "REMOTE_BT_MAC"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 651
    const-string v7, "SconnectManager"

    const-string v8, "MSG_REQUEST_START"

    const-string v9, "ACTION_SIDESYNC_CONTROL replace BT_MAC picking from CONFIRMED_ACTION"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    iget-object v7, v2, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;->mDevice:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "REMOTE_BT_MAC"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    .line 656
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v8, v2, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;->mDevice:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v9, v2, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;->mUris:Ljava/util/ArrayList;

    iget v10, v2, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;->mAction:I

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/sconnect/SconnectManager;->startAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;I)V

    goto/16 :goto_0

    .line 660
    .end local v2    # "cmd":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    :pswitch_4
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v7

    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.samsung.android.sconnect.periph.WAITING_P2PREQUEST"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 664
    :pswitch_5
    iget-object v7, p0, Lcom/samsung/android/sconnect/SconnectManager$1;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v7}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    goto/16 :goto_0

    .line 599
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
