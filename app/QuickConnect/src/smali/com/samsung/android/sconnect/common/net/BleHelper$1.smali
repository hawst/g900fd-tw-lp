.class Lcom/samsung/android/sconnect/common/net/BleHelper$1;
.super Landroid/os/Handler;
.source "BleHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/BleHelper;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    const/4 v5, -0x1

    .line 387
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 389
    :pswitch_0
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v1

    .line 390
    .local v1, "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    if-eqz v1, :cond_0

    .line 391
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    const/16 v3, 0xb

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$102(Lcom/samsung/android/sconnect/common/net/BleHelper;I)I

    .line 392
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$500(Lcom/samsung/android/sconnect/common/net/BleHelper;)Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->addLeRadioReference(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;)Z

    goto :goto_0

    .line 396
    .end local v1    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$600(Lcom/samsung/android/sconnect/common/net/BleHelper;)I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_5

    .line 397
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # operator++ for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$608(Lcom/samsung/android/sconnect/common/net/BleHelper;)I

    .line 399
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$400(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    if-nez v2, :cond_2

    .line 400
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$402(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/BluetoothAdapter;

    .line 401
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 402
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/BleHelper;->setLocalScanFilters()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$700(Lcom/samsung/android/sconnect/common/net/BleHelper;)V

    .line 405
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 407
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 408
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 410
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$900(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$800(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/content/BroadcastReceiver;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 411
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mRegisterIntent:Z
    invoke-static {v2, v4}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1002(Lcom/samsung/android/sconnect/common/net/BleHelper;Z)Z

    .line 413
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$400(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v3

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1102(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/bluetooth/le/BluetoothLeScanner;)Landroid/bluetooth/le/BluetoothLeScanner;

    .line 414
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v2

    if-nez v2, :cond_3

    .line 415
    const-string v2, "BleHelper"

    const-string v3, "MSG_START_LESCAN"

    const-string v4, "getBluetoothLeScanner returns null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    new-instance v3, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    invoke-direct {v3}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;-><init>()V

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$302(Lcom/samsung/android/sconnect/common/net/BleHelper;Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;)Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    .line 420
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v2

    if-nez v2, :cond_4

    .line 421
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mPowerManager:Landroid/os/PowerManager;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/os/PowerManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 422
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    goto/16 :goto_0

    .line 425
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    goto/16 :goto_0

    .line 428
    :cond_5
    const-string v2, "BleHelper"

    const-string v3, "MSG_START_LESCAN"

    const-string v4, "mBleStartCnt exceed MAX count 3"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$602(Lcom/samsung/android/sconnect/common/net/BleHelper;I)I

    goto/16 :goto_0

    .line 434
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopScan(Z)V

    goto/16 :goto_0

    .line 387
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
