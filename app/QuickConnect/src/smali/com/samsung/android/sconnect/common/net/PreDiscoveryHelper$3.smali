.class Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;
.super Landroid/content/BroadcastReceiver;
.source "PreDiscoveryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 573
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 574
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 575
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopAutoScanTimer()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 576
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setVisibilityFromSystemDb()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$700(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 577
    :cond_1
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 578
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setVisibilityFromSystemDb()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$700(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    goto :goto_0

    .line 579
    :cond_2
    const-string v1, "com.android.settings.DEVICE_NAME_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 580
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initializeAdvData()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$800(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    goto :goto_0

    .line 581
    :cond_3
    const-string v1, "com.android.settings.QUICK_CONNECT_SETTINGS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 582
    const-string v1, "PreDiscoveryHelper"

    const-string v2, "mReceiver"

    const-string v3, "com.android.settings.QUICK_CONNECT_SETTINGS_CHANGED"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->setVisibilityFromSystemDb()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$700(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    goto :goto_0
.end method
