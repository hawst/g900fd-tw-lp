.class Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FavoriteDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FavoriteIconSelectorAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final mResource:I = 0x7f03000b


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 635
    const v0, 0x7f03000b

    invoke-static {p1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->getData(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 636
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->mContext:Landroid/content/Context;

    .line 637
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 639
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$000()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImageType()I

    move-result v0

    # setter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$602(I)I

    .line 640
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$600()I

    move-result v0

    if-gez v0, :cond_0

    .line 641
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$000()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 642
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$602(I)I

    .line 647
    :cond_0
    :goto_0
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$600()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 648
    const/4 v0, 0x4

    # setter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$602(I)I

    .line 650
    :cond_1
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "FavoriteIconSelectorAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkIcon: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$600()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    return-void

    .line 644
    :cond_2
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$000()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v0

    # setter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$602(I)I

    goto :goto_0
.end method

.method protected static getData(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 654
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 656
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$000()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImage()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 657
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 659
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 660
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 661
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 663
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 664
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 665
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 667
    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 668
    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 669
    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 670
    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 671
    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 672
    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 673
    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 674
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 675
    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 676
    const/16 v1, 0x12

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 677
    const/16 v1, 0x1a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 678
    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 679
    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 681
    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 682
    const/16 v1, 0x18

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 683
    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 684
    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 685
    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v10, 0x7f09015c

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 691
    if-nez p2, :cond_2

    .line 692
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f03000b

    invoke-virtual {v5, v6, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 697
    .local v4, "view":Landroid/view/View;
    :goto_0
    const v5, 0x7f0c0038

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 698
    .local v2, "iconView":Landroid/widget/ImageView;
    const v5, 0x7f0c0037

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 700
    .local v1, "iconBackgroundView":Landroid/widget/ImageView;
    const v5, 0x7f0c0039

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 702
    .local v0, "checkView":Landroid/widget/ImageView;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 703
    .local v3, "type":I
    if-lt v3, v7, :cond_3

    const/16 v5, 0x1a

    if-gt v3, v5, :cond_3

    .line 705
    sget-object v5, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceImages:[I

    add-int/lit8 v6, v3, -0x1

    aget v5, v5, v6

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 706
    sget-object v5, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceBgImages:[I

    add-int/lit8 v6, v3, -0x1

    aget v5, v5, v6

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 707
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->mContext:Landroid/content/Context;

    new-array v6, v7, [Ljava/lang/Object;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$1300()Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getDeviceString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v5, v10, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 722
    :cond_0
    :goto_1
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 723
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$600()I

    move-result v5

    if-ne v3, v5, :cond_1

    .line 724
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 726
    :cond_1
    return-object v4

    .line 694
    .end local v0    # "checkView":Landroid/widget/ImageView;
    .end local v1    # "iconBackgroundView":Landroid/widget/ImageView;
    .end local v2    # "iconView":Landroid/widget/ImageView;
    .end local v3    # "type":I
    .end local v4    # "view":Landroid/view/View;
    :cond_2
    move-object v4, p2

    .restart local v4    # "view":Landroid/view/View;
    goto :goto_0

    .line 709
    .restart local v0    # "checkView":Landroid/widget/ImageView;
    .restart local v1    # "iconBackgroundView":Landroid/widget/ImageView;
    .restart local v2    # "iconView":Landroid/widget/ImageView;
    .restart local v3    # "type":I
    :cond_3
    if-nez v3, :cond_4

    .line 710
    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 711
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$000()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImage()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 712
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->mContext:Landroid/content/Context;

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f090090

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v5, v10, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 715
    :cond_4
    const/16 v5, 0x64

    if-ne v3, v5, :cond_0

    .line 716
    const v5, 0x7f020070

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 717
    const v5, 0x7f020009

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 718
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f09015d

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
