.class Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;
.super Ljava/lang/Object;
.source "UpnpHelper.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/UpnpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 4
    .param p1, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;
    .param p2, "state"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 263
    const-string v0, "UpnpHelper"

    const-string v1, "mMediaServiceListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreated : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    sget-object v0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$7;->$SwitchMap$com$samsung$android$allshare$ServiceConnector$ServiceState:[I

    invoke-virtual {p2}, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 283
    .end local p1    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 269
    .restart local p1    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    check-cast p1, Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .end local p1    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    # setter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0, p1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$302(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 270
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$300(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$300(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDeviceFinder()Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$402(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .line 273
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$400(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mAVPlayerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$500(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 275
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mMediaDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$400(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mImageViewerFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$600(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    goto :goto_0

    .line 264
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 3
    .param p1, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 257
    const-string v0, "UpnpHelper"

    const-string v1, "mMediaServiceListener"

    const-string v2, "onDeleted"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    return-void
.end method
