.class public Lcom/samsung/android/sconnect/update/DownloadManager;
.super Ljava/lang/Object;
.source "DownloadManager.java"


# static fields
.field private static mInstance:Lcom/samsung/android/sconnect/update/DownloadManager;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, "DownloadManager"

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/DownloadManager;->TAG:Ljava/lang/String;

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/DownloadManager;->mContext:Landroid/content/Context;

    .line 17
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/update/DownloadManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/update/DownloadManager;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/samsung/android/sconnect/update/DownloadManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/update/DownloadManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/update/DownloadManager;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/samsung/android/sconnect/update/DownloadManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getDownloadManager()Lcom/samsung/android/sconnect/update/DownloadManager;
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/sconnect/update/DownloadManager;->getDownloadManager(Landroid/content/Context;)Lcom/samsung/android/sconnect/update/DownloadManager;

    move-result-object v0

    return-object v0
.end method

.method public static getDownloadManager(Landroid/content/Context;)Lcom/samsung/android/sconnect/update/DownloadManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/android/sconnect/update/DownloadManager;->mInstance:Lcom/samsung/android/sconnect/update/DownloadManager;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/samsung/android/sconnect/update/DownloadManager;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/update/DownloadManager;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/update/DownloadManager;->mInstance:Lcom/samsung/android/sconnect/update/DownloadManager;

    .line 27
    :cond_0
    if-eqz p0, :cond_1

    .line 28
    sget-object v0, Lcom/samsung/android/sconnect/update/DownloadManager;->mInstance:Lcom/samsung/android/sconnect/update/DownloadManager;

    iput-object p0, v0, Lcom/samsung/android/sconnect/update/DownloadManager;->mContext:Landroid/content/Context;

    .line 30
    :cond_1
    sget-object v0, Lcom/samsung/android/sconnect/update/DownloadManager;->mInstance:Lcom/samsung/android/sconnect/update/DownloadManager;

    return-object v0
.end method


# virtual methods
.method public updateCheck(Z)V
    .locals 4
    .param p1, "showupDialog"    # Z

    .prologue
    .line 34
    iget-object v1, p0, Lcom/samsung/android/sconnect/update/DownloadManager;->TAG:Ljava/lang/String;

    const-string v2, "updateCheck"

    const-string v3, "--"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    iget-object v1, p0, Lcom/samsung/android/sconnect/update/DownloadManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sconnect/update/UpdateSamsungApps;->setContext(Landroid/content/Context;)V

    .line 36
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/update/DownloadManager$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sconnect/update/DownloadManager$1;-><init>(Lcom/samsung/android/sconnect/update/DownloadManager;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 52
    .local v0, "updateThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 53
    return-void
.end method
