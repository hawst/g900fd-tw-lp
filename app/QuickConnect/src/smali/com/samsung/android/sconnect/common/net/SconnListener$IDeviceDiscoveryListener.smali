.class public interface abstract Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
.super Ljava/lang/Object;
.source "SconnListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/SconnListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IDeviceDiscoveryListener"
.end annotation


# virtual methods
.method public abstract onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
.end method

.method public abstract onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
.end method

.method public abstract onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
.end method

.method public abstract onDiscoveryFinished()V
.end method

.method public abstract onDiscoveryStarted()V
.end method
