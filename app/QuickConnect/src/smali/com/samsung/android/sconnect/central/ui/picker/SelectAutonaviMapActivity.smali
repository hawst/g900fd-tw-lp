.class public Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
.super Landroid/app/Activity;
.source "SelectAutonaviMapActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Lcom/amap/api/services/geocoder/GeocodeSearch$OnGeocodeSearchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;,
        Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;,
        Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$CancelLocationUpdatesTask;
    }
.end annotation


# static fields
.field protected static final BUNDLE_KEY_LATITUDE:Ljava/lang/String; = "latitude"

.field protected static final BUNDLE_KEY_LOCATION:Ljava/lang/String; = "location"

.field protected static final BUNDLE_KEY_LONGITUDE:Ljava/lang/String; = "longitude"

.field protected static final BUNDLE_KEY_SNIPPET:Ljava/lang/String; = "snippet"

.field public static final INITIAL_PREACCURACY:F = 999.0f

.field public static final LOCATION_UPDATE_TIMEOUT:J = 0x4e20L

.field public static final LOCATION_UPDATE_TIMEOUT_ROAMING:J = 0xea60L

.field public static final MAX_TASK_COUNT:I = 0x63

.field private static final MENU_CANCEL:I = 0x65

.field private static final MENU_OK:I = 0x66

.field private static final OFFSET:I = 0x64

.field private static final TAG:Ljava/lang/String; = "SelectAutonaviMapActivity"

.field private static ZOOMIN_RATIO:F = 0.0f

.field private static ZOOMOUT_RATIO:F = 0.0f

.field private static final _a:D = 6378245.0

.field private static final _ee:D = 0.006693421622965943

.field static mBundle:Landroid/os/Bundle; = null

.field static mIsLongpress:Z = false

.field private static mLocationManager:Landroid/location/LocationManager; = null

.field private static mSearched:Z = false

.field private static final x_pi:D = 52.35987755982988


# instance fields
.field public OnMapLongClickListener:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarMenuCancel:Landroid/view/MenuItem;

.field private mActionBarMenuDone:Landroid/view/MenuItem;

.field private mAddressName:Ljava/lang/String;

.field mCameraPosition:Lcom/amap/api/maps/model/CameraPosition;

.field private mCity:Ljava/lang/String;

.field private mCurrentLocation:Landroid/widget/ImageButton;

.field private mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

.field private mGeocoder:Landroid/location/Geocoder;

.field private mGeocoderSearch:Lcom/amap/api/services/geocoder/GeocodeSearch;

.field mIsAutoNaviNlp:Z

.field mLatE6:D

.field private mLatitude:I

.field mLngE6:D

.field private mLoc_change_flag:Z

.field private mLocation:Ljava/lang/String;

.field private mLocationDisabledMsgDialog:Landroid/app/AlertDialog;

.field mLocationListener:Landroid/location/LocationListener;

.field private mLocationProvider:Ljava/lang/String;

.field private mLongitude:I

.field private mMap:Lcom/amap/api/maps/AMap;

.field private mMapView:Lcom/amap/api/maps/MapView;

.field private mMarker:Lcom/amap/api/maps/model/Marker;

.field private mMenuCancel:Landroid/widget/Button;

.field private mMenuDone:Landroid/widget/Button;

.field private mPreAccuracy:F

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mResearch:Z

.field private mSearchButton:Landroid/widget/ImageButton;

.field private mSearchText:Landroid/widget/EditText;

.field private mSearchView:Landroid/widget/SearchView;

.field mTaskEnabled:Z

.field private mTimer:Ljava/util/Timer;

.field mWgsLatE6:D

.field mWgsLngE6:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    const/high16 v0, 0x41500000    # 13.0f

    sput v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->ZOOMIN_RATIO:F

    .line 94
    const/high16 v0, 0x40e00000    # 7.0f

    sput v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->ZOOMOUT_RATIO:F

    .line 126
    sput-boolean v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearched:Z

    .line 131
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    .line 150
    sput-boolean v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsLongpress:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 98
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    .line 101
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMenuCancel:Landroid/widget/Button;

    .line 102
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMenuDone:Landroid/widget/Button;

    .line 107
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mResearch:Z

    .line 108
    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatitude:I

    .line 109
    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLongitude:I

    .line 116
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCameraPosition:Lcom/amap/api/maps/model/CameraPosition;

    .line 142
    const v0, 0x4479c000    # 999.0f

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mPreAccuracy:F

    .line 143
    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    .line 144
    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    .line 145
    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatE6:D

    .line 146
    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLngE6:D

    .line 148
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLoc_change_flag:Z

    .line 154
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsAutoNaviNlp:Z

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTaskEnabled:Z

    .line 160
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBarMenuCancel:Landroid/view/MenuItem;

    .line 161
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBarMenuDone:Landroid/view/MenuItem;

    .line 478
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationListener:Landroid/location/LocationListener;

    .line 589
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->OnMapLongClickListener:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    .line 853
    return-void
.end method

.method private RemoveListener(Z)V
    .locals 10
    .param p1, "drawMarker"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    .line 526
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoveListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    iput-boolean v8, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTaskEnabled:Z

    .line 530
    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    cmpl-double v1, v2, v6

    if-eqz v1, :cond_1

    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    cmpl-double v1, v2, v6

    if-eqz v1, :cond_1

    .line 531
    if-ne p1, v9, :cond_4

    .line 532
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->WGSToGCJ(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    .line 533
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 534
    .local v0, "wgsLatLng":Lcom/amap/api/maps/model/LatLng;
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->checkInChinaArea()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isLDUModel()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 535
    :cond_0
    new-instance v1, Lcom/amap/api/services/core/LatLonPoint;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/services/core/LatLonPoint;-><init>(DD)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getAddress(Lcom/amap/api/services/core/LatLonPoint;)V

    .line 548
    .end local v0    # "wgsLatLng":Lcom/amap/api/maps/model/LatLng;
    :cond_1
    :goto_0
    sget-object v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationListener:Landroid/location/LocationListener;

    if-eqz v1, :cond_2

    .line 549
    sget-object v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 551
    :cond_2
    return-void

    .line 537
    .restart local v0    # "wgsLatLng":Lcom/amap/api/maps/model/LatLng;
    :cond_3
    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;)V

    new-array v2, v9, [Lcom/amap/api/maps/model/LatLng;

    aput-object v0, v2, v8

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 540
    .end local v0    # "wgsLatLng":Lcom/amap/api/maps/model/LatLng;
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 541
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 542
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p1, "x1"    # I

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->setSearchButtonState(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->searchLocation()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/model/LatLng;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p1, "x1"    # Lcom/amap/api/maps/model/LatLng;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/AMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;DD)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p1, "x1"    # D
    .param p3, "x2"    # D

    .prologue
    .line 86
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->addMarker(DD)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mResearch:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$600()Landroid/location/LocationManager;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mPreAccuracy:F

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p1, "x1"    # F

    .prologue
    .line 86
    iput p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mPreAccuracy:F

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->RemoveListener(Z)V

    return-void
.end method

.method private addMarker(DD)V
    .locals 7
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    const/4 v6, 0x0

    .line 733
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addMarker ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    if-nez v2, :cond_0

    .line 736
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    const-string v4, "addMarker - mMap is null."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :goto_0
    return-void

    .line 740
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v2}, Lcom/amap/api/maps/AMap;->clear()V

    .line 741
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->setUpMapIfNeeded()V

    .line 742
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    new-instance v3, Lcom/amap/api/maps/model/MarkerOptions;

    invoke-direct {v3}, Lcom/amap/api/maps/model/MarkerOptions;-><init>()V

    new-instance v4, Lcom/amap/api/maps/model/LatLng;

    invoke-direct {v4, p1, p2, p3, p4}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v3, v4}, Lcom/amap/api/maps/model/MarkerOptions;->position(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v3

    const v4, 0x7f0200a0

    invoke-static {v4}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/amap/api/maps/AMap;->addMarker(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMarker:Lcom/amap/api/maps/model/Marker;

    .line 744
    iput-wide p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatE6:D

    .line 745
    iput-wide p3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLngE6:D

    .line 747
    const/4 v1, 0x0

    .line 749
    .local v1, "zoomLevel":F
    sget-boolean v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsLongpress:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v2}, Lcom/amap/api/maps/AMap;->getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 750
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v2}, Lcom/amap/api/maps/AMap;->getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v2

    iget v1, v2, Lcom/amap/api/maps/model/CameraPosition;->zoom:F

    .line 751
    :cond_1
    sput-boolean v6, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsLongpress:Z

    .line 753
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_2

    .line 754
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isLDUModel()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 755
    sget v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->ZOOMIN_RATIO:F

    .line 763
    :cond_2
    :goto_1
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addMarker - zoomLevel : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    new-instance v2, Lcom/amap/api/maps/model/CameraPosition$Builder;

    invoke-direct {v2}, Lcom/amap/api/maps/model/CameraPosition$Builder;-><init>()V

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    invoke-direct {v3, p1, p2, p3, p4}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v3}, Lcom/amap/api/maps/model/CameraPosition$Builder;->target(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/maps/model/CameraPosition$Builder;->build()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCameraPosition:Lcom/amap/api/maps/model/CameraPosition;

    .line 768
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCameraPosition:Lcom/amap/api/maps/model/CameraPosition;

    invoke-static {v2}, Lcom/amap/api/maps/CameraUpdateFactory;->newCameraPosition(Lcom/amap/api/maps/model/CameraPosition;)Lcom/amap/api/maps/CameraUpdate;

    move-result-object v0

    .line 769
    .local v0, "cameraUpdate":Lcom/amap/api/maps/CameraUpdate;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v2, v0}, Lcom/amap/api/maps/AMap;->animateCamera(Lcom/amap/api/maps/CameraUpdate;)V

    .line 770
    invoke-direct {p0, v6}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->showSoftKeyboard(Z)V

    goto/16 :goto_0

    .line 757
    .end local v0    # "cameraUpdate":Lcom/amap/api/maps/CameraUpdate;
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->checkInChinaArea()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 758
    sget v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->ZOOMIN_RATIO:F

    goto :goto_1

    .line 760
    :cond_4
    sget v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->ZOOMOUT_RATIO:F

    goto :goto_1
.end method

.method private convToDouble(I)D
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 1080
    int-to-double v0, p1

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private convToInt(D)I
    .locals 3
    .param p1, "d"    # D

    .prologue
    .line 1076
    const-wide v0, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v0

    return v0
.end method

.method private initAndStartAutonaviMap()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 196
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMapView:Lcom/amap/api/maps/MapView;

    invoke-virtual {v3, v5}, Lcom/amap/api/maps/MapView;->setVisibility(I)V

    .line 197
    new-instance v3, Lcom/amap/api/services/geocoder/GeocodeSearch;

    invoke-direct {v3, p0}, Lcom/amap/api/services/geocoder/GeocodeSearch;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGeocoderSearch:Lcom/amap/api/services/geocoder/GeocodeSearch;

    .line 198
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGeocoderSearch:Lcom/amap/api/services/geocoder/GeocodeSearch;

    invoke-virtual {v3, p0}, Lcom/amap/api/services/geocoder/GeocodeSearch;->setOnGeocodeSearchListener(Lcom/amap/api/services/geocoder/GeocodeSearch$OnGeocodeSearchListener;)V

    .line 200
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 201
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v7}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 202
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 204
    const v3, 0x7f0c0062

    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SearchView;

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchView:Landroid/widget/SearchView;

    .line 205
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3, v5}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 206
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchView:Landroid/widget/SearchView;

    const v4, 0x10000003

    invoke-virtual {v3, v4}, Landroid/widget/SearchView;->setImeOptions(I)V

    .line 207
    const-string v3, "search"

    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    .line 209
    .local v1, "searchManager":Landroid/app/SearchManager;
    new-instance v0, Landroid/content/ComponentName;

    const-string v3, "com.samsung.android.sconnect"

    const-string v4, "com.samsung.android.sconnect.central.ui.picker.SelectAutonaviMapActivity"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .local v0, "cn":Landroid/content/ComponentName;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1, v0}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 212
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 214
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "android:id/search_src_text"

    invoke-virtual {v3, v4, v6, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 216
    .local v2, "searchTextId":I
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3, v2}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    .line 218
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 219
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    new-instance v4, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 235
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_2

    .line 236
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    if-gtz v3, :cond_1

    .line 237
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 238
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 240
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$2;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    :cond_2
    const v3, 0x7f0c0063

    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCurrentLocation:Landroid/widget/ImageButton;

    .line 249
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCurrentLocation:Landroid/widget/ImageButton;

    new-instance v4, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275
    new-instance v3, Landroid/location/Geocoder;

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGeocoder:Landroid/location/Geocoder;

    .line 276
    iput-object v6, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCity:Ljava/lang/String;

    .line 278
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->clearFocus()V

    .line 279
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v7, :cond_3

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLoc_change_flag:Z

    if-nez v3, :cond_3

    .line 280
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->initSavedLocation()V

    .line 284
    :goto_0
    return-void

    .line 282
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->showMyLocation()V

    goto :goto_0
.end method

.method private initAutonaviMapManagers()V
    .locals 2

    .prologue
    .line 190
    const v0, 0x7f0c0064

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/MapView;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMapView:Lcom/amap/api/maps/MapView;

    .line 191
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMapView:Lcom/amap/api/maps/MapView;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/MapView;->onCreate(Landroid/os/Bundle;)V

    .line 192
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMapView:Lcom/amap/api/maps/MapView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/MapView;->setVisibility(I)V

    .line 193
    return-void
.end method

.method private initSavedLocation()V
    .locals 6

    .prologue
    .line 1095
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    const-string v4, "initSavedLocation()"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1096
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->setUpMapIfNeeded()V

    .line 1097
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1098
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatitude:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->convToDouble(I)D

    move-result-wide v2

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLongitude:I

    invoke-direct {p0, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->convToDouble(I)D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 1099
    .local v1, "mSavedWGS":Lcom/amap/api/maps/model/LatLng;
    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->WGSToGCJ(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    .line 1100
    .local v0, "mSavedGCJ":Lcom/amap/api/maps/model/LatLng;
    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->addMarker(DD)V

    .line 1101
    return-void
.end method

.method static outOfChina(Lcom/amap/api/maps/model/LatLng;)Z
    .locals 6
    .param p0, "loc"    # Lcom/amap/api/maps/model/LatLng;

    .prologue
    const/4 v0, 0x1

    .line 1104
    iget-wide v2, p0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    const-wide v4, 0x4052004189374bc7L    # 72.004

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v2, p0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    const-wide v4, 0x40613ab5dcc63f14L    # 137.8347

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    .line 1108
    :cond_0
    :goto_0
    return v0

    .line 1106
    :cond_1
    iget-wide v2, p0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide v4, 0x3fea89a027525461L    # 0.8293

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v2, p0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide v4, 0x404be9de69ad42c4L    # 55.8271

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 1108
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private searchLocation()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 554
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 555
    .local v0, "location":Ljava/lang/String;
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "searchLocation - location : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 558
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    const-string v3, "empay location to search"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mResearch:Z

    if-eqz v1, :cond_0

    .line 566
    :cond_2
    iput-boolean v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mResearch:Z

    .line 567
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;

    .line 569
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsAutoNaviNlp:Z

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->checkInChinaArea()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isLDUModel()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 570
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCity:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getAddressByName(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f090121

    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 576
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 577
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$8;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$8;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 572
    :cond_4
    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$SearchTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method private setSearchButtonState(I)V
    .locals 3
    .param p1, "length"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 287
    if-gtz p1, :cond_0

    .line 288
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 289
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 294
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 292
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    goto :goto_0
.end method

.method private setUpMap()V
    .locals 2

    .prologue
    .line 416
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->OnMapLongClickListener:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMap;->setOnMapLongClickListener(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V

    .line 417
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMap;->setMyLocationEnabled(Z)V

    .line 418
    return-void
.end method

.method private setUpMapIfNeeded()V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    if-nez v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMapView:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    .line 411
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->setUpMap()V

    .line 413
    :cond_0
    return-void
.end method

.method private showSoftKeyboard(Z)V
    .locals 8
    .param p1, "isShowing"    # Z

    .prologue
    const/4 v7, 0x0

    .line 1049
    const-string v3, "input_method"

    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 1050
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    const-string v3, "SelectAutonaviMapActivity"

    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showSoftKeyboard : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v2

    .line 1054
    .local v2, "mKeyboard":I
    if-eqz v2, :cond_0

    .line 1058
    const/4 p1, 0x0

    .line 1062
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1064
    .local v0, "focusView":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1067
    if-eqz p1, :cond_2

    .line 1068
    invoke-virtual {v1, v0, v7}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1073
    :cond_1
    :goto_0
    return-void

    .line 1070
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v1, v3, v7}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method private static tLat(DD)D
    .locals 10
    .param p0, "x"    # D
    .param p2, "y"    # D

    .prologue
    .line 1112
    const-wide/high16 v2, -0x3fa7000000000000L    # -100.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, p0

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    mul-double/2addr v4, p2

    add-double/2addr v2, v4

    const-wide v4, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v4, p2

    mul-double/2addr v4, p2

    add-double/2addr v2, v4

    const-wide v4, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v4, p0

    mul-double/2addr v4, p2

    add-double/2addr v4, v2

    const-wide v6, 0x3fc999999999999aL    # 0.2

    const-wide/16 v2, 0x0

    cmpl-double v2, p0, v2

    if-lez v2, :cond_0

    move-wide v2, p0

    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    mul-double/2addr v2, v6

    add-double v0, v4, v2

    .line 1114
    .local v0, "ret":D
    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    mul-double/2addr v4, p0

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, p0

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 1115
    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, p2

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4044000000000000L    # 40.0

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double v6, p2, v6

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 1116
    const-wide/high16 v2, 0x4064000000000000L    # 160.0

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    div-double v4, p2, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4074000000000000L    # 320.0

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, p2

    const-wide/high16 v8, 0x403e000000000000L    # 30.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 1117
    return-wide v0

    .line 1112
    .end local v0    # "ret":D
    :cond_0
    neg-double v2, p0

    goto :goto_0
.end method

.method private static tLon(DD)D
    .locals 10
    .param p0, "x"    # D
    .param p2, "y"    # D

    .prologue
    .line 1121
    const-wide v2, 0x4072c00000000000L    # 300.0

    add-double/2addr v2, p0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, p2

    add-double/2addr v2, v4

    const-wide v4, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v4, p0

    mul-double/2addr v4, p0

    add-double/2addr v2, v4

    const-wide v4, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v4, p0

    mul-double/2addr v4, p2

    add-double/2addr v4, v2

    const-wide v6, 0x3fb999999999999aL    # 0.1

    const-wide/16 v2, 0x0

    cmpl-double v2, p0, v2

    if-lez v2, :cond_0

    move-wide v2, p0

    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    mul-double/2addr v2, v6

    add-double v0, v4, v2

    .line 1123
    .local v0, "ret":D
    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    mul-double/2addr v4, p0

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, p0

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 1124
    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, p0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4044000000000000L    # 40.0

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double v6, p0, v6

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 1125
    const-wide v2, 0x4062c00000000000L    # 150.0

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    div-double v4, p0, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const-wide v4, 0x4072c00000000000L    # 300.0

    const-wide/high16 v6, 0x403e000000000000L    # 30.0

    div-double v6, p0, v6

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 1126
    return-wide v0

    .line 1121
    .end local v0    # "ret":D
    :cond_0
    neg-double v2, p0

    goto/16 :goto_0
.end method

.method private updateActionBar()V
    .locals 3

    .prologue
    .line 298
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_0

    .line 299
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 300
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBar:Landroid/app/ActionBar;

    const v2, 0x7f090121

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBar:Landroid/app/ActionBar;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 303
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBar:Landroid/app/ActionBar;

    const v2, 0x7f030005

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 304
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    .line 305
    .local v0, "customView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 306
    const v1, 0x7f0c0013

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMenuCancel:Landroid/widget/Button;

    .line 307
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMenuCancel:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$4;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 315
    const v1, 0x7f0c0014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMenuDone:Landroid/widget/Button;

    .line 316
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMenuDone:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$5;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private updateLocationInfo(DD)V
    .locals 7
    .param p1, "mGCJLatE6"    # D
    .param p3, "mGCJLngE6"    # D

    .prologue
    .line 1085
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLoc_change_flag:Z

    .line 1086
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;

    .line 1087
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 1088
    .local v0, "mTmpGCJ":Lcom/amap/api/maps/model/LatLng;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->GCJtoWGS(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    .line 1089
    .local v1, "mTmpWGS":Lcom/amap/api/maps/model/LatLng;
    iget-wide v2, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->convToInt(D)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatitude:I

    .line 1090
    iget-wide v2, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->convToInt(D)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLongitude:I

    .line 1091
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateLocationInfo "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatitude:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLongitude:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    return-void
.end method


# virtual methods
.method public BDtoGCJ(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;
    .locals 20
    .param p1, "bdLoc"    # Lcom/amap/api/maps/model/LatLng;

    .prologue
    .line 1158
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->outOfChina(Lcom/amap/api/maps/model/LatLng;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1168
    .end local p1    # "bdLoc":Lcom/amap/api/maps/model/LatLng;
    :goto_0
    return-object p1

    .line 1162
    .restart local p1    # "bdLoc":Lcom/amap/api/maps/model/LatLng;
    :cond_0
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    const-wide v16, 0x3f7a9fbe76c8b439L    # 0.0065

    sub-double v8, v14, v16

    .line 1163
    .local v8, "x":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide v16, 0x3f789374bc6a7efaL    # 0.006

    sub-double v10, v14, v16

    .line 1164
    .local v10, "y":D
    mul-double v14, v8, v8

    mul-double v16, v10, v10

    add-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    const-wide v16, 0x3ef4f8b588e368f1L    # 2.0E-5

    const-wide v18, 0x404a2e1077c7044eL    # 52.35987755982988

    mul-double v18, v18, v10

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    sub-double v12, v14, v16

    .line 1165
    .local v12, "z":D
    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v14

    const-wide v16, 0x3ec92a737110e454L    # 3.0E-6

    const-wide v18, 0x404a2e1077c7044eL    # 52.35987755982988

    mul-double v18, v18, v8

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    sub-double v6, v14, v16

    .line 1166
    .local v6, "theta":D
    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double v4, v12, v14

    .line 1167
    .local v4, "gcLocLon":D
    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double v2, v12, v14

    .line 1168
    .local v2, "gcLocLat":D
    new-instance p1, Lcom/amap/api/maps/model/LatLng;

    .end local p1    # "bdLoc":Lcom/amap/api/maps/model/LatLng;
    move-object/from16 v0, p1

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    goto :goto_0
.end method

.method public GCJtoBD(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;
    .locals 14
    .param p1, "gcLoc"    # Lcom/amap/api/maps/model/LatLng;

    .prologue
    .line 1172
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->outOfChina(Lcom/amap/api/maps/model/LatLng;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1180
    .end local p1    # "gcLoc":Lcom/amap/api/maps/model/LatLng;
    :goto_0
    return-object p1

    .line 1176
    .restart local p1    # "gcLoc":Lcom/amap/api/maps/model/LatLng;
    :cond_0
    iget-wide v2, p1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    .line 1177
    .local v2, "x":D
    iget-wide v4, p1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    .line 1178
    .local v4, "y":D
    mul-double v8, v2, v2

    mul-double v10, v4, v4

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    const-wide v10, 0x3ef4f8b588e368f1L    # 2.0E-5

    const-wide v12, 0x404a2e1077c7044eL    # 52.35987755982988

    mul-double/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    add-double v6, v8, v10

    .line 1179
    .local v6, "z":D
    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    const-wide v10, 0x3ec92a737110e454L    # 3.0E-6

    const-wide v12, 0x404a2e1077c7044eL    # 52.35987755982988

    mul-double/2addr v12, v2

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    add-double v0, v8, v10

    .line 1180
    .local v0, "theta":D
    new-instance p1, Lcom/amap/api/maps/model/LatLng;

    .end local p1    # "gcLoc":Lcom/amap/api/maps/model/LatLng;
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v8, v6

    const-wide v10, 0x3f789374bc6a7efaL    # 0.006

    add-double/2addr v8, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v10, v6

    const-wide v12, 0x3f7a9fbe76c8b439L    # 0.0065

    add-double/2addr v10, v12

    invoke-direct {p1, v8, v9, v10, v11}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    goto :goto_0
.end method

.method public GCJtoWGS(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;
    .locals 12
    .param p1, "gcLoc"    # Lcom/amap/api/maps/model/LatLng;

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 1145
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->outOfChina(Lcom/amap/api/maps/model/LatLng;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1154
    .end local p1    # "gcLoc":Lcom/amap/api/maps/model/LatLng;
    :goto_0
    return-object p1

    .line 1149
    .restart local p1    # "gcLoc":Lcom/amap/api/maps/model/LatLng;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->WGSToGCJ(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v4

    .line 1151
    .local v4, "tLoc":Lcom/amap/api/maps/model/LatLng;
    iget-wide v6, p1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    mul-double/2addr v6, v10

    iget-wide v8, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    sub-double v0, v6, v8

    .line 1152
    .local v0, "dLocLat":D
    iget-wide v6, p1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    mul-double/2addr v6, v10

    iget-wide v8, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    sub-double v2, v6, v8

    .line 1154
    .local v2, "dLocLon":D
    new-instance p1, Lcom/amap/api/maps/model/LatLng;

    .end local p1    # "gcLoc":Lcom/amap/api/maps/model/LatLng;
    invoke-direct {p1, v0, v1, v2, v3}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    goto :goto_0
.end method

.method public WGSToGCJ(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;
    .locals 18
    .param p1, "wgLoc"    # Lcom/amap/api/maps/model/LatLng;

    .prologue
    .line 1130
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->outOfChina(Lcom/amap/api/maps/model/LatLng;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1141
    .end local p1    # "wgLoc":Lcom/amap/api/maps/model/LatLng;
    :goto_0
    return-object p1

    .line 1133
    .restart local p1    # "wgLoc":Lcom/amap/api/maps/model/LatLng;
    :cond_0
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide v14, 0x4066800000000000L    # 180.0

    div-double/2addr v12, v14

    const-wide v14, 0x400921fb54442d18L    # Math.PI

    mul-double v10, v12, v14

    .line 1134
    .local v10, "radLat":D
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    const-wide v14, 0x3f7b6a8faf80ef0bL    # 0.006693421622965943

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    sub-double v6, v12, v14

    .line 1135
    .local v6, "magic":D
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    const-wide v14, 0x405a400000000000L    # 105.0

    sub-double/2addr v12, v14

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide v16, 0x4041800000000000L    # 35.0

    sub-double v14, v14, v16

    invoke-static {v12, v13, v14, v15}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->tLat(DD)D

    move-result-wide v2

    .line 1136
    .local v2, "dLat":D
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    const-wide v14, 0x405a400000000000L    # 105.0

    sub-double/2addr v12, v14

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide v16, 0x4041800000000000L    # 35.0

    sub-double v14, v14, v16

    invoke-static {v12, v13, v14, v15}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->tLon(DD)D

    move-result-wide v4

    .line 1137
    .local v4, "dLon":D
    const-wide v12, 0x4066800000000000L    # 180.0

    mul-double/2addr v12, v2

    const-wide v14, 0x41582b102de355c1L    # 6335552.717000426

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    mul-double v16, v16, v6

    div-double v14, v14, v16

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    mul-double v14, v14, v16

    div-double v2, v12, v14

    .line 1138
    const-wide v12, 0x4066800000000000L    # 180.0

    mul-double/2addr v12, v4

    const-wide v14, 0x415854c140000000L    # 6378245.0

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    div-double v14, v14, v16

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    mul-double v14, v14, v16

    div-double v4, v12, v14

    .line 1139
    new-instance v8, Lcom/amap/api/maps/model/LatLng;

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    add-double/2addr v12, v2

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    add-double/2addr v14, v4

    invoke-direct {v8, v12, v13, v14, v15}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .local v8, "mgLoc":Lcom/amap/api/maps/model/LatLng;
    move-object/from16 p1, v8

    .line 1141
    goto/16 :goto_0
.end method

.method checkInChinaArea()Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 785
    const/4 v0, 0x0

    .line 787
    .local v0, "isChinaArea":Z
    const-string v7, "gsm.operator.iso-country"

    const-string v8, ""

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 790
    .local v1, "isoCountryCode":Ljava/lang/String;
    const-string v7, "SelectAutonaviMapActivity"

    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "isoCountryCode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    const-string v7, "cn"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 792
    const/4 v0, 0x1

    .line 795
    :cond_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v4

    .line 797
    .local v4, "simSlotCount":I
    const/4 v5, 0x0

    .local v5, "tmpSlotCount":I
    :goto_0
    if-ge v5, v4, :cond_5

    .line 798
    invoke-static {v5}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v6

    .line 799
    .local v6, "tmpSubId":[J
    if-nez v6, :cond_2

    .line 800
    const-string v7, "SelectAutonaviMapActivity"

    const-string v8, ""

    const-string v9, "tmpSubId :is null "

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    :cond_1
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 802
    :cond_2
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    const-string v7, "gsm.operator.numeric"

    aget-wide v8, v6, v11

    const-string v10, ""

    invoke-static {v7, v8, v9, v10}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 804
    .local v2, "opNumSIM":Ljava/lang/String;
    const/4 v3, 0x0

    .line 806
    .local v3, "opNumSIMCode":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x5

    if-lt v7, v8, :cond_3

    .line 807
    const/4 v7, 0x3

    invoke-virtual {v2, v11, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 810
    :cond_3
    const-string v7, "SelectAutonaviMapActivity"

    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SlotCount : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " opNumSIM : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Simcode ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    const/16 v7, 0x1cc

    if-eq v3, v7, :cond_4

    const/16 v7, 0x1c7

    if-eq v3, v7, :cond_4

    const/16 v7, 0x1c6

    if-ne v3, v7, :cond_1

    .line 813
    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    .line 820
    .end local v2    # "opNumSIM":Ljava/lang/String;
    .end local v3    # "opNumSIMCode":I
    .end local v6    # "tmpSubId":[J
    :cond_5
    return v0
.end method

.method protected doFinish(Ljava/lang/String;)V
    .locals 5
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 1031
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, "doFinish"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "return result..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1033
    .local v0, "resultIntent":Landroid/content/Intent;
    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1034
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->setResult(ILandroid/content/Intent;)V

    .line 1039
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->finish()V

    .line 1040
    return-void

    .line 1036
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1037
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public getAddress(Lcom/amap/api/services/core/LatLonPoint;)V
    .locals 3
    .param p1, "latLonPoint"    # Lcom/amap/api/services/core/LatLonPoint;

    .prologue
    .line 633
    new-instance v0, Lcom/amap/api/services/geocoder/RegeocodeQuery;

    const/high16 v1, 0x43480000    # 200.0f

    const-string v2, "autonavi"

    invoke-direct {v0, p1, v1, v2}, Lcom/amap/api/services/geocoder/RegeocodeQuery;-><init>(Lcom/amap/api/services/core/LatLonPoint;FLjava/lang/String;)V

    .line 634
    .local v0, "query":Lcom/amap/api/services/geocoder/RegeocodeQuery;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGeocoderSearch:Lcom/amap/api/services/geocoder/GeocodeSearch;

    invoke-virtual {v1, v0}, Lcom/amap/api/services/geocoder/GeocodeSearch;->getFromLocationAsyn(Lcom/amap/api/services/geocoder/RegeocodeQuery;)V

    .line 635
    return-void
.end method

.method public getAddressByName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "searchText"    # Ljava/lang/String;
    .param p2, "city"    # Ljava/lang/String;

    .prologue
    .line 638
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAddressByName - city : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    new-instance v0, Lcom/amap/api/services/geocoder/GeocodeQuery;

    invoke-direct {v0, p1, p2}, Lcom/amap/api/services/geocoder/GeocodeQuery;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    .local v0, "query":Lcom/amap/api/services/geocoder/GeocodeQuery;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGeocoderSearch:Lcom/amap/api/services/geocoder/GeocodeSearch;

    invoke-virtual {v1, v0}, Lcom/amap/api/services/geocoder/GeocodeSearch;->getFromLocationNameAsyn(Lcom/amap/api/services/geocoder/GeocodeQuery;)V

    .line 642
    return-void
.end method

.method protected getURL(DD)Ljava/lang/String;
    .locals 3
    .param p1, "lat"    # D
    .param p3, "lng"    # D

    .prologue
    .line 1014
    const-string v0, ""

    .line 1015
    .local v0, "returnURL":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://mo.amap.com/?q="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1016
    return-object v0
.end method

.method markAddress(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 888
    const-string v4, "SelectAutonaviMapActivity"

    const-string v5, ""

    const-string v6, "markAddress"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 890
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09011d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 894
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    if-nez v4, :cond_2

    .line 895
    const-string v4, "SelectAutonaviMapActivity"

    const-string v5, ""

    const-string v6, "markAddress - mMap is null."

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    :goto_0
    return-void

    .line 900
    :cond_2
    if-eqz p1, :cond_e

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_e

    .line 901
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 903
    .local v0, "address":Landroid/location/Address;
    if-nez v0, :cond_3

    .line 904
    const-string v4, "SelectAutonaviMapActivity"

    const-string v5, ""

    const-string v6, "markAddress - address is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 908
    :cond_3
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v2

    .line 910
    .local v2, "index":I
    if-ltz v2, :cond_4

    invoke-virtual {v0, v8}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    .line 911
    :cond_4
    const-string v4, "SelectAutonaviMapActivity"

    const-string v5, ""

    const-string v6, "index is under zero or getAddressLine(0) is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 914
    :cond_5
    const-string v4, "SelectAutonaviMapActivity"

    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isAutoNaviNLP = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsAutoNaviNlp:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 917
    .local v3, "sb":Ljava/lang/StringBuilder;
    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsAutoNaviNlp:Z

    if-eqz v4, :cond_8

    if-lez v2, :cond_8

    .line 918
    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v1

    .line 919
    .local v1, "feature":Ljava/lang/String;
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 920
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 951
    .end local v1    # "feature":Ljava/lang/String;
    :cond_6
    :goto_1
    invoke-virtual {v0}, Landroid/location/Address;->hasLatitude()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v0}, Landroid/location/Address;->hasLongitude()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 952
    invoke-virtual {v0}, Landroid/location/Address;->getLatitude()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    .line 953
    invoke-virtual {v0}, Landroid/location/Address;->getLongitude()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    .line 955
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 956
    sget-boolean v4, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsLongpress:Z

    if-eqz v4, :cond_d

    .line 957
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->addMarker(DD)V

    .line 961
    :goto_2
    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    iget-wide v6, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->updateLocationInfo(DD)V

    .line 962
    const-string v4, "SelectAutonaviMapActivity"

    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "markAddress - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 964
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMarker:Lcom/amap/api/maps/model/Marker;

    invoke-virtual {v4, v9}, Lcom/amap/api/maps/model/Marker;->setDraggable(Z)V

    goto/16 :goto_0

    .line 923
    :cond_8
    packed-switch v2, :pswitch_data_0

    .line 943
    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 944
    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 925
    :pswitch_0
    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 926
    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 928
    :cond_9
    invoke-virtual {v0, v8}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 932
    :pswitch_1
    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 933
    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 935
    :cond_a
    invoke-virtual {v0, v9}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 936
    invoke-virtual {v0, v9}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 938
    :cond_b
    const-string v4, "SelectAutonaviMapActivity"

    const-string v5, ""

    const-string v6, "getAddressLine(1) is null."

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 946
    :cond_c
    invoke-virtual {v0, v8}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 959
    :cond_d
    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    iget-wide v6, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->addMarker(DD)V

    goto/16 :goto_2

    .line 966
    .end local v0    # "address":Landroid/location/Address;
    .end local v2    # "index":I
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_e
    const-string v4, "SelectAutonaviMapActivity"

    const-string v5, ""

    const-string v6, "markAddress - The size of addresses is 0"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 923
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1044
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1045
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 165
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 166
    const v0, 0x7f030015

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->setContentView(I)V

    .line 169
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBar:Landroid/app/ActionBar;

    .line 170
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->updateActionBar()V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.amap.android.location"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsAutoNaviNlp:Z

    .line 178
    sput-object p1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mBundle:Landroid/os/Bundle;

    .line 181
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->initAutonaviMapManagers()V

    .line 182
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->initAndStartAutonaviMap()V

    .line 187
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 972
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 973
    const/16 v0, 0x65

    const v1, 0x7f090089

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBarMenuCancel:Landroid/view/MenuItem;

    .line 974
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBarMenuCancel:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 977
    const/16 v0, 0x66

    const v1, 0x7f09008c

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBarMenuDone:Landroid/view/MenuItem;

    .line 978
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mActionBarMenuDone:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 981
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 369
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 370
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v2, "onDestroy()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearched:Z

    .line 373
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 378
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 381
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 382
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMarker:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_3

    .line 386
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMarker:Lcom/amap/api/maps/model/Marker;

    invoke-virtual {v0}, Lcom/amap/api/maps/model/Marker;->destroy()V

    .line 388
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGeocoder:Landroid/location/Geocoder;

    if-eqz v0, :cond_4

    .line 389
    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGeocoder:Landroid/location/Geocoder;

    .line 391
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMapView:Lcom/amap/api/maps/MapView;

    if-eqz v0, :cond_5

    .line 392
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMapView:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->onDestroy()V

    .line 394
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCameraPosition:Lcom/amap/api/maps/model/CameraPosition;

    if-eqz v0, :cond_6

    .line 395
    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCameraPosition:Lcom/amap/api/maps/model/CameraPosition;

    .line 396
    :cond_6
    return-void
.end method

.method public onGeocodeSearched(Lcom/amap/api/services/geocoder/GeocodeResult;I)V
    .locals 8
    .param p1, "result"    # Lcom/amap/api/services/geocoder/GeocodeResult;
    .param p2, "rCode"    # I

    .prologue
    const v7, 0x7f09011d

    const/4 v6, 0x0

    .line 645
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_0

    .line 646
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 647
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 650
    :cond_0
    if-nez p2, :cond_4

    .line 651
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/GeocodeResult;->getGeocodeAddressList()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/GeocodeResult;->getGeocodeAddressList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 654
    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/GeocodeResult;->getGeocodeAddressList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/geocoder/GeocodeAddress;

    .line 655
    .local v0, "address":Lcom/amap/api/services/geocoder/GeocodeAddress;
    invoke-virtual {v0}, Lcom/amap/api/services/geocoder/GeocodeAddress;->getProvince()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCity:Ljava/lang/String;

    .line 657
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    if-nez v2, :cond_1

    .line 658
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    const-string v4, "onGeocodeSearched - mMap is null."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    .end local v0    # "address":Lcom/amap/api/services/geocoder/GeocodeAddress;
    :goto_0
    return-void

    .line 661
    .restart local v0    # "address":Lcom/amap/api/services/geocoder/GeocodeAddress;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;

    invoke-virtual {v2}, Lcom/amap/api/maps/AMap;->clear()V

    .line 663
    invoke-virtual {v0}, Lcom/amap/api/services/geocoder/GeocodeAddress;->getLatLonPoint()Lcom/amap/api/services/core/LatLonPoint;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 664
    invoke-virtual {v0}, Lcom/amap/api/services/geocoder/GeocodeAddress;->getLatLonPoint()Lcom/amap/api/services/core/LatLonPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/services/core/LatLonPoint;->getLatitude()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    .line 665
    invoke-virtual {v0}, Lcom/amap/api/services/geocoder/GeocodeAddress;->getLatLonPoint()Lcom/amap/api/services/core/LatLonPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/services/core/LatLonPoint;->getLongitude()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    .line 670
    :goto_1
    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->addMarker(DD)V

    .line 672
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 673
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Lcom/amap/api/services/geocoder/GeocodeAddress;->getFormatAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 677
    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->updateLocationInfo(DD)V

    .line 679
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMarker:Lcom/amap/api/maps/model/Marker;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/amap/api/maps/model/Marker;->setDraggable(Z)V

    goto :goto_0

    .line 667
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    const-string v4, "onGeocodeSearched - address doesn\'t have latlng values."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 682
    .end local v0    # "address":Lcom/amap/api/services/geocoder/GeocodeAddress;
    :cond_3
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    const-string v4, "onGeocodeSearched - result is null or getGeocodeAddressList is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 689
    :cond_4
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onGeocodeSearched - rCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    const-wide/16 v6, 0x0

    .line 986
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, "onOptionsItemSelected"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "item: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1010
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 991
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->finish()V

    goto :goto_0

    .line 994
    :sswitch_1
    const-string v0, ""

    .line 996
    .local v0, "address":Ljava/lang/String;
    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatE6:D

    cmpl-double v2, v2, v6

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLngE6:D

    cmpl-double v2, v2, v6

    if-eqz v2, :cond_0

    .line 997
    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLatE6:D

    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLngE6:D

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getURL(DD)Ljava/lang/String;

    move-result-object v0

    .line 999
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1000
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, "mMenuDone"

    const-string v4, "address isEmpty"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09011f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1005
    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->doFinish(Ljava/lang/String;)V

    goto :goto_0

    .line 988
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x66 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 353
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 354
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v2, "onPause()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 365
    :cond_1
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 775
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 780
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->searchLocation()V

    .line 781
    const/4 v0, 0x0

    return v0
.end method

.method public onRegeocodeSearched(Lcom/amap/api/services/geocoder/RegeocodeResult;I)V
    .locals 7
    .param p1, "result"    # Lcom/amap/api/services/geocoder/RegeocodeResult;
    .param p2, "rCode"    # I

    .prologue
    const v6, 0x7f09011d

    const/4 v5, 0x0

    .line 696
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 697
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 698
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 702
    :cond_0
    if-nez p2, :cond_3

    .line 703
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/services/geocoder/RegeocodeAddress;->getFormatAddress()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 705
    sget-boolean v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsLongpress:Z

    if-nez v1, :cond_1

    .line 706
    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/services/geocoder/RegeocodeAddress;->getProvince()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCity:Ljava/lang/String;

    .line 707
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current city set : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mCity:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->addMarker(DD)V

    .line 711
    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/services/geocoder/RegeocodeAddress;->getFormatAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mAddressName:Ljava/lang/String;

    .line 713
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 714
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mAddressName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 715
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 716
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMarker:Lcom/amap/api/maps/model/Marker;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/Marker;->setDraggable(Z)V

    .line 717
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->updateLocationInfo(DD)V

    .line 730
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :goto_0
    return-void

    .line 719
    :cond_2
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    const-string v3, "onRegeocodeSearched - result is null or getGeocodeAddressList is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 726
    :cond_3
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onRegeocodeSearched - rCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 405
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 406
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 343
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 344
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v2, "onResume()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->setUpMapIfNeeded()V

    .line 349
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 400
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 401
    return-void
.end method

.method public showMyLocation()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 421
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v5, "showMyLocation"

    invoke-static {v0, v1, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_providers_allowed"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationProvider:Ljava/lang/String;

    .line 426
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    .line 429
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationProvider:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationProvider:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    :cond_0
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v2, " mLocationProvider is null "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :goto_0
    return-void

    .line 434
    :cond_1
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v5, "location is empty. Start updating."

    invoke-static {v0, v1, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const v0, 0x4479c000    # 999.0f

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mPreAccuracy:F

    .line 437
    iput-boolean v7, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTaskEnabled:Z

    .line 439
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_3

    .line 440
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v7, :cond_2

    .line 441
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v5, "showMyLocation - GPS updating..."

    invoke-static {v0, v1, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 445
    :cond_2
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v7, :cond_3

    .line 446
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v5, "showMyLocation - NETWORK updating..."

    invoke-static {v0, v1, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 452
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f09011e

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 453
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 454
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$6;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 465
    new-instance v6, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$CancelLocationUpdatesTask;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$CancelLocationUpdatesTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    .line 467
    .local v6, "timerTask":Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$CancelLocationUpdatesTask;
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;

    .line 468
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isRoaming(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 469
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v2, "showMyLocation - Roaming state"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v6, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0

    .line 472
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v6, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0
.end method
