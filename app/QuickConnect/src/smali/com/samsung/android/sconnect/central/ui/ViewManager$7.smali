.class Lcom/samsung/android/sconnect/central/ui/ViewManager$7;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/ViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$7;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 7
    .param p1, "btn"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x0

    .line 420
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 421
    .local v0, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    const-string v1, "ViewManager"

    const-string v2, "onCheckedChanged"

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$7;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$1000(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$7;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$1000(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 423
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$7;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$300(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$7;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$300(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0900c6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 425
    invoke-virtual {p1, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 429
    :goto_0
    return-void

    .line 427
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$7;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1, v0, p2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->onSelectionChanged(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0
.end method
