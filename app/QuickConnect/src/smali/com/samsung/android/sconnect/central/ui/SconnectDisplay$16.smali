.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showDisconnectDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 1777
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 1780
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothActionHelper()Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->disconnectAndUnbondDevice(Ljava/lang/String;)V

    .line 1785
    const-string v0, "SconnectDisplay"

    const-string v1, "DisconnectDialog onClick"

    const-string v2, "OK clicked, disconnect BT connection..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/16 v1, 0xbba

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;I)V

    .line 1788
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1789
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 1792
    :cond_0
    return-void
.end method
