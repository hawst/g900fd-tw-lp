.class Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "PrintPluginHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 314
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, "action":Ljava/lang/String;
    const-string v4, "PrintPluginHelper"

    const-string v5, "mPluginBroadcastReceiver"

    invoke-static {v4, v5, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string v4, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 318
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 320
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v4, 0xd

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 323
    .local v3, "p2pInfoWifi":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_0

    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 325
    const-string v4, "wifiP2pInfo"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pInfo;

    .line 327
    .local v2, "p2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    const-string v4, "PrintPluginHelper"

    const-string v5, "mPluginBroadcastReceiver"

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo$DetailedState;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    if-eqz v2, :cond_2

    iget-object v4, v2, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    if-eqz v4, :cond_2

    .line 330
    iget-object v4, v2, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v4}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 331
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    iget-object v5, v2, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v5}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    iget-object v7, v2, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v7}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$902(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 333
    const-string v4, "PrintPluginHelper"

    const-string v5, "mPluginBroadcastReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mP2pSubnetIP:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$900(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    .end local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "p2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    .end local v3    # "p2pInfoWifi":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return-void

    .line 335
    .restart local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    .restart local v2    # "p2pInfo":Landroid/net/wifi/p2p/WifiP2pInfo;
    .restart local v3    # "p2pInfoWifi":Landroid/net/NetworkInfo;
    :cond_1
    const-string v4, "PrintPluginHelper"

    const-string v5, "mPluginBroadcastReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "wrong Address:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v7}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;
    invoke-static {v4, v8}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$902(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 340
    :cond_2
    const-string v4, "PrintPluginHelper"

    const-string v5, "mPluginBroadcastReceiver"

    const-string v6, "p2pInfo or p2pInfo.groupOwnerAddress is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;
    invoke-static {v4, v8}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$902(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
