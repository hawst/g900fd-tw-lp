.class public Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.super Landroid/app/Activity;
.source "SconnectDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;
    }
.end annotation


# static fields
.field private static final DISCOVERY_LOCK_TIME:J = 0x1d4c0L

.field private static final EXTRA_CONTENTS_LIST:Ljava/lang/String; = "android.intent.extra.STREAM"

.field private static final EXTRA_FORWARD_LOCK:Ljava/lang/String; = "FORWARD_LOCK"

.field private static final MAXIMUM_NUMBER_OF_SEND_CONTENT:I = 0x1f4

.field public static final PICK_CHROMECAST:I = 0x3ef

.field public static final PICK_CROPPED_ICON:I = 0x3ed

.field public static final PICK_FAVORITE_ICON_IMAGE:I = 0x3ec

.field public static final PICK_FILE_SHARE:I = 0x3e9

.field public static final PICK_MEDIA_SHARE:I = 0x3ea

.field public static final PICK_PRINT:I = 0x3eb

.field public static final PICK_PROFILE_SHARE:I = 0x3ee

.field private static final SEND_CHECK_CONNECTION:I = 0x2

.field private static final SEND_DEFAULT:I = 0x0

.field private static final SEND_MULTIPLE:I = 0x1

.field public static SconnectDisplayActivitytoFinish:Landroid/app/Activity; = null

.field public static final TAG:Ljava/lang/String; = "SconnectDisplay"


# instance fields
.field private mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;

.field private final mBluetoothAdapterReceiver:Landroid/content/BroadcastReceiver;

.field private mBtSetting:I

.field private mBtTurnOn:Z

.field private mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

.field mCheckBoxListener:Landroid/view/View$OnClickListener;

.field private mContentRelatedAction:J

.field private mContentsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mContentsScheme:Ljava/lang/String;

.field private mContentsText:Ljava/lang/String;

.field private mContentsType:I

.field private mContext:Landroid/content/Context;

.field private mCropIconUri:Landroid/net/Uri;

.field public mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscardDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mDisconnectPairingDialog:Landroid/app/AlertDialog;

.field private mDownloadMgr:Lcom/samsung/android/sconnect/update/DownloadManager;

.field private mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

.field private mFavoriteDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

.field private mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

.field mGUIHandler:Landroid/os/Handler;

.field private mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

.field private mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

.field private mHidedDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

.field private mIsBluetoothAdapterReceiver:Z

.field private mIsContentsForwardLock:Z

.field private mIsContentsRelatedMode:Z

.field private mIsDiscovering:Z

.field private mIsPrintable:Z

.field private mIsShowPopup:Z

.field private mIsTerminatingCentralManager:Z

.field private mIsTvDiscovered:Z

.field private mIsWaitForDisconnect:Z

.field private mIsWaitForPrinting:Z

.field private mLastTime:J

.field private mLoadingDialog:Landroid/app/ProgressDialog;

.field private mLocalReceiver:Landroid/content/BroadcastReceiver;

.field private mMimeType:Ljava/lang/String;

.field private mMobileApEventReceiver:Landroid/content/BroadcastReceiver;

.field private mNetworkPermissionDialog:Landroid/app/AlertDialog;

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mPermissionBtCheckbox:Landroid/widget/CheckBox;

.field private mPermissionUserinfoCheckbox:Landroid/widget/CheckBox;

.field private mPermissionWifiCheckbox:Landroid/widget/CheckBox;

.field private mPickedContents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mPickedContentsType:I

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRegisterBroadcast:Z

.field private mSamsungAppsManager:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

.field private mSavedIntent:Landroid/content/Intent;

.field private mSelectedDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

.field private mSendErrorDialog:Landroid/app/AlertDialog;

.field private mSettingItemListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

.field private mSharableContentsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

.field private mStopSendingDialog:Landroid/app/AlertDialog;

.field private mTSDialog:Landroid/app/Dialog;

.field private mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

.field private mUriProcessor:Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

.field private mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

.field private mWifiSetting:I

.field private mWifiTurnOn:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 87
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionList:Ljava/util/ArrayList;

    .line 112
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .line 121
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsShowPopup:Z

    .line 122
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z

    .line 123
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z

    .line 124
    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtSetting:I

    .line 125
    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiSetting:I

    .line 127
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 129
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z

    .line 130
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z

    .line 131
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsPrintable:Z

    .line 132
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTerminatingCentralManager:Z

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDiscardDeviceList:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHidedDeviceList:Ljava/util/ArrayList;

    .line 140
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 147
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    .line 148
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;

    .line 149
    iput v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    .line 150
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsText:Ljava/lang/String;

    .line 151
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsScheme:Ljava/lang/String;

    .line 152
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    .line 154
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    .line 155
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;

    .line 156
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

    .line 157
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mUriProcessor:Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

    .line 158
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .line 160
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCropIconUri:Landroid/net/Uri;

    .line 162
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->NONE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 164
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    .line 170
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopSendingDialog:Landroid/app/AlertDialog;

    .line 171
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSendErrorDialog:Landroid/app/AlertDialog;

    .line 172
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDisconnectPairingDialog:Landroid/app/AlertDialog;

    .line 174
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 176
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForDisconnect:Z

    .line 178
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForPrinting:Z

    .line 180
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    .line 185
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    .line 187
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    .line 189
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z

    .line 588
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$6;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCheckBoxListener:Landroid/view/View$OnClickListener;

    .line 836
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1372
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$12;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSettingItemListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

    .line 1454
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$13;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1489
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$14;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 1842
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    .line 2744
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsBluetoothAdapterReceiver:Z

    .line 2745
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$24;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$24;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBluetoothAdapterReceiver:Landroid/content/BroadcastReceiver;

    .line 2794
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mRegisterBroadcast:Z

    .line 2829
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMobileApEventReceiver:Landroid/content/BroadcastReceiver;

    .line 2867
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 2951
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$27;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$27;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 3488
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mNetworkPermissionDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->unableToSendPopup(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    return v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionWifiCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->launchHelpApp()V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContainsDRMContents(Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->sendMultiple(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/util/ArrayList;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->sendWithCheckConnectionAndMax(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiSetting:I

    return v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showStopDialog()V

    return-void
.end method

.method static synthetic access$3100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHidedDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ActionFormatter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z

    return v0
.end method

.method static synthetic access$3502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z

    return p1
.end method

.method static synthetic access$3600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getDiscardedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDiscardDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateIfFavoriteDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateUselessDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeIfFavoriteDevice(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/TvController;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z

    return v0
.end method

.method static synthetic access$4402(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->checkChinaAndStart()V

    return-void
.end method

.method static synthetic access$4600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->terminateCentralManager()V

    return-void
.end method

.method static synthetic access$4700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4802(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$4900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mUriProcessor:Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionBtCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContentsType:I

    return v0
.end method

.method static synthetic access$5002(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContentsType:I

    return p1
.end method

.method static synthetic access$5100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showSendErrorDialog()V

    return-void
.end method

.method static synthetic access$5200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsPrintable:Z

    return v0
.end method

.method static synthetic access$5202(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsPrintable:Z

    return p1
.end method

.method static synthetic access$5300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCropIconUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$5302(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCropIconUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$5400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMobileApEventReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForDisconnect:Z

    return v0
.end method

.method static synthetic access$5502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForDisconnect:Z

    return p1
.end method

.method static synthetic access$5602(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # J

    .prologue
    .line 87
    iput-wide p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    return-wide p1
.end method

.method static synthetic access$5700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findFileShareItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "x2"    # Z

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    return-void
.end method

.method static synthetic access$5900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtSetting:I

    return v0
.end method

.method static synthetic access$6000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setP2pConnected(Z)V

    return-void
.end method

.method static synthetic access$6202(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForPrinting:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionUserinfoCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateCheck()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startSconnectDisplay()V

    return-void
.end method

.method private askForEmeeting(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)Z
    .locals 12
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3200
    .local p3, "contentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentSelected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3201
    const-string v0, "SconnectDisplay"

    const-string v1, "askForEmeeting"

    const-string v2, "contents not selected!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3202
    const/4 v0, 0x0

    .line 3245
    :goto_0
    return v0

    .line 3203
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3204
    :cond_1
    const-string v0, "SconnectDisplay"

    const-string v1, "askForEmeeting"

    const-string v2, "contents list is empty"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3205
    const/4 v0, 0x0

    goto :goto_0

    .line 3206
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    if-eqz v0, :cond_3

    .line 3207
    const-string v0, "SconnectDisplay"

    const-string v1, "askForEmeeting"

    const-string v2, "bIsContentsForwardLock"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3208
    const/4 v0, 0x0

    goto :goto_0

    .line 3210
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/Uri;

    .line 3211
    .local v9, "selectedUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v9, v1}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaFilepath(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 3212
    .local v8, "filepathUri":Landroid/net/Uri;
    if-nez v8, :cond_4

    .line 3213
    const-string v0, "SconnectDisplay"

    const-string v1, "askForEmeeting"

    const-string v2, "filepathUri is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3214
    const/4 v0, 0x0

    goto :goto_0

    .line 3216
    :cond_4
    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 3217
    .local v6, "contentsName":Ljava/lang/String;
    const-string v0, "text://"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "text://http"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3218
    const-string v0, "SconnectDisplay"

    const-string v1, "askForEmeeting"

    const-string v2, "selected contents is TEXT"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3219
    const/4 v0, 0x0

    goto :goto_0

    .line 3221
    :cond_5
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3222
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 3224
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3225
    .local v4, "selectedUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;)I

    move-result v5

    .line 3226
    .local v5, "contentType":I
    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3228
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090065

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090066

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090089

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$32;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$32;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f09008a

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;I)V

    invoke-virtual {v10, v11, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3245
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private askForGroupPlay(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Z)Z
    .locals 20
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I
    .param p4, "masterMode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 3134
    .local p3, "contentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentSelected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 3135
    const-string v2, "SconnectDisplay"

    const-string v3, "askForGroupPlay"

    const-string v4, "contents not selected!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3136
    const/4 v2, 0x0

    .line 3194
    :goto_0
    return v2

    .line 3137
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3138
    :cond_1
    const-string v2, "SconnectDisplay"

    const-string v3, "askForGroupPlay"

    const-string v4, "contents list is empty"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3139
    const/4 v2, 0x0

    goto :goto_0

    .line 3140
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    .line 3142
    const-string v2, "SconnectDisplay"

    const-string v3, "askForGroupPlay"

    const-string v4, "contents type is not media(image,audio,video)"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3143
    const/4 v2, 0x0

    goto :goto_0

    .line 3146
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/net/Uri;

    .line 3147
    .local v15, "selectedUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v15, v3}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaFilepath(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 3148
    .local v19, "filepathUri":Landroid/net/Uri;
    if-nez v19, :cond_4

    .line 3149
    const-string v2, "SconnectDisplay"

    const-string v3, "askForGroupPlay"

    const-string v4, "filepathUri is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3150
    const/4 v2, 0x0

    goto :goto_0

    .line 3153
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v15, v3}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaMime(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 3154
    .local v16, "mimeToPass":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v17

    .line 3155
    .local v17, "contentsName":Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3156
    .local v18, "file":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    .line 3158
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 3159
    .local v13, "selectedUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;)I

    move-result v7

    .line 3160
    .local v7, "contentType":I
    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3162
    new-instance v2, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f090062

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f090063

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v17, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f090089

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;

    move-object/from16 v3, p0

    move/from16 v4, p4

    move-object/from16 v5, p1

    move/from16 v6, p2

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;ZLcom/samsung/android/sconnect/common/device/SconnectDevice;II)V

    invoke-virtual {v8, v9, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f09008a

    new-instance v8, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$29;

    move-object/from16 v9, p0

    move/from16 v10, p4

    move-object/from16 v11, p1

    move/from16 v12, p2

    move v14, v7

    invoke-direct/range {v8 .. v16}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$29;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;ZLcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;ILandroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3194
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private askForTogether(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)Z
    .locals 12
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3250
    .local p3, "contentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentSelected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3251
    const-string v0, "SconnectDisplay"

    const-string v1, "askForTogether"

    const-string v2, "contents not selected!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3252
    const/4 v0, 0x0

    .line 3302
    :goto_0
    return v0

    .line 3253
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3254
    :cond_1
    const-string v0, "SconnectDisplay"

    const-string v1, "askForTogether"

    const-string v2, "contents list is empty"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3255
    const/4 v0, 0x0

    goto :goto_0

    .line 3256
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    if-eqz v0, :cond_3

    .line 3257
    const-string v0, "SconnectDisplay"

    const-string v1, "askForTogether"

    const-string v2, "bIsContentsForwardLock"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3258
    const/4 v0, 0x0

    goto :goto_0

    .line 3260
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/Uri;

    .line 3261
    .local v9, "selectedUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v9, v1}, Lcom/samsung/android/sconnect/common/util/UriUtil;->queryMediaFilepath(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 3262
    .local v8, "filepathUri":Landroid/net/Uri;
    if-nez v8, :cond_4

    .line 3263
    const-string v0, "SconnectDisplay"

    const-string v1, "askForTogether"

    const-string v2, "filepathUri is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3264
    const/4 v0, 0x0

    goto :goto_0

    .line 3266
    :cond_4
    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 3267
    .local v6, "contentsName":Ljava/lang/String;
    const-string v0, "text://"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "text://http"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3268
    const-string v0, "SconnectDisplay"

    const-string v1, "askForTogether"

    const-string v2, "selected contents is TEXT"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3269
    const/4 v0, 0x0

    goto :goto_0

    .line 3271
    :cond_5
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3272
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 3274
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3275
    .local v4, "selectedUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;)I

    move-result v5

    .line 3276
    .local v5, "contentType":I
    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3278
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090069

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09006a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090087

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$34;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$34;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f09008a

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;I)V

    invoke-virtual {v10, v11, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3302
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private checkChinaAndStart()V
    .locals 10

    .prologue
    const v9, 0x7f0900bd

    const v8, 0x7f0900bc

    const v7, 0x7f0900ba

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 451
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/CentralManager;->checkEnabledBt()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z

    .line 452
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/CentralManager;->checkEnabledWifi()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z

    .line 453
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isChinaNalSecurity()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 454
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "bluetooth_security_on_check"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtSetting:I

    .line 456
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wlan_permission_available"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiSetting:I

    .line 458
    const-string v1, "SconnectDisplay"

    const-string v2, "checkChinaAndStart"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ChinaNalSecurity(btSetting:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtSetting:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", wifiSetting:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiSetting:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsShowPopup:Z

    if-ne v1, v5, :cond_0

    .line 462
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showNetworkPermissionDialog()V

    .line 505
    :goto_0
    return-void

    .line 464
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "bluetooth_security_on_check"

    invoke-static {v1, v2, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 466
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wlan_permission_available"

    invoke-static {v1, v2, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 467
    const/4 v0, 0x0

    .line 468
    .local v0, "activateMsg":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z

    if-eqz v1, :cond_4

    .line 469
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-virtual {v2, v7}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 477
    :cond_1
    :goto_1
    if-eqz v0, :cond_3

    .line 478
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/Util;->isLocaleRTL()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 479
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u200f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 481
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 483
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startSconnectDisplay()V

    goto :goto_0

    .line 471
    :cond_4
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z

    if-eqz v1, :cond_5

    .line 472
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 473
    :cond_5
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z

    if-eqz v1, :cond_1

    .line 474
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 487
    .end local v0    # "activateMsg":Ljava/lang/String;
    :cond_6
    const/4 v0, 0x0

    .line 488
    .restart local v0    # "activateMsg":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z

    if-eqz v1, :cond_a

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z

    if-eqz v1, :cond_a

    .line 489
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-virtual {v2, v7}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 497
    :cond_7
    :goto_2
    if-eqz v0, :cond_9

    .line 498
    if-eqz v0, :cond_8

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/Util;->isLocaleRTL()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 499
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u200f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 501
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 503
    :cond_9
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startSconnectDisplay()V

    goto/16 :goto_0

    .line 491
    :cond_a
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z

    if-eqz v1, :cond_b

    .line 492
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 493
    :cond_b
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z

    if-eqz v1, :cond_7

    .line 494
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private checkDownloadables()V
    .locals 1

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->getManager(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSamsungAppsManager:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    .line 1345
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSamsungAppsManager:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->checkDownloadables()V

    .line 1346
    return-void
.end method

.method private checkNetworkToStart()V
    .locals 15

    .prologue
    const/4 v14, -0x1

    const/4 v2, 0x0

    .line 376
    const-string v0, "SconnectDisplay"

    const-string v3, "checkNetworkToStart"

    const-string v5, ""

    invoke-static {v0, v3, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    new-instance v0, Lcom/samsung/android/sconnect/central/CentralManager;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v6

    invoke-direct {v0, v3, v5, v6}, Lcom/samsung/android/sconnect/central/CentralManager;-><init>(Landroid/content/Context;Landroid/os/Handler;Landroid/app/LoaderManager;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    .line 381
    const/4 v12, -0x1

    .line 385
    .local v12, "securityMessage":I
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "false"

    aput-object v3, v4, v0

    .line 388
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v0, "content://com.sec.knox.provider/RestrictionPolicy4"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 389
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "isWifiEnabled"

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 391
    .local v13, "wifiCr":Landroid/database/Cursor;
    const-string v0, "content://com.sec.knox.provider/BluetoothPolicy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 392
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "isBluetoothEnabled"

    move-object v6, v1

    move-object v7, v2

    move-object v9, v2

    move-object v10, v2

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 395
    .local v11, "btCr":Landroid/database/Cursor;
    if-eqz v13, :cond_1

    .line 397
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 398
    const-string v0, "isWifiEnabled"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "false"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v2, 0x7f09019d

    invoke-virtual {v0, v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    .line 405
    :cond_0
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 408
    :cond_1
    if-eqz v11, :cond_3

    .line 410
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 411
    const-string v0, "isBluetoothEnabled"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "false"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_2

    .line 414
    if-ne v12, v14, :cond_4

    .line 415
    const v12, 0x7f09019b

    .line 422
    :cond_2
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 425
    :cond_3
    if-eq v12, v14, :cond_5

    .line 426
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090014

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090088

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$2;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$1;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 448
    :goto_1
    return-void

    .line 405
    :catchall_0
    move-exception v0

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v0

    .line 417
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v2, 0x7f09019f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v12

    goto :goto_0

    .line 422
    :catchall_1
    move-exception v0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v0

    .line 440
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->isEnableMobileAP(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 441
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showTetheringDialog()V

    goto :goto_1

    .line 442
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->isOxygenEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 443
    const-string v0, "SconnectDisplay"

    const-string v2, "checkNetworkToStart"

    const-string v3, "isOxygenEnabled, showOxygenDialog()"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showOxygenDialog()V

    goto :goto_1

    .line 446
    :cond_7
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->checkChinaAndStart()V

    goto :goto_1
.end method

.method private clearCache(Ljava/io/File;)V
    .locals 10
    .param p1, "dir"    # Ljava/io/File;

    .prologue
    .line 2676
    if-nez p1, :cond_0

    .line 2677
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExternalCacheDir()Ljava/io/File;

    move-result-object p1

    .line 2679
    :cond_0
    const/4 v3, 0x0

    .line 2680
    .local v3, "files":[Ljava/io/File;
    if-eqz p1, :cond_1

    .line 2681
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 2683
    :cond_1
    if-eqz v3, :cond_4

    .line 2685
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    :try_start_0
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_4

    aget-object v2, v0, v4

    .line 2686
    .local v2, "f":Ljava/io/File;
    if-eqz v2, :cond_2

    .line 2687
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2688
    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->clearCache(Ljava/io/File;)V

    .line 2685
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2690
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2694
    .end local v2    # "f":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_0
    move-exception v1

    .line 2695
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "SconnectDisplay"

    const-string v7, "clearCache"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2698
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    return-void
.end method

.method private disconnectConfirmPopup(Ljava/lang/String;I)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "action"    # I

    .prologue
    .line 3012
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-class v2, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3013
    .local v0, "intent":Landroid/content/Intent;
    const v1, 0x808000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3014
    const-string v1, "DEVICE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3015
    const-string v1, "ACT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3016
    const-string v1, "CONTINUE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3017
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivity(Landroid/content/Intent;)V

    .line 3018
    return-void
.end method

.method private findFileShareItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "mac"    # Ljava/lang/String;
    .param p3, "state"    # Ljava/lang/String;
    .param p4, "isFavorite"    # Z

    .prologue
    const/4 v1, 0x1

    .line 2974
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    .line 2975
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2976
    const-string v2, "SconnectDisplay"

    const-string v3, "findFileShareItem"

    const-string v4, "same mP2pMac"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2977
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2978
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateFileShareState(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2992
    :goto_0
    return v1

    .line 2980
    :cond_0
    const-string v2, "SconnectDisplay"

    const-string v3, "findFileShareItem"

    const-string v4, "not searched item. do not update sending view"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2983
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2984
    const-string v2, "SconnectDisplay"

    const-string v3, "findFileShareItem"

    const-string v4, "same mBtMac"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2985
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2986
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateFileShareState(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2988
    :cond_2
    const-string v2, "SconnectDisplay"

    const-string v3, "findFileShareItem"

    const-string v4, "not searched item. do not update sending view"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2992
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getDiscardedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 2170
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDiscardDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2171
    .local v0, "discard":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2175
    .end local v0    # "discard":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getHidedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 2179
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHidedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2180
    .local v0, "hidedItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2184
    .end local v0    # "hidedItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initializeDeviceList()V
    .locals 4

    .prologue
    .line 693
    const-string v1, "SconnectDisplay"

    const-string v2, "initializeDeviceList"

    const-string v3, " -- "

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 695
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 696
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 697
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDiscardDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 698
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHidedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 699
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->initDeviceView()V

    .line 700
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->queryFavoriteDevice()V

    .line 703
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 708
    :goto_0
    return-void

    .line 705
    :catch_0
    move-exception v0

    .line 706
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private isContainsDRMContents(Ljava/util/ArrayList;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3474
    .local p1, "contentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x0

    .line 3475
    .local v2, "result":Z
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;

    .line 3476
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 3477
    .local v3, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v4, v3}, Lcom/samsung/android/sconnect/common/util/UriUtil;->isAvailableForward(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    .line 3478
    .local v1, "isAvailable":Z
    if-eqz v1, :cond_0

    .line 3479
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3481
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 3484
    .end local v1    # "isAvailable":Z
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_1
    const-string v4, "SconnectDisplay"

    const-string v5, "isContainsDRMContents"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3485
    return v2
.end method

.method private isMirroring()Z
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getWfdUtil()Lcom/samsung/android/sconnect/common/util/WfdUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWfdConnected()Z

    move-result v0

    return v0
.end method

.method private isOutOfBounds(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 287
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v1, v3

    .line 288
    .local v1, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v2, v3

    .line 289
    .local v2, "y":I
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledWindowTouchSlop()I

    move-result v0

    .line 290
    .local v0, "slop":I
    neg-int v3, v0

    if-lt v1, v3, :cond_0

    neg-int v3, v0

    if-lt v2, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    if-gt v1, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, v0

    if-le v2, v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private launchHelpApp()V
    .locals 8

    .prologue
    .line 1272
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.samsung.helphub"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1273
    .local v1, "info":Landroid/content/pm/PackageInfo;
    const-string v4, "SconnectDisplay"

    const-string v5, "launchHelpApp"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Help versionCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v3, v4, 0xa

    .line 1276
    .local v3, "versionCode":I
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 1309
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    .end local v3    # "versionCode":I
    :cond_0
    :goto_0
    return-void

    .line 1281
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    .restart local v3    # "versionCode":I
    :cond_1
    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 1285
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1286
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v4, 0x24000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1287
    const-string v4, "helphub:section"

    const-string v5, "sconnect"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1288
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1289
    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1305
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "versionCode":I
    :catch_0
    move-exception v0

    .line 1306
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1291
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "info":Landroid/content/pm/PackageInfo;
    .restart local v3    # "versionCode":I
    :cond_2
    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 1299
    :try_start_1
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.helphub.HELP"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1300
    .restart local v2    # "intent":Landroid/content/Intent;
    const/high16 v4, 0x24000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1301
    const-string v4, "helphub:appid"

    const-string v5, "quick_connect"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1302
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1303
    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private loggingSurveyARTC()V
    .locals 7

    .prologue
    .line 631
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sconnect/common/util/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/samsung/android/sconnect/common/util/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 633
    .local v0, "isAllowToConnect":Z
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sconnect/common/util/Util;->KEY_CONTACT_ONLY:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/samsung/android/sconnect/common/util/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    .line 635
    .local v1, "isContactOnly":Z
    const/4 v2, -0x1

    .line 637
    .local v2, "visibilitySetting":I
    if-nez v0, :cond_0

    .line 638
    const/4 v2, 0x2

    .line 644
    :goto_0
    const-string v3, "SconnectDisplay"

    const-string v4, "loggingSurveyARTC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "visibilitySetting: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v4, "ARTC"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5, v2}, Lcom/samsung/android/sconnect/common/util/Util;->loggingSurvey(Landroid/content/Context;Ljava/lang/String;II)V

    .line 646
    return-void

    .line 639
    :cond_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 640
    const/4 v2, 0x1

    goto :goto_0

    .line 642
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onCropIconPickerResult(Landroid/content/Intent;)V
    .locals 2
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 2661
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$23;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$23;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2673
    return-void
.end method

.method private onFilePickerResult(Landroid/content/Intent;)V
    .locals 2
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 2337
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2416
    return-void
.end method

.method private onIconPickerResult(Landroid/content/Intent;)V
    .locals 2
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 2622
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$22;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2658
    return-void
.end method

.method private onMediaPickerResult(Landroid/content/Intent;I)V
    .locals 2
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "actionType"    # I

    .prologue
    .line 2419
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$19;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2455
    return-void
.end method

.method private onPrintPickerResult(Landroid/content/Intent;)V
    .locals 2
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 2492
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2619
    return-void
.end method

.method private onProfilePickerResult(Landroid/content/Intent;)V
    .locals 2
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 2458
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$20;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2489
    return-void
.end method

.method private registerBluetoothAdapterReceiver()V
    .locals 6

    .prologue
    .line 2767
    const-string v2, "SconnectDisplay"

    const-string v3, "registerBluetoothAdapterReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIsBluetoothAdapterReceiver = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsBluetoothAdapterReceiver:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2769
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsBluetoothAdapterReceiver:Z

    if-nez v2, :cond_0

    .line 2770
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 2771
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2773
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBluetoothAdapterReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2774
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsBluetoothAdapterReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2779
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 2775
    .restart local v1    # "intentFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    .line 2776
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SconnectDisplay"

    const-string v3, "registerBluetoothAdapterReceiver"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private registerBroadcast()V
    .locals 4

    .prologue
    .line 2797
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mRegisterBroadcast:Z

    if-nez v2, :cond_0

    .line 2798
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mRegisterBroadcast:Z

    .line 2799
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 2800
    .local v1, "localIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "com.samsung.android.sconnect.central.DISCONNECT_P2P"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2801
    const-string v2, "com.samsung.android.sconnect.central.PRINTER_FINISHED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2802
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 2805
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2806
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2807
    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2808
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2809
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2810
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2811
    const-string v2, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2812
    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2813
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 2814
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2817
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v1    # "localIntentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private removeIfFavoriteDevice(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z
    .locals 8
    .param p1, "removeDevice"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    const/4 v3, 0x0

    .line 2307
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2308
    .local v2, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    .line 2309
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2310
    const-string v4, "SconnectDisplay"

    const-string v5, "removeIfFavoriteDevice"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "It\'s favorite device: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2312
    invoke-virtual {v0, v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setSearched(Z)V

    .line 2313
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2314
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 2317
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2318
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->updateDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 2329
    :cond_2
    const/4 v3, 0x1

    .line 2332
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_3
    return v3
.end method

.method private resetNetworkSetting()V
    .locals 4

    .prologue
    .line 516
    const-string v0, "SconnectDisplay"

    const-string v1, "resetNetworkSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isChinaNalSecurity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isChinaNalSecurity()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isChinaNalSecurity()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "bluetooth_security_on_check"

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtSetting:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 521
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "wlan_permission_available"

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiSetting:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 523
    :cond_0
    return-void
.end method

.method private selectTvAppDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 4
    .param p1, "tvItem"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 3086
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09001d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const v3, 0x7f0900ca

    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0900cb

    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, -0x1

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$28;

    invoke-direct {v3, p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$28;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3100
    return-void
.end method

.method private sendMultiple(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    .local p2, "contentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/16 v5, 0x3e9

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1680
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;

    .line 1681
    const/4 v7, 0x0

    .line 1682
    .local v7, "disconnect":Z
    const/4 v11, 0x0

    .line 1683
    .local v11, "newClient":I
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1684
    .local v10, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v10}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    .line 1685
    .local v6, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1686
    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1687
    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_2

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-lez v0, :cond_2

    .line 1689
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/CentralManager;->checkConnectedDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1690
    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->unableToSendPopup(Ljava/lang/String;)V

    .line 1736
    .end local v6    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v10    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_1
    :goto_1
    return-void

    .line 1694
    .restart local v6    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .restart local v10    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/CentralManager;->unableToMirroring(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1695
    const/4 v7, 0x1

    .line 1702
    .end local v6    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v10    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_3
    if-eqz v7, :cond_6

    .line 1703
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentSelected()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1704
    invoke-direct {p0, v3, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto :goto_1

    .line 1697
    .restart local v6    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .restart local v10    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/CentralManager;->checkConnectedDevice(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1698
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 1706
    .end local v6    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v10    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_5
    invoke-direct {p0, v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto :goto_1

    .line 1711
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setP2pConnected()V

    .line 1713
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralManager;->getP2pConnectedDeviceCount()I

    move-result v0

    add-int/2addr v0, v11

    sget v1, Landroid/net/wifi/p2p/WifiP2pManager;->MAX_CLIENT_SUPPORT:I

    if-le v0, v1, :cond_7

    .line 1714
    const-string v0, "SconnectDisplay"

    const-string v1, "complete_btn"

    const-string v3, "MAX_DIRECT_CONNECTION"

    invoke-static {v0, v1, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1715
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const v3, 0x7f0900f2

    new-array v4, v4, [Ljava/lang/Object;

    sget v5, Landroid/net/wifi/p2p/WifiP2pManager;->MAX_CLIENT_SUPPORT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1721
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentSelected()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1722
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1723
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/lang/String;I)V

    .line 1725
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto :goto_1

    .line 1728
    :cond_8
    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1729
    new-instance v8, Landroid/content/Intent;

    const-class v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;

    invoke-direct {v8, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1731
    .local v8, "fileSharePickerIntent":Landroid/content/Intent;
    const-string v0, "request_code"

    invoke-virtual {v8, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1732
    const/high16 v0, 0x24000000

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1734
    invoke-virtual {p0, v8, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1
.end method

.method private sendWithCheckConnectionAndMax(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V
    .locals 7
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "contentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v6, 0x0

    .line 1645
    iput-object p3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;

    .line 1646
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    .line 1647
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1648
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-lez v1, :cond_0

    .line 1650
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/CentralManager;->checkConnectedDevice(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1651
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->unableToSendPopup(Ljava/lang/String;)V

    .line 1677
    :goto_0
    return-void

    .line 1656
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_1

    .line 1657
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/CentralManager;->unableToMirroring(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1658
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto :goto_0

    .line 1662
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/CentralManager;->needNewConnection(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1663
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto :goto_0

    .line 1667
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/CentralManager;->checkMaxDirectDevice(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1668
    const-string v1, "SconnectDisplay"

    const-string v2, "device_layout"

    const-string v3, "MAX_DIRECT_CONNECTION"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1669
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const v3, 0x7f0900f2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    sget v5, Landroid/net/wifi/p2p/WifiP2pManager;->MAX_CLIENT_SUPPORT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1676
    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method private setContentsInfo()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 649
    iput v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    .line 650
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    .line 651
    iput-boolean v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z

    .line 652
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 653
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 654
    .local v3, "uri":Landroid/net/Uri;
    if-nez v3, :cond_0

    .line 655
    const-string v4, "SconnectDisplay"

    const-string v5, "setContentsInfo"

    const-string v6, "mContentsList.get(0) is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    .end local v3    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 657
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 658
    .local v2, "path":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    .line 659
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getRelatedAction(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    .line 660
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsScheme:Ljava/lang/String;

    .line 662
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 663
    .local v0, "contentsSize":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 664
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 665
    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getRelatedAction(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    and-long/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    .line 666
    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 667
    const/16 v4, 0xd

    iput v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    .line 663
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 670
    :cond_2
    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    if-eqz v4, :cond_4

    .line 671
    iput-boolean v8, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z

    .line 672
    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    if-eq v4, v8, :cond_3

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_3

    .line 674
    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    const-wide/16 v6, -0x401

    and-long/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    .line 676
    :cond_3
    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    if-eqz v4, :cond_4

    .line 677
    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    const-wide/16 v6, -0x302

    and-long/2addr v4, v6

    iput-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    .line 681
    :cond_4
    const-string v4, "SconnectDisplay"

    const-string v5, "setContentsInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Scheme:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsScheme:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Type:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Size:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Action: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 688
    .end local v0    # "contentsSize":I
    .end local v1    # "i":I
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_5
    const-string v4, "SconnectDisplay"

    const-string v5, "setContentsInfo"

    const-string v6, "mContentsList is null / empty"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setP2pConnected()V
    .locals 2

    .prologue
    .line 1255
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    .line 1256
    .local v0, "mSConnectManager":Lcom/samsung/android/sconnect/SconnectManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1257
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->setP2pConnected(Z)V

    .line 1259
    :cond_0
    return-void
.end method

.method private setP2pConnected(Z)V
    .locals 2
    .param p1, "isConnected"    # Z

    .prologue
    .line 1263
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    .line 1264
    .local v0, "mSConnectManager":Lcom/samsung/android/sconnect/SconnectManager;
    if-eqz v0, :cond_0

    .line 1265
    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->setP2pConnected(Z)V

    .line 1267
    :cond_0
    return-void
.end method

.method private setViews()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 1350
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1351
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 1352
    .local v12, "size":I
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1353
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/Util;->countContact(Ljava/util/ArrayList;)I

    move-result v12

    .line 1355
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    invoke-virtual {v0, v1, v12}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getContentsString(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsText:Ljava/lang/String;

    .line 1359
    .end local v12    # "size":I
    :goto_0
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mOnClickListener:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSettingItemListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsText:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionList:Ljava/util/ArrayList;

    invoke-direct/range {v0 .. v11}, Lcom/samsung/android/sconnect/central/ui/ViewManager;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Landroid/widget/PopupMenu$OnMenuItemClickListener;Landroid/widget/AdapterView$OnItemClickListener;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/samsung/android/sconnect/common/util/GUIUtil;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .line 1363
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setViews()V

    .line 1364
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_1

    .line 1365
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    .line 1366
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0900fc

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1367
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v13}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1368
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v13}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1370
    :cond_1
    return-void

    .line 1357
    :cond_2
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsText:Ljava/lang/String;

    goto :goto_0
.end method

.method private showDisconnectDialog()V
    .locals 5

    .prologue
    .line 1774
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDisconnectPairingDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 1775
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090115

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090088

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$16;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090089

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDisconnectPairingDialog:Landroid/app/AlertDialog;

    .line 1795
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDisconnectPairingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1796
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDisconnectPairingDialog:Landroid/app/AlertDialog;

    const v1, 0x7f090116

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1798
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDisconnectPairingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1800
    :cond_1
    return-void
.end method

.method private showNetworkPermissionDialog()V
    .locals 5

    .prologue
    .line 526
    const-string v1, "SconnectDisplay"

    const-string v2, "showNetworkPermissionDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "btTurnOn:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBtTurnOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", wifiTurnOn:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mWifiTurnOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030013

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 531
    .local v0, "permissionDialog":Landroid/view/View;
    const v1, 0x7f0c005d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionWifiCheckbox:Landroid/widget/CheckBox;

    .line 532
    const v1, 0x7f0c005b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionBtCheckbox:Landroid/widget/CheckBox;

    .line 533
    const v1, 0x7f0c005f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionUserinfoCheckbox:Landroid/widget/CheckBox;

    .line 536
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionWifiCheckbox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCheckBoxListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionBtCheckbox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCheckBoxListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 538
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionUserinfoCheckbox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCheckBoxListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 540
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPermissionWifiCheckbox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v3, 0x7f090100

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setText(I)V

    .line 542
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090181

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090088

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$5;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090089

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$4;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$4;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$3;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mNetworkPermissionDialog:Landroid/app/AlertDialog;

    .line 583
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mNetworkPermissionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 584
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mNetworkPermissionDialog:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 586
    return-void
.end method

.method private showOxygenDialog()V
    .locals 3

    .prologue
    .line 3381
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900ef

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900ee

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090088

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$39;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$39;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3390
    return-void
.end method

.method private showSendErrorDialog()V
    .locals 3

    .prologue
    .line 1739
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSendErrorDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 1740
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900e5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900e6

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090088

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSendErrorDialog:Landroid/app/AlertDialog;

    .line 1745
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSendErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1746
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSendErrorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1748
    :cond_1
    return-void
.end method

.method private showStopDialog()V
    .locals 3

    .prologue
    .line 1751
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopSendingDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 1752
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900b6

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090086

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$15;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$15;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090087

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopSendingDialog:Landroid/app/AlertDialog;

    .line 1768
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopSendingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1769
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopSendingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1771
    :cond_1
    return-void
.end method

.method private showTetheringDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 3350
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 3351
    .local v2, "wifiManager":Landroid/net/wifi/WifiManager;
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 3352
    .local v0, "msg":Landroid/os/Message;
    const/4 v3, 0x3

    iput v3, v0, Landroid/os/Message;->what:I

    .line 3353
    invoke-virtual {v2, v0}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I

    move-result v1

    .line 3355
    .local v1, "num":I
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v5, 0x7f0900ea

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v5, 0x7f0900ed

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f090086

    new-instance v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$38;

    invoke-direct {v5, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$38;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f090087

    new-instance v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$37;

    invoke-direct {v5, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$37;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3378
    return-void
.end method

.method private startSconnectDisplay()V
    .locals 4

    .prologue
    .line 603
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothHelper()Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->getBluetoothAdapterState()I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    .line 605
    const-string v1, "SconnectDisplay"

    const-string v2, "startSconnectDisplay"

    const-string v3, "connectivity not ready - BT_STATE_TURNING_OFF"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->INITIATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 607
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->registerBluetoothAdapterReceiver()V

    .line 608
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showInitiatingView()V

    .line 627
    :goto_0
    return-void

    .line 611
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->unregisterBluetoothAdapterReceiver()V

    .line 612
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideInitiatingView()V

    .line 614
    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 616
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->initializeDeviceList()V

    .line 617
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->animateSearchingView()V

    .line 618
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/CentralManager;->startCentral(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 619
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->resetNetworkSetting()V

    .line 620
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->registerBroadcast()V

    .line 621
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/CentralManager;->getGUIStateListener()Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

    .line 622
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-interface {v1, v2}, Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;->onGUIStarted(Ljava/util/ArrayList;)V

    .line 624
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.APPLICATION_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 625
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->sendBroadcast(Landroid/content/Intent;)V

    .line 626
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->loggingSurveyARTC()V

    goto :goto_0
.end method

.method private startTutorial()V
    .locals 7

    .prologue
    .line 353
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideSearchingView()V

    .line 354
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 355
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v3, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 356
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 359
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 360
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "FORWARD_LOCK"

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 361
    const-string v3, "android.intent.extra.STREAM"

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 362
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 363
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 369
    return-void

    .line 365
    :catch_0
    move-exception v0

    .line 366
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SconnectDisplay"

    const-string v4, "startTutorial"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 4
    .param p1, "tv"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isWatchOn"    # Z

    .prologue
    .line 3103
    const-string v0, "SconnectDisplay"

    const-string v1, "startTvController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isWatchOn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    .line 3105
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 3106
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 3107
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideDeviceLayoutView()V

    .line 3108
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    if-nez v0, :cond_0

    .line 3109
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/TvController;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsText:Ljava/lang/String;

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/android/sconnect/central/ui/TvController;-><init>(Landroid/content/Context;Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    .line 3111
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/TvController;->actionTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 3112
    return-void
.end method

.method private terminateCentralManager()V
    .locals 1

    .prologue
    .line 776
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTerminatingCentralManager:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    if-eqz v0, :cond_0

    .line 777
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTerminatingCentralManager:Z

    .line 778
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralManager;->terminate()V

    .line 780
    :cond_0
    return-void
.end method

.method private unableToSendPopup(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 3021
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-class v2, Lcom/samsung/android/sconnect/central/ui/DisconnectDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3022
    .local v0, "intent":Landroid/content/Intent;
    const v1, 0x808000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3023
    const-string v1, "DEVICE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3024
    const-string v1, "CONTINUE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3025
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivity(Landroid/content/Intent;)V

    .line 3026
    return-void
.end method

.method private unregisterBluetoothAdapterReceiver()V
    .locals 5

    .prologue
    .line 2782
    const-string v1, "SconnectDisplay"

    const-string v2, "unregisterBluetoothAdapterReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIsBluetoothAdapterReceiver = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsBluetoothAdapterReceiver:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2784
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsBluetoothAdapterReceiver:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2786
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mBluetoothAdapterReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2787
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsBluetoothAdapterReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2792
    :cond_0
    :goto_0
    return-void

    .line 2788
    :catch_0
    move-exception v0

    .line 2789
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SconnectDisplay"

    const-string v2, "unregisterBluetoothAdapterReceiver"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private unregisterBroadcast()V
    .locals 2

    .prologue
    .line 2820
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mRegisterBroadcast:Z

    if-eqz v0, :cond_0

    .line 2821
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mRegisterBroadcast:Z

    .line 2822
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2823
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2824
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2827
    :cond_0
    return-void
.end method

.method private updateCheck()V
    .locals 20

    .prologue
    .line 1312
    const-wide/16 v2, 0x0

    .line 1313
    .local v2, "currentVersion":J
    const-string v15, "SconnectDisplay"

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 1314
    .local v14, "preference":Landroid/content/SharedPreferences;
    const-string v15, "versionCode"

    invoke-interface {v14, v15, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 1315
    .local v10, "marketVersion":J
    const-string v15, "lastCheckTime"

    const-wide/16 v16, 0x0

    invoke-interface/range {v14 .. v17}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 1316
    .local v8, "lastCheckTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    div-long v12, v16, v18

    .line 1317
    .local v12, "now":J
    sub-long v6, v12, v8

    .line 1319
    .local v6, "intervalTime":J
    const-string v15, "SconnectDisplay"

    const-string v16, "updateCheck"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "now : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", lastCheckTime : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ", interval : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/samsung/android/sconnect/update/DownloadManager;->getDownloadManager(Landroid/content/Context;)Lcom/samsung/android/sconnect/update/DownloadManager;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDownloadMgr:Lcom/samsung/android/sconnect/update/DownloadManager;

    .line 1325
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getPackageName()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    iget v15, v15, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v2, v15

    .line 1329
    :goto_0
    cmp-long v15, v2, v10

    if-gez v15, :cond_1

    .line 1330
    const-string v15, "SconnectDisplay"

    const-string v16, "updateCheck"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, " currentVersion: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " marketVersion: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1332
    new-instance v5, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-class v16, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    move-object/from16 v0, v16

    invoke-direct {v5, v15, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1333
    .local v5, "intent":Landroid/content/Intent;
    const/high16 v15, 0x24000000

    invoke-virtual {v5, v15}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1334
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v15, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1335
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDownloadMgr:Lcom/samsung/android/sconnect/update/DownloadManager;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sconnect/update/DownloadManager;->updateCheck(Z)V

    .line 1336
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->checkDownloadables()V

    .line 1341
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-void

    .line 1326
    :catch_0
    move-exception v4

    .line 1327
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v15, "SconnectDisplay"

    const-string v16, "updateCheck"

    const-string v17, "Can not get the version code"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1337
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const-wide/32 v16, 0x15180

    cmp-long v15, v6, v16

    if-gtz v15, :cond_2

    const-wide/16 v16, 0x0

    cmp-long v15, v8, v16

    if-nez v15, :cond_0

    .line 1338
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDownloadMgr:Lcom/samsung/android/sconnect/update/DownloadManager;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/sconnect/update/DownloadManager;->updateCheck(Z)V

    .line 1339
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->checkDownloadables()V

    goto :goto_1
.end method

.method private updateDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z
    .locals 11
    .param p1, "updateItem"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    const/4 v10, 0x0

    const/4 v5, 0x1

    .line 2188
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    .line 2189
    .local v4, "updateDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2190
    .local v2, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2191
    const-string v6, "SconnectDisplay"

    const-string v7, "updateDevice"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find update device "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2192
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v6

    if-gez v6, :cond_3

    .line 2193
    const-string v6, "SconnectDisplay"

    const-string v7, "updateDevice"

    const-string v8, "action is not avilable."

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2194
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2195
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2196
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 2197
    iput-object v10, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2199
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v6, v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->removeDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 2200
    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateUselessDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 2201
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2203
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2241
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_2
    :goto_0
    return v5

    .line 2206
    .restart local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_3
    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setDeviceView(Landroid/widget/LinearLayout;)V

    .line 2207
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 2208
    .local v3, "position":I
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2209
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2210
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v6

    if-nez v6, :cond_6

    .line 2211
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 2212
    iput-object v10, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2221
    :cond_4
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v6, v7, :cond_5

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2223
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 2224
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2226
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v6, p1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->updateDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto :goto_0

    .line 2213
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eq v6, v7, :cond_7

    .line 2215
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2216
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->expandActionList()V

    goto :goto_1

    .line 2218
    :cond_7
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    goto :goto_1

    .line 2231
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v3    # "position":I
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v6

    if-ltz v6, :cond_9

    .line 2232
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getHidedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    .line 2233
    .local v0, "hidedDevice":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    if-eqz v0, :cond_9

    .line 2234
    const-string v6, "SconnectDisplay"

    const-string v7, "updateDevice"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "find update device from hided list: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2236
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHidedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2237
    const/16 v6, 0xbb8

    invoke-virtual {p0, v6, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 2241
    .end local v0    # "hidedDevice":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method private updateFileShareState(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "mac"    # Ljava/lang/String;
    .param p3, "state"    # Ljava/lang/String;
    .param p4, "isFavorite"    # Z

    .prologue
    const/16 v2, 0xbbf

    .line 2997
    const-string v0, "STATUS_STARTED"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "STATUS_PROGRESS"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getState()I

    move-result v0

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_SENDING:I

    if-eq v0, v1, :cond_3

    .line 2999
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2

    if-nez p4, :cond_2

    .line 3000
    const/16 v0, 0xbb9

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 3001
    const/16 v0, 0xbbe

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 3009
    :cond_1
    :goto_0
    return-void

    .line 3003
    :cond_2
    invoke-virtual {p0, v2, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 3005
    :cond_3
    const-string v0, "STATUS_COMPLETED"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "STATUS_FAILED"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3006
    :cond_4
    invoke-virtual {p0, v2, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeGuiMessage(ILjava/lang/Object;)V

    .line 3007
    const/16 v0, 0xbc0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private updateIfFavoriteDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z
    .locals 9
    .param p1, "updateItem"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 2259
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    .line 2260
    .local v4, "updateDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2261
    .local v2, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    .line 2262
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2263
    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setDeviceView(Landroid/widget/LinearLayout;)V

    .line 2264
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 2265
    .local v3, "position":I
    const-string v5, "SconnectDisplay"

    const-string v6, "updateIfFavoriteDevice"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "It\'s favorite device: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2267
    invoke-virtual {v4, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->updateFavoriteDeviceInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 2268
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->getSelectedItem()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->getSelectedItem()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2270
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-virtual {v5, p1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->changeSelectedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 2272
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2273
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2274
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v5

    if-nez v5, :cond_5

    .line 2275
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 2276
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2285
    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v6, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2287
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 2288
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2290
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-virtual {v5, v4}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->updateFavorite(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 2292
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->isFavoriteViewEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2293
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    const/4 v6, -0x1

    invoke-virtual {v5, p1, v6}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2294
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showFavoriteView()V

    .line 2299
    :cond_4
    :goto_1
    const/4 v5, 0x1

    .line 2302
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v3    # "position":I
    :goto_2
    return v5

    .line 2277
    .restart local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .restart local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .restart local v3    # "position":I
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eq v5, v6, :cond_6

    .line 2279
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2280
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->expandActionList()V

    goto :goto_0

    .line 2282
    :cond_6
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    goto :goto_0

    .line 2297
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v5, p1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->updateDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto :goto_1

    .line 2302
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v3    # "position":I
    :cond_8
    const/4 v5, 0x0

    goto :goto_2
.end method

.method private updateUselessDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 2245
    const-string v0, "SconnectDisplay"

    const-string v1, "updateUselessDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Available action is not exist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2247
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z

    if-eqz v0, :cond_1

    .line 2248
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getHidedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2249
    const-string v0, "SconnectDisplay"

    const-string v1, "updateUselessDevice"

    const-string v2, "Add to hided list"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2250
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHidedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2256
    :cond_0
    :goto_0
    return-void

    .line 2252
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getDiscardedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2253
    const-string v0, "SconnectDisplay"

    const-string v1, "updateUselessDevice"

    const-string v2, "Add to discard list"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2254
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDiscardDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private verifyInputData(Landroid/content/Intent;)V
    .locals 7
    .param p1, "input"    # Landroid/content/Intent;

    .prologue
    .line 302
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/Util;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    if-eqz p1, :cond_6

    .line 305
    const-string v3, "android.intent.extra.INTENT"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 306
    .local v0, "originalIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 307
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    const-string v5, "change to originalIntent"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    move-object p1, v0

    .line 310
    :cond_0
    sget-boolean v3, Lcom/samsung/android/sconnect/common/util/DLog;->DEV_LOGGING_ON:Z

    if-eqz v3, :cond_1

    .line 311
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 312
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dump keySet : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :cond_1
    :goto_0
    const-string v3, "FORWARD_LOCK"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    .line 320
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bIsContentsForwardLock["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    .line 322
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mMimeType["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    .line 325
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    if-nez v3, :cond_5

    .line 326
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 327
    .local v2, "uri":Landroid/net/Uri;
    if-eqz v2, :cond_3

    .line 328
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get uri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    .line 330
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    .end local v0    # "originalIntent":Landroid/content/Intent;
    .end local v2    # "uri":Landroid/net/Uri;
    :goto_1
    return-void

    .line 315
    .restart local v0    # "originalIntent":Landroid/content/Intent;
    :cond_2
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    const-string v5, "dump keySet : nothing"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 333
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_3
    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 334
    .local v1, "text":Ljava/lang/CharSequence;
    if-eqz v1, :cond_4

    .line 335
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get CharSequence : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    .line 337
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "text://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    const-string v3, "text/sconnect"

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    .line 339
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 341
    :cond_4
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    const-string v5, "WARN - Extra is empty"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 345
    .end local v1    # "text":Ljava/lang/CharSequence;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_5
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 348
    .end local v0    # "originalIntent":Landroid/content/Intent;
    :cond_6
    const-string v3, "SconnectDisplay"

    const-string v4, "verifyInputData"

    const-string v5, "ERROR - Intent is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public buttonClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3127
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    if-eqz v0, :cond_0

    .line 3128
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/ui/TvController;->buttonClick(Landroid/view/View;)V

    .line 3130
    :cond_0
    return-void
.end method

.method public checkAndStartTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 9
    .param p1, "tv"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 3047
    const-string v4, "SconnectDisplay"

    const-string v5, "checkAndStartTvController"

    const-string v6, " -- "

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3049
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.yosemite.tab"

    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 3053
    .local v3, "isWatchOnInstalled":Z
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "tv.peel.smartremote.for.note4"

    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 3055
    .local v1, "isSmartRemoteInstalled":Z
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.yosemite.tab"

    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppDownloadable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 3059
    .local v2, "isWatchOnDownloadable":Z
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "tv.peel.smartremote.for.note4"

    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppDownloadable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 3062
    .local v0, "isSmartRemoteDownloadable":Z
    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    .line 3063
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->selectTvAppDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 3064
    const-string v4, "SconnectDisplay"

    const-string v5, "startTvController"

    const-string v6, "WatchOn & SmartRemote Installed"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3083
    :goto_2
    return-void

    .line 3049
    .end local v0    # "isSmartRemoteDownloadable":Z
    .end local v1    # "isSmartRemoteInstalled":Z
    .end local v2    # "isWatchOnDownloadable":Z
    .end local v3    # "isWatchOnInstalled":Z
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.watchon.phone"

    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    goto :goto_0

    .line 3055
    .restart local v1    # "isSmartRemoteInstalled":Z
    .restart local v3    # "isWatchOnInstalled":Z
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.watchon.phone"

    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppDownloadable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    goto :goto_1

    .line 3065
    .restart local v0    # "isSmartRemoteDownloadable":Z
    .restart local v2    # "isWatchOnDownloadable":Z
    :cond_2
    if-eqz v3, :cond_3

    .line 3066
    invoke-direct {p0, p1, v8}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 3067
    const-string v4, "SconnectDisplay"

    const-string v5, "startTvController"

    const-string v6, "WatchOn Installed"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 3068
    :cond_3
    if-eqz v1, :cond_4

    .line 3069
    invoke-direct {p0, p1, v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 3070
    const-string v4, "SconnectDisplay"

    const-string v5, "startTvController"

    const-string v6, "SmartRemote Installed"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 3071
    :cond_4
    if-eqz v2, :cond_5

    if-eqz v0, :cond_5

    .line 3072
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->selectTvAppDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 3073
    const-string v4, "SconnectDisplay"

    const-string v5, "startTvController"

    const-string v6, "WatchOn & SmartRemote Downloadable"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 3074
    :cond_5
    if-eqz v2, :cond_6

    .line 3075
    invoke-direct {p0, p1, v8}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 3076
    const-string v4, "SconnectDisplay"

    const-string v5, "startTvController"

    const-string v6, "WatchOn Downloadable"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 3077
    :cond_6
    if-eqz v0, :cond_7

    .line 3078
    invoke-direct {p0, p1, v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 3079
    const-string v4, "SconnectDisplay"

    const-string v5, "startTvController"

    const-string v6, "SmartRemote Downloadable"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 3081
    :cond_7
    const-string v4, "SconnectDisplay"

    const-string v5, "startTvController"

    const-string v6, "WatchOn & Smart remote not available"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public finishTvControl()V
    .locals 6

    .prologue
    .line 3029
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 3031
    const-wide/16 v0, 0x0

    .line 3032
    .local v0, "pausedTime":J
    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 3033
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    sub-long v0, v2, v4

    .line 3034
    const-wide/32 v2, 0x1d4c0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 3035
    const-string v2, "SconnectDisplay"

    const-string v3, "finishTvControl"

    const-string v4, "Exceed DISCOVERY_LOCK_TIME, refresh device list!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3036
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->refreshDiscoveryDevice()V

    .line 3038
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    .line 3040
    :cond_1
    const-string v2, "SconnectDisplay"

    const-string v3, "finishTvControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pausedTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3041
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 3042
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showDeviceLayoutView()V

    .line 3043
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->finishTvControl()V

    .line 3044
    return-void
.end method

.method public getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1

    .prologue
    .line 1443
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object v0
.end method

.method public getSelectedItem()Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1

    .prologue
    .line 1451
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object v0
.end method

.method public invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "action"    # I

    .prologue
    .line 887
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V

    .line 888
    return-void
.end method

.method public invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ILjava/util/ArrayList;)V
    .locals 17
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "action"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 891
    .local p3, "contentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    .line 892
    .local v4, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Action:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Name:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " NetType:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v12

    .line 895
    .local v12, "mSConnectManager":Lcom/samsung/android/sconnect/SconnectManager;
    packed-switch p2, :pswitch_data_0

    .line 1247
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 1248
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 1251
    .end local v4    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_0
    :goto_0
    return-void

    .line 897
    .restart local v4    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/CentralManager;->isConnectingState()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isP2PDevice()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isMyGroupDevice()Z

    move-result v3

    if-nez v3, :cond_1

    .line 899
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToConnectDialog(Ljava/lang/String;)V

    goto :goto_0

    .line 900
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    if-eqz v3, :cond_2

    .line 901
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const v5, 0x7f090114

    const/4 v6, 0x0

    invoke-static {v3, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 903
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v5, 0x1f4

    if-le v3, v5, :cond_3

    .line 905
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showMaximumSendDialog()V

    goto :goto_0

    .line 906
    :cond_3
    if-eqz p3, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContainsDRMContents(Ljava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 907
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v7, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v8, p2

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToSendDRMDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/util/ArrayList;III)V

    goto :goto_0

    .line 909
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setP2pConnected()V

    .line 910
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 912
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto :goto_0

    .line 916
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/CentralManager;->isConnectingState()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isP2PDevice()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isMyGroupDevice()Z

    move-result v3

    if-nez v3, :cond_5

    .line 918
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToConnectDialog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 920
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    if-eqz v3, :cond_6

    .line 921
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_7

    .line 922
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const v5, 0x7f0901a9

    const/4 v6, 0x1

    invoke-static {v3, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 929
    :cond_6
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setP2pConnected()V

    .line 930
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 931
    new-instance v11, Landroid/content/Intent;

    const-class v3, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 933
    .local v11, "fileSharePickerIntent":Landroid/content/Intent;
    const/high16 v3, 0x24000000

    invoke-virtual {v11, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 935
    const-string v3, "request_code"

    const/16 v5, 0x3e9

    invoke-virtual {v11, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 936
    const/16 v3, 0x3e9

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 925
    .end local v11    # "fileSharePickerIntent":Landroid/content/Intent;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const v5, 0x7f0901a8

    const/4 v6, 0x1

    invoke-static {v3, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 941
    :pswitch_3
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->checkAndStartTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto/16 :goto_0

    .line 945
    :pswitch_4
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_JOIN_TO_GROUP_PLAY"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.samsung.groupcast"

    invoke-static {v3, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 948
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isMirroring()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 949
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0xe

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 952
    :cond_8
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->askForGroupPlay(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 954
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceGroupPlay()Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;->mGpName:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceGroupPlay()Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;->mGpMac:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v3, v5, v6, v7, v8}, Lcom/samsung/android/sconnect/central/action/GroupPlayActionHelper;->launchGroupPlayAsSlave(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 957
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 961
    :cond_9
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.samsung.groupcast"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 967
    :pswitch_5
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getUpnpDeviceType()I

    move-result v16

    .line 968
    .local v16, "upnpDeviceType":I
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACTION_PLAY_CONTENT_VIA_ mContentsType["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], upnpDeviceType["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isMirroring()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 971
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0xa

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 973
    :cond_a
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v3

    const/16 v5, 0xd

    if-ne v3, v5, :cond_b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x3

    if-eq v3, v5, :cond_b

    .line 975
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "DLNA_AUDIO, go music selector directly."

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    new-instance v9, Landroid/content/Intent;

    const-string v3, "com.sec.android.music.intent.action.SOUND_PICKER"

    invoke-direct {v9, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 978
    .local v9, "audioIntent":Landroid/content/Intent;
    const-string v3, "vnd.android.cursor.dir/audio"

    invoke-virtual {v9, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 979
    const-string v3, "isMultiple"

    const/4 v5, 0x1

    invoke-virtual {v9, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 980
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;

    .line 981
    const/16 v3, 0x3ea

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 982
    .end local v9    # "audioIntent":Landroid/content/Intent;
    :cond_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x1

    if-eq v3, v5, :cond_c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x3

    if-eq v3, v5, :cond_c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_d

    .line 985
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    .line 986
    new-instance v3, Ljava/lang/Thread;

    new-instance v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v5, v0, v4, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$8;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)V

    invoke-direct {v3, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 996
    :cond_d
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 997
    new-instance v13, Landroid/content/Intent;

    const-class v3, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 999
    .local v13, "mediaSharePickerIntent":Landroid/content/Intent;
    const-string v3, "request_code"

    const/16 v5, 0x3ea

    invoke-virtual {v13, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1000
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;

    .line 1001
    const/16 v3, 0x3ea

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1007
    .end local v13    # "mediaSharePickerIntent":Landroid/content/Intent;
    .end local v16    # "upnpDeviceType":I
    :pswitch_6
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_PRINT"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/CentralManager;->isConnectingState()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isP2PDevice()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1009
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToConnectDialog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1010
    :cond_e
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isP2PDevice()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {v12}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v3

    if-eqz v3, :cond_f

    invoke-virtual {v12}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1012
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0xd

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1014
    :cond_f
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x1

    if-ne v3, v5, :cond_11

    .line 1015
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_PRINT - Print image"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v5, 0xa

    if-le v3, v5, :cond_10

    .line 1017
    new-instance v3, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0901a2

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v5, 0x7f0900d3

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/16 v8, 0xa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/16 v8, 0xa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v5, 0x7f090088

    new-instance v6, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$9;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$9;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v3, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1030
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v3}, Lcom/samsung/android/sconnect/SconnectManager;->setPrinterContext(Landroid/content/Context;)V

    .line 1031
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 1034
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/Util;->isPdfContents(Ljava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1035
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_PRINT - Print pdf"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x1

    if-le v3, v5, :cond_12

    .line 1038
    new-instance v3, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0900d5

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v5, 0x7f0900d4

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v5, 0x7f090088

    new-instance v6, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$11;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$11;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v3, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v5, 0x7f090089

    new-instance v6, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$10;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$10;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v3, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1065
    :cond_12
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_PRINT - Print image by plugin"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v3}, Lcom/samsung/android/sconnect/SconnectManager;->setPrinterContext(Landroid/content/Context;)V

    .line 1067
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 1075
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/CentralManager;->isConnectingState()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isP2PDevice()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1076
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToConnectDialog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1077
    :cond_13
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isP2PDevice()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-virtual {v12}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v3

    if-eqz v3, :cond_14

    invoke-virtual {v12}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 1079
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0xd

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1081
    :cond_14
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1082
    new-instance v14, Landroid/content/Intent;

    const-class v3, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1084
    .local v14, "printPickerIntent":Landroid/content/Intent;
    const-string v3, "request_code"

    const/16 v5, 0x3eb

    invoke-virtual {v14, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1085
    const/high16 v3, 0x24000000

    invoke-virtual {v14, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1087
    const/16 v3, 0x3eb

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1088
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "finish after print job - cancel"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1092
    .end local v14    # "printPickerIntent":Landroid/content/Intent;
    :pswitch_8
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isMirroring()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 1093
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_MIRROR_SCREEN, this is Mirroring connected device"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/CentralManager;->stopMirroring()V

    .line 1096
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->refreshDiscoveryDevice()V

    goto/16 :goto_0

    .line 1098
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/CentralManager;->isConnectingState()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isP2PDevice()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isMyGroupDevice()Z

    move-result v3

    if-nez v3, :cond_16

    .line 1100
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showUnableToConnectDialog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1101
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sconnect/central/CentralManager;->unableToMirroring(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1102
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1105
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    const/4 v5, 0x0

    const-string v6, ""

    move/from16 v0, p2

    invoke-virtual {v3, v4, v0, v5, v6}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;)Z

    .line 1106
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 1112
    :pswitch_9
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_SHARE_VIA_GROUP_PLAY"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1113
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.samsung.groupcast"

    invoke-static {v3, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 1115
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isMirroring()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 1116
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1119
    :cond_18
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->askForGroupPlay(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1121
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 1123
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 1127
    :cond_19
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.samsung.groupcast"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1133
    :pswitch_a
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_ASK_TO_EMEET"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.emeeting"

    invoke-static {v3, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.emeeting.b2c.hancom"

    invoke-static {v3, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 1138
    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isMirroring()Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 1139
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x11

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1141
    :cond_1b
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v4, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->askForEmeeting(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1142
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 1144
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 1148
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.emeeting"

    invoke-static {v3, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppDownloadable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 1150
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.sec.android.emeeting"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1152
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.emeeting.b2c.hancom"

    invoke-static {v3, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppDownloadable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1154
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.sec.android.emeeting.b2c.hancom"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1161
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.app.alltogether"

    invoke-static {v3, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 1163
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isMirroring()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 1164
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1167
    :cond_1e
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v4, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->askForTogether(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1168
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 1170
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 1174
    :cond_1f
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.sec.android.app.alltogether"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1180
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.app.alltogether"

    invoke-static {v3, v5}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 1182
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isMirroring()Z

    move-result v3

    if-eqz v3, :cond_20

    .line 1183
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x9

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1186
    :cond_20
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v4, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->askForTogether(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 1189
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 1193
    :cond_21
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "com.sec.android.app.alltogether"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1199
    :pswitch_d
    const/16 v3, 0xc

    move/from16 v0, p2

    if-ne v0, v3, :cond_23

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v3

    and-int/lit8 v3, v3, 0x4

    if-gtz v3, :cond_22

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v3

    and-int/lit8 v3, v3, 0x8

    if-lez v3, :cond_23

    :cond_22
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isConnected(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 1203
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    const-string v6, "ACTION_CONNECT, this is connected device"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mStopItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1205
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showDisconnectDialog()V

    goto/16 :goto_0

    .line 1207
    :cond_23
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 1209
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 1214
    :pswitch_e
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.settings.WIFI_SHARE_PROFILE"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1215
    .local v15, "profileShareIntent":Landroid/content/Intent;
    const/high16 v3, 0x4000000

    invoke-virtual {v15, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1217
    const/16 v3, 0x3ee

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1218
    :catch_0
    move-exception v10

    .line 1219
    .local v10, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACTION_PROFILE_SHARE: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const v5, 0x7f0900e6

    const/4 v6, 0x0

    invoke-static {v3, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1226
    .end local v10    # "e":Landroid/content/ActivityNotFoundException;
    .end local v15    # "profileShareIntent":Landroid/content/Intent;
    :pswitch_f
    const-string v3, "SconnectDisplay"

    const-string v5, "invokeAction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACTION_CHROMECAST mContentsType["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isMirroring()Z

    move-result v3

    if-eqz v3, :cond_24

    .line 1229
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x13

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1231
    :cond_24
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x1

    if-eq v3, v5, :cond_25

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x3

    if-eq v3, v5, :cond_25

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_26

    .line 1233
    :cond_25
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    move/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 1235
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 1237
    :cond_26
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1238
    new-instance v13, Landroid/content/Intent;

    const-class v3, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1240
    .restart local v13    # "mediaSharePickerIntent":Landroid/content/Intent;
    const-string v3, "request_code"

    const/16 v5, 0x3ef

    invoke-virtual {v13, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1241
    const/16 v3, 0x3ef

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 895
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_5
        :pswitch_3
        :pswitch_d
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_e
        :pswitch_f
        :pswitch_7
    .end packed-switch
.end method

.method public isConnected(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z
    .locals 3
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    const/4 v1, 0x0

    .line 3458
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    if-nez v2, :cond_1

    .line 3466
    :cond_0
    :goto_0
    return v1

    .line 3461
    :cond_1
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothActionHelper()Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    move-result-object v0

    .line 3463
    .local v0, "btActionHelper":Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    if-eqz v0, :cond_0

    .line 3466
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->isConnected(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isConnecting(Ljava/lang/String;)Z
    .locals 1
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    .line 3470
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/CentralManager;->isP2pConnectingDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isContentSelected()Z
    .locals 1

    .prologue
    .line 512
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContentsRelatedMode()Z
    .locals 1

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z

    return v0
.end method

.method public isDiscovering()Z
    .locals 1

    .prologue
    .line 1439
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z

    return v0
.end method

.method public notifyGuiMessage(I)V
    .locals 3
    .param p1, "message"    # I

    .prologue
    const/4 v2, 0x0

    .line 1803
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 1808
    :goto_0
    return-void

    .line 1806
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1807
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public notifyGuiMessage(ILjava/lang/Object;)V
    .locals 3
    .param p1, "message"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1811
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 1817
    :goto_0
    return-void

    .line 1814
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1815
    .local v0, "msg":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1816
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public notifyGuiMessage(ILjava/lang/Object;I)V
    .locals 4
    .param p1, "message"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "delay"    # I

    .prologue
    const/4 v2, 0x0

    .line 1820
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 1826
    :goto_0
    return-void

    .line 1823
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1824
    .local v0, "msg":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1825
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    int-to-long v2, p3

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 2702
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2703
    const-string v0, "SconnectDisplay"

    const-string v1, "onActivityResult"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", resultCode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2704
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->setDeviceSelectMode(Z)V

    .line 2705
    const/4 v0, -0x1

    if-ne v0, p2, :cond_2

    .line 2706
    if-eqz p3, :cond_1

    .line 2707
    packed-switch p1, :pswitch_data_0

    .line 2742
    :cond_0
    :goto_0
    return-void

    .line 2709
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2710
    invoke-direct {p0, p3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onFilePickerResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 2713
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2714
    const/16 v0, 0xa

    invoke-direct {p0, p3, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onMediaPickerResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 2717
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2718
    invoke-direct {p0, p3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onPrintPickerResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 2721
    :pswitch_3
    invoke-direct {p0, p3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onIconPickerResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 2724
    :pswitch_4
    invoke-direct {p0, p3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onCropIconPickerResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 2727
    :pswitch_5
    invoke-direct {p0, p3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onProfilePickerResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 2730
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2731
    const/16 v0, 0x13

    invoke-direct {p0, p3, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onMediaPickerResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 2737
    :cond_1
    const-string v0, "SconnectDisplay"

    const-string v1, "onActivityResult"

    const-string v2, "data is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2739
    :cond_2
    const/4 v0, 0x2

    if-ne v0, p2, :cond_0

    .line 2740
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showSendErrorDialog()V

    goto :goto_0

    .line 2707
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 819
    const-string v0, "SconnectDisplay"

    const-string v1, "onBackPressed"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SconnectLayout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->NONE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->INITIATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v0, v1, :cond_2

    .line 822
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    if-eqz v0, :cond_1

    .line 823
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 834
    :goto_0
    return-void

    .line 825
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto :goto_0

    .line 827
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v0, v1, :cond_3

    .line 828
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->endSelecteMode()V

    goto :goto_0

    .line 829
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v0, v1, :cond_4

    .line 830
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishTvControl()V

    goto :goto_0

    .line 832
    :cond_4
    const-string v0, "SconnectDisplay"

    const-string v1, "onBackPressed"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SconnectLayout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 3445
    const-string v1, "SconnectDisplay"

    const-string v2, "onConfigurationChanged"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[orientation]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " [keyboard]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/content/res/Configuration;->keyboard:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3448
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->dismissSettingMenu()Z

    move-result v0

    .line 3449
    .local v0, "showPopupMenu":Z
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setViews()V

    .line 3450
    if-eqz v0, :cond_0

    .line 3451
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showSettingMenu()V

    .line 3453
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setDisplaySize()V

    .line 3454
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3455
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x1

    .line 193
    const-string v0, "SconnectDisplay"

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 195
    iput-object p0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    .line 196
    sput-object p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->SconnectDisplayActivitytoFinish:Landroid/app/Activity;

    .line 198
    const-string v0, "SconnectDisplay"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 199
    .local v10, "preference":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isChinaNalSecurity()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    const-string v0, "PERMISSION_POPUP_CHECK"

    invoke-interface {v10, v0, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsShowPopup:Z

    .line 202
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsShowPopup:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isChinaNalSecurity()Z

    move-result v0

    if-nez v0, :cond_2

    .line 203
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateCheck()V

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v9

    .line 206
    .local v9, "mSConnectManager":Lcom/samsung/android/sconnect/SconnectManager;
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/SconnectManager;->isDialogShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 207
    const-string v0, "SconnectDisplay"

    const-string v1, "onCreate"

    const-string v2, "waiting/accept popup is showing. finish SconnectDisplay"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 250
    :cond_3
    :goto_0
    return-void

    .line 211
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 213
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/Util;->setSCRunningSetting(Landroid/content/Context;I)V

    .line 215
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;

    .line 216
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->verifyInputData(Landroid/content/Intent;)V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 218
    .local v7, "intentAction":Ljava/lang/String;
    if-eqz v7, :cond_6

    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 221
    :cond_5
    const-string v0, "SconnectDisplay"

    const-string v1, "onCreate"

    const-string v2, "It\'s called from Share via. do not save Intent"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSavedIntent:Landroid/content/Intent;

    .line 225
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setContentsInfo()V

    .line 226
    new-instance v0, Lcom/samsung/android/sconnect/central/ActionFormatter;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsList:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/Util;->isPrintableContents(Ljava/util/ArrayList;)Z

    move-result v3

    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentRelatedAction:J

    iget-boolean v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sconnect/central/ActionFormatter;-><init>(Landroid/content/Context;IZJZ)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;

    .line 229
    new-instance v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/android/sconnect/common/util/GUIUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceManager:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .line 230
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mUriProcessor:Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

    .line 231
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setViews()V

    .line 233
    const-string v0, "isTutorialMode"

    invoke-interface {v10, v0, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 235
    .local v8, "isTutorialMode":Z
    if-eqz v8, :cond_7

    .line 236
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startTutorial()V

    .line 241
    :goto_1
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->getInstance()Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    .line 242
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->initialize(Landroid/content/Context;Landroid/os/Handler;)V

    .line 244
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v0}, Lcom/samsung/android/sconnect/SconnectManager;->setPrinterContext(Landroid/content/Context;)V

    .line 246
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->isBmVersionDifferent(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 247
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const-string v1, "BeaconManager.apk"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->startActionInstallApk(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 238
    :cond_7
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->checkNetworkToStart()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 784
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 785
    const-string v0, "SconnectDisplay"

    const-string v1, "onDestroy"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    const/16 v0, 0x3e9

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishActivity(I)V

    .line 788
    const/16 v0, 0x3ea

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishActivity(I)V

    .line 789
    const/16 v0, 0x3ed

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishActivity(I)V

    .line 790
    const/16 v0, 0x3ef

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishActivity(I)V

    .line 791
    const/16 v0, 0x3ec

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishActivity(I)V

    .line 792
    const/16 v0, 0x3eb

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishActivity(I)V

    .line 793
    const/16 v0, 0x3ee

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishActivity(I)V

    .line 795
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->FINISHING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 797
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/Util;->getSCRunningSetting(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 798
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/Util;->setSCRunningSetting(Landroid/content/Context;I)V

    .line 801
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    .line 802
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

    if-eqz v0, :cond_1

    .line 803
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;

    invoke-interface {v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IGUIStateListener;->onGUIFinished()V

    .line 805
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->terminateCentralManager()V

    .line 806
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->unregisterBroadcast()V

    .line 807
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    if-eqz v0, :cond_2

    .line 808
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/TvController;->unregisterWatchonIntent()V

    .line 810
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    if-eqz v0, :cond_3

    .line 811
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->cancelHomeScreenCheck()V

    .line 813
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->resetNetworkSetting()V

    .line 814
    invoke-direct {p0, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->clearCache(Ljava/io/File;)V

    .line 815
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 254
    const-string v0, "SconnectDisplay"

    const-string v1, "onNewIntent"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 271
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 745
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 746
    const-string v0, "SconnectDisplay"

    const-string v1, "onPause"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v0, v1, :cond_1

    .line 748
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    .line 753
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    if-eqz v0, :cond_0

    .line 754
    const-string v0, "SconnectDisplay"

    const-string v1, "onPause"

    const-string v2, "mTvController pause"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/TvController;->unbindController()V

    .line 758
    :cond_0
    const/16 v0, 0xbc8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(I)V

    .line 759
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->dismissSettingMenu()Z

    .line 760
    return-void

    .line 750
    :cond_1
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 296
    const-string v0, "SconnectDisplay"

    const-string v1, "onPrepareOptionsMenu"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const/16 v0, 0xbc5

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;I)V

    .line 298
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 712
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 713
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForPrinting:Z

    if-eqz v2, :cond_0

    .line 714
    const-string v2, "SconnectDisplay"

    const-string v3, "onResume"

    const-string v4, "finish after print job"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 741
    :goto_0
    return-void

    .line 718
    :cond_0
    const-wide/16 v0, 0x0

    .line 719
    .local v0, "pausedTime":J
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 720
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    sub-long v0, v2, v4

    .line 721
    const-wide/32 v2, 0x1d4c0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 722
    const-string v2, "SconnectDisplay"

    const-string v3, "onResume"

    const-string v4, "Exceed DISCOVERY_LOCK_TIME, refresh device list!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->refreshDiscoveryDevice()V

    .line 725
    :cond_1
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    .line 729
    :cond_2
    :goto_1
    const-string v2, "SconnectDisplay"

    const-string v3, "onResume"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mPausedTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    if-eqz v2, :cond_3

    .line 732
    const-string v2, "SconnectDisplay"

    const-string v3, "onResume"

    const-string v4, "mTvController resume"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->bindController()V

    .line 735
    :cond_3
    const/16 v2, 0xbc9

    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(I)V

    .line 736
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    if-eqz v2, :cond_5

    .line 737
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/CentralManager;->pauseMirroring()V

    goto :goto_0

    .line 726
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v2, v3, :cond_2

    .line 727
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshDeviceList()V

    goto :goto_1

    .line 739
    :cond_5
    const-string v2, "SconnectDisplay"

    const-string v3, "onResume"

    const-string v4, "mCentralManager is NULL"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 764
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 765
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 769
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 770
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 275
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isOutOfBounds(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 277
    .local v0, "bIsOutOfBounds":Z
    const-string v1, "SconnectDisplay"

    const-string v2, "onTouchEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_DOWN: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 280
    const/4 v1, 0x1

    .line 283
    .end local v0    # "bIsOutOfBounds":Z
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public refreshDiscoveryDevice()V
    .locals 1

    .prologue
    .line 1419
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    if-nez v0, :cond_0

    .line 1430
    :goto_0
    return-void

    .line 1423
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->initializeDeviceList()V

    .line 1424
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->animateSearchingView()V

    .line 1425
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z

    if-eqz v0, :cond_1

    .line 1426
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralManager;->stopDiscoveryDevice()V

    .line 1429
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralManager;->startDiscoveryDevice()V

    goto :goto_0
.end method

.method public removeGuiMessage(I)V
    .locals 1
    .param p1, "message"    # I

    .prologue
    .line 1829
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1833
    :goto_0
    return-void

    .line 1832
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public removeGuiMessage(ILjava/lang/Object;)V
    .locals 1
    .param p1, "message"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1840
    :goto_0
    return-void

    .line 1839
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public setExpandedDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 0
    .param p1, "expandedDevice"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 1447
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1448
    return-void
.end method

.method public showAppDownloadDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 3332
    sget-object v2, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->DOWNLOADABLE_NAME:Ljava/util/HashMap;

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 3333
    .local v1, "appNameStringId":I
    const-string v2, "SconnectDisplay"

    const-string v3, "showAppDownloadDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "appNameStringId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3334
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3336
    .local v0, "appName":Ljava/lang/String;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const v4, 0x7f090191

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    const/4 v6, 0x2

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f090088

    new-instance v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$36;

    invoke-direct {v4, p0, p2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$36;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f090089

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3347
    return-void
.end method

.method public showMaximumSendDialog()V
    .locals 6

    .prologue
    .line 3315
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090197

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    const v2, 0x7f090196

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v5, 0x1f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090089

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090088

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$35;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$35;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3329
    return-void
.end method

.method public showTroubleShootingDialog()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3393
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTSDialog:Landroid/app/Dialog;

    if-nez v4, :cond_0

    .line 3394
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03000a

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 3396
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0c0034

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3397
    .local v0, "msg1":Landroid/widget/TextView;
    const v4, 0x7f0c0035

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 3398
    .local v1, "msg7":Landroid/widget/TextView;
    const v4, 0x7f0c0036

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 3399
    .local v2, "msg9":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v5, 0x7f090106

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 3400
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v5, 0x7f09010d

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 3401
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v5, 0x7f090110

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 3402
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f090104

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f090088

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTSDialog:Landroid/app/Dialog;

    .line 3406
    .end local v0    # "msg1":Landroid/widget/TextView;
    .end local v1    # "msg7":Landroid/widget/TextView;
    .end local v2    # "msg9":Landroid/widget/TextView;
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTSDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    .line 3407
    return-void
.end method

.method public showUnableToConnectDialog(Ljava/lang/String;)V
    .locals 5
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 3306
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900e5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v3, 0x7f09013b

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090088

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3312
    return-void
.end method

.method public showUnableToSendDRMDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Ljava/util/ArrayList;III)V
    .locals 10
    .param p1, "sendItem"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p3, "sendContentsType"    # I
    .param p4, "sendMode"    # I
    .param p5, "sendAction"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;III)V"
        }
    .end annotation

    .prologue
    .line 3412
    .local p2, "sendDeviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    const-string v0, "SconnectDisplay"

    const-string v1, "showUnableToSendDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3413
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090197

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090198

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090089

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    .line 3417
    .local v7, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3418
    const v0, 0x7f090199

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f090088

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;

    move-object v1, p0

    move v2, p4

    move-object v3, p2

    move-object v4, p1

    move v5, p5

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$40;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;ILjava/util/ArrayList;Lcom/samsung/android/sconnect/central/ui/DisplayItem;II)V

    invoke-virtual {v8, v9, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3440
    :cond_0
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3441
    return-void
.end method

.method public startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 3
    .param p1, "tv"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 3115
    const-string v0, "SconnectDisplay"

    const-string v1, "startTvController"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3116
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J

    .line 3117
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 3118
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 3119
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideDeviceLayoutView()V

    .line 3120
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    if-nez v0, :cond_0

    .line 3121
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/TvController;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsText:Ljava/lang/String;

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/android/sconnect/central/ui/TvController;-><init>(Landroid/content/Context;Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    .line 3123
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/ui/TvController;->actionTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 3124
    return-void
.end method

.method public stopDiscoveryDevice()V
    .locals 1

    .prologue
    .line 1433
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z

    if-eqz v0, :cond_0

    .line 1434
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralManager;->stopDiscoveryDevice()V

    .line 1436
    :cond_0
    return-void
.end method
