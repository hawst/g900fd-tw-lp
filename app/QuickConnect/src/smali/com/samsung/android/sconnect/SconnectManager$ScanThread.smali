.class Lcom/samsung/android/sconnect/SconnectManager$ScanThread;
.super Ljava/lang/Thread;
.source "SconnectManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/SconnectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScanThread"
.end annotation


# static fields
.field private static final DISCOVERY_THREAD_EXIT:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "ScanThread"


# instance fields
.field private mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sconnect/SconnectManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/SconnectManager;)V
    .locals 3

    .prologue
    .line 2195
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 2196
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "created!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2197
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 2198
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->setDaemon(Z)V

    .line 2199
    const-string v0, "SConnectScanThread"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->setName(Ljava/lang/String;)V

    .line 2200
    return-void
.end method

.method private processMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2251
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2316
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2253
    :pswitch_1
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "MSG_START_SCAN_UPNP_DEVICE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2254
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sconnect/SconnectManager;->mIsDiscovering:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$1602(Lcom/samsung/android/sconnect/SconnectManager;Z)Z

    .line 2255
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1700(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->checkEnabledNetwork()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mUpnpHelper:Lcom/samsung/android/sconnect/common/net/UpnpHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1700(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v1, v1, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    goto :goto_0

    .line 2258
    :cond_1
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "mUpnpHelper has no available network"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2263
    :pswitch_2
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "MSG_START_SCAN_PRINTER_DEVICE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2264
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1300(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->checkEnabledNetwork()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2265
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1300(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v1, v1, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    goto :goto_0

    .line 2267
    :cond_2
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "mPrintPluginHelper is not enabled!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2272
    :pswitch_3
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "MSG_START_SCAN_BLE_P2P_DEVICE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2273
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2274
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v1, v1, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    goto :goto_0

    .line 2279
    :pswitch_4
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "MSG_START_SCAN_BT_DEVICE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2280
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1800(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->checkEnabledNetwork()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2281
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mBTHelper:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1800(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v1, v1, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    goto/16 :goto_0

    .line 2283
    :cond_3
    const-string v0, "ScanThread"

    const-string v1, "ScanHandler"

    const-string v2, "mBTHelper is not enabled!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2288
    :pswitch_5
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "MSG_START_SCAN_WIFI_DEVICE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2289
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mWifiHelper:Lcom/samsung/android/sconnect/common/net/WifiHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$1900(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/WifiHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v1, v1, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    goto/16 :goto_0

    .line 2292
    :pswitch_6
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "MSG_START_SCAN_SLINK_DEVICE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2293
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getLocalIpAddress()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/SconnectManager;->mMyIpAddr:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$1502(Lcom/samsung/android/sconnect/SconnectManager;Ljava/lang/String;)Ljava/lang/String;

    .line 2294
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MSG_START_SCAN_SLINK_DEVICE - my IP [wlan0] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mMyIpAddr:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sconnect/SconnectManager;->access$1500(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2298
    :pswitch_7
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "MSG_START_SCAN_CHROMECAST_DEVICE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2299
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$2000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->checkEnabledNetwork()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2300
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mChromecastHelper:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$2000(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v1, v1, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V

    goto/16 :goto_0

    .line 2302
    :cond_4
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "mChromecastHelper is not enabled!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2306
    :pswitch_8
    const-string v0, "ScanThread"

    const-string v1, "ScanThread"

    const-string v2, "MSG_SCAN_FINISH"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2307
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/SconnectManager;->mIsDiscovering:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$1602(Lcom/samsung/android/sconnect/SconnectManager;Z)Z

    .line 2308
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    iget-object v0, v0, Lcom/samsung/android/sconnect/SconnectManager;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDiscoveryFinished()V

    .line 2309
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$2100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2310
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->setBtOppListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 2311
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$2100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->setBtOppListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    goto/16 :goto_0

    .line 2251
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_3
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public cleanMessages()V
    .locals 3

    .prologue
    .line 2227
    const-string v0, "ScanThread"

    const-string v1, "cleanMessages"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2228
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 2229
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .line 2235
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    .line 2236
    .local v1, "msg":Landroid/os/Message;
    const-string v2, "ScanThread"

    const-string v3, "run"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Message is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2238
    iget v2, v1, Landroid/os/Message;->what:I

    const/16 v3, 0x3e8

    if-ne v2, v3, :cond_0

    .line 2239
    const-string v2, "ScanThread"

    const-string v3, "run"

    const-string v4, "break while loop"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2248
    return-void

    .line 2243
    :cond_0
    invoke-direct {p0, v1}, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->processMessage(Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2244
    .end local v1    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 2245
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "ScanThread"

    const-string v3, "run"

    const-string v4, "InterruptedException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public declared-synchronized sendMessage(Landroid/os/Message;)I
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2203
    monitor-enter p0

    :try_start_0
    const-string v1, "ScanThread"

    const-string v2, "sendMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "msg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2205
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2210
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return v1

    .line 2206
    :catch_0
    move-exception v0

    .line 2207
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v1, "ScanThread"

    const-string v2, "sendMessage"

    const-string v3, "InterruptedException"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2208
    const/4 v1, -0x1

    goto :goto_0

    .line 2203
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public terminate()V
    .locals 5

    .prologue
    .line 2214
    const-string v2, "ScanThread"

    const-string v3, "terminate"

    const-string v4, "--"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2215
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 2217
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 2218
    .local v1, "msg":Landroid/os/Message;
    const/16 v2, 0x3e8

    iput v2, v1, Landroid/os/Message;->what:I

    .line 2220
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/SconnectManager$ScanThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2224
    :goto_0
    return-void

    .line 2221
    :catch_0
    move-exception v0

    .line 2222
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "ScanThread"

    const-string v3, "terminate"

    const-string v4, "InterruptedException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
