.class public final Lcom/samsung/android/sconnect/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_ic_menu_moreoverflow:I = 0x7f020000

.field public static final ab_ic_menu_refrash:I = 0x7f020001

.field public static final ab_ic_menu_rematching:I = 0x7f020002

.field public static final action_bar_back_btn:I = 0x7f020003

.field public static final action_bar_item_text_selector:I = 0x7f020004

.field public static final audio:I = 0x7f020005

.field public static final browser:I = 0x7f020006

.field public static final calendar:I = 0x7f020007

.field public static final camcorder:I = 0x7f020008

.field public static final change_icon_add_btn:I = 0x7f020009

.field public static final contact:I = 0x7f02000a

.field public static final default_selector:I = 0x7f02000b

.field public static final device_divider_selector:I = 0x7f02000c

.field public static final device_icon_bg_blue:I = 0x7f02000d

.field public static final device_icon_bg_orange:I = 0x7f02000e

.field public static final device_not_found_line_selector:I = 0x7f02000f

.field public static final device_not_found_text_selector:I = 0x7f020010

.field public static final easy_sconnect_ic_02_acc_01:I = 0x7f020011

.field public static final easy_sconnect_ic_02_acc_02:I = 0x7f020012

.field public static final easy_sconnect_ic_02_air_conditioner:I = 0x7f020013

.field public static final easy_sconnect_ic_02_blu_ray_player:I = 0x7f020014

.field public static final easy_sconnect_ic_02_camcorder:I = 0x7f020015

.field public static final easy_sconnect_ic_02_camera:I = 0x7f020016

.field public static final easy_sconnect_ic_02_chromecast:I = 0x7f020017

.field public static final easy_sconnect_ic_02_complete:I = 0x7f020018

.field public static final easy_sconnect_ic_02_dlna:I = 0x7f020019

.field public static final easy_sconnect_ic_02_dlna_audio:I = 0x7f02001a

.field public static final easy_sconnect_ic_02_dongle:I = 0x7f02001b

.field public static final easy_sconnect_ic_02_dvd:I = 0x7f02001c

.field public static final easy_sconnect_ic_02_general_device:I = 0x7f02001d

.field public static final easy_sconnect_ic_02_groupplay:I = 0x7f02001e

.field public static final easy_sconnect_ic_02_home_theater:I = 0x7f02001f

.field public static final easy_sconnect_ic_02_homesync:I = 0x7f020020

.field public static final easy_sconnect_ic_02_lfd:I = 0x7f020021

.field public static final easy_sconnect_ic_02_mobile:I = 0x7f020022

.field public static final easy_sconnect_ic_02_pc:I = 0x7f020023

.field public static final easy_sconnect_ic_02_printer:I = 0x7f020024

.field public static final easy_sconnect_ic_02_ref:I = 0x7f020025

.field public static final easy_sconnect_ic_02_robot_vc:I = 0x7f020026

.field public static final easy_sconnect_ic_02_stop:I = 0x7f020027

.field public static final easy_sconnect_ic_02_tablet:I = 0x7f020028

.field public static final easy_sconnect_ic_02_together:I = 0x7f020029

.field public static final easy_sconnect_ic_02_tv:I = 0x7f02002a

.field public static final easy_sconnect_ic_02_wearable:I = 0x7f02002b

.field public static final easy_sconnect_ic_02_wearable_circle:I = 0x7f02002c

.field public static final easy_sconnect_ic_02_wm:I = 0x7f02002d

.field public static final easy_sconnect_ic_acc_01:I = 0x7f02002e

.field public static final easy_sconnect_ic_acc_02:I = 0x7f02002f

.field public static final easy_sconnect_ic_air_conditioner:I = 0x7f020030

.field public static final easy_sconnect_ic_blu_ray_player:I = 0x7f020031

.field public static final easy_sconnect_ic_camcorder:I = 0x7f020032

.field public static final easy_sconnect_ic_camera:I = 0x7f020033

.field public static final easy_sconnect_ic_chromecast:I = 0x7f020034

.field public static final easy_sconnect_ic_complete:I = 0x7f020035

.field public static final easy_sconnect_ic_dlna:I = 0x7f020036

.field public static final easy_sconnect_ic_dlna_audio:I = 0x7f020037

.field public static final easy_sconnect_ic_dongle:I = 0x7f020038

.field public static final easy_sconnect_ic_dvd:I = 0x7f020039

.field public static final easy_sconnect_ic_general_device:I = 0x7f02003a

.field public static final easy_sconnect_ic_groupplay:I = 0x7f02003b

.field public static final easy_sconnect_ic_home_theater:I = 0x7f02003c

.field public static final easy_sconnect_ic_homesync:I = 0x7f02003d

.field public static final easy_sconnect_ic_lfd:I = 0x7f02003e

.field public static final easy_sconnect_ic_mobile:I = 0x7f02003f

.field public static final easy_sconnect_ic_pc:I = 0x7f020040

.field public static final easy_sconnect_ic_printer:I = 0x7f020041

.field public static final easy_sconnect_ic_ref:I = 0x7f020042

.field public static final easy_sconnect_ic_robot_vc:I = 0x7f020043

.field public static final easy_sconnect_ic_stop:I = 0x7f020044

.field public static final easy_sconnect_ic_tablet:I = 0x7f020045

.field public static final easy_sconnect_ic_together:I = 0x7f020046

.field public static final easy_sconnect_ic_tv:I = 0x7f020047

.field public static final easy_sconnect_ic_wearable:I = 0x7f020048

.field public static final easy_sconnect_ic_wearable_circle:I = 0x7f020049

.field public static final easy_sconnect_ic_wm:I = 0x7f02004a

.field public static final expand_btn_selector:I = 0x7f02004b

.field public static final expand_down_btn:I = 0x7f02004c

.field public static final expand_up_btn:I = 0x7f02004d

.field public static final files:I = 0x7f02004e

.field public static final handler_icon_selector:I = 0x7f02004f

.field public static final help_btn_selector:I = 0x7f020050

.field public static final ic_launcher:I = 0x7f020051

.field public static final images:I = 0x7f020052

.field public static final location:I = 0x7f020053

.field public static final memo:I = 0x7f020054

.field public static final message_add_text_templates:I = 0x7f020055

.field public static final message_attach_location_amap:I = 0x7f020056

.field public static final overlay_help_button_focus:I = 0x7f020057

.field public static final overlay_help_button_normal:I = 0x7f020058

.field public static final overlay_help_button_pressed:I = 0x7f020059

.field public static final record_audio:I = 0x7f02005a

.field public static final remote_123_botton:I = 0x7f02005b

.field public static final remote_button_selector:I = 0x7f02005c

.field public static final remote_ch_botton:I = 0x7f02005d

.field public static final remote_down_button_selector:I = 0x7f02005e

.field public static final remote_mute_botton:I = 0x7f02005f

.field public static final remote_power_botton:I = 0x7f020060

.field public static final remote_power_button_selector:I = 0x7f020061

.field public static final remote_source_botton:I = 0x7f020062

.field public static final remote_up_button_selector:I = 0x7f020063

.field public static final remote_vol_botton:I = 0x7f020064

.field public static final s_note:I = 0x7f020065

.field public static final sconnect_action_ic_more_feature:I = 0x7f020066

.field public static final sconnect_action_ic_play:I = 0x7f020067

.field public static final sconnect_badge_ic_bg:I = 0x7f020068

.field public static final sconnect_badge_ic_bluetooth:I = 0x7f020069

.field public static final sconnect_badge_ic_connect:I = 0x7f02006a

.field public static final sconnect_badge_ic_nonconnect:I = 0x7f02006b

.field public static final sconnect_badge_ic_wifi:I = 0x7f02006c

.field public static final sconnect_bottom_down:I = 0x7f02006d

.field public static final sconnect_bottom_down_focus:I = 0x7f02006e

.field public static final sconnect_bottom_down_press:I = 0x7f02006f

.field public static final sconnect_ic_add:I = 0x7f020070

.field public static final sconnect_icon_bg_mask_pro:I = 0x7f020071

.field public static final sconnect_icon_popup_check:I = 0x7f020072

.field public static final sconnect_list_ic_connect:I = 0x7f020073

.field public static final sconnect_list_ic_emeeting:I = 0x7f020074

.field public static final sconnect_list_ic_explore_content:I = 0x7f020075

.field public static final sconnect_list_ic_jointogether:I = 0x7f020076

.field public static final sconnect_list_ic_mirror_screen_on_homesync_mirroring_device:I = 0x7f020077

.field public static final sconnect_list_ic_mirror_screen_tv:I = 0x7f020078

.field public static final sconnect_list_ic_more_feature:I = 0x7f020079

.field public static final sconnect_list_ic_play_content_pc:I = 0x7f02007a

.field public static final sconnect_list_ic_print_via_action:I = 0x7f02007b

.field public static final sconnect_list_ic_send_more_content:I = 0x7f02007c

.field public static final sconnect_list_ic_share_wifi_profile:I = 0x7f02007d

.field public static final sconnect_list_ic_starttogether:I = 0x7f02007e

.field public static final sconnect_list_ic_view_contact_info:I = 0x7f02007f

.field public static final sconnect_list_icon_start_group_play_join_to_group_play:I = 0x7f020080

.field public static final sconnect_remote_123:I = 0x7f020081

.field public static final sconnect_remote_ch:I = 0x7f020082

.field public static final sconnect_remote_ch_vol_bg:I = 0x7f020083

.field public static final sconnect_remote_down_focus:I = 0x7f020084

.field public static final sconnect_remote_etc_bg:I = 0x7f020085

.field public static final sconnect_remote_focus:I = 0x7f020086

.field public static final sconnect_remote_mute:I = 0x7f020087

.field public static final sconnect_remote_power:I = 0x7f020088

.field public static final sconnect_remote_power_bg:I = 0x7f020089

.field public static final sconnect_remote_power_focus:I = 0x7f02008a

.field public static final sconnect_remote_source:I = 0x7f02008b

.field public static final sconnect_remote_up_focus:I = 0x7f02008c

.field public static final sconnect_remote_vol:I = 0x7f02008d

.field public static final settings_my_place_ic_search:I = 0x7f02008e

.field public static final take_a_picture:I = 0x7f02008f

.field public static final text_1_selector:I = 0x7f020090

.field public static final text_2_selector:I = 0x7f020091

.field public static final text_3_selector:I = 0x7f020092

.field public static final text_5_selector:I = 0x7f020093

.field public static final text_primary_selector:I = 0x7f020094

.field public static final text_secondary_selector:I = 0x7f020095

.field public static final tutorial_stroke_01:I = 0x7f020096

.field public static final tutorial_stroke_01_2:I = 0x7f020097

.field public static final tutorial_stroke_02:I = 0x7f020098

.field public static final tutorial_stroke_02_2:I = 0x7f020099

.field public static final tutorial_stroke_03:I = 0x7f02009a

.field public static final tutorial_stroke_03_2:I = 0x7f02009b

.field public static final tw_expander_close_mtrl_alpha:I = 0x7f02009c

.field public static final tw_expander_open_mtrl_alpha:I = 0x7f02009d

.field public static final tw_ic_ab_back_mtrl:I = 0x7f02009e

.field public static final video:I = 0x7f02009f

.field public static final widget_days_location_icon:I = 0x7f0200a0


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
