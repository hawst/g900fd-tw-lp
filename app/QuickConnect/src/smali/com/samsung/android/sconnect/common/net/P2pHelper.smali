.class public Lcom/samsung/android/sconnect/common/net/P2pHelper;
.super Ljava/lang/Object;
.source "P2pHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;,
        Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;
    }
.end annotation


# static fields
.field private static final CONNECTING_TIMEOUT:I = 0x9c40

.field private static final LISTEN_WAITING_TIMEOUT:I = 0x19a28

.field private static final MSG_CONNECT_TIMEOUT:I = 0x4

.field private static final MSG_REMOVE_P2PCONFIRM:I = 0x5

.field private static final MSG_START_DISCOVERY:I = 0x3

.field private static final MSG_STOP_DISCOVERY:I = 0x1

.field private static final MSG_WAITING_PEER:I = 0x2

.field static final TAG:Ljava/lang/String; = "P2pHelper"

.field private static mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;


# instance fields
.field private mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mContext:Landroid/content/Context;

.field private mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

.field private mCurSearchedDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;",
            ">;"
        }
    .end annotation
.end field

.field public mDiscoverP2p:Z

.field private mDiscoveryHandler:Landroid/os/Handler;

.field private mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mHandler:Landroid/os/Handler;

.field private mIgnoreGo:Z

.field private mIsDeviceSelectMode:Z

.field private mIsRunningWifiDirectSetting:Z

.field private mIsWorkByPreDiscovery:Z

.field private mManagedDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;",
            ">;"
        }
    .end annotation
.end field

.field mNeedListening:Z

.field private mP2pFinding:Z

.field private mP2pListening:Z

.field private mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field public mP2plistener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

.field private mProbeReqMessage:Landroid/os/Message;

.field private mProbeRespMessage:Landroid/os/Message;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRegisterIntent:Z

.field private mReqCreateGroup:Z

.field mRequestList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStopNoFlushMessage:Landroid/os/Message;

.field mTargetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWaitingTime:J

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWfdConnecting:Ljava/lang/String;

.field private mWfdExceptionalCase:Z

.field private mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

.field private mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    .line 58
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    .line 60
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 62
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    .line 65
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    .line 66
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pListening:Z

    .line 67
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoverP2p:Z

    .line 68
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 69
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 70
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    .line 72
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNeedListening:Z

    .line 73
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRegisterIntent:Z

    .line 74
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 75
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    .line 77
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mHandler:Landroid/os/Handler;

    .line 78
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    .line 80
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    .line 81
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIgnoreGo:Z

    .line 82
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    .line 83
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;

    .line 84
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    .line 85
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsDeviceSelectMode:Z

    .line 86
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z

    .line 88
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeReqMessage:Landroid/os/Message;

    .line 89
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeRespMessage:Landroid/os/Message;

    .line 90
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mStopNoFlushMessage:Landroid/os/Message;

    .line 92
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWaitingTime:J

    .line 837
    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2plistener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    .line 1004
    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$11;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1198
    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    .line 98
    const-string v1, "P2pHelper"

    const-string v2, "P2pHelper"

    const-string v3, "EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    .line 101
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mHandler:Landroid/os/Handler;

    .line 102
    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    .line 104
    sput-object p2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .line 105
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    const-string v2, "wifip2p"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 106
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 108
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRegisterIntent:Z

    if-nez v1, :cond_0

    .line 109
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRegisterIntent:Z

    .line 110
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 111
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 113
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 114
    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 115
    const-string v1, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 116
    const-string v1, "android.net.wifi.p2p.SCONNECT_PROBE_REQ"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 117
    const-string v1, "com.android.settings.wifi.p2p.SETTINGS_STRATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 118
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 121
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    .line 124
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeReqMessage:Landroid/os/Message;

    const v2, 0x2208d

    iput v2, v1, Landroid/os/Message;->what:I

    .line 125
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeRespMessage:Landroid/os/Message;

    const v2, 0x2208e

    iput v2, v1, Landroid/os/Message;->what:I

    .line 126
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mStopNoFlushMessage:Landroid/os/Message;

    const v2, 0x2208f

    iput v2, v1, Landroid/os/Message;->what:I

    .line 127
    return-void
.end method

.method private declared-synchronized DeviceAdd(Landroid/net/wifi/p2p/WifiP2pDevice;Z)V
    .locals 14
    .param p1, "peer"    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .param p2, "isConnected"    # Z

    .prologue
    .line 1468
    monitor-enter p0

    const/4 v11, 0x0

    .line 1469
    .local v11, "update":Z
    const/4 v6, 0x1

    .line 1471
    .local v6, "add":Z
    if-nez p1, :cond_0

    .line 1472
    :try_start_0
    const-string v1, "P2pHelper"

    const-string v2, "DeviceAdd"

    const-string v3, "peer == null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1538
    :goto_0
    monitor-exit p0

    return-void

    .line 1475
    :cond_0
    :try_start_1
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1476
    :cond_1
    const-string v1, "P2pHelper"

    const-string v2, "DeviceAdd"

    const-string v3, "name is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1468
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1479
    :cond_2
    :try_start_2
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 1480
    const-string v1, "P2pHelper"

    const-string v2, "DeviceAdd"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "P2pMac == null, name : "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v12, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1484
    :cond_3
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    if-eqz v1, :cond_8

    const/4 v4, 0x0

    .line 1485
    .local v4, "wfdProp":Z
    :goto_1
    if-eqz p2, :cond_9

    const/4 v5, 0x1

    .line 1486
    .local v5, "isMyGroup":Z
    :goto_2
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v3, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1489
    .local v0, "deviceP2p":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1490
    :try_start_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 1491
    const/4 v6, 0x1

    .line 1494
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    .line 1495
    .local v7, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    invoke-virtual {v0, v7}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1496
    invoke-virtual {v0, v7}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->equalWithGruopinfo(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v1

    if-nez v1, :cond_a

    .line 1497
    const/4 v11, 0x1

    .line 1506
    .end local v7    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    :cond_6
    :goto_3
    if-eqz v11, :cond_b

    .line 1507
    :try_start_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1508
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1509
    const-string v1, "P2pHelper"

    const-string v3, "DeviceAdd"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "UPDATE: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/TYPE("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")/P2p("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")/WFD("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")/MyGroup("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v1, v3, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1513
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v1, :cond_7

    .line 1514
    new-instance v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-direct {v10, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 1515
    .local v10, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v10, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;)V

    .line 1516
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v1, v10}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1537
    .end local v10    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_7
    :goto_4
    :try_start_5
    monitor-exit v2

    goto/16 :goto_0

    .end local v9    # "i$":Ljava/util/Iterator;
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v1

    .line 1484
    .end local v0    # "deviceP2p":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    .end local v4    # "wfdProp":Z
    .end local v5    # "isMyGroup":Z
    :cond_8
    invoke-static {p1}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWifiDisplay(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    move-result v4

    goto/16 :goto_1

    .line 1485
    .restart local v4    # "wfdProp":Z
    :cond_9
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v5

    goto/16 :goto_2

    .line 1499
    .restart local v0    # "deviceP2p":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    .restart local v5    # "isMyGroup":Z
    .restart local v7    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_a
    const/4 v6, 0x0

    .line 1501
    goto/16 :goto_3

    .line 1518
    .end local v7    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    :cond_b
    if-eqz v6, :cond_7

    .line 1519
    :try_start_7
    iget v1, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    const/16 v3, 0xb

    if-ne v1, v3, :cond_c

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    if-eqz v1, :cond_c

    .line 1521
    const-string v1, "P2pHelper"

    const-string v3, "DeviceAdd"

    const-string v12, "Do not add mirroring player on wfd exceptional case"

    invoke-static {v1, v3, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_4

    .line 1534
    :catch_0
    move-exception v8

    .line 1535
    .local v8, "e":Ljava/lang/NullPointerException;
    :try_start_8
    const-string v1, "P2pHelper"

    const-string v3, "DeviceAdd"

    const-string v12, "NullPointerException"

    invoke-static {v1, v3, v12, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_4

    .line 1523
    .end local v8    # "e":Ljava/lang/NullPointerException;
    :cond_c
    :try_start_9
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v1, :cond_7

    .line 1524
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1525
    const-string v1, "P2pHelper"

    const-string v3, "DeviceAdd"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "ADD: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/TYPE("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")/P2p("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")/WFD("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")/MyGroup("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v1, v3, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1529
    new-instance v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-direct {v10, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 1530
    .restart local v10    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v10, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;)V

    .line 1531
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v1, v10}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    :try_end_9
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_4
.end method

.method private declared-synchronized DeviceRemove(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 9
    .param p1, "peer"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 1541
    monitor-enter p0

    if-nez p1, :cond_0

    .line 1542
    :try_start_0
    const-string v2, "P2pHelper"

    const-string v3, "DeviceRemove"

    const-string v4, "peer == null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1575
    :goto_0
    monitor-exit p0

    return-void

    .line 1545
    :cond_0
    :try_start_1
    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1546
    :cond_1
    const-string v2, "P2pHelper"

    const-string v3, "DeviceRemove"

    const-string v4, "name is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1541
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1549
    :cond_2
    :try_start_2
    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1550
    const-string v2, "P2pHelper"

    const-string v3, "DeviceRemove"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "P2pMac == null, name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1554
    :cond_3
    new-instance v1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    const-string v2, ""

    iget-object v3, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1556
    .local v1, "deviceP2p":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1557
    :try_start_3
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v7

    .line 1558
    .local v7, "index":I
    const/4 v2, -0x1

    if-eq v7, v2, :cond_5

    .line 1559
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    move-object v1, v0

    .line 1561
    const-string v2, "P2pHelper"

    const-string v4, "DeviceRemove"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "REMOVE: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/TYPE("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")/P2p("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")/WFD("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")/MyGroup("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1565
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v2, :cond_4

    .line 1566
    new-instance v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-direct {v8, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 1567
    .local v8, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v8, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;)V

    .line 1568
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v2, v8}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 1570
    .end local v8    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1574
    :goto_1
    monitor-exit v3

    goto/16 :goto_0

    .end local v7    # "index":I
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1572
    .restart local v7    # "index":I
    :cond_5
    :try_start_5
    const-string v2, "P2pHelper"

    const-string v4, "DeviceRemove"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "device not found : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getPreferredConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->connect(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pConfigList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pConfigList;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->connectList(Landroid/net/wifi/p2p/WifiP2pConfigList;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeP2pConfirm(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->discoverP2p(Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceRemove(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    return-void
.end method

.method static synthetic access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .param p2, "x2"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceAdd(Landroid/net/wifi/p2p/WifiP2pDevice;Z)V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestGroupInfo()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->turnOffWfdSetting()V

    return-void
.end method

.method static synthetic access$2500(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/samsung/android/sconnect/common/net/P2pHelper;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWaitingTime:J

    return-wide v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestPeerList()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isBusyDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/sconnect/common/net/P2pHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/util/WfdUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/P2pHelper;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    return-object v0
.end method

.method private acquireWakeLock()V
    .locals 5

    .prologue
    .line 1438
    const-string v1, "P2pHelper"

    const-string v2, "acquireWakeLock"

    const-string v3, "EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPowerManager:Landroid/os/PowerManager;

    if-nez v1, :cond_0

    .line 1440
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 1441
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPowerManager:Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "QC_P2P_WAKE_LOCK"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1445
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_2

    .line 1446
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1447
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1449
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1454
    :cond_2
    :goto_0
    return-void

    .line 1451
    :catch_0
    move-exception v0

    .line 1452
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "P2pHelper"

    const-string v2, "acquireWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private addGroupDevices()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 1407
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-nez v1, :cond_1

    .line 1435
    :cond_0
    :goto_0
    return-void

    .line 1411
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v1, :cond_0

    .line 1412
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1413
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 1414
    .local v6, "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWfdConnected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1415
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    iget-object v1, v6, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iget-object v2, v6, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v3, v6, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1417
    .local v0, "deviceP2p":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    new-instance v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-direct {v8, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 1418
    .local v8, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v8, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;)V

    .line 1419
    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    invoke-virtual {v8, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addState(I)V

    .line 1420
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v1, :cond_2

    .line 1421
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v1, v8}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto :goto_1

    .line 1424
    .end local v0    # "deviceP2p":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    .end local v8    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_3
    const-string v1, "P2pHelper"

    const-string v2, "addGroupDevices"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " client : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v6, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    invoke-direct {p0, v6, v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceAdd(Landroid/net/wifi/p2p/WifiP2pDevice;Z)V

    goto :goto_1

    .line 1429
    .end local v6    # "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v6

    .line 1430
    .restart local v6    # "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v1, "P2pHelper"

    const-string v2, "addGroupDevices"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " owner : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v6, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1431
    invoke-direct {p0, v6, v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceAdd(Landroid/net/wifi/p2p/WifiP2pDevice;Z)V

    goto/16 :goto_0
.end method

.method private checkInit()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 164
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v1, :cond_0

    .line 165
    const-string v1, "P2pHelper"

    const-string v2, "startDiscovery"

    const-string v3, " mManager == null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :goto_0
    return v0

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v1, :cond_1

    .line 170
    const-string v1, "P2pHelper"

    const-string v2, "startDiscovery"

    const-string v3, " mChannel == null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRegisterIntent:Z

    if-nez v1, :cond_2

    .line 174
    const-string v1, "P2pHelper"

    const-string v2, "startDiscovery"

    const-string v3, " mRegisterIntent == false"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private connect(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 6
    .param p1, "device"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    const/4 v5, 0x4

    .line 733
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 734
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 737
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 738
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setIgnoreGoDevice(Z)V

    .line 741
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopDiscoveryForConnection()V

    .line 742
    const-string v0, "P2pHelper"

    const-string v1, "connect"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getPreferredConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;

    iget-object v4, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 746
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 747
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x9c40

    invoke-virtual {v0, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 748
    return-void
.end method

.method private connectList(Landroid/net/wifi/p2p/WifiP2pConfigList;)V
    .locals 5
    .param p1, "configList"    # Landroid/net/wifi/p2p/WifiP2pConfigList;

    .prologue
    const/4 v4, 0x4

    .line 752
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pConfigList;->getAllCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 756
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopDiscoveryForConnection()V

    .line 757
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 758
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setIgnoreGoDevice(Z)V

    .line 761
    :cond_2
    const-string v0, "P2pHelper"

    const-string v1, "connect"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "configList "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pConfigList;->getAllCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;

    const-string v3, ""

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfigList;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 763
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 764
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    const v1, 0x9c40

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private discoverP2p(Z)V
    .locals 6
    .param p1, "doFlush"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 298
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    if-ne v0, v3, :cond_2

    .line 299
    :cond_0
    const-string v0, "P2pHelper"

    const-string v1, "discoverP2p"

    const-string v2, "During connecting request -skipped"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestPeerList()V

    .line 393
    :cond_1
    :goto_0
    return-void

    .line 304
    :cond_2
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoverP2p:Z

    .line 305
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    if-eqz v0, :cond_1

    .line 306
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->acquireWakeLock()V

    .line 307
    if-eqz p1, :cond_9

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsDeviceSelectMode:Z

    if-nez v0, :cond_9

    .line 308
    const-string v0, "P2pHelper"

    const-string v1, "discoverP2p"

    const-string v2, "discoverPeersWithFlush"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeersWithFlush(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ILandroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 310
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    if-eqz v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getExceptionalCase()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 312
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    .line 313
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$1;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 324
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getExceptionalCase()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 325
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    .line 326
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$2;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 336
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getExceptionalCase()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_5

    .line 337
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    .line 338
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$3;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 349
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getExceptionalCase()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_6

    .line 350
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    .line 351
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$4;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 362
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 363
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    .line 364
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sconnect/common/net/P2pHelper$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$5;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 379
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getWfdState()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 380
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdExceptionalCase:Z

    .line 381
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getWfdSetting()I

    move-result v0

    if-eq v0, v3, :cond_8

    .line 382
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->turnOnWfdSetting()V

    .line 384
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->wfdUtilStartScan()V

    goto/16 :goto_0

    .line 388
    :cond_9
    const-string v0, "P2pHelper"

    const-string v1, "discoverP2p"

    const-string v2, "discoverPeers"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 390
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestPeerList()V

    goto/16 :goto_0
.end method

.method private getPreferredConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .locals 3
    .param p1, "device"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    const/4 v2, 0x0

    .line 793
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    .line 794
    .local v0, "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iput-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 795
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsPbcSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 796
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    .line 804
    :goto_0
    const/16 v1, 0xa

    iput v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    .line 805
    return-object v0

    .line 797
    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsKeypadSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 798
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    const/4 v2, 0x2

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0

    .line 799
    :cond_1
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsDisplaySupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 800
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    const/4 v2, 0x1

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0

    .line 802
    :cond_2
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0
.end method

.method private isBusyDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .locals 3
    .param p1, "device"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    const/4 v0, 0x1

    .line 1382
    iget v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    .line 1383
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->GOdeviceName:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupClient()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1390
    :cond_1
    :goto_0
    return v0

    .line 1386
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isGroupOwner()Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIgnoreGo:Z

    if-eqz v1, :cond_4

    :cond_3
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1390
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRunningWifiDirectActivity()Z
    .locals 2

    .prologue
    .line 938
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/Util;->getTopActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.settings.wifi.p2p.WifiP2pDummyPickerActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 940
    const/4 v0, 0x1

    .line 942
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 5

    .prologue
    .line 1458
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1459
    const-string v1, "P2pHelper"

    const-string v2, "releaseWakeLock"

    const-string v3, "EXEC"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1460
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1465
    :cond_0
    :goto_0
    return-void

    .line 1462
    :catch_0
    move-exception v0

    .line 1463
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "P2pHelper"

    const-string v2, "releaseWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private removeP2pConfirm(Z)V
    .locals 4
    .param p1, "skipped"    # Z

    .prologue
    .line 708
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    .line 709
    const-string v0, "P2pHelper"

    const-string v1, "removeP2pConfirm"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid manager: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :goto_0
    return-void

    .line 712
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 713
    const-string v0, "P2pHelper"

    const-string v1, "removeP2pConfirm"

    const-string v2, "isGroupOwner"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 716
    :cond_1
    const-string v0, "P2pHelper"

    const-string v1, "removeP2pConfirm"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "skip: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/sconnect/common/net/P2pHelper$8;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$8;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1, p1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestNfcConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ZLandroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0
.end method

.method private requestGroupInfo()V
    .locals 3

    .prologue
    .line 809
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v0, :cond_0

    .line 835
    :goto_0
    return-void

    .line 812
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestGroupInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    goto :goto_0
.end method

.method private requestPeerList()V
    .locals 3

    .prologue
    .line 703
    const-string v0, "P2pHelper"

    const-string v1, "requestPeerList"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2plistener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 705
    return-void
.end method

.method private stopDiscoveryForConnection()V
    .locals 3

    .prologue
    .line 508
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isRunningWifiDirectActivity()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z

    if-eqz v0, :cond_1

    .line 509
    :cond_0
    const-string v0, "P2pHelper"

    const-string v1, "stopDiscoveryForConnection"

    const-string v2, "ignore : wifi-direct activity"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :goto_0
    return-void

    .line 512
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->wfdUtilStopScan()V

    goto :goto_0
.end method

.method private stopPeerDiscovery()V
    .locals 3

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isRunningWifiDirectActivity()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z

    if-eqz v0, :cond_1

    .line 517
    :cond_0
    const-string v0, "P2pHelper"

    const-string v1, "stopPeerDiscovery"

    const-string v2, "ignore : wifi-direct activity"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :goto_0
    return-void

    .line 521
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 522
    :cond_2
    const-string v0, "P2pHelper"

    const-string v1, "stopPeerDiscovery"

    const-string v2, "During connecting request -skipped"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 525
    :cond_3
    const-string v0, "P2pHelper"

    const-string v1, "stopPeerDiscovery"

    const-string v2, "P2P_STOP_DISCOVERY"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0
.end method

.method private stopPeerDiscoveryNoFlush()V
    .locals 3

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isRunningWifiDirectActivity()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z

    if-eqz v0, :cond_1

    .line 495
    :cond_0
    const-string v0, "P2pHelper"

    const-string v1, "stopPeerDiscoveryNoFlush"

    const-string v2, "ignore : wifi-direct activity"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :goto_0
    return-void

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 500
    :cond_2
    const-string v0, "P2pHelper"

    const-string v1, "stopPeerDiscoveryNoFlush"

    const-string v2, "During connecting request -skipped"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 503
    :cond_3
    const-string v0, "P2pHelper"

    const-string v1, "stopPeerDiscoveryNoFlush"

    const-string v2, "P2P_STOP_DISCOVERY_NO_FLUSH"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mStopNoFlushMessage:Landroid/os/Message;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->callSECApi(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/os/Message;)I

    goto :goto_0
.end method

.method private turnOffWfdSetting()V
    .locals 1

    .prologue
    .line 1376
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    if-eqz v0, :cond_0

    .line 1377
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->turnOffWfdSetting()V

    .line 1379
    :cond_0
    return-void
.end method

.method private turnOnWfdSetting()V
    .locals 1

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1371
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->turnOnWfdSetting()V

    .line 1373
    :cond_0
    return-void
.end method

.method private unregisterIntent()V
    .locals 2

    .prologue
    .line 996
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRegisterIntent:Z

    if-eqz v0, :cond_0

    .line 997
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRegisterIntent:Z

    .line 998
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 999
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1002
    :cond_0
    return-void
.end method

.method private wfdUtilStartScan()V
    .locals 1

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    if-eqz v0, :cond_0

    .line 1359
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->scanWifiDisplays()V

    .line 1361
    :cond_0
    return-void
.end method

.method private wfdUtilStopScan()V
    .locals 1

    .prologue
    .line 1364
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    if-eqz v0, :cond_0

    .line 1365
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->stopScanWifiDisplays()V

    .line 1367
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelConnectPeer()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 668
    const-string v0, "P2pHelper"

    const-string v1, "cancelConnectPeer"

    const-string v2, " mTargetList clear all "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;

    .line 670
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 671
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 672
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 679
    :goto_0
    return-void

    .line 676
    :cond_0
    invoke-direct {p0, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeP2pConfirm(Z)V

    .line 677
    invoke-virtual {p0, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setIgnoreGoDevice(Z)V

    goto :goto_0
.end method

.method public cancelConnectPeer(Ljava/lang/String;)V
    .locals 4
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 687
    const-string v0, "P2pHelper"

    const-string v1, "cancelConnectPeer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mTargetList clear "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 690
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 695
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 696
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 697
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setIgnoreGoDevice(Z)V

    .line 700
    :cond_1
    return-void
.end method

.method public cancelP2pConnectRequest()V
    .locals 3

    .prologue
    .line 651
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 652
    const-string v0, "P2pHelper"

    const-string v1, "cancelP2pConnectRequest"

    const-string v2, "cancelConnect"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/sconnect/common/net/P2pHelper$7;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$7;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 665
    :cond_0
    return-void
.end method

.method public clearDiscoveryListener()V
    .locals 3

    .prologue
    .line 422
    const-string v0, "P2pHelper"

    const-string v1, "clearDiscoveryListener"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 424
    return-void
.end method

.method public clearProbeData()V
    .locals 3

    .prologue
    .line 436
    const-string v0, "P2pHelper"

    const-string v1, "clearProbeData"

    const-string v2, "Clear Probe-Data"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeReqMessage:Landroid/os/Message;

    const-string v1, "0000"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 438
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeReqMessage:Landroid/os/Message;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->callSECApi(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/os/Message;)I

    .line 439
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeRespMessage:Landroid/os/Message;

    const-string v1, "0000"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 440
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeRespMessage:Landroid/os/Message;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->callSECApi(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/os/Message;)I

    .line 441
    return-void
.end method

.method public connectPeer(Ljava/lang/String;)V
    .locals 4
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 561
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 562
    const-string v0, "P2pHelper"

    const-string v1, "connectPeer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mTargetList add "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 567
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->enable()V

    .line 568
    const-string v0, "P2pHelper"

    const-string v1, "connectPeer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "waiting "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    :goto_0
    return-void

    .line 570
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->readyToConnection(Z)V

    goto :goto_0
.end method

.method public connectPeer(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 575
    .local p1, "p2pAddressList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "P2pHelper"

    const-string v3, "connectPeer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "p2pAddressList : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 577
    .local v0, "address":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 578
    const-string v2, "P2pHelper"

    const-string v3, "connectPeer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mTargetList add "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 583
    .end local v0    # "address":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 584
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->enable()V

    .line 585
    const-string v2, "P2pHelper"

    const-string v3, "connectPeer"

    const-string v4, "waiting devices"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :goto_1
    return-void

    .line 587
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->readyToConnection(Z)V

    goto :goto_1
.end method

.method public createGroup()V
    .locals 3

    .prologue
    .line 592
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    const-string v0, "P2pHelper"

    const-string v1, "createGroup"

    const-string v2, "skip - isGroupOwner"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :goto_0
    return-void

    .line 596
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    if-eqz v0, :cond_1

    .line 597
    const-string v0, "P2pHelper"

    const-string v1, "createGroup"

    const-string v2, "skip - Requesting CreateGroup"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 600
    :cond_1
    const-string v0, "P2pHelper"

    const-string v1, "createGroup"

    const-string v2, "new Group"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    .line 602
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/android/sconnect/common/net/P2pHelper$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$6;-><init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->createGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0
.end method

.method public enable()V
    .locals 4

    .prologue
    .line 530
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 532
    :try_start_0
    const-string v1, "P2pHelper"

    const-string v2, "enable"

    const-string v3, "enableP2p"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->enableP2p(Landroid/net/wifi/p2p/WifiP2pManager$Channel;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 540
    :goto_0
    return-void

    .line 534
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "P2pHelper"

    const-string v2, "enable"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 538
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v1, "P2pHelper"

    const-string v2, "enableP2p"

    const-string v3, "already enabled"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public failedConnectPeer()V
    .locals 3

    .prologue
    .line 682
    const-string v0, "P2pHelper"

    const-string v1, "failedConnectPeer"

    const-string v2, " mRequestList clear all "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 684
    return-void
.end method

.method public getClientCount()I
    .locals 1

    .prologue
    .line 946
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 949
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRequestPeerList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 647
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRequestTargetList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 643
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public initiate()V
    .locals 4

    .prologue
    .line 130
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getWfdUtil()Lcom/samsung/android/sconnect/common/util/WfdUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;

    .line 131
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->start()V

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    const-string v0, "P2pHelper"

    const-string v1, "initiate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot start Thread: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->getState()Ljava/lang/Thread$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isConnectedWith(Ljava/lang/String;)Z
    .locals 9
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 911
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {v5}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pConnected()Z

    move-result v5

    if-nez v5, :cond_0

    .line 912
    const-string v4, "P2pHelper"

    const-string v5, "isConnectedWith"

    const-string v6, "P2p not connected"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    :goto_0
    return v3

    .line 915
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v5, :cond_3

    .line 916
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v5}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 917
    const-string v5, "P2pHelper"

    const-string v6, "isConnectedWith"

    const-string v7, "isGroupOwner"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v5}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 919
    .local v2, "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v5, "P2pHelper"

    const-string v6, "isConnectedWith"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    iget-object v5, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v3, v4

    .line 922
    goto :goto_0

    .line 926
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v5}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    .line 927
    .local v1, "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v5, "P2pHelper"

    const-string v6, "isConnectedWith"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isGroupClient - owner:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    .line 929
    goto/16 :goto_0

    .line 933
    .end local v1    # "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_3
    const-string v4, "P2pHelper"

    const-string v5, "isConnectedWith"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "not connected with : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public isConnectingDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "p2pAddress"    # Ljava/lang/String;

    .prologue
    .line 1394
    if-eqz p1, :cond_1

    .line 1395
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1396
    :cond_0
    const/4 v0, 0x1

    .line 1399
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnectingState()Z
    .locals 1

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGroupOwner()Z
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    if-eqz v0, :cond_2

    .line 954
    :cond_1
    const/4 v0, 0x1

    .line 956
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isP2pConnectedState()Z
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pConnected()Z

    move-result v0

    return v0
.end method

.method public isP2pEnabled()Z
    .locals 1

    .prologue
    .line 903
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pEnabled()Z

    move-result v0

    return v0
.end method

.method public isPossibleP2pProbe()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 195
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->isEnableMobileAP(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isRunningWifiDirectActivity()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->isOxygenEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 198
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pListening:Z

    if-nez v1, :cond_1

    .line 199
    const-string v1, "P2pHelper"

    const-string v2, "isPossibleP2pProbe"

    const-string v3, " possible P2p"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_0
    :goto_0
    return v0

    .line 201
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    if-nez v1, :cond_0

    .line 205
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWifiEnabled()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 182
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    const-string v3, "wifi"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 184
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    if-nez v0, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v1

    .line 188
    :cond_1
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public needToDisconnect(Ljava/lang/String;)Z
    .locals 5
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 960
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v1, :cond_0

    .line 961
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v1

    if-nez v1, :cond_0

    .line 962
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v0

    .line 963
    .local v0, "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    if-eqz p1, :cond_0

    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 964
    const-string v1, "P2pHelper"

    const-string v2, "needToDisconnect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isGroupClient - owner:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    const/4 v1, 0x1

    .line 969
    .end local v0    # "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public needToDisconnectOnPeriph(Ljava/lang/String;)Z
    .locals 8
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 973
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v5, :cond_3

    .line 974
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v5}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v5

    if-nez v5, :cond_1

    .line 975
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v5}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    .line 976
    .local v1, "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 977
    const-string v4, "P2pHelper"

    const-string v5, "needToDisconnectOnReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isGroupClient - owner:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    .end local v1    # "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_0
    :goto_0
    return v3

    .line 982
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v5}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 983
    .local v2, "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v5, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 984
    const-string v3, "P2pHelper"

    const-string v5, "needToDisconnectOnReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isGroupOwner - client:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 986
    goto :goto_0

    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_3
    move v3, v4

    .line 992
    goto :goto_0
.end method

.method public readyToConnection(Z)V
    .locals 3
    .param p1, "multiple"    # Z

    .prologue
    const/4 v2, 0x0

    .line 628
    const-wide/32 v0, 0x9c40

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startFindP2p(JZZ)V

    .line 629
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 630
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->createGroup()V

    .line 632
    :cond_1
    return-void
.end method

.method public removeGroup(Z)V
    .locals 5
    .param p1, "removeOnly"    # Z

    .prologue
    const/4 v4, 0x0

    .line 617
    const-string v0, "P2pHelper"

    const-string v1, "removeGroup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 619
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 620
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z

    .line 621
    if-nez p1, :cond_0

    .line 622
    const-string v0, "P2pHelper"

    const-string v1, "removeGroup"

    const-string v2, "MSG_WAITING_PEER "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 625
    :cond_0
    return-void
.end method

.method public setDeviceSelectMode(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsDeviceSelectMode:Z

    .line 260
    return-void
.end method

.method public setDiscoveryListener(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .prologue
    .line 209
    const-string v0, "P2pHelper"

    const-string v1, "setDiscoveryListener"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 211
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 212
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 213
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 215
    return-void

    .line 213
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setIgnoreGoDevice(Z)V
    .locals 4
    .param p1, "ignore"    # Z

    .prologue
    .line 635
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIgnoreGo:Z

    if-ne p1, v0, :cond_0

    .line 640
    :goto_0
    return-void

    .line 638
    :cond_0
    const-string v0, "P2pHelper"

    const-string v1, "setIgnoreGoDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIgnoreGo:Z

    goto :goto_0
.end method

.method public setPreDiscoveryPacketListener(Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    .prologue
    .line 1256
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    .line 1257
    return-void
.end method

.method public setProbeData([B[B)V
    .locals 22
    .param p1, "advData"    # [B
    .param p2, "resData"    # [B

    .prologue
    .line 218
    const/4 v8, 0x0

    .line 220
    .local v8, "compensationLen":I
    add-int/lit8 v11, v8, 0xb

    .line 222
    .local v11, "nameLen":I
    new-array v10, v11, [B

    .line 224
    .local v10, "name":[B
    const/16 v18, 0x2

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v10, v2, v11}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 226
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v10}, Ljava/lang/String;-><init>([B)V

    .line 228
    .local v6, "bdname":Ljava/lang/String;
    const/16 v18, 0x5

    aget-byte v13, p1, v18

    .line 230
    .local v13, "packetInfo":B
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v17, v0

    .line 231
    .local v17, "target":[B
    const/16 v18, 0x12

    const/16 v19, 0x0

    const/16 v20, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v17

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 233
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v14

    .line 235
    .local v14, "respTarget":Ljava/lang/String;
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v9, v0, [B

    .line 236
    .local v9, "mac":[B
    const/16 v18, 0xc

    const/16 v19, 0x0

    const/16 v20, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v9, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 238
    invoke-static {v9}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v12

    .line 240
    .local v12, "p2pMac":Ljava/lang/String;
    const/16 v18, 0x10

    move/from16 v0, v18

    new-array v7, v0, [B

    .line 241
    .local v7, "byte_ackTarget":[B
    const/16 v18, 0xd

    const/16 v19, 0x0

    const/16 v20, 0x10

    move-object/from16 v0, p2

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 243
    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v5

    .line 245
    .local v5, "ackTarget":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v15

    .line 246
    .local v15, "sAdvData":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v16

    .line 248
    .local v16, "sResData":Ljava/lang/String;
    const-string v18, "P2pHelper"

    const-string v19, "setProbeData"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "PACKET INFO - name: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/PacketInfo :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/RESP Target :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/BLE :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/ACK Target :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeReqMessage:Landroid/os/Message;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeReqMessage:Landroid/os/Message;

    move-object/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Landroid/net/wifi/p2p/WifiP2pManager;->callSECApi(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/os/Message;)I

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeRespMessage:Landroid/os/Message;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mProbeRespMessage:Landroid/os/Message;

    move-object/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Landroid/net/wifi/p2p/WifiP2pManager;->callSECApi(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/os/Message;)I

    .line 256
    return-void
.end method

.method public setWfdConnecting(Ljava/lang/String;)V
    .locals 0
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;

    .line 161
    return-void
.end method

.method public startFindP2p(JZZ)V
    .locals 5
    .param p1, "time"    # J
    .param p3, "byPreDiscovery"    # Z
    .param p4, "doFlush"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 263
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 264
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 265
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->checkInit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 265
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 271
    :cond_0
    if-eqz p3, :cond_1

    .line 272
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    .line 277
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->addGroupDevices()V

    .line 279
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 280
    const-string v0, "P2pHelper"

    const-string v1, "startFindP2p"

    const-string v2, "During connecting request -skipped"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestPeerList()V

    goto :goto_0

    .line 274
    :cond_1
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    goto :goto_1

    .line 284
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 286
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    .line 287
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pListening:Z

    .line 288
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 289
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoverP2p:Z

    .line 290
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->enable()V

    .line 294
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 292
    :cond_3
    invoke-direct {p0, p4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->discoverP2p(Z)V

    goto :goto_2
.end method

.method public startListenP2p(JZ)V
    .locals 5
    .param p1, "time"    # J
    .param p3, "byPreDiscovery"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 396
    if-eqz p3, :cond_0

    .line 397
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    .line 402
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 403
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    if-eqz v0, :cond_1

    .line 404
    const-string v0, "P2pHelper"

    const-string v1, "startListenP2p"

    const-string v2, "discoverPeers"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 406
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->acquireWakeLock()V

    .line 417
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 419
    return-void

    .line 399
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    goto :goto_0

    .line 408
    :cond_1
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pListening:Z

    .line 409
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isRunningWifiDirectActivity()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsRunningWifiDirectSetting:Z

    if-eqz v0, :cond_3

    .line 410
    :cond_2
    const-string v0, "P2pHelper"

    const-string v1, "startListenP2p"

    const-string v2, "ignore : wifi-direct activity"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 412
    :cond_3
    const-string v0, "P2pHelper"

    const-string v1, "startListenP2p"

    const-string v2, "requestP2pListen"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->requestP2pListen(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 414
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->acquireWakeLock()V

    goto :goto_1
.end method

.method public stopCentral()V
    .locals 3

    .prologue
    .line 153
    const-string v0, "P2pHelper"

    const-string v1, "stopCentral"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->turnOffWfdSetting()V

    .line 157
    :cond_0
    return-void
.end method

.method public stopDiscoverP2p()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 445
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    if-eqz v4, :cond_1

    .line 446
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 449
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 450
    :cond_0
    const-string v4, "P2pHelper"

    const-string v5, "stopDiscoverP2p"

    const-string v6, "exist target!!!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x2710

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 491
    :goto_0
    return-void

    .line 456
    :cond_1
    iput-boolean v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoverP2p:Z

    .line 457
    iget-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pListening:Z

    if-eqz v4, :cond_4

    .line 458
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    if-nez v4, :cond_3

    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/SconnectManager;->isCentralRunning()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 459
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopPeerDiscoveryNoFlush()V

    .line 460
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->wfdUtilStopScan()V

    .line 465
    :goto_1
    iput-boolean v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z

    .line 466
    iput-boolean v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pListening:Z

    .line 467
    iput-boolean v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    .line 469
    :cond_4
    invoke-direct {p0, v5}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeP2pConfirm(Z)V

    .line 471
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->releaseWakeLock()V

    .line 473
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    monitor-enter v5

    .line 474
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 475
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    .line 476
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/4 v6, -0x1

    if-ne v4, v6, :cond_5

    .line 477
    iget-boolean v4, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    if-nez v4, :cond_5

    .line 478
    const-string v4, "P2pHelper"

    const-string v6, "stopDiscoverP2p"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "REMOVE: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/TYPE("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")/P2p("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")/WFD("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")/MyGroup("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v4, :cond_6

    .line 482
    new-instance v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 483
    .local v3, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v3, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;)V

    .line 484
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v4, v3}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 486
    .end local v3    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 490
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 462
    :cond_7
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopPeerDiscovery()V

    goto/16 :goto_1

    .line 490
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;>;"
    :cond_8
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public stopPreDiscovery()V
    .locals 1

    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->clearProbeData()V

    .line 429
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    if-eqz v0, :cond_0

    .line 430
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mIsWorkByPreDiscovery:Z

    .line 431
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopDiscoverP2p()V

    .line 433
    :cond_0
    return-void
.end method

.method public terminate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 139
    const-string v0, "P2pHelper"

    const-string v1, "terminate"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mHandler:Landroid/os/Handler;

    .line 141
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->terminate()V

    .line 142
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->cancelConnectPeer()V

    .line 143
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopDiscoverP2p()V

    .line 144
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->unregisterIntent()V

    .line 145
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 146
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 147
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 148
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 149
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mManagedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 150
    return-void
.end method

.method public waitingPeer()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 543
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 544
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNeedListening:Z

    .line 545
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->enable()V

    .line 546
    const-string v0, "P2pHelper"

    const-string v1, "waitingPeer"

    const-string v2, "waiting "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWaitingTime:J

    .line 554
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x4650

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 558
    :cond_0
    return-void

    .line 548
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopDiscoveryForConnection()V

    .line 549
    const-wide/32 v0, 0x19a28

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startListenP2p(JZ)V

    .line 550
    invoke-direct {p0, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeP2pConfirm(Z)V

    goto :goto_0
.end method
