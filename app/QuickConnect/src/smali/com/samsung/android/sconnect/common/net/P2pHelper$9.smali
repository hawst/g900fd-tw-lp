.class Lcom/samsung/android/sconnect/common/net/P2pHelper$9;
.super Ljava/lang/Object;
.source "P2pHelper.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestGroupInfo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0

    .prologue
    .line 812
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 7
    .param p1, "group"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    .line 815
    const-string v3, "P2pHelper"

    const-string v4, "requestGroupInfo "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onGroupInfoAvailable- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v3, p1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$302(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 817
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z
    invoke-static {v3, v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$102(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)Z

    .line 818
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v3

    if-nez v3, :cond_1

    .line 833
    :cond_0
    :goto_0
    return-void

    .line 820
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 821
    const-string v3, "P2pHelper"

    const-string v4, "requestGroupInfo"

    const-string v5, "isGroupOwner true"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 823
    .local v2, "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v3, "P2pHelper"

    const-string v4, "requestGroupInfo"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 827
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCreatedGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$300(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    .line 828
    .local v1, "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    const-string v3, "P2pHelper"

    const-string v4, "requestGroupInfo"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isGroupClient - owner:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    .end local v1    # "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 831
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$9;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestPeerList()V
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$400(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    goto/16 :goto_0
.end method
