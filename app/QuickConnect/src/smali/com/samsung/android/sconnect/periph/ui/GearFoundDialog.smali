.class public Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;
.super Landroid/app/Activity;
.source "GearFoundDialog.java"


# static fields
.field static final TAG:Ljava/lang/String; = "GearFoundDialog"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:[B

.field private mDeviceAddress:Ljava/lang/String;

.field private mDeviceManager:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mGearDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceName:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceAddress:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceManager:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mGearDialog:Landroid/app/Dialog;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mData:[B

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceManager:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mData:[B

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceAddress:Ljava/lang/String;

    return-object v0
.end method

.method private static getPackageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "managerName"    # Ljava/lang/String;

    .prologue
    .line 142
    const-string v0, ""

    .line 144
    .local v0, "retPackageName":Ljava/lang/String;
    const-string v1, "watchmanager"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    const-string v0, "com.samsung.android.app.watchmanager"

    .line 152
    :goto_0
    return-object v0

    .line 146
    :cond_0
    const-string v1, "wms"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    const-string v0, "com.samsung.android.wms"

    goto :goto_0

    .line 149
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com.samsung.android."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected checkNearbyGear()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 74
    const-string v4, "GearFoundDialog"

    const-string v5, "checkNearbyGear"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mNearbyGear: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v4, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030010

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 77
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0c0002

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 80
    .local v1, "msgView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceManager:Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->getPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 81
    const v4, 0x7f090136

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceName:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    const v4, 0x7f090137

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceName:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 89
    .local v2, "titleTxt":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f090088

    new-instance v6, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;-><init>(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f090089

    new-instance v6, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$1;-><init>(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mGearDialog:Landroid/app/Dialog;

    .line 128
    iget-object v4, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mGearDialog:Landroid/app/Dialog;

    new-instance v5, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$3;-><init>(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)V

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :goto_1
    return-void

    .line 84
    .end local v2    # "titleTxt":Ljava/lang/String;
    :cond_0
    const v4, 0x7f090135

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceName:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    const v4, 0x7f090134

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "titleTxt":Ljava/lang/String;
    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "GearFoundDialog"

    const-string v5, "checkNearbyGear"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "exception : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 67
    const-string v0, "GearFoundDialog"

    const-string v1, "onConfigurationChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "newConfing.orientation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->checkNearbyGear()V

    .line 70
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 71
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 39
    const-string v1, "GearFoundDialog"

    const-string v2, "onCreate"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    iput-object p0, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mContext:Landroid/content/Context;

    .line 44
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 45
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 46
    const-string v1, "NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceName:Ljava/lang/String;

    .line 47
    const-string v1, "MAC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceAddress:Ljava/lang/String;

    .line 48
    const-string v1, "MANAGER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceManager:Ljava/lang/String;

    .line 49
    const-string v1, "DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mData:[B

    .line 51
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->checkNearbyGear()V

    .line 52
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x400000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 57
    :goto_0
    return-void

    .line 54
    :cond_0
    const-string v1, "GearFoundDialog"

    const-string v2, "onCreate"

    const-string v3, "ERROR - missed Intent data"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "GearFoundDialog"

    const-string v1, "onDestroy"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 63
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 157
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 158
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->finish()V

    .line 159
    return-void
.end method
