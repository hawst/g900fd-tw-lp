.class Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$3;
.super Landroid/os/CountDownTimer;
.source "BluetoothActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 516
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$3;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    .line 524
    const-string v0, "BluetoothActionHelper"

    const-string v1, "mCountDownTimer"

    const-string v2, "onFinish - reset STATE_BT_IDLE"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mState:I

    .line 526
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    .line 527
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "arg0"    # J

    .prologue
    .line 520
    return-void
.end method
