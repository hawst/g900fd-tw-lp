.class Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothOppActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x2

    .line 219
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 220
    .local v0, "action":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    if-nez v2, :cond_1

    .line 221
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "mBtOppBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " when SconnectManager is null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->unregisterBtOppReceiver()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;)V

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 228
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_2

    .line 229
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "mBtOppBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v2, "android.btopp.intent.action.BT_OPP_TRANSFER_STARTED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 237
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    if-eqz v2, :cond_0

    .line 238
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "mBtOppBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is STATUS_STARTED"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATUS_STARTED"

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 232
    :cond_2
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "mBtOppBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " including BluetoothDevice null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 244
    :cond_3
    const-string v2, "android.btopp.intent.action.MSG_SESSION_COMPLETE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 245
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    if-eqz v2, :cond_4

    .line 246
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "mBtOppBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is STATUS_COMPLETED"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATUS_COMPLETED"

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_4
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v2

    if-ne v2, v6, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->isTherePendingItem(Landroid/content/Context;)Z
    invoke-static {v2, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->access$100(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 255
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "mBtOppBroadcastReceiver"

    const-string v4, "now disableBluetoothAdapter"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->unregisterBtOppReceiver()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;)V

    .line 257
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothHelper()Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->disableBluetoothAdapter()Z

    goto/16 :goto_0

    .line 259
    :cond_5
    const-string v2, "android.btopp.intent.action.MSG_SESSION_ERROR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    if-eqz v2, :cond_6

    .line 261
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "mBtOppBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is STATUS_FAILED"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATUS_FAILED"

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_6
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v2

    if-ne v2, v6, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->isTherePendingItem(Landroid/content/Context;)Z
    invoke-static {v2, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->access$100(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 270
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "mBtOppBroadcastReceiver"

    const-string v4, "now disableBluetoothAdapter"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->unregisterBtOppReceiver()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;)V

    .line 272
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothHelper()Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->disableBluetoothAdapter()Z

    goto/16 :goto_0
.end method
