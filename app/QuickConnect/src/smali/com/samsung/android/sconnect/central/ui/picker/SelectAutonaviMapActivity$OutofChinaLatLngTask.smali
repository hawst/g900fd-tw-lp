.class Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;
.super Landroid/os/AsyncTask;
.source "SelectAutonaviMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OutofChinaLatLngTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/amap/api/maps/model/LatLng;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V
    .locals 0

    .prologue
    .line 853
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;

    .prologue
    .line 853
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 853
    check-cast p1, [Lcom/amap/api/maps/model/LatLng;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->doInBackground([Lcom/amap/api/maps/model/LatLng;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/amap/api/maps/model/LatLng;)Ljava/util/List;
    .locals 8
    .param p1, "latLng"    # [Lcom/amap/api/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/amap/api/maps/model/LatLng;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 856
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    const-string v4, "OutofChinaLatLngTask - doInBackground"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    new-instance v1, Landroid/location/Geocoder;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 859
    .local v1, "geocoder":Landroid/location/Geocoder;
    const/4 v0, 0x0

    .line 863
    .local v0, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, p1, v2

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const/4 v4, 0x0

    aget-object v4, p1, v4

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 867
    :goto_0
    return-object v0

    .line 864
    :catch_0
    move-exception v7

    .line 865
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 853
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 872
    .local p1, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    const-string v0, "SelectAutonaviMapActivity"

    const-string v1, ""

    const-string v2, "Selected location - onPostExecute"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 874
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->markAddress(Ljava/util/List;)V

    .line 879
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 880
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 884
    :cond_0
    return-void

    .line 876
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    iget-wide v2, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->addMarker(DD)V
    invoke-static {v0, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1400(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;DD)V

    goto :goto_0
.end method
