.class public Lcom/samsung/android/sconnect/periph/PeriphControlUtil;
.super Ljava/lang/Object;
.source "PeriphControlUtil.java"


# static fields
.field public static final EXTRA_FORCE_FG:Ljava/lang/String; = "PROCESS_FORCE_FG"

.field private static final TAG:Ljava/lang/String; = "PeriphServiceUtil"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lowerPeriphService(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isBleAutoEnable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 34
    const-string v1, "PeriphServiceUtil"

    const-string v2, "lowerPeriphService"

    const-string v3, "dont this until LE_AUTO_ENABLE feature"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    :goto_0
    return-void

    .line 38
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.periph.LOWER_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "intentService":Landroid/content/Intent;
    const-string v1, "com.samsung.android.sconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    new-instance v1, Landroid/os/UserHandle;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public static needToStart(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isBleAutoEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x1

    .line 22
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sconnect/common/util/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static startPeriphService(Landroid/content/Context;Z)Landroid/content/ComponentName;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requireForeground"    # Z

    .prologue
    .line 26
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.periph.START_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 27
    .local v0, "intentService":Landroid/content/Intent;
    const-string v1, "PROCESS_FORCE_FG"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 28
    const-string v1, "com.samsung.android.sconnect"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 29
    new-instance v1, Landroid/os/UserHandle;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    move-result-object v1

    return-object v1
.end method
