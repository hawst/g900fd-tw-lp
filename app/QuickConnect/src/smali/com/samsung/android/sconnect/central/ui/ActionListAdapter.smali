.class public Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ActionListAdapter.java"


# instance fields
.field private mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDeviceType:I

.field private mIsConnected:Z

.field private mIsMirroring:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p2, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    .line 28
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mActionList:Ljava/util/ArrayList;

    .line 30
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mActionList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Integer;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mActionList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 56
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->getItem(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 63
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v11, 0x7f090029

    const v10, 0x7f090026

    const v9, 0x7f090022

    const v8, 0x7f09001e

    .line 68
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030001

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 70
    const v5, 0x7f0c000a

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 71
    .local v0, "actionIconView":Landroid/widget/ImageView;
    const v5, 0x7f0c000b

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 73
    .local v2, "actionNameView":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->getItem(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 74
    .local v1, "actionName":I
    const-string v4, ""

    .line 75
    .local v4, "text":Ljava/lang/String;
    const/4 v3, -0x1

    .line 77
    .local v3, "imageId":I
    packed-switch v1, :pswitch_data_0

    .line 158
    :pswitch_0
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 159
    const v3, 0x7f020079

    .line 164
    :goto_0
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 166
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 168
    :cond_0
    invoke-virtual {p2, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 169
    return-object p2

    .line 79
    :pswitch_1
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f090020

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 80
    const v3, 0x7f02007c

    .line 81
    goto :goto_0

    .line 83
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 84
    const v3, 0x7f020080

    .line 85
    goto :goto_0

    .line 87
    :pswitch_3
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 88
    const v3, 0x7f020079

    .line 89
    goto :goto_0

    .line 91
    :pswitch_4
    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mIsMirroring:Z

    if-eqz v5, :cond_1

    .line 92
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f090028

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 96
    :goto_1
    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mDeviceType:I

    packed-switch v5, :pswitch_data_1

    .line 105
    :pswitch_5
    const v3, 0x7f020078

    .line 107
    goto :goto_0

    .line 94
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f090027

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 99
    :pswitch_6
    const v3, 0x7f020077

    .line 100
    goto :goto_0

    .line 109
    :pswitch_7
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f090025

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 110
    const v3, 0x7f020075

    .line 111
    goto :goto_0

    .line 113
    :pswitch_8
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f09002e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 114
    const v3, 0x7f02007e

    .line 115
    goto :goto_0

    .line 117
    :pswitch_9
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 118
    const v3, 0x7f02007a

    .line 119
    goto :goto_0

    .line 121
    :pswitch_a
    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mIsConnected:Z

    if-eqz v5, :cond_2

    .line 122
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f09002a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 126
    :goto_2
    const v3, 0x7f020073

    .line 127
    goto/16 :goto_0

    .line 124
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 129
    :pswitch_b
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 130
    const v3, 0x7f020080

    .line 131
    goto/16 :goto_0

    .line 133
    :pswitch_c
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f09002c

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 134
    const v3, 0x7f020074

    .line 135
    goto/16 :goto_0

    .line 137
    :pswitch_d
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 138
    const v3, 0x7f020073

    .line 139
    goto/16 :goto_0

    .line 141
    :pswitch_e
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f09002d

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 143
    goto/16 :goto_0

    .line 145
    :pswitch_f
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v6

    const v7, 0x7f090030

    invoke-virtual {v6, v7}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 147
    const v3, 0x7f02007d

    .line 148
    goto/16 :goto_0

    .line 150
    :pswitch_10
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 151
    const v3, 0x7f02007a

    .line 152
    goto/16 :goto_0

    .line 154
    :pswitch_11
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f090019

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 155
    const v3, 0x7f02007b

    .line 156
    goto/16 :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_d
        :pswitch_e
        :pswitch_c
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 96
    :pswitch_data_1
    .packed-switch 0xb
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ZZ)V
    .locals 5
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isConnected"    # Z
    .param p3, "isMirroring"    # Z

    .prologue
    .line 33
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mDeviceType:I

    .line 34
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 35
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v0

    .line 36
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 38
    .local v1, "actionName":I
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mActionList:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 41
    .end local v1    # "actionName":I
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    iput-boolean p2, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mIsConnected:Z

    .line 42
    iput-boolean p3, p0, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->mIsMirroring:Z

    .line 43
    return-void
.end method
