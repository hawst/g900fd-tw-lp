.class Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;
.super Ljava/lang/Object;
.source "SelectMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/Thread;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 229
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocThread:Ljava/lang/Thread;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$202(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 257
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mCurrentLocThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
