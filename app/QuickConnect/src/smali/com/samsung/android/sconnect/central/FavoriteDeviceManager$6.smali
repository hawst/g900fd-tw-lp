.class Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$6;
.super Ljava/lang/Object;
.source "FavoriteDeviceManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showNameDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$6;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 550
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$6;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$1000(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 551
    .local v0, "deviceName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 552
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$6;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # invokes: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->changeName(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$1100(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;Ljava/lang/String;)V

    .line 554
    :cond_0
    return-void
.end method
