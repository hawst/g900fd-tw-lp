.class public Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;
.super Ljava/lang/Object;
.source "SamsungAppsChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StubDownloadApiResult"
.end annotation


# instance fields
.field appId:Ljava/lang/String;

.field contentSize:Ljava/lang/String;

.field downloadURI:Ljava/lang/String;

.field productId:Ljava/lang/String;

.field productName:Ljava/lang/String;

.field resultCode:Ljava/lang/String;

.field resultMsg:Ljava/lang/String;

.field versionCode:I

.field versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->appId:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->resultCode:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->resultMsg:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->downloadURI:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->contentSize:Ljava/lang/String;

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->versionCode:I

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->versionName:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->productId:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubDownloadApiResult;->productName:Ljava/lang/String;

    return-void
.end method
