.class final Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$InputDeviceServiceListener;
.super Ljava/lang/Object;
.source "HidProfile.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "InputDeviceServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$InputDeviceServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$1;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$InputDeviceServiceListener;-><init>(Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 50
    # getter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->V:Z
    invoke-static {}, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "HidProfile"

    const-string v1, "onServiceConnected"

    const-string v2, "Bluetooth service connected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$InputDeviceServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    check-cast p2, Landroid/bluetooth/BluetoothInputDevice;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->mService:Landroid/bluetooth/BluetoothInputDevice;
    invoke-static {v0, p2}, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->access$102(Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;Landroid/bluetooth/BluetoothInputDevice;)Landroid/bluetooth/BluetoothInputDevice;

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$InputDeviceServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->mIsProfileReady:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->access$202(Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;Z)Z

    .line 55
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "profile"    # I

    .prologue
    .line 59
    # getter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->V:Z
    invoke-static {}, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "HidProfile"

    const-string v1, "onServiceDisconnected"

    const-string v2, "Bluetooth service disconnected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$InputDeviceServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->mIsProfileReady:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->access$202(Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;Z)Z

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile$InputDeviceServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->mService:Landroid/bluetooth/BluetoothInputDevice;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;->access$102(Lcom/samsung/android/sconnect/central/action/BtProfile/HidProfile;Landroid/bluetooth/BluetoothInputDevice;)Landroid/bluetooth/BluetoothInputDevice;

    .line 64
    return-void
.end method
