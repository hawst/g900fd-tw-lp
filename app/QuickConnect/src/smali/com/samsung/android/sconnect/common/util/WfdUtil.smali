.class public Lcom/samsung/android/sconnect/common/util/WfdUtil;
.super Ljava/lang/Object;
.source "WfdUtil.java"


# static fields
.field public static final CASE_GROUPPLAY:I = 0x7

.field public static final CASE_HDMI:I = 0x3

.field public static final CASE_POWERSAVE:I = 0x5

.field public static final CASE_SIDESYNC:I = 0x4

.field public static final DEVICETYPE_WFD_PLAYER:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "WfdUtil"


# instance fields
.field protected mAvailableDisplays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field protected mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mScanning:Z

.field private mSettingOn:Z

.field protected mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->DEVICETYPE_WFD_PLAYER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mContext:Landroid/content/Context;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mAvailableDisplays:Ljava/util/List;

    .line 26
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 28
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    .line 38
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mScanning:Z

    .line 39
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mSettingOn:Z

    .line 42
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mContext:Landroid/content/Context;

    .line 43
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mContext:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    .line 45
    return-void
.end method

.method private static isPrimarySinkDeviceType(I)Z
    .locals 2
    .param p0, "deviceType"    # I

    .prologue
    const/4 v0, 0x1

    .line 147
    if-eq p0, v0, :cond_0

    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWifiDisplay(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .locals 1
    .param p0, "device"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 141
    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pDevice;->wfdInfo:Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pDevice;->wfdInfo:Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->isWfdEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pDevice;->wfdInfo:Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->isSessionAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/net/wifi/p2p/WifiP2pDevice;->wfdInfo:Landroid/net/wifi/p2p/WifiP2pWfdInfo;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pWfdInfo;->getDeviceType()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isPrimarySinkDeviceType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public connectWifiDisplay(Ljava/lang/String;)V
    .locals 4
    .param p1, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "WfdUtil"

    const-string v1, "connectWifiDisplay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deviceAddress : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->connectWifiDisplay(Ljava/lang/String;)V

    .line 160
    :cond_0
    return-void
.end method

.method public disconnectWifiDisplay()V
    .locals 3

    .prologue
    .line 163
    const-string v0, "WfdUtil"

    const-string v1, "disconnectWifiDisplay"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 167
    :cond_0
    return-void
.end method

.method public getAvailableDisplays()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mAvailableDisplays:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 103
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-eqz v4, :cond_0

    .line 104
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getDisplays()[Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    .local v0, "arr$":[Landroid/hardware/display/WifiDisplay;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 105
    .local v1, "d":Landroid/hardware/display/WifiDisplay;
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mAvailableDisplays:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 108
    .end local v0    # "arr$":[Landroid/hardware/display/WifiDisplay;
    .end local v1    # "d":Landroid/hardware/display/WifiDisplay;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mAvailableDisplays:Ljava/util/List;

    return-object v4
.end method

.method public getConnectedAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getConnectedDisplayInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getPrimaryDeviceType()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getConnectedName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getExceptionalCase()I
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->checkExceptionalCase()I

    move-result v0

    return v0
.end method

.method public getPrimaryDeviceType(Landroid/os/Parcelable;)Ljava/lang/String;
    .locals 2
    .param p1, "d"    # Landroid/os/Parcelable;

    .prologue
    .line 122
    move-object v0, p1

    check-cast v0, Landroid/hardware/display/WifiDisplay;

    .line 123
    .local v0, "dev":Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getPrimaryDeviceType()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getWfdSetting()I
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "wifi_display_on"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getWfdState()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    return v0
.end method

.method public isWfdConnected()Z
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 128
    const-string v0, "WfdUtil"

    const-string v1, "isWfdConnected"

    const-string v2, ">> true"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const/4 v0, 0x1

    .line 133
    :goto_0
    return v0

    .line 132
    :cond_0
    const-string v0, "WfdUtil"

    const-string v1, "isWfdConnected"

    const-string v2, " >> false"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pauseWifiDisplay()V
    .locals 3

    .prologue
    .line 170
    const-string v0, "WfdUtil"

    const-string v1, "pauseWifiDisplay"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWfdConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->pauseWifiDisplay()V

    .line 174
    :cond_0
    return-void
.end method

.method public resumeWifiDisplay()V
    .locals 3

    .prologue
    .line 177
    const-string v0, "WfdUtil"

    const-string v1, "resumeWifiDisplay"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWfdConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->resumeWifiDisplay()V

    .line 181
    :cond_0
    return-void
.end method

.method public scanWifiDisplays()V
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v0

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mScanning:Z

    .line 82
    const-string v0, "WfdUtil"

    const-string v1, "scanWifiDisplays"

    const-string v2, "DisplayManager.scanWifiDisplays()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    return-void
.end method

.method public stopScanWifiDisplays()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mScanning:Z

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mScanning:Z

    .line 90
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->stopScanWifiDisplays()V

    .line 91
    const-string v0, "WfdUtil"

    const-string v1, "stopScanWifiDisplays"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    return-void
.end method

.method public turnOffWfdSetting()V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mSettingOn:Z

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getWfdState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getWfdState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getExceptionalCase()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->getExceptionalCase()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 68
    const-string v0, "WfdUtil"

    const-string v1, "turnOffWfdSetting"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v1, Landroid/hardware/display/DisplayManager$WfdAppState;->TEARDOWN:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mSettingOn:Z

    .line 74
    :cond_0
    return-void
.end method

.method public turnOnWfdSetting()V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v1, Landroid/hardware/display/DisplayManager$WfdAppState;->SETUP:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/util/WfdUtil;->mSettingOn:Z

    .line 57
    const-string v0, "WfdUtil"

    const-string v1, "turnOnWfdSetting"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_0
    return-void
.end method
