.class Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;
.super Ljava/lang/Object;
.source "ChromecastHelper.java"

# interfaces
.implements Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/ChromecastHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Lcom/sec/android/sdial/DeviceFinder;Lcom/sec/android/sdial/CAVPlayer;)V
    .locals 3
    .param p1, "deviceFinder"    # Lcom/sec/android/sdial/DeviceFinder;
    .param p2, "player"    # Lcom/sec/android/sdial/CAVPlayer;

    .prologue
    .line 118
    const-string v0, "ChromecastHelper"

    const-string v1, "IPlayerEventListener"

    const-string v2, "In onServiceConnected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;
    invoke-static {v0, p1}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$102(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;Lcom/sec/android/sdial/DeviceFinder;)Lcom/sec/android/sdial/DeviceFinder;

    .line 120
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$100(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/sec/android/sdial/DeviceFinder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->IMediaDeviceDiscoveryListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/sdial/DeviceFinder;->setDeviceFinderEventListener(Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$100(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/sec/android/sdial/DeviceFinder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/sdial/DeviceFinder;->startDiscovery()V

    .line 122
    return-void
.end method

.method public onServiceDisconnected()V
    .locals 3

    .prologue
    .line 110
    const-string v0, "ChromecastHelper"

    const-string v1, "IPlayerEventListener"

    const-string v2, "In onServiceDisconnected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mServiceConnectionListener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$000(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mServiceConnectionListener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$000(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;->onServiceDisconnected()V

    .line 114
    :cond_0
    return-void
.end method
