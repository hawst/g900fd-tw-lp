.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onFilePickerResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$data:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2337
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->val$data:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 2340
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11, v12}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4802(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2341
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->val$data:Landroid/content/Intent;

    invoke-virtual {v11}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    .line 2342
    .local v10, "url":Landroid/net/Uri;
    if-eqz v10, :cond_0

    .line 2343
    const-string v11, "SconnectDisplay"

    const-string v12, "onFilePickerResult"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getData url: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2344
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2347
    :cond_0
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->val$data:Landroid/content/Intent;

    invoke-virtual {v11}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2348
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 2349
    const-string v11, "android.intent.extra.STREAM"

    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    .line 2350
    .local v8, "uri":Landroid/net/Uri;
    if-eqz v8, :cond_1

    .line 2351
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->val$data:Landroid/content/Intent;

    const-string v12, "vcard"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2352
    .local v3, "extrastring":Ljava/lang/String;
    if-eqz v3, :cond_1

    const-string v11, "vcard"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 2353
    const-string v11, "SconnectDisplay"

    const-string v12, "onFilePickerResult"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "vcard results: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2354
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2358
    .end local v3    # "extrastring":Ljava/lang/String;
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2359
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mUriProcessor:Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->val$data:Landroid/content/Intent;

    invoke-virtual {v11, v12}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->process(Landroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v9

    .line 2360
    .local v9, "uris":[Landroid/net/Uri;
    if-eqz v9, :cond_3

    .line 2362
    move-object v0, v9

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v8, v0, v5

    .line 2363
    .restart local v8    # "uri":Landroid/net/Uri;
    if-eqz v8, :cond_2

    .line 2364
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2362
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2370
    .end local v0    # "arr$":[Landroid/net/Uri;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "uri":Landroid/net/Uri;
    .end local v9    # "uris":[Landroid/net/Uri;
    :cond_3
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_4

    .line 2371
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/Uri;

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2372
    .local v7, "path":Ljava/lang/String;
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;)I

    move-result v12

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContentsType:I
    invoke-static {v11, v12}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5002(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;I)I

    .line 2373
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2374
    .local v2, "contentsSize":I
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_1
    if-ge v4, v2, :cond_4

    .line 2375
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/Uri;

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2376
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContentsType:I
    invoke-static {v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v11

    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;)I

    move-result v12

    if-eq v11, v12, :cond_6

    .line 2377
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/16 v12, 0xd

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContentsType:I
    invoke-static {v11, v12}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5002(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;I)I

    .line 2383
    .end local v2    # "contentsSize":I
    .end local v4    # "i":I
    .end local v7    # "path":Ljava/lang/String;
    :cond_4
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v11, v11, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-eqz v11, :cond_5

    .line 2384
    iget-object v11, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v11, v11, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    new-instance v12, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;

    invoke-direct {v12, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2413
    :cond_5
    return-void

    .line 2374
    .restart local v2    # "contentsSize":I
    .restart local v4    # "i":I
    .restart local v7    # "path":Ljava/lang/String;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
