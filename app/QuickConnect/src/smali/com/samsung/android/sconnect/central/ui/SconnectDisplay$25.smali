.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25;
.super Landroid/content/BroadcastReceiver;
.source "SconnectDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 2829
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2833
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2835
    .local v0, "action":Ljava/lang/String;
    const-string v3, "SconnectDisplay"

    const-string v4, "onReceive"

    const-string v5, "mReceiver : WIFI_AP_STATE_CHANGED_ACTION"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2837
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25;->isInitialStickyBroadcast()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2864
    :cond_0
    :goto_0
    return-void

    .line 2840
    :cond_1
    const-string v3, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2841
    const-string v3, "wifi_state"

    const/16 v4, 0xe

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2844
    .local v1, "result":I
    const-string v3, "SconnectDisplay"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mReceiver : EXTRA_WIFI_AP_STATE - Result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2846
    const/16 v3, 0xb

    if-ne v1, v3, :cond_0

    .line 2847
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMobileApEventReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/BroadcastReceiver;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2848
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25$1;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$25;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2860
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
