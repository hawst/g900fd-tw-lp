.class Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
.super Ljava/lang/Object;
.source "PreDiscoveryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BleLastMacAddr"
.end annotation


# instance fields
.field fifthAddr:B

.field sixthAddr:B

.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;BB)V
    .locals 1
    .param p2, "fifthAddr"    # B
    .param p3, "sixthAddr"    # B

    .prologue
    const/4 v0, 0x0

    .line 1490
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1487
    iput-byte v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->fifthAddr:B

    .line 1488
    iput-byte v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->sixthAddr:B

    .line 1491
    iput-byte p2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->fifthAddr:B

    .line 1492
    iput-byte p3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->sixthAddr:B

    .line 1493
    return-void
.end method

.method private getOuterType()Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    .locals 1

    .prologue
    .line 1520
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1497
    if-ne p0, p1, :cond_1

    .line 1516
    :cond_0
    :goto_0
    return v1

    .line 1500
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 1501
    goto :goto_0

    .line 1503
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 1504
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 1506
    check-cast v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;

    .line 1507
    .local v0, "other":Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->getOuterType()Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v3

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->getOuterType()Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 1508
    goto :goto_0

    .line 1510
    :cond_4
    iget-byte v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->fifthAddr:B

    iget-byte v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->fifthAddr:B

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 1511
    goto :goto_0

    .line 1513
    :cond_5
    iget-byte v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->sixthAddr:B

    iget-byte v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->sixthAddr:B

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 1514
    goto :goto_0
.end method

.method public get2Byte()[B
    .locals 3

    .prologue
    .line 1530
    const/4 v1, 0x2

    new-array v0, v1, [B

    const/4 v1, 0x0

    iget-byte v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->fifthAddr:B

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    iget-byte v2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->sixthAddr:B

    aput-byte v2, v0, v1

    .line 1533
    .local v0, "aaa":[B
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%02x"

    new-array v2, v5, [Ljava/lang/Object;

    iget-byte v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->fifthAddr:B

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%02x"

    new-array v2, v5, [Ljava/lang/Object;

    iget-byte v3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$BleLastMacAddr;->sixthAddr:B

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
