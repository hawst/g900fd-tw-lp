.class public Lcom/samsung/android/sconnect/central/ui/WaitingDialog;
.super Landroid/app/Activity;
.source "WaitingDialog.java"


# static fields
.field static final TAG:Ljava/lang/String; = "WaitingDialog"

.field private static final TIMEOUT_DELAY:I = 0x7530

.field private static final TIME_DELAY_MIN:I = 0x7d0


# instance fields
.field private MAX_COUNT:I

.field private final MSG_ALLDONE:I

.field private final MSG_TIMEOUT_ACCEPT:I

.field private final MSG_TIMEOUT_CONNECT:I

.field private mAction:I

.field private mContext:Landroid/content/Context;

.field private mDeviceView:Landroid/widget/LinearLayout;

.field private mFileNumber:I

.field private mFileType:I

.field protected mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

.field private mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

.field private mId:Ljava/lang/String;

.field private mIsAnswered:Z

.field private mIsConnecting:Z

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocalReceiver:Landroid/content/BroadcastReceiver;

.field private mParams:Landroid/widget/LinearLayout$LayoutParams;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mReg:Z

.field private mRequestSize:I

.field protected mResources:Landroid/content/res/Resources;

.field private mResponseCheckHandler:Landroid/os/Handler;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 61
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    .line 62
    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mRequestSize:I

    .line 64
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mDeviceView:Landroid/widget/LinearLayout;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 67
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mReg:Z

    .line 69
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 73
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MSG_ALLDONE:I

    .line 74
    const/16 v0, 0x7d1

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MSG_TIMEOUT_ACCEPT:I

    .line 75
    const/16 v0, 0x7d2

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MSG_TIMEOUT_CONNECT:I

    .line 77
    const/16 v0, 0xfa

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    .line 82
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 84
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mIsAnswered:Z

    .line 85
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mIsConnecting:Z

    .line 595
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$2;-><init>(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    .line 694
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;-><init>(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 820
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$4;-><init>(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mAction:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->waitingAnswerTimeout()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateState(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateConnectedDevice(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;
    .param p1, "x1"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    return-object p1
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->sendDisconnectForDecline()V

    return-void
.end method

.method private getFileShareMessage()Ljava/lang/String;
    .locals 9

    .prologue
    const v8, 0x7f09006f

    const v7, 0x7f09006e

    const v5, 0x7f090072

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 377
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileType:I

    packed-switch v0, :pswitch_data_0

    .line 462
    :pswitch_0
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_13

    .line 463
    const v0, 0x7f09007c

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 467
    :goto_0
    return-object v0

    .line 379
    :pswitch_1
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_0

    .line 380
    const v0, 0x7f090078

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 381
    :cond_0
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_1

    .line 382
    new-array v0, v4, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v7, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 384
    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "%d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v7, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 387
    :pswitch_2
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_2

    .line 388
    const v0, 0x7f09007e

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 389
    :cond_2
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_3

    .line 390
    const v0, 0x7f090074

    new-array v1, v4, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 392
    :cond_3
    const v0, 0x7f090074

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 395
    :pswitch_3
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_4

    .line 396
    const v0, 0x7f09007d

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 397
    :cond_4
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_5

    .line 398
    const v0, 0x7f090073

    new-array v1, v4, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 400
    :cond_5
    const v0, 0x7f090073

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 403
    :pswitch_4
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_6

    .line 404
    const v0, 0x7f09007f

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 406
    :cond_6
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "%d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 409
    :pswitch_5
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_7

    .line 410
    const v0, 0x7f09007b

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 411
    :cond_7
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_8

    .line 412
    const v0, 0x7f090071

    new-array v1, v4, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 414
    :cond_8
    const v0, 0x7f090071

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 417
    :pswitch_6
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_9

    .line 418
    const v0, 0x7f090083

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 419
    :cond_9
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_a

    .line 420
    const v0, 0x7f090077

    new-array v1, v4, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 422
    :cond_a
    const v0, 0x7f090077

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 425
    :pswitch_7
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_b

    .line 426
    const v0, 0x7f090080

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 427
    :cond_b
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_c

    .line 428
    const v0, 0x7f090075

    new-array v1, v4, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 430
    :cond_c
    const v0, 0x7f090075

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 433
    :pswitch_8
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_d

    .line 434
    const v0, 0x7f090082

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 435
    :cond_d
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_e

    .line 436
    new-array v0, v4, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 438
    :cond_e
    new-array v0, v4, [Ljava/lang/Object;

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 442
    :pswitch_9
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_f

    .line 443
    const v0, 0x7f090079

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 444
    :cond_f
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_10

    .line 445
    new-array v0, v4, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v8, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 447
    :cond_10
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "%d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v8, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 451
    :pswitch_a
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    if-ne v0, v4, :cond_11

    .line 452
    const v0, 0x7f09007a

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 453
    :cond_11
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_12

    .line 454
    const v0, 0x7f090070

    new-array v1, v4, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 456
    :cond_12
    const v0, 0x7f090070

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 464
    :cond_13
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    if-le v0, v1, :cond_14

    .line 465
    new-array v0, v4, [Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->MAX_COUNT:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 467
    :cond_14
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "%d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 377
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method private getIconResource()I
    .locals 2

    .prologue
    const v0, 0x7f020007

    .line 345
    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileType:I

    packed-switch v1, :pswitch_data_0

    .line 372
    :pswitch_0
    const v0, 0x7f02004e

    :goto_0
    :pswitch_1
    return v0

    .line 347
    :pswitch_2
    const v0, 0x7f020005

    goto :goto_0

    .line 349
    :pswitch_3
    const v0, 0x7f02009f

    goto :goto_0

    .line 351
    :pswitch_4
    const v0, 0x7f020052

    goto :goto_0

    .line 353
    :pswitch_5
    const v0, 0x7f020053

    goto :goto_0

    .line 359
    :pswitch_6
    const v0, 0x7f020006

    goto :goto_0

    .line 361
    :pswitch_7
    const v0, 0x7f02000a

    goto :goto_0

    .line 363
    :pswitch_8
    const v0, 0x7f020054

    goto :goto_0

    .line 366
    :pswitch_9
    const v0, 0x7f020055

    goto :goto_0

    .line 368
    :pswitch_a
    const v0, 0x7f020065

    goto :goto_0

    .line 345
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private releaseWakeLock()V
    .locals 3

    .prologue
    .line 562
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    const-string v0, "WaitingDialog"

    const-string v1, "releaseWakeLock"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 565
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 567
    :cond_0
    return-void
.end method

.method private sendDisconnectForDecline()V
    .locals 3

    .prologue
    .line 544
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.central.DISCONNECT_P2P"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 545
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "FORCE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 546
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 547
    return-void
.end method

.method private setDeviceView(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;Landroid/widget/LinearLayout;Z)V
    .locals 13
    .param p1, "info"    # Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    .param p2, "view"    # Landroid/widget/LinearLayout;
    .param p3, "isLarge"    # Z

    .prologue
    .line 270
    const v9, 0x7f0c0001

    invoke-virtual {p2, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 271
    .local v2, "deviceImageView":Landroid/widget/ImageView;
    const/high16 v9, 0x7f0c0000

    invoke-virtual {p2, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 273
    .local v1, "deviceImageBackgroundView":Landroid/widget/ImageView;
    const v9, 0x7f0c0002

    invoke-virtual {p2, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 274
    .local v3, "devicetextView":Landroid/widget/TextView;
    const/4 v8, -0x1

    .line 275
    .local v8, "type":I
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    iget-object v10, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mNumber:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getContactImage(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 277
    .local v6, "icon":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sconnect/SconnectManager;->getFavoriteList()Ljava/util/ArrayList;

    move-result-object v4

    .line 278
    .local v4, "favoriteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/central/ui/DisplayItem;>;"
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 279
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 280
    .local v7, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v7}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    .line 281
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    iget-object v9, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 282
    const-string v9, "WaitingDialog"

    const-string v10, "setDeviceView"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "It\'s favorite device: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImageType()I

    move-result v8

    .line 284
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getFavoriteImage()Landroid/graphics/Bitmap;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 285
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getFavoriteImage()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 291
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_1
    if-gtz v8, :cond_3

    if-eqz v6, :cond_3

    .line 297
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    const v10, 0x7f070023

    invoke-static {v9, v10, v6}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->cropIcon(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 316
    :goto_0
    iget-object v9, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget v9, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v9, v9, 0x8

    if-nez v9, :cond_2

    iget v9, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v9, v9, 0x10

    if-lez v9, :cond_2

    .line 319
    const/4 v9, 0x3

    iput v9, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 321
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateState(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    .line 322
    return-void

    .line 302
    :cond_3
    const/4 v9, 0x1

    if-ge v8, v9, :cond_4

    .line 303
    iget v8, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceType:I

    .line 305
    :cond_4
    const/4 v9, 0x1

    if-lt v8, v9, :cond_5

    const/16 v9, 0x1d

    if-le v8, v9, :cond_6

    .line 306
    :cond_5
    new-instance v9, Ljava/util/Random;

    invoke-direct {v9}, Ljava/util/Random;-><init>()V

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    add-int/lit8 v8, v9, 0x1a

    .line 308
    :cond_6
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_7

    if-nez p3, :cond_7

    .line 309
    sget-object v9, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceImagesLarge1:[I

    add-int/lit8 v10, v8, -0x1

    aget v9, v9, v10

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 310
    sget-object v9, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceBgImages:[I

    add-int/lit8 v10, v8, -0x1

    aget v9, v9, v10

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 312
    :cond_7
    sget-object v9, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceImagesLarge1:[I

    add-int/lit8 v10, v8, -0x1

    aget v9, v9, v10

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 313
    sget-object v9, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceBgImages:[I

    add-int/lit8 v10, v8, -0x1

    aget v9, v9, v10

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setIntentFilter()V
    .locals 4

    .prologue
    .line 570
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mReg:Z

    if-nez v2, :cond_0

    .line 571
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 572
    .local v1, "localIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "com.samsung.android.sconnect.central.REQUEST_RESULT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 573
    const-string v2, "com.samsung.android.sconnect.central.PLUGIN_PRINTER_CONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 574
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 577
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 578
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 579
    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 580
    const-string v2, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 581
    const-string v2, "com.samsung.android.sconnect.central.PRINTER"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 582
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 583
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mReg:Z

    .line 585
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v1    # "localIntentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private setView()V
    .locals 9

    .prologue
    const/4 v7, -0x2

    .line 169
    const v6, 0x7f030025

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->setContentView(I)V

    .line 170
    const v6, 0x7f0c00a8

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mDeviceView:Landroid/widget/LinearLayout;

    .line 178
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 180
    iget v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mRequestSize:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 181
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f070028

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 190
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mRequestSize:I

    if-ge v1, v6, :cond_4

    .line 191
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030014

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 193
    .local v4, "view":Landroid/widget/LinearLayout;
    if-lez v1, :cond_1

    .line 194
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 196
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mDeviceView:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 197
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    const/4 v7, 0x0

    invoke-direct {p0, v6, v4, v7}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->setDeviceView(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;Landroid/widget/LinearLayout;Z)V

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 183
    .end local v1    # "i":I
    .end local v4    # "view":Landroid/widget/LinearLayout;
    :cond_2
    iget v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mRequestSize:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_3

    .line 184
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f070029

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 186
    :cond_3
    iget v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mRequestSize:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_0

    .line 187
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f07002a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 201
    .restart local v1    # "i":I
    :cond_4
    const v6, 0x7f0c0003

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 202
    .local v2, "requestIconView":Landroid/widget/ImageView;
    const v6, 0x7f0c00a9

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 204
    .local v3, "requestTextView":Landroid/widget/TextView;
    const v6, 0x7f0c0026

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 206
    .local v0, "cancelButton":Landroid/widget/Button;
    iget v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mAction:I

    sparse-switch v6, :sswitch_data_0

    .line 248
    :goto_2
    new-instance v6, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$1;-><init>(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    iget-boolean v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mIsAnswered:Z

    if-eqz v6, :cond_5

    .line 260
    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 261
    iget-boolean v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mIsConnecting:Z

    if-eqz v6, :cond_5

    .line 262
    const v6, 0x7f0c00aa

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 263
    .local v5, "waitingView":Landroid/widget/TextView;
    const v6, 0x7f0900d1

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 267
    .end local v5    # "waitingView":Landroid/widget/TextView;
    :cond_5
    return-void

    .line 210
    :sswitch_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getFileShareMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getIconResource()I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 212
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/common/util/Util;->setSCRunningSetting(Landroid/content/Context;I)V

    goto :goto_2

    .line 215
    :sswitch_1
    const v6, 0x7f020080

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 217
    const v6, 0x7f09001e

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 220
    :sswitch_2
    const v6, 0x7f020074

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 221
    const v6, 0x7f09002c

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 224
    :sswitch_3
    const v6, 0x7f02007e

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 225
    const v6, 0x7f09002e

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 228
    :sswitch_4
    const v6, 0x7f020076

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 229
    const v6, 0x7f09001c

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 232
    :sswitch_5
    const v6, 0x7f02007b

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 233
    const v6, 0x7f090018

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 237
    :sswitch_6
    const v6, 0x7f09002d

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 240
    :sswitch_7
    const v6, 0x7f02007d

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 242
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    const v7, 0x7f090030

    invoke-virtual {v6, v7}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 206
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0xd -> :sswitch_5
        0x10 -> :sswitch_6
        0x11 -> :sswitch_2
        0x12 -> :sswitch_7
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method private setWakeLock()V
    .locals 4

    .prologue
    .line 550
    const-string v1, "WaitingDialog"

    const-string v2, "setWakeLock"

    const-string v3, "Screen locked: awake from sleep mode"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 553
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    const-string v2, "SCONNECT_WaitingDialog"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 556
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 557
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v2, 0x7918

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 559
    :cond_0
    return-void
.end method

.method private unregisterIntentFilter()V
    .locals 2

    .prologue
    .line 588
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mReg:Z

    if-eqz v0, :cond_0

    .line 589
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 590
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 591
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mReg:Z

    .line 593
    :cond_0
    return-void
.end method

.method private updateConnectedDevice(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .prologue
    const/4 v5, 0x3

    .line 473
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v3

    if-nez v3, :cond_2

    .line 477
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    .line 478
    .local v1, "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    if-eqz v1, :cond_0

    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v4, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 480
    iput v5, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto :goto_0

    .line 485
    .end local v1    # "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 486
    .local v2, "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v3, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v4, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 487
    iput v5, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto :goto_0
.end method

.method private updateState(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .prologue
    .line 325
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mDeviceView:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 327
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0c0060

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 329
    .local v1, "indicatorView":Landroid/widget/ImageView;
    :try_start_0
    iget v3, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    iget v3, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 331
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 332
    const v3, 0x7f02006a

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 342
    :cond_1
    :goto_0
    return-void

    .line 334
    :cond_2
    iget v3, p1, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 335
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 336
    const v3, 0x7f02006b

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v3, "WaitingDialog"

    const-string v4, "NullPointerException"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private waitingAnswerTimeout()V
    .locals 9

    .prologue
    const/16 v8, 0x7d0

    const/4 v6, 0x2

    const/4 v7, 0x1

    .line 496
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 497
    .local v0, "addressList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 498
    .local v2, "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-eqz v5, :cond_1

    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-ne v5, v6, :cond_3

    .line 500
    :cond_1
    iget-object v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 501
    iget-object v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    :cond_2
    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v5, v5, 0x8

    if-lez v5, :cond_3

    .line 504
    iput v6, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 505
    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateState(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    .line 508
    :cond_3
    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-eqz v5, :cond_4

    iget v5, v2, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-ne v5, v7, :cond_0

    .line 510
    :cond_4
    iput-boolean v7, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mIsConnecting:Z

    goto :goto_0

    .line 514
    .end local v2    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_5
    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mAction:I

    const/16 v6, 0xd

    if-ne v5, v6, :cond_9

    .line 515
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.samsung.android.sconnect.central.PRINTER_WAITING_CANCEL"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 516
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 517
    const-string v5, "ADDRESSLIST"

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 518
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 529
    :cond_6
    :goto_1
    iput-boolean v7, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mIsAnswered:Z

    .line 530
    const v5, 0x7f0c0026

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 531
    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mIsConnecting:Z

    if-eqz v5, :cond_7

    .line 532
    const v5, 0x7f0c00aa

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 533
    .local v4, "waitingView":Landroid/widget/TextView;
    const v5, 0x7f0900d1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 536
    .end local v4    # "waitingView":Landroid/widget/TextView;
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    if-eqz v5, :cond_8

    .line 537
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v5

    if-nez v5, :cond_8

    .line 538
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 541
    :cond_8
    return-void

    .line 521
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_9
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.samsung.android.sconnect.central.WAITING_CANCEL"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 522
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v5, "KEY"

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mId:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 523
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 524
    const-string v5, "ADDRESSLIST"

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 525
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_1
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 162
    const-string v0, "WaitingDialog"

    const-string v1, "onConfigurationChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "newConfing.orientation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->setView()V

    .line 165
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 166
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 89
    const-string v2, "WaitingDialog"

    const-string v3, "onCreate"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    .line 93
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResources:Landroid/content/res/Resources;

    .line 94
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 96
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 97
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_3

    .line 106
    const-string v2, "KEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mId:Ljava/lang/String;

    .line 107
    const-string v2, "CMD"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mAction:I

    .line 108
    const-string v2, "CONTENTTYPE"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileType:I

    .line 109
    const-string v2, "COUNT"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mFileNumber:I

    .line 111
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 112
    .local v0, "b":Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 113
    const-string v2, "DEVICE_INFO"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    .line 114
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 115
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mRequestSize:I

    .line 116
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->setView()V

    .line 117
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->setIntentFilter()V

    .line 118
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->setWakeLock()V

    .line 119
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    const/16 v3, 0x7d0

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 121
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    const/16 v3, 0x7d1

    const-wide/16 v4, 0x7530

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 123
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    const/16 v3, 0x7d2

    const-wide/32 v4, 0x1d4c0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 138
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 127
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_1
    const-string v2, "WaitingDialog"

    const-string v3, "onCreate"

    const-string v4, "ERROR - missed request device info"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    goto :goto_0

    .line 131
    :cond_2
    const-string v2, "WaitingDialog"

    const-string v3, "onCreate"

    const-string v4, "ERROR - missed bundle"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    goto :goto_0

    .line 135
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_3
    const-string v2, "WaitingDialog"

    const-string v3, "onCreate"

    const-string v4, "ERROR - missed Intent data"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 142
    const-string v0, "WaitingDialog"

    const-string v1, "onDestroy"

    const-string v2, "="

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 145
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    const/16 v1, 0x7d1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 146
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;

    const/16 v1, 0x7d2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 149
    :cond_0
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getPreDiscoveryHelper()Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 151
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getPreDiscoveryHelper()Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopCmdRequest()V

    .line 153
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->releaseWakeLock()V

    .line 154
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->waitingAnswerTimeout()V

    .line 155
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->unregisterIntentFilter()V

    .line 156
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/Util;->setSCRunningSetting(Landroid/content/Context;I)V

    .line 157
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 158
    return-void
.end method
