.class Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;
.super Ljava/lang/Object;
.source "SelectAutonaviMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->initAndStartAutonaviMap()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 252
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, ""

    const-string v4, "CurrentLocation - onClick"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->clearFocus()V

    .line 255
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    const-string v3, ""

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mLocation:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$302(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 256
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mResearch:Z
    invoke-static {v2, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$402(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Z)Z

    .line 259
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "gps"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 262
    .local v0, "gpsEnabled":Z
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "network"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    .line 265
    .local v1, "networkEnabled":Z
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 266
    const-string v2, "SelectAutonaviMapActivity"

    const-string v3, "mMenuDone"

    const-string v4, "gpsEnabled is false & networkEnabled is false"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09011f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->showMyLocation()V

    goto :goto_0
.end method
