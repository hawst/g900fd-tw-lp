.class public final Lcom/samsung/android/sconnect/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accept_btn:I = 0x7f0c0008

.field public static final action_icon:I = 0x7f0c000a

.field public static final action_icon_background:I = 0x7f0c0089

.field public static final action_list:I = 0x7f0c000c

.field public static final action_list_layout:I = 0x7f0c007d

.field public static final action_name:I = 0x7f0c000b

.field public static final actionbar:I = 0x7f0c001f

.field public static final actionbar_back_btn:I = 0x7f0c0021

.field public static final actionbar_btn_layout:I = 0x7f0c0020

.field public static final actionbar_text:I = 0x7f0c0022

.field public static final bg_device_1:I = 0x7f0c0071

.field public static final bg_device_2:I = 0x7f0c0076

.field public static final bg_device_3:I = 0x7f0c007e

.field public static final bottom_button_layout:I = 0x7f0c006f

.field public static final bt_checkbox:I = 0x7f0c005b

.field public static final bt_layout:I = 0x7f0c005a

.field public static final button_layout:I = 0x7f0c0006

.field public static final cancel_btn:I = 0x7f0c0026

.field public static final cant_find_btn:I = 0x7f0c006e

.field public static final cant_find_btn_when_center:I = 0x7f0c006d

.field public static final ch_down_btn:I = 0x7f0c0091

.field public static final ch_layout:I = 0x7f0c008f

.field public static final ch_up_btn:I = 0x7f0c0090

.field public static final check_icon:I = 0x7f0c0039

.field public static final checkbox:I = 0x7f0c001e

.field public static final china_popup_menu:I = 0x7f0c00af

.field public static final complete_btn:I = 0x7f0c0028

.field public static final connectable_device_info_textview_1:I = 0x7f0c0034

.field public static final connectable_device_info_textview_7:I = 0x7f0c0035

.field public static final connectable_device_info_textview_9:I = 0x7f0c0036

.field public static final connecting_text:I = 0x7f0c0009

.field public static final connection_message:I = 0x7f0c0005

.field public static final content:I = 0x7f0c004e

.field public static final contentPanel:I = 0x7f0c003d

.field public static final current_location:I = 0x7f0c0063

.field public static final custom_action_menu:I = 0x7f0c0012

.field public static final decline_btn:I = 0x7f0c0007

.field public static final device_icon:I = 0x7f0c0019

.field public static final device_icon_2:I = 0x7f0c0079

.field public static final device_icon_3:I = 0x7f0c0081

.field public static final device_icon_background:I = 0x7f0c0018

.field public static final device_icon_background_1:I = 0x7f0c0073

.field public static final device_icon_background_2:I = 0x7f0c0078

.field public static final device_icon_background_3:I = 0x7f0c0080

.field public static final device_icon_layout:I = 0x7f0c0017

.field public static final device_icon_layout_1:I = 0x7f0c0072

.field public static final device_icon_layout_2:I = 0x7f0c0077

.field public static final device_icon_layout_3:I = 0x7f0c007f

.field public static final device_image:I = 0x7f0c0001

.field public static final device_image_background:I = 0x7f0c0000

.field public static final device_layout:I = 0x7f0c0015

.field public static final device_listing_text:I = 0x7f0c0030

.field public static final device_name:I = 0x7f0c0002

.field public static final device_name_edit_text_popup:I = 0x7f0c0032

.field public static final device_name_layout:I = 0x7f0c001b

.field public static final device_name_layout_1:I = 0x7f0c0074

.field public static final device_name_layout_2:I = 0x7f0c007a

.field public static final device_name_layout_3:I = 0x7f0c0082

.field public static final device_name_menu:I = 0x7f0c00ae

.field public static final device_name_msg:I = 0x7f0c0033

.field public static final device_visibility_menu:I = 0x7f0c00ad

.field public static final expand_btn:I = 0x7f0c001d

.field public static final expand_btn_1:I = 0x7f0c0075

.field public static final expand_btn_2:I = 0x7f0c007c

.field public static final expanded_device_divider:I = 0x7f0c0016

.field public static final favorite_device_icon:I = 0x7f0c0038

.field public static final favorite_device_icon_background:I = 0x7f0c0037

.field public static final favorite_device_name_edit_text_popup:I = 0x7f0c003b

.field public static final favorite_icon_list:I = 0x7f0c003a

.field public static final favorite_list:I = 0x7f0c002d

.field public static final favorite_title:I = 0x7f0c002c

.field public static final handler:I = 0x7f0c004f

.field public static final help_menu:I = 0x7f0c00b0

.field public static final icon:I = 0x7f0c003c

.field public static final indicator_icon:I = 0x7f0c001a

.field public static final indicator_image:I = 0x7f0c0060

.field public static final initiating_layout:I = 0x7f0c0051

.field public static final initiating_progressbar:I = 0x7f0c0052

.field public static final initiating_text:I = 0x7f0c0053

.field public static final keypad_0_btn:I = 0x7f0c004b

.field public static final keypad_1_btn:I = 0x7f0c0041

.field public static final keypad_2_btn:I = 0x7f0c0042

.field public static final keypad_3_btn:I = 0x7f0c0043

.field public static final keypad_4_btn:I = 0x7f0c0044

.field public static final keypad_5_btn:I = 0x7f0c0045

.field public static final keypad_6_btn:I = 0x7f0c0046

.field public static final keypad_7_btn:I = 0x7f0c0047

.field public static final keypad_8_btn:I = 0x7f0c0048

.field public static final keypad_9_btn:I = 0x7f0c0049

.field public static final keypad_btn:I = 0x7f0c0099

.field public static final keypad_btn_layout:I = 0x7f0c0098

.field public static final keypad_btn_text:I = 0x7f0c009a

.field public static final keypad_delete_btn:I = 0x7f0c004a

.field public static final keypad_enter_btn:I = 0x7f0c004c

.field public static final keypad_layout:I = 0x7f0c0040

.field public static final list_layout:I = 0x7f0c002b

.field public static final map_main:I = 0x7f0c0061

.field public static final mapview_select:I = 0x7f0c0064

.field public static final menu_cancel:I = 0x7f0c0013

.field public static final menu_done:I = 0x7f0c0014

.field public static final message:I = 0x7f0c0004

.field public static final multiple_device_layout:I = 0x7f0c00a8

.field public static final mute_btn:I = 0x7f0c0093

.field public static final mute_btn_layout:I = 0x7f0c0092

.field public static final mute_btn_text:I = 0x7f0c0094

.field public static final nearby_list:I = 0x7f0c0031

.field public static final nearby_title:I = 0x7f0c002e

.field public static final nearby_title_textView:I = 0x7f0c002f

.field public static final new_tv_btn:I = 0x7f0c00a4

.field public static final new_tv_text:I = 0x7f0c00a3

.field public static final no_devices_text_view:I = 0x7f0c006c

.field public static final overlay_image:I = 0x7f0c0085

.field public static final power_btn:I = 0x7f0c008b

.field public static final primary_action_name:I = 0x7f0c001c

.field public static final quick_connect_start_layout:I = 0x7f0c0010

.field public static final radioBtn:I = 0x7f0c006b

.field public static final refresh_anim_view:I = 0x7f0c0025

.field public static final refresh_btn:I = 0x7f0c0024

.field public static final refresh_layout:I = 0x7f0c0023

.field public static final request_icon:I = 0x7f0c0003

.field public static final request_text:I = 0x7f0c00a9

.field public static final resolver_list:I = 0x7f0c003f

.field public static final rootview:I = 0x7f0c004d

.field public static final scrollview:I = 0x7f0c002a

.field public static final search_view:I = 0x7f0c0062

.field public static final searching_layout:I = 0x7f0c0054

.field public static final searching_progressbar:I = 0x7f0c0055

.field public static final searching_text:I = 0x7f0c0056

.field public static final select_location_actionbar_title:I = 0x7f0c000d

.field public static final select_location_cancel_btn:I = 0x7f0c000e

.field public static final select_location_done_btn:I = 0x7f0c000f

.field public static final selected_content_text:I = 0x7f0c0029

.field public static final setting_btn:I = 0x7f0c0027

.field public static final setting_visibility_layout:I = 0x7f0c0066

.field public static final setting_visibility_textView:I = 0x7f0c0067

.field public static final settings_description_textview:I = 0x7f0c0065

.field public static final source_btn:I = 0x7f0c0096

.field public static final source_btn_layout:I = 0x7f0c0095

.field public static final source_btn_text:I = 0x7f0c0097

.field public static final start_btn:I = 0x7f0c0068

.field public static final stub_device_layout:I = 0x7f0c0057

.field public static final stub_tv_main_layout:I = 0x7f0c0058

.field public static final text1:I = 0x7f0c0069

.field public static final text2:I = 0x7f0c006a

.field public static final title:I = 0x7f0c003e

.field public static final top_btn:I = 0x7f0c0050

.field public static final trouble_shooting_btn_layout:I = 0x7f0c0083

.field public static final tutorial_btn:I = 0x7f0c0011

.field public static final tutorial_main_bg_LinearLayout:I = 0x7f0c0070

.field public static final tutorial_page1_RelativeLayout:I = 0x7f0c0084

.field public static final tutorial_page2_RelativeLayout:I = 0x7f0c0086

.field public static final tutorial_page3_RelativeLayout:I = 0x7f0c0087

.field public static final tutorial_tv_action:I = 0x7f0c007b

.field public static final tv_action_item:I = 0x7f0c0088

.field public static final tv_action_list:I = 0x7f0c00a1

.field public static final tv_actionbar:I = 0x7f0c009b

.field public static final tv_actionbar_btn_layout:I = 0x7f0c009c

.field public static final tv_actionbar_text:I = 0x7f0c009e

.field public static final tv_back_btn:I = 0x7f0c009d

.field public static final tv_controller_layout:I = 0x7f0c008a

.field public static final tv_init_layout:I = 0x7f0c00a2

.field public static final tv_list_btn:I = 0x7f0c00a5

.field public static final tv_main_layout:I = 0x7f0c0059

.field public static final tv_selected_content_text:I = 0x7f0c00a0

.field public static final tv_setting_btn:I = 0x7f0c009f

.field public static final ui_action_cancel:I = 0x7f0c00ab

.field public static final ui_action_done:I = 0x7f0c00ac

.field public static final update_text_body:I = 0x7f0c00a7

.field public static final update_text_layout:I = 0x7f0c00a6

.field public static final userinfo_checkbox:I = 0x7f0c005f

.field public static final userinfo_layout:I = 0x7f0c005e

.field public static final vol_down_btn:I = 0x7f0c008e

.field public static final vol_layout:I = 0x7f0c008c

.field public static final vol_up_btn:I = 0x7f0c008d

.field public static final waiting_textview:I = 0x7f0c00aa

.field public static final wifi_checkbox:I = 0x7f0c005d

.field public static final wifi_layout:I = 0x7f0c005c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
