.class Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$DrawerToggler;
.super Ljava/lang/Object;
.source "CustomSlidingDrawer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrawerToggler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;)V
    .locals 0

    .prologue
    .line 943
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$DrawerToggler;->this$0:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$1;

    .prologue
    .line 943
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$DrawerToggler;-><init>(Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$DrawerToggler;->this$0:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    # getter for: Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->mLocked:Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->access$200(Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 958
    :goto_0
    return-void

    .line 953
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$DrawerToggler;->this$0:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    # getter for: Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->mAnimateOnClick:Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->access$300(Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 954
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$DrawerToggler;->this$0:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->animateToggle()V

    goto :goto_0

    .line 956
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$DrawerToggler;->this$0:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->toggle()V

    goto :goto_0
.end method
