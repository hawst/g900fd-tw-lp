.class Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;
.super Landroid/os/AsyncTask;
.source "SelectMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateLocationTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/maps/GeoPoint;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V
    .locals 0

    .prologue
    .line 972
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$1;

    .prologue
    .line 972
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 972
    check-cast p1, [Lcom/google/android/maps/GeoPoint;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;->doInBackground([Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    .locals 2
    .param p1, "point"    # [Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 975
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$800(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 972
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 980
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 981
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$UpdateLocationTask;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$702(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 985
    :cond_0
    return-void
.end method
