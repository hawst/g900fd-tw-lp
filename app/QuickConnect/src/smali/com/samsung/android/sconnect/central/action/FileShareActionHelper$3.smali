.class Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;
.super Ljava/lang/Object;
.source "FileShareActionHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 269
    const-string v1, "FileShareHelper"

    const-string v2, "onServiceConnected()"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-static {p2}, Lcom/samsung/android/sconnect/central/action/IFileShareService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sconnect/central/action/IFileShareService;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mService:Lcom/samsung/android/sconnect/central/action/IFileShareService;

    .line 274
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mService:Lcom/samsung/android/sconnect/central/action/IFileShareService;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    invoke-interface {v1, v2}, Lcom/samsung/android/sconnect/central/action/IFileShareService;->registerCallback(Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :goto_0
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 282
    const-string v1, "FileShareHelper"

    const-string v2, "onServiceDisconnected()"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mService:Lcom/samsung/android/sconnect/central/action/IFileShareService;

    if-eqz v1, :cond_0

    .line 285
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mService:Lcom/samsung/android/sconnect/central/action/IFileShareService;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    invoke-interface {v1, v2}, Lcom/samsung/android/sconnect/central/action/IFileShareService;->unregisterCallback(Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 286
    :catch_0
    move-exception v0

    .line 287
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
