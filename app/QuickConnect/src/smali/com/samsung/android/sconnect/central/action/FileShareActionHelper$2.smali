.class Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;
.super Landroid/os/Handler;
.source "FileShareActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 164
    const-string v1, "FileShareHelper"

    const-string v2, "handleMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 168
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->updateCurrentTransferStatus()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$300(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V

    goto :goto_0

    .line 172
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->isP2pConnection()Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$400(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 173
    const-string v1, "FileShareHelper"

    const-string v2, "sendFile"

    const-string v3, "p2p is not connected. FAILED!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 178
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$500(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 179
    const-string v1, "FileShareHelper"

    const-string v2, "sendFile"

    const-string v3, "bindService"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$500(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$600(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Landroid/content/ServiceConnection;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
