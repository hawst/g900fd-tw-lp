.class public interface abstract Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
.super Ljava/lang/Object;
.source "SconnListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/SconnListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "INetworkStateListener"
.end annotation


# virtual methods
.method public abstract onConnected(BLjava/lang/String;)V
.end method

.method public abstract onDisabled(B)V
.end method

.method public abstract onDisconnected(BLjava/lang/String;)V
.end method

.method public abstract onEnabled(B)V
.end method

.method public abstract onPoor(B)V
.end method
