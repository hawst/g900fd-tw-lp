.class Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;
.super Ljava/lang/Object;
.source "SelectAutonaviMapActivity.java"

# interfaces
.implements Lcom/amap/api/maps/AMap$OnMapLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V
    .locals 0

    .prologue
    .line 589
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapLongClick(Lcom/amap/api/maps/model/LatLng;)V
    .locals 11
    .param p1, "gcjLatLng"    # Lcom/amap/api/maps/model/LatLng;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 593
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 594
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->clearFocus()V

    .line 596
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    const v4, 0x7f090121

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 597
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 598
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1100(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9$1;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;)V

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 609
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, p1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v6, p1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1202(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;

    .line 610
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->GCJtoWGS(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    .line 611
    .local v1, "wgsLatLng":Lcom/amap/api/maps/model/LatLng;
    sput-boolean v8, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsLongpress:Z

    .line 613
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->checkInChinaArea()Z

    move-result v2

    if-nez v2, :cond_1

    .line 614
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-boolean v2, v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mIsAutoNaviNlp:Z

    if-eqz v2, :cond_0

    .line 615
    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-direct {v2, v3, v10}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;)V

    new-array v3, v8, [Lcom/amap/api/maps/model/LatLng;

    aput-object v1, v3, v9

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 629
    :goto_0
    return-void

    .line 617
    :cond_0
    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-direct {v2, v3, v10}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$1;)V

    new-array v3, v8, [Lcom/amap/api/maps/model/LatLng;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$OutofChinaLatLngTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 620
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1300(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/AMap;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 621
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mMap:Lcom/amap/api/maps/AMap;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1300(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/AMap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/maps/AMap;->clear()V

    .line 622
    :cond_2
    new-instance v0, Lcom/amap/api/services/core/LatLonPoint;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mGcjLatLng:Lcom/amap/api/maps/model/LatLng;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v4

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/services/core/LatLonPoint;-><init>(DD)V

    .line 623
    .local v0, "latLonPoint":Lcom/amap/api/services/core/LatLonPoint;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->getAddress(Lcom/amap/api/services/core/LatLonPoint;)V

    .line 626
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iput-wide v4, v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    .line 627
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$9;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iput-wide v4, v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    goto :goto_0
.end method
