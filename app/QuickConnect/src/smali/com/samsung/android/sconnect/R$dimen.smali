.class public final Lcom/samsung/android/sconnect/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_back_btn_width:I = 0x7f070012

.field public static final action_bar_height_with_header:I = 0x7f07003c

.field public static final action_bar_left_margin:I = 0x7f070011

.field public static final action_bar_switch_padding:I = 0x7f070008

.field public static final action_icon_size:I = 0x7f070003

.field public static final action_item_height:I = 0x7f070001

.field public static final action_item_left_margin:I = 0x7f070002

.field public static final action_list_bottom_margin_with_arrow:I = 0x7f070043

.field public static final action_list_divider_height:I = 0x7f070009

.field public static final action_list_top_margin_with_arrow:I = 0x7f070042

.field public static final action_name_left_margin:I = 0x7f070004

.field public static final action_name_right_margin:I = 0x7f070005

.field public static final activity_side_margin:I = 0x7f070000

.field public static final arrow_left_item_margin:I = 0x7f07000f

.field public static final arrow_right_item_margin:I = 0x7f070010

.field public static final bottom_button_height:I = 0x7f070036

.field public static final bottom_sheet_picker_height:I = 0x7f070041

.field public static final checkbox_size:I = 0x7f070035

.field public static final connectable_device_info_textsize:I = 0x7f07003b

.field public static final custom_actionbar_divider_height:I = 0x7f07002f

.field public static final custom_actionbar_divider_width:I = 0x7f07002e

.field public static final device_icon_circle_size:I = 0x7f070022

.field public static final device_icon_layout_width:I = 0x7f070015

.field public static final device_icon_left_margin:I = 0x7f07000c

.field public static final device_icon_size:I = 0x7f070021

.field public static final device_icon_size_large:I = 0x7f070023

.field public static final device_indicator_icon_bottom_margin:I = 0x7f070014

.field public static final device_indicator_icon_size:I = 0x7f070013

.field public static final device_item_height:I = 0x7f07000b

.field public static final device_list_divider_height:I = 0x7f07000e

.field public static final device_list_min_height:I = 0x7f07002d

.field public static final device_name_left_margin:I = 0x7f07000d

.field public static final device_name_text_size_large:I = 0x7f07003a

.field public static final device_name_text_size_normal:I = 0x7f070039

.field public static final dialog_device_icon_circle_size:I = 0x7f070027

.field public static final dialog_device_icon_size:I = 0x7f070026

.field public static final dialog_message_bottom_padding:I = 0x7f070032

.field public static final dialog_message_side_padding:I = 0x7f070030

.field public static final dialog_message_top_padding:I = 0x7f070031

.field public static final horizontal_spacing:I = 0x7f07000a

.field public static final indicator_icon_circle_size:I = 0x7f07002c

.field public static final indicator_icon_size:I = 0x7f07002b

.field public static final keypad_column_1_width:I = 0x7f07001e

.field public static final keypad_column_2_width:I = 0x7f07001f

.field public static final keypad_column_3_width:I = 0x7f070020

.field public static final keypad_dialog_width:I = 0x7f07003f

.field public static final keypad_left_margin:I = 0x7f07001d

.field public static final keypad_top_margin:I = 0x7f07001c

.field public static final quick_connect_description_height:I = 0x7f070038

.field public static final quick_connect_description_with_no_button_height:I = 0x7f070037

.field public static final radiobutton_marginEnd:I = 0x7f07003e

.field public static final radiobutton_marginStart:I = 0x7f07003d

.field public static final request_device_view_margin_2:I = 0x7f070028

.field public static final request_device_view_margin_3:I = 0x7f070029

.field public static final request_device_view_margin_4:I = 0x7f07002a

.field public static final seaching_text_left_margin:I = 0x7f070034

.field public static final seaching_text_size:I = 0x7f070033

.field public static final tooltip_toast_margin:I = 0x7f070040

.field public static final trouble_shooting_btn_bottom_margin:I = 0x7f070017

.field public static final trouble_shooting_btn_height:I = 0x7f07001b

.field public static final trouble_shooting_btn_height_when_center:I = 0x7f07001a

.field public static final trouble_shooting_btn_top_margin:I = 0x7f070016

.field public static final trouble_shooting_text_size:I = 0x7f070019

.field public static final trouble_shooting_text_size_when_center:I = 0x7f070018

.field public static final tv_action_icon_circle_size:I = 0x7f070025

.field public static final tv_action_icon_size:I = 0x7f070024

.field public static final tv_action_item_height:I = 0x7f070006

.field public static final tv_action_item_left_margin:I = 0x7f070007


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
