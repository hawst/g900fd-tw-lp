.class Lcom/samsung/android/sconnect/central/ui/ViewManager$3;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$OnDrawerCloseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/ViewManager;->setViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$3;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawerClosed()V
    .locals 3

    .prologue
    .line 291
    const-string v0, "ViewManager"

    const-string v1, "onDrawerClosed"

    const-string v2, "Finish activity"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$3;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 293
    return-void
.end method
