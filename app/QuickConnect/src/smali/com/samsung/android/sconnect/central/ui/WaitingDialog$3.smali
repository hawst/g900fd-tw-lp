.class Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;
.super Landroid/content/BroadcastReceiver;
.source "WaitingDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/WaitingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)V
    .locals 0

    .prologue
    .line 694
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 697
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 698
    .local v1, "action":Ljava/lang/String;
    const-string v15, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 699
    const-string v15, "wifiP2pDeviceList"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v14

    check-cast v14, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .line 701
    .local v14, "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    const-string v15, "connectedDevAddress"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 703
    .local v5, "connectedDevAddr":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 704
    const/4 v6, 0x0

    .line 705
    .local v6, "device":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 706
    .local v10, "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget-object v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-eqz v15, :cond_0

    iget-object v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v5, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 707
    move-object v6, v10

    .line 711
    .end local v10    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    :cond_1
    if-nez v6, :cond_3

    .line 817
    .end local v5    # "connectedDevAddr":Ljava/lang/String;
    .end local v6    # "device":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v14    # "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    :cond_2
    :goto_0
    return-void

    .line 714
    .restart local v5    # "connectedDevAddr":Ljava/lang/String;
    .restart local v6    # "device":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v14    # "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    :cond_3
    invoke-virtual {v14, v5}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->get(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v4

    .line 715
    .local v4, "changedDevice":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget v15, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-nez v15, :cond_5

    iget v15, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v15, v15, 0x8

    if-nez v15, :cond_5

    const/4 v3, 0x1

    .line 716
    .local v3, "bP2pConnecting":Z
    :goto_1
    iget v15, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    iget v15, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v15, v15, 0x8

    if-lez v15, :cond_6

    const/4 v2, 0x1

    .line 717
    .local v2, "bBleAccepted":Z
    :goto_2
    if-eqz v4, :cond_a

    .line 718
    iget v15, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    .line 719
    const-string v15, "WaitingDialog"

    const-string v16, "mBroadcastReceiver"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "PEERS_CHANGED -FAILED :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    if-eqz v3, :cond_7

    .line 722
    const/4 v15, 0x2

    iput v15, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 740
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # invokes: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->updateState(Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V
    invoke-static {v15, v6}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$500(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;)V

    .line 741
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v15

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v15

    const/16 v16, 0x7d0

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v15

    if-nez v15, :cond_2

    .line 743
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v15

    const/16 v16, 0x7d0

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 715
    .end local v2    # "bBleAccepted":Z
    .end local v3    # "bP2pConnecting":Z
    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 716
    .restart local v3    # "bP2pConnecting":Z
    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    .line 723
    .restart local v2    # "bBleAccepted":Z
    :cond_7
    if-eqz v2, :cond_4

    .line 724
    const/4 v15, 0x2

    iput v15, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto :goto_3

    .line 726
    :cond_8
    iget v15, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-nez v15, :cond_4

    .line 727
    const-string v15, "WaitingDialog"

    const-string v16, "mBroadcastReceiver"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "PEERS_CHANGED -CONNECTED :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v4, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    if-nez v3, :cond_9

    if-eqz v2, :cond_4

    .line 730
    :cond_9
    const/4 v15, 0x3

    iput v15, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto :goto_3

    .line 734
    :cond_a
    const-string v15, "WaitingDialog"

    const-string v16, "mBroadcastReceiver"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "PEERS_CHANGED -NO DEVICE :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    if-nez v3, :cond_b

    if-eqz v2, :cond_4

    .line 737
    :cond_b
    const/4 v15, 0x3

    iput v15, v6, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto/16 :goto_3

    .line 746
    .end local v2    # "bBleAccepted":Z
    .end local v3    # "bP2pConnecting":Z
    .end local v4    # "changedDevice":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v5    # "connectedDevAddr":Ljava/lang/String;
    .end local v6    # "device":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v14    # "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    :cond_c
    const-string v15, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1a

    .line 747
    const-string v15, "networkInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Landroid/net/NetworkInfo;

    .line 749
    .local v11, "netInfo":Landroid/net/NetworkInfo;
    const-string v15, "connectedDevAddress"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 752
    .restart local v5    # "connectedDevAddr":Ljava/lang/String;
    if-nez v11, :cond_d

    .line 753
    const-string v15, "WaitingDialog"

    const-string v16, "mBroadcastReceiver"

    const-string v17, "CONNECTION_CHANGED - no netInfo"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 756
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    move-object/from16 v16, v0

    const-string v15, "p2pGroupInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Landroid/net/wifi/p2p/WifiP2pGroup;

    move-object/from16 v0, v16

    # setter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v0, v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$802(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;Landroid/net/wifi/p2p/WifiP2pGroup;)Landroid/net/wifi/p2p/WifiP2pGroup;

    .line 758
    invoke-virtual {v11}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v15

    if-eqz v15, :cond_17

    .line 759
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mGroupInfo:Landroid/net/wifi/p2p/WifiP2pGroup;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$800(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/net/wifi/p2p/WifiP2pGroup;

    move-result-object v7

    .line 760
    .local v7, "groupInfo":Landroid/net/wifi/p2p/WifiP2pGroup;
    if-eqz v7, :cond_11

    .line 761
    invoke-virtual {v7}, Landroid/net/wifi/p2p/WifiP2pGroup;->isGroupOwner()Z

    move-result v15

    if-nez v15, :cond_12

    .line 762
    invoke-virtual {v7}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v12

    .line 763
    .local v12, "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_11

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 764
    .restart local v10    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget-object v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-eqz v15, :cond_e

    iget-object v15, v12, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v0, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 766
    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-nez v15, :cond_f

    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v15, v15, 0x8

    if-eqz v15, :cond_10

    :cond_f
    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v15, v15, 0x8

    if-lez v15, :cond_e

    .line 768
    :cond_10
    const/4 v15, 0x3

    iput v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 808
    .end local v7    # "groupInfo":Landroid/net/wifi/p2p/WifiP2pGroup;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    .end local v12    # "ownerDev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_11
    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v15

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v15

    const/16 v16, 0x7d0

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v15

    if-nez v15, :cond_2

    .line 810
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mResponseCheckHandler:Landroid/os/Handler;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$700(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Landroid/os/Handler;

    move-result-object v15

    const/16 v16, 0x7d0

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 775
    .restart local v7    # "groupInfo":Landroid/net/wifi/p2p/WifiP2pGroup;
    :cond_12
    invoke-virtual {v7}, Landroid/net/wifi/p2p/WifiP2pGroup;->getClientList()Ljava/util/Collection;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_13
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_11

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 776
    .local v13, "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_14
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_13

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 777
    .restart local v10    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget-object v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-eqz v15, :cond_14

    iget-object v15, v13, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v0, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_14

    .line 779
    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-nez v15, :cond_15

    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v15, v15, 0x8

    if-eqz v15, :cond_16

    :cond_15
    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_14

    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v15, v15, 0x8

    if-lez v15, :cond_14

    .line 781
    :cond_16
    const/4 v15, 0x3

    iput v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto :goto_5

    .line 790
    .end local v7    # "groupInfo":Landroid/net/wifi/p2p/WifiP2pGroup;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    .end local v13    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_17
    if-eqz v5, :cond_11

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_11

    .line 791
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    # getter for: Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->mList:Ljava/util/ArrayList;
    invoke-static {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->access$300(Lcom/samsung/android/sconnect/central/ui/WaitingDialog;)Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_18
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_11

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;

    .line 792
    .restart local v10    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    iget-object v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    if-eqz v15, :cond_18

    iget-object v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v5, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_18

    .line 793
    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    if-nez v15, :cond_19

    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v15, v15, 0x8

    if-nez v15, :cond_19

    .line 795
    const-string v15, "WaitingDialog"

    const-string v16, "mBroadcastReceiver"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "CONNECTION_CHANGED disconnect: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    const/4 v15, 0x2

    iput v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto/16 :goto_4

    .line 799
    :cond_19
    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_18

    iget v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    and-int/lit8 v15, v15, 0x8

    if-lez v15, :cond_18

    .line 801
    const/4 v15, 0x2

    iput v15, v10, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    goto/16 :goto_4

    .line 813
    .end local v5    # "connectedDevAddr":Ljava/lang/String;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "info":Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
    .end local v11    # "netInfo":Landroid/net/NetworkInfo;
    :cond_1a
    const-string v15, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 814
    const-string v15, "WaitingDialog"

    const-string v16, "mBroadcastReceiver"

    const-string v17, "ACTION_SCREEN_OFF : auto cancel"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/sconnect/central/ui/WaitingDialog$3;->this$0:Lcom/samsung/android/sconnect/central/ui/WaitingDialog;

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/central/ui/WaitingDialog;->finish()V

    goto/16 :goto_0
.end method
