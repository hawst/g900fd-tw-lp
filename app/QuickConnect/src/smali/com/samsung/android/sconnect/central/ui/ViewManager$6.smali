.class Lcom/samsung/android/sconnect/central/ui/ViewManager$6;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshFavoriteDeviceList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$6;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 382
    const-string v2, "ViewManager"

    const-string v3, "refreshFavoriteDeviceList"

    const-string v4, "mFavoriteListView post, add device list"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$6;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$800(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 384
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$6;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$900(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 385
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 386
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$6;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$6;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$900(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v3, v2, v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z

    .line 385
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 388
    :cond_0
    return-void
.end method
