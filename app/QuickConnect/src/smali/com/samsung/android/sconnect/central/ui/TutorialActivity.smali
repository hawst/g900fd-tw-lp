.class public Lcom/samsung/android/sconnect/central/ui/TutorialActivity;
.super Landroid/app/Activity;
.source "TutorialActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TutorialActivity"

.field public static final TUTORIAL_KEY:Ljava/lang/String; = "isTutorialMode"

.field public static TutorialActivitytoFinish:Landroid/app/Activity;


# instance fields
.field private final PAGE_1:I

.field private final PAGE_2:I

.field private final PAGE_3:I

.field private mContentsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mIsContentsForwardLock:Z

.field private mIsMerlot:Z

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mMimeType:Ljava/lang/String;

.field private mPageNumber:I

.field private mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 38
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->PAGE_1:I

    .line 39
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->PAGE_2:I

    .line 40
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->PAGE_3:I

    .line 42
    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mPageNumber:I

    .line 44
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 45
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 50
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsContentsForwardLock:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsMerlot:Z

    .line 52
    iput-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mMimeType:Ljava/lang/String;

    .line 312
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$3;-><init>(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mPageNumber:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setPage2View()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setPage3View()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsContentsForwardLock:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mContentsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private registerBroadcast()V
    .locals 2

    .prologue
    .line 298
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 299
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 300
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 301
    return-void
.end method

.method private setInitSettings()V
    .locals 3

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sconnect/common/util/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    sget v2, Lcom/samsung/android/sconnect/common/util/Util;->DISABLED:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/Util;->saveToDb(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 309
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sconnect/common/util/Util;->KEY_CONTACT_ONLY:Ljava/lang/String;

    sget v2, Lcom/samsung/android/sconnect/common/util/Util;->ENABLED:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/Util;->saveToDb(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 310
    return-void
.end method

.method private setPage1View()V
    .locals 5

    .prologue
    .line 217
    const/4 v2, 0x1

    iput v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mPageNumber:I

    .line 219
    const v2, 0x7f0c0084

    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 220
    .local v1, "pageLayout":Landroid/widget/RelativeLayout;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 224
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsMerlot:Z

    if-eqz v2, :cond_0

    .line 225
    const-string v2, "TutorialActivity"

    const-string v3, "setPage1View"

    const-string v4, "change overlay image for 1520px"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const v2, 0x7f0c0085

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 227
    .local v0, "overlayImageView":Landroid/widget/ImageView;
    const v2, 0x7f020096

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 229
    .end local v0    # "overlayImageView":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method private setPage2View()V
    .locals 7

    .prologue
    .line 232
    const/4 v4, 0x2

    iput v4, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mPageNumber:I

    .line 234
    const v4, 0x7f0c0086

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 235
    .local v3, "pageLayout":Landroid/widget/RelativeLayout;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 248
    const v4, 0x7f0c007d

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 249
    .local v0, "actionListLayout":Landroid/widget/LinearLayout;
    const v4, 0x7f0c007c

    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 250
    .local v1, "expandBtn":Landroid/widget/Button;
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 251
    const v4, 0x7f02004c

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 253
    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsMerlot:Z

    if-eqz v4, :cond_0

    .line 254
    const-string v4, "TutorialActivity"

    const-string v5, "setPage2View"

    const-string v6, "change overlay image for 1520px"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const v4, 0x7f0c0085

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 256
    .local v2, "overlayImageView":Landroid/widget/ImageView;
    const v4, 0x7f020099

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 260
    .end local v2    # "overlayImageView":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method private setPage3View()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 263
    const/4 v6, 0x3

    iput v6, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mPageNumber:I

    .line 265
    const v6, 0x7f0c0087

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 266
    .local v3, "pageLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 268
    const v4, 0x7f02009a

    .line 269
    .local v4, "resId":I
    const v6, 0x7f0c0085

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 276
    .local v2, "overlayImageView":Landroid/widget/ImageView;
    const v6, 0x7f0c007d

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 277
    .local v0, "actionListLayout":Landroid/widget/LinearLayout;
    const v6, 0x7f0c007c

    invoke-virtual {p0, v6}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 278
    .local v1, "expandBtn":Landroid/widget/Button;
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 279
    const v6, 0x7f02004d

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 281
    iget-boolean v6, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsMerlot:Z

    if-eqz v6, :cond_0

    .line 282
    const-string v6, "TutorialActivity"

    const-string v7, "setPage3View"

    const-string v8, "change overlay image for 1520px"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const v4, 0x7f02009b

    .line 284
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 288
    :cond_0
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/Util;->isLocaleRTL()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 289
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->reverseLeftRight(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 290
    .local v5, "rtlImage":Landroid/graphics/Bitmap;
    if-eqz v5, :cond_1

    .line 291
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 295
    .end local v5    # "rtlImage":Landroid/graphics/Bitmap;
    :cond_1
    return-void
.end method

.method private unregisterBroadcast()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 305
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 197
    const-string v1, "TutorialActivity"

    const-string v2, "onDestroy"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mPageNumber : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mPageNumber:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mPageNumber:I

    packed-switch v1, :pswitch_data_0

    .line 214
    :goto_0
    return-void

    .line 201
    :pswitch_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 204
    :pswitch_1
    const v1, 0x7f0c0086

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 205
    .local v0, "pageLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 206
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setPage1View()V

    goto :goto_0

    .line 209
    .end local v0    # "pageLayout":Landroid/widget/RelativeLayout;
    :pswitch_2
    const v1, 0x7f0c0087

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 210
    .restart local v0    # "pageLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 211
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setPage2View()V

    goto :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 56
    const-string v9, "TutorialActivity"

    const-string v10, "onCreate"

    const-string v11, ""

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    sput-object p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->TutorialActivitytoFinish:Landroid/app/Activity;

    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 60
    .local v2, "input":Landroid/content/Intent;
    const/4 v3, 0x0

    .line 61
    .local v3, "isLastPage":Z
    if-eqz v2, :cond_0

    .line 62
    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {v2, v9}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mContentsList:Ljava/util/ArrayList;

    .line 63
    const-string v9, "FORWARD_LOCK"

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsContentsForwardLock:Z

    .line 64
    const-string v9, "TUTORIAL_LAST_PAGE"

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 65
    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mMimeType:Ljava/lang/String;

    .line 66
    const-string v9, "TutorialActivity"

    const-string v10, "onCreate"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mContentsList : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_0
    const-string v9, "SconnectDisplay"

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 70
    .local v6, "preference":Landroid/content/SharedPreferences;
    const-string v9, "TUTORIAL_SETTING_ALIVE"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 71
    .local v4, "isTutorialSettingAlive":Z
    if-eqz v4, :cond_1

    .line 72
    const-string v9, "TutorialActivity"

    const-string v10, "onCreate"

    const-string v11, "isTutorialSettingAlive, call settings and finish"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v7, Landroid/content/Intent;

    const-class v9, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-direct {v7, p0, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    .local v7, "settingIntent":Landroid/content/Intent;
    const/high16 v9, 0x24000000

    invoke-virtual {v7, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 76
    const-string v9, "isTutorial"

    const/4 v10, 0x1

    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    const-string v9, "FORWARD_LOCK"

    iget-boolean v10, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsContentsForwardLock:Z

    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 78
    const-string v9, "android.intent.extra.STREAM"

    iget-object v10, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 79
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mMimeType:Ljava/lang/String;

    invoke-virtual {v7, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    invoke-virtual {p0, v7}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->startActivity(Landroid/content/Intent;)V

    .line 81
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->finish()V

    .line 166
    .end local v7    # "settingIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 84
    :cond_1
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 85
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 86
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v9

    if-nez v9, :cond_3

    iget v9, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v10, 0x5f0

    if-lt v9, v10, :cond_3

    const/4 v9, 0x1

    :goto_1
    iput-boolean v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsMerlot:Z

    .line 87
    const-string v9, "TutorialActivity"

    const-string v10, "onCreate"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "display width: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const v9, 0x7f03001b

    invoke-virtual {p0, v9}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setContentView(I)V

    .line 90
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mResources:Landroid/content/res/Resources;

    .line 92
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isIRSupport(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 93
    const v9, 0x7f0c007b

    invoke-virtual {p0, v9}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 94
    .local v8, "tvActionText":Landroid/widget/TextView;
    const v9, 0x7f090026

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 103
    .end local v8    # "tvActionText":Landroid/widget/TextView;
    :cond_2
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setRequestedOrientation(I)V

    .line 106
    const-string v9, "quickconnect"

    invoke-virtual {p0, v9}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 108
    new-instance v9, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$1;

    invoke-direct {v9, p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$1;-><init>(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)V

    iput-object v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 113
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v10, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 115
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setInitSettings()V

    .line 117
    const v9, 0x7f0c006f

    invoke-virtual {p0, v9}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 118
    .local v0, "bottomButtonLayout":Landroid/widget/LinearLayout;
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 119
    const/4 v5, 0x0

    .line 125
    .local v5, "nextButton":Landroid/view/View;
    const v9, 0x7f0c0011

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 128
    new-instance v9, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;

    invoke-direct {v9, p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;-><init>(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)V

    invoke-virtual {v5, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    if-eqz v3, :cond_4

    .line 161
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setPage3View()V

    .line 165
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->registerBroadcast()V

    goto/16 :goto_0

    .line 86
    .end local v0    # "bottomButtonLayout":Landroid/widget/LinearLayout;
    .end local v5    # "nextButton":Landroid/view/View;
    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 163
    .restart local v0    # "bottomButtonLayout":Landroid/widget/LinearLayout;
    .restart local v5    # "nextButton":Landroid/view/View;
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setPage1View()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 183
    const-string v0, "TutorialActivity"

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 186
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    .line 187
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->unregisterBroadcast()V

    .line 188
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 189
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/quickconnect/QuickConnectManager;->terminate()V

    .line 190
    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 191
    iput-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 193
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 170
    const-string v0, "TutorialActivity"

    const-string v1, "onNewIntent"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    if-eqz p1, :cond_0

    .line 172
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mContentsList:Ljava/util/ArrayList;

    .line 173
    const-string v0, "FORWARD_LOCK"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsContentsForwardLock:Z

    .line 174
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mMimeType:Ljava/lang/String;

    .line 175
    const-string v0, "TutorialActivity"

    const-string v1, "onNewIntent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mContentsList : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mContentsList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", FORWARD_LOCK: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsContentsForwardLock:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 179
    return-void
.end method
