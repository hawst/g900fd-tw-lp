.class public final enum Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;
.super Ljava/lang/Enum;
.source "SconnectDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SconnectLayout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

.field public static final enum FINISHING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

.field public static final enum INITIATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

.field public static final enum MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

.field public static final enum MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

.field public static final enum NONE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

.field public static final enum TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

.field public static final enum WATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 167
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->NONE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    const-string v1, "MULTIPLE_CHOICE_MODE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    const-string v1, "MAIN"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    const-string v1, "TV"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    const-string v1, "WATING"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->WATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    const-string v1, "INITIATING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->INITIATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    new-instance v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    const-string v1, "FINISHING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->FINISHING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 166
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->NONE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->WATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->INITIATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->FINISHING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->$VALUES:[Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 166
    const-class v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->$VALUES:[Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    invoke-virtual {v0}, [Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    return-object v0
.end method
