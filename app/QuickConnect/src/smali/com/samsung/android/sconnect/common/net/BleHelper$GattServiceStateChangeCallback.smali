.class Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;
.super Ljava/lang/Object;
.source "BleHelper.java"

# interfaces
.implements Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GattServiceStateChangeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/common/net/BleHelper;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/common/net/BleHelper;Lcom/samsung/android/sconnect/common/net/BleHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/common/net/BleHelper$1;

    .prologue
    .line 294
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;-><init>(Lcom/samsung/android/sconnect/common/net/BleHelper;)V

    return-void
.end method


# virtual methods
.method public onGattServiceStateChange(ZLandroid/bluetooth/IBluetoothGatt;)V
    .locals 12
    .param p1, "up"    # Z
    .param p2, "iGatt"    # Landroid/bluetooth/IBluetoothGatt;

    .prologue
    const-wide/16 v10, 0x12c

    const/4 v8, 0x0

    const/4 v7, 0x3

    const/16 v6, 0xa

    const/4 v5, 0x1

    .line 298
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[up]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " [IBluetoothGatt]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    if-eqz p1, :cond_2

    .line 300
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v0

    .line 301
    .local v0, "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->isGattServiceReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 302
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    const-string v3, "(up_state) isGattServiceReady FALSE"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    .end local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_0
    :goto_0
    return-void

    .line 305
    .restart local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$100(Lcom/samsung/android/sconnect/common/net/BleHelper;)I

    move-result v1

    const/16 v2, 0xb

    if-ne v1, v2, :cond_0

    .line 306
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    const-string v3, "(up_state) Radio turned on! [mLeRadioState]LE_RADIO_STATE_TURNING_ON >> LE_RADIO_STATE_ON"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    const/16 v2, 0xc

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$102(Lcom/samsung/android/sconnect/common/net/BleHelper;I)I

    .line 309
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$200(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 310
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$200(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v7, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 313
    .end local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$100(Lcom/samsung/android/sconnect/common/net/BleHelper;)I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_3

    .line 314
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    const-string v3, "(down_state) Radio turned off! [mLeRadioState]LE_RADIO_STATE_TURNING_OFF >> LE_RADIO_STATE_OFF"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I
    invoke-static {v1, v6}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$102(Lcom/samsung/android/sconnect/common/net/BleHelper;I)I

    goto :goto_0

    .line 317
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$100(Lcom/samsung/android/sconnect/common/net/BleHelper;)I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 318
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "(down_state) recover Radio...[mLeRadioState]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$100(Lcom/samsung/android/sconnect/common/net/BleHelper;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " >> LE_RADIO_STATE_OFF"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I
    invoke-static {v1, v6}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$102(Lcom/samsung/android/sconnect/common/net/BleHelper;I)I

    .line 322
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$300(Lcom/samsung/android/sconnect/common/net/BleHelper;)Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 323
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;
    invoke-static {v1, v8}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$302(Lcom/samsung/android/sconnect/common/net/BleHelper;Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;)Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    .line 325
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$400(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 326
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1, v8}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$402(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/BluetoothAdapter;

    .line 328
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$200(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 329
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$200(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 330
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$200(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0
.end method
