.class public Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;
.super Ljava/lang/Object;
.source "SmartHomeActionHelper.java"


# static fields
.field private static final SMART_HOME_ACTION:Ljava/lang/String; = ""

.field private static final SMART_HOME_PACKAGE_NAME:Ljava/lang/String; = ""

.field private static final TAG:Ljava/lang/String; = "SmartHomeActionHelper"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;->mContext:Landroid/content/Context;

    .line 20
    const-string v0, "SmartHomeActionHelper"

    const-string v1, "SmartHomeActionHelper"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;->mContext:Landroid/content/Context;

    .line 23
    return-void
.end method

.method private isAppRun()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method private showDialog()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method


# virtual methods
.method public startSmartHome()V
    .locals 4

    .prologue
    .line 27
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;->mContext:Landroid/content/Context;

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 28
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;->isAppRun()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 30
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;->showDialog()V

    .line 42
    :goto_0
    return-void

    .line 33
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 34
    .local v1, "launchApp":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 38
    .end local v1    # "launchApp":Landroid/content/Intent;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "market://details?id="

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 40
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/SmartHomeActionHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
