.class Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;
.super Ljava/lang/Thread;
.source "P2pHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/P2pHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "P2pHelperWorkThread"
.end annotation


# static fields
.field private static final DISCOVERY_THREAD_EXIT:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "p2pHelperWorkThread"


# instance fields
.field private mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 3

    .prologue
    .line 1265
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1263
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 1266
    const-string v0, "p2pHelperWorkThread"

    const-string v1, "p2pHelperWorkThread"

    const-string v2, "created!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 1268
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->setDaemon(Z)V

    .line 1269
    const-string v0, "SConnectP2pHelperWorkThread"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->setName(Ljava/lang/String;)V

    .line 1270
    return-void
.end method

.method private declared-synchronized processMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v11, 0x70

    .line 1322
    monitor-enter p0

    :try_start_0
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .line 1324
    .local v10, "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    invoke-virtual {v10}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 1325
    .local v9, "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v1, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->SConnectInfo:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->SConnectInfo:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ne v1, v11, :cond_1

    .line 1326
    const/4 v6, 0x0

    .line 1328
    .local v6, "data":Ljava/lang/String;
    :try_start_1
    iget-object v1, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->SConnectInfo:Ljava/lang/String;

    const/4 v2, 0x2

    const/16 v3, 0x70

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 1332
    :goto_1
    if-eqz v6, :cond_0

    :try_start_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x6e

    if-ne v1, v2, :cond_0

    .line 1333
    const-string v1, "p2pHelperWorkThread"

    const-string v2, "processMessage "

    const-string v3, "receive SCONNECT_PROBE_RESP"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2500(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iget-object v3, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-static {v6}, Lcom/samsung/android/sconnect/common/util/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;->onReceived(ILjava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1322
    .end local v6    # "data":Ljava/lang/String;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v10    # "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1329
    .restart local v6    # "data":Ljava/lang/String;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v9    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    .restart local v10    # "peerList":Landroid/net/wifi/p2p/WifiP2pDeviceList;
    :catch_0
    move-exception v7

    .line 1330
    .local v7, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_3
    const-string v1, "p2pHelperWorkThread"

    const-string v2, "processMessage"

    const-string v3, "IndexOutOfBoundsException"

    invoke-static {v1, v2, v3, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1338
    .end local v6    # "data":Ljava/lang/String;
    .end local v7    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->isBusyDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    invoke-static {v1, v9}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$600(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1339
    const-string v1, "p2pHelperWorkThread"

    const-string v2, "processMessage "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "busy: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1344
    :cond_2
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    const-string v1, ""

    iget-object v2, v9, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1346
    .local v0, "deviceP2p":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/util/ArrayList;

    move-result-object v2

    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1347
    :try_start_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_3

    .line 1348
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mCurSearchedDeviceList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1349
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x0

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->DeviceAdd(Landroid/net/wifi/p2p/WifiP2pDevice;Z)V
    invoke-static {v1, v9, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2000(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;Z)V

    .line 1351
    :cond_3
    monitor-exit v2

    goto/16 :goto_0

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1354
    .end local v0    # "deviceP2p":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    .end local v9    # "peer":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_4
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public cleanMessages()V
    .locals 3

    .prologue
    .line 1297
    const-string v0, "p2pHelperWorkThread"

    const-string v1, "cleanMessages"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1298
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 1299
    return-void
.end method

.method public run()V
    .locals 5

    .prologue
    .line 1305
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    .line 1308
    .local v1, "msg":Landroid/os/Message;
    iget v2, v1, Landroid/os/Message;->what:I

    const/16 v3, 0x3e8

    if-ne v2, v3, :cond_0

    .line 1309
    const-string v2, "p2pHelperWorkThread"

    const-string v3, "run"

    const-string v4, "break while loop"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1318
    return-void

    .line 1313
    :cond_0
    invoke-direct {p0, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->processMessage(Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1314
    .end local v1    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 1315
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "p2pHelperWorkThread"

    const-string v3, "run"

    const-string v4, "InterruptedException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public declared-synchronized sendMessage(Landroid/os/Message;)I
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1275
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1280
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return v1

    .line 1276
    :catch_0
    move-exception v0

    .line 1277
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v1, "p2pHelperWorkThread"

    const-string v2, "sendMessage"

    const-string v3, "InterruptedException"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1278
    const/4 v1, -0x1

    goto :goto_0

    .line 1275
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public terminate()V
    .locals 5

    .prologue
    .line 1284
    const-string v2, "p2pHelperWorkThread"

    const-string v3, "terminate"

    const-string v4, "--"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1285
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 1287
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 1288
    .local v1, "msg":Landroid/os/Message;
    const/16 v2, 0x3e8

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1290
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->mMessageQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1294
    :goto_0
    return-void

    .line 1291
    :catch_0
    move-exception v0

    .line 1292
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "p2pHelperWorkThread"

    const-string v3, "terminate"

    const-string v4, "InterruptedException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
