.class Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;
.super Ljava/util/TimerTask;
.source "PreDiscoveryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->startAutoScanTimer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 423
    const-string v0, "PreDiscoveryHelper"

    const-string v1, "TimerTask"

    const-string v2, "RUN BLE DISCOVERY by AUTO-SCAN TIMER"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryReq()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 425
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 427
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mNeedP2pFind:Z
    invoke-static {v0, v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$202(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    .line 429
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 430
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessage(I)Z

    .line 433
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 434
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v0

    const-wide/16 v2, 0x1b58

    invoke-virtual {v0, v4, v2, v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 436
    return-void
.end method
