.class public interface abstract Lcom/samsung/android/sconnect/extern/IBleProxyService;
.super Ljava/lang/Object;
.source "IBleProxyService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/extern/IBleProxyService$Stub;
    }
.end annotation


# virtual methods
.method public abstract registerCallback(Lcom/samsung/android/sconnect/extern/IBleProxyCallback;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract unregisterCallback(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
