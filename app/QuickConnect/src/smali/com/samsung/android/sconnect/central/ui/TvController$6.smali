.class Lcom/samsung/android/sconnect/central/ui/TvController$6;
.super Ljava/lang/Object;
.source "TvController.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/TvController;->showTvList(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

.field final synthetic val$bUpdate:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V
    .locals 0

    .prologue
    .line 927
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    iput-boolean p2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->val$bUpdate:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 930
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mTvAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1100(Lcom/samsung/android/sconnect/central/ui/TvController;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 931
    .local v0, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mTvMap:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1300(Lcom/samsung/android/sconnect/central/ui/TvController;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    # setter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static {v2, v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1202(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    .line 932
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 933
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->showTvController(Z)V
    invoke-static {v1, v4}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1400(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V

    .line 936
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->val$bUpdate:Z

    if-eqz v1, :cond_1

    .line 937
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$400(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v3

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->updateTvOnDB(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1500(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V

    .line 941
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$500(Lcom/samsung/android/sconnect/central/ui/TvController;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$500(Lcom/samsung/android/sconnect/central/ui/TvController;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0900ac

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 943
    return-void

    .line 939
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$400(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController$6;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSelectedTv:Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;

    move-result-object v3

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->registerTvOnDB(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V
    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$1600(Lcom/samsung/android/sconnect/central/ui/TvController;Lcom/samsung/android/sconnect/central/ui/DisplayItem;Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;)V

    goto :goto_0
.end method
