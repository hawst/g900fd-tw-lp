.class Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$4;
.super Ljava/lang/Object;
.source "FavoriteDeviceManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showAddIconDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$4;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v4, 0x1

    .line 475
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 476
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$4;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$900(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .line 477
    .local v1, "sconnectDisplay":Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    packed-switch p2, :pswitch_data_0

    .line 493
    :goto_0
    const/16 v2, 0x3ec

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    .line 495
    return-void

    .line 479
    :pswitch_0
    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 480
    const-string v2, "com.sec.android.app.camera"

    const-string v3, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    const-string v2, "Rcs_Camera_Key"

    const-string v3, "Rcs_Camera_Request"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 483
    const-string v2, "return-uri"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 486
    :pswitch_1
    const-string v2, "android.intent.action.PICK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    const-string v2, "image/*"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 488
    const-string v2, "android.intent.extra.LOCAL_ONLY"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 477
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
