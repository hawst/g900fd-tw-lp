.class public Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;
.super Ljava/lang/Object;
.source "UpdateSamsungApps.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/update/UpdateSamsungApps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StubDownloadResult"
.end annotation


# instance fields
.field appId:Ljava/lang/String;

.field contentSize:Ljava/lang/String;

.field downloadURI:Ljava/lang/String;

.field productId:Ljava/lang/String;

.field productName:Ljava/lang/String;

.field resultCode:Ljava/lang/String;

.field resultMsg:Ljava/lang/String;

.field versionCode:I

.field versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->appId:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->resultCode:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->resultMsg:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->downloadURI:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->contentSize:Ljava/lang/String;

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->versionCode:I

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->versionName:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->productId:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubDownloadResult;->productName:Ljava/lang/String;

    return-void
.end method
