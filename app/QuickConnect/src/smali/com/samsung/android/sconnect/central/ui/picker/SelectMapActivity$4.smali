.class Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;
.super Landroid/os/Handler;
.source "SelectMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$600(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "map halder, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 281
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getMyLocation()Lcom/google/android/maps/GeoPoint;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$500(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$402(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;

    .line 282
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 283
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$1;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$600(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const-string v2, "search point is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getLocationServiceAvailable()Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$1000(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09011f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 302
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$700(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->searchLocation()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$100(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)V

    goto :goto_0

    .line 310
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$700(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$600(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPDATE_LOCATION_FROM_GET_MYLOCATION: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$700(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$700(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 317
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$2;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
