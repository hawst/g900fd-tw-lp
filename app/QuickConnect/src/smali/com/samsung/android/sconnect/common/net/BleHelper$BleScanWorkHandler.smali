.class Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;
.super Landroid/os/Handler;
.source "BleHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BleScanWorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 799
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    .line 800
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 801
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 805
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 858
    :goto_0
    return-void

    .line 808
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    .line 809
    .local v1, "isLcdOn":Ljava/lang/Boolean;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 810
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 811
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mIsScanFilter_LCD_ON:Z
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1400(Lcom/samsung/android/sconnect/common/net/BleHelper;)Z

    move-result v3

    if-eq v2, v3, :cond_0

    .line 812
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopLeScan()V

    .line 814
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 819
    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 820
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mIsScanFilter_LCD_ON:Z
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1402(Lcom/samsung/android/sconnect/common/net/BleHelper;Z)Z

    .line 821
    const-string v2, "BleHelper"

    const-string v3, "startScan"

    const-string v4, "call BleScanner.startScan(LCD_ON)"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_ON:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1500(Lcom/samsung/android/sconnect/common/net/BleHelper;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/BleHelper;->getMyScanPreferences()Landroid/bluetooth/le/ScanSettings;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanSettings;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;
    invoke-static {v5}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1700(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanCallback;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V

    .line 835
    :goto_2
    const-string v2, "BleHelper"

    const-string v3, "startScan"

    const-string v4, "call BleScanner.startScan() complete"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    const-wide/16 v2, 0x64

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 838
    :catch_0
    move-exception v0

    .line 839
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 815
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 816
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 825
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mIsScanFilter_LCD_ON:Z
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1402(Lcom/samsung/android/sconnect/common/net/BleHelper;Z)Z

    .line 826
    const-string v2, "BleHelper"

    const-string v3, "startScan"

    const-string v4, "call BleScanner.startScan(LDC_OFF)"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_OFF:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1800(Lcom/samsung/android/sconnect/common/net/BleHelper;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/BleHelper;->getMyScanPreferences()Landroid/bluetooth/le/ScanSettings;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanSettings;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;
    invoke-static {v5}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1700(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanCallback;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V

    goto :goto_2

    .line 831
    :cond_2
    const-string v2, "BleHelper"

    const-string v3, "startScan"

    const-string v4, "call BleScanner.startScan()"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/BleHelper;->getMyScanPreferences()Landroid/bluetooth/le/ScanSettings;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1600(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanSettings;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;
    invoke-static {v5}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1700(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanCallback;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V

    goto :goto_2

    .line 842
    :cond_3
    const-string v2, "BleHelper"

    const-string v3, "startScan"

    const-string v4, "mBleScanner is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 846
    .end local v1    # "isLcdOn":Ljava/lang/Boolean;
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 847
    const-string v2, "BleHelper"

    const-string v3, "stopScan"

    const-string v4, "call BleScanner.stopScan()"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->access$1700(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanCallback;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V

    .line 849
    const-string v2, "BleHelper"

    const-string v3, "stopScan"

    const-string v4, "call BleScanner.stopScan() complete"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 851
    :cond_4
    const-string v2, "BleHelper"

    const-string v3, "stopScan"

    const-string v4, "mBleScanner is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 805
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
