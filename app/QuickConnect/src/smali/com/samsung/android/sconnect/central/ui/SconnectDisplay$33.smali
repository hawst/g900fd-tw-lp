.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->askForTogether(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$action:I

.field final synthetic val$contentType:I

.field final synthetic val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

.field final synthetic val$selectedUris:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 3287
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iput p3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->val$action:I

    iput-object p4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->val$selectedUris:Ljava/util/ArrayList;

    iput p5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->val$contentType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 3290
    const-string v0, "SconnectDisplay"

    const-string v1, "askForTogether"

    const-string v2, "onPositiveButton"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3291
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsForwardLock:Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3292
    const-string v0, "SconnectDisplay"

    const-string v1, "askForTogether"

    const-string v2, "onPositiveButton but bIsContentsForwardLock"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3294
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090114

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3299
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 3300
    return-void

    .line 3297
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->val$action:I

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->val$selectedUris:Ljava/util/ArrayList;

    const-string v4, ""

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$33;->val$contentType:I

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    goto :goto_0
.end method
