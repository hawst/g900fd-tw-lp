.class Lcom/samsung/android/sconnect/common/net/P2pHelper$12;
.super Landroid/os/Handler;
.source "P2pHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/P2pHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0

    .prologue
    .line 1198
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 1202
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 1248
    const-string v2, "P2pHelper"

    const-string v3, "mDiscoveryHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UNKNOWN_MSG: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    :cond_0
    :goto_0
    return-void

    .line 1204
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1205
    const-string v2, "P2pHelper"

    const-string v3, "mDiscoveryHandler"

    const-string v4, "START_DISCOVERY"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const-wide/16 v4, 0x4e20

    invoke-virtual {v2, v4, v5, v6, v6}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startFindP2p(JZZ)V

    goto :goto_0

    .line 1208
    :cond_1
    const-string v2, "P2pHelper"

    const-string v3, "mDiscoveryHandler"

    const-string v4, "START_DISCOVERY - ignore"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1212
    :pswitch_1
    const-string v2, "P2pHelper"

    const-string v3, "mDiscoveryHandler"

    const-string v4, "STOP_DISCOVERY"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1213
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->stopDiscoverP2p()V

    goto :goto_0

    .line 1216
    :pswitch_2
    const-string v2, "P2pHelper"

    const-string v3, "mDiscoveryHandler"

    const-string v4, "WAITING_PEER"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->waitingPeer()V

    goto :goto_0

    .line 1220
    :pswitch_3
    const-string v2, "P2pHelper"

    const-string v3, "mDiscoveryHandler"

    const-string v4, "CONNECT_TIMEOUT"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$702(Lcom/samsung/android/sconnect/common/net/P2pHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 1222
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1223
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->cancelP2pConnectRequest()V

    .line 1224
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1225
    .local v0, "addr":Ljava/lang/String;
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1226
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v2

    sget-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    invoke-interface {v2, v3, v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisconnected(BLjava/lang/String;)V

    goto :goto_1

    .line 1229
    .end local v0    # "addr":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1231
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1232
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->requestPeerList()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$400(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V

    .line 1234
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->setIgnoreGoDevice(Z)V

    goto/16 :goto_0

    .line 1237
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pConnectedState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1238
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWaitingTime:J
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$2700(Lcom/samsung/android/sconnect/common/net/P2pHelper;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x19a28

    cmp-long v2, v2, v4

    if-gez v2, :cond_6

    .line 1239
    const-string v2, "P2pHelper"

    const-string v3, "mDiscoveryHandler"

    const-string v4, "MSG_REMOVE_P2PCONFIRM - keep"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    const/4 v3, 0x1

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeP2pConfirm(Z)V
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1400(Lcom/samsung/android/sconnect/common/net/P2pHelper;Z)V

    .line 1241
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$12;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x5

    const-wide/16 v4, 0x4650

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1243
    :cond_6
    const-string v2, "P2pHelper"

    const-string v3, "mDiscoveryHandler"

    const-string v4, "MSG_REMOVE_P2PCONFIRM - timeout"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1202
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
