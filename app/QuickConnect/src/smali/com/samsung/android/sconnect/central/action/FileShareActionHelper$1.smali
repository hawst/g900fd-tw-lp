.class Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;
.super Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback$Stub;
.source "FileShareActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "mac"    # Ljava/lang/String;
    .param p3, "sessionID"    # Ljava/lang/String;
    .param p4, "filePath"    # Ljava/lang/String;
    .param p5, "state"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 59
    const-string v1, "FileShareHelper"

    const-string v2, "stateChanged"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v2

    monitor-enter v2

    .line 65
    :try_start_0
    const-string v1, "STATUS_STARTED"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "STATUS_PROGRESS"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 67
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Add TransferList:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .local v0, "item":Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    .end local v0    # "item":Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    invoke-virtual {v1, p3, p4}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->addSessionInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$200(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 133
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$200(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :try_start_1
    const-string v1, "STATUS_COMPLETED"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "STATUS_FAILED"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 135
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 136
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$200(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    move-result-object v1

    const-string v4, "STATUS_PROGRESS"

    invoke-interface {v1, p1, p2, v4}, Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v4, "FileShareHelper"

    const-string v5, "stateChanged"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NOT FINISHED("

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "): "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->access$100(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 147
    :cond_4
    :try_start_2
    monitor-exit v2

    .line 148
    return-void

    .line 72
    :cond_5
    const-string v1, "STATUS_COMPLETED"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 73
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 74
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->isContainedSessionID(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 75
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->access$100(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, p4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 76
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Remove:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->access$100(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, p4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 79
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->access$100(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 80
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Remove empty sessionID:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->removeSessionID(Ljava/lang/String;)Z

    .line 84
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->getSessionSize()I

    move-result v1

    if-nez v1, :cond_2

    .line 85
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Remove empty TransferItem:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 90
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    const-string v4, "TransferList is Empty"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 147
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 95
    :cond_6
    :try_start_3
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No FilePath:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 99
    :cond_7
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No SessionID:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 103
    :cond_8
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No TransferItem:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 105
    :cond_9
    const-string v1, "STATUS_FAILED"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 107
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 108
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->isContainedSessionID(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 109
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "remove sessionID:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->mSession:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->access$100(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->getSessionSize()I

    move-result v1

    if-nez v1, :cond_2

    .line 113
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Remove empty TransferItem:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 118
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    const-string v4, "TransferList is Empty"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    :cond_a
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No SessionID:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 126
    :cond_b
    const-string v1, "FileShareHelper"

    const-string v3, "stateChanged"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No TransferItem:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 140
    :cond_c
    :try_start_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$200(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    move-result-object v1

    invoke-interface {v1, p1, p2, p5}, Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 145
    :catchall_1
    move-exception v1

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 143
    :cond_d
    :try_start_6
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->access$200(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    move-result-object v1

    invoke-interface {v1, p1, p2, p5}, Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1
.end method
