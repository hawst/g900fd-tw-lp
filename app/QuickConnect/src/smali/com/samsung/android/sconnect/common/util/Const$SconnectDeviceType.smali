.class public Lcom/samsung/android/sconnect/common/util/Const$SconnectDeviceType;
.super Ljava/lang/Object;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/util/Const;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SconnectDeviceType"
.end annotation


# static fields
.field public static final ACCESSORY_INPUT:I = 0xf

.field public static final ACCESSORY_OUTPUT:I = 0x10

.field public static final AIR_CONDITIONER:I = 0x17

.field public static final BD_PLAYER:I = 0x11

.field public static final CAMCORDER:I = 0x19

.field public static final CAMERA:I = 0x7

.field public static final CHROMECAST:I = 0x13

.field public static final DLNA:I = 0xc

.field public static final DLNA_AUDIO:I = 0xd

.field public static final DVD_PLAYER:I = 0x14

.field public static final GROUP_PLAY_AP:I = 0x3

.field public static final HOMESYNC:I = 0xe

.field public static final HTS:I = 0x12

.field public static final LAPTOP:I = 0x5

.field public static final MIRRORING_PLAYER:I = 0xb

.field public static final MOBILE:I = 0x1

.field public static final PC:I = 0x4

.field public static final PRINTER:I = 0x9

.field public static final REFRIGERATOR:I = 0x18

.field public static final ROBOT_VACUUM:I = 0x15

.field public static final TABLET:I = 0x2

.field public static final TOGETHER_ROOM:I = 0xa

.field public static final TV:I = 0x6

.field public static final UNKNOWN_1:I = 0x1a

.field public static final UNKNOWN_2:I = 0x1b

.field public static final UNKNOWN_3:I = 0x1c

.field public static final UNKNOWN_4:I = 0x1d

.field public static final WASHING_MACHINE:I = 0x16

.field public static final WEARABLE:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
