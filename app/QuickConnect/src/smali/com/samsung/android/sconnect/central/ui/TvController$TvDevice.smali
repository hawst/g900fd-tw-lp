.class public Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;
.super Ljava/lang/Object;
.source "TvController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/TvController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TvDevice"
.end annotation


# instance fields
.field mBrandName:Ljava/lang/String;

.field mDevice:Ltv/peel/samsung/widget/service/Device;

.field mDeviceType:Ljava/lang/String;

.field mRoomID:Ljava/lang/String;

.field mRoomName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;)V
    .locals 1
    .param p1, "r"    # Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;
    .param p2, "d"    # Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    invoke-virtual {p1}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->getRoomId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Lcom/sec/msc/android/yosemite/infrastructure/model/room/Room;->getRoomName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomName:Ljava/lang/String;

    .line 109
    invoke-virtual {p2}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getDeviceType()Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/msc/android/yosemite/infrastructure/constant/remocon/RemoteControlDeviceType;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    .line 110
    invoke-virtual {p2}, Lcom/sec/msc/android/yosemite/infrastructure/model/remotecontrol/RemoteControlSetting;->getBrandName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    .line 111
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "brand"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    .line 123
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomName:Ljava/lang/String;

    .line 124
    iput-object p3, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    .line 125
    iput-object p4, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    .line 126
    return-void
.end method

.method constructor <init>(Ltv/peel/samsung/widget/service/Room;Ltv/peel/samsung/widget/service/Device;)V
    .locals 1
    .param p1, "r"    # Ltv/peel/samsung/widget/service/Room;
    .param p2, "d"    # Ltv/peel/samsung/widget/service/Device;

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    invoke-virtual {p1}, Ltv/peel/samsung/widget/service/Room;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Ltv/peel/samsung/widget/service/Room;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomName:Ljava/lang/String;

    .line 116
    invoke-virtual {p2}, Ltv/peel/samsung/widget/service/Device;->getType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    .line 117
    invoke-virtual {p2}, Ltv/peel/samsung/widget/service/Device;->getBrandName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    .line 118
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDevice:Ltv/peel/samsung/widget/service/Device;

    .line 119
    return-void
.end method


# virtual methods
.method public getString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "brand"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomID:Ljava/lang/String;

    .line 138
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mRoomName:Ljava/lang/String;

    .line 139
    iput-object p3, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDeviceType:Ljava/lang/String;

    .line 140
    iput-object p4, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mBrandName:Ljava/lang/String;

    .line 141
    return-void
.end method

.method public updateDevice(Ltv/peel/samsung/widget/service/Device;)V
    .locals 0
    .param p1, "d"    # Ltv/peel/samsung/widget/service/Device;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$TvDevice;->mDevice:Ltv/peel/samsung/widget/service/Device;

    .line 134
    return-void
.end method
