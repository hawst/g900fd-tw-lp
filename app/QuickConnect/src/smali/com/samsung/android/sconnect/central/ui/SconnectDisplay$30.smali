.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->askForGroupPlay(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$action:I

.field final synthetic val$contentType:I

.field final synthetic val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

.field final synthetic val$masterMode:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;ZLcom/samsung/android/sconnect/common/device/SconnectDevice;II)V
    .locals 0

    .prologue
    .line 3164
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-boolean p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$masterMode:Z

    iput-object p3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iput p4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$action:I

    iput p5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$contentType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x0

    .line 3167
    const-string v0, "SconnectDisplay"

    const-string v1, "askForGroupPlay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNegativeButton masterMode="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$masterMode:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3168
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$masterMode:Z

    if-eqz v0, :cond_0

    .line 3169
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$action:I

    const-string v4, ""

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$contentType:I

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 3175
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 3176
    return-void

    .line 3171
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceGroupPlay()Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;->mGpName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$30;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceGroupPlay()Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;->mGpMac:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v3}, Lcom/samsung/android/sconnect/central/action/GroupPlayActionHelper;->launchGroupPlayAsSlave(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0
.end method
