.class Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;
.super Landroid/content/BroadcastReceiver;
.source "PrinterActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 510
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 511
    .local v0, "action":Ljava/lang/String;
    const-string v3, "PrinterActionHelper"

    const-string v4, "onReceive: "

    invoke-static {v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const-string v3, "com.android.printspooler.ACTION_SELECTED_PRINTER_SAVED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 514
    const-string v3, "PrinterActionHelper"

    const-string v4, "onReceive"

    const-string v5, "ACTION_SELECTED_PRINTER_SAVED"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->releaseBroadcastReceiver()V
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)V

    .line 518
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mUris:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$100(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)Ljava/util/ArrayList;

    move-result-object v4

    # invokes: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->getFilePathFromUri(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    invoke-static {v3, v4}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$200(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 520
    .local v1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContentsType:I
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$300(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 521
    const-string v3, "PrinterActionHelper"

    const-string v4, "onReceive"

    const-string v5, "Print image files"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->printImage(Ljava/util/ArrayList;)V
    invoke-static {v3, v1}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$400(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;Ljava/util/ArrayList;)V

    .line 528
    :cond_0
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.android.sconnect.central.PRINTER_FINISHED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 529
    .local v2, "localIntent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$600(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 531
    .end local v1    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "localIntent":Landroid/content/Intent;
    :cond_1
    return-void

    .line 523
    .restart local v1    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mUris:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$100(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/Util;->isPdfContents(Ljava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 524
    const-string v3, "PrinterActionHelper"

    const-string v4, "onReceive"

    const-string v5, "Print pdf files"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->printPdf(Ljava/util/ArrayList;)V
    invoke-static {v3, v1}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$500(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;Ljava/util/ArrayList;)V

    goto :goto_0
.end method
