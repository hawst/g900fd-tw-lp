.class public Lcom/samsung/android/sconnect/central/ActionFormatter;
.super Ljava/lang/Object;
.source "ActionFormatter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ActionFormatter"


# instance fields
.field private mContentsType:I

.field private mContext:Landroid/content/Context;

.field private mIsContentsForwardLock:Z

.field private mIsPrintable:Z

.field private mRelatedActionList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;IZJZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentsType"    # I
    .param p3, "isPrintable"    # Z
    .param p4, "relatedActions"    # J
    .param p6, "isContentsForwardLock"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    .line 27
    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsPrintable:Z

    .line 28
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mRelatedActionList:Ljava/util/HashSet;

    .line 29
    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsContentsForwardLock:Z

    .line 33
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    .line 34
    iput p2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContentsType:I

    .line 35
    iput-boolean p3, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsPrintable:Z

    .line 36
    iput-boolean p6, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsContentsForwardLock:Z

    .line 37
    invoke-virtual {p0, p4, p5}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setRelatedActionList(J)V

    .line 38
    return-void
.end method

.method private checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "action"    # I
    .param p3, "isContentRelatedMode"    # Z

    .prologue
    const/4 v0, 0x0

    .line 541
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v1

    if-gez v1, :cond_1

    .line 542
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 549
    :cond_0
    :goto_0
    return v0

    .line 545
    :cond_1
    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mRelatedActionList:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 546
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setChromecastAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    .line 439
    const/16 v0, 0x13

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 440
    return-void
.end method

.method private setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "action"    # I
    .param p3, "isContentRelatedMode"    # Z

    .prologue
    .line 535
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mRelatedActionList:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 536
    :cond_0
    invoke-virtual {p1, p2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setPrimaryAction(I)V

    .line 538
    :cond_1
    return-void
.end method


# virtual methods
.method public setActionList(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 111
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setDefaultAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 114
    :goto_0
    return-void

    .line 54
    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setMobileAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 57
    :pswitch_2
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setTabletAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 60
    :pswitch_3
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setGroupPlayAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 64
    :pswitch_4
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPcAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 67
    :pswitch_5
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setTvAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 70
    :pswitch_6
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setCameraAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 73
    :pswitch_7
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setWearableAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 76
    :pswitch_8
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrinterAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 79
    :pswitch_9
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setTogetherAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 82
    :pswitch_a
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setMirroringAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 86
    :pswitch_b
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setDLNAAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 89
    :pswitch_c
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setHomesyncAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 93
    :pswitch_d
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setBDplayerAndHTSAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 96
    :pswitch_e
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setChromecastAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public setBDplayerAndHTSAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/4 v3, 0x5

    .line 477
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 478
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v1, 0xa

    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 479
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-lez v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getServices()I

    move-result v1

    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    and-int/2addr v1, v2

    if-lez v1, :cond_0

    .line 481
    invoke-direct {p0, p1, v3, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 486
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 488
    :cond_1
    return-void
.end method

.method public setCameraAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    .line 363
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_0

    .line 364
    const/16 v0, 0xf

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 366
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-lez v0, :cond_1

    .line 367
    const/16 v0, 0xc

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 369
    :cond_1
    return-void
.end method

.method public setDLNAAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    .line 435
    const/16 v0, 0xa

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 436
    return-void
.end method

.method public setDefaultAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 7
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/16 v6, 0xc

    const/4 v5, 0x5

    const/4 v4, 0x1

    .line 491
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 492
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    .line 494
    .local v1, "netType":I
    and-int/lit8 v2, v1, 0x4

    if-lez v2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v2

    const/high16 v3, 0x100000

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothClass;->hasService(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 497
    iget v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContentsType:I

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsContentsForwardLock:Z

    if-nez v2, :cond_5

    .line 498
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 499
    invoke-direct {p0, p1, v4, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 500
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 508
    :cond_0
    :goto_0
    and-int/lit8 v2, v1, 0x4

    if-gtz v2, :cond_1

    and-int/lit8 v2, v1, 0x8

    if-lez v2, :cond_2

    .line 509
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v2

    if-gez v2, :cond_6

    .line 510
    invoke-direct {p0, p1, v6, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 517
    :cond_2
    :goto_1
    and-int/lit8 v2, v1, 0x2

    if-lez v2, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getServices()I

    move-result v2

    sget v3, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    and-int/2addr v2, v3

    if-lez v2, :cond_3

    .line 519
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v2

    if-gez v2, :cond_7

    .line 520
    invoke-direct {p0, p1, v5, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 521
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceType(I)V

    .line 529
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 530
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 532
    :cond_4
    return-void

    .line 503
    :cond_5
    invoke-direct {p0, p1, v4, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    goto :goto_0

    .line 512
    :cond_6
    invoke-direct {p0, p1, v6, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 513
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 523
    :cond_7
    invoke-direct {p0, p1, v5, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 524
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public setGroupPlayAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v1, "com.samsung.groupcast"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    const/16 v0, 0xe

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    const-string v0, "ActionFormatter"

    const-string v1, "setGroupPlayAction"

    const-string v2, "GroupPlay not Available"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHomesyncAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/16 v2, 0xa

    const/4 v5, 0x5

    .line 443
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 444
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x7

    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 446
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-lez v1, :cond_0

    .line 447
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v1

    if-gez v1, :cond_3

    .line 448
    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 456
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-lez v1, :cond_1

    .line 458
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getServices()I

    move-result v1

    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    and-int/2addr v1, v2

    if-lez v1, :cond_1

    .line 459
    const-string v1, "ActionFormatter"

    const-string v2, "setHomesyncAction"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Support Mirroring :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v1

    if-gez v1, :cond_4

    .line 462
    invoke-direct {p0, p1, v5, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 471
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 472
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 474
    :cond_2
    return-void

    .line 450
    :cond_3
    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 451
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 464
    :cond_4
    invoke-direct {p0, p1, v5, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 465
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public setMirroringAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    .line 431
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 432
    return-void
.end method

.method public setMobileAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 8
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/16 v7, 0x12

    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContentsType:I

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsContentsForwardLock:Z

    if-nez v2, :cond_8

    .line 119
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 120
    invoke-direct {p0, p1, v3, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getServices()I

    move-result v1

    .line 128
    .local v1, "services":I
    if-eqz v1, :cond_6

    .line 129
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_GROUPPLAY:I

    and-int/2addr v2, v1

    if-lez v2, :cond_1

    .line 130
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.samsung.groupcast"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 132
    invoke-direct {p0, p1, v4, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 133
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_1
    :goto_1
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_TOGETHER:I

    and-int/2addr v2, v1

    if-lez v2, :cond_2

    .line 140
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.alltogether"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 142
    invoke-direct {p0, p1, v5, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 143
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_2
    :goto_2
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_SIDESYNC_SOURCE:I

    and-int/2addr v2, v1

    if-lez v2, :cond_3

    .line 150
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.sidesync30"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 152
    invoke-direct {p0, p1, v6, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 153
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    :cond_3
    :goto_3
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_EMEETING:I

    and-int/2addr v2, v1

    if-lez v2, :cond_5

    .line 160
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.emeeting"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.emeeting.b2c.hancom"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 164
    :cond_4
    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_5
    :goto_4
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_PROFILE_SHARE:I

    and-int/2addr v2, v1

    if-lez v2, :cond_6

    .line 170
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isProfileShareAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 171
    invoke-direct {p0, p1, v7, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 172
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    :cond_6
    :goto_5
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 180
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 182
    :cond_7
    return-void

    .line 124
    .end local v1    # "services":I
    :cond_8
    invoke-direct {p0, p1, v3, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    goto/16 :goto_0

    .line 136
    .restart local v1    # "services":I
    :cond_9
    const-string v2, "ActionFormatter"

    const-string v3, "setMobileAction"

    const-string v4, "GroupPlay not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 146
    :cond_a
    const-string v2, "ActionFormatter"

    const-string v3, "setMobileAction"

    const-string v4, "Together not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 156
    :cond_b
    const-string v2, "ActionFormatter"

    const-string v3, "setMobileAction"

    const-string v4, "SideSync not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 166
    :cond_c
    const-string v2, "ActionFormatter"

    const-string v3, "setMobileAction"

    const-string v4, "eMeeting not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 175
    :cond_d
    const-string v2, "ActionFormatter"

    const-string v3, "setMobileAction"

    const-string v4, "ProfileShare not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method public setPcAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 7
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x7

    const/16 v4, 0xc

    const/4 v3, 0x1

    .line 273
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 275
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-lez v1, :cond_1

    .line 276
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v1

    const/high16 v2, 0x100000

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothClass;->hasService(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 278
    iget v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContentsType:I

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsContentsForwardLock:Z

    if-nez v1, :cond_6

    .line 279
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 281
    invoke-direct {p0, p1, v3, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 287
    :cond_0
    :goto_0
    invoke-direct {p0, p1, v4, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 288
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, -0x5

    if-lez v1, :cond_2

    .line 295
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isSlinkSupport(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.pcw"

    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 298
    invoke-direct {p0, p1, v5, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 299
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-gtz v1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-lez v1, :cond_4

    .line 305
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getUpnpDeviceType()I

    move-result v1

    and-int/lit8 v1, v1, 0x3

    if-lez v1, :cond_4

    .line 306
    invoke-direct {p0, p1, v6, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 307
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 313
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 315
    :cond_5
    return-void

    .line 285
    :cond_6
    invoke-direct {p0, p1, v3, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    goto :goto_0

    .line 291
    :cond_7
    invoke-direct {p0, p1, v4, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    goto :goto_1
.end method

.method public setPrinterAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/16 v2, 0x14

    .line 414
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 415
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsPrintable:Z

    if-eqz v1, :cond_2

    .line 416
    const/16 v1, 0xd

    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 418
    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 426
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 428
    :cond_1
    return-void

    .line 422
    :cond_2
    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    goto :goto_0
.end method

.method public setRelatedActionList(J)V
    .locals 7
    .param p1, "relatedActions"    # J

    .prologue
    .line 41
    const-string v1, "ActionFormatter"

    const-string v2, "setRelatedActionList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "relatedActions: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mRelatedActionList:Ljava/util/HashSet;

    .line 43
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x14

    if-gt v0, v1, :cond_1

    .line 44
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    int-to-long v2, v1

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mRelatedActionList:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 46
    const-string v1, "ActionFormatter"

    const-string v2, "setRelatedActionList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_1
    return-void
.end method

.method public setTabletAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 8
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/16 v7, 0x11

    const/16 v6, 0x8

    const/4 v5, 0x7

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 185
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 186
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContentsType:I

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mIsContentsForwardLock:Z

    if-nez v2, :cond_8

    .line 187
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 188
    invoke-direct {p0, p1, v3, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getServices()I

    move-result v1

    .line 196
    .local v1, "services":I
    if-eqz v1, :cond_6

    .line 197
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_GROUPPLAY:I

    and-int/2addr v2, v1

    if-lez v2, :cond_1

    .line 198
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.samsung.groupcast"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 200
    invoke-direct {p0, p1, v4, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 201
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    :cond_1
    :goto_1
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_TOGETHER:I

    and-int/2addr v2, v1

    if-lez v2, :cond_2

    .line 208
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.alltogether"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 210
    invoke-direct {p0, p1, v6, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 211
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_2
    :goto_2
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_EMEETING:I

    and-int/2addr v2, v1

    if-lez v2, :cond_4

    .line 218
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.emeeting"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.emeeting.b2c.hancom"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 222
    :cond_3
    invoke-direct {p0, p1, v7, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 223
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    :cond_4
    :goto_3
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_SLINK_BROWSE_CONTENTS:I

    and-int/2addr v2, v1

    if-lez v2, :cond_5

    .line 230
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isSlinkSupport(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.pcw"

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 233
    invoke-direct {p0, p1, v5, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 234
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    :cond_5
    :goto_4
    sget v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_PROFILE_SHARE:I

    and-int/2addr v2, v1

    if-lez v2, :cond_6

    .line 241
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isProfileShareAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 242
    const/16 v2, 0x12

    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 243
    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    :cond_6
    :goto_5
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 252
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 254
    :cond_7
    return-void

    .line 192
    .end local v1    # "services":I
    :cond_8
    invoke-direct {p0, p1, v3, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    goto/16 :goto_0

    .line 204
    .restart local v1    # "services":I
    :cond_9
    const-string v2, "ActionFormatter"

    const-string v3, "setTabletAction"

    const-string v4, "GroupPlay not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 214
    :cond_a
    const-string v2, "ActionFormatter"

    const-string v3, "setTabletAction"

    const-string v4, "Together not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 226
    :cond_b
    const-string v2, "ActionFormatter"

    const-string v3, "setTabletAction"

    const-string v4, "eMeeting not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 237
    :cond_c
    const-string v2, "ActionFormatter"

    const-string v3, "setTabletAction"

    const-string v4, "S-Link not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 246
    :cond_d
    const-string v2, "ActionFormatter"

    const-string v3, "setMobileAction"

    const-string v4, "ProfileShare not Available"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method public setTogetherAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    .line 265
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.alltogether"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    const/16 v0, 0x9

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 270
    :goto_0
    return-void

    .line 268
    :cond_0
    const-string v0, "ActionFormatter"

    const-string v1, "setTogetherAction"

    const-string v2, "Together not Available"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTvAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 8
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/16 v5, 0xa

    const/4 v7, 0x5

    const/4 v6, 0x3

    .line 318
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isIRSupport(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isTvControllerAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isJPNDevice()Z

    move-result v2

    if-nez v2, :cond_5

    .line 321
    const/16 v2, 0xb

    invoke-direct {p0, p1, v2, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 322
    const-string v2, "ActionFormatter"

    const-string v3, "setTvAction"

    const-string v4, " IR  : SUPPORT IR"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v2

    and-int/lit8 v2, v2, 0x10

    if-lez v2, :cond_0

    .line 329
    invoke-direct {p0, p1, v5, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-lez v2, :cond_1

    .line 334
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getServices()I

    move-result v2

    sget v3, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    and-int/2addr v2, v3

    if-lez v2, :cond_1

    .line 335
    const-string v2, "ActionFormatter"

    const-string v3, "setTvAction"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Support Mirroring :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    invoke-direct {p0, p1, v7, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 337
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isIRSupport(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ActionFormatter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isTvControllerAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isJPNDevice()Z

    move-result v2

    if-nez v2, :cond_3

    .line 344
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 345
    .local v1, "name":Ljava/lang/String;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 346
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 347
    const-string v2, "[TV][LG]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "BRAVIA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 348
    :cond_2
    const-string v2, "ActionFormatter"

    const-string v3, "setTvAction"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Do not add more features action: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    .end local v1    # "name":Ljava/lang/String;
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 358
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 360
    :cond_4
    return-void

    .line 324
    .end local v0    # "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_5
    const-string v2, "ActionFormatter"

    const-string v3, "setTvAction"

    const-string v4, " IR  : NO SUPPORT IR"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 351
    .restart local v0    # "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v1    # "name":Ljava/lang/String;
    :cond_6
    invoke-direct {p0, p1, v6, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 352
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public setWearableAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 8
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isContentRelatedMode"    # Z

    .prologue
    const/4 v7, 0x3

    .line 372
    const/16 v4, 0xc

    invoke-direct {p0, p1, v4, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setPrimaryAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)V

    .line 374
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 376
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 377
    .local v3, "isConnected":Z
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothActionHelper()Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    move-result-object v1

    .line 379
    .local v1, "btActionHelper":Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
    if-eqz v1, :cond_0

    .line 380
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->isConnected(Ljava/lang/String;)Z

    move-result v3

    .line 382
    :cond_0
    if-eqz v3, :cond_1

    .line 383
    const-string v4, "ActionFormatter"

    const-string v5, "setWearableAction"

    const-string v6, "this is connected device"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableType()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableInfo()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 386
    invoke-direct {p0, p1, v7, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 387
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 409
    invoke-virtual {p1, v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setActionList(Ljava/util/ArrayList;)V

    .line 411
    :cond_2
    return-void

    .line 391
    :cond_3
    const-string v4, "ActionFormatter"

    const-string v5, "setWearableAction"

    const-string v6, "this code must not be called"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v2

    .line 393
    .local v2, "deviceName":Ljava/lang/String;
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 394
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 395
    const-string v4, "galaxy gear"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "gear 2"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "gear 3"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "gear fit"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "gear s"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 399
    :cond_4
    invoke-direct {p0, p1, v7, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->checkAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 400
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 404
    :cond_5
    const-string v4, "ActionFormatter"

    const-string v5, "setWearableAction"

    const-string v6, "deviceName is null or empty"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
