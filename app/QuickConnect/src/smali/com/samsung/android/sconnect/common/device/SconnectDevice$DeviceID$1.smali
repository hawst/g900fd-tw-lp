.class Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID$1;
.super Ljava/lang/Object;
.source "SconnectDevice.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;)V
    .locals 0

    .prologue
    .line 1121
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID$1;->this$1:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 1124
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID$1;->this$1:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->this$0:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 1125
    .local v0, "ids":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    .line 1126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 1127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    .line 1128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    .line 1129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    .line 1130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    .line 1131
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 1121
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 1136
    new-array v0, p1, [Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 1121
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID$1;->newArray(I)[Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    return-object v0
.end method
