.class Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 332
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 333
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 334
    .local v1, "bundle":Landroid/os/Bundle;
    if-nez v1, :cond_1

    .line 335
    const-string v6, "BluetoothActionHelper"

    const-string v7, "mBtBondReceiver"

    const-string v8, "getExtras() is null"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    const-string v6, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 340
    .local v4, "remoteDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 341
    const-string v6, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 343
    const-string v6, "android.bluetooth.device.extra.BOND_STATE"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 344
    .local v3, "remoteBondState":I
    const-string v6, "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 346
    .local v5, "remotePrevBondState":I
    const-string v6, "BluetoothActionHelper"

    const-string v7, "mBtBondReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACTION_BOND_STATE_CHANGED : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " state["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " >> "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    const/16 v6, 0xc

    if-ne v3, v6, :cond_4

    const/16 v6, 0xa

    if-eq v5, v6, :cond_2

    const/16 v6, 0xb

    if-ne v5, v6, :cond_4

    .line 351
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$100(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 352
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->unregisterBondReceiver()V
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$200(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V

    .line 353
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->registerProfileReceiver(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$300(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Ljava/lang/String;)V

    .line 354
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v7

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->connectAllProfile(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$400(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Landroid/bluetooth/BluetoothDevice;)V

    .line 355
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    const/4 v7, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$102(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Z)Z

    goto/16 :goto_0

    .line 357
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    const/4 v7, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$102(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Z)Z

    goto/16 :goto_0

    .line 359
    :cond_4
    const/16 v6, 0xa

    if-ne v3, v6, :cond_0

    const/16 v6, 0xb

    if-ne v5, v6, :cond_0

    .line 361
    const-string v6, "BluetoothActionHelper"

    const-string v7, "mBtBondReceiver"

    const-string v8, "Bond fail...startBleScan here"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mCountDownTimer:Landroid/os/CountDownTimer;
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$500(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Landroid/os/CountDownTimer;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/CountDownTimer;->cancel()V

    .line 363
    const/4 v6, 0x0

    sput v6, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mState:I

    .line 364
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/SconnectManager;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v6

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    .line 366
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/SconnectManager;->getHowToControlBluetooth()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 367
    const-string v6, "BluetoothActionHelper"

    const-string v7, "mBtBondReceiver"

    const-string v8, "disableBluetoothAdapter()"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothHelper()Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->disableBluetoothAdapter()Z

    goto/16 :goto_0

    .line 372
    .end local v3    # "remoteBondState":I
    .end local v5    # "remotePrevBondState":I
    :cond_5
    const-string v6, "android.bluetooth.device.action.UUID"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 375
    const-string v6, "android.bluetooth.device.extra.UUID"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/ParcelUuid;

    .line 377
    .local v2, "parcelUuid":Landroid/os/ParcelUuid;
    const-string v6, "BluetoothActionHelper"

    const-string v7, "mBtBondReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACTION_UUID : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " UUID["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v7

    # setter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mRemoteUuids:[Landroid/os/ParcelUuid;
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$602(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;[Landroid/os/ParcelUuid;)[Landroid/os/ParcelUuid;

    .line 380
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$100(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_6

    .line 381
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->unregisterBondReceiver()V
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$200(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V

    .line 382
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->registerProfileReceiver(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$300(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Ljava/lang/String;)V

    .line 383
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v7

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->connectAllProfile(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$400(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Landroid/bluetooth/BluetoothDevice;)V

    .line 384
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    const/4 v7, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$102(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Z)Z

    goto/16 :goto_0

    .line 386
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    const/4 v7, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mIsReadyToConnect:Z
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$102(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;Z)Z

    goto/16 :goto_0
.end method
