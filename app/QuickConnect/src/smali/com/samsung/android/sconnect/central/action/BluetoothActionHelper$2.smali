.class Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 426
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 427
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 428
    .local v1, "bundle":Landroid/os/Bundle;
    if-nez v1, :cond_1

    .line 429
    const-string v5, "BluetoothActionHelper"

    const-string v6, "mBtProfileReceiver"

    const-string v7, "getExtras() is null"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :cond_0
    :goto_0
    return-void

    .line 432
    :cond_1
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 434
    .local v2, "remoteDevice":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 435
    const/4 v4, 0x0

    .line 436
    .local v4, "remoteProfileState":I
    const/4 v3, 0x0

    .line 437
    .local v3, "remotePrevProfileState":I
    const-string v5, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 438
    const-string v5, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 439
    const-string v5, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 440
    const-string v5, "BluetoothActionHelper"

    const-string v6, "mBtProfileReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " complete with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] state ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " >> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    if-ne v4, v10, :cond_2

    .line 445
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$700(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/util/ArrayList;

    move-result-object v5

    const-string v6, "HSP"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 469
    :cond_2
    :goto_1
    const-string v5, "BluetoothActionHelper"

    const-string v6, "mBtProfileReceiver"

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$700(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$700(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v5, v9, :cond_0

    .line 471
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->unregisterProfileReceiver()V
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$800(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)V

    .line 472
    const v5, 0x7f090031

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 476
    const-string v5, "BluetoothActionHelper"

    const-string v6, "mBtProfileReceiver"

    const-string v7, "connect success...startBleScan here"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mCountDownTimer:Landroid/os/CountDownTimer;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$500(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Landroid/os/CountDownTimer;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/CountDownTimer;->cancel()V

    .line 478
    sput v11, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mState:I

    .line 479
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/SconnectManager;->getBleHelper()Lcom/samsung/android/sconnect/common/net/BleHelper;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    goto/16 :goto_0

    .line 447
    :cond_3
    const-string v5, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 448
    const-string v5, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 449
    const-string v5, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 450
    const-string v5, "BluetoothActionHelper"

    const-string v6, "mBtProfileReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " complete with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] state ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " >> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    if-ne v4, v10, :cond_2

    .line 455
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$700(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/util/ArrayList;

    move-result-object v5

    const-string v6, "A2DP"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 457
    :cond_4
    const-string v5, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 458
    const-string v5, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 459
    const-string v5, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 460
    const-string v5, "BluetoothActionHelper"

    const-string v6, "mBtProfileReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " complete with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTargetBtMacAddr:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] state ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " >> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    if-ne v4, v10, :cond_2

    .line 465
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mTryingProfiles:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->access$700(Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;)Ljava/util/ArrayList;

    move-result-object v5

    const-string v6, "HID"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method
