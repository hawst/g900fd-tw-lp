.class Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$1;
.super Ljava/lang/Object;
.source "SelectMapActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$800(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$702(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 287
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$4;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSelectMapHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$900(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 289
    return-void
.end method
