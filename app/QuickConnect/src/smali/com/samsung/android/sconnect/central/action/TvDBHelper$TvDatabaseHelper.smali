.class Lcom/samsung/android/sconnect/central/action/TvDBHelper$TvDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "TvDBHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/TvDBHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TvDatabaseHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4, "version"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 54
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 59
    const-string v0, "TvDatabaseHelper"

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v0, "create table SConnect_WatchOn_TABLE(_id integer primary key autoincrement, UUID text not null , P2PMAC text not null , BTMAC text not null , ROOM_ID text not null , ROOM_NAME text not null, D_TYPE text not null , D_BRAND text not null , IS_WATCHON text not null );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 66
    const-string v3, "TvDatabaseHelper"

    const-string v4, "onUpgrade"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[oldVersion]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [newVersion]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    const/4 v3, 0x2

    if-ne p3, v3, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 71
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "SELECT sql FROM sqlite_master WHERE name=\'SConnect_WatchOn_TABLE\'"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 74
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "sqlCreate":Ljava/lang/String;
    const-string v3, "IS_WATCHON"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 76
    const-string v3, "TvDatabaseHelper"

    const-string v4, "onUpgrade"

    const-string v5, "already has IS_WATCHON"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    .end local v2    # "sqlCreate":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 88
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 92
    .end local v0    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_1
    return-void

    .line 78
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v2    # "sqlCreate":Ljava/lang/String;
    :cond_1
    :try_start_1
    const-string v3, "TvDatabaseHelper"

    const-string v4, "onUpgrade"

    const-string v5, "SQL_ALTER_V1_TO_V2"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v3, "ALTER TABLE SConnect_WatchOn_TABLE ADD COLUMN IS_WATCHON text not null default \'true\';"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 84
    .end local v2    # "sqlCreate":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 85
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "TvDatabaseHelper"

    const-string v4, "onUpgrade"

    const-string v5, "Exception"

    invoke-static {v3, v4, v5, v1}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 87
    if-eqz v0, :cond_0

    .line 88
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 82
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_3
    const-string v3, "TvDatabaseHelper"

    const-string v4, "onUpgrade"

    const-string v5, "nothing to do"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_3

    .line 88
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
.end method
