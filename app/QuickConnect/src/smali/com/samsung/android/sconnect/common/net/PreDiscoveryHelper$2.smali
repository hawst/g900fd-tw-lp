.class Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "PreDiscoveryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 508
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    .line 509
    .local v13, "action":Ljava/lang/String;
    const-string v2, "com.samsung.android.sconnect.central.WAITING_CANCEL"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 510
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver"

    const-string v8, "ACTION_C_WAITING_CANCEL"

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    const-string v2, "KEY"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 512
    .local v3, "id":Ljava/lang/String;
    const-string v2, "ADDRESSLIST"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    .line 513
    .local v14, "addr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v14, :cond_0

    .line 514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-virtual {v2, v3, v14}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendCancelCommand(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 567
    .end local v3    # "id":Ljava/lang/String;
    .end local v14    # "addr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    const-string v2, "com.samsung.android.sconnect.periph.ACCEPT_RESULT"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 517
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver"

    const-string v8, "ACTION_P_ACCEPT_RESULT"

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    const-string v2, "RES"

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 519
    .local v5, "result":Z
    const-string v2, "KEY"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 520
    .restart local v3    # "id":Ljava/lang/String;
    const-string v2, "ADDRESS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 521
    .local v4, "address":Ljava/lang/String;
    const-string v2, "CMD"

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 522
    .local v15, "cmd":I
    if-eqz v5, :cond_a

    .line 523
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACTION_P_ACCEPT_RESULT : accept, CMD : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const/16 v2, 0x50

    if-ne v15, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v2

    const-string v7, "ADDRESS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->needToDisconnectOnPeriph(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v2

    const-string v7, "ADDRESS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->needToDisconnect(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 528
    :cond_3
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver"

    const-string v8, "ACTION_P_ACCEPT_RESULT : removeGroup and waitingPeer"

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeGroup(Z)V

    .line 532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v2

    const/16 v7, 0xa

    invoke-virtual {v2, v7}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v16

    .line 533
    .local v16, "msg":Landroid/os/Message;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v2

    if-nez v2, :cond_4

    .line 535
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver"

    const-string v8, "mAdvActHandler==null"

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 538
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v2

    const-wide/16 v8, 0x5dc

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v8, v9}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 541
    .end local v16    # "msg":Landroid/os/Message;
    :cond_5
    if-eqz v15, :cond_6

    const/4 v2, 0x1

    if-eq v15, v2, :cond_6

    const/16 v2, 0x50

    if-eq v15, v2, :cond_6

    const/16 v2, 0x12

    if-ne v15, v2, :cond_8

    .line 545
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 546
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver"

    const-string v8, "ACTION_P_ACCEPT_RESULT : waitingPeer"

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->waitingPeer()V

    .line 549
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v6, 0x0

    const/4 v7, 0x0

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendConfirmCommand(Ljava/lang/String;Ljava/lang/String;Z[BZ)V
    invoke-static/range {v2 .. v7}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/lang/String;Ljava/lang/String;Z[BZ)V

    goto/16 :goto_0

    .line 550
    :cond_8
    const/16 v2, 0x10

    if-ne v15, v2, :cond_9

    .line 551
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getBtMacAddress()Ljava/lang/String;

    move-result-object v17

    .line 552
    .local v17, "myBtMac":Ljava/lang/String;
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver.ACTION_P_ACCEPT_RESULT.ACTION_SIDESYNC_CONTROL"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[myBtMac]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToByte(Ljava/lang/String;)[B

    move-result-object v6

    .line 556
    .local v6, "myBtMacByte":[B
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v7, 0x1

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendConfirmCommand(Ljava/lang/String;Ljava/lang/String;Z[BZ)V
    invoke-static/range {v2 .. v7}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/lang/String;Ljava/lang/String;Z[BZ)V

    goto/16 :goto_0

    .line 558
    .end local v6    # "myBtMacByte":[B
    .end local v17    # "myBtMac":Ljava/lang/String;
    :cond_9
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver"

    const-string v8, "ACTION_P_ACCEPT_RESULT : No use p2p"

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object v8, v3

    move-object v9, v4

    move v10, v5

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendConfirmCommand(Ljava/lang/String;Ljava/lang/String;Z[BZ)V
    invoke-static/range {v7 .. v12}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/lang/String;Ljava/lang/String;Z[BZ)V

    goto/16 :goto_0

    .line 562
    :cond_a
    const-string v2, "PreDiscoveryHelper"

    const-string v7, "mLocalBleReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACTION_P_ACCEPT_RESULT : reject, CMD : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v8, v3

    move-object v9, v4

    move v10, v5

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendConfirmCommand(Ljava/lang/String;Ljava/lang/String;Z[BZ)V
    invoke-static/range {v7 .. v12}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/lang/String;Ljava/lang/String;Z[BZ)V

    goto/16 :goto_0
.end method
