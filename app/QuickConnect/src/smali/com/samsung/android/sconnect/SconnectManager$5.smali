.class Lcom/samsung/android/sconnect/SconnectManager$5;
.super Ljava/lang/Object;
.source "SconnectManager.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/SconnectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/SconnectManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/SconnectManager;)V
    .locals 0

    .prologue
    .line 1045
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(BLjava/lang/String;)V
    .locals 4
    .param p1, "networkType"    # B
    .param p2, "extraInfo"    # Ljava/lang/String;

    .prologue
    .line 1069
    const-string v0, "SconnectManager"

    const-string v1, "INetworkStateListener, onConnected"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "networkType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    sget-byte v0, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    if-ne p1, v0, :cond_1

    .line 1071
    const-string v0, "SconnectManager"

    const-string v1, "onConnected"

    const-string v2, "NETWORK_P2P"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getRequestPeerList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getRequestTargetList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1074
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1076
    :cond_0
    if-eqz p2, :cond_1

    .line 1077
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPrintPluginHelper:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$1300(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/samsung/android/sconnect/central/CentralActionManager;->startHoldingIntent(Ljava/lang/String;Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V

    .line 1080
    :cond_1
    return-void
.end method

.method public onDisabled(B)V
    .locals 4
    .param p1, "networkType"    # B

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    xor-int/lit8 v1, p1, -0x1

    # &= operator for: Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$1272(Lcom/samsung/android/sconnect/SconnectManager;I)B

    .line 1056
    const-string v0, "SconnectManager"

    const-string v1, "INetworkStateListener, onDisabled"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mNetworkState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B
    invoke-static {v3}, Lcom/samsung/android/sconnect/SconnectManager;->access$1200(Lcom/samsung/android/sconnect/SconnectManager;)B

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    sget-byte v0, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    if-eq p1, v0, :cond_0

    sget-byte v0, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    if-ne p1, v0, :cond_2

    .line 1058
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1059
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->cancelConnectPeer()V

    .line 1061
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1062
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralActionManager;->removeHoldingIntentAll()V

    .line 1065
    :cond_2
    return-void
.end method

.method public onDisconnected(BLjava/lang/String;)V
    .locals 6
    .param p1, "networkType"    # B
    .param p2, "extraInfo"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1084
    const-string v1, "SconnectManager"

    const-string v2, "INetworkStateListener, onDisconnected"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "networkType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    sget-byte v1, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    if-ne p1, v1, :cond_1

    .line 1086
    if-eqz p2, :cond_0

    .line 1087
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/samsung/android/sconnect/central/CentralActionManager;->removeHoldingIntent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1088
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.central.REQUEST_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1089
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "KEY"

    const-string v2, "CANCEL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1090
    const-string v1, "ADDRESS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1091
    const-string v1, "RES"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1092
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1093
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0900fb

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1097
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getRequestPeerList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getRequestTargetList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1099
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x16

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1103
    :cond_1
    return-void
.end method

.method public onEnabled(B)V
    .locals 4
    .param p1, "networkType"    # B

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # |= operator for: Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B
    invoke-static {v0, p1}, Lcom/samsung/android/sconnect/SconnectManager;->access$1276(Lcom/samsung/android/sconnect/SconnectManager;I)B

    .line 1050
    const-string v0, "SconnectManager"

    const-string v1, "INetworkStateListener, onEnabled"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mNetworkState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/SconnectManager$5;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mNetworkState:B
    invoke-static {v3}, Lcom/samsung/android/sconnect/SconnectManager;->access$1200(Lcom/samsung/android/sconnect/SconnectManager;)B

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1051
    return-void
.end method

.method public onPoor(B)V
    .locals 0
    .param p1, "networkType"    # B

    .prologue
    .line 1107
    return-void
.end method
