.class Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;
.super Ljava/lang/Object;
.source "TutorialActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/16 v4, 0x8

    .line 132
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mPageNumber:I
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->access$000(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 158
    :goto_0
    return-void

    .line 134
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    const v3, 0x7f0c0084

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 135
    .local v1, "pageLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 136
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setPage2View()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->access$100(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)V

    goto :goto_0

    .line 139
    .end local v1    # "pageLayout":Landroid/widget/RelativeLayout;
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    const v3, 0x7f0c0086

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 140
    .restart local v1    # "pageLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 141
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->setPage3View()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->access$200(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)V

    goto :goto_0

    .line 144
    .end local v1    # "pageLayout":Landroid/widget/RelativeLayout;
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    sget v4, Lcom/samsung/android/sconnect/common/util/Util;->ENABLED:I

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/Util;->saveToDb(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 145
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_CONTACT_ONLY:Ljava/lang/String;

    sget v4, Lcom/samsung/android/sconnect/common/util/Util;->ENABLED:I

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/Util;->saveToDb(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 146
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    const-class v3, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 148
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 150
    const-string v2, "isTutorial"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 151
    const-string v2, "FORWARD_LOCK"

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mIsContentsForwardLock:Z
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 152
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mMimeType:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string v2, "android.intent.extra.STREAM"

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->access$500(Lcom/samsung/android/sconnect/central/ui/TutorialActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 154
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->startActivity(Landroid/content/Intent;)V

    .line 155
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->finish()V

    goto :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
