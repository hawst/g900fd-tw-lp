.class public Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubUpdateCheckResult;
.super Ljava/lang/Object;
.source "UpdateSamsungApps.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/update/UpdateSamsungApps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StubUpdateCheckResult"
.end annotation


# instance fields
.field appId:Ljava/lang/String;

.field resultCode:Ljava/lang/String;

.field resultMsg:Ljava/lang/String;

.field versionCode:I

.field versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubUpdateCheckResult;->appId:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubUpdateCheckResult;->resultCode:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubUpdateCheckResult;->resultMsg:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubUpdateCheckResult;->versionCode:I

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sconnect/update/UpdateSamsungApps$StubUpdateCheckResult;->versionName:Ljava/lang/String;

    return-void
.end method
