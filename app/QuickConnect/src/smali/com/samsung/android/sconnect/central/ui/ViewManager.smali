.class public Lcom/samsung/android/sconnect/central/ui/ViewManager;
.super Ljava/lang/Object;
.source "ViewManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ViewManager"


# instance fields
.field private mActionItemHeight:I

.field private mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mActionListAdapter:Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;

.field private mActionListDividerHeight:I

.field private mActionListLayout:Landroid/widget/LinearLayout;

.field private mActionListView:Landroid/widget/ListView;

.field private mActionbarbtnLayout:Landroid/widget/LinearLayout;

.field private mBackButton:Landroid/widget/ImageView;

.field private mBackgroundResId:I

.field private mBoldTypeface:Landroid/graphics/Typeface;

.field private mCancelButton:Landroid/widget/TextView;

.field mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mCompleteButton:Landroid/widget/TextView;

.field private mContentTextView:Landroid/widget/TextView;

.field private mContentsText:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDeviceLayoutView:Landroid/widget/RelativeLayout;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFavoriteDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFavoriteListView:Landroid/widget/LinearLayout;

.field private mFavoriteTitleView:Landroid/widget/TextView;

.field private mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

.field private mHeight:I

.field private mInitiatingView:Landroid/widget/LinearLayout;

.field private mIsMenuShowing:Z

.field private mListLayout:Landroid/widget/LinearLayout;

.field private mListingText:Landroid/widget/TextView;

.field private mNearbyListView:Landroid/widget/LinearLayout;

.field private mNearbyTitleTextView:Landroid/widget/TextView;

.field private mNearbyTitleView:Landroid/widget/RelativeLayout;

.field private mNormalTypeface:Landroid/graphics/Typeface;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mRefreshAnimView:Landroid/widget/ImageView;

.field private mRefreshAnimation:Landroid/view/animation/Animation;

.field private mRefreshButton:Landroid/widget/Button;

.field private mRefreshButtonLayout:Landroid/widget/RelativeLayout;

.field private mResources:Landroid/content/res/Resources;

.field private mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field private mScrollGap:I

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSearchingView:Landroid/widget/LinearLayout;

.field private mSelectedDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSettingButton:Landroid/widget/ImageButton;

.field private mSettingItemListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

.field private mSettingMenu:Landroid/widget/PopupMenu;

.field private mSlidingDrawer:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

.field private mTSButton:Landroid/widget/Button;

.field private mTSButtonCenter:Landroid/widget/Button;

.field private mTSView:Landroid/widget/LinearLayout;

.field private mTitleView:Landroid/widget/TextView;

.field private mTooltipListener:Landroid/view/View$OnLongClickListener;

.field private mTsNoDeviceText:Landroid/widget/TextView;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Landroid/widget/PopupMenu$OnMenuItemClickListener;Landroid/widget/AdapterView$OnItemClickListener;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/samsung/android/sconnect/common/util/GUIUtil;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onClickListener"    # Landroid/view/View$OnClickListener;
    .param p3, "onLongClickListener"    # Landroid/view/View$OnLongClickListener;
    .param p4, "settingItemListener"    # Landroid/widget/PopupMenu$OnMenuItemClickListener;
    .param p5, "onItemClickListener"    # Landroid/widget/AdapterView$OnItemClickListener;
    .param p6, "contentsText"    # Ljava/lang/String;
    .param p10, "guiUtil"    # Lcom/samsung/android/sconnect/common/util/GUIUtil;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnLongClickListener;",
            "Landroid/widget/PopupMenu$OnMenuItemClickListener;",
            "Landroid/widget/AdapterView$OnItemClickListener;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/central/ui/DisplayItem;",
            ">;",
            "Lcom/samsung/android/sconnect/common/util/GUIUtil;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p7, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/central/ui/DisplayItem;>;"
    .local p8, "favoriteDeviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/central/ui/DisplayItem;>;"
    .local p9, "selectedDeviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/central/ui/DisplayItem;>;"
    .local p11, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    .line 112
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTsNoDeviceText:Landroid/widget/TextView;

    .line 113
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButtonCenter:Landroid/widget/Button;

    .line 114
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    .line 115
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mIsMenuShowing:Z

    .line 117
    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    .line 118
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBackgroundResId:I

    .line 320
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/ViewManager$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$4;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTooltipListener:Landroid/view/View$OnLongClickListener;

    .line 417
    new-instance v0, Lcom/samsung/android/sconnect/central/ui/ViewManager$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$7;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 129
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    .line 130
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .line 131
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 132
    iput-object p3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 133
    iput-object p4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingItemListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

    .line 134
    iput-object p5, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 135
    iput-object p6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContentsText:Ljava/lang/String;

    .line 136
    iput-object p7, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    .line 137
    iput-object p8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    .line 138
    iput-object p9, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    .line 139
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    .line 140
    iput-object p10, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 141
    iput-object p11, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionList:Ljava/util/ArrayList;

    .line 143
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setDisplaySize()V

    .line 144
    const-string v0, "sec-roboto-light"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNormalTypeface:Landroid/graphics/Typeface;

    .line 145
    const-string v0, "sec-roboto-light"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBoldTypeface:Landroid/graphics/Typeface;

    .line 146
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/sconnect/central/ui/ViewManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mIsMenuShowing:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/sconnect/central/ui/ViewManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;
    .param p1, "x1"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/ui/ViewManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mWidth:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/ViewManager;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private setDisableHelpMenu()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 1161
    const/4 v4, 0x0

    .line 1162
    .local v4, "isUsaModel":Z
    const/4 v3, 0x0

    .line 1163
    .local v3, "isOnDeviceHelp":Z
    const-string v6, "ro.csc.country_code"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1164
    .local v0, "countryCode":Ljava/lang/String;
    const-string v6, "USA"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    .line 1167
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.samsung.helphub"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 1169
    .local v2, "info":Landroid/content/pm/PackageInfo;
    iget v6, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v6, v6, 0xa
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    const/4 v3, 0x1

    .line 1174
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    const-string v6, "ViewManager"

    const-string v7, "setDisableHelpMenu"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isUsaModel : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", isOnDeviceHelp: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    if-eqz v4, :cond_0

    if-nez v3, :cond_0

    .line 1179
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v6}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v6

    const v7, 0x7f0c00b0

    invoke-interface {v6, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1181
    :cond_0
    return-void

    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    :cond_1
    move v3, v5

    .line 1169
    goto :goto_0

    .line 1170
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 1171
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private startSelecteMode()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 738
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/samsung/android/sconnect/SconnectManager;->setDeviceSelectMode(Z)V

    .line 739
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 740
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 743
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTitleView:Landroid/widget/TextView;

    const v4, 0x7f09001f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 744
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionbarbtnLayout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 745
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionbarbtnLayout:Landroid/widget/LinearLayout;

    const v4, 0x101045c

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 748
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshAnimView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->clearAnimation()V

    .line 749
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 750
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 751
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCompleteButton:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 752
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 753
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCompleteButton:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 757
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 758
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 759
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f070012

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 760
    .local v0, "backButtonWidth":I
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 761
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContentTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 763
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 764
    .local v2, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setSelectModeView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto :goto_1

    .line 755
    .end local v0    # "backButtonWidth":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCompleteButton:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 766
    .restart local v0    # "backButtonWidth":I
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 767
    .restart local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {p0, v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setSelectModeView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto :goto_2

    .line 769
    .end local v2    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    new-instance v4, Lcom/samsung/android/sconnect/central/ui/ViewManager$13;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$13;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    .line 778
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 779
    return-void
.end method


# virtual methods
.method public addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z
    .locals 13
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "position"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v12, -0x1

    .line 1222
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    .line 1223
    .local v6, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v6, :cond_1

    .line 1224
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1225
    .local v1, "child":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v9

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1226
    const-string v9, "ViewManager"

    const-string v10, "addDeviceView"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "already added nearby item: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    .end local v1    # "child":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :goto_1
    return v8

    .line 1223
    .restart local v1    # "child":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1231
    .end local v1    # "child":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    .line 1232
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v6, :cond_3

    .line 1233
    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1234
    .restart local v1    # "child":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v9

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1235
    const-string v9, "ViewManager"

    const-string v10, "addDeviceView"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "already added favorite item: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1232
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1240
    .end local v1    # "child":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    .line 1241
    .local v5, "parentView":Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    .line 1242
    .local v3, "isFavorite":Z
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1243
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    .line 1244
    const/4 v3, 0x1

    .line 1246
    :cond_4
    const-string v8, "ViewManager"

    const-string v9, "addDeviceView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "(isFavorite: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v12, v12}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1250
    .local v4, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    const/4 v0, 0x0

    .line 1251
    .local v0, "addTSBtn":Z
    if-nez v3, :cond_5

    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v8

    if-ltz v8, :cond_5

    .line 1252
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1253
    const/4 v0, 0x1

    .line 1255
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f030006

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 1257
    .local v7, "view":Landroid/widget/LinearLayout;
    invoke-virtual {p1, v7}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setDeviceView(Landroid/widget/LinearLayout;)V

    .line 1259
    if-ltz p2, :cond_6

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v8

    if-le p2, v8, :cond_7

    .line 1260
    :cond_6
    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p2

    .line 1262
    :cond_7
    invoke-virtual {v5, v7, p2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1263
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->updateDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 1264
    if-eqz v0, :cond_8

    .line 1265
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addTroubleShootingButton()V

    .line 1267
    :cond_8
    const/4 v8, 0x1

    goto/16 :goto_1
.end method

.method public addTroubleShootingButton()V
    .locals 11

    .prologue
    const/4 v10, -0x2

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 1185
    const-string v4, "ViewManager"

    const-string v5, "addTroubleShootingButton"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Device list:  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1187
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v4

    if-ltz v4, :cond_0

    .line 1188
    const-string v4, "ViewManager"

    const-string v5, "addTroubleShootingButton"

    const-string v6, "view already added!!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    :goto_0
    return-void

    .line 1191
    :cond_0
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v1, v4, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1194
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    .line 1195
    .local v3, "position":I
    if-nez v3, :cond_2

    .line 1196
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getHeight()I

    move-result v4

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1197
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideSearchingView()V

    .line 1198
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showDeviceList()V

    .line 1199
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButtonCenter:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 1200
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTsNoDeviceText:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1201
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 1209
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 1210
    .local v2, "parentViewGroup":Landroid/view/ViewGroup;
    if-eqz v2, :cond_1

    .line 1211
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1215
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5, v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1216
    :catch_0
    move-exception v0

    .line 1217
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "ViewManager"

    const-string v5, "addTroubleShootingButton"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception on addview: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1203
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "parentViewGroup":Landroid/view/ViewGroup;
    :cond_2
    iput v10, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1204
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButtonCenter:Landroid/widget/Button;

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 1205
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTsNoDeviceText:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1206
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method public animateSearchingView()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1086
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideInitiatingView()V

    .line 1087
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshAnimView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1088
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1089
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1090
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSearchingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1096
    :goto_0
    return-void

    .line 1092
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1093
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSearchingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public animateSendingView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 6
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 1141
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v2

    .line 1142
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 1143
    const v3, 0x7f0c0019

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1144
    .local v0, "deviceIconView":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const/high16 v4, 0x7f040000

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1147
    const v3, 0x7f020035

    :try_start_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1151
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/16 v4, 0xbc1

    const/16 v5, 0x7d0

    invoke-virtual {v3, v4, p1, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;I)V

    .line 1153
    .end local v0    # "deviceIconView":Landroid/widget/ImageView;
    :cond_0
    return-void

    .line 1148
    .restart local v0    # "deviceIconView":Landroid/widget/ImageView;
    :catch_0
    move-exception v1

    .line 1149
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "ViewManager"

    const-string v4, "Exception on animateSendingView"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public contractActionList()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 433
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v3

    .line 434
    .local v3, "expandedDevice":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    .line 435
    .local v7, "view":Landroid/widget/LinearLayout;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 436
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    .line 438
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 440
    if-eqz v3, :cond_1

    .line 441
    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v5

    .line 442
    .local v5, "itemView":Landroid/widget/LinearLayout;
    const v8, 0x7f0c001d

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 444
    .local v2, "expandBtn":Landroid/widget/ImageButton;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v10, 0x7f09013e

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v10, 0x7f09013f

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 446
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 448
    const v8, 0x7f0c0002

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 449
    .local v1, "deviceNameView":Landroid/widget/TextView;
    const v8, 0x7f0c001c

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 450
    .local v0, "actionNameView":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNormalTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 451
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNormalTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 453
    .end local v0    # "actionNameView":Landroid/widget/TextView;
    .end local v1    # "deviceNameView":Landroid/widget/TextView;
    .end local v2    # "expandBtn":Landroid/widget/ImageButton;
    .end local v5    # "itemView":Landroid/widget/LinearLayout;
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v8, v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setExpandedDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 455
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 456
    .local v6, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v6, :cond_2

    .line 457
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {p0, v8, v12}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 458
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 456
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 460
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 461
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v6, :cond_3

    .line 462
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {p0, v8, v12}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 463
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 461
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 465
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v11}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 466
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v8, v11}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 468
    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    new-instance v9, Lcom/samsung/android/sconnect/central/ui/ViewManager$8;

    invoke-direct {v9, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$8;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual {v8, v9}, Landroid/widget/Button;->post(Ljava/lang/Runnable;)Z

    .line 474
    return-void
.end method

.method public dismissSettingMenu()Z
    .locals 3

    .prologue
    .line 1118
    const-string v0, "ViewManager"

    const-string v1, "dismissSettingMenu"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mIsMenuShowing:Z

    if-eqz v0, :cond_0

    .line 1120
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 1121
    const/4 v0, 0x1

    .line 1123
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 10
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isEnable"    # Z

    .prologue
    const v7, 0x7f0c001d

    const/16 v9, 0x66

    const/16 v8, 0xff

    .line 636
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    .line 637
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v6

    if-nez v6, :cond_0

    .line 638
    const/4 p2, 0x0

    .line 640
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v5

    .line 641
    .local v5, "view":Landroid/view/View;
    invoke-virtual {v5, p2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 642
    if-eqz p2, :cond_2

    iget v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBackgroundResId:I

    :goto_0
    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 643
    const v6, 0x7f0c0018

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 644
    .local v1, "deviceIconBgView":Landroid/widget/ImageView;
    const v6, 0x7f0c0019

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 645
    .local v2, "deviceIconView":Landroid/widget/ImageView;
    const v6, 0x7f0c001a

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 647
    .local v4, "indicatorView":Landroid/widget/ImageView;
    if-eqz p2, :cond_3

    .line 648
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 652
    :goto_1
    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p2}, Landroid/view/View;->setClickable(Z)V

    .line 653
    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 654
    const v6, 0x7f0c001b

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 655
    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 657
    move v3, p2

    .line 658
    .local v3, "enableFavoriteBtn":Z
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v6, v6, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v6, v7, :cond_4

    .line 659
    const/4 v3, 0x0

    .line 663
    :cond_1
    :goto_2
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 664
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 665
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 666
    if-eqz p2, :cond_5

    .line 667
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 669
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 670
    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 671
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 683
    :goto_3
    return-void

    .line 642
    .end local v1    # "deviceIconBgView":Landroid/widget/ImageView;
    .end local v2    # "deviceIconView":Landroid/widget/ImageView;
    .end local v3    # "enableFavoriteBtn":Z
    .end local v4    # "indicatorView":Landroid/widget/ImageView;
    :cond_2
    const v6, 0x106000d

    goto :goto_0

    .line 650
    .restart local v1    # "deviceIconBgView":Landroid/widget/ImageView;
    .restart local v2    # "deviceIconView":Landroid/widget/ImageView;
    .restart local v4    # "indicatorView":Landroid/widget/ImageView;
    :cond_3
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 660
    .restart local v3    # "enableFavoriteBtn":Z
    :cond_4
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v6

    if-nez v6, :cond_1

    .line 661
    const/4 v3, 0x1

    goto :goto_2

    .line 673
    :cond_5
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 675
    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 676
    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 677
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImageType()I

    move-result v6

    if-gtz v6, :cond_6

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImage()Landroid/graphics/Bitmap;

    move-result-object v6

    if-nez v6, :cond_7

    .line 678
    :cond_6
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setImageAlpha(I)V

    goto :goto_3

    .line 680
    :cond_7
    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageAlpha(I)V

    goto :goto_3
.end method

.method public enableTSButton(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 629
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 630
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 631
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    .line 632
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 633
    return-void

    .line 629
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public endSelecteMode()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 782
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/samsung/android/sconnect/SconnectManager;->setDeviceSelectMode(Z)V

    .line 783
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v2, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 784
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 785
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTitleView:Landroid/widget/TextView;

    const v2, 0x7f090014

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 786
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionbarbtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 787
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionbarbtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 789
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isDiscovering()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 790
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshAnimView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 792
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshButtonLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 793
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 794
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCompleteButton:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 795
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 796
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 797
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f070011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 798
    .local v0, "paddingLeft":I
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 799
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContentTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 801
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->refreshDiscoveryDevice()V

    .line 802
    return-void
.end method

.method public expandActionList()V
    .locals 24

    .prologue
    .line 477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v9

    .line 478
    .local v9, "expandedDevice":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    if-nez v9, :cond_0

    .line 479
    const-string v20, "ViewManager"

    const-string v21, "expandActionList"

    const-string v22, "expandedDevice is null"

    invoke-static/range {v20 .. v22}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    :goto_0
    return-void

    .line 482
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    .line 483
    .local v19, "view":Landroid/widget/LinearLayout;
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    .line 487
    :cond_1
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v11

    .line 488
    .local v11, "id":I
    if-gez v11, :cond_2

    .line 489
    const-string v20, "ViewManager"

    const-string v21, "expandActionList"

    const-string v22, "expandedDevice\'s view is not on mDeviceListLayout"

    invoke-static/range {v20 .. v22}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 493
    :cond_2
    const/4 v12, 0x0

    .line 494
    .local v12, "isConnected":Z
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v20

    and-int/lit8 v20, v20, 0x4

    if-lez v20, :cond_3

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isConnected(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v12

    .line 498
    :cond_3
    const/4 v13, 0x0

    .line 499
    .local v13, "isMirroring":Z
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getState()I

    move-result v20

    sget v21, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_MIRRORING:I

    and-int v20, v20, v21

    if-lez v20, :cond_4

    .line 500
    const/4 v13, 0x1

    .line 503
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListAdapter:Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v12, v13}, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->setDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;ZZ)V

    .line 504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListAdapter:Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;->notifyDataSetChanged()V

    .line 505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListView:Landroid/widget/ListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setTag(Ljava/lang/Object;)V

    .line 508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup;

    .line 509
    .local v17, "parentViewGroup":Landroid/view/ViewGroup;
    if-eqz v17, :cond_5

    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 513
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 514
    .local v18, "size":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move/from16 v0, v18

    if-ge v10, v0, :cond_6

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v20

    new-instance v21, Lcom/samsung/android/sconnect/central/ui/ViewManager$9;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager$9;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 514
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 525
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 526
    const/4 v10, 0x0

    :goto_2
    move/from16 v0, v18

    if-ge v10, v0, :cond_7

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v20

    new-instance v21, Lcom/samsung/android/sconnect/central/ui/ViewManager$10;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager$10;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 526
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 538
    :cond_7
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v9, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 543
    .local v3, "actionListParams":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v3, :cond_8

    .line 544
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .end local v3    # "actionListParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {v19 .. v19}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionList:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->getActionListHeight(I)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v3, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 548
    .restart local v3    # "actionListParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_8
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v14

    .line 549
    .local v14, "itemView":Landroid/widget/LinearLayout;
    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 550
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 551
    const v20, 0x7f0c001d

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    .line 553
    .local v5, "contractBtn":Landroid/widget/ImageButton;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f090140

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f090141

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 556
    const v20, 0x7f0c0002

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 557
    .local v7, "deviceNameView":Landroid/widget/TextView;
    const v20, 0x7f0c001c

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 558
    .local v4, "actionNameView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBoldTypeface:Landroid/graphics/Typeface;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBoldTypeface:Landroid/graphics/Typeface;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 561
    invoke-virtual/range {v19 .. v19}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v20

    move/from16 v0, v20

    iput v0, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->getActionListHeight(I)I

    move-result v20

    move/from16 v0, v20

    iput v0, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 564
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 565
    .local v8, "deviceRect":Landroid/graphics/Rect;
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 566
    .local v16, "listRect":Landroid/graphics/Rect;
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 567
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f07000b

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v6, v0

    .line 570
    .local v6, "deviceHeight":I
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v20

    iget v0, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    move/from16 v21, v0

    add-int v21, v21, v6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_9

    .line 571
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v20

    sub-int v20, v20, v6

    move/from16 v0, v20

    iput v0, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 574
    :cond_9
    iget v0, v8, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_d

    .line 575
    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    iget v0, v8, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    sub-int v20, v20, v21

    sub-int v20, v20, v6

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    .line 591
    :cond_a
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    move/from16 v20, v0

    if-eqz v20, :cond_b

    .line 592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    new-instance v21, Lcom/samsung/android/sconnect/central/ui/ViewManager$11;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager$11;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 600
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 601
    new-instance v15, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual/range {v19 .. v19}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v20

    iget v0, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v15, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 602
    .local v15, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    add-int/lit8 v21, v11, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 603
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v20

    if-lez v20, :cond_c

    .line 604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 605
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addTroubleShootingButton()V

    .line 608
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollView:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    new-instance v21, Lcom/samsung/android/sconnect/central/ui/ViewManager$12;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager$12;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 625
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableTSButton(Z)V

    goto/16 :goto_0

    .line 578
    .end local v15    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_d
    const-string v20, "ViewManager"

    const-string v21, "expandActionList"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "listRect bottom: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    iget v0, v8, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_e

    .line 580
    iget v0, v8, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    add-int v20, v20, v6

    iget v0, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    move/from16 v21, v0

    add-int v20, v20, v21

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    .line 586
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    move/from16 v20, v0

    if-gez v20, :cond_a

    .line 587
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    goto/16 :goto_3

    .line 583
    :cond_e
    iget v0, v8, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    add-int v20, v20, v6

    iget v0, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    sub-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollGap:I

    goto :goto_4
.end method

.method public getActionListHeight(I)I
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 1271
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionItemHeight:I

    mul-int/2addr v0, p1

    iget v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListDividerHeight:I

    add-int/2addr v0, v1

    return v0
.end method

.method public hideDeviceLayoutView()V
    .locals 2

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceLayoutView:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1026
    return-void
.end method

.method public hideDeviceList()V
    .locals 2

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1012
    return-void
.end method

.method public hideFavoriteView()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1048
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1049
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1050
    return-void
.end method

.method public hideInitiatingView()V
    .locals 2

    .prologue
    .line 998
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mInitiatingView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 999
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1000
    return-void
.end method

.method public hideSearchingView()V
    .locals 2

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSearchingView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1004
    return-void
.end method

.method public initDeviceView()V
    .locals 2

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1030
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1031
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollView:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1032
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableTSButton(Z)V

    .line 1033
    return-void
.end method

.method public isFavoriteViewEmpty()Z
    .locals 1

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSelectionChanged(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .param p2, "isSelected"    # Z

    .prologue
    .line 401
    if-eqz p2, :cond_1

    .line 402
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 409
    .local v0, "selectedSize":I
    if-nez v0, :cond_2

    .line 410
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCompleteButton:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 414
    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setSelectModeView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 415
    return-void

    .line 406
    .end local v0    # "selectedSize":I
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 412
    .restart local v0    # "selectedSize":I
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCompleteButton:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1
.end method

.method public refreshDeviceList()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    .line 376
    return-void
.end method

.method public refreshDeviceListingMode()V
    .locals 8

    .prologue
    const v7, 0x7f09018e

    const v6, 0x7f090188

    const v5, 0x7f090160

    const v4, 0x7f09015f

    const v3, 0x7f09018f

    .line 1275
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1276
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1277
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentsRelatedMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1278
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1279
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyTitleView:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1281
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1282
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v3, 0x7f090164

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1297
    :goto_0
    return-void

    .line 1286
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1287
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyTitleView:Landroid/widget/RelativeLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1289
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1290
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v3, 0x7f090163

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1295
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public refreshFavoriteDeviceList()V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/ViewManager$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$6;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    .line 390
    return-void
.end method

.method public removeDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 6
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 1053
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    .line 1054
    .local v2, "parentView":Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    .line 1055
    .local v1, "isFavorite":Z
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1056
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    .line 1057
    const/4 v1, 0x1

    .line 1059
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    .line 1060
    .local v0, "expandedDevice":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1061
    const-string v3, "ViewManager"

    const-string v4, "removeDeviceView"

    const-string v5, "item is expanded device!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 1069
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1070
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1071
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v3, v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v3, v4, :cond_3

    .line 1072
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1073
    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->onSelectionChanged(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 1075
    :cond_2
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-nez v3, :cond_3

    .line 1076
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->endSelecteMode()V

    .line 1079
    :cond_3
    if-eqz v0, :cond_4

    .line 1080
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setExpandedDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 1081
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->expandActionList()V

    .line 1083
    :cond_4
    return-void

    .line 1063
    :cond_5
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v3

    if-ne v1, v3, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 1067
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    goto :goto_0
.end method

.method public setDisplaySize()V
    .locals 5

    .prologue
    .line 393
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 394
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 395
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mWidth:I

    .line 396
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mHeight:I

    .line 397
    const-string v1, "ViewManager"

    const-string v2, "setDisplaySize"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " x "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method public setSelectModeView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 13
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    const v12, 0x7f0c0015

    const v11, 0x7f0c001d

    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 686
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v5

    .line 688
    .local v5, "view":Landroid/view/View;
    const v6, 0x7f0c001e

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 689
    .local v0, "checkbox":Landroid/widget/CheckBox;
    const v6, 0x7f0c001c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 690
    .local v4, "primaryActionText":Landroid/widget/TextView;
    const v6, 0x7f0c0002

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 692
    .local v3, "nameText":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v6, v6, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v6, v7, :cond_4

    .line 693
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f07003a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    invoke-virtual {v3, v8, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 695
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 696
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v1

    .line 697
    .local v1, "deviceType":I
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v6

    if-eqz v6, :cond_2

    if-eq v1, v9, :cond_0

    const/4 v6, 0x2

    if-ne v1, v6, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v6

    const/4 v7, 0x4

    if-eq v6, v7, :cond_2

    .line 700
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 701
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 702
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 703
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 704
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSelectedDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 705
    invoke-virtual {p0, p1, v9}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 706
    invoke-virtual {v5, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 714
    :goto_0
    const v6, 0x7f0c001e

    invoke-virtual {v5, v6}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 715
    invoke-virtual {v0, v12}, Landroid/widget/CheckBox;->setNextFocusLeftId(I)V

    .line 730
    .end local v1    # "deviceType":I
    :cond_1
    :goto_1
    return-void

    .line 708
    .restart local v1    # "deviceType":I
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 709
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 711
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 717
    .end local v1    # "deviceType":I
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f070039

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    invoke-virtual {v3, v8, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 719
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 720
    invoke-virtual {v0, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 721
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 722
    invoke-virtual {p0, p1, v9}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 724
    :cond_5
    invoke-virtual {v5, v11}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 725
    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v6, v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 726
    .local v2, "expandButton":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 727
    invoke-virtual {v2, v12}, Landroid/view/View;->setNextFocusLeftId(I)V

    goto :goto_1
.end method

.method public setViews()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f09015f

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 149
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f030012

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setContentView(I)V

    .line 150
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f040001

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->overridePendingTransition(II)V

    .line 160
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 161
    .local v0, "outValue":Landroid/util/TypedValue;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x101030e

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 163
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBackgroundResId:I

    .line 165
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionItemHeight:I

    .line 166
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f070009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListDividerHeight:I

    .line 169
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c004d

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSlidingDrawer:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    .line 170
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSlidingDrawer:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->open()V

    .line 171
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSlidingDrawer:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->requestFocus()Z

    .line 172
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0057

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceLayoutView:Landroid/widget/RelativeLayout;

    .line 175
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c004f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v4, 0x7f090149

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v4, 0x7f09014a

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0051

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mInitiatingView:Landroid/widget/LinearLayout;

    .line 180
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mInitiatingView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 182
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0054

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSearchingView:Landroid/widget/LinearLayout;

    .line 183
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSearchingView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 185
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c002b

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    .line 186
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 188
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c002e

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyTitleView:Landroid/widget/RelativeLayout;

    .line 189
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyTitleView:Landroid/widget/RelativeLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v4, 0x7f090188

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c002c

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteTitleView:Landroid/widget/TextView;

    .line 192
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteTitleView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v4, 0x7f090187

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c002d

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    .line 195
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0031

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    .line 196
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c002f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyTitleTextView:Landroid/widget/TextView;

    .line 198
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 199
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshDeviceList()V

    .line 200
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshFavoriteDeviceList()V

    .line 202
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 203
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showFavoriteView()V

    .line 208
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0030

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    .line 209
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListingText:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 211
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshDeviceListingMode()V

    .line 222
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0022

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTitleView:Landroid/widget/TextView;

    .line 223
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0029

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContentTextView:Landroid/widget/TextView;

    .line 224
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c002a

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollView:Landroid/widget/ScrollView;

    .line 225
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0025

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshAnimView:Landroid/widget/ImageView;

    .line 226
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0024

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshButton:Landroid/widget/Button;

    .line 227
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0023

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshButtonLayout:Landroid/widget/RelativeLayout;

    .line 228
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0027

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingButton:Landroid/widget/ImageButton;

    .line 229
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0026

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCancelButton:Landroid/widget/TextView;

    .line 230
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0028

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCompleteButton:Landroid/widget/TextView;

    .line 231
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCancelButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mCompleteButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTooltipListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 236
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTooltipListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 237
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0021

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mBackButton:Landroid/widget/ImageView;

    .line 238
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v2, 0x7f0c0020

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionbarbtnLayout:Landroid/widget/LinearLayout;

    .line 241
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContentTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContentsText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContentTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContentsText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 244
    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingButton:Landroid/widget/ImageButton;

    const v4, 0x800005

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    .line 245
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0b0001

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 246
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingItemListener:Landroid/widget/PopupMenu$OnMenuItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 247
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/ViewManager$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$1;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 254
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0c00af

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isChinaNalSecurity()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 256
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isVerizon()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 257
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0c00ae

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0900bf

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 259
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setDisableHelpMenu()V

    .line 261
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v2, 0x7f040002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshAnimation:Landroid/view/animation/Animation;

    .line 263
    new-instance v1, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionList:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListAdapter:Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;

    .line 264
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030002

    invoke-virtual {v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    .line 266
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0c000c

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListView:Landroid/widget/ListView;

    .line 267
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListAdapter:Lcom/samsung/android/sconnect/central/ui/ActionListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 268
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 269
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mActionListView:Landroid/widget/ListView;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/ViewManager$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$2;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 288
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSlidingDrawer:Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/ViewManager$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$3;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer;->setOnDrawerCloseListener(Lcom/samsung/android/sconnect/central/ui/CustomSlidingDrawer$OnDrawerCloseListener;)V

    .line 296
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    if-nez v1, :cond_1

    .line 297
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03001a

    invoke-virtual {v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    .line 299
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    const v2, 0x7f0c006e

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    .line 300
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    const v2, 0x7f0c006d

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButtonCenter:Landroid/widget/Button;

    .line 301
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSView:Landroid/widget/LinearLayout;

    const v2, 0x7f0c006c

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTsNoDeviceText:Landroid/widget/TextView;

    .line 302
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mTSButtonCenter:Landroid/widget/Button;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 306
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v1, v2, :cond_4

    .line 307
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getSelectedItem()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 318
    :cond_2
    :goto_1
    return-void

    .line 205
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideFavoriteView()V

    goto/16 :goto_0

    .line 308
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v1, v2, :cond_5

    .line 309
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->startMultiSelecteMode()V

    goto :goto_1

    .line 310
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->NONE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v1, v2, :cond_6

    .line 311
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideSearchingView()V

    .line 312
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideDeviceList()V

    goto :goto_1

    .line 313
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->INITIATING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v1, v2, :cond_7

    .line 314
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showInitiatingView()V

    goto :goto_1

    .line 315
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isDiscovering()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 316
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->animateSearchingView()V

    goto :goto_1
.end method

.method public showDeviceLayoutView()V
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceLayoutView:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1020
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshDeviceList()V

    .line 1021
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshFavoriteDeviceList()V

    .line 1022
    return-void
.end method

.method public showDeviceList()V
    .locals 2

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mListLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1016
    return-void
.end method

.method public showFavoriteView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1036
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1037
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1038
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mFavoriteListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1039
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshFavoriteDeviceList()V

    .line 1041
    :cond_0
    return-void
.end method

.method public showInitiatingView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 993
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 994
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mInitiatingView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 995
    return-void
.end method

.method public showSearchingView()V
    .locals 2

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSearchingView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1008
    return-void
.end method

.method public showSendingView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 5
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 1128
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    sget v4, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_FILESHARE:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addState(I)V

    .line 1129
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v2

    .line 1130
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 1131
    const v3, 0x7f0c0019

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1132
    .local v1, "deviceIconView":Landroid/widget/ImageView;
    const v3, 0x7f0c0018

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1133
    .local v0, "deviceIconBgView":Landroid/widget/ImageView;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const v4, 0x7f090015

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1134
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1135
    const v3, 0x7f02000d

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1136
    const v3, 0x7f020044

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1138
    .end local v0    # "deviceIconBgView":Landroid/widget/ImageView;
    .end local v1    # "deviceIconView":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method public showSettingMenu()V
    .locals 3

    .prologue
    .line 1108
    const-string v0, "ViewManager"

    const-string v1, "showSettingMenu"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mIsMenuShowing:Z

    if-eqz v0, :cond_0

    .line 1110
    const-string v0, "ViewManager"

    const-string v1, "showSettingMenu"

    const-string v2, "already showing! do not show duplicately"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1115
    :goto_0
    return-void

    .line 1112
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mIsMenuShowing:Z

    .line 1113
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSettingMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0
.end method

.method public startMultiSelecteMode()V
    .locals 2

    .prologue
    .line 733
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    iput-object v1, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    .line 734
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->startSelecteMode()V

    .line 735
    return-void
.end method

.method public stopSearchingView()V
    .locals 2

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mRefreshAnimView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1100
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSearchingView:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1101
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->NONE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->FINISHING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-eq v0, v1, :cond_0

    .line 1103
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addTroubleShootingButton()V

    .line 1105
    :cond_0
    return-void
.end method

.method public stopSendingView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 2
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 1156
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_FILESHARE:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->removeState(I)V

    .line 1157
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->updateDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 1158
    return-void
.end method

.method public updateActionName(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 5
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 979
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v2

    .line 980
    .local v2, "view":Landroid/widget/LinearLayout;
    const v3, 0x7f0c001c

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 981
    .local v1, "actionNameView":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getPrimaryActionName(I)Ljava/lang/String;

    move-result-object v0

    .line 982
    .local v0, "actionName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v3

    if-nez v3, :cond_0

    .line 983
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isDiscovering()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 984
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v4, 0x7f0900b8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 989
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 990
    return-void

    .line 986
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    const v4, 0x7f090186

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public updateDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 27
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 805
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v22

    .line 806
    .local v22, "view":Landroid/widget/LinearLayout;
    const v23, 0x7f0c0019

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 807
    .local v6, "deviceIconView":Landroid/widget/ImageView;
    const v23, 0x7f0c0018

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 808
    .local v5, "deviceIconBgView":Landroid/widget/ImageView;
    const v23, 0x7f0c0002

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 809
    .local v8, "deviceNameView":Landroid/widget/TextView;
    const v23, 0x7f0c001c

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 810
    .local v3, "actionNameView":Landroid/widget/TextView;
    const v23, 0x7f0c001d

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageButton;

    .line 811
    .local v12, "expandButtonView":Landroid/widget/ImageButton;
    const v23, 0x7f0c001a

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 812
    .local v15, "indicatorView":Landroid/widget/ImageView;
    const v23, 0x7f0c0015

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 814
    .local v7, "deviceLayout":Landroid/widget/LinearLayout;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 818
    const/16 v23, 0x2

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 819
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v13

    .line 820
    .local v13, "expandedDevice":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v10

    .line 821
    .local v10, "deviceType":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v19

    .line 822
    .local v19, "primaryAction":I
    const/16 v23, 0xb

    move/from16 v0, v19

    move/from16 v1, v23

    if-eq v0, v1, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getActionList()Ljava/util/ArrayList;

    move-result-object v23

    if-eqz v23, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v23

    if-eqz v23, :cond_9

    .line 824
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_8

    .line 826
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f09013e

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f09013f

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 837
    :goto_0
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 838
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 839
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 840
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 844
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImageType()I

    move-result v14

    .line 845
    .local v14, "imageType":I
    if-lez v14, :cond_a

    .line 846
    const/16 v23, 0x1d

    move/from16 v0, v23

    if-lt v14, v0, :cond_0

    .line 847
    const/16 v14, 0x1c

    .line 849
    :cond_0
    sget-object v23, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceImages:[I

    add-int/lit8 v24, v14, -0x1

    aget v23, v23, v24

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 850
    sget-object v23, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceBgImages:[I

    add-int/lit8 v24, v14, -0x1

    aget v23, v23, v24

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 868
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v18

    .line 869
    .local v18, "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-object/from16 v23, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getDeviceString(I)Ljava/lang/String;

    move-result-object v11

    .line 870
    .local v11, "deviceTypeString":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 871
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->updateActionName(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 873
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 874
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 875
    .local v21, "tbString":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v23

    if-eqz v23, :cond_e

    .line 876
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f09015a

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 877
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    if-eqz v23, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImageType()I

    move-result v23

    const/16 v24, -0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_1

    .line 878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-object/from16 v23, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImageType()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getDeviceString(I)Ljava/lang/String;

    move-result-object v11

    .line 885
    :cond_1
    :goto_3
    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 886
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 887
    const v23, 0x7f0c0018

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    .line 888
    const v23, 0x7f0c0015

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setNextFocusRightId(I)V

    .line 890
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getPrimaryActionTTS(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 892
    .local v4, "deviceDescription":Ljava/lang/String;
    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 894
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v9

    .line 895
    .local v9, "deviceNetType":I
    const/16 v16, 0x0

    .line 896
    .local v16, "isConnected":Z
    and-int/lit8 v23, v9, 0x4

    if-gtz v23, :cond_2

    and-int/lit8 v23, v9, 0x8

    if-lez v23, :cond_f

    .line 898
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isConnected(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v16

    .line 904
    :cond_3
    :goto_4
    const/16 v17, 0x0

    .line 905
    .local v17, "isMirroring":Z
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getState()I

    move-result v23

    sget v24, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_MIRRORING:I

    and-int v23, v23, v24

    if-lez v23, :cond_4

    .line 906
    const/16 v17, 0x1

    .line 908
    :cond_4
    const-string v23, "ViewManager"

    const-string v24, "updateDeviceView"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", deviceNetType : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", isConnected : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", isMyGroupDevice : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isMyGroupDevice()Z

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getState()I

    move-result v23

    sget v24, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_SENDING:I

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_5

    .line 912
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 913
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 914
    const v23, 0x7f02000d

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 915
    const v23, 0x7f020044

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 918
    :cond_5
    if-eqz v16, :cond_10

    .line 919
    const v23, 0x7f02006a

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 920
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 921
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v23

    const/16 v24, 0xc

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_6

    .line 922
    const v23, 0x7f09002a

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 923
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f090152

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 925
    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 970
    :cond_6
    :goto_5
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->setSelectModeView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 971
    if-eqz v13, :cond_7

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_7

    .line 972
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->enableDeviceItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 975
    :cond_7
    invoke-virtual/range {v22 .. v22}, Landroid/widget/LinearLayout;->invalidate()V

    .line 976
    return-void

    .line 832
    .end local v4    # "deviceDescription":Ljava/lang/String;
    .end local v9    # "deviceNetType":I
    .end local v11    # "deviceTypeString":Ljava/lang/String;
    .end local v14    # "imageType":I
    .end local v16    # "isConnected":Z
    .end local v17    # "isMirroring":Z
    .end local v18    # "name":Ljava/lang/String;
    .end local v21    # "tbString":Ljava/lang/String;
    :cond_8
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f09013e

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f090141

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 842
    :cond_9
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 851
    .restart local v14    # "imageType":I
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImage()Landroid/graphics/Bitmap;

    move-result-object v23

    if-eqz v23, :cond_b

    .line 852
    const v23, 0x106000d

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 854
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    move-object/from16 v23, v0

    const v24, 0x7f02000e

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    check-cast v20, Landroid/graphics/drawable/RippleDrawable;

    .line 856
    .local v20, "rd":Landroid/graphics/drawable/RippleDrawable;
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/RippleDrawable;->getId(I)I

    move-result v23

    new-instance v24, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mResources:Landroid/content/res/Resources;

    move-object/from16 v25, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImage()Landroid/graphics/Bitmap;

    move-result-object v26

    invoke-direct/range {v24 .. v26}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v20

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/RippleDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 858
    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 860
    .end local v20    # "rd":Landroid/graphics/drawable/RippleDrawable;
    :cond_b
    move v14, v10

    .line 861
    if-ltz v14, :cond_c

    const/16 v23, 0x1d

    move/from16 v0, v23

    if-lt v14, v0, :cond_d

    .line 862
    :cond_c
    const/16 v14, 0x1c

    .line 864
    :cond_d
    sget-object v23, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceImages:[I

    add-int/lit8 v24, v14, -0x1

    aget v23, v23, v24

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 865
    sget-object v23, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceBgImages:[I

    add-int/lit8 v24, v14, -0x1

    aget v23, v23, v24

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 882
    .restart local v11    # "deviceTypeString":Ljava/lang/String;
    .restart local v18    # "name":Ljava/lang/String;
    .restart local v21    # "tbString":Ljava/lang/String;
    :cond_e
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f09015b

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_3

    .line 899
    .restart local v4    # "deviceDescription":Ljava/lang/String;
    .restart local v9    # "deviceNetType":I
    .restart local v16    # "isConnected":Z
    :cond_f
    and-int/lit8 v23, v9, 0x2

    if-lez v23, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isConnecting(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 901
    const v23, 0x7f0900d1

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_4

    .line 927
    .restart local v17    # "isMirroring":Z
    :cond_10
    if-eqz v17, :cond_13

    .line 928
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v23

    const/16 v24, 0x5

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_11

    .line 929
    const v23, 0x7f090028

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 931
    :cond_11
    and-int/lit8 v23, v9, 0x10

    if-lez v23, :cond_12

    .line 932
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 933
    :cond_12
    and-int/lit8 v23, v9, 0x2

    if-lez v23, :cond_6

    .line 934
    const v23, 0x7f02006c

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 935
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 938
    :cond_13
    const/16 v23, 0x1

    move/from16 v0, v23

    if-eq v10, v0, :cond_14

    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v10, v0, :cond_19

    .line 939
    :cond_14
    and-int/lit8 v23, v9, 0x8

    if-lez v23, :cond_15

    .line 940
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 941
    :cond_15
    and-int/lit8 v23, v9, 0x2

    if-lez v23, :cond_16

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isMyGroupDevice()Z

    move-result v23

    if-nez v23, :cond_16

    .line 943
    const v23, 0x7f02006c

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 944
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 945
    :cond_16
    and-int/lit8 v23, v9, 0x10

    if-lez v23, :cond_17

    .line 946
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 947
    :cond_17
    and-int/lit8 v23, v9, 0x4

    if-lez v23, :cond_18

    .line 948
    const v23, 0x7f020069

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 949
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 951
    :cond_18
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 954
    :cond_19
    and-int/lit8 v23, v9, 0x8

    if-gtz v23, :cond_1a

    and-int/lit8 v23, v9, 0x10

    if-lez v23, :cond_1b

    .line 956
    :cond_1a
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 957
    :cond_1b
    and-int/lit8 v23, v9, 0x2

    if-lez v23, :cond_1c

    .line 958
    const v23, 0x7f02006c

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 959
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 960
    :cond_1c
    and-int/lit8 v23, v9, 0x4

    if-lez v23, :cond_1d

    .line 961
    const v23, 0x7f020069

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 962
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 964
    :cond_1d
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5
.end method
