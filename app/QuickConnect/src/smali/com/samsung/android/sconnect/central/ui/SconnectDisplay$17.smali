.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;
.super Landroid/os/Handler;
.source "SconnectDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 1842
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 27
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1845
    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1847
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v23, v0

    packed-switch v23, :pswitch_data_0

    .line 2166
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1849
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_1

    .line 1850
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_ADD_DEVICE"

    const-string v25, "current layout is TV, return"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1853
    :cond_1
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 1854
    .local v4, "addDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_ADD_DEVICE"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "added device : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideSearchingView()V

    .line 1857
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showDeviceList()V

    .line 1858
    new-instance v7, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ActionFormatter;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v7, v4, v0, v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V

    .line 1860
    .local v7, "addItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v23

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_3

    .line 1861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v23

    if-nez v23, :cond_4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_4

    .line 1863
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    .line 1864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1865
    .local v11, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v8

    .line 1866
    .local v8, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v23

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    .line 1868
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_ADD_DEVICE"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Wi-fi/DLNA tv added, remove BT TV: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbb9

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v8}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 1873
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getDiscardedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v0, v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v23

    if-nez v23, :cond_3

    .line 1874
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDiscardDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1892
    .end local v8    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateIfFavoriteDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z
    invoke-static {v0, v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v23

    if-nez v23, :cond_0

    .line 1893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ActionFormatter;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v24

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->listingActions(Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V

    .line 1894
    invoke-virtual {v7}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v23

    if-gez v23, :cond_5

    .line 1895
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateUselessDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    invoke-static {v0, v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto/16 :goto_0

    .line 1879
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v23

    if-eqz v23, :cond_3

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_3

    .line 1881
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_ADD_DEVICE"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Already wi-fi/dlna TV added, don\'t add BT TV: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1885
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getDiscardedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v0, v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v23

    if-nez v23, :cond_0

    .line 1886
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDiscardDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1899
    :cond_5
    invoke-virtual {v7}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getHasContactInfo()Z

    move-result v23

    if-nez v23, :cond_6

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    and-int/lit8 v23, v23, 0x4

    if-lez v23, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isConnected(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 1901
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 1902
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1906
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    const/16 v24, -0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 1907
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1913
    .end local v4    # "addDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v7    # "addItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :pswitch_2
    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v14, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 1914
    .local v14, "removeDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_REMOVE_DEVICE"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "removed device : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v14}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1916
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_8

    .line 1917
    invoke-virtual {v14}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v23

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_0

    .line 1921
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeIfFavoriteDevice(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z
    invoke-static {v0, v14}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v23

    if-nez v23, :cond_a

    .line 1922
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_9
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1923
    .restart local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 1924
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    and-int/lit8 v23, v23, 0x8

    if-lez v23, :cond_c

    invoke-virtual {v14}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    and-int/lit8 v23, v23, 0x8

    if-nez v23, :cond_c

    .line 1926
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_REMOVE_DEVICE"

    const-string v25, "ignore removing BLE Device"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1941
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_a
    :goto_1
    invoke-virtual {v14}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v23

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_0

    invoke-virtual {v14}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 1943
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    .line 1944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1945
    .restart local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v8

    .line 1946
    .restart local v8    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v23

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_b

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_b

    .line 1948
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    goto/16 :goto_0

    .line 1929
    .end local v8    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_c
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_REMOVE_DEVICE"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "remove : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1931
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_a

    .line 1932
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_REMOVE_DEVICE"

    const-string v25, "Remove only unselected Devices"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1934
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1935
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->removeDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto/16 :goto_1

    .line 1955
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v14    # "removeDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :pswitch_3
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 1956
    .local v20, "updateDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_UPDATE_DEVICE"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "updated device : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1958
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_10

    .line 1959
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v23

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_0

    .line 1960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v23

    if-nez v23, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->sameNameCheck(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 1962
    :cond_d
    new-instance v21, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ActionFormatter;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V

    .line 1964
    .local v21, "updateItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1102(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1965
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/TvController;

    move-result-object v23

    if-nez v23, :cond_e

    .line 1966
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 1968
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mTvController:Lcom/samsung/android/sconnect/central/ui/TvController;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/TvController;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/TvController;->actionTvController(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 1970
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_f
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 1971
    .restart local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v8

    .line 1972
    .restart local v8    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 1973
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDeviceView()Landroid/widget/LinearLayout;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->setDeviceView(Landroid/widget/LinearLayout;)V

    .line 1974
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v13

    .line 1975
    .local v13, "position":I
    const-string v23, "SconnectDisplay"

    const-string v24, "updateIfFavoriteDevice"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "TV is favorite device: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1977
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->updateFavoriteDeviceInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 1978
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v13, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1987
    .end local v8    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v13    # "position":I
    .end local v21    # "updateItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_10
    new-instance v21, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ActionFormatter;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v23

    move/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V

    .line 1988
    .restart local v21    # "updateItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateIfFavoriteDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v23

    if-nez v23, :cond_0

    .line 1989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ActionFormatter;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v24

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->listingActions(Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V

    .line 1990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Z

    move-result v23

    if-nez v23, :cond_0

    .line 1991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getDiscardedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v9

    .line 1992
    .local v9, "discard":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    if-eqz v9, :cond_0

    .line 1993
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_UPDATE_DEVICE - Discarded device updated, ADD :"

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1995
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDiscardDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1996
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbb8

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 2002
    .end local v9    # "discard":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v20    # "updateDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v21    # "updateItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :pswitch_4
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_DISCOVERY_STARTED"

    const-string v25, ""

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2003
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbb8

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeGuiMessage(I)V

    .line 2004
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbb9

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeGuiMessage(I)V

    .line 2005
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbba

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeGuiMessage(I)V

    .line 2006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbbe

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeGuiMessage(I)V

    .line 2007
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbbf

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeGuiMessage(I)V

    .line 2008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbc0

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeGuiMessage(I)V

    .line 2009
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0xbc1

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->removeGuiMessage(I)V

    .line 2010
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4402(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    goto/16 :goto_0

    .line 2013
    :pswitch_5
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_DISCOVERY_FINISHED"

    const-string v25, ""

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2014
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4402(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    .line 2015
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->stopSearchingView()V

    .line 2016
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_11
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2017
    .restart local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v23

    if-nez v23, :cond_11

    .line 2018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->updateActionName(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto :goto_2

    .line 2023
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 2026
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2027
    .local v17, "sendingItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v16

    .line 2028
    .local v16, "sendingDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_START_SENDING_VIEW"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "sending device : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2030
    sget v23, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_FILESHARE:I

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addState(I)V

    .line 2031
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v12

    .line 2032
    .local v12, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/central/ui/DisplayItem;>;"
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v23

    if-eqz v23, :cond_12

    .line 2033
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v12

    .line 2035
    :cond_12
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2036
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z

    goto/16 :goto_0

    .line 2039
    .end local v12    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/central/ui/DisplayItem;>;"
    .end local v16    # "sendingDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v17    # "sendingItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 2042
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2043
    .local v18, "sendingItem2":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_SHOW_SENDING_VIEW"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "sending device : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2045
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showSendingView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto/16 :goto_0

    .line 2048
    .end local v18    # "sendingItem2":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 2051
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2052
    .local v19, "sendingItem3":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_ANIMATE_SENDING_VIEW"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "sending device : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2054
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->animateSendingView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto/16 :goto_0

    .line 2057
    .end local v19    # "sendingItem3":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->TV:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 2060
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_REMOVE_SENDING_VIEW"

    const-string v25, ""

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2061
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v24

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->stopSendingView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto/16 :goto_0

    .line 2064
    :pswitch_a
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_START_SCONNECT_DISPLAY"

    const-string v25, ""

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2065
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->checkChinaAndStart()V
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    goto/16 :goto_0

    .line 2068
    :pswitch_b
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_FINISH_DISPLAY"

    const-string v25, ""

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2069
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 2072
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v23, v0

    sget-object v24, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 2073
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showSettingMenu()V

    goto/16 :goto_0

    .line 2077
    :pswitch_d
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Landroid/view/View;

    .line 2078
    .local v22, "view":Landroid/view/View;
    if-eqz v22, :cond_0

    goto/16 :goto_0

    .line 2083
    .end local v22    # "view":Landroid/view/View;
    :pswitch_e
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_HOME_RESUMED"

    const-string v25, "home resumed. finish!"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2084
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->terminateCentralManager()V
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    .line 2085
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 2088
    :pswitch_f
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_START_HOME_CHECKER"

    const-string v25, ""

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    move-result-object v23

    if-eqz v23, :cond_0

    .line 2090
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->startHomeScreenCheck()V

    goto/16 :goto_0

    .line 2094
    :pswitch_10
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_CANCEL_HOME_CHECKER"

    const-string v25, ""

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2095
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    move-result-object v23

    if-eqz v23, :cond_0

    .line 2096
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mHomeChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4700(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->cancelHomeScreenCheck()V

    goto/16 :goto_0

    .line 2100
    :pswitch_11
    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 2101
    .local v5, "addFavoriteDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_ADD_FAVORITE_DEVICE"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "added favorite device : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2103
    new-instance v6, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ActionFormatter;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v6, v5, v0, v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V

    .line 2105
    .local v6, "addFavoriteItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavorite(Z)V

    .line 2106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mExpandedDevice:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v23

    if-eqz v23, :cond_13

    .line 2107
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 2109
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    const/16 v24, -0x1

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 2110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideSearchingView()V

    .line 2112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showDeviceList()V

    .line 2113
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showFavoriteView()V

    goto/16 :goto_0

    .line 2117
    .end local v5    # "addFavoriteDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v6    # "addFavoriteItem":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :pswitch_12
    move-object/from16 v0, p1

    iget-object v15, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v15, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 2118
    .local v15, "removeFavoriteDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v23, "SconnectDisplay"

    const-string v24, "MSG_REMOVE_FAVORITE_DEVICE"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "removed favorite device : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_14
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_16

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2121
    .restart local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 2122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mFavoriteDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->removeDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 2124
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v23

    if-eqz v23, :cond_15

    .line 2125
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavorite(Z)V

    .line 2126
    const/16 v23, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavoriteImage(Landroid/graphics/Bitmap;)V

    .line 2127
    const/16 v23, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavoriteName(Ljava/lang/String;)V

    .line 2129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionformatter:Lcom/samsung/android/sconnect/central/ActionFormatter;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ActionFormatter;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v24, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsContentsRelatedMode:Z
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v24

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->listingActions(Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V

    .line 2130
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v23

    if-gez v23, :cond_18

    .line 2131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->updateUselessDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    invoke-static {v0, v11}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 2140
    :cond_15
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->isFavoriteViewEmpty()Z

    move-result v23

    if-eqz v23, :cond_16

    .line 2141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideFavoriteView()V

    .line 2146
    .end local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_16
    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v23

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_0

    invoke-virtual {v15}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 2148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    .line 2149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_17
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2150
    .restart local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v8

    .line 2151
    .restart local v8    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v23

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_17

    invoke-virtual {v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v23

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_17

    .line 2153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsTvDiscovered:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    goto/16 :goto_0

    .line 2133
    .end local v8    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->hideSearchingView()V

    .line 2134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->showDeviceList()V

    .line 2135
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v11, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z

    move-result v23

    if-eqz v23, :cond_15

    .line 2136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mDeviceList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$3300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_3

    .line 2160
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v15    # "removeFavoriteDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :pswitch_13
    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2161
    .restart local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$17;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mViewManager:Lcom/samsung/android/sconnect/central/ui/ViewManager;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/ViewManager;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->updateDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    goto/16 :goto_0

    .line 1847
    :pswitch_data_0
    .packed-switch 0xbb8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method
