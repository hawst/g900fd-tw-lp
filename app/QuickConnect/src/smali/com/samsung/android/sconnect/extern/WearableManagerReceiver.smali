.class public Lcom/samsung/android/sconnect/extern/WearableManagerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WearableManagerReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "WearableManagerReceiver"

.field private static final WM_INTENT:Ljava/lang/String; = "com.samsung.android.sconnect.action.CONNECT_WEARABLE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getDeviceType(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 32
    .local v0, "retDeviceType":I
    const-string v1, "watchmanager"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    .line 34
    :cond_0
    const-string v1, "wms"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 35
    const/4 v0, 0x2

    goto :goto_0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 18
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 19
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.samsung.android.sconnect.action.CONNECT_WEARABLE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 20
    const-string v3, "WM_MANAGER"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 21
    .local v2, "packageInfo":Ljava/lang/String;
    const-string v3, "MAC"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 22
    .local v1, "deviceMac":Ljava/lang/String;
    const-string v3, "WearableManagerReceiver"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "packageInfo : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Mac : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 24
    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/extern/WearableManagerReceiver;->getDeviceType(Ljava/lang/String;)I

    move-result v3

    invoke-static {p1, v1, v3, v2}, Lcom/samsung/android/sconnect/central/action/WearableManagerActionHelper;->startWearableManager(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    .line 28
    .end local v1    # "deviceMac":Ljava/lang/String;
    .end local v2    # "packageInfo":Ljava/lang/String;
    :cond_0
    return-void
.end method
