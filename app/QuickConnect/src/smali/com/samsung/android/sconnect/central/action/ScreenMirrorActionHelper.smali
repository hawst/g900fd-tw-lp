.class public Lcom/samsung/android/sconnect/central/action/ScreenMirrorActionHelper;
.super Ljava/lang/Object;
.source "ScreenMirrorActionHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ScreenMirrorActionHelper"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, "ScreenMirrorActionHelper"

    const-string v1, "ScreenMirrorActionHelper"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    return-void
.end method


# virtual methods
.method public stopMirroring()V
    .locals 3

    .prologue
    .line 20
    const-string v0, "ScreenMirrorActionHelper"

    const-string v1, "stopMirroring"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getWfdUtil()Lcom/samsung/android/sconnect/common/util/WfdUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->disconnectWifiDisplay()V

    .line 22
    return-void
.end method
