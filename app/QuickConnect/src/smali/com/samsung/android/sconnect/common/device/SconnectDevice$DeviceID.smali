.class public Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
.super Ljava/lang/Object;
.source "SconnectDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/device/SconnectDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DeviceID"
.end annotation


# instance fields
.field public final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;",
            ">;"
        }
    .end annotation
.end field

.field public mBleMac:Ljava/lang/String;

.field public mBtMac:Ljava/lang/String;

.field public mDeviceIP:Ljava/lang/String;

.field public mP2pMac:Ljava/lang/String;

.field public mUpnpUUID:Ljava/lang/String;

.field public mWifiMac:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/sconnect/common/device/SconnectDevice;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 948
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->this$0:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 949
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    .line 950
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 951
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    .line 952
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    .line 953
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    .line 954
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    .line 1121
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID$1;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1104
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1013
    if-ne p1, p0, :cond_1

    .line 1063
    :cond_0
    :goto_0
    return v1

    .line 1016
    :cond_1
    if-nez p1, :cond_3

    .line 1017
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    .line 1021
    goto :goto_0

    .line 1025
    :cond_3
    instance-of v3, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    if-eqz v3, :cond_a

    move-object v0, p1

    .line 1026
    check-cast v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    .line 1027
    .local v0, "in":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1031
    :cond_4
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1035
    :cond_5
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1039
    :cond_6
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 1040
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1043
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v3, v4, :cond_7

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1046
    :cond_7
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v3, v4, :cond_8

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1053
    :cond_8
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    if-eqz v3, :cond_9

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1058
    :cond_9
    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-eqz v3, :cond_a

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .end local v0    # "in":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    :cond_a
    move v1, v2

    .line 1063
    goto/16 :goto_0
.end method

.method public exclusive(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 957
    if-nez p1, :cond_1

    .line 1008
    :cond_0
    :goto_0
    return v1

    .line 960
    :cond_1
    instance-of v2, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 961
    check-cast v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    .line 962
    .local v0, "in":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 963
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 967
    :cond_2
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 968
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 972
    :cond_3
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 973
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 977
    :cond_4
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 978
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 982
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v2, v3, :cond_5

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 986
    :cond_5
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 994
    :cond_6
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 995
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1000
    :cond_7
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 1001
    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1006
    :cond_8
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public hasMac()Z
    .locals 1

    .prologue
    .line 1095
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setValue(Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;)V
    .locals 2
    .param p1, "in"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    .prologue
    .line 1068
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1069
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    .line 1071
    :cond_0
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1072
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 1074
    :cond_1
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1075
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    .line 1077
    :cond_2
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1078
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1079
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    .line 1085
    :cond_3
    :goto_0
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 1086
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    .line 1089
    :cond_4
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1090
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    .line 1092
    :cond_5
    return-void

    .line 1081
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1082
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1113
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1114
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1115
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1116
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1117
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1119
    return-void
.end method
