.class public Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper$FavoriteDeviceDatabase;
.super Ljava/lang/Object;
.source "FavoriteDeviceDBHelper.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FavoriteDeviceDatabase"
.end annotation


# static fields
.field public static final _CREATE:Ljava/lang/String; = "create table SConnect_Favorite_TABLE(_id integer primary key autoincrement, UUID text not null , P2PMAC text not null , WIFIMAC text not null , BTMAC text not null , BLEMAC text not null , NAME text not null , IMAGE text not null , TYPE text not null );"

.field public static final _TABLENAME:Ljava/lang/String; = "SConnect_Favorite_TABLE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
