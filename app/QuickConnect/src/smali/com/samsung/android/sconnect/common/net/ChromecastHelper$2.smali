.class Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;
.super Ljava/lang/Object;
.source "ChromecastHelper.java"

# interfaces
.implements Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/ChromecastHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/sec/android/sdial/Device;)V
    .locals 6
    .param p1, "sdialDevice"    # Lcom/sec/android/sdial/Device;

    .prologue
    .line 150
    const-string v2, "ChromecastHelper"

    const-string v3, "onDeviceAdded"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/sdial/Device;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/sdial/Device;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$200(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 153
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;

    invoke-virtual {p1}, Lcom/sec/android/sdial/Device;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/sdial/Device;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;
    new-instance v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$300(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 156
    .local v1, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;)V

    .line 157
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$400(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 158
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$400(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$200(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 161
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$200(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 164
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;
    .end local v1    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_1
    return-void
.end method

.method public onDeviceRemoved(Lcom/sec/android/sdial/Device;)V
    .locals 6
    .param p1, "sdialDevice"    # Lcom/sec/android/sdial/Device;

    .prologue
    .line 131
    const-string v2, "ChromecastHelper"

    const-string v3, "onDeviceRemoved"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/sdial/Device;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/sdial/Device;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$200(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 134
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;

    invoke-virtual {p1}, Lcom/sec/android/sdial/Device;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/sdial/Device;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;
    new-instance v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$300(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 137
    .local v1, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v1, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;)V

    .line 138
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$400(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 139
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$400(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 141
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$200(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 142
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->access$200(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 145
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;
    .end local v1    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_1
    return-void
.end method
