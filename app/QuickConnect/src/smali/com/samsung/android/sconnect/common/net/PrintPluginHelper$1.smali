.class Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;
.super Ljava/lang/Object;
.source "PrintPluginHelper.java"

# interfaces
.implements Landroid/print/PrinterDiscoverySession$OnPrintersChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->startSearchViaPlugin()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrintersChanged()V
    .locals 10

    .prologue
    .line 185
    const-string v6, "PrintPluginHelper"

    const-string v7, "searchPrintersViaPlugin - onPrintersChanged"

    const-string v8, ""

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$000(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/print/PrinterDiscoverySession;

    move-result-object v6

    invoke-virtual {v6}, Landroid/print/PrinterDiscoverySession;->getPrinters()Ljava/util/List;

    move-result-object v5

    .line 188
    .local v5, "printerList":Ljava/util/List;, "Ljava/util/List<Landroid/print/PrinterInfo;>;"
    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 189
    :cond_0
    const-string v6, "PrintPluginHelper"

    const-string v7, "searchPrintersViaPlugin - onPrintersChanged"

    const-string v8, "mPrinterList is null or empty"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :cond_1
    :goto_0
    return-void

    .line 194
    :cond_2
    const-string v6, "PrintPluginHelper"

    const-string v7, "searchPrintersViaPlugin - onPrintersChanged"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "printerList: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;
    invoke-static {v9}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " items"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/print/PrinterInfo;

    .line 197
    .local v4, "printer":Landroid/print/PrinterInfo;
    const-string v6, "PrintPluginHelper"

    const-string v7, "searchPrintersViaPlugin - onPrintersChanged"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Name: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " LocalID:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v9

    invoke-virtual {v9}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v7

    invoke-virtual {v7}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->getIPAddress(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$200(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 203
    .local v3, "ip":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->isP2pAddress(Ljava/lang/String;)Z
    invoke-static {v6, v3}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$400(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 204
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 205
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    new-instance v6, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v3}, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;)V

    .line 207
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.samsung.android.sconnect.central.PLUGIN_PRINTER_CONNECTED"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 209
    .local v2, "intent":Landroid/content/Intent;
    const-string v6, "DEVICE"

    invoke-virtual {v2, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 210
    const-string v6, "URIS"

    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mUris:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$600(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 211
    const-string v6, "PRINTER_INFO"

    invoke-virtual {v2, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 213
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 214
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    const/4 v7, 0x0

    # setter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z
    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$302(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Z)Z

    .line 215
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$700(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0xbb9

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 219
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 220
    const-string v6, "PrintPluginHelper"

    const-string v7, "searchPrintersViaPlugin - onPrintersChanged"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ADDED("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;
    invoke-static {v9}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$800(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 228
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$500(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 229
    .restart local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    new-instance v6, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;

    invoke-virtual {v4}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v3}, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;)V

    .line 230
    const-string v6, "PrintPluginHelper"

    const-string v7, "searchPrintersViaPlugin"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "PRINTER_FOUND"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$800(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v6

    invoke-interface {v6, v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_1

    .line 224
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_4
    const-string v6, "PrintPluginHelper"

    const-string v7, "searchPrintersViaPlugin"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DUPLICATED KEY!! - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 234
    :cond_5
    const-string v6, "PrintPluginHelper"

    const-string v7, "searchPrintersViaPlugin - onPrintersChanged"

    const-string v8, "mListener is not correct"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
