.class public Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
.super Ljava/lang/Object;
.source "HomeScreenChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$1;,
        Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;
    }
.end annotation


# static fields
.field public static final CHECK_TIME:I = 0xbb8

.field public static final FIRST_CHECK_TIME:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "HomeScreenChecker"

.field private static mHomeScreenChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;


# instance fields
.field public mCancelled:Z

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHomeScreenCheckerTask:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenCheckerTask:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mCancelled:Z

    .line 24
    const-string v0, "HomeScreenChecker"

    const-string v1, "HomeScreenChecker"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static checkHomeScreen(Landroid/content/Context;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 102
    invoke-static {p0}, Lcom/samsung/android/sconnect/common/util/Util;->getTopActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 103
    .local v7, "topActivity":Ljava/lang/String;
    const-string v9, "HomeScreenChecker"

    const-string v10, "checkHomeScreen"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "topActivity = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    if-eqz v7, :cond_0

    .line 106
    new-instance v9, Landroid/content/Intent;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v10, "android.intent.category.HOME"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 109
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 110
    .local v4, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, v2, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 111
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_0

    .line 112
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    .line 113
    .local v6, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 114
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 115
    .local v5, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v9, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v9, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 116
    .local v3, "launcherName":Ljava/lang/String;
    const-string v9, "HomeScreenChecker"

    const-string v10, "checkHomeScreen"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v12, v1, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 118
    const-string v8, "HomeScreenChecker"

    const-string v9, "checkHomeScreen"

    const-string v10, "this is home screen!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const/4 v8, 0x1

    .line 124
    .end local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v1    # "i":I
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "launcherName":Ljava/lang/String;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    .end local v6    # "size":I
    :cond_0
    return v8

    .line 113
    .restart local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v1    # "i":I
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "launcherName":Ljava/lang/String;
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    .restart local v5    # "ri":Landroid/content/pm/ResolveInfo;
    .restart local v6    # "size":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static checkHomeScreenByProcess(Landroid/content/Context;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 129
    invoke-static {p0}, Lcom/samsung/android/sconnect/common/util/Util;->getTopProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 130
    .local v7, "topProcess":Ljava/lang/String;
    const-string v9, "HomeScreenChecker"

    const-string v10, "checkHomeScreenByProcess"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "topProcess = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    if-eqz v7, :cond_0

    .line 133
    new-instance v9, Landroid/content/Intent;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v10, "android.intent.category.HOME"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 136
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 137
    .local v4, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, v2, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 138
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_0

    .line 139
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    .line 140
    .local v6, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 141
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 142
    .local v5, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v9, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 143
    .local v3, "launcherPkgName":Ljava/lang/String;
    const-string v9, "HomeScreenChecker"

    const-string v10, "checkHomeScreenByProcess"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v12, v1, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 145
    const-string v8, "HomeScreenChecker"

    const-string v9, "checkHomeScreenByProcess"

    const-string v10, "this is home screen process!"

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const/4 v8, 0x1

    .line 151
    .end local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v1    # "i":I
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "launcherPkgName":Ljava/lang/String;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "ri":Landroid/content/pm/ResolveInfo;
    .end local v6    # "size":I
    :cond_0
    return v8

    .line 140
    .restart local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v1    # "i":I
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "launcherPkgName":Ljava/lang/String;
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    .restart local v5    # "ri":Landroid/content/pm/ResolveInfo;
    .restart local v6    # "size":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    .line 31
    :cond_0
    sget-object v0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenChecker:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;

    return-object v0
.end method


# virtual methods
.method public cancelHomeScreenCheck()V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenCheckerTask:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "HomeScreenChecker"

    const-string v1, "cancelHomeScreenCheck"

    const-string v2, "Home screen check cancelled"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->setCancel(Z)V

    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenCheckerTask:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->interrupt()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenCheckerTask:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;

    .line 56
    :cond_0
    return-void
.end method

.method public initialize(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 35
    const-string v0, "HomeScreenChecker"

    const-string v1, "initialize"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHandler:Landroid/os/Handler;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenCheckerTask:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;

    .line 39
    return-void
.end method

.method public setCancel(Z)V
    .locals 0
    .param p1, "cancel"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mCancelled:Z

    .line 60
    return-void
.end method

.method public startHomeScreenCheck()V
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->cancelHomeScreenCheck()V

    .line 43
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->setCancel(Z)V

    .line 44
    new-instance v0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;-><init>(Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$1;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenCheckerTask:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;

    .line 45
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->mHomeScreenCheckerTask:Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker$HomeScreenCheckerTask;->start()V

    .line 46
    const-string v0, "HomeScreenChecker"

    const-string v1, "startHomeScreenCheck"

    const-string v2, "Home screen check started"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void
.end method
