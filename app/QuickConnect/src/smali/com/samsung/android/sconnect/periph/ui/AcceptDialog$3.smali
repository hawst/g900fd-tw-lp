.class Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$3;
.super Landroid/content/BroadcastReceiver;
.source "AcceptDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$3;->this$0:Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 450
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 451
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.samsung.android.sconnect.periph.REQUEST_CANCEL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 452
    const-string v2, "KEY"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 453
    .local v1, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$3;->this$0:Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mId:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->access$100(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 454
    const-string v2, "AcceptDialog"

    const-string v3, "mLocalReceiver"

    const-string v4, "ACTION_P_REQUEST_CANCEL"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$3;->this$0:Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->finish()V

    .line 461
    .end local v1    # "id":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 457
    .restart local v1    # "id":Ljava/lang/String;
    :cond_1
    const-string v2, "AcceptDialog"

    const-string v3, "mLocalReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_P_REQUEST_CANCEL ignore "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", current: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$3;->this$0:Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->mId:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->access$100(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
