.class Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;
.super Ljava/lang/Object;
.source "UpnpHelper.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/UpnpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 4
    .param p1, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;
    .param p2, "state"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 233
    const-string v0, "UpnpHelper"

    const-string v1, "mFileServiceListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreated : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    sget-object v0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$7;->$SwitchMap$com$samsung$android$allshare$ServiceConnector$ServiceState:[I

    invoke-virtual {p2}, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 251
    .end local p1    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 239
    .restart local p1    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    check-cast p1, Lcom/samsung/android/allshare/file/FileServiceProvider;

    .end local p1    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    # setter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v0, p1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$002(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/file/FileServiceProvider;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    .line 240
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$000(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileServiceProvider:Lcom/samsung/android/allshare/file/FileServiceProvider;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$000(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/file/FileServiceProvider;->getDeviceFinder()Lcom/samsung/android/allshare/file/FileDeviceFinder;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$102(Lcom/samsung/android/sconnect/common/net/UpnpHelper;Lcom/samsung/android/allshare/file/FileDeviceFinder;)Lcom/samsung/android/allshare/file/FileDeviceFinder;

    .line 243
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileDeviceFinder:Lcom/samsung/android/allshare/file/FileDeviceFinder;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$100(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/file/FileDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/UpnpHelper$1;->this$0:Lcom/samsung/android/sconnect/common/net/UpnpHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/UpnpHelper;->mFileReceiverFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/UpnpHelper;->access$200(Lcom/samsung/android/sconnect/common/net/UpnpHelper;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/file/FileDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    goto :goto_0

    .line 234
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 3
    .param p1, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 227
    const-string v0, "UpnpHelper"

    const-string v1, "mFileServiceListener"

    const-string v2, "onDeleted"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-void
.end method
