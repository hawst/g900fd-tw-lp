.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 836
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 839
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v2, "SconnectDisplay"

    const-string v3, "OnItemClickListener"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 883
    :goto_0
    return-void

    .line 842
    :pswitch_0
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 843
    .local v1, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    if-eqz v1, :cond_3

    .line 844
    const-string v2, "SconnectDisplay"

    const-string v3, "OnItemClickListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", action: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v2, v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1102(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 847
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 848
    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    .line 849
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 850
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v2

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v2

    and-int/lit8 v2, v2, 0x10

    if-lez v2, :cond_0

    .line 852
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/CentralManager;->checkConnectedDevice(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 854
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->unableToSendPopup(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1300(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 858
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1

    .line 859
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/CentralManager;->unableToMirroring(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 861
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V
    invoke-static {v3, v4, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 866
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/central/CentralManager;->needNewConnection(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 868
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->disconnectConfirmPopup(Ljava/lang/String;I)V
    invoke-static {v3, v4, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 875
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$7;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mActionList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v1, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)V

    goto/16 :goto_0

    .line 877
    :cond_3
    const-string v2, "SconnectDisplay"

    const-string v3, "OnItemClickListener"

    const-string v4, "item is null!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 840
    :pswitch_data_0
    .packed-switch 0x7f0c000c
        :pswitch_0
    .end packed-switch
.end method
