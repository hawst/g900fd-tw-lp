.class public Lcom/samsung/android/sconnect/common/net/BleHelper;
.super Ljava/lang/Object;
.source "BleHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;,
        Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;
    }
.end annotation


# static fields
.field private static final BLE_START_CNT_MAX:I = 0x3

.field private static final IDX_PACKET_INFO:I = 0x5

.field private static final IDX_TARGET_APP_ID:I = 0x6

.field private static final LCD_OFF:I = 0x0

.field private static final LCD_ON:I = 0x1

.field public static final LCD_STATE_NONE:I = -0x1

.field private static final LE_RADIO_STATE_OFF:I = 0xa

.field private static final LE_RADIO_STATE_ON:I = 0xc

.field private static final LE_RADIO_STATE_TURNING_OFF:I = 0xd

.field private static final LE_RADIO_STATE_TURNING_ON:I = 0xb

.field private static final MSG_CALL_START_LESCAN:I = 0x1

.field private static final MSG_CALL_STOP_LESCAN:I = 0x0

.field private static final MSG_ENABLE_BLE:I = 0x1

.field private static final MSG_START_LESCAN:I = 0x3

.field private static final MSG_STOP_BLE_SCAN:I = 0x2

.field private static final PACKET_INFO_APP_DATA:B = -0x10t

.field private static final TAG:Ljava/lang/String; = "BleHelper"

.field private static final TARGET_APP_ID_EMEETING:B = 0x3t

.field private static final TARGET_APP_ID_GROUP_PLAY:B = 0x2t

.field private static final TARGET_APP_ID_SIDE_SYNC:B = 0x1t

.field private static final TARGET_APP_ID_TOGETHER:B = 0x4t

.field private static final TIMEOUT_SCREENOFF:J

.field private static mSearchWearable:Z


# instance fields
.field private mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

.field private mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

.field private mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

.field private mBleStartCnt:I

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mContext:Landroid/content/Context;

.field private mGattServiceStateChangeCallback:Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsScanFilter_LCD_ON:Z

.field private mLastRespAdvTime:J

.field private mLeRadioState:I

.field private mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

.field private mRegisterIntent:Z

.field private mScanFilter_LCD_OFF:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/le/ScanFilter;",
            ">;"
        }
    .end annotation
.end field

.field private mScanFilter_LCD_ON:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/le/ScanFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mSearchWearable:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    .line 59
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 60
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    .line 62
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;

    .line 64
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mRegisterIntent:Z

    .line 66
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    .line 70
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLastRespAdvTime:J

    .line 82
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    .line 88
    iput v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I

    .line 90
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_ON:Ljava/util/List;

    .line 91
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_OFF:Ljava/util/List;

    .line 93
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mIsScanFilter_LCD_ON:Z

    .line 99
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandlerThread:Landroid/os/HandlerThread;

    .line 100
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    .line 384
    new-instance v0, Lcom/samsung/android/sconnect/common/net/BleHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/BleHelper$1;-><init>(Lcom/samsung/android/sconnect/common/net/BleHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    .line 568
    new-instance v0, Lcom/samsung/android/sconnect/common/net/BleHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/BleHelper$2;-><init>(Lcom/samsung/android/sconnect/common/net/BleHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

    .line 774
    new-instance v0, Lcom/samsung/android/sconnect/common/net/BleHelper$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/BleHelper$3;-><init>(Lcom/samsung/android/sconnect/common/net/BleHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 106
    const-string v0, "BleHelper"

    const-string v1, "BleHelper"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BleScanWorkHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandlerThread:Landroid/os/HandlerThread;

    .line 109
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 110
    new-instance v0, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;-><init>(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    .line 112
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 115
    new-instance v0, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;

    invoke-direct {v0, p0, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;-><init>(Lcom/samsung/android/sconnect/common/net/BleHelper;Lcom/samsung/android/sconnect/common/net/BleHelper$1;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;

    .line 116
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    .line 117
    invoke-virtual {p0, v4}, Lcom/samsung/android/sconnect/common/net/BleHelper;->setCentralRunningMode(Z)V

    .line 118
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/net/BleHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/sconnect/common/net/BleHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mRegisterIntent:Z

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/android/sconnect/common/net/BleHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/BluetoothLeScanner;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/bluetooth/le/BluetoothLeScanner;)Landroid/bluetooth/le/BluetoothLeScanner;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p1, "x1"    # Landroid/bluetooth/le/BluetoothLeScanner;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/os/PowerManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "x2"    # I
    .param p3, "x3"    # [B

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->parseBlePacket(Landroid/bluetooth/BluetoothDevice;I[B)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/sconnect/common/net/BleHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mIsScanFilter_LCD_ON:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/sconnect/common/net/BleHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mIsScanFilter_LCD_ON:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/sconnect/common/net/BleHelper;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_ON:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanSettings;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->getMyScanPreferences()Landroid/bluetooth/le/ScanSettings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/le/ScanCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/sconnect/common/net/BleHelper;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_OFF:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/common/net/BleHelper;)Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/sconnect/common/net/BleHelper;Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;)Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sconnect/common/net/BleHelper;Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/BluetoothAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/common/net/BleHelper;)Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/common/net/BleHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/sconnect/common/net/BleHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I

    return p1
.end method

.method static synthetic access$608(Lcom/samsung/android/sconnect/common/net/BleHelper;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/common/net/BleHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->setLocalScanFilters()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/common/net/BleHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BleHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getBTAddress(Ljava/lang/String;[B)Ljava/lang/String;
    .locals 11
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "scanRecord"    # [B

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 588
    array-length v7, p2

    if-lt v7, v8, :cond_0

    if-nez p1, :cond_2

    .line 589
    :cond_0
    const-string v7, "BleHelper"

    const-string v8, "getBLEAddressType"

    const-string v9, "address null or length < 1"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    .line 625
    .end local p1    # "address":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p1

    .line 593
    .restart local p1    # "address":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    .line 594
    .local v4, "offset":I
    :goto_1
    array-length v7, p2

    if-ge v4, v7, :cond_3

    .line 595
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "offset":I
    .local v5, "offset":I
    aget-byte v3, p2, v4

    .line 596
    .local v3, "len":I
    if-ge v3, v8, :cond_4

    .line 597
    const-string v7, "BleHelper"

    const-string v8, "getBLEAddressType"

    const-string v9, "len < 1"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .end local v3    # "len":I
    .end local v5    # "offset":I
    .restart local v4    # "offset":I
    :cond_3
    move-object p1, v0

    .line 625
    goto :goto_0

    .line 600
    .end local v4    # "offset":I
    .restart local v3    # "len":I
    .restart local v5    # "offset":I
    :cond_4
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "offset":I
    .restart local v4    # "offset":I
    aget-byte v6, p2, v5

    .line 601
    .local v6, "type":B
    packed-switch v6, :pswitch_data_0

    .line 621
    add-int/lit8 v7, v3, -0x1

    add-int/2addr v4, v7

    .line 624
    goto :goto_1

    .line 603
    :pswitch_0
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "offset":I
    .restart local v5    # "offset":I
    aget-byte v2, p2, v4

    .line 604
    .local v2, "flag":B
    const-string v7, "BleHelper"

    const-string v8, "getBLEAddressType"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "FLAG:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    shr-int/lit8 v7, v2, 0x2

    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_1

    .line 609
    const/4 v7, 0x0

    const/4 v8, 0x2

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    int-to-byte v1, v7

    .line 612
    .local v1, "checker":B
    and-int/lit16 v7, v1, 0xc0

    const/16 v8, 0xc0

    if-ne v7, v8, :cond_6

    .line 613
    invoke-static {p1}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getBtMacAddrFromBle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 614
    .local v0, "btmac":Ljava/lang/String;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .end local v0    # "btmac":Ljava/lang/String;
    :cond_5
    move-object p1, v0

    goto :goto_0

    .line 616
    :cond_6
    const-string v7, "BleHelper"

    const-string v8, "getBLEAddressType"

    const-string v9, "Dynamic Random address"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    .line 617
    goto :goto_0

    .line 601
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private getMyScanPreferences()Landroid/bluetooth/le/ScanSettings;
    .locals 3

    .prologue
    .line 219
    new-instance v1, Landroid/bluetooth/le/ScanSettings$Builder;

    invoke-direct {v1}, Landroid/bluetooth/le/ScanSettings$Builder;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/bluetooth/le/ScanSettings$Builder;->setCallbackType(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanMode(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanResultType(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/le/ScanSettings$Builder;->build()Landroid/bluetooth/le/ScanSettings;

    move-result-object v0

    .line 223
    .local v0, "myScanPreferences":Landroid/bluetooth/le/ScanSettings;
    return-object v0
.end method

.method private getSConnectDataFromBLE([B)[B
    .locals 14
    .param p1, "scanRecord"    # [B

    .prologue
    const/16 v13, 0x75

    const/4 v9, 0x0

    const/4 v12, 0x1

    .line 632
    array-length v10, p1

    if-ge v10, v12, :cond_1

    .line 633
    const-string v10, "BleHelper"

    const-string v11, "getSConnectDataFromBLE"

    const-string v12, "length < 1"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v9

    .line 715
    :cond_0
    :goto_0
    return-object v7

    .line 636
    :cond_1
    const/4 v3, 0x0

    .line 638
    .local v3, "offset":I
    :goto_1
    array-length v10, p1

    add-int/lit8 v10, v10, -0x2

    if-ge v3, v10, :cond_2

    .line 639
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "offset":I
    .local v4, "offset":I
    aget-byte v2, p1, v3

    .line 640
    .local v2, "len":I
    if-ge v2, v12, :cond_3

    move v3, v4

    .line 714
    .end local v2    # "len":I
    .end local v4    # "offset":I
    .restart local v3    # "offset":I
    :cond_2
    const-string v10, "BleHelper"

    const-string v11, "getSConnectDataFromBLE"

    const-string v12, "can not resolve length info"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v9

    .line 715
    goto :goto_0

    .line 644
    .end local v3    # "offset":I
    .restart local v2    # "len":I
    .restart local v4    # "offset":I
    :cond_3
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "offset":I
    .restart local v3    # "offset":I
    aget-byte v8, p1, v4

    .line 645
    .local v8, "type":B
    packed-switch v8, :pswitch_data_0

    .line 710
    add-int/lit8 v10, v2, -0x1

    add-int/2addr v3, v10

    goto :goto_1

    .line 648
    :pswitch_0
    :try_start_0
    aget-byte v10, p1, v3

    if-nez v10, :cond_4

    add-int/lit8 v10, v3, 0x1

    aget-byte v10, p1, v10

    if-eq v10, v13, :cond_5

    :cond_4
    aget-byte v10, p1, v3

    if-ne v10, v13, :cond_b

    add-int/lit8 v10, v3, 0x1

    aget-byte v10, p1, v10

    if-nez v10, :cond_b

    .line 654
    :cond_5
    add-int/lit8 v10, v3, 0x2

    aget-byte v10, p1, v10

    if-eq v10, v12, :cond_6

    .line 655
    const-string v10, "BleHelper"

    const-string v11, "getSConnectDataFromBLE"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unknown BLE version code : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    add-int/lit8 v13, v3, 0x2

    aget-byte v13, p1, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v9

    .line 657
    goto :goto_0

    .line 660
    :cond_6
    add-int/lit8 v10, v3, 0x3

    aget-byte v10, p1, v10

    if-nez v10, :cond_b

    .line 661
    add-int/lit8 v10, v3, 0x4

    aget-byte v10, p1, v10

    if-ne v10, v12, :cond_9

    .line 662
    const/16 v10, 0x37

    new-array v7, v10, [B

    .line 665
    .local v7, "ssData":[B
    const/4 v10, 0x0

    add-int/lit8 v11, v2, -0x1

    invoke-static {p1, v3, v7, v10, v11}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 666
    add-int v10, v3, v2

    add-int/lit8 v10, v10, -0x1

    aget-byte v6, p1, v10

    .line 667
    .local v6, "resLen":I
    if-le v6, v12, :cond_7

    .line 668
    add-int v10, v3, v2

    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v11, v2, -0x1

    add-int/lit8 v12, v6, -0x1

    invoke-static {p1, v10, v7, v11, v12}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 671
    :cond_7
    add-int/lit8 v10, v2, -0x1

    const/16 v11, 0x1a

    if-ne v10, v11, :cond_8

    add-int/lit8 v10, v6, -0x1

    const/16 v11, 0x1d

    if-eq v10, v11, :cond_0

    .line 673
    :cond_8
    const-string v10, "BleHelper"

    const-string v11, "getSConnectDataFromBLE"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "invalid data len : adv-"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " // scan-"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 703
    .end local v6    # "resLen":I
    .end local v7    # "ssData":[B
    :catch_0
    move-exception v0

    .line 704
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v10, "BleHelper"

    const-string v11, "getSConnectDataFromBLE"

    const-string v12, "IndexOutOfBoundsException: data is wrong"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v9

    .line 706
    goto/16 :goto_0

    .line 678
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_9
    add-int/lit8 v10, v3, 0x4

    :try_start_1
    aget-byte v10, p1, v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_b

    sget-boolean v10, Lcom/samsung/android/sconnect/common/net/BleHelper;->mSearchWearable:Z

    if-eqz v10, :cond_b

    .line 683
    add-int/lit8 v10, v3, 0x5

    aget-byte v10, p1, v10

    if-nez v10, :cond_b

    .line 684
    add-int/lit8 v10, v3, 0x6

    aget-byte v10, p1, v10

    if-nez v10, :cond_a

    .line 685
    const-string v10, "BleHelper"

    const-string v11, "getSConnectDataFromBLE"

    const-string v12, "Unknown service/deviceID"

    invoke-static {v10, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v9

    .line 687
    goto/16 :goto_0

    .line 689
    :cond_a
    add-int/lit8 v10, v3, 0x7

    aget-byte v10, p1, v10

    if-ne v10, v12, :cond_b

    .line 690
    add-int/lit8 v10, v3, 0x8

    aget-byte v5, p1, v10

    .line 691
    .local v5, "packageLength":B
    add-int/lit8 v10, v5, 0xc

    new-array v1, v10, [B

    .line 693
    .local v1, "gearData":[B
    add-int/lit8 v10, v3, -0x2

    const/4 v11, 0x0

    add-int/lit8 v12, v5, 0xc

    invoke-static {p1, v10, v1, v11, v12}, Ljava/lang/System;->arraycopy([BI[BII)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v7, v1

    .line 695
    goto/16 :goto_0

    .line 702
    .end local v1    # "gearData":[B
    .end local v5    # "packageLength":B
    :cond_b
    add-int/lit8 v10, v2, -0x1

    add-int/2addr v3, v10

    goto/16 :goto_1

    .line 645
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method private parseBlePacket(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 9
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "rssi"    # I
    .param p3, "scanRecord"    # [B

    .prologue
    const/16 v1, 0x75

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x6

    .line 499
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    .line 501
    .local v6, "bdname":Ljava/lang/String;
    :goto_0
    array-length v0, p3

    if-ge v0, v3, :cond_2

    .line 502
    const-string v0, "BleHelper"

    const-string v1, "parseBlePacket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignore insufficient scanRecord < 6 from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_0
    :goto_1
    return-void

    .line 499
    .end local v6    # "bdname":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 506
    .restart local v6    # "bdname":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    .line 507
    .local v4, "sConnectData":[B
    invoke-direct {p0, p3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->getSConnectDataFromBLE([B)[B

    move-result-object v4

    .line 508
    if-eqz v4, :cond_0

    .line 514
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->getBTAddress(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v5

    .line 516
    .local v5, "btMac":Ljava/lang/String;
    aget-byte v0, v4, v7

    if-nez v0, :cond_3

    aget-byte v0, v4, v8

    if-eq v0, v1, :cond_4

    :cond_3
    aget-byte v0, v4, v7

    if-ne v0, v1, :cond_5

    aget-byte v0, v4, v8

    if-nez v0, :cond_5

    .line 518
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    if-eqz v0, :cond_5

    .line 519
    const-string v0, "BleHelper"

    const-string v1, "parseBlePacket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Add gear : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", rssi: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", sConnectData : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    invoke-interface {v0, p1, v4, p2, v5}, Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;->onWearableDeviceAdded(Landroid/bluetooth/BluetoothDevice;[BILjava/lang/String;)V

    goto :goto_1

    .line 527
    :cond_5
    array-length v0, v4

    if-ge v0, v3, :cond_6

    .line 528
    const-string v0, "BleHelper"

    const-string v1, "parseBlePacket"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignore insufficient sConnectData < 6 from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 532
    :cond_6
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 533
    const-string v0, "BleHelper"

    const-string v1, "parseBlePacket"

    const-string v2, "should find PacketInfo of Samsung Application"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    const/4 v0, 0x5

    aget-byte v0, v4, v0

    const/16 v1, -0x10

    if-ne v0, v1, :cond_b

    .line 535
    const-string v0, "BleHelper"

    const-string v1, "parseBlePacket"

    const-string v2, "this is reserved PacketInfo for Samsung Application"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    aget-byte v0, v4, v3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 537
    const-string v0, "com.sec.android.sidesync30"

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->proxyToAppsCallback(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;I[B)V

    goto/16 :goto_1

    .line 539
    :cond_7
    aget-byte v0, v4, v3

    if-ne v0, v7, :cond_8

    .line 540
    const-string v0, "com.samsung.groupcast"

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->proxyToAppsCallback(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;I[B)V

    goto/16 :goto_1

    .line 542
    :cond_8
    aget-byte v0, v4, v3

    if-ne v0, v8, :cond_a

    .line 544
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    const-string v1, "com.sec.android.emeeting"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 546
    const-string v0, "com.sec.android.emeeting"

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->proxyToAppsCallback(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;I[B)V

    goto/16 :goto_1

    .line 548
    :cond_9
    sget-object v0, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    const-string v1, "com.sec.android.emeeting.b2c.hancom"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 550
    const-string v0, "com.sec.android.emeeting.b2c.hancom"

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->proxyToAppsCallback(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;I[B)V

    goto/16 :goto_1

    .line 553
    :cond_a
    aget-byte v0, v4, v3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 554
    const-string v0, "com.sec.android.app.alltogether"

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->proxyToAppsCallback(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;I[B)V

    goto/16 :goto_1

    .line 561
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;->onReceived(ILjava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V

    goto/16 :goto_1
.end method

.method private proxyToAppsCallback(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "rssi"    # I
    .param p4, "scanRecord"    # [B

    .prologue
    .line 724
    sget-object v1, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 725
    sget-object v1, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 726
    const-string v1, "BleHelper"

    const-string v2, "proxyToAppsCallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    :try_start_0
    sget-object v1, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/extern/IBleProxyCallback;

    invoke-interface {v1, p2, p3, p4}, Lcom/samsung/android/sconnect/extern/IBleProxyCallback;->onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 743
    :goto_0
    return-void

    .line 730
    :catch_0
    move-exception v0

    .line 731
    .local v0, "e":Landroid/os/DeadObjectException;
    const-string v1, "BleHelper"

    const-string v2, "proxyToAppsCallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeadObjectException. remove "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 733
    invoke-static {p1}, Lcom/samsung/android/sconnect/extern/BleProxyService;->removeBleProxyCallback(Ljava/lang/String;)V

    goto :goto_0

    .line 734
    .end local v0    # "e":Landroid/os/DeadObjectException;
    :catch_1
    move-exception v0

    .line 735
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BleHelper"

    const-string v2, "proxyToAppsCallback"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 738
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v1, "BleHelper"

    const-string v2, "proxyToAppsCallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no registered callback of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 741
    :cond_1
    const-string v1, "BleHelper"

    const-string v2, "proxyToAppsCallback"

    const-string v3, "mBleProxyCallbackHash size  < 1"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setLocalScanFilters()V
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x3

    .line 180
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_ON:Ljava/util/List;

    .line 181
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_OFF:Ljava/util/List;

    .line 183
    const/16 v4, 0x7500

    .line 184
    .local v4, "manufacturerId":I
    new-array v3, v8, [B

    fill-array-data v3, :array_0

    .line 187
    .local v3, "manudata_QC":[B
    new-array v1, v8, [B

    fill-array-data v1, :array_1

    .line 190
    .local v1, "manudataMask_QC":[B
    new-array v2, v9, [B

    fill-array-data v2, :array_2

    .line 193
    .local v2, "manudata_GEAR":[B
    new-array v0, v9, [B

    fill-array-data v0, :array_3

    .line 197
    .local v0, "manudataMask_GEAR":[B
    new-instance v7, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v7}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    invoke-virtual {v7, v4, v3, v1}, Landroid/bluetooth/le/ScanFilter$Builder;->setManufacturerData(I[B[B)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v6

    .line 199
    .local v6, "sFilter_QC":Landroid/bluetooth/le/ScanFilter;
    new-instance v7, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v7}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    invoke-virtual {v7, v4, v2, v0}, Landroid/bluetooth/le/ScanFilter$Builder;->setManufacturerData(I[B[B)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v5

    .line 202
    .local v5, "sFilter_GEAR":Landroid/bluetooth/le/ScanFilter;
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_OFF:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_ON:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_ON:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    const/16 v4, 0x75

    .line 208
    new-instance v7, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v7}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    invoke-virtual {v7, v4, v3, v1}, Landroid/bluetooth/le/ScanFilter$Builder;->setManufacturerData(I[B[B)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v6

    .line 210
    new-instance v7, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v7}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    invoke-virtual {v7, v4, v2, v0}, Landroid/bluetooth/le/ScanFilter$Builder;->setManufacturerData(I[B[B)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v5

    .line 213
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_OFF:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_ON:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mScanFilter_LCD_ON:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    return-void

    .line 184
    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 187
    :array_1
    .array-data 1
        0x0t
        -0x1t
        -0x1t
    .end array-data

    .line 190
    :array_2
    .array-data 1
        0x1t
        0x0t
        0x2t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 193
    nop

    :array_3
    .array-data 1
        0x0t
        -0x1t
        -0x1t
        0x0t
        0x0t
        -0x1t
    .end array-data
.end method

.method private shouldScanBleBg(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 746
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/common/util/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 747
    const-string v2, "BleHelper"

    const-string v3, "shouldScanBleBg"

    const-string v4, "KEY_ALLOW_TO_CONNECT == true"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    :goto_0
    return v1

    .line 750
    :cond_0
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->isDialogShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 752
    const-string v2, "BleHelper"

    const-string v3, "shouldScanBleBg"

    const-string v4, "isDialogShowing == true"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 755
    :cond_1
    sget-object v2, Lcom/samsung/android/sconnect/extern/BleProxyService;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->size()I

    move-result v0

    .line 756
    .local v0, "callbackCount":I
    if-lez v0, :cond_2

    .line 757
    const-string v2, "BleHelper"

    const-string v3, "shouldScanBleBg"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "callbackCount = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 760
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private shouldScanBleFg()Z
    .locals 3

    .prologue
    .line 764
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 765
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isCentralRunning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->isDialogShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 767
    :cond_0
    const-string v0, "BleHelper"

    const-string v1, "shouldScanBleFg"

    const-string v2, "true...isCentralRunning || isDialogShowing"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const/4 v0, 0x1

    .line 771
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public applyBleScanPolicy()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopScan(Z)V

    .line 176
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    .line 177
    return-void
.end method

.method public isBleOn()Z
    .locals 2

    .prologue
    .line 168
    iget v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 171
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setCentralRunningMode(Z)V
    .locals 3
    .param p1, "centralRunning"    # Z

    .prologue
    .line 158
    if-nez p1, :cond_0

    .line 159
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mSearchWearable:Z

    .line 160
    const-string v0, "BleHelper"

    const-string v1, "mSearchWearable"

    const-string v2, "true"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method public setLastRespAdvTime(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 337
    iput-wide p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLastRespAdvTime:J

    .line 338
    return-void
.end method

.method public setPreDiscoveryPacketListener(Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    .prologue
    .line 719
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPreDiscoveryPacketListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;

    .line 720
    return-void
.end method

.method public startAdv(Z[B[B)V
    .locals 22
    .param p1, "isForegroundAdv"    # Z
    .param p2, "advData"    # [B
    .param p3, "respData"    # [B

    .prologue
    .line 443
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    move/from16 v18, v0

    const/16 v19, 0xc

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 444
    const/4 v8, 0x0

    .line 446
    .local v8, "compensationLen":I
    add-int/lit8 v11, v8, 0xb

    .line 448
    .local v11, "nameLen":I
    new-array v10, v11, [B

    .line 450
    .local v10, "name":[B
    const/16 v18, 0x2

    const/16 v19, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v10, v2, v11}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 452
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v10}, Ljava/lang/String;-><init>([B)V

    .line 454
    .local v6, "bdname":Ljava/lang/String;
    const/16 v18, 0x5

    aget-byte v13, p2, v18

    .line 456
    .local v13, "packetInfo":B
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v15, v0, [B

    .line 457
    .local v15, "target":[B
    const/16 v18, 0x12

    const/16 v19, 0x0

    const/16 v20, 0x6

    move-object/from16 v0, p2

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v15, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 459
    invoke-static {v15}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v14

    .line 461
    .local v14, "respTarget":Ljava/lang/String;
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v9, v0, [B

    .line 462
    .local v9, "mac":[B
    const/16 v18, 0xc

    const/16 v19, 0x0

    const/16 v20, 0x6

    move-object/from16 v0, p2

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v9, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 464
    invoke-static {v9}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v12

    .line 466
    .local v12, "p2pMac":Ljava/lang/String;
    const/16 v18, 0x10

    move/from16 v0, v18

    new-array v7, v0, [B

    .line 467
    .local v7, "byte_ackTarget":[B
    const/16 v18, 0xd

    const/16 v19, 0x0

    const/16 v20, 0x10

    move-object/from16 v0, p3

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 469
    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v5

    .line 471
    .local v5, "ackTarget":Ljava/lang/String;
    const-string v18, "BleHelper"

    const-string v19, "startAdv"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "PACKET INFO - name: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/PacketInfo :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/RESP Target :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/P2P :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/ACK Target :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const/16 v18, 0x18

    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v16, v0

    .line 477
    .local v16, "trimAdvData":[B
    const/16 v18, 0x1b

    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v17, v0

    .line 479
    .local v17, "trimRespData":[B
    const/16 v18, 0x2

    const/16 v19, 0x0

    const/16 v20, 0x18

    move-object/from16 v0, p2

    move/from16 v1, v18

    move-object/from16 v2, v16

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 480
    const/16 v18, 0x2

    const/16 v19, 0x0

    const/16 v20, 0x1b

    move-object/from16 v0, p3

    move/from16 v1, v18

    move-object/from16 v2, v17

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 482
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    move-object/from16 v18, v0

    const/16 v19, 0x75

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->startBroadcast(I[B[B)V

    .line 487
    .end local v5    # "ackTarget":Ljava/lang/String;
    .end local v6    # "bdname":Ljava/lang/String;
    .end local v7    # "byte_ackTarget":[B
    .end local v8    # "compensationLen":I
    .end local v9    # "mac":[B
    .end local v10    # "name":[B
    .end local v11    # "nameLen":I
    .end local v12    # "p2pMac":Ljava/lang/String;
    .end local v13    # "packetInfo":B
    .end local v14    # "respTarget":Ljava/lang/String;
    .end local v15    # "target":[B
    .end local v16    # "trimAdvData":[B
    .end local v17    # "trimRespData":[B
    :goto_0
    return-void

    .line 485
    :cond_0
    const-string v18, "BleHelper"

    const-string v19, "startAdv"

    const-string v20, "mBleAdvertiser is null or LeRadioState not turned on"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized startScan(I)V
    .locals 11
    .param p1, "lcdState"    # I

    .prologue
    const/4 v10, 0x1

    .line 227
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v3

    .line 229
    .local v3, "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    const/4 v6, -0x1

    if-ne p1, v6, :cond_1

    .line 230
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v6}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    .line 236
    .local v1, "isLcdOn":Z
    :goto_0
    if-eqz v3, :cond_6

    .line 237
    invoke-virtual {v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->isGattServiceReady()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 238
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    if-nez v6, :cond_3

    .line 239
    const-string v6, "BleHelper"

    const-string v7, "startScan"

    const-string v8, "isGattServiceReady BUT mBleAdvertiser is NULL"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 231
    .end local v1    # "isLcdOn":Z
    :cond_1
    if-ne p1, v10, :cond_2

    .line 232
    const/4 v1, 0x1

    .restart local v1    # "isLcdOn":Z
    goto :goto_0

    .line 234
    .end local v1    # "isLcdOn":Z
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "isLcdOn":Z
    goto :goto_0

    .line 242
    :cond_3
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->isEnableBluetoothLeAdvertiser()Z

    move-result v6

    if-nez v6, :cond_4

    .line 243
    const-string v6, "BleHelper"

    const-string v7, "startScan"

    const-string v8, "isGattServiceReady BUT isEnableBluetoothLeAdvertiser FALSE"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 227
    .end local v1    # "isLcdOn":Z
    .end local v3    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 248
    .restart local v1    # "isLcdOn":Z
    .restart local v3    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_4
    :try_start_2
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v6

    if-nez v6, :cond_7

    if-nez v1, :cond_7

    .line 249
    const-string v6, "BleHelper"

    const-string v7, "startScan"

    const-string v8, "isGattServiceReady BUT isLcdOn FALSE"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 253
    :cond_5
    const-string v6, "BleHelper"

    const-string v7, "startScan"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isGattServiceReady FALSE. try [mLeRadioState]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " >> LE_RADIO_STATE_TURNING_ON calling addLeRadioReference()"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const/16 v6, 0xb

    iput v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    .line 257
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;

    invoke-virtual {v3, v6}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->addLeRadioReference(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;)Z

    goto :goto_1

    .line 261
    :cond_6
    const-string v6, "BleHelper"

    const-string v7, "startScan"

    const-string v8, "radioManager is null"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 264
    :cond_7
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleStartCnt:I

    .line 266
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 268
    const-string v6, "sconnect.scan.disable"

    const-string v7, "0"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "propDisableScan":Ljava/lang/String;
    sget v0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mState:I

    .line 270
    .local v0, "bluetoothActionState":I
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0, v6}, Lcom/samsung/android/sconnect/common/net/BleHelper;->shouldScanBleBg(Landroid/content/Context;)Z

    move-result v4

    .line 271
    .local v4, "shouldScanBleBg":Z
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->shouldScanBleFg()Z

    move-result v5

    .line 273
    .local v5, "shouldScanBleFg":Z
    const-string v6, "BleHelper"

    const-string v7, "startScan"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\"0\".equals(["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]) && ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] != BluetoothActionHelper.STATE_BT_WORKING then (["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]||["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "])"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v6, "0"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-eq v0, v10, :cond_0

    .line 278
    if-nez v4, :cond_8

    if-eqz v5, :cond_a

    .line 279
    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v6, :cond_9

    iget v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    const/16 v7, 0xc

    if-ne v6, v7, :cond_9

    .line 280
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->removeMessages(I)V

    .line 281
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->removeMessages(I)V

    .line 282
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    const/4 v8, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 285
    :cond_9
    const-string v6, "BleHelper"

    const-string v7, "startScan"

    const-string v8, "mBtAdapter is null or LE is not ON state"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 288
    :cond_a
    const-string v6, "BleHelper"

    const-string v7, "startScan"

    const-string v8, "not startLeScan() but lowerPeriphService()"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/sconnect/periph/PeriphControlUtil;->lowerPeriphService(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method public stopAdv()V
    .locals 3

    .prologue
    .line 490
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 491
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->stopBroadcast()V

    .line 495
    :goto_0
    return-void

    .line 493
    :cond_0
    const-string v0, "BleHelper"

    const-string v1, "stopAdv"

    const-string v2, "mBleAdvertiser is null or LeRadioState not turned on"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopLeScan()V
    .locals 4

    .prologue
    .line 366
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    .line 367
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    if-eqz v1, :cond_0

    .line 368
    const-string v1, "BleHelper"

    const-string v2, "stopLeScan"

    const-string v3, "call BleScanner.stopScan()"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v1, v2}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    :goto_0
    const-string v1, "BleHelper"

    const-string v2, "stopLeScan"

    const-string v3, "call BleScanner.stopScan() complete"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :goto_1
    return-void

    .line 371
    :catch_0
    move-exception v0

    .line 372
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "BleHelper"

    const-string v2, "stopLeScan"

    const-string v3, "NullPointerException catched BleScanner.stopScan"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 376
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_0
    const-string v1, "BleHelper"

    const-string v2, "stopLeScan"

    const-string v3, "mBleScanner is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 379
    :cond_1
    const-string v1, "BleHelper"

    const-string v2, "stopLeScan"

    const-string v3, "mBtAdapter is null or LE is not ON state"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public declared-synchronized stopScan(Z)V
    .locals 10
    .param p1, "force"    # Z

    .prologue
    const-wide/16 v8, 0x0

    .line 341
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLastRespAdvTime:J

    sub-long v0, v4, v6

    .line 343
    .local v0, "deltaTime":J
    cmp-long v3, v0, v8

    if-ltz v3, :cond_3

    .line 344
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->shouldScanBleBg(Landroid/content/Context;)Z

    move-result v2

    .line 346
    .local v2, "shouldScanBleBg":Z
    const-string v3, "BleHelper"

    const-string v4, "stopScan"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] || ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]==false"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    if-nez p1, :cond_0

    if-nez v2, :cond_1

    .line 348
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    const/16 v4, 0xc

    if-ne v3, v4, :cond_2

    .line 349
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->removeMessages(I)V

    .line 350
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->removeMessages(I)V

    .line 351
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    .end local v2    # "shouldScanBleBg":Z
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 354
    .restart local v2    # "shouldScanBleBg":Z
    :cond_2
    :try_start_1
    const-string v3, "BleHelper"

    const-string v4, "stopScan"

    const-string v5, "mBtAdapter is null or LE is not ON state"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 341
    .end local v0    # "deltaTime":J
    .end local v2    # "shouldScanBleBg":Z
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 358
    .restart local v0    # "deltaTime":J
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 359
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 360
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    sub-long v6, v8, v0

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public terminate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 121
    const-string v1, "BleHelper"

    const-string v2, "terminate"

    const-string v3, "Destructor"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mRegisterIntent:Z

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 125
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mRegisterIntent:Z

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 130
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 131
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    if-eqz v1, :cond_2

    .line 135
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->removeMessages(I)V

    .line 136
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleScanWorkHandler:Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sconnect/common/net/BleHelper$BleScanWorkHandler;->removeMessages(I)V

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 140
    iput-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mHandlerThread:Landroid/os/HandlerThread;

    .line 142
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopLeScan()V

    .line 144
    iget v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_4

    .line 145
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v0

    .line 146
    .local v0, "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    if-eqz v0, :cond_4

    .line 147
    const/16 v1, 0xd

    iput v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mLeRadioState:I

    .line 148
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    if-eqz v1, :cond_3

    .line 149
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->terminate()V

    .line 150
    iput-object v6, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mBleAdvertiser:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    .line 152
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/sconnect/common/net/BleHelper$GattServiceStateChangeCallback;

    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->releaseLeRadioReference(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;)Z

    .line 155
    .end local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_4
    return-void
.end method
