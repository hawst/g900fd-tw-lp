.class public Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
.super Ljava/lang/Object;
.source "DeviceP2p.java"


# static fields
.field static final TAG:Ljava/lang/String; = "DeviceP2p"

.field public static final p2pDeviceTypes:[I


# instance fields
.field public mDeviceType:I

.field public mIsMyGroup:Z

.field public mMirror:Z

.field public mP2pMac:Ljava/lang/String;

.field public mP2pName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->p2pDeviceTypes:[I

    return-void

    :array_0
    .array-data 4
        0x4
        0xf
        0x9
        0x7
        0x1a
        0x1a
        0x1a
        0x1a
        0x1a
        0x1
        0x10
        0xb
        0x1a
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 3
    .param p1, "displayname"    # Ljava/lang/String;
    .param p2, "p2pMac"    # Ljava/lang/String;
    .param p3, "p2pDeviceType"    # Ljava/lang/String;
    .param p4, "mirrorOn"    # Z
    .param p5, "isMyGroup"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pName:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    .line 31
    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    .line 32
    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    .line 33
    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    .line 37
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pName:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    .line 39
    iput-boolean p4, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    .line 40
    invoke-virtual {p0, p3, p1}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->convertP2pDeviceType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    .line 41
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_0

    .line 42
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    .line 44
    :cond_0
    iput-boolean p5, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    .line 47
    return-void
.end method


# virtual methods
.method public final convertP2pDeviceType(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p1, "p2pDeviceType"    # Ljava/lang/String;
    .param p2, "displayname"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x6

    const/4 v8, 0x1

    const/4 v4, 0x7

    .line 50
    const/16 v3, 0x1a

    .line 52
    .local v3, "type":I
    if-eqz p1, :cond_5

    .line 53
    const/4 v2, 0x0

    .line 54
    .local v2, "tokens":[Ljava/lang/String;
    const-string v6, "-"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 55
    array-length v6, v2

    if-lez v6, :cond_5

    .line 56
    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 57
    .local v1, "p2pType":I
    if-lt v1, v8, :cond_5

    const/16 v6, 0xd

    if-ge v1, v6, :cond_5

    .line 58
    sget-object v6, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->p2pDeviceTypes:[I

    add-int/lit8 v7, v1, -0x1

    aget v3, v6, v7

    .line 59
    iget-boolean v6, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    if-eqz v6, :cond_1

    const/4 v6, 0x2

    if-eq v1, v6, :cond_0

    const/4 v6, 0x5

    if-eq v1, v6, :cond_0

    if-eq v1, v5, :cond_0

    if-eq v1, v4, :cond_0

    const/16 v6, 0x8

    if-eq v1, v6, :cond_0

    const/16 v6, 0x9

    if-eq v1, v6, :cond_0

    const/16 v6, 0xb

    if-ne v1, v6, :cond_1

    .line 62
    :cond_0
    const/16 v3, 0xb

    .line 65
    :cond_1
    if-eqz p2, :cond_5

    .line 66
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p2, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "name":Ljava/lang/String;
    const/16 v6, 0xa

    if-ne v1, v6, :cond_2

    const-string v6, "[CAMERA]"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 81
    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "p2pType":I
    .end local v2    # "tokens":[Ljava/lang/String;
    :goto_0
    return v4

    .line 70
    .restart local v0    # "name":Ljava/lang/String;
    .restart local v1    # "p2pType":I
    .restart local v2    # "tokens":[Ljava/lang/String;
    :cond_2
    if-ne v1, v4, :cond_4

    const-string v4, "[TV]"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "BRAVIA"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    move v4, v5

    .line 72
    goto :goto_0

    .line 73
    :cond_4
    if-ne v1, v8, :cond_5

    .line 74
    const/16 v3, 0xb

    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "p2pType":I
    .end local v2    # "tokens":[Ljava/lang/String;
    :cond_5
    move v4, v3

    .line 81
    goto :goto_0
.end method

.method public equalWithGruopinfo(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 99
    if-nez p1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 102
    check-cast v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    .line 104
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    iget-boolean v3, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    if-ne v2, v3, :cond_0

    .line 105
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 86
    if-nez p1, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 89
    check-cast v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    .line 91
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 92
    const/4 v1, 0x1

    goto :goto_0
.end method
