.class public Lcom/samsung/android/sconnect/periph/PeriphService;
.super Landroid/app/Service;
.source "PeriphService.java"


# static fields
.field private static final MSG_SETSTATE_BACKGROUND:I = 0x1

.field private static final MSG_SETSTATE_FOREGROUND:I = 0x0

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_RUN:I = 0x1

.field public static mFoundDevice:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private TAG:Ljava/lang/String;

.field mAm:Landroid/app/IActivityManager;

.field private mContext:Landroid/content/Context;

.field final mForegroundToken:Landroid/os/IBinder;

.field private mHandler:Landroid/os/Handler;

.field mIsProcessForeground:Z

.field private mIsUserReceiver:Z

.field private mPserviceState:I

.field private mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

.field private mUserReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/periph/PeriphService;->mFoundDevice:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 39
    const-string v0, "PeriphService"

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    .line 45
    iput v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mPserviceState:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    .line 103
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mForegroundToken:Landroid/os/IBinder;

    .line 104
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mAm:Landroid/app/IActivityManager;

    .line 105
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsProcessForeground:Z

    .line 121
    new-instance v0, Lcom/samsung/android/sconnect/periph/PeriphService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/periph/PeriphService$1;-><init>(Lcom/samsung/android/sconnect/periph/PeriphService;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;

    .line 137
    iput-boolean v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsUserReceiver:Z

    .line 138
    new-instance v0, Lcom/samsung/android/sconnect/periph/PeriphService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/periph/PeriphService$2;-><init>(Lcom/samsung/android/sconnect/periph/PeriphService;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/periph/PeriphService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/PeriphService;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/periph/PeriphService;->setState(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/periph/PeriphService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/PeriphService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/periph/PeriphService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/periph/PeriphService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private idleService()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v1, "idleService"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->terminate()V

    .line 97
    :cond_0
    return-void
.end method

.method private registerUserReceiver()V
    .locals 6

    .prologue
    .line 160
    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v3, "registerUserReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIsUserReceiver == "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsUserReceiver:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsUserReceiver:Z

    if-nez v2, :cond_0

    .line 162
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 163
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 164
    const-string v2, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 165
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 167
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 168
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsUserReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 169
    .restart local v1    # "filter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v3, "registerUserReceiver"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private runService()V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v1, "runService"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iput-object p0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mContext:Landroid/content/Context;

    .line 87
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    .line 88
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mSConnectManager:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/SconnectManager;->createPreDiscoveryHelper()V

    .line 89
    return-void
.end method

.method private setProcessForeground(Z)V
    .locals 5
    .param p1, "isForeground"    # Z

    .prologue
    .line 108
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v2, "setProcessForeground"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsProcessForeground:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " >> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsProcessForeground:Z

    if-eq v1, p1, :cond_0

    .line 111
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mAm:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-interface {v1, v2, v3, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 112
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v2, "setProcessForeground"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[myPid]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " >> [isForeground]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsProcessForeground:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v2, "setProcessForeground"

    const-string v3, "RemoteException"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setState(I)V
    .locals 4
    .param p1, "newState"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v1, "setState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mPserviceState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " >> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mPserviceState:I

    if-ne v0, p1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iput p1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mPserviceState:I

    .line 75
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mPserviceState:I

    if-ne v0, v1, :cond_2

    .line 76
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/PeriphService;->runService()V

    goto :goto_0

    .line 77
    :cond_2
    iget v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mPserviceState:I

    if-nez v0, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/PeriphService;->idleService()V

    goto :goto_0
.end method

.method private unregisterUserReceiver()V
    .locals 5

    .prologue
    .line 176
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v2, "unregisterUserReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIsUserReceiver == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsUserReceiver:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsUserReceiver:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 179
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mUserReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 180
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mIsUserReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v2, "unregisterUserReceiver"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 201
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "[START]"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "[START] USER_OWNER"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/periph/PeriphService;->setState(I)V

    .line 61
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/PeriphService;->registerUserReceiver()V

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "[START] not USER_OWNER"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 189
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    const-string v2, "[END]"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 193
    iget-object v0, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 195
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/periph/PeriphService;->unregisterUserReceiver()V

    .line 196
    invoke-direct {p0, v3}, Lcom/samsung/android/sconnect/periph/PeriphService;->setState(I)V

    .line 197
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 206
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    if-nez v2, :cond_4

    .line 207
    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v3, "onStartCommand"

    const-string v4, "USER_OWNER"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    if-eqz p1, :cond_3

    .line 209
    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v3, "onStartCommand"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " [flags]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " [startId]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.samsung.android.sconnect.periph.START_SERVICE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    const-string v2, "PROCESS_FORCE_FG"

    invoke-virtual {p1, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    invoke-direct {p0, v1}, Lcom/samsung/android/sconnect/periph/PeriphService;->setProcessForeground(Z)V

    .line 229
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 217
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    const-string v2, "com.samsung.android.sconnect.periph.STOP_SERVICE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 218
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/periph/PeriphService;->stopSelf()V

    goto :goto_0

    .line 219
    :cond_2
    const-string v2, "com.samsung.android.sconnect.periph.LOWER_SERVICE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220
    invoke-direct {p0, v6}, Lcom/samsung/android/sconnect/periph/PeriphService;->setProcessForeground(Z)V

    .line 221
    const/4 v1, 0x2

    goto :goto_0

    .line 224
    .end local v0    # "action":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v3, "onStartCommand"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " [flags]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " [startId]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/PeriphService;->TAG:Ljava/lang/String;

    const-string v2, "onStartCommand"

    const-string v3, "not USER_OWNER"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const/4 v1, -0x1

    goto :goto_0
.end method
