.class public Lcom/samsung/android/sconnect/common/util/GUIUtil;
.super Ljava/lang/Object;
.source "GUIUtil.java"


# static fields
.field static final TAG:Ljava/lang/String; = "GUIUtil"

.field public static final deviceBgImages:[I

.field public static final deviceImages:[I

.field public static final deviceImagesLarge1:[I

.field public static mActivityPackageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile mInstance:Lcom/samsung/android/sconnect/common/util/GUIUtil;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLocalStrings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x1d

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mInstance:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 43
    new-instance v0, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/util/GUIUtil$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mActivityPackageList:Ljava/util/ArrayList;

    .line 61
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceBgImages:[I

    .line 93
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceImages:[I

    .line 125
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->deviceImagesLarge1:[I

    return-void

    .line 61
    :array_0
    .array-data 4
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
        0x7f02000e
    .end array-data

    .line 93
    :array_1
    .array-data 4
        0x7f02003f
        0x7f020045
        0x7f02003b
        0x7f020040
        0x7f020040
        0x7f020047
        0x7f020033
        0x7f020048
        0x7f020041
        0x7f020046
        0x7f020038
        0x7f020036
        0x7f020037
        0x7f02003d
        0x7f02002e
        0x7f02002f
        0x7f020031
        0x7f02003c
        0x7f020034
        0x7f020039
        0x7f020043
        0x7f02004a
        0x7f020030
        0x7f020042
        0x7f020032
        0x7f02003a
        0x7f02003a
        0x7f02003a
        0x7f02003a
    .end array-data

    .line 125
    :array_2
    .array-data 4
        0x7f020022
        0x7f020028
        0x7f02001e
        0x7f020023
        0x7f020023
        0x7f02002a
        0x7f020016
        0x7f02002b
        0x7f020024
        0x7f020029
        0x7f02001b
        0x7f020019
        0x7f02001a
        0x7f020020
        0x7f020011
        0x7f020012
        0x7f020014
        0x7f02001f
        0x7f020017
        0x7f02001c
        0x7f020026
        0x7f02002d
        0x7f020013
        0x7f020025
        0x7f020015
        0x7f02001d
        0x7f02001d
        0x7f02001d
        0x7f02001d
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    .line 158
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    .line 159
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->initialize()V

    .line 160
    return-void
.end method

.method public static cropIcon(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sizeDimen"    # I
    .param p2, "source"    # Landroid/graphics/Bitmap;

    .prologue
    .line 499
    const/4 v6, 0x0

    .line 500
    .local v6, "resultIcon":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 501
    .local v2, "mask":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 503
    .local v7, "scaledSource":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v8, v9

    .line 505
    .local v8, "size":I
    :try_start_0
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 506
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 507
    .local v3, "maskCanvas":Landroid/graphics/Canvas;
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 508
    .local v4, "maskPaint":Landroid/graphics/Paint;
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 509
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x7f060000

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 510
    div-int/lit8 v9, v8, 0x2

    int-to-float v9, v9

    div-int/lit8 v10, v8, 0x2

    int-to-float v10, v10

    div-int/lit8 v11, v8, 0x2

    int-to-float v11, v11

    invoke-virtual {v3, v9, v10, v11, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 512
    if-eqz v2, :cond_0

    if-eqz p2, :cond_0

    .line 513
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v10, 0x1

    invoke-virtual {p2, v9, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 514
    const/4 v9, 0x1

    invoke-static {v7, v8, v8, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 516
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 518
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 519
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v0, v7, v9, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 521
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 522
    .local v5, "paint":Landroid/graphics/Paint;
    new-instance v9, Landroid/graphics/PorterDuffXfermode;

    sget-object v10, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v9, v10}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 523
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v0, v2, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 524
    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v5    # "paint":Landroid/graphics/Paint;
    :cond_0
    if-eqz v2, :cond_1

    .line 530
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 532
    :cond_1
    if-eqz v7, :cond_2

    .line 533
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 536
    .end local v3    # "maskCanvas":Landroid/graphics/Canvas;
    .end local v4    # "maskPaint":Landroid/graphics/Paint;
    :cond_2
    :goto_0
    return-object v6

    .line 526
    :catch_0
    move-exception v1

    .line 527
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529
    if-eqz v2, :cond_3

    .line 530
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 532
    :cond_3
    if-eqz v7, :cond_2

    .line 533
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 529
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    if-eqz v2, :cond_4

    .line 530
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 532
    :cond_4
    if-eqz v7, :cond_5

    .line 533
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    throw v9
.end method

.method public static getContactImage(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 464
    const/4 v8, 0x0

    .line 465
    .local v8, "phones":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 467
    .local v6, "clsInputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 468
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 469
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 470
    .local v1, "uri":Landroid/net/Uri;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "photo_uri"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 473
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 474
    const-string v2, "photo_uri"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 477
    .local v9, "photo":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v6

    .line 478
    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 484
    if-eqz v8, :cond_0

    .line 485
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 487
    :cond_0
    if-eqz v6, :cond_1

    .line 489
    :try_start_1
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 495
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v9    # "photo":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-object v2

    .line 490
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v9    # "photo":Landroid/net/Uri;
    :catch_0
    move-exception v7

    .line 491
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 484
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v9    # "photo":Landroid/net/Uri;
    :cond_2
    if-eqz v8, :cond_3

    .line 485
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 487
    :cond_3
    if-eqz v6, :cond_4

    .line 489
    :try_start_2
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_4
    :goto_1
    move-object v2, v10

    .line 495
    goto :goto_0

    .line 490
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    :catch_1
    move-exception v7

    .line 491
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 481
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v7    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 482
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 484
    if-eqz v8, :cond_5

    .line 485
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 487
    :cond_5
    if-eqz v6, :cond_4

    .line 489
    :try_start_4
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    .line 490
    :catch_3
    move-exception v7

    .line 491
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 484
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_6

    .line 485
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 487
    :cond_6
    if-eqz v6, :cond_7

    .line 489
    :try_start_5
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 492
    :cond_7
    :goto_2
    throw v2

    .line 490
    :catch_4
    move-exception v7

    .line 491
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public static final getIconBackground(III)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "iconSize"    # I
    .param p1, "circleSize"    # I
    .param p2, "color"    # I

    .prologue
    .line 453
    mul-int/lit8 v3, p0, 0x2

    mul-int/lit8 v4, p0, 0x2

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 454
    .local v1, "colorIcon":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 455
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 456
    .local v2, "paint":Landroid/graphics/Paint;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 457
    invoke-virtual {v2, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 458
    int-to-float v3, p0

    int-to-float v4, p0

    int-to-float v5, p1

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 459
    return-object v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/GUIUtil;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 163
    if-eqz p0, :cond_1

    sget-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mInstance:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    if-nez v0, :cond_1

    .line 164
    const-class v1, Lcom/samsung/android/sconnect/common/util/GUIUtil;

    monitor-enter v1

    .line 165
    :try_start_0
    sget-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mInstance:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/util/GUIUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mInstance:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 167
    const-string v0, "GUIUtil"

    const-string v2, "getInstance"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "make new instance "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mInstance:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :cond_1
    const-string v0, "GUIUtil"

    const-string v1, "getInstance"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return existing instance "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mInstance:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    sget-object v0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mInstance:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    return-object v0

    .line 169
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private initialize()V
    .locals 4

    .prologue
    const v3, 0x7f0900ed

    .line 176
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    .line 177
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isCHNDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f090061

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f090060

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900ba

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900b9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900bc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900bb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900c3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900c2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900d7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900d6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900da

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900dd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900dc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900e2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900e4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900f2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900f4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900f3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f090100

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900ff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f090106

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f090105

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f09010d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f09010c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f090110

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f09010f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f090119

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f090118

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f09013b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f09013a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f090030

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f09002f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f09019d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f09019c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f09019f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f09019e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0901a4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0901a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    const v1, 0x7f0900ea

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900e9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTmobile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0900ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static reverseLeftRight(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    const/4 v1, 0x0

    .line 544
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 545
    .local v0, "originalImage":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 546
    const-string v1, "GUIUtil"

    const-string v2, "reverseLeftRight"

    const-string v3, "fail to decode originalImage resource to Bitmap"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const/4 v7, 0x0

    .line 554
    :goto_0
    return-object v7

    .line 549
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 550
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 551
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 553
    .local v7, "reverseImage":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method


# virtual methods
.method public getContentsString(II)Ljava/lang/String;
    .locals 7
    .param p1, "type"    # I
    .param p2, "size"    # I

    .prologue
    const v6, 0x7f09005a

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 318
    const-string v1, "%d"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "fSize":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 390
    :pswitch_0
    const-string v1, ""

    :goto_0
    return-object v1

    .line 321
    :pswitch_1
    if-le p2, v4, :cond_0

    .line 322
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090056

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 324
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09004a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 327
    :pswitch_2
    if-le p2, v4, :cond_1

    .line 328
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09005c

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 330
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090053

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 333
    :pswitch_3
    if-le p2, v4, :cond_2

    .line 334
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09005b

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 336
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09004f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 339
    :pswitch_4
    if-le p2, v4, :cond_3

    .line 340
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {v1, v6, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 342
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090050

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 345
    :pswitch_5
    if-le p2, v4, :cond_4

    .line 346
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090059

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 348
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09004d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 351
    :pswitch_6
    if-le p2, v4, :cond_5

    .line 352
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09005f

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 354
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090055

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 357
    :pswitch_7
    if-le p2, v4, :cond_6

    .line 358
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v6, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 360
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090054

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 363
    :pswitch_8
    if-le p2, v4, :cond_7

    .line 364
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090057

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 366
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09004b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 369
    :pswitch_9
    if-le p2, v4, :cond_8

    .line 370
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09005e

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 372
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090051

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 376
    :pswitch_a
    if-le p2, v4, :cond_9

    .line 377
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f090058

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 379
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09004c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 384
    :pswitch_b
    if-le p2, v4, :cond_a

    .line 385
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {v1, v6, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 387
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f09004e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 319
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_b
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public getDeviceString(I)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 395
    packed-switch p1, :pswitch_data_0

    .line 448
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090173

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 397
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090167

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 399
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090174

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 401
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f0900c9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 404
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090168

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 406
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090169

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 408
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09016e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 410
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090166

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 412
    :pswitch_8
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09016f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 414
    :pswitch_9
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090170

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 416
    :pswitch_a
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09016c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 418
    :pswitch_b
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09016d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 420
    :pswitch_c
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090171

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 422
    :pswitch_d
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090172

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 424
    :pswitch_e
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090165

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 426
    :pswitch_f
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09016a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 428
    :pswitch_10
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09016b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 430
    :pswitch_11
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090175

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 432
    :pswitch_12
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090176

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 434
    :pswitch_13
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090177

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 436
    :pswitch_14
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090178

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 438
    :pswitch_15
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090179

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 440
    :pswitch_16
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09017a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 442
    :pswitch_17
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09017b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 395
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public getLocalString(I)I
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 540
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mLocalStrings:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .end local p1    # "id":I
    :cond_0
    return p1
.end method

.method public getPrimaryActionName(I)Ljava/lang/String;
    .locals 4
    .param p1, "actionType"    # I

    .prologue
    const v3, 0x7f090026

    const v2, 0x7f090022

    const v1, 0x7f09001c

    .line 232
    packed-switch p1, :pswitch_data_0

    .line 268
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 234
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090021

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 236
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09001f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 238
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09001e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 240
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 242
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090027

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 244
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090025

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 246
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09002e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 248
    :pswitch_8
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 250
    :pswitch_9
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 252
    :pswitch_a
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09001d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 254
    :pswitch_b
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090029

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 256
    :pswitch_c
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09001a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 258
    :pswitch_d
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090018

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 260
    :pswitch_e
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 262
    :pswitch_f
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 264
    :pswitch_10
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 266
    :pswitch_11
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090030

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getLocalString(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_11
        :pswitch_10
        :pswitch_d
    .end packed-switch
.end method

.method public getPrimaryActionTTS(I)Ljava/lang/String;
    .locals 4
    .param p1, "actionType"    # I

    .prologue
    const v3, 0x7f090158

    const v2, 0x7f090157

    const v1, 0x7f090154

    .line 274
    packed-switch p1, :pswitch_data_0

    .line 311
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 276
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09014e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 278
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09014d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 280
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f09014b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 282
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090151

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 284
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090155

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 286
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090153

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 288
    :pswitch_7
    const-string v0, ""

    goto :goto_0

    .line 290
    :pswitch_8
    const-string v0, ""

    goto :goto_0

    .line 292
    :pswitch_9
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 294
    :pswitch_a
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 296
    :pswitch_b
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 299
    :pswitch_c
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090159

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 301
    :pswitch_d
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    const v1, 0x7f090150

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 303
    :pswitch_e
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 305
    :pswitch_f
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 307
    :pswitch_10
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 309
    :pswitch_11
    const-string v0, ""

    goto/16 :goto_0

    .line 274
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_11
        :pswitch_10
        :pswitch_c
    .end packed-switch
.end method
