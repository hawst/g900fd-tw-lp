.class public Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;
.super Landroid/preference/PreferenceActivity;
.source "QuickConnectPopupSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;
    }
.end annotation


# static fields
.field private static final IS_POPUP_DISABLE:Z = false

.field private static final IS_POPUP_ENABLE:Z = true

.field private static final KEY_POPUP:Ljava/lang/String; = "quick_connect_popup_settings"

.field private static final PREF_KEY_POPUP:Ljava/lang/String; = "SconnectDisplay"

.field private static final TAG:Ljava/lang/String; = "QuickConnectPopupSettings"

.field private static mPreference:Landroid/content/SharedPreferences;


# instance fields
.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 63
    new-instance v0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$1;-><init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 87
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->getPopupPref()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 25
    invoke-static {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->putPopupPref(Z)V

    return-void
.end method

.method private static getPopupPref()Z
    .locals 4

    .prologue
    .line 187
    sget-object v1, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "PERMISSION_POPUP_CHECK"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 188
    .local v0, "retValue":Z
    return v0
.end method

.method private static putPopupPref(Z)V
    .locals 2
    .param p0, "isPopup"    # Z

    .prologue
    .line 181
    sget-object v1, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 182
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "PERMISSION_POPUP_CHECK"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 183
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    return-void
.end method

.method private registerBroadcast()V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 55
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 57
    return-void
.end method

.method private unregisterBroadcast()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 61
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 37
    const-string v0, "QuickConnectPopupSettings"

    const-string v1, "onCreate"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;

    invoke-direct {v2}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings$QuickConnectPopupSettingsFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 41
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 42
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->registerBroadcast()V

    .line 43
    const-string v0, "SconnectDisplay"

    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->mPreference:Landroid/content/SharedPreferences;

    .line 44
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 48
    const-string v0, "QuickConnectPopupSettings"

    const-string v1, "onDestroy"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->unregisterBroadcast()V

    .line 51
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 193
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 201
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 195
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectPopupSettings;->finish()V

    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
