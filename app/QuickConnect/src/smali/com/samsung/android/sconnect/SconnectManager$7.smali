.class Lcom/samsung/android/sconnect/SconnectManager$7;
.super Ljava/lang/Object;
.source "SconnectManager.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/SconnListener$ICommandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/SconnectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/SconnectManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/SconnectManager;)V
    .locals 0

    .prologue
    .line 1896
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfirmed(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 10
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "p2pMac"    # Ljava/lang/String;
    .param p3, "respData"    # [B

    .prologue
    const/16 v9, 0x10

    const/4 v7, 0x1

    .line 1938
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;

    .line 1939
    .local v0, "cmd":Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1940
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1941
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x18

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1943
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1944
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopScanForPopup()V

    .line 1946
    :cond_1
    if-nez v0, :cond_2

    .line 1947
    const-string v5, "SconnectManager"

    const-string v6, "onConfirmed"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no matching request :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1977
    :goto_0
    return-void

    .line 1951
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.samsung.android.sconnect.central.REQUEST_RESULT"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1953
    .local v1, "intent":Landroid/content/Intent;
    iget v5, v0, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;->mAction:I

    if-ne v5, v9, :cond_3

    .line 1954
    const-string v5, "FINISH"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1956
    :cond_3
    const-string v5, "KEY"

    invoke-virtual {v1, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1957
    const-string v5, "ADDRESS"

    invoke-virtual {v1, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1958
    const-string v5, "RES"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1959
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1961
    const-string v5, "SconnectManager"

    const-string v6, "onConfirmed"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "request :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1963
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x15

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1964
    .local v2, "msg":Landroid/os/Message;
    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1965
    iget v5, v0, Lcom/samsung/android/sconnect/SconnectManager$RequestCommand;->mAction:I

    if-ne v5, v9, :cond_4

    .line 1966
    const/4 v5, 0x6

    new-array v5, v5, [B

    fill-array-data v5, :array_0

    invoke-static {v5, p3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1969
    const-string v5, "SconnectManager"

    const-string v6, "onConfirmed"

    const-string v7, "ACTION_SIDESYNC_CONTROL get BT_MAC from CONFIRM"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1970
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1971
    .local v4, "respBundle":Landroid/os/Bundle;
    invoke-static {p3}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseBtMacAddressToString([B)Ljava/lang/String;

    move-result-object v3

    .line 1972
    .local v3, "remoteBtMac":Ljava/lang/String;
    const-string v5, "REMOTE_BT_MAC"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1973
    invoke-virtual {v2, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1976
    .end local v3    # "remoteBtMac":Ljava/lang/String;
    .end local v4    # "respBundle":Landroid/os/Bundle;
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1966
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public onRejected(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "p2pMac"    # Ljava/lang/String;

    .prologue
    .line 1916
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1917
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1919
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x18

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1921
    :cond_0
    const-string v1, "SconnectManager"

    const-string v2, "onRejected"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1922
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.central.REQUEST_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1923
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "KEY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1924
    const-string v1, "ADDRESS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1925
    const-string v1, "RES"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1926
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1931
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1932
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopScanForPopup()V

    .line 1934
    :cond_1
    return-void

    .line 1928
    :cond_2
    const-string v1, "SconnectManager"

    const-string v2, "onRejected"

    const-string v3, "no matching request"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRequestReceived(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 1899
    const-string v1, "CMD"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1900
    .local v0, "cmd":I
    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    .line 1901
    const-string v1, "SconnectManager"

    const-string v2, "onRequestReceived"

    const-string v3, "ACTION_ASK_TO_EMEET"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1910
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1911
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # setter for: Lcom/samsung/android/sconnect/SconnectManager;->mIsAcceptDialogShowing:Z
    invoke-static {v1, v4}, Lcom/samsung/android/sconnect/SconnectManager;->access$502(Lcom/samsung/android/sconnect/SconnectManager;Z)Z

    .line 1912
    return-void

    .line 1902
    :cond_0
    const/16 v1, 0x50

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    const-string v2, "ADDRESS"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->needToDisconnectOnPeriph(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/SconnectManager$7;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v1

    const-string v2, "ADDRESS"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->needToDisconnect(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1905
    :cond_2
    const-string v1, "NEWCONNECTION"

    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1906
    const-string v1, "SconnectManager"

    const-string v2, "onRequestReceived"

    const-string v3, "with warning"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1908
    :cond_3
    const-string v1, "SconnectManager"

    const-string v2, "onRequestReceived"

    const-string v3, "without warning"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
