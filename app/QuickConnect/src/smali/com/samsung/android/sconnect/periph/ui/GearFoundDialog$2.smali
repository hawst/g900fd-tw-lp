.class Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;
.super Ljava/lang/Object;
.source "GearFoundDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->checkNearbyGear()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/16 v9, 0xc8

    .line 95
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$000(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.samsung.android.app.watchmanagerstub"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 98
    .local v2, "pinfo":Landroid/content/pm/PackageInfo;
    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 100
    .local v3, "versionCode":I
    const-string v5, "GearFoundDialog"

    const-string v6, "onClick"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mDeviceManager : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceManager:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$100(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", installer versionCode : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", string : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mData:[B
    invoke-static {v8}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$200(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)[B

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const/16 v5, 0x65

    if-lt v3, v5, :cond_1

    if-ge v3, v9, :cond_1

    .line 105
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.samsung.android.sconnect.action.CONNECT_WEARABLE"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "MAC"

    iget-object v6, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceAddress:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$300(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v5, "WM_MANAGER"

    iget-object v6, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceManager:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$100(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    iget-object v5, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$000(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 120
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pinfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "versionCode":I
    :cond_0
    :goto_0
    return-void

    .line 110
    .restart local v2    # "pinfo":Landroid/content/pm/PackageInfo;
    .restart local v3    # "versionCode":I
    :cond_1
    if-lt v3, v9, :cond_0

    .line 111
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.samsung.android.wmanger.action.CONNECT_WEARABLE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    .local v4, "wmanagerIntent":Landroid/content/Intent;
    const-string v5, "MAC"

    iget-object v6, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mDeviceAddress:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$300(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string v5, "DATA"

    iget-object v6, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mData:[B
    invoke-static {v6}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$200(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 115
    iget-object v5, p0, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog$2;->this$0:Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    # getter for: Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;->access$000(Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    .end local v2    # "pinfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "versionCode":I
    .end local v4    # "wmanagerIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "GearFoundDialog"

    const-string v6, "onClick"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "WearableInstaller is not preloaded! - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
