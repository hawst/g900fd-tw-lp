.class public Lcom/samsung/android/sconnect/common/net/ChromecastHelper;
.super Ljava/lang/Object;
.source "ChromecastHelper.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/IDiscoveryAction;


# static fields
.field private static final TAG:Ljava/lang/String; = "ChromecastHelper"


# instance fields
.field public IMediaDeviceDiscoveryListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

.field private IPlayerEventListener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

.field private mContext:Landroid/content/Context;

.field private mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mServiceConnectionListener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    .line 28
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 30
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mServiceConnectionListener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    .line 106
    new-instance v0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$1;-><init>(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->IPlayerEventListener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    .line 126
    new-instance v0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper$2;-><init>(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->IMediaDeviceDiscoveryListener:Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;

    .line 34
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;

    .line 35
    return-void
.end method

.method private ConnectCF()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->IPlayerEventListener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    invoke-static {v0, v1}, Lcom/sec/android/sdial/ServiceProvider;->startService(Landroid/content/Context;Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;)V

    .line 55
    return-void
.end method

.method private DisconnectCF()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 58
    const-string v0, "ChromecastHelper"

    const-string v1, "DisconnectCF"

    const-string v2, "Inside DisconnectCF Helper"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    invoke-virtual {v0, v3}, Lcom/sec/android/sdial/DeviceFinder;->setDeviceFinderEventListener(Lcom/sec/android/sdial/DeviceFinder$IDeviceFinderEventListener;)V

    .line 61
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 65
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    .line 68
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mServiceConnectionListener:Lcom/sec/android/sdial/ServiceProvider$IServiceConnectionEventlistener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/sec/android/sdial/DeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;Lcom/sec/android/sdial/DeviceFinder;)Lcom/sec/android/sdial/DeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/ChromecastHelper;
    .param p1, "x1"    # Lcom/sec/android/sdial/DeviceFinder;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/common/net/ChromecastHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/ChromecastHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public checkEnabledNetwork()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 170
    const-string v4, "ChromecastHelper"

    const-string v5, "checkEnabledNetwork"

    const-string v6, ""

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 174
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 175
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    const/4 v2, 0x0

    .line 176
    .local v2, "wifiConnected":Z
    if-eqz v1, :cond_1

    .line 177
    const-string v4, "ChromecastHelper"

    const-string v5, "checkEnabledNetwork"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WIFI: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move v2, v3

    .line 182
    :cond_1
    :goto_0
    return v2

    .line 178
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDeviceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public initiate()V
    .locals 3

    .prologue
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.chromecastservice"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const-string v0, "ChromecastHelper"

    const-string v1, "initiate"

    const-string v2, "AppPackageUtil.isAppInstalled  is true"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->ConnectCF()V

    .line 44
    :cond_0
    return-void
.end method

.method public startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .prologue
    .line 73
    const-string v0, "ChromecastHelper"

    const-string v1, "startDiscovery"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.chromecastservice"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 78
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    if-nez v0, :cond_2

    .line 85
    const-string v0, "ChromecastHelper"

    const-string v1, "startDiscovery"

    const-string v2, "DeviceFinder is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    :goto_1
    return-void

    .line 81
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceList:Ljava/util/ArrayList;

    goto :goto_0

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/sdial/DeviceFinder;->startDiscovery()V

    goto :goto_1
.end method

.method public stopDiscovery()V
    .locals 3

    .prologue
    .line 94
    const-string v0, "ChromecastHelper"

    const-string v1, "stopDiscovery"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDeviceFinder:Lcom/sec/android/sdial/DeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/sdial/DeviceFinder;->stopDiscovery()V

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 99
    :cond_0
    return-void
.end method

.method public terminate()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.chromecastservice"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ChromecastHelper;->DisconnectCF()V

    .line 51
    :cond_0
    return-void
.end method
