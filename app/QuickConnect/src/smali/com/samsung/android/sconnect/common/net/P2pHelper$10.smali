.class Lcom/samsung/android/sconnect/common/net/P2pHelper$10;
.super Ljava/lang/Object;
.source "P2pHelper.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/P2pHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;)V
    .locals 0

    .prologue
    .line 837
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 14
    .param p1, "peers"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    const-wide/32 v12, 0x9c40

    const/4 v10, 0x4

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 840
    const-string v4, "P2pHelper"

    const-string v5, "onPeersAvailable"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Entered onPeersAvailable "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 844
    .local v3, "msg":Landroid/os/Message;
    iput-object p1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 845
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWorkThread:Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$500(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper$P2pHelperWorkThread;->sendMessage(Landroid/os/Message;)I

    .line 847
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfigList;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfigList;-><init>()V

    .line 849
    .local v0, "configList":Landroid/net/wifi/p2p/WifiP2pConfigList;
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$100(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z

    move-result v4

    if-ne v4, v8, :cond_1

    .line 850
    const-string v4, "P2pHelper"

    const-string v5, "onPeersAvailable"

    const-string v6, "waiting createGroup.."

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    :cond_0
    :goto_0
    return-void

    .line 853
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mReqCreateGroup:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$100(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z

    move-result v4

    if-ne v4, v8, :cond_3

    .line 854
    :cond_2
    const-string v4, "P2pHelper"

    const-string v5, "onPeersAvailable"

    const-string v6, "already request.."

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 857
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 861
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 862
    .local v1, "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 863
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->isBusyDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    invoke-static {v4, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$600(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 864
    const-string v4, "P2pHelper"

    const-string v5, "onPeersAvailable"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isBusyDevice - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 867
    :cond_5
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWifiDisplay(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 868
    const-string v4, "P2pHelper"

    const-string v5, "onPeersAvailable"

    const-string v6, "isWifiDisplay"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 870
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 871
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    # setter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdConnecting:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$702(Lcom/samsung/android/sconnect/common/net/P2pHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 872
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 873
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 874
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mDiscoveryHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$800(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v10, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 877
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$900(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Lcom/samsung/android/sconnect/common/util/WfdUtil;

    move-result-object v4

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->connectWifiDisplay(Ljava/lang/String;)V

    .line 884
    :goto_2
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pConfigList;->getAllCount()I

    move-result v4

    if-ne v4, v8, :cond_4

    .line 885
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->connect(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    invoke-static {v4, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)V

    goto/16 :goto_0

    .line 879
    :cond_7
    const-string v4, "P2pHelper"

    const-string v5, "connect"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " update "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->getPreferredConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;
    invoke-static {v4, v1}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1000(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/net/wifi/p2p/WifiP2pConfigList;->update(Landroid/net/wifi/p2p/WifiP2pConfig;)V

    .line 881
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mTargetList:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 882
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 891
    .end local v1    # "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_8
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pConfigList;->getAllCount()I

    move-result v4

    if-nez v4, :cond_9

    .line 892
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mP2pFinding:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/P2pHelper;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 893
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-virtual {v4, v12, v13, v9, v9}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->startFindP2p(JZZ)V

    goto/16 :goto_0

    .line 896
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$10;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/P2pHelper;->connectList(Landroid/net/wifi/p2p/WifiP2pConfigList;)V
    invoke-static {v4, v0}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/P2pHelper;Landroid/net/wifi/p2p/WifiP2pConfigList;)V

    goto/16 :goto_0
.end method
