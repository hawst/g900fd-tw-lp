.class public Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;
.super Ljava/lang/Object;
.source "DeviceSlink.java"


# instance fields
.field private mDeviceType:I

.field private mId:J

.field private mIpAddr:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JILjava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # J
    .param p4, "type"    # I
    .param p5, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->mName:Ljava/lang/String;

    .line 12
    iput-wide p2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->mId:J

    .line 13
    iput p4, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->mDeviceType:I

    .line 14
    iput-object p5, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->mIpAddr:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getDeviceType()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->mDeviceType:I

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->mId:J

    return-wide v0
.end method

.method public getIpAddr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->mIpAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->mName:Ljava/lang/String;

    return-object v0
.end method
