.class public Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;
.super Ljava/lang/Object;
.source "PreDiscoveryPacket.java"


# instance fields
.field public bleAddr:Ljava/lang/String;

.field public btAddr:Ljava/lang/String;

.field public data:[B

.field public name:Ljava/lang/String;

.field public netType:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0
    .param p1, "netType"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "bleAddr"    # Ljava/lang/String;
    .param p4, "btAddr"    # Ljava/lang/String;
    .param p5, "sConnectData"    # [B

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->netType:I

    .line 15
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->name:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->btAddr:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->bleAddr:Ljava/lang/String;

    .line 18
    iput-object p5, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->data:[B

    .line 19
    return-void
.end method
