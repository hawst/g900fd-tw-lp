.class Lcom/samsung/android/sconnect/central/ui/ViewManager$9;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/ViewManager;->expandActionList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$9;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 519
    const-string v0, "ViewManager"

    const-string v1, "device view onTouch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$9;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$1100(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 521
    const/4 v0, 0x0

    return v0
.end method
