.class Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;
.super Ljava/lang/Object;
.source "P2pHelper.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/P2pHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionConnectListener"
.end annotation


# instance fields
.field private mExtraInfo:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/common/net/P2pHelper;Ljava/lang/String;)V
    .locals 1
    .param p2, "extraInfo"    # Ljava/lang/String;

    .prologue
    .line 773
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 771
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;->mExtraInfo:Ljava/lang/String;

    .line 774
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;->mExtraInfo:Ljava/lang/String;

    .line 775
    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4
    .param p1, "reason"    # I

    .prologue
    .line 779
    const-string v0, "P2pHelper"

    const-string v1, "connect"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connect failed with reason:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;->mExtraInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;->this$0:Lcom/samsung/android/sconnect/common/net/P2pHelper;

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/P2pHelper;->mRequestList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;->mExtraInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 782
    # getter for: Lcom/samsung/android/sconnect/common/net/P2pHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->access$200()Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    move-result-object v0

    sget-byte v1, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/P2pHelper$ActionConnectListener;->mExtraInfo:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;->onDisconnected(BLjava/lang/String;)V

    .line 784
    :cond_0
    return-void
.end method

.method public onSuccess()V
    .locals 3

    .prologue
    .line 788
    const-string v0, "P2pHelper"

    const-string v1, "connect"

    const-string v2, "Connect Success"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    return-void
.end method
