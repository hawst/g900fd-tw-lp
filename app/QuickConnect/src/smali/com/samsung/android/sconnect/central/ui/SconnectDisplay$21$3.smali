.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;)V
    .locals 0

    .prologue
    .line 2578
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2581
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2582
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2583
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showSendErrorDialog()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    .line 2584
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->refreshDiscoveryDevice()V

    .line 2613
    :cond_1
    :goto_0
    return-void

    .line 2585
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsPrintable:Z
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2586
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0900d5

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900d2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090088

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3$1;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 2606
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2607
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v0

    .line 2609
    .local v0, "mSConnectManager":Lcom/samsung/android/sconnect/SconnectManager;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/SconnectManager;->setPrinterContext(Landroid/content/Context;)V

    .line 2610
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    const/16 v3, 0xd

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;

    iget-object v4, v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;)Z

    goto :goto_0
.end method
