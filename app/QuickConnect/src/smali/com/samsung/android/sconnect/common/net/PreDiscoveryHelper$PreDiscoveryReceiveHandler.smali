.class Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;
.super Landroid/os/Handler;
.source "PreDiscoveryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PreDiscoveryReceiveHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1699
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    .line 1700
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1701
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 50
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1705
    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 2000
    :cond_0
    :goto_0
    return-void

    .line 1707
    :pswitch_0
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;

    .line 1708
    .local v39, "packet":Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;
    move-object/from16 v0, v39

    iget v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->netType:I

    move/from16 v38, v0

    .line 1709
    .local v38, "netType":I
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->name:Ljava/lang/String;

    move-object/from16 v34, v0

    .line 1710
    .local v34, "deviceName":Ljava/lang/String;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->btAddr:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 1711
    .local v24, "btAddr":Ljava/lang/String;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->bleAddr:Ljava/lang/String;

    move-object/from16 v23, v0

    .line 1712
    .local v23, "bleAddr":Ljava/lang/String;
    move-object/from16 v0, v39

    iget-object v9, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;->data:[B

    .line 1714
    .local v9, "sConnectData":[B
    const/16 v4, 0x8

    move/from16 v0, v38

    if-ne v0, v4, :cond_1

    .line 1715
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "netType : BLE"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1720
    :goto_1
    const/16 v40, -0x1

    .line 1722
    .local v40, "packetInfo":B
    const/16 v41, 0x0

    .line 1723
    .local v41, "respTarget":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1724
    .local v13, "btMac":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1725
    .local v6, "bleMac":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1726
    .local v8, "p2pMac":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1727
    .local v10, "number":Ljava/lang/String;
    const/16 v21, 0x0

    .line 1728
    .local v21, "ackTarget":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1730
    .local v5, "bdname":Ljava/lang/String;
    const/4 v4, 0x5

    aget-byte v40, v9, v4

    .line 1731
    array-length v4, v9

    const/16 v14, 0x37

    if-eq v4, v14, :cond_2

    .line 1732
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "data len "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    array-length v0, v9

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1717
    .end local v5    # "bdname":Ljava/lang/String;
    .end local v6    # "bleMac":Ljava/lang/String;
    .end local v8    # "p2pMac":Ljava/lang/String;
    .end local v10    # "number":Ljava/lang/String;
    .end local v13    # "btMac":Ljava/lang/String;
    .end local v21    # "ackTarget":Ljava/lang/String;
    .end local v40    # "packetInfo":B
    .end local v41    # "respTarget":Ljava/lang/String;
    :cond_1
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "netType : P2P"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1736
    .restart local v5    # "bdname":Ljava/lang/String;
    .restart local v6    # "bleMac":Ljava/lang/String;
    .restart local v8    # "p2pMac":Ljava/lang/String;
    .restart local v10    # "number":Ljava/lang/String;
    .restart local v13    # "btMac":Ljava/lang/String;
    .restart local v21    # "ackTarget":Ljava/lang/String;
    .restart local v40    # "packetInfo":B
    .restart local v41    # "respTarget":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x3

    new-array v0, v4, [B

    move-object/from16 v33, v0

    .line 1737
    .local v33, "deviceInfoHash":[B
    const/4 v4, 0x6

    const/4 v14, 0x0

    const/4 v15, 0x3

    move-object/from16 v0, v33

    invoke-static {v9, v4, v0, v14, v15}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1740
    const/16 v4, 0x9

    aget-byte v4, v9, v4

    and-int/lit8 v7, v4, 0xf

    .line 1741
    .local v7, "deviceType":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    const/16 v4, 0x9

    aget-byte v4, v9, v4

    and-int/lit8 v4, v4, -0x80

    const/16 v15, -0x80

    if-ne v4, v15, :cond_7

    const/4 v4, 0x1

    :goto_2
    # setter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mSupportP2pResponse:Z
    invoke-static {v14, v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2002(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Z)Z

    .line 1742
    const/4 v4, 0x2

    new-array v0, v4, [B

    move-object/from16 v19, v0

    .line 1743
    .local v19, "Servicetype":[B
    const/16 v4, 0xa

    const/4 v14, 0x0

    const/4 v15, 0x2

    move-object/from16 v0, v19

    invoke-static {v9, v4, v0, v14, v15}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1746
    const/4 v4, 0x6

    new-array v0, v4, [B

    move-object/from16 v27, v0

    .line 1747
    .local v27, "byteP2pMac":[B
    const/16 v26, 0x0

    .line 1748
    .local v26, "byteBleMac":[B
    const/16 v4, 0xc

    const/4 v14, 0x0

    const/4 v15, 0x6

    move-object/from16 v0, v27

    invoke-static {v9, v4, v0, v14, v15}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1749
    const/16 v4, 0x8

    move/from16 v0, v38

    if-ne v0, v4, :cond_8

    .line 1750
    invoke-static/range {v27 .. v27}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v8

    .line 1751
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToByte(Ljava/lang/String;)[B

    move-result-object v26

    .line 1752
    move-object/from16 v6, v23

    .line 1754
    move-object/from16 v13, v24

    .line 1775
    :goto_3
    const/4 v4, 0x6

    new-array v0, v4, [B

    move-object/from16 v46, v0

    .line 1776
    .local v46, "target":[B
    const/16 v4, 0x12

    const/4 v14, 0x0

    const/4 v15, 0x6

    move-object/from16 v0, v46

    invoke-static {v9, v4, v0, v14, v15}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1777
    invoke-static/range {v46 .. v46}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v41

    .line 1779
    const/4 v4, 0x2

    new-array v0, v4, [B

    move-object/from16 v32, v0

    .line 1780
    .local v32, "deviceInfoCRC":[B
    const/16 v4, 0x18

    const/4 v14, 0x0

    const/4 v15, 0x2

    move-object/from16 v0, v32

    invoke-static {v9, v4, v0, v14, v15}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1782
    const/16 v30, 0x0

    .line 1783
    .local v30, "compensationLen":I
    const/16 v4, 0x8

    move/from16 v0, v38

    if-eq v0, v4, :cond_3

    if-nez v34, :cond_f

    .line 1784
    :cond_3
    const/16 v37, 0x0

    .line 1785
    .local v37, "name":[B
    const/16 v4, 0x1a

    aget-byte v4, v9, v4

    const/16 v14, 0x75

    if-ne v4, v14, :cond_c

    const/16 v4, 0x1b

    aget-byte v4, v9, v4

    if-nez v4, :cond_c

    .line 1787
    const/16 v4, 0xb

    new-array v0, v4, [B

    move-object/from16 v37, v0

    .line 1788
    const/16 v4, 0x1c

    const/4 v14, 0x0

    const/16 v15, 0xb

    move-object/from16 v0, v37

    invoke-static {v9, v4, v0, v14, v15}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1796
    :goto_4
    new-instance v5, Ljava/lang/String;

    .end local v5    # "bdname":Ljava/lang/String;
    move-object/from16 v0, v37

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    .line 1798
    .restart local v5    # "bdname":Ljava/lang/String;
    const/16 v36, 0x0

    .local v36, "i":I
    :goto_5
    :try_start_0
    move-object/from16 v0, v37

    array-length v4, v0

    move/from16 v0, v36

    if-ge v0, v4, :cond_4

    .line 1799
    aget-byte v4, v37, v36

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v47

    .line 1800
    .local v47, "value":Ljava/lang/String;
    const-string v4, "0"

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1801
    new-instance v22, Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v37

    move/from16 v2, v36

    invoke-direct {v0, v1, v4, v2}, Ljava/lang/String;-><init>([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1802
    .end local v5    # "bdname":Ljava/lang/String;
    .local v22, "bdname":Ljava/lang/String;
    :try_start_1
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Name changed(contains null): "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v5, v22

    .line 1816
    .end local v22    # "bdname":Ljava/lang/String;
    .end local v47    # "value":Ljava/lang/String;
    .restart local v5    # "bdname":Ljava/lang/String;
    :cond_4
    :goto_6
    const-string v4, "..."

    invoke-virtual {v5, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v14, v30, 0xb

    if-lt v4, v14, :cond_5

    .line 1818
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v5, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v14, "..."

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1819
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Name changed(over 13byte): "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1826
    .end local v36    # "i":I
    .end local v37    # "name":[B
    :cond_5
    :goto_7
    const/16 v4, 0x10

    new-array v0, v4, [B

    move-object/from16 v28, v0

    .line 1827
    .local v28, "byte_ackTarget":[B
    and-int/lit8 v4, v40, 0xc

    const/16 v14, 0x8

    if-ne v4, v14, :cond_10

    .line 1828
    const/16 v4, 0x27

    const/4 v14, 0x0

    const/16 v15, 0x10

    move-object/from16 v0, v28

    invoke-static {v9, v4, v0, v14, v15}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1830
    invoke-static/range {v28 .. v28}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v21

    .line 1837
    :goto_8
    invoke-static/range {v33 .. v33}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v45

    .line 1838
    .local v45, "strHash":Ljava/lang/String;
    invoke-static/range {v32 .. v32}, Lcom/samsung/android/sconnect/common/util/Util;->byteToString([B)Ljava/lang/String;

    move-result-object v44

    .line 1839
    .local v44, "strCRC":Ljava/lang/String;
    const/16 v31, 0x0

    .line 1841
    .local v31, "data":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v44

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1842
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "Find in ContactCache"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1843
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v44

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v31

    .end local v31    # "data":Ljava/lang/String;
    check-cast v31, Ljava/lang/String;

    .line 1856
    .restart local v31    # "data":Ljava/lang/String;
    :goto_9
    if-nez v31, :cond_13

    .line 1857
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener*****  find Contact (fail) :"

    move-object/from16 v0, v31

    invoke-static {v4, v14, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1865
    :goto_a
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "PACKET INFO - name: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/PacketInfo :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, v40

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/RESP Target :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v41

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/BLE :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/BT :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/P2P :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/Number :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/ACK Target :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1870
    const/4 v4, 0x5

    aget-byte v4, v9, v4

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_6

    .line 1871
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->handleCmdData(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLjava/lang/String;)V
    invoke-static/range {v4 .. v10}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2800(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLjava/lang/String;)V

    .line 1874
    :cond_6
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v4

    if-nez v4, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPowerManager:Landroid/os/PowerManager;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/os/PowerManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v4

    if-nez v4, :cond_14

    .line 1875
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "LCD OFF!! ignore Packet"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1741
    .end local v19    # "Servicetype":[B
    .end local v26    # "byteBleMac":[B
    .end local v27    # "byteP2pMac":[B
    .end local v28    # "byte_ackTarget":[B
    .end local v30    # "compensationLen":I
    .end local v31    # "data":Ljava/lang/String;
    .end local v32    # "deviceInfoCRC":[B
    .end local v44    # "strCRC":Ljava/lang/String;
    .end local v45    # "strHash":Ljava/lang/String;
    .end local v46    # "target":[B
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1756
    .restart local v19    # "Servicetype":[B
    .restart local v26    # "byteBleMac":[B
    .restart local v27    # "byteP2pMac":[B
    :cond_8
    move-object/from16 v8, v23

    .line 1757
    move-object/from16 v26, v27

    .line 1758
    invoke-static/range {v27 .. v27}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v6

    .line 1760
    const/16 v4, 0x9

    aget-byte v4, v9, v4

    and-int/lit8 v4, v4, 0x40

    if-nez v4, :cond_9

    .line 1761
    move-object v13, v6

    goto/16 :goto_3

    .line 1763
    :cond_9
    const/4 v4, 0x0

    const/4 v14, 0x2

    invoke-virtual {v6, v4, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v14, 0x10

    invoke-static {v4, v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-byte v0, v4

    move/from16 v29, v0

    .line 1766
    .local v29, "checker":B
    move/from16 v0, v29

    and-int/lit16 v4, v0, 0xc0

    const/16 v14, 0xc0

    if-ne v4, v14, :cond_b

    .line 1767
    invoke-static {v6}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getBtMacAddrFromBle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 1768
    .local v25, "btmac":Ljava/lang/String;
    if-eqz v25, :cond_a

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    .line 1769
    :goto_b
    goto/16 :goto_3

    :cond_a
    move-object/from16 v13, v25

    .line 1768
    goto :goto_b

    .line 1770
    .end local v25    # "btmac":Ljava/lang/String;
    :cond_b
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "Dynamic Random address"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1791
    .end local v29    # "checker":B
    .restart local v30    # "compensationLen":I
    .restart local v32    # "deviceInfoCRC":[B
    .restart local v37    # "name":[B
    .restart local v46    # "target":[B
    :cond_c
    const/16 v30, 0x2

    .line 1792
    const/16 v4, 0xd

    new-array v0, v4, [B

    move-object/from16 v37, v0

    .line 1793
    const/16 v4, 0x1a

    const/4 v14, 0x0

    const/16 v15, 0xd

    move-object/from16 v0, v37

    invoke-static {v9, v4, v0, v14, v15}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto/16 :goto_4

    .line 1805
    .restart local v36    # "i":I
    .restart local v47    # "value":Ljava/lang/String;
    :cond_d
    :try_start_2
    const-string v4, "10"

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1806
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v14, Ljava/lang/String;

    const/4 v15, 0x0

    move-object/from16 v0, v37

    move/from16 v1, v36

    invoke-direct {v14, v0, v15, v1}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v14, "..."

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1807
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Name changed(contains newline): "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_6

    .line 1812
    .end local v47    # "value":Ljava/lang/String;
    :catch_0
    move-exception v35

    .line 1813
    .local v35, "e":Ljava/lang/Exception;
    :goto_c
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v35

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1798
    .end local v35    # "e":Ljava/lang/Exception;
    .restart local v47    # "value":Ljava/lang/String;
    :cond_e
    add-int/lit8 v36, v36, 0x1

    goto/16 :goto_5

    .line 1823
    .end local v36    # "i":I
    .end local v37    # "name":[B
    .end local v47    # "value":Ljava/lang/String;
    :cond_f
    move-object/from16 v5, v34

    goto/16 :goto_7

    .line 1832
    .restart local v28    # "byte_ackTarget":[B
    :cond_10
    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->ACK_TARGET_NONE:[B
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2700()[B

    move-result-object v4

    const/4 v14, 0x0

    const/4 v15, 0x0

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->ACK_TARGET_NONE:[B
    invoke-static {}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2700()[B

    move-result-object v16

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move-object/from16 v0, v28

    move/from16 v1, v16

    invoke-static {v4, v14, v0, v15, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1834
    invoke-static/range {v28 .. v28}, Lcom/samsung/android/sconnect/common/util/NetUtil;->parseMacAddressToString([B)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_8

    .line 1845
    .restart local v31    # "data":Ljava/lang/String;
    .restart local v44    # "strCRC":Ljava/lang/String;
    .restart local v45    # "strHash":Ljava/lang/String;
    :cond_11
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "Not Find in ContactCache"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1846
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-static {v4, v0, v1}, Lcom/samsung/android/sconnect/common/util/Hash;->retrieveDB(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 1847
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    const/16 v14, 0x31

    if-le v4, v14, :cond_12

    .line 1848
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "ContactCache Clear"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1849
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 1851
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v44

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v31

    invoke-virtual {v4, v14, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1852
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ContactCache size : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContactCache:Ljava/util/HashMap;
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/HashMap;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 1860
    :cond_13
    move-object/from16 v10, v31

    .line 1861
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener*****  find Contact (Success) :"

    move-object/from16 v0, v31

    invoke-static {v4, v14, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1879
    :cond_14
    if-nez v26, :cond_15

    .line 1880
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "btMac is Null"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1884
    :cond_15
    and-int/lit8 v4, v40, 0x8

    if-eqz v4, :cond_17

    .line 1885
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "FLAG_PACKETINFO_SCANMODE found!!"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1887
    const/16 v20, 0x1

    .line 1888
    .local v20, "acceptRequest":Z
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/SconnectManager;->isCentralRunning()Z

    move-result v4

    if-nez v4, :cond_19

    .line 1889
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)I

    move-result v4

    const/4 v14, 0x2

    if-ne v4, v14, :cond_18

    .line 1890
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "ignore discovery, SETTING_NOT_VISIBLE!!"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1892
    const/16 v20, 0x0

    .line 1903
    :cond_16
    :goto_d
    if-eqz v20, :cond_17

    .line 1904
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v4

    const/4 v14, 0x4

    invoke-virtual {v4, v14}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->removeMessages(I)V

    .line 1906
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryRespRunning:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 1907
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, v28

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->checkForAckTarget([B)Z
    invoke-static {v4, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 1908
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, v26

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->addDiscoveryRespAckDevice([B)V
    invoke-static {v4, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)V

    .line 1909
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->isReceiveAllAck()Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1910
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v4

    const/4 v14, 0x4

    invoke-virtual {v4, v14}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessage(I)Z

    .line 1955
    .end local v20    # "acceptRequest":Z
    :cond_17
    :goto_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, v46

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->checkForRespTarget([B)Z
    invoke-static {v4, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3400(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)Z

    move-result v4

    if-nez v4, :cond_1f

    and-int/lit8 v4, v40, 0x2

    if-nez v4, :cond_1f

    and-int/lit8 v4, v40, 0x8

    if-nez v4, :cond_1f

    .line 1957
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "Ignore - not for me"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1893
    .restart local v20    # "acceptRequest":Z
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)I

    move-result v4

    const/4 v14, 0x1

    if-ne v4, v14, :cond_16

    if-nez v10, :cond_16

    .line 1895
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "ignore discovery, SETTING_ONLY_CONTACTS & not exist in my contact!!"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1897
    const/16 v20, 0x0

    goto :goto_d

    .line 1900
    :cond_19
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "Accept discovery, Central Running!!"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1913
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v4

    const/4 v14, 0x4

    const-wide/16 v48, 0xbb8

    move-wide/from16 v0, v48

    invoke-virtual {v4, v14, v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_e

    .line 1917
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, v26

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->isNewDiscoveryDevice([B)Z
    invoke-static {v4, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)Z

    move-result v4

    if-nez v4, :cond_1c

    .line 1918
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v4

    const/4 v14, 0x4

    const-wide/16 v48, 0xbb8

    move-wide/from16 v0, v48

    invoke-virtual {v4, v14, v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_e

    .line 1921
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mVisibilitySetting:I
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2900(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)I

    move-result v4

    if-nez v4, :cond_1d

    .line 1922
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v4

    const/4 v14, 0x4

    const-wide/16 v48, 0xbb8

    move-wide/from16 v0, v48

    invoke-virtual {v4, v14, v0, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1928
    :goto_f
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "mIsDiscoveryRespRunning - new Discovery Request device!!!"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 1925
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v4

    const/4 v14, 0x3

    invoke-virtual {v4, v14}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessage(I)Z

    goto :goto_f

    .line 1933
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, v28

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->checkForAckTarget([B)Z
    invoke-static {v4, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)Z

    move-result v4

    if-nez v4, :cond_17

    .line 1934
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, v26

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->isNewDiscoveryDevice([B)Z
    invoke-static {v4, v0}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[B)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 1935
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryWorkHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;

    move-result-object v4

    const/4 v14, 0x3

    invoke-virtual {v4, v14}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryWorkHandler;->sendEmptyMessage(I)Z

    .line 1942
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    const-string v15, "new Discovery Request device!!!"

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 1961
    .end local v20    # "acceptRequest":Z
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-object/from16 v0, v26

    move/from16 v1, v38

    move-object/from16 v2, v34

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->isNewDiscoveryRespDevice([BILjava/lang/String;)I
    invoke-static {v4, v0, v1, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3500(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;[BILjava/lang/String;)I

    move-result v42

    .line 1962
    .local v42, "result":I
    const/4 v4, 0x1

    move/from16 v0, v42

    if-eq v0, v4, :cond_20

    const/4 v4, 0x2

    move/from16 v0, v42

    if-ne v0, v4, :cond_0

    .line 1963
    :cond_20
    move-object v12, v5

    .line 1964
    .local v12, "displayname":Ljava/lang/String;
    const/16 v17, 0x0

    .line 1966
    .local v17, "byteArrayToIntValue":I
    const/4 v4, 0x0

    aget-byte v4, v19, v4

    shl-int/lit8 v4, v4, 0x8

    const v14, 0xff00

    and-int/2addr v4, v14

    or-int v17, v17, v4

    .line 1967
    const/4 v4, 0x1

    aget-byte v4, v19, v4

    and-int/lit16 v4, v4, 0xff

    or-int v17, v17, v4

    .line 1969
    new-instance v11, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;

    move-object v14, v6

    move-object v15, v8

    move-object/from16 v16, v10

    move/from16 v18, v7

    invoke-direct/range {v11 .. v18}, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1973
    .local v11, "deviceBle":Lcom/samsung/android/sconnect/common/device/net/DeviceBle;
    new-instance v43, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-direct {v0, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 1974
    .local v43, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    move-object/from16 v0, v43

    invoke-virtual {v0, v11}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceBle;)V

    .line 1976
    const/4 v4, 0x1

    move/from16 v0, v42

    if-ne v0, v4, :cond_23

    .line 1977
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mIsDiscoveryReqRunning:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1700(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 1978
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->updateDiscAdv()V
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$3600(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    .line 1982
    :goto_10
    const-string v4, "PreDiscoveryHelper"

    const-string v14, "PreDiscoveryPacketListener"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "new Discovery Response device!!! "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1985
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mOldPreDiscoveryDeviceList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$2300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/4 v14, -0x1

    if-ne v4, v14, :cond_21

    .line 1986
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v4

    if-eqz v4, :cond_21

    .line 1987
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-interface {v4, v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 1990
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryDeviceList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1980
    :cond_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->initDiscoveryReq()V
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$000(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V

    goto :goto_10

    .line 1992
    :cond_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1993
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-interface {v4, v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_0

    .line 1812
    .end local v5    # "bdname":Ljava/lang/String;
    .end local v11    # "deviceBle":Lcom/samsung/android/sconnect/common/device/net/DeviceBle;
    .end local v12    # "displayname":Ljava/lang/String;
    .end local v17    # "byteArrayToIntValue":I
    .end local v28    # "byte_ackTarget":[B
    .end local v31    # "data":Ljava/lang/String;
    .end local v42    # "result":I
    .end local v43    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v44    # "strCRC":Ljava/lang/String;
    .end local v45    # "strHash":Ljava/lang/String;
    .restart local v22    # "bdname":Ljava/lang/String;
    .restart local v36    # "i":I
    .restart local v37    # "name":[B
    .restart local v47    # "value":Ljava/lang/String;
    :catch_1
    move-exception v35

    move-object/from16 v5, v22

    .end local v22    # "bdname":Ljava/lang/String;
    .restart local v5    # "bdname":Ljava/lang/String;
    goto/16 :goto_c

    .line 1705
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
