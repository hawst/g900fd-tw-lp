.class public final Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;
.super Ljava/lang/Object;
.source "ProfileHelper.java"


# static fields
.field static final D:Z = true

.field static final V:Z = true


# instance fields
.field final TAG:Ljava/lang/String;

.field adapter:Landroid/bluetooth/BluetoothAdapter;

.field dev:Landroid/bluetooth/BluetoothDevice;

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "ProfileHelper"

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->TAG:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->adapter:Landroid/bluetooth/BluetoothAdapter;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->dev:Landroid/bluetooth/BluetoothDevice;

    .line 41
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/ProfileHelper;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method public bondDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 5
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 48
    if-nez p1, :cond_0

    .line 49
    const-string v1, "ProfileHelper"

    const-string v2, "bondDevice"

    const-string v3, "dev is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const/4 v1, 0x0

    .line 60
    :goto_0
    return v1

    .line 53
    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    .line 54
    .local v0, "state":I
    const-string v1, "ProfileHelper"

    const-string v2, "bondDevice"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bond state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 57
    const-string v1, "ProfileHelper"

    const-string v2, "bondDevice"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bonding..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z
    .locals 1
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "profile"    # Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;

    .prologue
    .line 80
    if-eqz p2, :cond_0

    invoke-interface {p2, p1}, Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x1

    .line 84
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z
    .locals 1
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "profile"    # Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;

    .prologue
    .line 88
    if-eqz p2, :cond_0

    invoke-interface {p2, p1}, Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    .line 92
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected(Landroid/bluetooth/BluetoothDevice;Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;)Z
    .locals 2
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "profile"    # Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;

    .prologue
    .line 96
    if-eqz p2, :cond_0

    invoke-interface {p2, p1}, Lcom/samsung/android/sconnect/central/action/BtProfile/LocalBluetoothProfile;->getConnectionStatus(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 97
    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unbondDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 5
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 65
    const-string v1, "ProfileHelper"

    const-string v2, "unbondDevice"

    const-string v3, "dev is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const/4 v1, 0x0

    .line 76
    :goto_0
    return v1

    .line 69
    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    .line 70
    .local v0, "state":I
    const-string v1, "ProfileHelper"

    const-string v2, "unbondDevice"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bond state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 73
    const-string v1, "ProfileHelper"

    const-string v2, "unbondDevice"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unbonding..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->removeBond()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
