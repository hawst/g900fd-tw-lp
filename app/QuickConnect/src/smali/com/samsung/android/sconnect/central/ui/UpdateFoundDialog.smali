.class public Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;
.super Landroid/app/Activity;
.source "UpdateFoundDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UpdateFoundDialog"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSconnectDisplayActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field private mTutorialActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

.field private mUpdateDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mUpdateDialog:Landroid/app/Dialog;

    .line 28
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->SconnectDisplayActivitytoFinish:Landroid/app/Activity;

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mSconnectDisplayActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    .line 29
    sget-object v0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;->TutorialActivitytoFinish:Landroid/app/Activity;

    check-cast v0, Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mTutorialActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mSconnectDisplayActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Lcom/samsung/android/sconnect/central/ui/TutorialActivity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mTutorialActivitytoFinish:Lcom/samsung/android/sconnect/central/ui/TutorialActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 33
    const-string v1, "UpdateFoundDialog"

    const-string v2, "onCreate"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    iput-object p0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mContext:Landroid/content/Context;

    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 38
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->showUpdatePopup()V

    .line 40
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x400000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 45
    :goto_0
    return-void

    .line 42
    :cond_0
    const-string v1, "UpdateFoundDialog"

    const-string v2, "onCreate"

    const-string v3, "ERROR - missed Intent data"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 49
    const-string v0, "UpdateFoundDialog"

    const-string v1, "onDestroy"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mUpdateDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mUpdateDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const-string v0, "UpdateFoundDialog"

    const-string v1, "onDestroy"

    const-string v2, "--"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mUpdateDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 56
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 57
    return-void
.end method

.method protected showUpdatePopup()V
    .locals 5

    .prologue
    .line 60
    const-string v1, "UpdateFoundDialog"

    const-string v2, "showUpdatePopup"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :try_start_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090014

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090185

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090088

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$2;-><init>(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090089

    new-instance v3, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$1;-><init>(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mUpdateDialog:Landroid/app/Dialog;

    .line 91
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;->mUpdateDialog:Landroid/app/Dialog;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog$3;-><init>(Lcom/samsung/android/sconnect/central/ui/UpdateFoundDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UpdateFoundDialog"

    const-string v2, "showUpdatePopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
